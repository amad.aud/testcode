from django.conf.urls import url, include
from branch import views as branch_views
import branch.views.people as people_view
import branch.views.events as event_views
import branch.views.pages as page_views
from venture import views as venture_views
from trunk import views as trunk_views

urlpatterns = [
    # Nav Discover
    url(r'^startups$',
        branch_views.DiscoverStartup.as_view(), name='discover_startup'),
    url(r'^startups_test$',
        branch_views.DiscoverStartupTest.as_view(), name='discover_startup_test'),
    url(r'^projects$',
        branch_views.DiscoverProject.as_view(), name='discover_projects'),
    url(r'^opportunities$', venture_views.DiscoverOpportunity.as_view(),
        name='discover_opportunities'),
    # Nav People
    url(r'^people/(?P<page_name>[^/]+)$',
        people_view.DiscoverPeople.as_view(), name='discover_people'),
    url(r'^display_team_mentors$', people_view.display_team_mentors_view, name='display_team_mentors_view'),
    url(r'^resources$', branch_views.discover_resources,
        name='discover_resources'),
    url(r'^resources/edit_categories$', branch_views.edit_resource_categories,
        name='edit_resource_categories'),
    url(r'^events$', event_views.DiscoverEvents.as_view(), name='discover_events'),
    # url(r'^events$', event_views.user_create_event_form, name='user-create-event'),
    url(r'^events/calendar$', event_views.discover_events_calendar, name='discover_events_calendar'),
    url(r'^competitions$', event_views.DiscoverCompetitions.as_view(), name='discover_competitions'),
    url(r'^applications$', event_views.DiscoverApplications.as_view(), name='discover_applications'),
    url(r'^surveys$', event_views.DiscoverSurveys.as_view(), name='discover_surveys'),
    url(r'^roadmap$', trunk_views.ManageRoadMap.as_view(), name='discover_roadmap'),
     # Nav Community
    url(r'^resources$', branch_views.discover_resources,
        name='discover_resources'),
    url(r'^forum/', include('discussion.urls.discover_forum_urls')),
    url(r'^pages$', page_views.DiscoverPages.as_view(),
        name='discover_pages')
]
