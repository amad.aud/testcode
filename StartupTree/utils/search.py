from elasticsearch_dsl import analyzer, tokenizer
"""
For Documents that does not require additional filtering for autocomplete suggestion,
just use field.CompletionField() to utilize elasticsearch's Completion Suggester.
If the documents require additional filterings, we cannot use completion suggester,
so use StringField() with analyzer set to the below edge_ngram analyzer.
"""
# ngram_analyzer = analyzer(
#     'ngram_analyzer',
#     tokenizer=tokenizer('autocomplete', 'edge_ngram', min_gram=1, max_gram=15),
#     filter=['lowercase']
# )
ngram_analyzer = analyzer(
    'ngram_analyzer',
    tokenizer=tokenizer('autocomplete', 'whitespace'),
    filter=['lowercase']
)


"""
HTML strip for summernote related fields.
"""
html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["classic", "lowercase", "stop"],
    char_filter=["html_strip"]
)


def needs_escaping(character):
    escape_chars = {
        '\\' : True, '+' : True, '-' : True, '!' : True,
        '(' : True, ')' : True, ':' : True, '^' : True,
        '[' : True, ']': True, '\"' : True, '{' : True,
        '}' : True, '~' : True, '*' : True, '?' : True,
        '|' : True, '&' : True, '/' : True
    }
    return escape_chars.get(character, False)


def escape_query(query):
    query = query.strip()
    sanitized = ''
    for character in query:
        if needs_escaping(character):
            sanitized += '\\%s' % character
        else:
            sanitized += character
    return sanitized.lower()
