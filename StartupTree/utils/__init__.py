# Django Imports
from bleach.utils import force_unicode
from django.core.validators import FileExtensionValidator, get_available_image_extensions
from django.db.models import QuerySet
from django.test import Client
from django.utils import timezone
from django.utils.cache import get_cache_key

# StartupTree Imports
from django.utils.safestring import mark_safe

from StartupTree.caches import page_caches
from StartupTree.loggers import app_logger
import StartupTree.settings as settings

# Other Libraries
import bleach
from bleach.sanitizer import ALLOWED_TAGS, ALLOWED_ATTRIBUTES
import bs4
import calendar
from datetime import date, time, datetime
import io as StringIO
import math
from PIL import Image
import random
import six
import string as string_lib
import sys
from typing import Iterator, Union, TypeVar, Generic
import unicodedata
import re


def custom_variable_extractor(text):
    return re.findall(r'\b_(?:\w*_)?\b', text)


def custom_variable_highlight(text):
    custom_variables = custom_variable_extractor(text)
    for variable in custom_variables:
        text = text.replace(
            variable, '<b>{}</b>'.format(variable))

    return text


def better_title(string):
    if string is None:
        return ""
    tmp = string.title().split(" ")
    new_tmp = []
    for t in tmp:
        if t == 'And':
            t = 'and'
        if t == 'Of':
            t = 'of'
        if t == 'In':
            t = 'in'
        if t == 'The':
            t = 'the'
        new_tmp.append(t)
    return str.join(' ', new_tmp)


def make_plural(word):
    if word.endswith('y'):
        word = word[:-1]
        word += 'ies'
    elif word.endswith(('s', 'x', 'z', 'ch', 'sh')):
        word += 'es'
    else:
        word += 's'
    return word


def to_unicode(string):
    if isinstance(string, six.string_types):
        return str(string)
    return string


def unicode_safe(string):
    if sys.version_info < (3, 0):
        return unicodedata.normalize('NFKD', to_unicode(string)) \
            .encode('utf-8', 'ignore')
    return str(string)


def filter_ascii(string):
    """ Useful for generating filenames that can only contain ascii characters. """
    ascii_set = set(string_lib.printable)
    return ''.join(filter(lambda c: c in ascii_set, string))


def replace_mentorship_word(private_str, uni):
    """
    replaces all instances of "mentorship" with the university's designated word, in the same casing.

    Applies if the word(s) used is all lower case, all uppercase, or title case.
    """
    from trunk.models import University, UniversityStyle  # avoid circular import
    uni: University
    uni_style: UniversityStyle = uni.style
    intermediate_str = private_str
    intermediate_str = str.replace(intermediate_str, 'Mentorship', uni_style.mentorship_word.title())
    intermediate_str = str.replace(intermediate_str, 'mentorship', uni_style.mentorship_word)
    intermediate_str = str.replace(intermediate_str, 'MENTORSHIP', uni_style.mentorship_word.upper())
    return intermediate_str


B = 1000000000
M = 1000000
K = 1000


def dollar_string_to_number(dollar_string):
    """convert dollar in string format to number and return it


    :param dollar_string: dollar amount in string (e.g. $199.6M)
    :return: $<amount>(b|m|k|'')
    """
    if not isinstance(dollar_string, str) or len(dollar_string) < 1:
        raise ValueError("An argument must be a none empty string.")
    if dollar_string[0] != '$':
        raise ValueError("An argument must start with $.")
    try:
        amount = dollar_string.split("$")[1].lower()
    except IndexError:
        raise ValueError("Dollar String must contain value after $.")
    try:
        if len(amount) > 1:
            dollar_string_number = float(amount[:-1])
        else:
            return int(amount)

        if dollar_string_number < 0:
            raise ValueError("Dollar amount must be positive.")
        if amount[-1] == 'b':
            return dollar_string_number * B
        elif amount[-1] == 'm':
            return dollar_string_number * M
        elif amount[-1] == 'k':
            return dollar_string_number * K
        else:
            return float(amount)
    except ValueError:
        raise ValueError("An argument must end with number, b, m or k.")


def number_to_dollar_string(number, round_decimal=False):
    """convert number to dollar in string format

    :param round_decimal:
    :param number: number
    :return: $<amount>(B|M|K|'')
    """

    def stringfy(num, base, unit, round_dec=False):
        if base > 1:
            num = math.ceil(((number / float(base)) * 100)) / 100
            tmp = str(num).split(".")
            if len(tmp) > 1 and not (len(tmp) == 2 and tmp[1] == '0'):
                if round_dec:
                    num = round(num, 1)
                return "${0}{1}".format(num, unit)
            return "${0}{1}".format(tmp[0], unit)
        return "${0}{1}".format(num, unit)

    if not isinstance(number, int) and not isinstance(number, float):
        try:
            number = int(number)
        except ValueError and TypeError:
            raise ValueError("An argument must be an integer.")

    if number < 0:
        raise ValueError("An argument must be a positive integer.")

    if number / B >= 1:
        return stringfy(number, B, "B", round_decimal)
    elif number / M >= 1:
        return stringfy(number, M, "M", round_decimal)
    elif number / K >= 1:
        return stringfy(number, K, "K", round_decimal)
    else:
        return stringfy(number, 1, "", round_decimal)


def invalidate_page_cache(request, path):
    request.path = "/startup/appinions"
    key = get_cache_key(request)
    page_caches.delete(key)
    return True


def size_down_pic(f_in, size=600):
    """unlike thumbnail, this maintains the ratio of the picture and just sizes down without padding"""
    image = Image.open(f_in)
    dimensions = (size, size)
    image.thumbnail(dimensions, Image.ANTIALIAS)
    thumb = image
    # thumb_io should be closed when the usage is ended
    thumb_io = StringIO.BytesIO()
    try:
        thumb.save(thumb_io, format='PNG')
    except OSError:
        thumb.save(thumb_io, format='JPEG')
    return thumb_io, image


def make_thumb(f_in, size=(150, 150), pad=True):
    """Thumbnail creator.
    pad = True to fit image into the size without distorting the image.
    :param f_in:
    :param size:
    :param pad:
    :return: thumb_io, files
        - files has to be closed after calling ".save()" of the modals to upload the image.
    """
    image = Image.open(f_in)
    # check if original image is small
    try:
        # https://stackoverflow.com/questions/37941648/unable-to-crop-away-transparency-neither-pil-getbbox-nor-numpy-are-working
        tmp = image.crop(image.convert('RGBa').getbbox())
        ori_width, ori_height = tmp.size
        if ori_width < size[0] and ori_height < size[1]:
            new_width = ori_width
            multiplier = 1
            while new_width < size[0]:
                multiplier = multiplier + 1
                new_width = ori_width * multiplier
            new_height = multiplier * ori_height
            image = tmp.resize((new_width, new_height), Image.ANTIALIAS)
        else:
            image = tmp
    except:
        app_logger.debug("Error trying to readjust original image before creating thumbnail")
    if pad:
        image.thumbnail(size, Image.ANTIALIAS)
        image_size = image.size
        offset_x = int(max((size[0] - image_size[0]) / 2, 0))
        offset_y = int(max((size[1] - image_size[1]) / 2, 0))
        offset = (offset_x, offset_y)
        # create the image object to be the final product
        thumb = Image.new(mode='RGBA', size=size, color=(255, 255, 255, 0))
        # paste the thumbnail into the full sized image
        thumb.paste(image, offset)
    else:
        image.thumbnail(size, Image.ANTIALIAS)
        thumb = image
    # thumb_io should be closed when the usage ends
    thumb_io = StringIO.BytesIO()
    try:
        thumb.save(thumb_io, format='PNG')
    except OSError:
        thumb.save(thumb_io, format='JPEG')
    files = [image]
    if pad:
        files.append(thumb)
    return thumb_io, files


def get_date_time_object(date, time):
    """ Given a date and time, returns a DateTimeField object """
    date = date.split("/")
    time = time.split(":")
    hour = int(time[0])
    if (time[1].endswith("pm") or time[1].endswith("PM")) and hour < 12:
        hour += 12
    elif (time[1].endswith("am") or time[1].endswith("AM")) and hour == 12:
        hour = 0
    minutes = int(time[1][0:2])
    return timezone.datetime(int(date[2]),
                             int(date[0]),
                             int(date[1]),
                             hour, minutes, 0)


def year_month_to_date(year, month):
    """Gets year and month and return
    python date object.
    """
    if year is None or month is None:
        return None
    year = int(year)
    month = int(month)
    if year == 0 or month == 0:
        return None
    return date(year, month, 1)


def date_to_year_month(date):
    """Gets python date object and return
    year and month.
    """
    if date is None:
        return None
    return date.year, date.month


def get_media_url():
    """Get a media url for django app.
    :return: correct media url.
    """
    media = settings.MEDIA_URL
    return media


def get_static_url():
    """Get a static url for django app.
    :return: correct static url.
    """
    static = settings.STATIC_URL
    return static


def get_formatted_timestamp(given_datetime):
    """
    Given a datetime object, format it in a form like 'Jan. 01, 2000, 12:00 p.m.'
    and then return it.
    """
    formatted_datetime = timezone.localtime(given_datetime).strftime("%b. %d, %Y, %-I:%M %p")
    if formatted_datetime[len(formatted_datetime) - 2:] == "AM":
        return formatted_datetime[:len(formatted_datetime) - 2] + "a.m."
    else:
        return formatted_datetime[:len(formatted_datetime) - 2] + "p.m."


def get_roles_list():
    """
    Returns a list of all roles
    """
    return ["business development - general", "business development executive",
            "business development manager", "business development representative",
            "channel partner sales executive", "leadership coach", "pitch coach",
            "account manager", "technical account manager",
            "database administrator", "data architect", "data scientist",
            "ui designer", "ui/ux designer", "visual designer", "graphic designer",
            "product designer", "industrial designer", "animator",
            "mechanical engineer", "civil engineer", "electrical engineer",
            "ceo", "cto", "coo", "cfo",
            "finance - general", "controller", "accountant", "procurement",
            "hardware engineer",
            "angel investor", "venture capitalist", "grant writer",
            "counsel", "attorney",
            "marketing - general", "digital marketer", "branding",
            "search engine optimization", "growth hacker", "content marketer",
            "product marketer", "copywriter", "technical writer",
            "operations - general", "human resources", "human resources administrator",
            "office manager", "supply chain", "buyer and planner",
            "product manager", "technical product manager", "project manager",
            "researcher", "research engineer", "research scientist",
            "sales - general", "sales executive", "sales manager", "sales engineer",
            "inside sales manager", "sales development representative",
            "enterprise sales representative",
            "scientist", "material scientist", "biologist", "chemist",
            "security engineer",
            "software engineer", "full stack developer", "backend developer",
            "frontend developer", "mobile developer", "qa engineer",
            "network engineer", "release engineer", "web developer",
            "machine learning engineer",
            "strategy & operations",
            "support engineer",
            "systems administrator", "systems engineer", "business systems",
            "ux designer", "interaction designer"]


"""
Django ChoiceField with "other" choices:
Credit: https://djangosnippets.org/snippets/863/
"""


# class ChoiceWithOtherRenderer(forms.RadioSelect.renderer):
#     """RadioFieldRenderer that renders its last choice with a placeholder."""
#     def __init__(self, *args, **kwargs):
#         super(ChoiceWithOtherRenderer, self).__init__(*args, **kwargs)
#         self.choices, self.other = self.choices[:-1], self.choices[-1]
#
#     def __iter__(self):
#         for html_input in super(ChoiceWithOtherRenderer, self).__iter__():
#             yield html_input
#         if 'id' in self.attrs:
#             input_id = '%s_%s' % (self.attrs['id'], self.other[0])
#         else:
#             input_id = ''
#         if input_id:
#             label_for = ' for="%s"' % input_id
#         else:
#             label_for = ''
#         if not force_unicode(self.other[0]) == self.value:
#             checked = ''
#         else:
#             checked = 'checked="true" '
#
#         yield '<label%s><input type="radio" id="%s" value="%s" name="%s" %s/> \
#                 %s</label> %%s' % (
#             label_for, input_id, self.other[0],
#             self.name, checked, self.other[1])
#
#
# class ChoiceWithOtherWidget(forms.MultiWidget):
#     """MultiWidget for use with ChoiceWithOtherField."""
#     def __init__(self, choices):
#         widgets = [
#             forms.RadioSelect(choices=choices,
#                               renderer=ChoiceWithOtherRenderer),
#             forms.TextInput
#         ]
#         super(ChoiceWithOtherWidget, self).__init__(widgets)
#
#     def decompress(self, value):
#         if not value:
#             return [None, None]
#         return value
#
#     def format_output(self, rendered_widgets):
#         """Format the output by substituting the "other"
#         choice into the first widget.
#         """
#         return rendered_widgets[0] % rendered_widgets[1]
#
#
# class ChoiceWithOtherField(forms.MultiValueField):
#     def __init__(self, *args, **kwargs):
#         fields = [
#             forms.ChoiceField(
#                 widget=forms.RadioSelect(renderer=ChoiceWithOtherRenderer),
#                 *args,
#                 **kwargs
#             ),
#             forms.CharField(required=False)
#         ]
#         widget = ChoiceWithOtherWidget(choices=kwargs['choices'])
#         kwargs.pop('choices')
#         self._was_required = kwargs.pop('required', True)
#         kwargs['required'] = False
#         super(ChoiceWithOtherField, self).__init__(widget=widget,
#                                                    fields=fields,
#                                                    *args, **kwargs)
#
#     def compress(self, value):
#         if self._was_required and not value or value[0] in (None, ''):
#             raise forms.ValidationError(self.error_messages['required'])
#         if not value:
#             return [None, u'']
#         if force_unicode(value[0]) == \
#                 force_unicode(self.fields[0].choices[-1][0]):
#             return value[0], value[1]
#         else:
#             return u''


def time_minute_diff(t1, t2):
    """
    given the time object, return t2 - t1 in minutes
    :param t1:
    :param t2:
    :return:
    """
    if not isinstance(t1, time) or not isinstance(t2, time):
        raise ValueError("expect datetime.time object")
    t1_h = t1.hour
    t1_m = t1.minute
    t2_h = t2.hour
    t2_m = t2.minute
    return (t2_h * 60 + t2_m) - (t1_h * 60 + t1_m)


def time_minute_diff_datetimes(t1, t2):
    if not isinstance(t1, datetime) or not isinstance(t2, datetime):
        raise ValueError("expect datetime.datetime object")
    diff = t2 - t1
    minutes = diff.seconds / 60
    return minutes


def diggy_paginator(paginator, cur_page, adjacent_pages=2, arg_max_page=None):
    """

    :param paginator: a Pagniator object
    :param cur_page: current page. must be an integer
    :param adjacent_pages: number of pages adjancent to the current page.
    ex) if we want to display 5 pages each, 2 adjacent pages: 2 + 1 + 2 = 5.
    :return:
    """
    if paginator is None and arg_max_page is not None:
        max_page = arg_max_page
    else:
        max_page = paginator.num_pages
    end_page = cur_page + adjacent_pages
    start_page = max(cur_page - adjacent_pages, 1)
    if start_page < adjacent_pages:
        start_page = 1
        end_page = min(max_page, start_page + 2 * adjacent_pages)
    next_page = end_page + 1
    prev_page = None
    if start_page > 1:
        prev_page = start_page - 1
    if end_page > max_page:
        end_page = max_page
        next_page = None
    cur_pages = [i for i in range(start_page, end_page + 1)]
    if len(cur_pages) == 0:
        cur_pages = [1]
    if next_page and next_page > max_page:
        next_page = None
    return prev_page, cur_pages, next_page


def month_delta(given_date, delta):
    """
    Date arithmetic based on months.
    :param given_date:
    :param delta:
    :return:
    """
    m = (given_date.month + delta) % 12
    y = given_date.year + (given_date.month + delta - 1) // 12
    if not m:
        m = 12
    d = min(given_date.day, calendar.monthrange(y, m)[1])
    return given_date.replace(day=d, month=m, year=y)


def check_weekly_date(weekly_date, start_date, end_date):
    """ Checks wether a given date (weekly_date) will eventually
    fall betweeen a given date range (between start_date and end_date)
    """

    if weekly_date > end_date:
        return False

    weeks = ((start_date - weekly_date) / 7).days
    if start_date != end_date:
        weeks += 1

    # check_date is the first weekly date after the start_date
    check_date = weekly_date + timezone.timedelta(days=(weeks * 7))
    return start_date <= check_date <= end_date


def get_monday_from_page(page, start_next_week):
    """ Given a page number, return the monday corresponding to that week.
    Page 1 is this monday. Page 2 is next week's monday.

    If mentor scheduling "starts next week", per admin settings, then
    offset forward by one week.
    Page 1 is next monday. Page 2 is the monday after.
    """

    today = datetime.today().date()
    this_monday = today - timezone.timedelta(today.weekday())

    monday = this_monday + timezone.timedelta(7 * (page - 1))

    if start_next_week:
        monday += timezone.timedelta(7)

    return monday


def number_to_one_decimal(number):
    """
    A utility function to change number to one decimal place.
    For now, it only allow=s whole numbers.
    :param number: whole number
    :return:
    """
    total_ban = "$0"
    if number is None:
        return total_ban
    if isinstance(number, int) or isinstance(number, float):
        if number <= 0:
            return total_ban
    else:  # number is of form '$5.55B'
        number = dollar_string_to_number(number)
    number_str = str(number)
    number_list = number_str.split('.')
    if len(number_list) <= 1 or float(number_list[1]) == 0:
        return number_to_dollar_string(number_list[0], True)
    decimals = round(float("0.{0}".format(number_list[1])), 1)
    total_ban = int(number_list[0]) + decimals
    return number_to_dollar_string(total_ban, True)


ALLOWED_TAGS = ALLOWED_TAGS + ['p', 'strike', 'sub', 'u', 'sup', 'br', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'table',
                               'tr', 'td', 'tbody', 'hr', 'font', 'span', 'iframe', 'small', 'o:p', 'div', 'img']
ALLOWED_ATTRIBUTES['table'] = ['class', 'style', 'border']
ALLOWED_ATTRIBUTES['p'] = ['style']
ALLOWED_ATTRIBUTES['span'] = ['style']
ALLOWED_ATTRIBUTES['font'] = ['face']
ALLOWED_ATTRIBUTES['o:p'] = ['style']
ALLOWED_ATTRIBUTES['iframe'] = ['src', 'width', 'height', 'frameborder', 'allowfullscreen']
ALLOWED_ATTRIBUTES['div'] = ['style']
ALLOWED_ATTRIBUTES['img'] = ['style', 'src', 'class', 'width', 'height', 'alt']
ALLOWED_ATTRIBUTES['a'] = ['href', 'title', 'rel', 'target']
ALLOWED_ATTRIBUTES['td'] = ['style']
ALLOWED_ATTRIBUTES['ol'] = ['style']
ALLOWED_ATTRIBUTES['ul'] = ['style']
ALLOWED_STYLES = ['margin-left', 'text-align', 'line-height', 'font-size', 'color', 'background-color',
                  'height', 'width', 'float', 'font-family',
                  'border-style', 'border', 'border-color', 'border-collapse',
                  'list-style-type', 'padding-left', 'text-decoration'
                  ]


def bleach_clean(input_text):
    if input_text is None:
        return ''
    cleaned = bleach.clean(input_text, tags=ALLOWED_TAGS, attributes=ALLOWED_ATTRIBUTES, styles=ALLOWED_STYLES)
    # check iframe src
    soup = bs4.BeautifulSoup(cleaned, "html.parser")
    for iframe in soup.find_all('iframe'):
        if iframe and iframe['src']:
            src = iframe['src']
            if not (src.startswith('//www.youtube.com/embed') or src.startswith('https://instagram.com/p/')
                    or src.startswith('//player.vimeo.com/video/') or src.startswith(
                        '//www.dailymotion.com/embed/video/')
                    or src.startswith('//player.youku.com/embed/') or src.startswith(
                        'http://v.qq.com/iframe/player.html?vid=') or src.startswith('https://www.youtube.com')
                    or src.startswith('https://youtube.be') or src.startswith('https://player.vimeo.com/video/')):
                app_logger.info(iframe['src'])
                iframe.decompose()
    for link in soup.find_all('a'):
        if link:
            link['rel'] = 'noopener nofollow noreferrer'
    return str(soup)  # cleaned


def bleach_tinymce(input_text, with_media=False):
    tinymce_tags = ALLOWED_TAGS
    tinymce_attributes = ALLOWED_ATTRIBUTES
    tinymce_protocols = ['http', 'https']

    if with_media:
        tinymce_tags += ['video', 'audio', 'source']
        tinymce_attributes['video'] = ['controls', 'width', 'height',
                                       'allowfullscreen', 'preload',
                                       'poster']
        tinymce_attributes['audio'] = ['controls', 'preload']
        tinymce_attributes['source'] = ['src']

    return bleach.clean(
        input_text,
        tags=tinymce_tags,
        attributes=tinymce_attributes,
        styles=ALLOWED_STYLES,
        protocols=tinymce_protocols,
    )


def remove_html_tags(text):
    """Remove html tags from a string

    Acknowledgements: https://medium.com/@jorlugaqui/how-to-strip-html-tags-from-a-string-in-python-7cb81a2bbf44
    """
    import re
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


def pdf_extension_validate(value):
    return FileExtensionValidator(allowed_extensions=['pdf'])(value)


def file_whitelist_validator(value):
    whitelist = get_available_image_extensions() + ['pdf', 'xlsx', 'csv', 'xls', 'ppt', 'pptx', 'mp4',
                                                    'doc', 'docx', 'txt', 'zip', 'md', 'tar.gz', 'tar.bz2', '7z',
                                                    'avi', 'mov', 'wmv', 'flv', 'avi', 'webm', 'mkv']
    return FileExtensionValidator(allowed_extensions=whitelist)(value)


def resume_whitelist(value):
    return FileExtensionValidator(allowed_extensions=['pdf', 'txt', 'doc', 'docx'])(value)


T = TypeVar("T")


class QuerySetType(Generic[T], QuerySet):
    """ Inspired by: https://github.com/Vieolo/django-hint/blob/master/django_hint/typehint.py#L167 """

    def __iter__(self) -> Iterator[T]:
        pass


def encode_oauth_state(site_id):
    return "{0}".format(float(site_id) + random.random())


def decode_oauth_state(state):
    return int(float(state))


def test_response(url, host, arguments=None, is_post=False):
    """ Creates an http request for testing purposes.
    Returns the resulting http response

    host: eg uni.get_host() (where uni is a University object)
    """
    client = Client(HTTP_HOST=host)
    client.session['django_timezone'] = 'US/Eastern'
    if is_post:
        return client.post(url, data=arguments)
    return client.get(url, data=arguments)


def filtered_filename(string):
    return "".join(x if x.isalnum() else '_' for x in string)


class PrintDebugVar:
    """"
    This utility makes it more convenient to print variables for debugging purposes only.

    For most use cases, all uses of this class should be deleted after use.

    Instructions:

        Instantiate the printer (p) before first use, passing in locals() and an optional print function (eg app_logger.debug).
            If omitting print function, built-in print will be used.
            globals() can optionally be passed too. (If you are getting an 'undefined" error, it is likely because you need to pass globals).

        Call p.v('var') each time you want to print a variable whose variable name is 'var'.
            eg:
                Instead of:     print(f'var = {var}')
                Do:             p.f('var')

        In fact, any expression that's valid within the caller's local scope can be printed.
           eg:
                Instead of:     print(f'x + y = {x + y}')
                                print(f'obj.get_val() = {obj.get_val()}')

                Do:             p.f('x + y')
                                p.f('obj.get_val()')

        Call p.update(locals()) each time the contents of the variable has changed since the last time locals() was passed
            to the constructor or update function.

    Example:

        This code:

            variable_1 = 10
            variable_2 = 20
            variable_3 = 30

            app_logger.debug(f'variable_1 = {variable_1}')
            app_logger.debug(f'variable_2 = {variable_2}')
            app_logger.debug(f'variable_3 = {variable_3}')
            app_logger.debug(f'variable_1 + variable_2 + variable_3 = {variable_1 + variable_2 + variable_3}')

            variable_1 = 100
            variable_2 = 200
            variable_3 = 300

            app_logger.debug(f'variable_1 = {variable_1}')
            app_logger.debug(f'variable_2 = {variable_2}')
            app_logger.debug(f'variable_3 = {variable_3}')
            app_logger.debug(f'variable_1 + variable_2 + variable_3 = {variable_1 + variable_2 + variable_3}')


        Which outputs:
            [DEBUG][...] variable_1 = 10
            [DEBUG][...] variable_2 = 20
            [DEBUG][...] variable_3 = 30
            [DEBUG][...] variable_1 + variable_2 + variable_3 = 60
            [DEBUG][...] variable_1 = 100
            [DEBUG][...] variable_2 = 200
            [DEBUG][...] variable_3 = 300
            [DEBUG][...] variable_1 + variable_2 + variable_3 = 600


        Can be shortened to:

            variable_1 = 10
            variable_2 = 20
            variable_3 = 30

            p = PrintDebugVar(locals(), print_function=app_logger.debug)         # Instantiate before first use
            p.f('variable_1')
            p.f('variable_2')
            p.f('variable_3')
            p.f('variable_1 + variable_2 + variable_3')

            variable_1 = 100
            variable_2 = 200
            variable_3 = 300

            p.update(locals())        # Don't forget to update locals when variable contents change!
            p.f('variable_1')
            p.f('variable_2')
            p.f('variable_3')
            p.f('variable_1 + variable_2 + variable_3')

    """

    def __init__(self, locals_, globals_=None, print_function=print):
        self.locals = locals_
        self.globals = globals_ if globals_ else dict()
        self.print_function = print_function
        self.f = self.print_var  # function name alias

    def print_var(self, var):
        new_locals = self.locals.update(locals())
        command = f"self.print_function(f'{var} = {{{var}}}')"  # self.print_function(f'x = {x}')
        exec(command, self.globals, self.locals)

    def update(self, locals_):
        self.locals = locals_
