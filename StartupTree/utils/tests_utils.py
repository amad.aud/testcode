from django.test import TestCase
from StartupTree.utils import (
    better_title,
    to_unicode,
    unicode_safe,
    dollar_string_to_number,
    number_to_dollar_string,
    number_to_one_decimal
)
import six


class StartupTreeUtilTest(TestCase):

    def test_to_unicode(self):
        string = 'string'
        self.assertEqual(isinstance(to_unicode(string), six.string_types), True)
        ascii = u'\xa0'
        self.assertEqual(isinstance(to_unicode(ascii), six.string_types), True)
        not_string = 0
        self.assertEqual(isinstance(to_unicode(not_string), int), True)

    def test_unicode_safe_(self):
        string = 'string'
        self.assertEqual(isinstance(unicode_safe(string), six.string_types), True)
        not_string = 0
        self.assertEqual(isinstance(to_unicode(not_string), int), True)
        ascii = u'\xa0'
        result = unicode_safe(ascii)
        self.assertEqual(isinstance(result, str), True)

    def test_better_title(self):
        school = 'school of industrial and relation'
        self.assertEqual(better_title(school),
                         'School of Industrial and Relation')

    def test_dollar_string_to_number(self):
        dollar_string = '$7'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 7)

        dollar_string = '$0'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 0)

        dollar_string = '$2000'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 2000)

        dollar_string = '$232K'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 232000)

        dollar_string = '$42k'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 42000)

        dollar_string = '$223M'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 223000000)

        dollar_string = '$432m'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 432000000)

        dollar_string = '$23B'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 23000000000)

        dollar_string = '$21b'
        result = dollar_string_to_number(dollar_string)
        self.assertEqual(result, 21000000000)

        with self.assertRaises(ValueError,
                               msg="An argument must be a none empty string."):
            dollar_string = ''
            dollar_string_to_number(dollar_string)
            dollar_string = 1000
            dollar_string_to_number(dollar_string)

        dollar_string = '345k'
        with self.assertRaises(ValueError,
                               msg="An argument must start with $."):
            dollar_string_to_number(dollar_string)

        dollar_string = '$'
        with self.assertRaises(
                ValueError,
                msg="Dollar String must contain value after $."):
            dollar_string_to_number(dollar_string)

        dollar_string = '$421A'
        with self.assertRaises(
                ValueError,
                msg="An argument must end with number, b, m or k."):
            dollar_string_to_number(dollar_string)

        dollar_string = '$-140'
        with self.assertRaises(ValueError,
                               msg="Dollar amount must be positive."):
            dollar_string_to_number(dollar_string)

    def test_number_to_dollar_string(self):
        number = 7
        result = number_to_dollar_string(number)
        self.assertEqual(result, '$7')

        number = 7765
        result = number_to_dollar_string(number)
        self.assertEqual(result, '$7.77K')

        number = 132000
        result = number_to_dollar_string(number)
        self.assertEqual(result, '$132K')

        number = 43000000
        result = number_to_dollar_string(number)
        self.assertEqual(result, '$43M')

        number = 4200000000
        result = number_to_dollar_string(number)
        self.assertEqual(result, '$4.2B')

        number = 400000000
        result = number_to_dollar_string(number)
        self.assertEqual(result, '$400M')

        with self.assertRaises(ValueError,
                               msg="An argument must be an integer."):
            number = '$140K'
            number_to_dollar_string(number)

        with self.assertRaises(ValueError,
                               msg="An argument must be a positive integer."):
            number = -3123
            number_to_dollar_string(number)

    def test_number_to_one_decimal(self):
        n = None
        expected = "$0"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = 0
        expected = "$0"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = 123
        expected = "$123"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = 123.456
        expected = "$123.5"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = 0.5
        expected = "$0.5"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = 1.23
        expected = "$1.2"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = 5.
        expected = "$5"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = '$5.55B'
        expected = "$5.5B"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = "$451"
        expected = "$451"
        self.assertEqual(expected, number_to_one_decimal(n))
        n = "$451.4"
        expected = "$451.4"
        self.assertEqual(expected, number_to_one_decimal(n))
