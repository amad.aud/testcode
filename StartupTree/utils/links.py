import pytz


def datetime_to_utc_str(datetime):
    utc = pytz.timezone('UTC')
    return datetime.astimezone(utc).strftime('%Y%m%dT%H%M%SZ')


def gen_google_calendar(title, start_datetime, end_datetime, details, guests=(), location=None, link=None):
    """
    https://stackoverflow.com/questions/10488831/link-to-add-to-google-calendar
    https://github.com/InteractionDesignFoundation/add-event-to-calendar-docs/blob/master/services/google.md


    datetime format in UTC: YYYYMMDDTHHmmSSZ
        YYYYMMDDTHHmmSSZ/YYYYMMDDTHHmmSSZ
        example: dates=20201231T193000Z/20201231T223000Z
    :return:
    """
    guest_list = ','.join(guests)

    start = datetime_to_utc_str(start_datetime)
    end = datetime_to_utc_str(end_datetime)

    gcal = "http://www.google.com/calendar/render?" \
           "action=TEMPLATE" \
           "&text={title}" \
           "&dates={start}/{end}" \
           "&details={details}".format(
        title=title,
        start=start,
        end=end,
        details=details
    )
    if guest_list:
        gcal += "&add={guest_list}".format(guest_list=guest_list) 
    if location:
        gcal += "&location={location}".format(location=location)
    if link:
        gcal += "&sprop=website:{link}".format(link=link)
    return gcal


def gen_yahoo_calendar(title, start_datetime, end_datetime, details, location=None):
    """
    Guest seems to be not supported for yahoo cal.
    https://github.com/InteractionDesignFoundation/add-event-to-calendar-docs/blob/master/services/yahoo.md

    datetime format: datetime (ISO8601)
        20201231T193000Z (in UTC)
    for end time, duration is more recommended if duration is less than 100 hours
    :return:
    """
    duration = (end_datetime - start_datetime).seconds
    duration_hour = int(duration / 3600)
    duration_minutes = int(duration / 60)
    start = datetime_to_utc_str(start_datetime)
    end = datetime_to_utc_str(end_datetime)

    yahoo = "https://calendar.yahoo.com/?" \
            "v=60" \
            "&title={title}" \
            "&desc={details}" \
            "&st={start}".format(
        title=title,
        start=start,
        details=details
    )
    if duration_hour >= 100:
        yahoo += "&et={end}".format(end=end)
    else:
        duration_hour = str(duration_hour).zfill(2)
        duration_minutes = str(duration_minutes).zfill(2)
        yahoo += "&dur={hours}{minutes}".format(hours=duration_hour, minutes=duration_minutes)

    if location:
        yahoo += "&in_loc={location}".format(location=location)
    return yahoo