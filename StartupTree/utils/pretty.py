from django.db.models import Model, QuerySet
from forms.models import Rubric
from trunk.utils import format_datetime
from datetime import datetime
from typing import Union


class PrettyPrinter:
    default_attributes = {
        # activity.models
        'Activity': [
            'user',
            'activity',
            'activity_type',
            'site',
            # 'obj_event',
        ],

        # branch.models
        'Label': [
            'id',
            'title',
            'visible_to_users',
            'users_can_tag',
            'startuptreestaff_assignable_only',
        ],

        # emailuser.models
        'UserEmail': [
            'email',
            'cronofy_access_token',
            'cronofy_refresh_token',
            'cronofy_token_expiration',
            'cronofy_account_id',
        ],

        # form.models
        'Question': [
            'q_type',
            'get_question_type_str()',
            'number',
            'text',
        ],
        'Answer': [
            'startup',
            'member',
            'q.id',
            'q.text',
            'q.get_question_type_str()',
            'text',
            'choices.all()',
            'date',
            'uploaded_file',
        ],
        'Rubric': [
            'get_rubric_type()',
            'text',
            'round',
        ],
        'RubricAnswer': [
            'judge.get_name()',
            'applicant',
            'anonymous_applicant',
            'r.text',
            'text',
            'choices',
            'score',
        ],

        # mentorship.models
        'Mentor': [
            'id',
            'profile.get_full_name()',
            'profile.user.email',
            'platform',
            'is_active',
        ],
        'MentorActiveWindow': [
            'mentor',
            'timestamp',
            'active_end',
        ],
        'MentorshipSession': [
            'is_team_session()',
            'mentorship.mentor',
            'mentorship.student',
            'mentorship.id',
            'datetime',
            'is_mentor_accepted',
            'is_admin_accepted',
            'is_cancelled',
        ],
        'MentorshipSessionFeedback': [
            'session',
            'from_member',
            'to_member',
            'created_on',
            'rating',
            'feedback',
            'is_from_mentor',
            'is_team()',
            'team_session',
        ],
        'TeamMentorshipSession': [
            'is_team_session()',
            'teammentorshipsession.team',
            'teammentorshipsession.get_mentors()',
            'teammentorshipsession.members_attending.all()',
            'datetime',
            'is_mentor_accepted',
            'is_admin_accepted',
            'is_cancelled',
        ],

        # notification.models
        'NotificationBoard': [
            'user',
            'primary_email',
            'receive_feature_update',
            'receive_inbox_notification',
            'receive_admin_notifications',
            'receive_mentorship_notifications'
        ],

        # trunk.models
        'AdminCalSyncOption': [
            'sync_type',
        ],
        'AdminEmailNotiOption': [
            'option_type',
        ],
        'Event': [
            'name',
            'creator',
            'status',
            'start',
            'end',
            'university',
            'is_template_only',
            'checked_in.all()',
        ],
        'UniversityExportHistory': [
            'category',
            'public_category_str()',
            'extra_info',
        ],
        'UniversityStaff': [
            'admin.email',
            'admin.get_full_name()'
        ],

        # venture.models
        'MemberToAdmin': [
            'member',
            'startup',
        ]
    }

    def __init__(self, attrs=None, tab_level=0, print_function=print, make_header=None, empty_end_line=True):
        """
        attrs is a string list of attributes to print. If none, default fields based on model will be used.
        make_header is a function that takes an object and returns a string to serve as header

        empty_end_line is whether to separate each entry with an empty line.
        """
        self.attrs = attrs
        self.tab_level = tab_level
        self.print_function = print_function
        self.make_header = make_header if make_header else PrettyPrinter.__default_make_header
        self.empty_end_line = empty_end_line
        self.max_label_width = PrettyPrinter.__get_max_label_width(self.attrs) if self.attrs else None

    @staticmethod
    def __default_make_header(obj: Model):
        return f'{str(obj)}:'

    @staticmethod
    def __get_max_label_width(attrs):
        return len(max(attrs, key=len))

    def printer(self, obj_or_qset: Union[Model, QuerySet]):
        if isinstance(obj_or_qset, Model):
            self.printer_obj(obj_or_qset)
        elif isinstance(obj_or_qset, QuerySet):
            for obj in obj_or_qset:
                self.printer_obj(obj)

    def printer_obj(self, obj: Model):
        if self.attrs:
            attrs = self.attrs
            max_label_width = self.max_label_width
        else:
            try:
                attrs = PrettyPrinter.default_attributes[obj.__class__.__name__]
            except KeyError:
                attrs = ['id']
            max_label_width = PrettyPrinter.__get_max_label_width(attrs)

        indent = '\t' * self.tab_level

        self.print_function(indent + self.make_header(obj))

        def print_attr_cmd(attr):
            """
            Returns cmd, make_error

            cmd:
                the string of a print command to run.

            make_error:
                a function that takes an exception and returns the string of a print command to run

            If running cmd returns an exception, that exception be passed to make_error for printing.
            """
            padding = PrettyPrinter.get_padding(max_label_width, attr)
            cmd = f"self.print_function(indent + '\t{attr}{padding} = ' + PrettyPrinter.to_string(obj.{attr}))"

            def make_err(exc: Exception):
                exc_class = exc.__class__.__name__
                exc_msg = str(exc).replace('\'', '').replace('\"', '')
                return f"self.print_function(indent + '\t{attr}{padding} = {exc_class}: {exc_msg}')"

            return cmd, make_err

        for attr in attrs:
            command, make_error = print_attr_cmd(attr)
            try:
                exec(command)
            except Exception as e:
                exec(make_error(e))

        if self.empty_end_line:
            self.print_function('')

    @staticmethod
    def get_padding(max_label_width, attr):
        """ Gets padding needed to make proper alignment. """
        width = max(0, max_label_width - len(attr))
        return ' ' * width

    @staticmethod
    def to_string(obj):
        """ returns string representation of input."""
        if isinstance(obj, datetime):
            return format_datetime(obj)
        return str(obj)


printer = PrettyPrinter().printer  # PrettyPrinter initialized with default settings
