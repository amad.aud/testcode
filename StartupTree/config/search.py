"""
List all ES document type names here.
Names should also be the name of the Index.
All document should have a field called "doc_type", which has one of the values here.
Names should be the lowercase.
"""
STARTUP_DOCUMENT = 'startup'
STARTUP_MEMBER_DOCUMENT = 'member'
SKILL_DOCUMENT = 'skill'
ORGANIZATION_DOCUMENT = 'organization'
EVENT_DOCUMENT = 'event'
MENTOR_DOCUMENT = 'mentor'
EMPLOYEE_TITLE_DOCUMENT = 'employee_title'
CITY_DOCUMENT = 'city'
GROUP_DOCUMENT = 'group'
ROLE_DOCUMENT = 'role'
TAG_DOCUMENT = 'tag'
MAJOR_DOCUMENT = 'major'
INBOX_DOCUMENT = 'inbox'
FORUM_POST_DOCUMENT = 'forum'