"""
Django settings for StartupTree project.
"""

from os import path, environ
import sys


PROJECT_ROOT = path.dirname(path.abspath(path.dirname(__file__)))

IS_DEV = True if environ.get('DJ_DEBUG') else False
IS_STAGING = True if environ.get('DJ_STAGING') else False
IS_PRODUCTION = True if not IS_DEV else False
DEBUG = True if IS_DEV or IS_STAGING else False
IS_TEST = True if environ.get('AUTO_TEST') else False

if IS_DEV:
    ALLOWED_HOSTS = (
        '.startuptreetest.co',
        '.startuptreetest2.co',
        '.startuptree.co',
    )

else:
    ALLOWED_HOSTS = [
        '.startuptree.co',
    ]
    import requests
    try:
        internal_ip = requests.get('http://instance-data/latest/meta-data/local-ipv4').text
    except requests.exceptions.ConnectionError:
        pass
    else:
        ALLOWED_HOSTS.append(internal_ip)
    del requests

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': environ['PSQL_DB_NAME'],
        'USER': environ['PSQL_DB_USER'],
        'PASSWORD': environ['PSQL_DB_PASSWORD'],
        'HOST': environ['PSQL_DB_HOST'],
        'PORT': environ['PSQL_DB_PORT'],
        'CONN_MAX_AGE': None,
    },
}

if IS_DEV:
    DATABASES['default']['TEST'] = {
        'NAME': environ['PSQL_DB_NAME']
    }

"""
'slave1': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'startuptree_slave1',
    'USER': 'postgres',
    'PASSWORD': '1234',
    'HOST': 'localhost',
    'PORT': '5432',
},
DATABASE_ROUTERS = ['Startuptree.routers.MasterSlaverouter']
"""

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = path.join(PROJECT_ROOT, "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
MEDIA_URL = '/media/'
MEDIA_DIRECTORY = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# STATIC_ROOT = path.join(PROJECT_ROOT, 'static')
STATIC_ROOT = ''
if IS_PRODUCTION or IS_STAGING:
    STATICFILES_DIRS = [path.join(PROJECT_ROOT, 'static')]

# URL prefix for static files.
STATIC_URL = '/static/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n(bd1f1c%e8=_xad02x5qtfn%wgwpi492e$8_erx+d)!tpeoim'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
            path.join(PROJECT_ROOT, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'branch.templatetags.startuptree_landing_url',
                'trunk.context_processor.platform_related',
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.request",
                "branch.templatetags.media",
                "branch.templatetags.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

MIDDLEWARE = [
    # 'appenlight_client.django_middleware.AppenlightMiddleware',
    # 'site.middleware.CookieDomainMiddleware',
    'reversion.middleware.RevisionMiddleware',
    'multisite.middleware.DynamicSiteMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # Obsolete by CSP
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
    'app.middleware.CrossPlatformMiddleWare',
    'app.middleware.TimezoneMiddleware',
    'app.middleware.RateLimitMiddleware',
    'data_collection.middleware.RequestInfoMiddleware',
    'csp.middleware.CSPMiddleware',
]

INTERNAL_IPS = ['127.0.0.1']

ROOT_URLCONF = 'StartupTree.urls'

INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.messages',
]

if IS_PRODUCTION or IS_STAGING:
    # caching manage.py collectstatic histories.
    INSTALLED_APPS += [
        'collectfast',
    ]

if IS_PRODUCTION:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration
    sentry_sdk.init(
        dsn="https://de399b94003d4598aa0cff09ba99e10d@o177058.ingest.sentry.io/1262130",
        integrations=[DjangoIntegration()],

        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True
    )

if IS_STAGING:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration
    sentry_sdk.init(
        dsn="https://5c7f212354b943adbcaa4759c256bde7@o177058.ingest.sentry.io/1283347",
        integrations=[DjangoIntegration()],

        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True
    )

if IS_STAGING:
    CSRF_COOKIE_NAME = 'stagingcsrftoken'
    SESSION_COOKIE_NAME = 'stagingsessionid'

INSTALLED_APPS += [
    'django.contrib.staticfiles',
    # 'django.contrib.humanize',
    'corsheaders',
    'storages',
    'multisite',
    'reversion',
    'rest_framework',
    'rest_framework.authtoken',
    'app',
    'api',
    'notifications',
    'hamster',
    'activity',
    'trunk',
    'branch',
    'emailuser',
    'inbox',
    'mentorship',
    'recommendation',
    'videocall',
    'cities_light',
    'django_summernote',
    'localflavor',
    'data_collection',
    'django_user_agents',
    'venture',
    'discussion',
    'forms',
    'oauth2_provider',
    'django.contrib.postgres', #added to enable djang Q (query) object to use __search
    'django_tables2',
    'fcm_django',
    'django_celery_beat',
    'phonenumber_field',
    'django_elasticsearch_dsl',
    'tinymce',
    'channels',
    'djstripe',
    'boostbudget'
]

if IS_DEV:
    INSTALLED_APPS.append('dev_debug')
    FILE_UPLOAD_PERMISSIONS = 0o644

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '[%(levelname)s]%(asctime)s %(module)s %(funcName)s %(lineno)d %(message)s',
        },
        'simple': {
            'format': '[%(levelname)s][%(funcName)s(%(lineno)d)] %(message)s'
        },
        "rq_console": {
            "format": "%(asctime)s %(message)s",
            "datefmt": "%y %b %d, %H:%M:%S",
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'rq_console': {
            'level': 'DEBUG',
            "formatter": "simple",
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'StartupTree': {
            'handlers': ['console', 'null'],
            'level': 'DEBUG' if IS_DEV else 'WARNING',
            'propagate': True,
        },
        "celery": {
            "handlers": ['rq_console'],
            "level": "DEBUG"
        },
    }
}

REDIS_URL = environ['REDIS_HOST']
REDIS_PORT = environ['REDIS_PORT']
REDIS_BACKEND = 'redis_lock.django_cache.RedisCache'
REDIS_CACHE_URL = 'redis://{0}:{1}/'.format(REDIS_URL, REDIS_PORT)
CACHES = {
    'default': {
        'BACKEND': REDIS_BACKEND,
        'LOCATION': REDIS_CACHE_URL + '1',
        'OPTIONS': {

        },
        'VERSION': 2
    },
    # DB 2 is reserved.
    'multisite': {
        'BACKEND': REDIS_BACKEND,
        'LOCATION': REDIS_CACHE_URL + '3',
        'TIMEOUT': None,
        'VERSION': 2
    },
    'page_caches': {
        'BACKEND': REDIS_BACKEND,
        'LOCATION': REDIS_CACHE_URL + '4',
        'OPTIONS': {

        },
        'VERSION': 2
    },
    'attempt_caches': {
        'BACKEND': REDIS_BACKEND,
        'LOCATION': REDIS_CACHE_URL + '5',
        'OPTIONS': {

        },
        'VERSION': 2
    },
    'collectfast': {
        'BACKEND': REDIS_BACKEND,
        'LOCATION': REDIS_CACHE_URL + '6',
        'OPTIONS': {
        },
        'VERSION': 2
    }
    # DB 7 is reserved
}

# refer to config.py at hamster
CELERY_WORKER = {
    'connection': {
        'HOST': REDIS_URL,
        'PORT': REDIS_PORT,
        'DB': 2,
    },
}
# collectfast
COLLECTFAST_CACHE = 'collectfast'

# django-cities-light
# CITIES_LIGHT_CITY_SOURCES = ["http://download.geonames.org/export/dump/cities1000.zip"]
CITIES_LIGHT_TRANSLATION_LANGUAGES = ['en']

# Multi-Tenant options
from multisite import SiteID
SITE_ID = SiteID(default=1)

AUTHENTICATION_BACKENDS = ('emailuser.authenticate.EmailAuth',)
AUTH_USER_MODEL = 'emailuser.UserEmail'
LOGIN_URL = '/login'

# Number of seconds of inactivity before a user is marked offline
USER_ONLINE_TIMEOUT = 300

# Number of seconds that we will keep track of inactive users for before
# their last seen is removed from the cache
USER_LASTSEEN_TIMEOUT = 60 * 60 * 24 * 7

# Email settings
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'StartupTree <hello@startuptree.co>'
EMAIL_HOST = environ['EMAIL_HOST']
EMAIL_PORT = environ['EMAIL_PORT']
EMAIL_HOST_USER = environ['EMAIL_USER']
EMAIL_HOST_PASSWORD = environ['EMAIL_PASSWORD']
if not IS_STAGING and not IS_PRODUCTION:
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = 'tmp/email'

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

ROOT_DOMAIN = "startuptree.co"
if IS_DEV and not IS_TEST:
    ROOT_DOMAIN = "startuptreetest.co"

if IS_TEST:
    ROOT_DOMAIN = "startuptreetest.co"

if IS_STAGING:
    ROOT_DOMAIN = "startuptreetest2.co"

if not IS_PRODUCTION:
    MULTISITE_IS_LOCALHOST = True

MESSAGE_STORAGE = 'app.messages.SessionDedupStorage'

# Specify the default test runner.
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

if DEBUG:
    if IS_DEV:
        STATIC_ROOT = path.join(PROJECT_ROOT, "static")

    class DisableMigrations(object):

        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return None
    TEST_RUNNER = (
        'StartupTree.startuptree_test_runner'
        '.UseTestDatabaseTestSuiteRunner'
    )

    if IS_TEST:
        PASSWORD_HASHERS = (
            'django.contrib.auth.hashers.MD5PasswordHasher',
        )
        MIGRATION_MODULES = DisableMigrations()
        EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
        CELERY_TASK_ALWAYS_EAGER = True

    if not IS_TEST:
        INSTALLED_APPS += [
            'debug_toolbar'
        ]
        MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

        # HACK need debug toolbar for docker-compose
        def show_toolbar(request):
            # if request.is_ajax():
            #     return False
            return True

        '''DEBUG_TOOLBAR_PANELS = [
            'ddt_request_history.panels.request_history.RequestHistoryPanel',  # Here it is
            'debug_toolbar.panels.versions.VersionsPanel',
            'debug_toolbar.panels.timer.TimerPanel',
            'debug_toolbar.panels.settings.SettingsPanel',
            'debug_toolbar.panels.headers.HeadersPanel',
            'debug_toolbar.panels.request.RequestPanel',
            'debug_toolbar.panels.sql.SQLPanel',
            'debug_toolbar.panels.templates.TemplatesPanel',
            'debug_toolbar.panels.staticfiles.StaticFilesPanel',
            'debug_toolbar.panels.cache.CachePanel',
            'debug_toolbar.panels.signals.SignalsPanel',
            'debug_toolbar.panels.logging.LoggingPanel',
            'debug_toolbar.panels.redirects.RedirectsPanel',
            'debug_toolbar.panels.profiling.ProfilingPanel',
        ]'''
        DEBUG_TOOLBAR_CONFIG = {
            'SHOW_TOOLBAR_CALLBACK': 'StartupTree.settings.show_toolbar',
            "SHOW_COLLAPSED": True
        }

SESSION_COOKIE_DOMAIN = '.{0}'.format(ROOT_DOMAIN).split(':')[0]
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

CACHE_MULTISITE_ALIAS = 'multisite'
MULTISITE_FALLBACK = 'django.views.generic.base.RedirectView'
MULTISITE_FALLBACK_KWARGS = {'url': 'https://{0}/'.format(ROOT_DOMAIN),
                             'permanent': False}

# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'StartupTree.haystack_custom_backend.StartupTreeElasticSearchEngine',
#         'URL': environ['ELASTIC_SEARCH_HOST'],
#         'INDEX_NAME': 'haystack',
#     },
# }
#
# HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
from elasticsearch import Elasticsearch, RequestsHttpConnection
ELASTICSEARCH_DSL={
    'default': {
        'hosts': environ['ELASTIC_SEARCH_HOST']
    }
}
if IS_DEV or IS_STAGING:
    ELASTICSEARCH_DSL={
        'default': {
            'hosts': environ['ELASTIC_SEARCH_HOST'],
            'verify_certs': False,
            'connection_class': RequestsHttpConnection
        }
    }

if IS_PRODUCTION or IS_STAGING:
    '''Static file hosting using aws s3 (for deployment)
    '''
    # DEFAULT_FILE_STORAGE = 'storages.backends.s3.S3Storage'
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
    AWS_ACCESS_KEY_ID = environ['AWS_ACCESS_KEY_ID']
    AWS_SECRET_ACCESS_KEY = environ['AWS_SECRET_ACCESS_KEY']
    AWS_STORAGE_BUCKET_NAME = 'startuptree-static'
    if IS_STAGING:
        AWS_STORAGE_BUCKET_NAME = 'staging-startuptree'
    # AWS_STORAGE_BUCKET_MEDIA_NAME = 'startuptree-media'
    # AWS_S3_CUSTOM_DOMAIN = 'd2iyw9iwn4sv89.cloudfront.net'
    AWS_S3_CUSTOM_DOMAIN = 'static.startuptree.co'
    AWS_S3_CUSTOM_URL = 'https://static.startuptree.co/'
    if IS_STAGING:
        AWS_S3_CUSTOM_DOMAIN = 'staging-startuptree.s3.amazonaws.com'
        AWS_S3_CUSTOM_URL = 'https://staging-startuptree.s3.amazonaws.com/'
    # AWS_QUERYSTRING_AUTH = False
    AWS_S3_SECURE_URLS = True
    STATICFILES_STORAGE = 'StartupTree.s3_custom_storage.StaticStorage'
    AWS_PRELOAD_METADATA = True
    S3_URL = 'https://{0}.s3.amazonaws.com/'.format(AWS_STORAGE_BUCKET_NAME)
    # S3_URL = 'https://d2iyw9iwn4sv89.cloudfront.net/'
    # S3_MEDIA_URL = 'https://{0}.s3.amazonaws.com/'.format(AWS_STORAGE_BUCKET_MEDIA_NAME)
    AWS_STATIC_FOLDER = 'static'
    AWS_DEFAULT_ACL = None
    # STATIC_URL = "{0}{1}/".format(S3_URL, AWS_STATIC_FOLDER)
    STATIC_URL = "{0}{1}/".format(AWS_S3_CUSTOM_URL, AWS_STATIC_FOLDER)
    STATIC_ROOT = STATIC_URL
    MEDIA_URL = S3_URL
    # ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'
    from django.utils.http import http_date
    from time import time

    max_age = 31536000

    AWS_HEADERS = {
        'x-amz-acl': 'public-read',
        'Expires': http_date(time() + max_age),
        'Cache-Control': 'public, max-age=' + str(max_age)
    }

    AWS_S3_OBJECT_PARAMETERS = {
        'ACL': 'public-read',
        'Expires': http_date(time() + max_age),
        'CacheControl': 'public, max-age=' + str(max_age)
    }

LINKEDIN_CLIENT_ID = environ['LINKEDIN_ID']
LINKEDIN_CLIENT_SECRET = environ['LINKEDIN_SECRET']

GOOGLE_API_KEY = environ['GOOGLE_API_KEY']
GEOIP_PATH = 'geoip_data'
GEOIP_CITY = 'GeoLiteCity.dat'

DATA_COLLECTION_API_TOKEN = environ['DATA_COLLECTION_TOKEN']

# import REST API settings
# Not required for now, thus try catch
from api.settings import *
MIDDLEWARE += API_MIDDLEWARE
AUTHENTICATION_BACKENDS += API_AUTHENTICATION_BACKENDS

#must be here instead of api/settings.py
OAUTH2_PROVIDER = {
    'ACCESS_TOKEN_EXPIRE_SECONDS': 3600, # 1 hr of access time
    'OAUTH_SINGLE_ACCESS_TOKEN': True,
    'OAUTH_DELETE_EXPIRED': True,
}

# Facebook API
FACEBOOK_APP_ID = environ['FACEBOOK_ID']
FACEBOOK_APP_SECRET = environ['FACEBOOK_SECRET']

# Allow select schools to embed in iFrame
if IS_DEV or IS_STAGING:
    CSP_FRAME_ANCESTORS = ("file://*", "*.startuptreetest.co", "*.startuptree.co",
                           "*.startuptreetest2.co",
                           '*.wyominginnovates.org', 'https://staging-startuptree.s3.amazonaws.com')
    CSP_CONNECT_SRC = ("file://*", "'self'", "*.cronofy.com", "wss://*.startuptreetest.co:*",
                       # 'https://*.agora.io:*', 'https://*.agoraio.cn:*', 'wss://*.agora.io:*', 'wss://*.agoraio.cn:*' # videocall related
                       '*.sentry.io'
                       )
    CSP_SCRIPT_SRC = ("'unsafe-inline'", "'self'", "https:", "http:", "'unsafe-eval'")
    CSP_FONT_SRC = ("'unsafe-inline'", "'self'", "https:", "http:", 'data:')
    CSP_STYLE_SRC = ("'unsafe-inline'", "'self'", "https:", "http:")
    CSP_IMG_SRC = ("'unsafe-inline'", "'self'", "https:", "http:", "data:", "blob:")
    CSP_FRAME_SRC = ("'self'", "https:", "http:")
else:
    CSP_FRAME_ANCESTORS = ("*.asu.edu", "*.uwyo.edu", "*.startuptree.co",
                           '*.wyominginnovates.org', "*.uwyo-iie.org",
                           "uwyo-iie.org", "cel.ucf.edu", "cel.projectsbyryan.com",
                           "http://*.umt.edu", "https://*.umt.edu", "umt.cascadecms.com", # UMT reqeuts
                           "projects.getunpackage.com", # UCF request
                           "rihub.org", "*.rihub.org", # RIHub
                           "https://incubate.usc.edu", # USC requested
                           "https://daytonabeach.erau.edu", # ERAU
                           "https://bowiestate.edu",
                           "http://ocr-d8.localhost", # Yale 1
                           "https://test-ocr-d8.pantheonsite.io", # Yale 2
                           "https://eship.msu.edu",  # Michigan State 1
                           "https://entrepreneurship.msu.edu",  # Michigan State 2
                           "https://dev.entrepreneurship.msu.edu",  # Michigan State 3
                           "https://www.entrepreneurship.artsci.utoronto.ca",  # Toronto
                           "https://www.utrgv.edu",  # UT Rio Grande Valley
                           "eshipatcornell.wpengine.com",  # Cornell
                           "eship.cornell.edu",  # Cornell
                           "www.clemson.edu",   # Clemson
                           "https://elevate217.org", #elevate
                           "https://www.apex.vt.edu",
                           "https://www.qc.cuny.edu",
                           "https://www.launchpadalbany.com",
                           "http://qclaunchpad.social.qwriting.qc.cuny.edu"
                           )
    CSP_CONNECT_SRC = ("'self'", "*.cronofy.com", "wss://*.startuptree.co",
                       # 'https://*.agora.io:*', 'https://*.agoraio.cn:*', 'wss://*.agora.io:*', 'wss://*.agoraio.cn:*' # videocall related
                       '*.sentry.io'
                       )
    CSP_SCRIPT_SRC = ("'unsafe-inline'", "'self'", "https:", "'unsafe-eval'", "http:")
    CSP_FONT_SRC = ("'unsafe-inline'", "'self'", "https:", "http:", 'data:')
    CSP_STYLE_SRC = ("'unsafe-inline'", "'self'", "https:", "http:")
    CSP_IMG_SRC = ("'unsafe-inline'", "'self'", "https:", "data:", "http:", "blob:")
    CSP_FRAME_SRC = ("'self'", "https:", "http:")  ## HTTP added for UMT
CSP_WORKER_SRC = ("'self' blob:",)

CHANNELS_ORIGINS = [".asu.edu", ".uwyo.edu", ".startuptree.co",
                    '.wyominginnovates.org', ".uwyo-iie.org",
                    "uwyo-iie.org", "cel.ucf.edu", "cel.projectsbyryan.com",
                    ".umt.edu", "umt.cascadecms.com", # UMT reqeuts
                    "projects.getunpackage.com", # UCF request
                    ".rihub.org", # RIHub
                    "incubate.usc.edu",
                    "daytonabeach.erau.edu",
                    "bowiestate.edu",
                    "ocr-d8.localhost", # Yale 1
                    "test-ocr-d8.pantheonsite.io", # Yale 2
                    "eship.msu.edu",  # Michigan State 1
                    "entrepreneurship.msu.edu",  # Michigan State 2
                    "dev.entrepreneurship.msu.edu",  # Michigan State 3
                    "www.entrepreneurship.artsci.utoronto.ca",  # Toronto
                    "www.utrgv.edu",  # UT Rio Grande Valley
                    "eshipatcornell.wpengine.com",  # Cornell
                    "eship.cornell.edu",  # Cornell
                    "www.clemson.edu",   # Clemson
                    "https://elevate217.org", #elevate
                    "https://www.apex.vt.edu",
                    "https://www.qc.cuny.edu",
                    "https://www.launchpadalbany.com",
                    "http://qclaunchpad.social.qwriting.qc.cuny.edu"
                    ]

if IS_DEV or IS_STAGING:
    CHANNELS_ORIGINS += ['.startuptreetest.co', "file://*", '.startuptreetest2.co']

CSRF_TRUSTED_ORIGINS = list(CSP_FRAME_ANCESTORS)
CSRF_COOKIE_SAMESITE = 'None'
CSRF_FAILURE_VIEW = 'app.csrf.csrf_failure'
SESSION_COOKIE_SAMESITE = 'None'

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000

SUMMERNOTE_CONFIG = {
    'iframe': False,
    'width': '100%',
    'toolbar': [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript',
                  'strikethrough', 'clear']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert',  ['link', 'picture', 'video', 'hr']],
        ['view', ['fullscreen']],
        ['help', ['help']],
    ],
    'css': (STATIC_URL + 'summernote/summernote-full.min.css',)
}

SUMMERNOTE_THEME = 'lite'

TINYMCE_DEFAULT_CONFIG = {
    "theme": "silver",
    "height": 500,
    "convert_urls": False, 
    "menubar": False,
    "plugins": "preview paste importcss searchreplace autolink autosave save directionality code "
               "visualblocks visualchars fullscreen image link media template codesample table charmap hr "
               "pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern "
               "noneditable help charmap quickbars emoticons",
    "toolbar": ['undo redo | bold italic underline strikethrough | '
                'fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | '
                'outdent indent',
                'numlist bullist | forecolor backcolor removeformat'
                'charmap emoticons | fullscreen  preview  | '
                'insertfile image media template link anchor codesample | ltr rtl',
                'table tabledelete | tableprops tablerowprops tablecellprops | '
                'tableinsertrowbefore tableinsertrowafter tabledeleterow | '
                'tableinsertcolbefore tableinsertcolafter tabledeletecol'],
    "image_uploadtab" : True,
    "images_upload_url": '/event/upload_image',
    "images_upload_handler": 'editorImageUpload',
    'automatic_uploads': True
}
TINYMCE_JS_URL = STATIC_URL + 'js/tinymce/tinymce.min.js'
TINYMCE_JS_ROOT = STATIC_URL + 'js/tinymce'

ASGI_APPLICATION = "StartupTree.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [(REDIS_URL, REDIS_PORT)],
        },
    },
}

DJSTRIPE_USE_NATIVE_JSONFIELD = True
STRIPE_LIVE_SECRET_KEY = environ.get("STRIPE_API_KEY")
STRIPE_TEST_SECRET_KEY = environ.get("STRIPE_API_KEY")
STRIPE_LIVE_MODE = True  # Change to True in production
DJSTRIPE_FOREIGN_KEY_TO_FIELD = 'id'
DJSTRIPE_WEBHOOK_EVENT_CALLBACK = 'boostbudget.webhooks.webhook_event_callback_for_connect'
STRIPE_API_KEY = STRIPE_LIVE_SECRET_KEY
DJSTRIPE_WEBHOOK_SECRET = environ.get("STRIPE_WEBHOOK_KEY")
if IS_DEV or IS_TEST:
    # STRIPE_API_HOST = 'http://stripe-cli'  # 'http://stripe-mock:12111'
    STRIPE_LIVE_MODE = False
    DJSTRIPE_WEBHOOK_SECRET = "whsec_241susUP9NB24nAlMJqFd6fnjNFo12qT"
    STRIPE_API_KEY = STRIPE_TEST_SECRET_KEY
STRIPE_PUBLISHABLE_KEY = environ.get("STRIPE_PUBLISHABLE_KEY")

import json
with open('package.json', 'r') as f:
    _package = json.load(f)
    ST_VERSION = _package['version']
    ST_GULP_VERSION = _package['gulp-version']

SSR_EXPRESS_ADDRESS = 'http://express:3000'
if IS_TEST:
    SSR_EXPRESS_ADDRESS = 'http://express_test:3000'
