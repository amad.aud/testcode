from django.core.cache import caches

FUNDING = '{0}_total_funding'
EXIT = '{0}_total_exit'
REVENUE = '{0}_total_revenue'
COMPANYSIZE = '{0}_company_size'
MEMBERSBYSCHOOL = '{0}_members_by_school'
EMPLOYEEBYYEAR = '{0}_employees_by_year'
default_cache = caches['default']
page_caches = caches['page_caches']
attempt_caches = caches['attempt_caches']
LOGO = '{0}_logo'
CSS = '{0}_css'
COLOR = '{0}_color'
DASHBOARD = '{0}_user_dashboard'
MENTORBANNER = '{0}_mentor_banner'
FEED = '{0}_feed'
PEOPLE = '{0}_people'
DISCUSSION = '{0}_discussion'
U_WEB = '{0}_website'
UE_WEB = '{0}_eship'
UE_WEB_NAME = '{0}_eship_name'
U_BLOG = '{0}_blog'
NEWSTART = '{0}_new_startups'
USERSTAT = '{0}_uni_user_stat'
FOLLOWERSTAT = '{0}_uni_follower_stat'
STARTUPVIEWCOUNT = '{0}_uni_startup_view_count'
UE_FB = '{0}_facebook'
UE_TW = '{0}_twitter'
UE_EXPT = '{0}_export'


def set_total_funding(site_name, amount):
    default_cache.set(FUNDING.format(site_name), amount, timeout=None)


def get_total_funding(site_name):
    return default_cache.get(FUNDING.format(site_name))


def set_total_exit(site_name, amount):
    default_cache.set(EXIT.format(site_name), amount, timeout=None)


def get_total_exit(site_name):
    return default_cache.get(EXIT.format(site_name))


def set_total_revenue(site_name, amount):
    default_cache.set(REVENUE.format(site_name), amount, timeout=None)


def get_total_revenue(site_name):
    return default_cache.get(REVENUE.format(site_name))


def set_company_size(site_name, data):
    default_cache.set(COMPANYSIZE.format(site_name), data, timeout=None)


def get_company_size(site_name):
    return default_cache.get(COMPANYSIZE.format(site_name))
    # return None


def set_members_by_school(site_name, data):
    default_cache.set(MEMBERSBYSCHOOL.format(site_name), data, timeout=None)


def get_members_by_school(site_name):
    return default_cache.get(MEMBERSBYSCHOOL.format(site_name))
    # return None


def set_members_by_year(site_name, data):
    default_cache.set(EMPLOYEEBYYEAR.format(site_name), data, timeout=None)


def get_members_by_year(site_name):
    # return default_cache.get(EMPLOYEEBYYEAR.format(site_name))
    return None


def set_uni_logo(site_name, data):
    default_cache.set(LOGO.format(site_name), data, timeout=None)


def get_uni_logo(site_name):
    return default_cache.get(LOGO.format(site_name))


def set_uni_css(site_name, data):
    default_cache.set(CSS.format(site_name), data, timeout=None)


def get_uni_css(site_name):
    return default_cache.get(CSS.format(site_name))


def set_uni_colors(site_name, data):
    '''
    data = list of colors ex) [#11111, #22222]
    '''
    default_cache.set(COLOR.format(site_name), data, timeout=None)


def get_uni_colors(site_name):
    return default_cache.get(COLOR.format(site_name))


def set_user_dashboard(site_name, data):
    default_cache.set(DASHBOARD.format(site_name), data, timeout=None)


def get_user_dashboard(site_name):
    return default_cache.get(DASHBOARD.format(site_name))


def set_mentor_banner(site_name, data):
    default_cache.set(MENTORBANNER.format(site_name), data, timeout=None)


def get_mentor_banner(site_name):
    return default_cache.get(MENTORBANNER.format(site_name))


def set_people_feed(site_name, data):
    default_cache.set(PEOPLE.format(site_name), data, timeout=None)


def get_people_feed(site_name):
    return default_cache.get(PEOPLE.format(site_name))


def set_forum_feed(site_name, data):
    default_cache.set(DISCUSSION.format(site_name), data, timeout=None)


def get_forum_feed(site_name):
    return default_cache.get(DISCUSSION.format(site_name))


def set_uni_feed(site_name, data):
    '''
    data = integer value indicating an option set by University staff:
    1: show everything
    2: hide startup related feed
    '''
    default_cache.set(FEED.format(site_name), data, timeout=None)


def get_uni_feed(site_name):
    """
    get feed option set by university
    :param site_name:
    :return: feed_option, enable_global
    """
    data = default_cache.get(FEED.format(site_name))
    if isinstance(data, tuple):
        return data
    # legacy code compatibility
    return data, True


def set_uni_eship_web(site_name, data):
    default_cache.set(UE_WEB.format(site_name), data, timeout=None)


def get_uni_eship_web(site_name):
    return default_cache.get(UE_WEB.format(site_name))


def set_uni_website(site_name, data):
    default_cache.set(U_WEB.format(site_name), data, timeout=None)


def get_uni_website(site_name):
    return default_cache.get(U_WEB.format(site_name))


def set_uni_blog(site_name, data):
    default_cache.set(U_BLOG.format(site_name), data, timeout=None)


def get_uni_blog(site_name):
    return default_cache.get(U_BLOG.format(site_name))


def set_new_startups(site_name, data):
    '''
    data = [[x-axis dates],[number of startups]]
    data[0][0] is data[1][0]
    '''
    default_cache.set(NEWSTART.format(site_name), data, timeout=None)


def get_new_startups(site_name):
    return default_cache.get(NEWSTART.format(site_name))
    # return None


def set_user_stat(site_name, data):
    '''
    data = [[x-axis dates],[number of startups]]
    data[0][0] is data[1][0]
    '''
    default_cache.set(USERSTAT.format(site_name), data, timeout=None)


def get_user_stat(site_name):
    return default_cache.get(USERSTAT.format(site_name))
    # return None


def set_follower_stat(site_name, data):
    '''
    data = [[x-axis dates],[number of startups]]
    data[0][0] is data[1][0]
    '''
    default_cache.set(FOLLOWERSTAT.format(site_name), data, timeout=None)


def get_follower_stat(site_name):
    return default_cache.get(FOLLOWERSTAT.format(site_name))


def set_startup_view_count(site_name, data):
    '''
    data = [[x-axis dates],[number of startups]]
    data[0][0] is data[1][0]
    '''
    default_cache.set(STARTUPVIEWCOUNT.format(site_name), data, timeout=None)


def get_startup_view_count(site_name):
    return default_cache.get(STARTUPVIEWCOUNT.format(site_name))


def set_uni_eship_web_name(site_name, data):
    default_cache.set(UE_WEB_NAME.format(site_name), data, timeout=None)


def get_uni_eship_web_name(site_name):
    return default_cache.get(UE_WEB_NAME.format(site_name))


def set_uni_facebook(site_name, data):
    default_cache.set(UE_FB.format(site_name), data, timeout=None)


def get_uni_facebook(site_name):
    return default_cache.get(UE_FB.format(site_name))


def set_uni_twitter(site_name, data):
    default_cache.set(UE_TW.format(site_name), data, timeout=None)


def get_uni_twitter(site_name):
    return default_cache.get(UE_TW.format(site_name))


def set_uni_export(site_name, data):
    default_cache.set(UE_EXPT.format(site_name), data, timeout=None)


def get_uni_export(site_name):
    return default_cache.get(UE_EXPT.format(site_name))
