from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import OriginValidator
from notifications.consumers import NotificationConsumer, TestConsumer
from django.conf import settings

if settings.IS_DEV:
    url_routers = [url(r'^ws/notification/$', NotificationConsumer), url(r'^ws/checkabc/$', TestConsumer)]
else:
    url_routers = [url(r'^ws/notification/$', NotificationConsumer)]

application = ProtocolTypeRouter({
    "websocket": OriginValidator(
        AuthMiddlewareStack(
            URLRouter(url_routers)
        ),
        settings.CHANNELS_ORIGINS
    )
})