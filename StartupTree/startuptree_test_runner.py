from django.test.runner import DiscoverRunner
from django.conf import settings
import os


class UseTestDatabaseTestSuiteRunner(DiscoverRunner):
    """
    def setup_databases(self, **kwargs):
        pass

    def teardown_databases(self, *args, **kwargs):
        pass
    """

    def teardown_test_environment(self, **kwargs):
        super(UseTestDatabaseTestSuiteRunner, self).teardown_test_environment(**kwargs)
        from django.contrib.sites.models import Site
        Site.objects.clear_cache()  # clear cache from redis after testing.

    def setup_test_environment(self, **kwargs):
        """
        Set up test environment so that celery's .delay operation is taken care locally.
        Code is based on django-celery's test_runner (djcelery.contrib.test_runner.py)
        :param kwargs:
        :return:
        """
        os.environ['DJANGO_LIVE_TEST_SERVER_ADDRESS'] = 'cornell.startuptreetest.co'
        settings.EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
        super(UseTestDatabaseTestSuiteRunner, self).setup_test_environment(**kwargs)
