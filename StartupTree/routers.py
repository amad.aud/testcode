class MasterSlaveRouter:
    """
    @todo:
    A router for later if we need replication or slave.
    """
    def db_for_read(self, model, **hints):
        '''
        @todo:
        Later, chose between slaves.
        '''
        return 'slave1'

    def db_for_write(self, model, **hints):
        """
        Writes always go to master.
        """
        return 'master'

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed if both objects are
        in the master/slave pool.
        """
        db_list = ('master', 'slave1')
        if obj1._state.db in db_list and obj2._state.db in db_list:
            return True
        return None

    def allow_migrate(self, db, model):
        return True
