"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
"""

import os
import django
from channels.routing import get_default_application
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

IS_DEV = True if os.environ.get('DJ_DEBUG') else False
IS_STAGING = True if os.environ.get('DJ_STAGING') else False

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "StartupTree.settings")
django.setup()

if IS_DEV and not IS_STAGING:
    application = get_default_application()
else:
    os.environ.setdefault("ASGI_THREADS", "5")
    application = SentryAsgiMiddleware(get_default_application())
