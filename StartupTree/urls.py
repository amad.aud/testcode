from django.conf.urls import url, include
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView, PasswordResetCompleteView, PasswordResetDoneView
from django.views.generic.base import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from django.views.defaults import page_not_found
from django.urls import path
from branch import views as branch_views
from branch.views import search as branch_search
from discussion.views import post_content_legacy
from mentorship.views import search as mentor_search
from trunk.views import search as trunk_search
from app import views as app_views
from emailuser import views as emailuser_views
from api.views.login import DeleteTokens
import api.views as api_views
import oauth2_provider.views as oauth2_views
from django.utils import timezone
from emailuser.conf import (
    PROTOCOL,
    FROM_EMAIL
)

from webhooks.views import EventWebhook



urlpatterns = [
    #path('webhook/events', EventWebhook.as_view(), name='event-webhook'),
    url(r'^$', branch_views.home, name='home'),
    url(r'^company/', include('branch.urls.company_urls')),
    url(r'^user/', include('emailuser.urls')),
    url(r'^university/', include('trunk.urls')),
    url(r'^', include('activity.urls')),
    url(r'^account/settings$', branch_views.account_settings,
        name="account-settings"),
    # url(r'^contact/$', app_views.contact, name='contact'),
    url(r'^people/', include('branch.urls.people_urls')),
    url(r'^connect/linkedin$',
        branch_views.connect_with_linkedin,
        name='connect-linkedin'),
    url(r'^discover/', include('StartupTree.mixins.urls.discover_urls')),
    url(r'^forum/', include('discussion.urls.post_content_urls')),
    url(r'^webhook/', include('webhooks.urls')),
    # for backward compatibility
    url(r'^discussions/(?P<post_id>[0-9]+)$',
        post_content_legacy, name="post-content-id-legacy"),
    url(r'^discussions/(?P<post_id>[0-9]+)/(?P<title>[\W\w]+)$',
        post_content_legacy, name="post-content-legacy"),

    # for backward compatibility
    url(r'^organization/', include('branch.urls.page_urls')),
    url(r'^page/', include('branch.urls.page_urls')),

    url(r'^follow/', include('branch.urls.follow_urls')),
    url(r'^unfollow/', include('branch.urls.unfollow_urls')),
    url(r'^review/', include('branch.urls.judge_review_urls')),
    url(r'^about$', branch_views.about, name='about'),

    url(r'^products/$', branch_views.products, name='products'),
    url(r'^insights/$', branch_views.insights, name='insights'),
    url(r'^careers/$', branch_views.careers, name='careers'),
    url(r'^case_studies/$', branch_views.case_studies, name='case_studies'),
    url(r'^students/$', app_views.students, name='students'),
    url(r'^faqs/$', app_views.faqs, name='faqs'),

    url(r'^mentors$', app_views.mentors, name='mentors'),

    url(r'^resource$', app_views.resource, name='resource'),
    url(r'^track$', app_views.track, name='track'),
    url(r'^grow$', app_views.grow, name='grow'),
    url(r'^engage$', app_views.engage, name='engage'),
    url(r'^globaltest/$', app_views.globaltest, name='globaltest'),

    url(r'^terms/$', app_views.terms, name='terms'),
    url(r'^privacy/$', app_views.privacy, name='privacy'),
    url(r'^login$', emailuser_views.user_login, name='login'),

    url(r'^recover$', PasswordResetView.as_view(
        template_name='emailuser/forgot_password.html',
        extra_email_context={"year": timezone.now().year, 'PROTO': PROTOCOL},
        from_email=FROM_EMAIL,
        email_template_name='email_templates/accounts/Reset_your_password.txt',
        html_email_template_name='email_templates/accounts/Reset_your_password.html',
        subject_template_name='email_templates/accounts/Reset_your_password_title.txt',
        success_url='/recover/done'
    ), name='recover'),
    url(r'^recover/done', PasswordResetDoneView.as_view(
        template_name='emailuser/forgot_password_success.html',
        extra_context={'year': timezone.now().year}
    ), name='password_reset_done'),
    path('recover/confirm/<uidb64>/<token>', PasswordResetConfirmView.as_view(
        template_name="emailuser/recover_password.html"
    ), name='password_reset_confirm'),
    url(r'^recover/complete$', PasswordResetCompleteView.as_view(
        template_name="emailuser/reset_success.html"
    ), name='password_reset_complete'),

    url(r'^referral/(?P<url_name>[-@\w]+)$', emailuser_views.process_referral,
        name='process-referral'),
    url(r'^logout$', emailuser_views.linkedin_logout, name='logout'),
    path('unsubscribe/<uuid:uuid>', emailuser_views.unsubscribe_emails, name='unsubscribe'),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt',
                                               content_type='text/plain')),
    url(r'^inbox/', include('inbox.urls.urls')),
    url(r'^rooms/', include('inbox.urls.messaging')),
    url(r'^notis/', include('notifications.urls.urls')),
    url(r'^connection-request/', include('inbox.urls.team_request_urls')),
    url(r'^team-request/', include('inbox.urls.team_request_urls')),  # to be deprecated
    url(r'^summernote/', include('django_summernote.urls')),
    url(r'^event/', include('branch.urls.event_urls')),
    url(r'^competition/', include('branch.urls.competition_urls')),
    url(r'^application/', include('branch.urls.application_urls')),
    url(r'^survey/', include('branch.urls.survey_urls')),
    url(r'^api/', include('api.urls')),
    # url(r'^api/v1/', include('api.urls')),
    url(r'^api/internal/', include('data_collection.urls')),

    url(r'^mentorship/', include('mentorship.urls')),
    url(r'^status$', app_views.service_status),
    url(r'^matches/', include('recommendation.urls')),
    url(r'^venture/', include('venture.urls')),

    url(r'^boost-budget/', include('boostbudget.urls.admin')),
    url(r'^membership/', include('boostbudget.urls.membership')),

    url(r'^tutorial$', app_views.extra_tutorial),
    url(r'^activity/', include('activity.urls')),
    url(r'^questions/', include('forms.urls')),
    url(r'^calendar/', include('forms.urls')),
    url(r'^timezone$', app_views.set_timezone),
    url(r'^timezone2$', app_views.set_timezone2),
    # url(r'^video/', include('videocall.urls'))
    # url('^google1dafade814abfeba.html$',
    #     TemplateView.as_view(template_name='google1dafade814abfeba.html',
    #                          content_type='text/plain'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Search URL
urlpatterns += [
    # (r'^search/', include('haystack.urls')),
    url(r'^search$', branch_search.search),
    url(r'^search/members$', branch_search.find_member_autocomplete),
    url(r'^search/members/email$',
        branch_search.find_useremail_member_autocomplete),
    url(r'^search/autocomplete/email$',
        branch_search.autocomplete_useremail),
    url(r'^search/autocomplete/mentor$',
        mentor_search.mentor_autocomplete),
    url(r'^search/autocomplete/mentor/new$',
        mentor_search.mentor_autocomplete, {"search_member" : True}),
    url(r'^search/autocomplete/team_mentor$',
        mentor_search.mentor_autocomplete, {"team_mentor" : True}),
    url(r'^search/autocomplete$', branch_search.autocomplete_general,
        name='autocomplete-general'),
    # Warning, below will show results for all companies at all univresitise
    url(r'^search/autocomplete/company$',
        branch_search.autocomplete_experience_company,
        name='autocomplete-company'),
    # Below will show results for companies at current university only
    url(r'^search/autocomplete/company_this_uni$',
        branch_search.autocomplete_company_at_uni,
        name='autocomplete-company-uni'),
    url(r'^search/autocomplete/position$',
        branch_search.autocomplete_experience_position,
        name='autocomplete-position'),
    url(r'^search/autocomplete/skill$',
        branch_search.autocomplete_experience_skill,
        name='autocomplete-skill'),
    url(r'^search/autocomplete/role',
        branch_search.autocomplete_member_role),
    url(r'^search/autocomplete/group',
        branch_search.autocomplete_member_group),
    url(r'^search/autocomplete/city$',
        branch_search.autocomplete_location),
    url(r'^search/autocomplete/major$',
        trunk_search.autocomplete_major),
    url(r'^search/company/global$', branch_search.global_search),
    url(r'^search/add/startup$', branch_views.add_startup_search,
        name='add_startup_search'),
    url(r'^search/autocomplete/tag$',
        branch_search.autocomplete_tags),
]

# OAuth2 provider endpoints
oauth2_endpoint_views = [
    url(r'^authorize/$', oauth2_views.AuthorizationView.as_view(), name="authorize"),
    url(r'^token/$', oauth2_views.TokenView.as_view(), name="token"),
    url(r'^revoke_token/$', oauth2_views.RevokeTokenView.as_view(), name="revoke-token"),
    url(r'^delete_token/$', DeleteTokens.as_view()),
]

# OAuth2 Application Management endpoints
oauth2_endpoint_views += [
    url(r'^applications/$', api_views.ApplicationList.as_view(), name="list"),
    url(r'^applications/register/$', api_views.ApplicationRegistration.as_view(), name="register"),
    url(r'^applications/(?P<pk>\d+)/$', api_views.ApplicationDetail.as_view(), name="detail"),
    url(r'^applications/(?P<pk>\d+)/delete/$', api_views.ApplicationDelete.as_view(), name="delete"),
    url(r'^applications/(?P<pk>\d+)/update/$', api_views.ApplicationUpdate.as_view(), name="update"),
]

# OAuth2 Token Management endpoints
oauth2_endpoint_views += [
    url(r'^authorized-tokens/$', api_views.AuthorizedTokensListView.as_view(), name="authorized-token-list"),
    url(r'^authorized-tokens/(?P<pk>\d+)/delete/$', api_views.AuthorizedTokenDeleteView.as_view(),
        name="authorized-token-delete"),
]


#for oauth2
urlpatterns += [
    url(r'^o/', include((oauth2_endpoint_views, 'oauth2_provider_app',), namespace="oauth2_provider")),
]

urlpatterns += [
    url(r'^tinymce/', include('tinymce.urls')),
]

# stripe webhooks
urlpatterns += [
    url(r'^stripe/', include('djstripe.urls', namespace='djstripe'))
]

if settings.DEBUG or settings.IS_DEV:
    urlpatterns.append(url(r'^404/$', page_not_found))
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^__test__/', include('dev_debug.urls')),
    ]
