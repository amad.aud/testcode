// --- Config --- //
var purecookieTitle = "Cookie Policy."; // Title
var purecookieDesc = "By using this website, you automatically accept that we use cookies."; // Description
var purecookieLink = '<a tabindex=0 href="https://www.startuptree.co/privacy/#sections.image-text.181" target="_blank">Privacy policy?</a>'; // Cookiepolicy link
var purecookieTermsLink = '<a  tabindex=0 style="padding-left:10px" href="https://www.startuptree.co/terms-of-use/#sections.image-text.1355" target="_blank">Terms Of Use & Agreement?</a>'; // Terms Of Use & Agreement
var purecookieButton = "Accept"; // Button text
var cookieButtonReject = "Reject"; // Button text
// ---        --- //


function pureFadeIn(elem, display) {
    var el = document.getElementById(elem);
    el.style.opacity = 0;
    el.style.display = display || "block";

    (function fade() {
        var val = parseFloat(el.style.opacity);
        if (!((val += .02) > 1)) {
            el.style.opacity = val;
            requestAnimationFrame(fade);
        }
    })();
};

function pureFadeOut(elem) {
    var el = document.getElementById(elem);
    el.style.opacity = 1;

    (function fade() {
        if ((el.style.opacity -= .02) < 0) {
            el.style.display = "none";
        } else {
            requestAnimationFrame(fade);
        }
    })();
};

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}

function cookieConsent() {
    if (!getCookie('cookieConsent')) {
        consentBody = '<div class="cookieConsentContainer" id="cookieConsentContainer">' +
            '<div class="cookieTitle">' +
            '<a>' + purecookieTitle + '</a>' +
            '</div>' +
            '<div class="cookieDesc">' +
            '<p>' + purecookieDesc + ' ' + purecookieLink +  purecookieTermsLink +'</p>' +
            '</div><div class="cookieButton">' +
            '<button tabindex=0 onClick="purecookieDismiss();">' + purecookieButton + '</button>' +
            '<button tabindex=0 class="rejectbtn" onClick="purecookieReject();">' + cookieButtonReject + '</button>' +
            '</div></div>';
        var consent = document.createElement('consent_body');
        consent.id = 'consent_body';
        consent.innerHTML = consentBody;
        document.body.appendChild(consent);
        pureFadeIn("cookieConsentContainer");
    }
}

function purecookieDismiss() {
    setCookie('cookieConsent', '1', 30);
    pureFadeOut("cookieConsentContainer");
}

function purecookieReject() {
    setCookie('cookieConsent', '1', 7);

    if (window.ga) window.ga('remove');
    // Remove the default cookies
    // _ga is used to distinguish users.
    window['ga-disable-UA-53645746-1'] = true; //disabling cookie on reject
    window['ga-disable-UA-79359381-1'] = true; //disabling cookie on reject
    console.log('removing cookie')
    var gaProperty = 'UA-79359381-1';

    // Disable tracking if the opt-out cookie exists.
    var disableStr = 'ga-disable-' + gaProperty;
    if (document.cookie.indexOf(disableStr + '=true') > -1) {
        window[disableStr] = true;
    }
    gaOptout()

    // Opt-out function
    function gaOptout() {
        document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
        window[disableStr] = true;
    }

    pureFadeOut("cookieConsentContainer");
}

window.onload = function () {
    cookieConsent();
};