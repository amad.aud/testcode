{
    "openapi": "3.0.2",
    "info": {
        "title": "StartupTree API Service",
        "version": "1.0.0",
        "description": "API document for StartupTree.\nIt is currently in alpha state.",
        "termsOfService": "",
        "contact": {
            "name": "StartupTree Team",
            "url": "https://startuptree.co",
            "email": "support@startuptree.co"
        }
    },
    "servers": [
        {
            "url": "https://{platform_domain}.startuptree.co",
            "description": "The root url to access API endpoints.  platform_domain is the subdomain of your platform.\nFor instance, for Cornell University platform, https://cornell.startuptree.co, the URL is https://cornell.startuptree.co/api",
            "variables": {
                "platform_domain": {
                    "default": "platform_domain"
                }
            }
        }
    ],
    "paths": {
        "/api/events/": {
            "summary": "Get list of all existing events on the platform",
            "get": {
                "responses": {
                    "200": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/Event"
                                    }
                                },
                                "example": {
                                  "value": [
                                    {
                                        "start": "2019-03-03T05:00:00Z",
                                        "end": "2019-11-08T18:00:00Z",
                                        "location": "Lindt Chocolate Café, Martin Place, Martin Place, Sydney NSW, Australia",
                                        "name": "Example Event 1",
                                        "event_id": "1f4897a8-13ec-46df-a692-83af6c8eb5f4",
                                        "description": "description of this event...",
                                        "url": "https://test.startuptree.co/event/s/ZxdDGGMTKqe5z5xxDepivw/Example-Event-1"
                                    },
                                    {
                                        "start": "2019-03-03T05:00:00Z",
                                        "end": "2019-11-08T18:00:00Z",
                                        "location": "Lindt Chocolate Café, Martin Place, Martin Place, Sydney NSW, Australia",
                                        "name": "Example Event 2",
                                        "event_id": "1f4897a8-13ec-46df-a692-83af6c8eb5f6",
                                        "description": "description of this event...",
                                        "url": "https://test.startuptree.co/event/s/FNjdzPPh6junJQi3rKGuEm/Example-Event-2"
                                    }
                                ]
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "API Key": [
                        ]
                    }
                ],
                "summary": "Get list of all existing events on the platform",
                "description": "Get list of all existing events on the platform. You can narrow the result by providing optional start or end parameters.",
                "parameters": [
                    {
                        "name": "start",
                        "description": "Filter events whose date is greater than equal to the given date. Date should be in ISO 8601 date format. ex) 2020-08-01",
                        "schema": {
                            "type": "string"
                        },
                        "in": "query",
                        "required": false
                    },
                    {
                        "name": "end",
                        "description": "Filter events whose start date is less than or equal to the given date. Date should be in ISO 8601 date format. ex) 2020-08-01",
                        "schema": {
                            "type": "string"
                        },
                        "in": "query",
                        "required": false
                    }
                ]
            }
        },
        "/api/events/{event_id}": {
            "summary": "Get basic event information",
            "get": {
                "parameters": [
                    {
                        "name": "event_id",
                        "description": "ID of an event",
                        "schema": {
                            "type": "string"
                        },
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Event"
                                },
                                "example": {
                                  "value": {
                                        "start": "2019-03-03T05:00:00Z",
                                        "end": "2019-11-08T18:00:00Z",
                                        "location": "Lindt Chocolate Café, Martin Place, Martin Place, Sydney NSW, Australia",
                                        "name": "Example Event 1",
                                        "event_id": "1f4897a8-13ec-46df-a692-83af6c8eb5f4",
                                        "description": "description of this event...",
                                        "url": "https://test.startuptree.co/event/s/ZxdDGGMTKqe5z5xxDepivw/Example-Event-1"
                                    }
                                }
                            }
                        }
                    },
                    "400": {

                    }
                },
                "security": [
                    {
                        "API Key": [
                        ]
                    }
                ],
                "summary": "Get basic event information",
                "description": "Get basic event information of the given event."
            }
        },
        "/api/events/{event_id}/rsvp": {
            "summary": "RSVP user to the event",
            "description": "RSVP user to the event.\nIf user's account exists, email and affiliation should be provided.\nIf user is a guest, all parameters should be filled.",
            "post": {
                "requestBody": {
                    "required": true
                },
                "parameters": [
                    {
                        "name": "first_name",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": false
                    },
                    {
                        "name": "last_name",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": false
                    },
                    {
                        "name": "email",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "affiliation",
                        "description": "Affiliation of RSVP user. One of the following integers:\n1 - Student\n2 - Alumni\n3 - Faculty Staff\n4 - Not affiliated",
                        "schema": {
                            "type": "integer"
                        },
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "201": {

                    }
                },
                "security": [
                    {
                        "API Key": [
                        ]
                    }
                ],
                "summary": "RSVP user to the event",
                "description": "RSVP user to the event.\nIf user's account exists, email and affiliation should be provided.\nIf user is a guest, all parameters should be filled."
            }
        },
        "/api/events/{event_id}/check-in": {
            "summary": "Check In user to the event",
            "description": "Check in user to the event.\nIf user has RSVPed before (email as a unique key), only email needs to be provided.\nIf user hasn't RSVPed:\n- If user's account exists, email and affiliation should be provided.\n- If user is a guest, all parameters should be filled.",
            "post": {
                "requestBody": {
                    "required": true
                },
                "parameters": [
                    {
                        "name": "first_name",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": false
                    },
                    {
                        "name": "last_name",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": false
                    },
                    {
                        "name": "email",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "affiliation",
                        "description": "Affiliation of RSVP user. One of the following integers:\n1 - Student\n2 - Alumni\n3 - Faculty Staff\n4 - Not affiliated",
                        "schema": {
                            "type": "integer"
                        },
                        "in": "query",
                        "required": false
                    }
                ],
                "responses": {
                    "201": {

                    }
                },
                "security": [
                    {
                        "API Key": [
                        ]
                    }
                ],
                "summary": "Check In user to the event",
                "description": "Check in user to the event.\nIf user has RSVPed before (email as a unique key), only email needs to be provided.\nIf user hasn't RSVPed:\n- If user's account exists, email and affiliation should be provided.\n- If user is a guest, all parameters should be filled."
            }
        },
        "/api/people/email": {
            "summary": "Get User Profile",
            "description": "Get user profile with email if exists.\nIf user does not exist, returns null value.",
            "get": {
                "parameters": [
                    {
                        "name": "email",
                        "description": "Email of an user to retrieve",
                        "schema": {
                            "type": "string"
                        },
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Member"
                                },
                                "example": {
                                  "value": {
                                    "email": "example@startuptree.co",
                                    "first_name": "John",
                                    "last_name": "Smith",
                                    "gender": "Male",
                                    "get_image": "/media/example.png",
                                    "location": "Ithaca, New York",
                                    "name": "John Smith"
                                  }
                                }
                            }
                        }
                    }
                },
                "security": [
                    {
                        "API Key": [
                        ]
                    }
                ],
                "summary": "Get User Profile",
                "description": "Get user profile with email if exists.\nIf user does not exist, returns null value."
            }
        },
        "/api/user/signup": {
            "summary": "Create User",
            "description": "Create platform's user.\nThe user's account will be created with randomly generated password.\nUser will need to reset password to get access to the account.",
            "post": {
                "parameters": [
                    {
                        "name": "email",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "first_name",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "last_name",
                        "description": "",
                        "schema": {

                        },
                        "in": "query",
                        "required": true
                    },
                    {
                        "name": "affiliation",
                        "description": "Affiliation of user. Value should be one of the following integers:\n1 - Staff\n2 - Faculty\n3 - Not affiliated\n4 - Student\n5 - Alumni",
                        "schema": {
                            "type": "integer"
                        },
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Member"
                                },
                                "example": {
                                  "value": {
                                    "email": "example@startuptree.co",
                                    "first_name": "John",
                                    "last_name": "Smith",
                                    "gender": "Male",
                                    "get_image": "/media/example.png",
                                    "location": "Ithaca, New York",
                                    "name": "John Smith"
                                  }
                                }
                            }
                        }
                    }
                },
                "summary": "Create User",
                "description": "Create platform's user.\nThe user's account will be created with randomly generated password.\nUser will need to reset password to get access to the account."
            }
        }
    },
    "components": {
        "schemas": {
            "Event": {
                "description": "Contains basic information of event",
                "type": "object",
                "properties": {
                    "start": {
                        "description": "Start datetime in ISO-8601 format."
                    },
                    "end": {
                        "description": "End datetime in ISO-8601 format."
                    },
                    "name": {
                        "description": "Name of the event"
                    },
                    "event_id": {
                        "description": "Unique ID of the event"
                    },
                    "location": {
                        "description": "Location of the event"
                    },
                    "description": {
                        "description": "Description of the event.`"
                    }
                },
                "example": {
                    "start": "2019-03-03T05:00:00Z",
                    "end": "2019-11-08T18:00:00Z",
                    "location": "Lindt Chocolate Café, Martin Place, Martin Place, Sydney NSW, Australia",
                    "name": "Example Event",
                    "event_id": "1f4897a8-13ec-46df-a692-83af6c8eb5f4",
                    "description": "description of this event..."
                }
            },
            "Member": {
                "description": "Profile of a user on the platform.",
                "type": "object",
                "properties": {
                    "email": {
                        "description": ""
                    },
                    "first_name": {
                        "description": ""
                    },
                    "last_name": {
                        "description": ""
                    },
                    "gender": {
                        "description": ""
                    },
                    "get_image": {
                        "description": ""
                    },
                    "location": {
                        "description": ""
                    }
                },
                "example": {
                  "value": {
                    "email": "example@startuptree.co",
                    "first_name": "John",
                    "last_name": "Smith",
                    "gender": "Male",
                    "get_image": "/media/example.png",
                    "location": "Ithaca, New York",
                    "name": "John Smith"
                  }
                }
            }
        },
        "responses": {
            "value": {
                "content": {
                    "application/json": {

                    }
                },
                "description": "value returned for the request. \n{\n    'value': 'output_of_api'\n}"
            },
            "error": {
                "description": "Error return message"
            }
        },
        "securitySchemes": {
            "API Key": {
                "type": "apiKey",
                "description": "All endpoints require api key to in AUTHORIZATION HTTP header for authentication. \n\nYour api key should be included in HTTP header's AUTHORIZATION in the following format: \n\nAUTHORIZATION: token <your_api_key> \n\nExample with python requests: <code>\n\n    def get_request(url, parm): \n        # HTTP_AUTHORIZATION header in HTTP request for authorization \n        headers = {'AUTHORIZATION': 'token 5f20cc4cdef8bfcf747fc70cdc058bc79673c1'} \n        r = requests.get(url, headers=headers, params=parm) \n        return r \n\n \n    def post_request(url, parm): \n        # HTTP_AUTHORIZATION header in HTTP request for authorization \n        headers = {'AUTHORIZATION': 'token 5f20cc4cdef8bfcf747fc70cdc058bc79673c1'} \n        r = requests.post(url, headers=headers, data=parm) \n        return r</code>",
                "name": "AUTHORIZATION",
                "in": "header"
            },
            "Browser API Key": {
                "type": "HTTP REFERER",
                "description": "Browser API Key is specific for frontend usage. It is similar to regular API Key, but provides a way to set HTTP_REFERER so that the key cannot be used easily by other websites. \n\n You can set HTTP_REFERER on admin panel. HTTP_REFERER should be in a full URL format such as https://www.example.com/ , https://www.example.com/api/used/page. \n\n If HTTP_REFERER is not provided, the key can be used anywhere. \n\n Note that at the moment, browser api key can only be used for GET operations."
            }
        }
    },
    "security": [
        {
            "API Key": [
            ]
        }
    ]
}
