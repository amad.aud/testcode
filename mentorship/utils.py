import pytz
from django.utils import timezone
from trunk.models import AccessLevel
from venture.models import MemberToAdmin
import datetime as dtime
from StartupTree.loggers import app_logger
from StartupTree.utils.links import gen_google_calendar, gen_yahoo_calendar
import os
from bs4 import BeautifulSoup


def get_formatted_time(year, selected_date, selected_start_time):
    try:
        tmp_date = dtime.datetime.strptime(
                "{0} {1}".format(year, selected_date), "%Y %b %d")
    except ValueError:
        # team mentorship is in format YYYY-MM-DD
        tmp_date = dtime.datetime.strptime(selected_date, "%Y-%m-%d")
    tmp_time = dtime.datetime.strptime(selected_start_time, "%I:%M%p")

    return tmp_date, tmp_time


def get_mentorship_file_url(instance, filename):

    if instance.single_mentorship:
        path_id = instance.single_mentorship_id
        path = os.path.join("storage", "mentorship", "%s" % path_id, filename)
    else:
        path_id = instance.team.url_name
        path = os.path.join("storage", "mentorship", "team", "%s" % path_id, filename)
    return path


def localize(time, tz):
    try:
        result = timezone.localtime(time, pytz.timezone(tz))
    except ValueError:
        try:
            result = timezone.make_aware(time, pytz.timezone(tz))
        except ValueError:
            result = time

    return result


def string_to_bool(temp):
    if temp in ['True', 'true', 'False', 'false']:
        return True if temp in ['True', 'true'] else False
    else:
        return None


def is_user_authorized_team_mentorship(request__is_university, request__access_levels,
                                       team_mentorship, request__user_id):
    if team_mentorship.mentor.profile.user_id != request__user_id and \
            not MemberToAdmin.objects.filter(member__user=request__user_id, startup=team_mentorship.team).exists():
        if request__is_university:
            if not AccessLevel.TEAM_MENTORSHIP in request__access_levels and not AccessLevel.ALL in request__access_levels:
                return False
        else:
            return False
    return True


def email_link_generator_m(session_start, session_end, location, mentor_email,
                           email_cal_title, email_cal_desc):
    """

    :param session_start: Timezone aware DateTime Object
    :param session_end: Timezone aware DateTime Object
    :param location:
    :param mentor_email:
    :param email_cal_title:
    :param email_cal_desc:
    :return:
    """
    if mentor_email:
        guests = [mentor_email]
    else:
        guests = []
    email_link_gcal = gen_google_calendar(
        email_cal_title,
        session_start,
        session_end,
        email_cal_desc,
        location=location,
        guests=guests
    )
    email_cal_desc = email_cal_desc.replace("<br>", "%0A")
    email_cal_desc = BeautifulSoup(email_cal_desc).get_text()
    email_link_yahoo = gen_yahoo_calendar(
        email_cal_title,
        session_start,
        session_end,
        email_cal_desc,
        location=location
    )

    return email_link_gcal, email_link_yahoo


def get_session_type_and_location(session, mentor):
    """
    This is the official way the StartupTree platform renders the
    session type and location for a single-mentorship session (NOT TEAM MENTORSHIP SESSION)
    """
    from mentorship.models import (  # Avoid circular import
        MentorshipSession,
        PREFERRED_CONTACT,
        PHONE,
        SKYPE,
        IN_PERSON,
        TO_BE_DETERMINED,
    )

    session: MentorshipSession
    if session.is_team_session():
        app_logger.error(f'Session ID = {session.id}: get_session_type_and_location() can only be called '
                         f'on single-mentorship sessions, not team-mentorship sessions. Returning None, None')
        return None, None

    meeting_type = session.meeting_type
    session_type_dict = dict(PREFERRED_CONTACT)
    session_type = session_type_dict[meeting_type]
    session_location = None

    if meeting_type == PHONE:
        if session.phone_number:
            session_location = session.phone_number
        elif mentor is not None and mentor.phone:
            session_location = mentor.phone
        elif mentor is not None and mentor.phone_number:
            session_location = mentor.phone_number
    elif meeting_type == SKYPE:
        if session.video_id:
            session_location = session.video_id
        elif mentor is not None and mentor.skype:
            session_location = mentor.skype
    elif meeting_type == IN_PERSON:
        if session.location and session.location != '':
            session_location = session.location
        elif mentor is not None and mentor.default_location:
            session_location = mentor.default_location
    elif meeting_type == TO_BE_DETERMINED:
        session_location = '(to be determined)'
    else:
        app_logger.error(f'Session ID {session.id}: Invalid session.meeting_type: {session.meeting_type}')

    return session_type, session_location


def get_session_contact_info(session_type, session_location):
    if not session_location:
        session_location = 'Location not specified.'
    session_location = str(session_location)
    if session_type == "Phone" or session_type == "Video Call":
        contact_info = session_location
    elif session_type == "StartupTree Messaging":
        contact_info = "Message them on StartupTree"
    else:
        contact_info = session_location

    return contact_info
