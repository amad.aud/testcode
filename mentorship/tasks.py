from datetime import datetime
from forms.models import Answer
from html import unescape

from django.contrib.sites.models import Site
from django.db import transaction
from django.db.models import Q
from django.template.loader import render_to_string, get_template
from django.urls import reverse
from django.utils import timezone

from StartupTree.loggers import rq_logger, app_logger
from branch.templatetags.filters import list_comma
from branch.models import StartupMember, Startup
from emailuser.conf import (
    PROTOCOL,
    EMAIL_INVITATION_URL
)
from emailuser.engine import render_send_email
from emailuser.models import UserEmail
from forms.serializer.forms import custom_meeting_dict
from hamster.config import Roborovski
from mentorship import cronofy_cache
from mentorship.models import (
    Mentor,
    Mentorship,
    MentorshipSession,
    MentorshipSessionSet,
    MentorshipRequest,
    TeamMentorshipSession,
    MentorshipEmailTemplate,
)
from mentorship.strings import SESSION_CREATED, SESSION_CANCELLED, SESSION_CHANGED
from mentorship.utils import email_link_generator_m, get_session_type_and_location, \
    get_session_contact_info
from mentorship.st_cronofy import is_cronofy_linked, get_cronofy_calendar_id, \
    cronofy_publish_to_calendar, cronofy_delete_from_calendar
from StartupTree.utils import QuerySetType, custom_variable_extractor, get_media_url, get_static_url
from trunk.models import (
    AccessLevel,
    AdminCalSyncOption,
    University,
    UniversityStaff,
    UniversityStyle,
)
from trunk.utils import format_datetime_english
from trunk.utils.default_mentorship_emails import (
    get_new_session_mentee_email_body,
    get_new_session_mentor_email_body,
    get_session_reminder_mentee_email_body,
    get_session_reminder_mentor_email_body
)
from venture.models import MemberToAdmin


def get_mentor_word_and_article(university, team_mentor):
    mentor_word = university.style.mentor_word
    if team_mentor:
        mentor_word = 'team ' + mentor_word
    if mentor_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        article = 'an'
    else:
        article = 'a'

    return mentor_word, article


def mentorship_rec_noti(email, ment, university, request, is_mentor, rec_msg, ment_url):
    if is_mentor:
        transaction.on_commit(
            lambda: send_mentorship_recommendation.delay(
                email,
                ment.profile.get_full_name(),
                university.site_id,
                request.session.get('user_name'),
                university.name,
                is_mentor=True,
                rec_msg=rec_msg,
                mentee_url=ment_url))
    else:
        transaction.on_commit(
            lambda: send_mentorship_recommendation.delay(
                email,
                ment.get_full_name(),
                university.site_id,
                request.session.get('user_name'),
                university.name,
                is_mentor=False,
                rec_msg=rec_msg,
                mentor_url=ment_url))


def mentor_mentorship_accepted_noti(m_request, university):
    transaction.on_commit(lambda: mentorship_notify_mentor.delay(
        m_request.requested_mentorship.student_id,
        university.site_id,
        m_request.requested_mentorship.id,
        m_request.id))


def mentee_mentorship_decision_noti(m_request, university):
    transaction.on_commit(lambda: mentorship_notify_student.delay(
        m_request.requested_mentorship.student_id,
        university.site_id,
        m_request.requested_mentorship.id,
        m_request.id))


def uni_mentor_request_noti(student_profile, site):
    transaction.on_commit(lambda: send_mentor_request_noti.delay(
        student_profile.get_full_name(),
        site.id))


def session_change_noti_mentor(session, current_site_id, action, admin_decision=False):
    """
    In addition to sending email notifications to mentors,
    this method updates cronofy-synced mentor calendars.

    Furthermore, if an admin has opted to sync their calendar for all mentorship meetings,
    then those admins' calendars will get updated too.

    Note:
    The parameter admin_decision is poorly named.
        True means that the session change is due to mentor approving/declining a pending meeting
        and/or admin approving/declining a pending meeting.

        False means that the session change is due to mentee action.

    Note:
        Usually, session is of type MentorshipSession. A special case is when session is of type MentorshipSessionSet,
        in which case this method is called to notify the mentor that all pending sessions of that set bave been declined
        (eg when mentee cancels their multi-slot meeting request).
    """
    is_team = isinstance(session, TeamMentorshipSession)

    if isinstance(session, MentorshipSessionSet):
        # Special Case: Mentor has declined all pending sessions in this set.
        transaction.on_commit(lambda: notify_mentor_times_declined.delay(
            session.id, current_site_id))
    else:
        state = action
        transaction.on_commit(lambda: notify_session_change_mentor.delay(
            session.id, action, admin_decision, current_site_id, is_team))

        # Update mentor cronofy-linked calendars
        if not is_team:
            mentor_user = session.mentorship.mentor.profile.user
            if is_cronofy_linked(mentor_user):
                session_change_cronofy_mentor(
                    session, current_site_id, state, mentor_user)
        else:
            for mentorship in session.mentorships.all():
                mentor_user = mentorship.mentor.profile.user
                if is_cronofy_linked(mentor_user):
                    session_change_cronofy_mentor(
                        session, current_site_id, state, mentor_user, mentorship=mentorship)

        # Update admin cronofy-linked calendars
        uni = University.objects.get(site_id=current_site_id)
        session_change_cronofy_admins__async(uni, session, action)


def session_change_noti_mentee(session, current_site_id, action, admin_decision=False):
    """
    Note:
    The parameter admin_decision is poorly named.
        True means that the session change is due to mentor approving/declining a pending meeting
        and/or admin approving/declining a pending meeting.

        False means that the session change is due to mentee action.
    """
    is_team = isinstance(session, TeamMentorshipSession)
    if isinstance(session, MentorshipSessionSet):
        transaction.on_commit(lambda: notify_mentee_times_declined.delay(
            session.id, current_site_id))
    else:
        transaction.on_commit(lambda: notify_session_change_mentee.delay(
            session.id, action, admin_decision, current_site_id, is_team))


def session_request_noti_uni(session: MentorshipSession, current_site_id):
    transaction.on_commit(lambda: send_session_request_noti_to_uni.delay(
        session.id, current_site_id))


def session_request_noti_mentor(mentorship, session, current_site_id, is_multiple, meeting_topic):
    if is_cronofy_linked(mentorship.mentor.profile.user):
        session_request_cronofy(mentorship, session,
                                current_site_id, is_multiple)
    transaction.on_commit(lambda: send_session_request_noti_to_mentor.delay(
        session.id, mentorship.student.get_full_name(),
        mentorship.mentor.id,
        current_site_id,
        is_multiple,
        meeting_topic))


def team_session_changed_noti_admin(team_session: TeamMentorshipSession, platform: University, state):
    """Sends a notification to platform admin that a team mentorship session has been scheduled or cancelled
    Requires platform.style.admin_notify_of_team_meeting_scheduled
    """
    uni_style: UniversityStyle = platform.style
    if state == SESSION_CREATED and not uni_style.admin_notify_of_team_meeting_scheduled:
        app_logger.error(
            'Precondition fail: platform.style.admin_notify_of_team_meeting_scheduled is false')
        return
    elif state == SESSION_CANCELLED and not uni_style.admin_notify_of_team_meeting_cancelled:
        app_logger.error(
            'Precondition fail: platform.style.admin_notify_of_team_meeting_cancelled is false')
        return

    transaction.on_commit(lambda: send_team_session_changed_noti_admin.delay(
        team_session.id, platform.id, state=state))


@Roborovski.task
def check_upcoming_mentorship_sessions():
    """
    Runs on a daily basis.

    Looks for upcoming mentorship sessions and sends out a reminder for meetings that are
    within one or two days in the future.

    Of those sessions that are pending approval from mentor, a reminder is sent to the mentor within.
    """
    pending_sets = set()
    for session in MentorshipSession.objects.filter(datetime__gte=timezone.now(),
                                                    is_cancelled=False,
                                                    is_cancelled_by_student=False):
        if MentorshipSessionSet.objects.filter(sessions=session).exists():
            session_set = MentorshipSessionSet.objects.get(sessions=session)
            if not session_set.decided and session_set not in pending_sets:
                pending_sets.add(session_set)
            continue

        delta = session.datetime - timezone.now()

        # check if it is teammentorshipsession
        if not session.is_team_session():
            mentor = session.mentorship.mentor
            current_site = mentor.platform.site

            if delta.days == 1:
                if session.needs_mentor_approval() and not session.is_mentor_accepted:
                    send_session_reminder_to_mentor.delay(
                        current_site.id, session.id, True)
                if session.needs_admin_approval() and not session.is_admin_accepted:
                    send_session_reminder_to_uni.delay(
                        session.id, current_site.id)
                if session.is_mentor_accepted and session.is_admin_accepted:
                    send_session_reminder_to_mentor.delay(
                        current_site.id, session.id, False)
                    send_session_reminder_to_mentee.delay(
                        current_site.id, session.id)
            elif delta.days == 2:
                if session.needs_mentor_approval() and not session.is_mentor_accepted:
                    send_session_reminder_to_mentor.delay(
                        current_site.id, session.id, True)
                if session.needs_admin_approval() and not session.is_admin_accepted:
                    send_session_reminder_to_uni.delay(
                        session.id, current_site.id)
        else:
            # For Team mentorship
            current_site = session.teammentorshipsession.mentorships.all()[
                0].mentor.platform.site

            if delta.days == 1 or delta.days == 2:
                send_session_reminder_to_mentor.delay(
                    current_site.id, session.id, False, is_team=True)
                send_session_reminder_to_mentee.delay(
                    current_site.id, session.id, is_team=True)

    for session_set in pending_sets:

        # Currently, all session sets are for single-mentorship.
        # When team mentorship session sets are implemented, we will need to convert this into a proper check.
        is_team_mentorship_set = False

        if not is_team_mentorship_set:
            mentor = session_set.mentorship.mentor
            current_site = mentor.platform.site
            first_session = session_set.sessions.first()
            delta = first_session.datetime - timezone.now()
            if delta.days == 1 or delta.days == 2:
                if not first_session.is_mentor_accepted:
                    send_session_reminder_to_mentor.delay(
                        current_site.id, session_set.id, True, True)
        else:
            # for teammentorship
            # currently, no multipled timeslot for TeamMentorship
            pass


@Roborovski.task
def check_recent_mentorship_sessions():
    # This loop iterates over single-mentorship AND team mentorship sessions.
    for session in MentorshipSession.objects.filter(datetime__lt=timezone.now(),
                                                    is_cancelled=False,
                                                    is_cancelled_by_student=False,
                                                    is_mentor_accepted=True):
        delta = timezone.now() - session.get_endtime()
        minutes_since_meeting = delta.total_seconds() / 60
        if 0 < minutes_since_meeting <= 15:
            send_session_followup_to_mentor.delay(session.id)
            send_session_followup_to_mentee.delay(session.id)


@Roborovski.task
def send_mentor_invitation(emailuser_id, name='', site_id=None,
                           invitor="StartupTree",
                           university="StartupTree",
                           program_name=None,
                           is_existing=False, team_mentor=False):
    """Send invitation email to the email user who was just created by
    someone else on the platform.
    @TODO
    :param eu:
    :param name:
    :param site:
    :param invitor:
    :param university:
    :param program_name:
    :return:
    """
    eu = UserEmail.objects.get(id=int(emailuser_id))
    email = eu.email.lower()
    # rq_logger.debug("Start sending invitation.")
    if site_id is None:
        current_site = Site.objects.get(id=1)
    else:
        current_site = Site.objects.get(id=int(site_id))
    uni = University.objects.get(site=current_site)
    is_admin = False
    if invitor == university:
        is_admin = True
        if program_name:
            invitor = program_name

    invite_email_template = None
    email_body = None
    if MentorshipEmailTemplate.objects.filter(platform=uni).exists():
        invite_email_template = MentorshipEmailTemplate.objects.filter(
            platform=uni).first()
        email_body = invite_email_template.email_body

    protocol = PROTOCOL
    ctd = {
        "email": email,
        "year": timezone.now().year,
        "mentor_first_name": name,
        "uni_name": university,
        "team_mentoring": team_mentor,
        "email_body": email_body
    }

    mentor_word, article = get_mentor_word_and_article(uni, team_mentor)

    if not is_existing:
        tmp = eu.generate_confirmation_key()
        url = "{0}://{1}/{2}&type=mentor".format(
            protocol,
            current_site.domain,
            EMAIL_INVITATION_URL.format(eu.email, tmp)
        )
        eu.set_password(tmp)
        template = 'email_templates/mentorship/Mentor_Invite_to_join_StartupTree.html'
        if invite_email_template and invite_email_template.subject:
            subject = invite_email_template.subject
        else:
            subject = "{0} is inviting you to be {1} {2} on StartupTree".format(
                program_name, article, mentor_word)
        ctd['invite_url'] = url
    else:
        url = f"{uni.get_url()}/{reverse('mentor-signup')}"
        template = 'email_templates/mentorship/Mentor_Invite_for_Existing_Users.html'
        ctd['mentor_setting_url'] = url
        if invite_email_template and invite_email_template.subject:
            subject = invite_email_template.subject
        else:
            subject = "You have been added as {0} {1}".format(
                article, mentor_word)
    ctd['subject'] = subject

    render_send_email(subject, template, ctd, current_site,
                      eu.email, force_send=True)
    eu.confirmation_sent = timezone.now()
    eu.save()
    # rq_logger.debug("Sent Mentor invitation.")


@Roborovski.task
def notify_admin_team_removed(request, team, mentor):
    current_site = request.current_site
    uni = University.objects.get(site=current_site)
    admin_emails = [staff.admin.email for staff in UniversityStaff.objects.filter(
        university=uni, is_super=True)]
    mentor_full_name = mentor.profile.full_name

    subject = mentor_full_name + " has removed " + \
        str(team) + " as a team mentee"
    template = 'email_templates/mentorship/Team_Removed_By_Mentor.html'
    ctd = {
        "team": str(team),
        "mentor_full_name": mentor_full_name
    }

    for admin_email in admin_emails:
        render_send_email(subject, template, ctd, current_site,
                          admin_email, is_admin_email=True)
    return True


@Roborovski.task
def notify_mentor_team_mentor_removed(request, team, mentor):
    current_site = request.current_site
    mentor_first_name = mentor.profile.first_name

    subject = str(team) + " has removed you as a team mentor"
    template = 'email_templates/mentorship/Team_Mentor_Removed.html'
    ctd = {
        "team": str(team),
        "mentor_first_name": mentor_first_name
    }

    render_send_email(subject, template, ctd, current_site,
                      mentor.profile.user.email, is_mentorship_email=True)
    return True


@Roborovski.task
def notify_members_team_mentor_removed(request, team, mentor):
    current_site = request.current_site
    initiator = request.current_member.full_name
    mentor_full_name = mentor.profile.full_name

    staff = [
        admin.member for admin in MemberToAdmin.objects.filter(startup=team)]

    subject = mentor_full_name + " is no longer a team mentor for " + str(team)
    template = 'email_templates/mentorship/Team_Mentor_Removed_toMembers.html'
    ctd = {
        "team": str(team),
        "mentor_full_name": mentor_full_name,
        "initiator": initiator
    }
    for member in staff:
        ctd["member_first_name"] = member.first_name
        member_email = member.user.email
        render_send_email(subject, template, ctd, current_site,
                          member_email, is_mentorship_email=True)
    return True


@Roborovski.task
def remind_team_session_attendants(request, session_id):
    current_site = request.current_site

    session = TeamMentorshipSession.objects.get(id=int(session_id))
    session_date = datetime.strftime(
        session.get_datetime().date(), '%b %d, %Y')
    session_time = datetime.strftime(session.get_datetime(), '%H:%M %p')
    session_tz = session.get_timezone()

    team = session.team
    staff = MemberToAdmin.objects.filter(startup=team)
    founder = staff.first()
    founder_email = founder.member.user.email

    subject = "REMINDER: You have an upcoming team mentorship session with " + \
        str(team)
    template = 'email_templates/mentorship/Team_Session_Remind.html'
    ctd = {
        "team": str(team),
        "founder_email": founder_email,
        "session_date": session_date,
        "session_time": session_time,
        "session_timezone": session_tz
    }
    meeting_type = session.meeting_type
    ctd["meeting_type"] = meeting_type
    if meeting_type is 4:
        ctd["meeting_location"] = session.location

    for admin in staff:
        ctd["member_first_name"] = admin.member.first_name
        render_send_email(subject, template, ctd, current_site,
                          admin.member.user.email, is_mentorship_email=True)

    mentors = [
        mentorship.mentor.profile for mentorship in session.mentorships.iterator()]
    for mentor in mentors:
        ctd["member_first_name"] = mentor.first_name
        render_send_email(subject, template, ctd, current_site,
                          mentor.user.email, is_mentorship_email=True)
    return True


@Roborovski.task
def team_session_change_notify_venture(current_user_id, current_site_id, session_id, change: str):
    current_site = Site.objects.get(id=int(current_site_id))
    current_profile = StartupMember.objects.get_user(
        user=UserEmail.objects.get(id=current_user_id))

    session = TeamMentorshipSession.objects.get(id=int(session_id))
    session_date = datetime.strftime(
        session.get_datetime().date(), '%b %d, %Y')
    session_time = datetime.strftime(session.get_datetime(), '%H:%M %p')
    session_tz = session.get_timezone()
    cancel_reason = session.cancel_reason

    team = session.team
    staff = MemberToAdmin.objects.filter(startup=team)
    founder = staff.first()
    founder_email = founder.member.user.email

    if change == "cancel":
        subject = current_profile.full_name + \
            " has cancelled an upcoming team meeting for " + str(team)
        template = 'email_templates/mentorship/Team_Session_Cancelled_Members.html'
    elif change == "auto cancel":
        subject = "An upcoming team meeting for " + \
            str(team) + " has been cancelled due to lower attendee count"
        template = 'email_templates/mentorship/Team_Session_Cancelled_Members.html'
    elif change == "drop":
        subject = current_profile.full_name + \
            " will not be attending an upcoming team meeting for " + str(team)
        template = 'email_templates/mentorship/Team_Session_Dropped_Members.html'
    elif change == "scheduled":
        subject = current_profile.full_name + \
            " has scheduled a team meeting for " + str(team)
        template = 'email_templates/mentorship/Team_Session_Scheduled_Members.html'

    ctd = {
        "team": str(team),
        "initiator": current_profile.full_name,
        "initiator_email": current_profile.user.email,
        "founder_email": founder_email,
        "session_date": session_date,
        "session_time": session_time,
        "session_timezone": session_tz,
    }

    if (change == "cancel" or change == "auto cancel"):
        ctd["cancel_reason"] = cancel_reason

    if (change == "scheduled"):
        meeting_type = session.meeting_type
        ctd["meeting_type"] = meeting_type
        if meeting_type is 4:
            ctd["meeting_location"] = session.location

    for admin in staff:
        ctd["member_first_name"] = admin.member.first_name
        render_send_email(subject, template, ctd, current_site,
                          admin.member.user.email, is_mentorship_email=True)
    return True


@Roborovski.task
def team_session_change_notify_mentor(current_user_id, current_site_id, session_id, change: str):
    current_site = Site.objects.get(id=int(current_site_id))
    current_profile = StartupMember.objects.get_user(
        user=UserEmail.objects.get(id=current_user_id))

    session = TeamMentorshipSession.objects.get(id=int(session_id))
    session_date = datetime.strftime(
        session.get_datetime().date(), '%b %d, %Y')
    session_time = datetime.strftime(session.get_datetime(), '%H:%M %p')
    session_tz = session.get_timezone()
    cancel_reason = session.cancel_reason

    team = session.team
    staff = MemberToAdmin.objects.filter(startup=team)
    founder = staff.first()
    founder_email = founder.member.user.email

    mentors = [mentorship.mentor for mentorship in session.mentorships.iterator()]

    if change == "cancel":
        subject = current_profile.full_name + \
            " has cancelled an upcoming team meeting for " + str(team)
        template = 'email_templates/mentorship/Team_Session_Cancelled_Members.html'
    elif change == "auto cancel":
        subject = "An upcoming team meeting for " + \
            str(team) + " has been cancelled due to lower attendee count"
        template = 'email_templates/mentorship/Team_Session_Cancelled_Members.html'
    elif change == "drop":
        subject = current_profile.full_name + \
            " will not be attending an upcoming team meeting for " + str(team)
        template = 'email_templates/mentorship/Team_Session_Dropped_Members.html'
    elif change == "scheduled":
        subject = current_profile.full_name + \
            " has scheduled a team meeting for " + str(team)
        template = 'email_templates/mentorship/Team_Session_Scheduled_Mentors.html'

    ctd = {
        "team": str(team),
        "initiator": current_profile.full_name,
        "initiator_email": current_profile.user.email,
        "founder_email": founder_email,
        "session_date": session_date,
        "session_time": session_time,
        "session_timezone": session_tz,
    }

    if (change == "cancel" or change == "auto cancel"):
        ctd["cancel_reason"] = cancel_reason

    if (change == "scheduled"):
        meeting_type = session.meeting_type
        ctd["meeting_type"] = meeting_type
        if meeting_type is 4:
            ctd["meeting_location"] = session.location

    for mentor in mentors:
        ctd["member_first_name"] = mentor.profile.first_name
        render_send_email(subject, template, ctd, current_site,
                          mentor.profile.user.email, is_mentorship_email=True)
    return True


# TEAM HAS BEEN MATCHED
@Roborovski.task
def team_mentorship_notify_venture(current_site_id, team_url_id, mentor_email):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    team = Startup.objects.get(university=uni, url_name=team_url_id)
    staff = MemberToAdmin.objects.filter(startup=team)

    mentor = Mentor.objects.get(
        profile__user__email=mentor_email, platform=uni)
    mentor_name = mentor.profile.full_name

    subject = str(uni) + " has matched " + str(team) + \
        " with " + mentor_name + " for Team Mentoring."
    template = 'email_templates/mentorship/Team_Matched.html'
    protocol = PROTOCOL
    url = protocol + "://" + current_site.domain + \
        reverse('visit-team-mentors')
    for admin in staff:
        ctd = {
            "team": str(team),
            "uni": str(uni),
            "mentor": mentor_name,
            "member_first_name": admin.member.first_name,
            "manage_url": url
        }
        render_send_email(subject, template, ctd, current_site,
                          admin.member.user.email, is_mentorship_email=True)
    return True


@Roborovski.task
def team_mentorship_notify_mentor(current_site_id, team_url_id, mentor_email):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    team = Startup.objects.get(university=uni, url_name=team_url_id)
    staff = MemberToAdmin.objects.filter(startup=team)

    mentor = Mentor.objects.get(
        profile__user__email=mentor_email, platform=uni)
    mentor_name = mentor.profile.first_name

    subject = str(uni) + " has matched " + str(team) + \
        " with you for Team Mentoring."
    template = 'email_templates/mentorship/Team_Matched_Mentor.html'
    protocol = PROTOCOL
    url = protocol + "://" + current_site.domain + \
        reverse('visit-team-mentees')
    ctd = {
        "team": str(team),
        "uni": str(uni),
        "mentor_first_name": mentor_name,
        "manage_url": url
    }
    render_send_email(subject, template, ctd, current_site,
                      mentor_email, is_mentorship_email=True)
    return True


# TEAM HAS BEEN ADDED TO TEAM MENTORSHIP FEATURE
@Roborovski.task
def notify_team_add_mentorship_feature(current_site_id, team_url_id):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    team = Startup.objects.get(university=uni, url_name=team_url_id)
    staff = MemberToAdmin.objects.filter(startup=team)

    # EMAIL itself
    subject = str(uni) + " has added " + str(team) + " to Team Mentoring."
    template = 'email_templates/mentorship/Team_Added_to_Mentorship_Feature.html'
    protocol = PROTOCOL
    url = protocol + "://" + current_site.domain + \
        reverse('visit-team-mentors')
    for admin in staff:
        ctd = {
            "team": str(team),
            "uni": str(uni),
            "member_first_name": admin.member.first_name,
            "manage_url": url
        }
        render_send_email(subject, template, ctd, current_site,
                          admin.member.user.email, is_mentorship_email=True)
    return True


def get_template_for_uni(uni_style, email_type):
    """
    Getting saved template according to email type.
    """
    email_body = ''
    if email_type == 'mentorship_session_mentee':
        if uni_style.session_scheduled_mentee_email:
            email_body = uni_style.session_scheduled_mentee_email
        else:
            email_body = get_new_session_mentee_email_body(uni_style)
    elif email_type == 'mentorship_session_mentor':
        if uni_style.session_scheduled_mentor_email:
            email_body = uni_style.session_scheduled_mentor_email
        else:
            email_body = get_new_session_mentor_email_body(uni_style)
    elif email_type == 'session_reminder_mentee':
        if uni_style.session_reminder_mentee_email:
            email_body = uni_style.session_reminder_mentee_email
        else:
            email_body = get_session_reminder_mentee_email_body(uni_style)
    elif email_type == 'session_reminder_mentor':
        if uni_style.session_reminder_mentor_email:
            email_body = uni_style.session_reminder_mentor_email
        else:
            email_body = get_session_reminder_mentor_email_body(uni_style)

    return email_body


def render_custom_email_variables(email_body, session, university):
    """
    This is responsible for replacing all custom variables that follow this regex pattern WORD with
    the appropriate values from the mentorship object passed in.
    """
    mentor = session.mentorship.mentor
    mentor_profile = mentor.profile
    mentee = session.mentorship.student

    mentor_name = mentor_profile.get_full_name()
    mentor_email = mentor_profile.user.email
    mentee_name = mentee.get_full_name()
    mentee_email = mentee.user.email

    meeting_datetime = session.get_datetime_str(mentor)
    meeting_type, meeting_location = get_session_type_and_location(
        session, mentor)
    meeting_topic = session.meeting_topic if session.meeting_topic else "N/A"

    custom_variables = custom_variable_extractor(email_body)

    for var in custom_variables:
        if var == "_mentor_name_":
            email_body = email_body.replace('_mentor_name_', str(mentor_name))
        elif var == "_mentor_email_":
            email_body = email_body.replace(
                '_mentor_email_',  str(mentor_email))
        elif var == "_mentee_name_":
            email_body = email_body.replace('_mentee_name_',  str(mentee_name))
        elif var == "_mentee_email_":
            email_body = email_body.replace(
                '_mentee_email_',  str(mentee_email))
        elif var == "_meeting_datetime_":
            email_body = email_body.replace(
                '_meeting_datetime_',  str(meeting_datetime))
        elif var == "_meeting_type_":
            email_body = email_body.replace(
                '_meeting_type_',  str(meeting_type))
        elif var == "_meeting_location_":
            email_body = email_body.replace(
                '_meeting_location_',  str(meeting_location))
        elif var == "_meeting_topic_":
            email_body = email_body.replace(
                '_meeting_topic_',  str(meeting_topic))
        elif var == "_custom_meeting_questions_":
            answers = Answer.objects.filter(meeting=session)
            # answers = Answer.objects.all()[5:35]
            email_body = email_body.replace('_custom_meeting_questions_', '<p>There are '+str(answers.count()) + ' question types: Multiple Choice, Dropdown, Text Response, File Upload, Date Picker</p>\
            <br><ul>_custom_meeting_questions_</ul></br>')
            for answer in answers:
                if answer.q.q_type == 1:
                    email_body = email_body.replace('_custom_meeting_questions_', '<li style="font-weight:600">'+answer.q.text+': mcq_ans </li>\
                    _custom_meeting_questions_')
                    choices = answer.choices.all()
                    for mcq in choices:
                        email_body = email_body.replace('mcq_ans', ' <span style="font-weight:400">'+mcq.text+' </span>\
                    mcq_ans')
                    email_body = email_body.replace('mcq_ans', '')
                elif answer.q.q_type == 3:
                    email_body = email_body.replace('_custom_meeting_questions_', '<li style="font-weight:600">'+answer.q.text+': <span  style="font-weight:400">'+answer.text+'</span></li>\
                    _custom_meeting_questions_')
                elif answer.q.q_type == 2:
                    email_body = email_body.replace('_custom_meeting_questions_', '<li  style="font-weight:600">'+answer.q.text + ': mcq_ans </li>\
                    _custom_meeting_questions_')
                    choices = answer.choices.all()
                    for mcq in choices:
                        email_body = email_body.replace('mcq_ans', ' <span  style="font-weight:400">'+mcq.text+' </span>\
                    mcq_ans')
                    email_body = email_body.replace('mcq_ans', '')
                elif answer.q.q_type == 4:
                    email_body = email_body.replace('_custom_meeting_questions_', '<li style="font-weight:600">'+answer.q.text + ': <span  style="font-weight:400">'+str(answer.date)+'</span></li>\
                    _custom_meeting_questions_')
                elif answer.q.q_type == 5:
                    link = "/questions/attached/{}".format(answer.id)
                    email_body = email_body.replace('_custom_meeting_questions_', '<li style="font-weight:600">'+answer.q.text + ': <a target="_blank" href='+link+'>File Link</a></li>\
                    _custom_meeting_questions_')

            email_body = email_body.replace('_custom_meeting_questions_', '')
    return email_body


@Roborovski.task
def mentorship_notify_student(student_sm_id, current_site_id, mentorship_id, m_request_id):
    """
    Send student (mentee) about change in status of his/her mentorship.
    :param student_sm_id:
    :param current_site_id:
    :param mentorship_id:
    :param m_request_id:
    :return:
    """
    student = StartupMember.objects.get(id=int(student_sm_id))
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    mentorship = Mentorship.objects.get(id=int(mentorship_id))
    m_request = MentorshipRequest.objects.get(id=int(m_request_id))

    protocol = PROTOCOL
    url = "{0}://{1}/{2}".format(
        protocol,
        current_site.domain,
        "mentorship/mentee-dashboard"
    )
    if m_request.is_admin_accepted is None:
        raise ValueError('Request cannot be empty.')
    mentor_full_name = mentorship.mentor.profile.get_full_name()
    mentee_first_name = student.get_first_name()

    mentor_contact = mentorship.mentor.profile.email

    mentor_profile_photo = "{0}://{1}{2}".format(
        protocol,
        current_site.domain,
        str(mentorship.mentor.profile.get_image())
    )
    if mentorship.mentor.profile.get_image() is None:
        mentor_profile_photo = False

    if mentor_contact is None or mentor_contact == "":
        mentor_contact = mentorship.mentor.profile.user.email
    if mentor_contact is None or mentor_contact == "":
        mentor_contact = False
    student_email = student.user.email
    if m_request.is_admin_accepted:
        is_approved = True
        reject_reason = None
    else:
        is_approved = False
        reject_reason = m_request.admin_reject_reason

    preferred_contact = mentorship.mentor.get_preferred_contact()

    if preferred_contact == "Phone":
        contact_info = preferred_contact + ': ' + \
            str(mentorship.mentor.profile.phone)
    elif preferred_contact == "StartupTree Messaging":
        contact_info = "Message me on StartupTree"
    elif preferred_contact == "Video Call":
        contact_info = preferred_contact + ': ' + \
            str(mentorship.mentor.profile.skype)
    elif preferred_contact == "In Person":
        contact_info = "Address: " + str(mentorship.mentor.profile.address)

    mentor_word = uni.style.mentor_word

    if is_approved:
        subject = "Your {0} request for {1} is now approved!".format(
            mentor_word, mentor_full_name)
    else:
        subject = "Your {0} request has been declined".format(mentor_word)
    template = 'email_templates/mentorship/Mentor_Request_Approved.html'

    ctd = {
        "schedule_url": url,
        "mentee_first_name": mentee_first_name,
        "mentor_full_name": mentor_full_name,
        "mentor_contact": mentor_contact,
        "year": timezone.now().year,
        "mentor_profile_photo": mentor_profile_photo,
        "is_approved": is_approved,
        "reject_reason": reject_reason,
        "subject": subject
    }

    ctd['contact_info'] = contact_info
    ctd['preferred_contact'] = preferred_contact

    render_send_email(subject, template, ctd, current_site,
                      student_email, is_mentorship_email=True)
    rq_logger.debug("Notified student.")
    return True


@Roborovski.task
def mentorship_notify_mentor(student_sm_id, current_site_id, mentorship_id, m_request_id):
    """Send mentor notification about new student (mentee) being assigned.
    :param student_sm_id:
    :param current_site_id:
    :param mentorship_id:
    :param m_request_id:
    :return:
    """
    protocol = PROTOCOL
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)

    student = StartupMember.objects.get(id=int(student_sm_id))
    mentee_profile = "{0}://{1}{2}".format(
        protocol,
        current_site.domain,
        str(student.get_url())
    )
    mentee_profile_photo = "{0}://{1}{2}".format(
        protocol,
        current_site.domain,
        str(student.get_image())
    )
    if student.get_image() is None:
        mentee_profile_photo = False

    mentorship = Mentorship.objects.get(id=int(mentorship_id))
    m_request = MentorshipRequest.objects.get(id=int(m_request_id))

    mentee_url = "{0}://{1}/mentorship/mentor-dashboard".format(
        protocol,
        current_site.domain
    )
    scheduling_url = "{0}://{1}/mentorship/settings".format(
        protocol,
        current_site.domain
    )
    if not m_request.is_admin_accepted:
        raise ValueError('Request must be True.')
    user = mentorship.mentor.profile.user

    mentor_first_name = mentorship.mentor.profile.first_name
    mentee_full_name = student.get_full_name()
    mentee_word = uni.style.mentee_word

    student_email = student.user.email

    if student.email is not None:
        student_email = student.email
    if student.user.email is not None:
        student_email = student.user.email
    else:
        student_email = 'error'

    subject = mentee_full_name + \
        " has been approved to be your {0}!".format(mentee_word)
    template = 'email_templates/mentorship/Mentee_Assigned.html'

    ctd = {
        "mentee_url": mentee_url,
        "scheduling_url": scheduling_url,
        "mentee_profile": mentee_profile,
        "mentee_profile_photo": mentee_profile_photo,
        "mentor_first_name": mentor_first_name,
        'mentee_full_name': mentee_full_name,
        "request": m_request,
        "year": timezone.now().year,
        "subject": subject,
        'mentee_email': student_email,
    }

    render_send_email(subject, template, ctd, current_site,
                      user.email, is_mentorship_email=True)
    rq_logger.debug("Notified Mentor.")

    return True


def mentor_make_cronofy_event_details(uni, session, is_pending_meeting_block=False, mentorship=None):
    """ Supply event details as needed by cronofy, for adding mentor session
    to mentor's cronofy-synced personal calendar.

    If is_pending_meeting_block, then event title and description will be adjusted accordingly,
    as it will imply that this is part of a set of pending meeting blocks.

    mentorship is required if team mentorship session.
    """
    mentorship_word = uni.style.mentorship_word
    mentee_word = uni.style.mentee_word

    is_team = isinstance(session, TeamMentorshipSession)

    if is_team:
        mentor = mentorship.mentor
        student = mentorship.team
        cronofy_event_timezone = session.timezone
        student_full_name = student.name
        student_emails = session.members_attending.all().values_list('email', flat=True)

        desc_name = '{0} Name: {1}'.format(mentee_word, student_full_name)
        desc_email = '{0} Emails: {1}'.format(
            mentee_word.title(), (',').join(student_emails))

        session_type, session_location = get_session_type_and_location(
            session, None)
    else:
        mentor = session.mentorship.mentor
        student = session.mentorship.student
        cronofy_event_timezone = mentor.timezone

        student_full_name = student.get_full_name()
        student_email = student.user.email

        desc_name = '{0} Name: {1}'.format(mentee_word, student_full_name)
        desc_email = '{0} Email Address: {1}'.format(
            mentee_word.title(), student_email)

        session_type, session_location = get_session_type_and_location(
            session, mentor)
    contact_info = get_session_contact_info(session_type, session_location)

    if not is_pending_meeting_block:
        cronofy_event_title = '{0} Session With {1}'.format(
            mentorship_word.title(), student_full_name)
        cronofy_event_description = 'You have a new {0} session scheduled.\n{1}\n{2}'.format(mentorship_word,
                                                                                             desc_name.title(),
                                                                                             desc_email)
    else:
        cronofy_event_title = 'Pending Review {0} Meeting with {1}'.format(
            mentorship_word.title(), student_full_name)
        cronofy_event_description = (
            '{0} has proposed this time for a {1} meeting. From Pending Meetings on the My {2}s page, ' +
            'Accept or Decline this meeting to confirm or remove this timeslot.').format(
            student_full_name, mentorship_word, mentee_word.title())

    cronofy_event_location = {
        'description': contact_info,
    }

    cronofy_event_details = {
        'title': cronofy_event_title,
        'description': cronofy_event_description,
        'timezone': cronofy_event_timezone,
        'location': cronofy_event_location
    }

    return cronofy_event_details


def admin_make_cronofy_mentorship_meeting_details(uni_staff: UniversityStaff, session: MentorshipSession):
    uni_style: UniversityStyle = uni_staff.university.style

    mentorship_word_title = uni_style.mentorship_word.title()
    mentor_word_title = uni_style.mentor_word.title()

    if session.is_team_session():
        team_session: TeamMentorshipSession = session.teammentorshipsession
        team_name = team_session.team.name
        mentor_names = map(
            lambda mentor: mentor.profile.get_full_name(), team_session.get_mentors())
        mentor_names_str = ', '.join(mentor_names)

        cronofy_event_title = f'Team {mentorship_word_title} Meeting w/ {team_name} and {mentor_names_str}'

        cronofy_event_timezone = team_session.timezone

        session_type, session_location = get_session_type_and_location(
            session, None)

        cronofy_event_description = (f'Scheduled Team {mentorship_word_title} Meeting. \n'
                                     f'Team: {team_name}\n'
                                     f'{mentor_word_title}(s): {mentor_names_str}')

    else:
        mentor = session.mentorship.mentor
        mentor_name = mentor.profile.get_full_name()
        mentee_name = session.mentorship.student.get_full_name()

        cronofy_event_title = f'{mentorship_word_title} Meeting w/ {mentor_name} and {mentee_name}'

        cronofy_event_timezone = session.mentorship.mentor.timezone

        session_type, session_location = get_session_type_and_location(
            session, mentor)

        mentee_word_title = uni_style.mentee_word.title()
        cronofy_event_description = (f'Scheduled {mentorship_word_title} Meeting. \n'
                                     f'{mentor_word_title}: {mentor_name}\n'
                                     f'{mentee_word_title}: {mentee_name}')

    cronofy_event_location = {
        'description': get_session_contact_info(session_type, session_location),
    }

    cronofy_event_details = {
        'title': cronofy_event_title,
        'description': cronofy_event_description,
        'timezone': cronofy_event_timezone,
        'location': cronofy_event_location
    }

    return cronofy_event_details


def add_session_to_cronofy(user, session, cronofy_calendar_id, cronofy_event_details, is_refresh_cronofy_cache):
    """
    If this session is being added to a mentor's cronofy calendar (thus impacting the mentor's availability,
    then is_refresh_cronofy_cache should be true).

    If this session is being added to an admin's calendar, then no refresh is needed (is_refresh_cronofy_cache should
    be false)
    """
    cronofy_event = {
        'event_id': session.id,
        'summary': cronofy_event_details['title'],  # The event title
        'description': cronofy_event_details['description'],
        'start': session.datetime.isoformat(),
        'end': session.get_raw_endtime(),
        'tzid': cronofy_event_details['timezone'],
        'location': cronofy_event_details['location']
    }

    cronofy_publish_to_calendar(user, cronofy_calendar_id, cronofy_event,
                                refresh_cronofy_cache=is_refresh_cronofy_cache)


def mentor_add_session_to_cronofy(uni, user, session, cronofy_calendar_id=None, is_pending_meeting_block=False,
                                  mentorship=None):
    '''
    Add mentoring session to cronofy calendar.

    cronofy_calendar_id optionally given if previously evaluated.
    If not, re evaluate it (this will cost an cronofy API call).

    If is_pending_meeting_block, then event title and description will be adjusted accordingly.
    '''
    if cronofy_calendar_id is None:
        cronofy_calendar_id = get_cronofy_calendar_id(user)

    cronofy_event_details = mentor_make_cronofy_event_details(uni, session, is_pending_meeting_block,
                                                              mentorship=mentorship)

    add_session_to_cronofy(user, session, cronofy_calendar_id,
                           cronofy_event_details, is_refresh_cronofy_cache=True)


def admin_add_session_to_cronofy(uni_staff: UniversityStaff, session: MentorshipSession):
    """ Applies to both mentorship and team mentorship sessions
    Requires: Admin is cronofy-linked, has proper permissions, and has opted to receive calendar sync for the
    appropriate session type (team or non-team mentorship).
    """
    admin_user = uni_staff.admin
    cronofy_calendar_id = get_cronofy_calendar_id(admin_user)
    cronofy_event_details = admin_make_cronofy_mentorship_meeting_details(
        uni_staff, session)
    add_session_to_cronofy(admin_user, session, cronofy_calendar_id,
                           cronofy_event_details, is_refresh_cronofy_cache=False)


def session_change_cronofy_mentor(session, current_site_id, action, mentor_user, mentorship=None):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    cronofy_calendar_id = get_cronofy_calendar_id(mentor_user)
    if action == SESSION_CREATED:
        mentor_add_session_to_cronofy(
            uni, mentor_user, session, cronofy_calendar_id, mentorship=mentorship)
    elif action == SESSION_CANCELLED:
        cronofy_delete_from_calendar(
            mentor_user, cronofy_calendar_id, session.id, refresh_cronofy_cache=True)
    elif action == SESSION_CHANGED:
        mentor_add_session_to_cronofy(
            uni, mentor_user, session, cronofy_calendar_id, mentorship=mentorship)


def session_change_cronofy_admins__async(uni, session, action):
    transaction.on_commit(
        lambda: session_change_cronofy_admins.delay(uni.id, session.id, action))


@Roborovski.task
def session_change_cronofy_admins(uni_id, session_id, action):
    """ Updates the personal calendar of all admins (on relevant platform) who"""
    uni = University.objects.get(id=uni_id)
    session = MentorshipSession.objects.get(id=session_id)

    if session.is_team_session():
        sync_type = AdminCalSyncOption.TEAM_MENTORSHIP_MEETINGS
    else:
        sync_type = AdminCalSyncOption.MENTORSHIP_MEETINGS

    cal_option = AdminCalSyncOption.objects.get(sync_type=sync_type)

    uni_staffs: QuerySetType[UniversityStaff] = UniversityStaff.objects.filter(university_id=uni.id,
                                                                               cal_sync_option=cal_option)

    for uni_staff in uni_staffs:
        admin_user = uni_staff.admin
        if is_cronofy_linked(admin_user):
            if action == SESSION_CREATED:
                admin_add_session_to_cronofy(uni_staff, session)
            elif action == SESSION_CHANGED:
                admin_add_session_to_cronofy(uni_staff, session)
            elif action == SESSION_CANCELLED:
                calendar_id = get_cronofy_calendar_id(admin_user)
                cronofy_delete_from_calendar(
                    admin_user, calendar_id, session.id, refresh_cronofy_cache=False)
            else:
                app_logger.error(
                    f"Invalid action code ({action}) for session_change_cronofy_admins")


@Roborovski.task
def notify_session_change_mentor(session_id, state, admin_decision, current_site_id, is_team=False):
    """
    Note:
    The parameter admin_decision is poorly named.
        True means that the session change is due to mentor approving/declining a pending meeting
        and/or admin approving/declining a pending meeting.

        False means that the session change is due to mentee action.
    """
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    uni_style = UniversityStyle.objects.get(_university=uni)
    try:
        if is_team:
            session = TeamMentorshipSession.objects.select_related(
                'team').get(id=int(session_id))
        else:
            session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                               'mentorship__mentor__profile',
                                                               'mentorship__mentor',
                                                               'mentorship__student__user',
                                                               'mentorship__student') \
                .get(id=int(session_id))
    except MentorshipSession.DoesNotExist:
        raise ValueError("Invalid Session id given - {0}".format(session_id))
    protocol = PROTOCOL
    if is_team:
        url = '{0}://{1}/mentorship/team/mentor-dashboard/{2}'.format(
            protocol,
            current_site.domain,
            session.team.url_name
        )
    else:
        url = '{0}://{1}/mentorship/mentor-dashboard'.format(
            protocol,
            current_site.domain
        )

    mentee_word = uni.style.mentee_word
    mentorship_word = uni.style.mentorship_word

    if is_team:
        session_type, session_location = get_session_type_and_location(
            session, None)
        session_timezone = session.timezone
        mentorships = session.mentorships.all()
    else:
        tmp_mentorship = session.mentorship
        tmp_mentor = tmp_mentorship.mentor
        mentorships = [tmp_mentorship]
        session_timezone = tmp_mentor.get_timezone()
        session_type, session_location = get_session_type_and_location(
            session, tmp_mentor)
    contact_info = get_session_contact_info(session_type, session_location)

    email_starttime = session.get_datetime(user_timezone=session_timezone)
    email_endtime = session.get_endtime(user_timezone=session_timezone)
    session_date = email_starttime.strftime('%b %d')
    session_time = email_starttime.strftime('%H:%M')
    session_end_time = email_endtime.strftime('%H:%M:%S')

    for mentorship in mentorships:
        mentor = mentorship.mentor
        mentor_first_name = mentor.profile.first_name
        mentor_email = mentor.profile.user.email

        if is_team:
            student_full_name = session.team.name
            student_email = ','.join(
                session.members_attending.all().values_list('email', flat=True))
            email_cal_title = 'Team {0} Session With {1}'.format(
                mentorship_word.capitalize(), student_full_name)
        else:
            student = session.mentorship.student
            student_full_name = student.get_full_name()
            student_email = student.user.email

            person_type = student_full_name
            email_cal_title = '{0} Session With {1}'.format(
                mentorship_word.capitalize(), person_type)
        email_cal_desc = ""
        if uni_style.session_scheduled_mentor_email is not None:
            email_cal_desc = uni_style.session_scheduled_mentor_email.replace(
            "&nbsp;", " ")
        email_cal_desc = render_custom_email_variables(
            email_cal_desc, session, uni)

        # gcal_link, yahoo_link = email_link_generator_m(email_starttime,
        #                                                email_endtime,
        #                                                contact_info,
        #                                                mentor_email,
        #                                                email_cal_title,
        #                                                email_cal_desc)
        gcal_link = "{0}://{1}/calendar/{2}/{3}".format(
            protocol,
            current_site.domain, 'google', session.id)
        yahoo_link = "{}/calendar/{}/{}".format(
            'https://cornell.startuptreetest.co', 'yahoo', session.id)

        ics_download_link = "{0}://{1}/mentorship/session/{2}/ics_export".format(
            protocol,
            current_site.domain,
            session.id
        )

        ctd = {
            "session_id": session.id,
            "current_site": current_site,
            "session_url": url,
            "site_name": current_site.name,
            "admin_decision": admin_decision,
            "session_date": session_date,
            'session_time': session_time,
            'session_end_time': session_end_time,
            "session_datetime": email_starttime.isoformat(),
            'session_timezone': session_timezone,
            "year": timezone.now().year,
            'session_type': session_type,
            'session_location': session_location,
            'mentor_first_name': mentor_first_name,
            'mentee_full_name': student_full_name,
            'mentee_email': student_email,
            'email_link_gcal': gcal_link,
            'email_link_yahoo': yahoo_link,
            'ics_download_link': ics_download_link
        }

        if state == SESSION_CREATED:
            ##############################
            # send session created email #
            ##############################
            if admin_decision:
                subject = "APPROVED: Your {0} session with {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
            else:
                subject = "CONFIRMED: Your {0} session with {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)

            template = "email_templates/mentorship/New_Mentor_Session_Scheduled_to_Mentor.html"

            ctd['meeting_topic'] = session.meeting_topic
            ctd['mentor_meeting_custom'] = custom_meeting_dict(session)
            ctd['url_domain'] = current_site.university.get_url()

            has_custom_email = uni_style.session_scheduled_mentor_email is not None and \
                uni_style.session_scheduled_mentor_email.strip() != ''
            if has_custom_email:
                email_body = uni_style.session_scheduled_mentor_email
                rendered_email_body = render_custom_email_variables(
                    email_body, session, uni)
            else:
                rendered_email_body = None

            ctd['has_custom_email'] = has_custom_email
            ctd['email_body'] = rendered_email_body

            render_send_email(subject, template, ctd, current_site,
                              mentor_email, is_mentorship_email=True)
            rq_logger.debug(
                "Sent session created to mentor - {0}.".format(session.id))
        elif state == SESSION_CANCELLED:
            ###########################
            # Send cancellation email #
            ###########################
            if admin_decision:
                subject = "DECLINED: Your {0} session with {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
            else:
                subject = "CANCELLED: Your {0} session with {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
            template = "email_templates/mentorship/Mentor_Session_Cancelled_to_Mentor.html"

            ctd['cancel_reason'] = session.cancel_reason

            render_send_email(subject, template, ctd, current_site,
                              mentor_email, is_mentorship_email=True)
            rq_logger.debug(
                "Sent session cancelled to mentor - {0}.".format(session.id))
        elif state == SESSION_CHANGED:
            ############################################
            # Send notification of new session details #
            ############################################

            subject = "Details for your {0} session with {1} have changed".format(
                mentorship_word, student_full_name)
            template = "email_templates/mentorship/Mentor_Session_Changed_to_Mentor.html"
            render_send_email(subject, template, ctd, current_site,
                              mentor_email, is_mentorship_email=True)
            rq_logger.debug(
                "Sent session changed to mentor - {0}.".format(session.id))

    return True


@Roborovski.task
def notify_session_change_mentee(session_id, state, admin_decision, current_site_id, is_team=False):
    """
    Note:
    The parameter admin_decision is poorly named.
        True means that the session change is due to mentor approving/declining a pending meeting
        and/or admin approving/declining a pending meeting.

        False means that the session change is due to mentee action.
    """
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    uni_style = UniversityStyle.objects.get(_university=uni)
    session_timezone = uni.style.timezone
    try:
        if is_team:
            session = TeamMentorshipSession.objects.select_related(
                'team').get(id=int(session_id))
        else:
            session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                               'mentorship__mentor__profile',
                                                               'mentorship__mentor',
                                                               'mentorship__student__user',
                                                               'mentorship__student') \
                .get(id=int(session_id))
    except MentorshipSession.DoesNotExist:
        raise ValueError("Invalid Session id given - {0}".format(session_id))
    tmp_mentorship = None
    tmp_mentor = None
    protocol = PROTOCOL
    if is_team:
        url = '{0}://{1}/mentorship/team/mentee-dashboard/{2}'.format(
            protocol,
            current_site.domain,
            session.team.url_name
        )
    else:
        url = '{0}://{1}/mentorship/mentee-dashboard'.format(
            protocol,
            current_site.domain
        )
        tmp_mentorship = session.mentorship
        tmp_mentor = tmp_mentorship.mentor
        team = None

    email_starttime = session.get_datetime(user_timezone=session_timezone)
    email_endtime = session.get_endtime(user_timezone=session_timezone)
    session_date = email_starttime.strftime('%b %d')
    session_time = email_starttime.strftime('%H:%M')
    session_end_time = email_endtime.strftime('%H:%M:%S')

    mentorship_word = uni.style.mentorship_word
    mentor_word, article = get_mentor_word_and_article(uni, False)
    if is_team:
        session_type, session_location = get_session_type_and_location(
            session, None)
        contact_info = get_session_contact_info(session_type, session_location)
        team = session.team
        mentorships = session.mentorships.all()
        """
        for mentee, emails should be sent to mentee, so forloop should just generate
        contexts used for email (list of mentor info)
        Emails sent with bcc.
        """
        mentor_emails = []
        mentor_names = []

        student_emails = []
        for mentorship in mentorships:
            mentor = mentorship.mentor
            mentor_full_name = mentor.profile.get_full_name()
            mentor_email = mentor.profile.user.email
            mentor_emails.append(mentor_email)
            mentor_names.append(mentor_full_name)
        attending_users = session.members_attending.all()
        for attending in attending_users:
            student_email = attending.email
            student_emails.append(student_email)

        mentor_full_name = ','.join(mentor_names)
        student_full_name = team.name
        student_first_name = team.name
        mentor_email = ','.join(mentor_emails)
        student_email = ','.join(student_emails)

        email_cal_title = 'Team {0} Session for {1}'.format(
            mentorship_word.capitalize(), team.name)
        email_cal_desc = ""
        if uni_style.session_scheduled_mentee_email  is not None:
            email_cal_desc = uni_style.session_scheduled_mentee_email.replace(
                "&nbsp;", " ")
        email_cal_desc = render_custom_email_variables(
            email_cal_desc, session, uni)
        for attending in attending_users:
            if is_cronofy_linked(attending):
                cronofy_calendar_id = get_cronofy_calendar_id(attending)
                cronofy_event_title = 'Team {0} Session for {1}'.format(
                    mentorship_word.title(), team.name)
                cronofy_event_description = email_cal_desc
                cronofy_event = {
                    'event_id': session.id,
                    'summary': cronofy_event_title,  # The event title
                    'description': cronofy_event_description,
                    'start': session.datetime.isoformat(),
                    'end': session.get_raw_endtime(),
                    'tzid': session_timezone,
                    'location': contact_info
                }
                if state == SESSION_CREATED:
                    cronofy_publish_to_calendar(attending, cronofy_calendar_id, cronofy_event,
                                                refresh_cronofy_cache=False)
                elif state == SESSION_CANCELLED:
                    cronofy_delete_from_calendar(attending, cronofy_calendar_id, session.id,
                                                 refresh_cronofy_cache=False)
                elif state == SESSION_CHANGED:
                    cronofy_publish_to_calendar(attending, cronofy_calendar_id, cronofy_event,
                                                refresh_cronofy_cache=False)
    else:
        session_type, session_location = get_session_type_and_location(
            session, tmp_mentor)
        contact_info = get_session_contact_info(session_type, session_location)
        mentor = tmp_mentorship.mentor
        mentor_full_name = mentor.profile.get_full_name()
        mentor_email = mentor.profile.user.email

        student = session.mentorship.student
        student_full_name = student.get_full_name()
        student_first_name = student.get_first_name()
        student_email = session.mentorship.student.user.email

        person_type = mentor_full_name
        email_cal_title = '{0} Session With {1}'.format(
            mentorship_word.capitalize(), person_type)
        email_cal_desc = ""
        if uni_style.session_scheduled_mentee_email  is not None:
            email_cal_desc = uni_style.session_scheduled_mentee_email.replace(
                "&nbsp;", " ")
        email_cal_desc = render_custom_email_variables(
            email_cal_desc, session, uni)

    # gcal_link, yahoo_link = email_link_generator_m(email_starttime,
    #                                                email_endtime,
    #                                                contact_info,
    #                                                None,
    #                                                email_cal_title,
    #                                                email_cal_desc)
    gcal_link = "{0}://{1}/calendar/{2}/{3}".format(
        protocol,
        current_site.domain, 'google', session.id)
    yahoo_link = "{}/calendar/{}/{}".format(
        'https://cornell.startuptreetest.co', 'yahoo', session.id)

    ics_download_link = "{0}://{1}/mentorship/session/{2}/ics_export".format(
        protocol,
        current_site.domain,
        session.id
    )
    ctd = {
        "session_id": session.id,
        "current_site": current_site,
        "session_url": url,
        "site_name": current_site.name,
        "admin_decision": admin_decision,
        "session_date": session_date,
        'session_time': session_time,
        'session_end_time': session_end_time,
        "session_datetime": email_starttime.isoformat(),
        'session_timezone': session_timezone,
        "year": timezone.now().year,
        'session_type': session_type,
        'session_location': session_location,
        'mentor_full_name': mentor_full_name,
        'mentee_full_name': student_full_name,
        'mentee_first_name': student_first_name,
        'mentor_email': mentor_email,
        'mentee_email': student_email,
        'email_link_gcal': "/calendar/google/{}".format(session.id),
        'email_link_yahoo': yahoo_link,
        'ics_download_link': ics_download_link
    }

    if state == SESSION_CREATED:

        ##############################
        # send session created email #
        ##############################
        if is_team:
            if admin_decision:
                subject = "APPROVED: Your {0} session for {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
            else:
                subject = "CONFIRMED: Your {0} session for {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
        else:
            if admin_decision:
                subject = "APPROVED: Your {0} session with {1} on {2}".format(
                    mentorship_word, mentor_full_name, session_date)
            else:
                subject = "CONFIRMED: Your {0} session with {1} on {2}".format(
                    mentorship_word, mentor_full_name, session_date)

        ctd['meeting_topic'] = session.meeting_topic
        ctd['contact_info'] = contact_info
        ctd['email_link_gcal_s'] = gcal_link
        ctd['email_link_yahoo_s'] = yahoo_link

        has_custom_email = uni_style.session_scheduled_mentee_email is not None and \
            uni_style.session_scheduled_mentee_email.strip() != ''
        if has_custom_email:
            email_body = uni_style.session_scheduled_mentee_email
            rendered_email_body = render_custom_email_variables(
                email_body, session, uni)
        else:
            rendered_email_body = None
        ctd['has_custom_email'] = has_custom_email
        ctd['email_body'] = rendered_email_body

        template = "email_templates/mentorship/New_Mentor_Session_Scheduled_to_Mentee.html"

        render_send_email(subject, template, ctd, current_site,
                          student_email, is_mentorship_email=True)

    elif state == SESSION_CANCELLED:
        ###########################
        # Send cancellation email #
        ###########################
        if is_team:
            if admin_decision:
                subject = "DECLINED: Your {0} session for {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
            else:
                subject = "CANCELLED: Your {0} session for {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
        else:
            if admin_decision:
                subject = "DECLINED: Your {0} session with {1} on {2}".format(
                    mentorship_word, mentor_full_name, session_date)
            else:
                subject = "CANCELLED: Your {0} session with {1} on {2}".format(
                    mentorship_word, mentor_full_name, session_date)
        template = "email_templates/mentorship/Mentor_Session_Cancelled_to_Mentee.html"

        ctd['cancel_reason'] = session.cancel_reason

        render_send_email(subject, template, ctd, current_site,
                          student_email, is_mentorship_email=True)

        #########################
        # delete calendar event #
        #########################
        # if not admin_decision:
        #     delete_g_calendar_event(session.event_id)
    elif state == SESSION_CHANGED:
        ############################################
        # Send notification of new session details #
        ############################################
        if is_team:
            subject = "Details for your {0} session for {1} have changed".format(
                mentorship_word, student_full_name)
        else:
            subject = "Details for your {0} session with {1} have changed".format(
                mentorship_word, mentor_full_name)
        template = "email_templates/mentorship/Mentor_Session_Changed_to_Mentee.html"
        render_send_email(subject, template, ctd, current_site,
                          student_email, is_mentorship_email=True)
    return True


@Roborovski.task
def notify_mentor_times_declined(set_id, current_site_id):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    try:
        session_set = MentorshipSessionSet.objects.get(id=int(set_id))
    except MentorshipSessionSet.DoesNotExist:
        raise ValueError("Invalid SessionSet id given - {0}".format(set_id))

    mentor = session_set.mentorship.mentor
    mentorship_word = uni.style.mentorship_word
    mentor_first_name = mentor.profile.first_name

    mentor_email = mentor.profile.user.email
    student = session_set.mentorship.student
    student_full_name = student.get_full_name()

    session_dicts = []
    for session in session_set.sessions.all():
        session_dict = {
            'meeting_time': session.get_datetime().strftime('%b %d %H:%M'),
            'timezone': session.get_timezone()
        }
        session_dicts.append(session_dict)
    ctd = {
        'mentor_first_name': mentor_first_name,
        'mentee_full_name': student_full_name,
        'sessions': session_dicts,
        'cancel_reason': session_set.sessions.first().cancel_reason,
        'is_multiple': True
    }

    subject = "DECLINED: Your {0} session request from {1}".format(
        mentorship_word, student_full_name)
    template = "email_templates/mentorship/Mentor_Session_Cancelled_to_Mentor.html"

    mentor_user = mentor.profile.user
    if is_cronofy_linked(mentor_user):
        cronofy_calendar_id = get_cronofy_calendar_id(mentor_user)
        for session in session_set.sessions.all():
            cronofy_delete_from_calendar(
                mentor_user, cronofy_calendar_id, session.id, refresh_cronofy_cache=True)

    render_send_email(subject, template, ctd, current_site,
                      mentor_email, is_mentorship_email=True)


@Roborovski.task
def notify_mentee_times_declined(set_id, current_site_id):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    try:
        session_set = MentorshipSessionSet.objects.get(id=int(set_id))
    except MentorshipSessionSet.DoesNotExist:
        raise ValueError("Invalid SessionSet id given - {0}".format(set_id))

    mentor = session_set.mentorship.mentor
    mentorship_word = uni.style.mentorship_word
    mentor_full_name = mentor.profile.get_full_name()
    student = session_set.mentorship.student
    student_first_name = student.get_first_name()
    student_email = student.user.email

    session_dicts = []
    for session in session_set.sessions.all():
        session_dict = {
            'meeting_time': session.get_datetime().strftime('%b %d %H:%M'),
            'timezone': session.get_timezone()
        }
        session_dicts.append(session_dict)
    ctd = {
        'mentee_first_name': student_first_name,
        'mentor_full_name': mentor_full_name,
        'sessions': session_dicts,
        'cancel_reason': session_set.sessions.first().cancel_reason,
        'is_multiple': True
    }

    subject = "DECLINED: Your {0} session request with {1}".format(
        mentorship_word, mentor_full_name)
    template = "email_templates/mentorship/Mentor_Session_Cancelled_to_Mentee.html"

    render_send_email(subject, template, ctd, current_site,
                      student_email, is_mentorship_email=True)


@Roborovski.task(default_retry_delay=60, max_retries=3)
def send_session_reminder_to_mentor(current_site_id, session_id, needs_approval, is_multiple=False, is_team=False):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    uni_style = UniversityStyle.objects.get(_university=uni)
    session = None
    session_set = None
    if is_multiple:
        try:
            session_set = MentorshipSessionSet.objects.get(id=int(session_id))
        except MentorshipSessionSet.DoesNotExist:
            raise ValueError(
                "Invalid SessionSet id given - {0}".format(session_id))
    else:
        try:
            session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                               'mentorship__mentor__profile',
                                                               'mentorship__mentor',
                                                               'mentorship__student__user',
                                                               'mentorship__student') \
                .get(id=int(session_id))
        except MentorshipSession.DoesNotExist:
            raise ValueError(
                "Invalid Session id given - {0}".format(session_id))
        if is_team:
            session = session.teammentorshipsession
    protocol = PROTOCOL

    mentorships = []
    if is_team:
        url = '{0}://{1}/{2}'.format(
            protocol,
            current_site.domain,
            'mentorship/team/mentor-dashboard'
        )
        mentorships = session.mentorships.all()
    else:
        url = '{0}://{1}/{2}'.format(
            protocol,
            current_site.domain,
            'mentorship/mentor-dashboard'
        )
        if is_multiple and session_set is not None:
            mentorships = [session_set.mentorship]
        elif not is_multiple and session is not None:
            mentorships = [session.mentorship]
    for mentorship in mentorships:
        mentor = mentorship.mentor
        user = mentor.profile.user
        mentor_first_name = mentor.profile.first_name
        mentee_word = uni.style.mentee_word
        mentorship_word = uni.style.mentorship_word
        mentor_word, article = get_mentor_word_and_article(uni, False)

        mentor_email = mentor.profile.user.email
        if is_team:
            student_full_name = mentorship.team.name
            student_email = ''
        else:
            student = mentorship.student
            student_full_name = student.get_full_name()
            student_email = student.user.email

        session_date = None
        if is_multiple:
            session_dicts = []
            for session in session_set.sessions.all():
                session_dict = {
                    'meeting_time': session.get_datetime().strftime('%b %d %H:%M'),
                    'timezone': session.get_timezone(),
                    'location': session.get_location_or_type()
                }
                session_dicts.append(session_dict)
            ctd = {
                'sessions': session_dicts,
                'mentor_first_name': mentor_first_name,
                'mentee_full_name': student_full_name,
                'meeting_topic': session_set.sessions.first().meeting_topic,
                'is_multiple': is_multiple,
                'dashboard_url': url,
            }
        else:
            session_type, session_location = get_session_type_and_location(
                session, mentor)
            session_date = session.get_datetime().strftime('%b %d')
            session_time = session.get_datetime().strftime('%H:%M %p')

            ctd = {
                'session_date': session_date,
                'session_time': session_time,
                'session_timezone': session.get_timezone(),
                'session_type': session_type,
                'session_location': session_location,
                'mentor_phone_number': mentor.phone,
                'mentor_skype': mentor.skype,
                'mentor_first_name': mentor_first_name,
                'mentee_full_name': student_full_name,
                'mentee_email': student_email,
                'meeting_topic': session.meeting_topic,
                'dashboard_url': url,
            }

            if is_cronofy_linked(user):
                if session_type == "Phone":
                    ctd['mentor_phone_number'] = session_location
                elif session_type == "Video Call":
                    ctd['mentor_skype'] = session_location

        has_custom_email = uni_style.session_reminder_mentor_email is not None and \
            uni_style.session_reminder_mentor_email.strip() != ''
        if has_custom_email:
            email_body = uni_style.session_reminder_mentor_email
            rendered_email_body = render_custom_email_variables(
                email_body, session, uni)
        else:
            rendered_email_body = None

        ctd['has_custom_email'] = has_custom_email
        ctd['email_body'] = rendered_email_body

        if needs_approval:
            subject = "Pending approval: Meeting request from {0}".format(
                student_full_name)
            template = "email_templates/mentorship/Session_Approval_Reminder_to_Mentor.html"
        else:
            if session_date:
                subject = "REMINDER: Your {0} session with {1} on {2}".format(
                    mentorship_word, student_full_name, session_date)
            else:
                subject = "REMINDER: Your {0} session with {1}".format(
                    mentorship_word, student_full_name)
            template = "email_templates/mentorship/Session_Reminder_to_Mentor.html"

        render_send_email(subject, template, ctd, current_site,
                          mentor_email, is_mentorship_email=True)

    rq_logger.debug(
        "Sent session reminder to mentor - {0}.".format(session_id))


def send_session_reminder_to_mentee__helper(mentee: StartupMember, session: MentorshipSession, uni: University):
    protocol = PROTOCOL
    current_site = uni.site
    uni_style = UniversityStyle.objects.get(_university=uni)
    is_team = session.is_team_session()

    if is_team:
        url = '{0}://{1}/{2}'.format(
            protocol,
            current_site.domain,
            'mentorship/team/mentee-dashboard'
        )

        team_session: TeamMentorshipSession = session.teammentorshipsession
        session_type = team_session.get_team_session_type()
        session_location = session.location
        mentor_names = list_comma(team_session.get_mentors())

    else:
        url = '{0}://{1}/{2}'.format(
            protocol,
            current_site.domain,
            'mentorship/mentee-dashboard'
        )

        mentorship = session.mentorship
        mentor = mentorship.mentor
        session_type, session_location = get_session_type_and_location(
            session, mentor)
        mentor_names = mentor.profile.get_full_name()

    session_date = session.get_datetime().strftime('%b %d')
    session_time = session.get_datetime().strftime('%H:%M %p')

    has_custom_email = uni_style.session_reminder_mentee_email is not None and \
        uni_style.session_reminder_mentee_email.strip() != ''
    if has_custom_email:
        email_body = uni_style.session_reminder_mentee_email
        rendered_email_body = render_custom_email_variables(
            email_body, session, uni)
    else:
        rendered_email_body = None

    ctd = {
        'has_custom_email': has_custom_email,
        'email_body': rendered_email_body,
        'session_date': session_date,
        'session_time': session_time,
        'session_timezone': session.get_timezone(),
        'session_type': session_type,
        'session_location': session_location,
        'meeting_topic': session.meeting_topic,
        'mentee_first_name': mentee.get_first_name(),
        'mentor_names': mentor_names,
        'dashboard_url': url,
        'is_team': is_team,
    }

    # ctd fields specific to single-mentorship
    if not is_team:
        mentor = session.mentorship.mentor
        ctd['mentor_phone_number'] = mentor.phone
        ctd['mentor_skype'] = mentor.skype
        if is_cronofy_linked(mentor.profile.user):
            if session_type == "Phone":
                ctd['mentor_phone_number'] = session_location
            elif session_type == "Video Call":
                ctd['mentor_skype'] = session_location

    mentorship_word = uni.style.mentorship_word
    if is_team:
        mentorship_word = "team " + mentorship_word

    subject = f"REMINDER: Your {mentorship_word} session on {session_date} with {mentor_names}"
    template = "email_templates/mentorship/Session_Reminder_to_Mentee.html"
    student_email = mentee.user.email

    render_send_email(subject, template, ctd, current_site,
                      student_email, is_mentorship_email=True)

    rq_logger.debug(f"Sent {mentorship_word} session reminder (session id: {session.id}) "
                    f"to mentee (startupmember id: {mentee.id}.")


@Roborovski.task(default_retry_delay=60, max_retries=3)
def send_session_reminder_to_mentee(current_site_id, session_id, is_team=False):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    try:
        session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                           'mentorship__mentor__profile',
                                                           'mentorship__mentor',
                                                           'mentorship__student__user',
                                                           'mentorship__student') \
            .get(id=int(session_id))
    except MentorshipSession.DoesNotExist:
        raise ValueError("Invalid Session id given - {0}".format(session_id))

    if is_team:
        team_session: TeamMentorshipSession = session.teammentorshipsession
        for mentee_user in team_session.members_attending.all():
            send_session_reminder_to_mentee__helper(
                mentee_user.startup_member, session, uni)
    else:
        send_session_reminder_to_mentee__helper(
            session.mentorship.student, session, uni)


@Roborovski.task
def send_session_reminder_to_uni(session_id, current_site_id):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    try:
        session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                           'mentorship__mentor__profile',
                                                           'mentorship__mentor',
                                                           'mentorship__student__user',
                                                           'mentorship__student') \
            .get(id=int(session_id))
    except MentorshipSession.DoesNotExist:
        raise ValueError("Invalid Session id given - {0}".format(session_id))
    protocol = PROTOCOL
    url = "{0}://{1}/{2}".format(
        protocol,
        current_site.domain,
        'university/manage/mentorship/approval/meetings'
    )
    mentor = session.mentorship.mentor
    mentor_user = mentor.profile.user
    mentor_full_name = mentor.profile.get_full_name()

    session_type, session_location = get_session_type_and_location(
        session, mentor)

    student = session.mentorship.student
    student_full_name = student.get_full_name()
    session_date = session.get_datetime().strftime('%b %d')
    session_time = session.get_datetime().strftime('%H:%M')

    ctd = {
        'session_date': session_date,
        'session_time': session_time,
        'session_timezone': mentor.timezone,
        'session_type': session_type,
        'session_location': session_location,
        'mentor_phone_number': mentor.phone,
        'mentor_skype': mentor.skype,
        'mentor_full_name': mentor_full_name,
        'mentee_full_name': student_full_name,
        'review_url': url
    }

    if is_cronofy_linked(mentor_user):
        if session_type == "Phone":
            ctd['mentor_phone_number'] = session_location
        elif session_type == "Video Call":
            ctd['mentor_skype'] = session_location

    subject = "Admin approval requested for {0}'s meeting with {1}".format(
        student_full_name, mentor_full_name)
    template = "email_templates/mentorship/Session_Approval_Reminder_to_Uni.html"

    staffs = uni.uni_staffs.all()
    for staff in staffs:
        if UniversityStaff.objects.filter(
                admin_id=staff.id, university_id=uni.id, is_super=True).exists():
            render_send_email(subject, template, ctd,
                              current_site, staff.email, is_admin_email=True)
    rq_logger.debug("Sent session reminder to admin - {0}.".format(session.id))


def send_session_followup_to_mentor__helper(mentor: Mentor, session: MentorshipSession):
    current_site = mentor.platform.site
    protocol = PROTOCOL

    is_team = session.is_team_session()
    team_feedback_url = None

    if is_team:
        team_mentorship_session: TeamMentorshipSession = session.teammentorshipsession
        team = team_mentorship_session.team
        mentee_full_name = team.name
        url = '{0}://{1}{2}'.format(
            protocol,
            current_site.domain,
            reverse('team-mentor-dashboard-type',
                    args=(team.url_name, 'notes'))
        )
        team_feedback_url = '{0}://{1}{2}'.format(
            protocol,
            current_site.domain,
            reverse('team-mentor-dashboard-type',
                    args=(team.url_name, 'sessions'))
        )
    else:
        student = session.mentorship.student
        mentee_full_name = student.get_full_name()
        url = '{0}://{1}/{2}?add_note=true&session_id={3}'.format(
            protocol,
            current_site.domain,
            'mentorship/mentor-dashboard',
            session.id
        )

    mentor_first_name = mentor.profile.first_name

    ctd = {
        'mentor_first_name': mentor_first_name,
        'mentee_full_name': mentee_full_name,
        'dashboard_url': url,
        'is_team': is_team,
        'team_feedback_url': team_feedback_url,
    }

    subject = "Your recent meeting with {0}".format(mentee_full_name)
    template = "email_templates/mentorship/Session_Followup_to_Mentor.html"

    mentor_user = mentor.profile.user
    mentor_email = mentor_user.email

    render_send_email(subject, template, ctd, current_site,
                      mentor_email, is_mentorship_email=True)

    if is_team:
        rq_logger.debug("Sent team mentorship session followup to mentor id {0} for session id {1}."
                        .format(mentor.id, session.id))
    else:
        rq_logger.debug("Sent single-mentorship session followup to mentor id {0} for session id {1}."
                        .format(mentor.id, session.id))


@Roborovski.task(default_retry_delay=60, max_retries=3)
def send_session_followup_to_mentor(session_id):
    try:
        session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                           'mentorship__mentor__profile',
                                                           'mentorship__mentor',
                                                           'mentorship__student') \
            .get(id=int(session_id))
    except MentorshipSession.DoesNotExist:
        raise ValueError("Invalid Session id given - {0}".format(session_id))

    if session.is_team_session():
        mentors = session.teammentorshipsession.get_mentors()
        for mentor in mentors:
            send_session_followup_to_mentor__helper(mentor, session)
    else:
        mentor = session.mentorship.mentor
        send_session_followup_to_mentor__helper(mentor, session)


def send_session_followup_to_mentee__helper(mentee: StartupMember, session: MentorshipSession):
    protocol = PROTOCOL
    current_site = mentee.universities.first().site

    is_team = session.is_team_session()

    if is_team:
        team_session: TeamMentorshipSession = session.teammentorshipsession
        mentor_names = list_comma(team_session.get_mentors())
        team = team_session.team

        url = '{0}://{1}{2}'.format(
            protocol,
            current_site.domain,
            reverse('team-mentee-dashboard-type',
                    args=(team.url_name, 'sessions'))
        )

    else:
        mentor = session.mentorship.mentor
        mentor_names = mentor.profile.get_full_name()

        url = '{0}://{1}/{2}?add_note=true&session_id={3}'.format(
            protocol,
            current_site.domain,
            'mentorship/mentee-dashboard',
            session.id
        )

    subject = "Your recent meeting with {0}".format(mentor_names)
    template = "email_templates/mentorship/Session_Followup_to_Mentee.html"

    ctd = {
        'mentor_names': mentor_names,
        'mentee_first_name': mentee.first_name,
        'dashboard_url': url
    }

    student_email = mentee.user.email

    render_send_email(subject, template, ctd, current_site,
                      student_email, is_mentorship_email=True)

    if is_team:
        rq_logger.debug("Sent team mentorship session followup to mentee id {0} for session id {1}."
                        .format(mentee.id, session.id))
    else:
        rq_logger.debug("Sent single-mentorship session followup to mentee id {0} for session id {1}."
                        .format(mentee.id, session.id))


@Roborovski.task(default_retry_delay=60, max_retries=3)
def send_session_followup_to_mentee(session_id):
    try:
        session = MentorshipSession.objects.select_related('mentorship__mentor__profile',
                                                           'mentorship__mentor',
                                                           'mentorship__student__user',
                                                           'mentorship__student') \
            .get(id=int(session_id))
    except MentorshipSession.DoesNotExist:
        raise ValueError("Invalid Session id given - {0}".format(session_id))

    if session.is_team_session():
        mentee_users = session.teammentorshipsession.members_attending.all()
        for mentee_user in mentee_users:
            send_session_followup_to_mentee__helper(
                mentee_user.startup_member, session)
    else:
        send_session_followup_to_mentee__helper(
            session.mentorship.student, session)


@Roborovski.task
def send_mentorship_recommendation(
        email,
        name='',
        site_id=None,
        invitor="StartupTree",
        university="StartupTree",
        is_mentor=False,
        rec_msg="",
        mentor_url=None,
        mentee_url=None):
    """Send recommendation email to the mentor-mentee pair chosen by a university admin
    @TODO
    :param email:
    :param name:
    :param site:
    :param invitor:
    :param university:
    :return:
    """
    email = email.lower()
    try:
        eu = UserEmail.objects.get(email__iexact=email)
    except UserEmail.DoesNotExist:
        rq_logger.warning("Email does not exist: {0}".format(email))
        return
    rq_logger.debug("Start sending invitation.")
    if site_id is None:
        current_site = Site.objects.get(id=1)
    else:
        current_site = Site.objects.get(id=int(site_id))
    protocol = PROTOCOL
    url = "{0}://{1}".format(
        protocol,
        current_site.domain,
    )
    if mentor_url is not None:
        mentor_url = url + mentor_url
    if mentee_url is not None:
        mentee_url = url + mentee_url
    template = 'email_templates/mentorship/recommendation.html'
    ctd = {
        "email": eu.email,
        "reset_url": url,
        "year": timezone.now().year,
        "name": name,
        "invitor": invitor,
        "university": university,
        "is_mentor": is_mentor,
        "rec_msg": rec_msg,
        "mentor_url": mentor_url,
        "mentee_url": mentee_url,
    }
    subject_template = 'mentorship/emails/recommendation_subject.txt'
    subject = render_to_string(subject_template, ctd)
    subject = "".join(subject.splitlines())

    render_send_email(subject, template, ctd, current_site,
                      eu.email, is_mentorship_email=True)
    rq_logger.debug("Sent Mentor recommendation.")


@Roborovski.task
def send_mentor_request_noti(mentee_full_name, current_site_id):
    """Send notification to University Staffs of current site that mentee has requested
    mentorship to some mentor.
    :mentee_full_name: a full name of a mentee
    :mentee_email: an email of mentee
    :current_site: a Site object of University
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)

    mentee_word = uni.style.mentee_word
    if mentee_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        mentee_article = 'An'
    else:
        mentee_article = 'A'

    mentor_word, mentor_article = get_mentor_word_and_article(uni, False)

    protocol = PROTOCOL
    url = "{0}://{1}/{2}".format(
        protocol,
        current_site.domain,
        'university/manage/mentorship/approval/connections'
    )
    subject = "[Admin Approval Needed] {0} {1} would like to be connected to {2} {3}.".format(
        mentee_article, mentee_word, mentor_article, mentor_word)
    ctd = {
        "review_url": url,
        "year": timezone.now().year,
        "mentee_full_name": mentee_full_name,
        "subject": subject
    }
    template = "email_templates/mentorship/Mentor_Request_Notification.html"
    staffs = uni.uni_staffs.all()
    for staff in staffs:
        if UniversityStaff.objects.filter(admin_id=staff.id, university_id=uni.id, is_super=True).exists():
            render_send_email(subject, template, ctd,
                              current_site, staff.email, is_admin_email=True)
    rq_logger.debug("Send mentor request notification")


@Roborovski.task
def send_session_request_noti_to_uni(session_id, current_site_id):
    """
    This method is called in two scenarios:

        1. Send notification to University Staffs of current site that mentee has requested
        mentorship meeting with a mentor.

        2. Mentor approves a pending multi-meeting request and platform requires admin approval for new meetings.

    Currently, this method is only applicable to singe-mentorship meetings (not team mentorship).

    :session_id: the MentorshipSession object of the meeting
    :current_site: a Site object of University
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    session = MentorshipSession.objects.get(id=session_id)

    assert not session.is_team_session(
    ), "This method only applicable to non-team mentorship sessions. "

    mentor_full_name = session.mentorship.mentor.profile.get_full_name()
    mentee_full_name = session.mentorship.student.get_full_name()

    mentee_word = uni.style.mentee_word
    if mentee_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        mentee_article = 'An'
    else:
        mentee_article = 'A'

    mentor_word, mentor_article = get_mentor_word_and_article(uni, False)

    protocol = PROTOCOL
    url = "{0}://{1}/{2}".format(
        protocol,
        current_site.domain,
        'university/manage/mentorship/approval/meetings'
    )
    subject = "[Admin Approval Needed] {0} {1} would like to schedule a meeting with {2} {3}.".format(
        mentee_article, mentee_word, mentor_article, mentor_word)
    ctd = {
        "review_url": url,
        "year": timezone.now().year,
        "mentee_full_name": mentee_full_name,
        "mentor_full_name": mentor_full_name,
        "subject": subject,
        "mentor_meeting_custom": custom_meeting_dict(session),
        "url_domain": uni.get_url(),
    }
    template = "email_templates/mentorship/Session_Request_Notification_to_Uni.html"
    staffs = UniversityStaff.objects.filter(university=uni)
    for staff in staffs:
        is_mentorship = staff.access_levels.filter(
            name=AccessLevel.MENTORSHIP).exists()
        startup_member = staff.admin.startup_member
        if staff.is_super or is_mentorship:
            render_send_email(subject, template, ctd, current_site,
                              staff.admin.email, is_admin_email=True)
    rq_logger.debug("Send mentor session request notification to admins")


def session_request_cronofy(mentorship, session_or_set, current_site_id, is_multiple):
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    mentor = mentorship.mentor
    user = mentor.profile.user
    if is_multiple:
        for session in session_or_set.sessions.all():
            mentor_add_session_to_cronofy(
                uni, user, session, cronofy_calendar_id=None, is_pending_meeting_block=True)
    else:
        if mentor.approve_meetings_or_admin_override:
            """Special case when it is just one block with mentorship approval is needed."""
            mentor_add_session_to_cronofy(uni, user, session_or_set, cronofy_calendar_id=None,
                                          is_pending_meeting_block=True)


@Roborovski.task
def send_session_request_noti_to_mentor(
        session_id,
        mentee_full_name,
        mentor_id,
        current_site_id,
        is_multiple,
        meeting_topic):
    """Send notification to mentor that mentee has requested mentorship meeting.

    IF IS_MULTIPLE, AND MENTOR IS CRONOFY LINKED, THEN BLOCK MENTOR CALENDAR FOR THOSE TIMESLOTS.
    SPECIAL CASE: If not is_multiple but approve_meetings_or_admin_override, then also block mentor calendar for that timeslot.

    :mentee_full_name: a full name of a mentee
    :mentor_first_name: first name of mentor
    :current_site: a Site object of University
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    mentor = Mentor.objects.get(id=mentor_id)

    protocol = PROTOCOL
    url = '{0}://{1}/{2}'.format(
        protocol,
        current_site.domain,
        'mentorship/mentor-dashboard'
    )

    if is_multiple:
        session_set = MentorshipSessionSet.objects.get(id=session_id)
        session_dicts = []
        for session in session_set.sessions.all():
            session_dict = {
                'meeting_time': session.get_datetime().strftime('%b %d %H:%M'),
                'timezone': session.get_timezone(),
                'location': session.get_location_or_type()
            }
            session_dicts.append(session_dict)
        mentor_meeting_custom = custom_meeting_dict(
            session_set.sessions.first())
        ctd = {
            "review_url": url,
            "mentor_first_name": mentor.profile.first_name,
            "mentee_full_name": mentee_full_name,
            "is_multiple": is_multiple,
            "sessions": session_dicts,
            "meeting_topic": meeting_topic,
            "mentor_meeting_custom": mentor_meeting_custom,
            'url_domain': current_site.university.get_url(),
        }
    else:
        session = MentorshipSession.objects.get(id=session_id)

        session_type, session_location = get_session_type_and_location(
            session, mentor)
        contact_info = get_session_contact_info(session_type, session_location)
        session_dict = {
            'meeting_time': session.get_datetime().strftime('%b %d %H:%M'),
            'timezone': session.get_timezone(),
        }
        mentor_meeting_custom = custom_meeting_dict(session)
        ctd = {
            "review_url": url,
            "mentor_first_name": mentor.profile.first_name,
            "mentee_full_name": mentee_full_name,
            "is_multiple": is_multiple,
            "session": session_dict,
            "session_type": session_type,
            "contact_info": contact_info,
            "meeting_topic": meeting_topic,
            "mentor_meeting_custom": mentor_meeting_custom,
            'url_domain': current_site.university.get_url(),
        }

    subject = "New meeting request from {0}".format(
        mentee_full_name)
    template = "email_templates/mentorship/Session_Request_Notification_to_Mentor.html"

    render_send_email(subject, template, ctd, current_site,
                      mentor.profile.user.email, is_mentorship_email=True)
    rq_logger.debug("Send mentor session request notification to mentor")


@Roborovski.task
def send_new_goal_noti(
        current_site_id,
        mentee_sm_id,
        mentor_full_name,
        new_goals,
        session_id):
    """Send a notification email to mentee about new goals.
    :param current_site_id:
    :param mentee_sm_id:
    :param mentor_full_name:
    :param new_goals:
    :param session_id:
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    mentee = StartupMember.objects.select_related(
        'user').get(id=int(mentee_sm_id))
    protocol = PROTOCOL
    url = "{0}://{1}/mentorship/mentee-dashboard".format(
        protocol,
        current_site.domain,
        session_id
    )
    subject = "You have a new goal assigned to you by {0}".format(
        mentor_full_name)
    ctd = {
        "year": timezone.now().year,
        "mentor_full_name": mentor_full_name,
        "mentee_first_name": mentee.first_name,
        "subject": subject,
        "session_note_url": url,
        "new_goals": new_goals,
        "is_single_goal": True if len(new_goals) < 2 else False
    }
    template = "email_templates/mentorship/goals.html"

    render_send_email(subject, template, ctd, current_site,
                      mentee.user.email, is_mentorship_email=True)
    rq_logger.debug("Send new goal notification")


@Roborovski.task
def send_goal_completed_noti(
        current_site_id,
        mentor_id,
        mentee_full_name,
        completed_goal):
    """Send a notification email to mentee about new goals.
    :param current_site_id:
    :param mentor_sm_id:
    :param mentee_full_name:
    :param completed_goal:
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    uni = University.objects.get(site=current_site)
    mentor = Mentor.objects.select_related(
        'profile', 'profile__user').get(id=int(mentor_id))
    protocol = PROTOCOL
    mentees_url = "{0}://{1}/mentorship/mentor-dashboard".format(
        protocol,
        current_site.domain
    )
    subject = "Your {0} has completed a goal!".format(uni.style.mentee_word)
    ctd = {
        "mentor_first_name": mentor.profile.get_first_name(),
        "mentee_full_name": mentee_full_name,
        "subject": subject,
        "goal": completed_goal,
        "mentees_url": mentees_url
    }
    template = "email_templates/mentorship/goals_completed.html"

    render_send_email(subject, template, ctd, current_site,
                      mentor.profile.user.email, is_mentorship_email=True)
    rq_logger.debug("Send goal completed notification")


@Roborovski.task
def refresh_cronofy_cache():
    cronofy_cache.refresh_cronofy_cache()


@Roborovski.task
def refresh_cronofy_cache__user(mentor_id):
    mentor_user = Mentor.objects.get(id=mentor_id).profile.user
    cronofy_cache.refresh_cronofy_cache__user(mentor_user)


@Roborovski.task
def send_team_session_changed_noti_admin(team_session_id, platform_id, state):
    """Sends a notification to platform admin that a team mentorship session has been scheduled or cancelled"""
    team_session = TeamMentorshipSession.objects.get(id=team_session_id)
    platform = University.objects.get(id=platform_id)

    super_or_team_mentorship = Q(is_super=True) | Q(
        access_levels__name=AccessLevel.TEAM_MENTORSHIP)
    staffs = UniversityStaff.objects.filter(
        super_or_team_mentorship, university_id=platform.id)

    platform_style: UniversityStyle = platform.style

    initiator = team_session.creator.get_full_name()
    mentorship_word = platform_style.mentorship_word
    team_name = team_session.team.name
    mentors_str = ', '.join(
        map(lambda mentor: mentor.profile.get_full_name(), team_session.get_mentors()))
    session_datetime_str = format_datetime_english(
        team_session.datetime, tz=platform_style.timezone)

    protocol = PROTOCOL
    uni_recent_team_activity_full_all = reverse(
        'uni-recent-team-activity-full-all')
    url = f'{protocol}://{platform.site.domain}{uni_recent_team_activity_full_all}'

    ctd = {
        'initiator': initiator,
        'team_name': team_name,
        'mentors_str': mentors_str,
        'session_datetime_str': session_datetime_str,
        'all_team_mentorship_activity_url': url,
    }

    if state == SESSION_CREATED:
        template = 'email_templates/mentorship/Team_Session_Scheduled_to_Admin.html'
        subject = f"A Team {mentorship_word.title()} session has been scheduled by {initiator} for {team_name} with {mentors_str}. "

    elif state == SESSION_CANCELLED:
        template = 'email_templates/mentorship/Team_Session_Cancelled_to_Admin.html'
        subject = f"A Team {mentorship_word.title()} session has been cancelled by {initiator} for {team_name} with {mentors_str}. "
    else:
        app_logger.error(
            'send_team_session_changed_noti_admin(): Invalid State Param')
        return

    for staff in staffs:
        recipient = staff.admin.email
        render_send_email(subject, template, ctd, platform.site,
                          recipient, is_admin_email=True)


@Roborovski.task
def resync_user_cronofy_events(user_id):
    user = UserEmail.objects.get(id=user_id)
    from mentorship.st_cronofy import resync_calendar_events
    resync_calendar_events(user)
