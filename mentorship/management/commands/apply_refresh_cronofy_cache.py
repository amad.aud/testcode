from django.core.management import BaseCommand
from mentorship.tasks import refresh_cronofy_cache


class Command(BaseCommand):
    """ runs refresh_cronofy_cache()
    """
    def handle(self, *args, **options):
        refresh_cronofy_cache.apply()
