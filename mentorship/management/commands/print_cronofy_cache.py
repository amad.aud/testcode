from django.core.management import BaseCommand
from mentorship.models import CronofyCacheSlot
from trunk.utils import format_datetime


class Command(BaseCommand):
    """
    Prints all cronofy chache slot objects.
    """

    @staticmethod
    def print_cache_obj(slot: CronofyCacheSlot):
        print(f'Slot ID: {slot.id}')
        print(f'\tslot.mentor_user = {slot.mentor_user}')
        print(f'\tslot.start_datetime = {format_datetime(slot.start_datetime)}')
        print(f'\tslot.end_datetime = {format_datetime(slot.end_datetime)}')
        print(f'\tslot.timestamp = {format_datetime(slot.timestamp)}')

    def handle(self, *args, **options):
        print('Begin')
        for slot in CronofyCacheSlot.objects.all():
            Command.print_cache_obj(slot)
        print('End')
