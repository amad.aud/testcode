from django.core.management import BaseCommand
from mentorship.models import Mentor, Mentorship
from branch.models import StartupMember


class Command(BaseCommand):
    """
    python3 manage.py reset_mentorship <mentor_urlname> <mentee_urlname>

    Deletes the Mentorship object between specified mentor and mentee.
    (Has automatic side effect of also deleted associated MentorshipRequest object.)
    """

    def add_arguments(self, parser):
        parser.add_argument('mentor_urlname')
        parser.add_argument('mentee_urlname')

    @staticmethod
    def print_mentorship(mentorship):
        print('Found Mentorship: ')
        print('\tmentorship.id = ' + str(mentorship.id))
        print('\tmentorship.mentor = ' + str(mentorship.mentor))
        print('\tmentorship.student = ' + str(mentorship.student))
        print('\tmentorship.created_on = ' + str(mentorship.created_on))

    def handle(self, *args, **options):
        mentor_urlname = options['mentor_urlname']
        mentee_urlname = options['mentee_urlname']

        mentor = Mentor.objects.get(profile__url_name=mentor_urlname)
        student = StartupMember.objects.get(url_name=mentee_urlname)

        mentorship = Mentorship.objects.filter(mentor=mentor, student=student)

        mentorship.delete()

        print('Deleted mentorship id: ' + str(mentorship.id))



