from django.core.management.base import BaseCommand
from StartupTree.loggers import app_logger
from mentorship.models import Mentor

class Command(BaseCommand):
    help = "Prints out a list of mentors"

    @staticmethod
    def print_mentor(mentor):
        print(mentor)
        print("ID = " + str(mentor.id))
        print("Is Team Mentor: " + str(mentor.is_team_mentor))
        print("")

    def handle(self, *args, **options):
        for mentor in Mentor.objects.all():
            self.print_mentor(mentor)
