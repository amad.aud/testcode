from django.core.management import BaseCommand
from django.utils import timezone
from mentorship.models import Mentor, Mentorship, MentorshipSession, MentorshipSessionSet
from branch.models import StartupMember
from trunk.utils import format_datetime
from datetime import datetime


class Command(BaseCommand):
    """
    print_session_set <session_set_id>

    Pretty prints the attributes (specified in attrs) of the MentorshipSession objects in the specified session set

    SAMPLE OUTPUT:

        root@35465bd4bddd:/project# python3 manage.py print_sessions 138
        session.id = 443
                session.mentorship.mentor  = Timothy (Mentor 2) Zhu
                session.mentorship.student = Sungho Park
                session.datetime           = 2020-06-01 11:00:00 EDT
                session.is_mentor_accepted = None
                session.is_admin_accepted  = None
                session.is_cancelled       = False

        session.id = 442
                session.mentorship.mentor  = Timothy (Mentor 2) Zhu
                session.mentorship.student = Sungho Park
                session.datetime           = 2020-06-05 13:00:00 EDT
                session.is_mentor_accepted = None
                session.is_admin_accepted  = None
                session.is_cancelled       = False

        session.id = 441
                session.mentorship.mentor  = Timothy (Mentor 2) Zhu
                session.mentorship.student = Sungho Park
                session.datetime           = 2020-06-05 12:00:00 EDT
                session.is_mentor_accepted = None
                session.is_admin_accepted  = None
                session.is_cancelled       = False

    """

    # MODIFY this to adjust which attributes of MentorshipSession objects get printed
    attrs = [
        'mentorship.mentor',
        'mentorship.student',
        'datetime',
        'get_duration()',
        'is_mentor_accepted',
        'is_admin_accepted',
        'is_cancelled',
    ]

    def add_arguments(self, parser):
        parser.add_argument('mentor_urlname')
        parser.add_argument('mentee_urlname')

    def handle(self, *args, **options):
        mentor_urlname = options['mentor_urlname']
        mentee_urlname = options['mentee_urlname']

        min_label_width = Command.get_min_label_width()
        for session in Command.get_sessions(mentor_urlname, mentee_urlname):
            Command.print_session(session, min_label_width)

    @staticmethod
    def get_sessions(mentor_urlname, mentee_urlname):
        """ returns iterable of MentorshipSession objects to print. """

        mentor = Mentor.objects.get(profile__url_name=mentor_urlname)  # tzhu2
        mentee = StartupMember.objects.get(url_name=mentee_urlname)  # spark37

        mentorship = Mentorship.objects.get(mentor=mentor, student=mentee)

        session_set = MentorshipSessionSet.objects.filter(mentorship=mentorship).order_by('-id').first()

        return session_set.sessions.all()

    @staticmethod
    def get_padding(attr, min_label_width):
        """ Gets padding needed to make proper alignment. """
        padding_width = max(0, min_label_width - len(attr))
        padding = ' ' * padding_width
        return padding

    @staticmethod
    def get_min_label_width():
        """ Gets width of label that is the longest. """
        max_label = max(Command.attrs, key=len)
        return len(max_label)

    @staticmethod
    def to_string(obj):
        """ returns string representation of input. If it is a datetime, pretty-print it. """
        if isinstance(obj, datetime):
            return format_datetime(obj)
        return str(obj)

    @staticmethod
    def print_session(session, min_label_width):
        """ Print all the attributes (specified by attrs) of the session. """
        def print_attr_cmd(attr):
            padding = Command.get_padding(attr, min_label_width)
            return f"print('\tsession.{attr}{padding} = ' + Command.to_string(session.{attr}))"

        print('session.id = ' + str(session.id))

        for attr in Command.attrs:
            exec(print_attr_cmd(attr))

        print('')
