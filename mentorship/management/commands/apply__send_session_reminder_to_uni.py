from django.core.management.base import BaseCommand
from mentorship.models import MentorshipSession
from mentorship.tasks import send_session_reminder_to_uni

class Command(BaseCommand):
    ''' Applies send_session_reminder_to_uni to the most recent mentorship session.
    (Assumes that this mentorship session is still pending admin approval)
    '''

    @staticmethod
    def print_session_info(session):
        print_cmd = lambda var_name: "print('\tsession." + var_name + " = ' + str(session." + var_name + "))"
        attrs = [
            'id',
            'mentorship.mentor',
            'mentorship.student',
            'get_datetime()',
        ]

        print('Session Info')

        for attr in attrs:
            exec(print_cmd(attr))

        print('')

    def handle(self, *args, **options):
        session = MentorshipSession.objects.all().order_by('-id').first()
        Command.print_session_info(session)

        uni = session.mentorship.mentor.platform
        current_site = uni.site

        print('University: ' + uni.name)

        print('')

        cmd = 'send_session_reminder_to_uni.apply((' + str(session.id) + ', ' + str(current_site.id) + '))'

        print('EXECUTE: ' + cmd)
        exec(cmd)
        
        print('')
