from django.core.management.base import BaseCommand
from mentorship.models import Mentor, MentorSchedule, MentorshipSession
from django.utils import timezone


class Command(BaseCommand):

    help = "Sets the contact info of all existing non-'In Person' MentorSchedule and \
            MentorshipSession objects to whatever the mentor's defaults are, so that \
            these objects can contain their own contact info."

    def handle(self, *args, **options):
        for mentor in Mentor.objects.all():
            for sched in MentorSchedule.objects.filter(mentor_id=mentor.id):
                if sched.meeting_type == 1:     # phone
                    if mentor.phone:
                        sched.phone_number = mentor.phone
                    elif mentor.phone_number:
                        sched.phone_number = mentor.phone_number
                elif sched.meeting_type == 3:   # video call
                    if mentor.skype:
                        sched.video_id = mentor.skype
                sched.save()
            for session in MentorshipSession.objects.select_related('mentorship')\
                    .filter(mentorship__mentor_id=mentor.id, datetime__gt=timezone.now()):
                if session.meeting_type == 1:     # phone
                    if mentor.phone:
                        session.phone_number = mentor.phone
                    elif mentor.phone_number:
                        session.phone_number = mentor.phone_number
                elif session.meeting_type == 3:   # video call
                    if mentor.skype:
                        session.video_id = mentor.skype
                session.save()
