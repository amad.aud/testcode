from collections import defaultdict
from datetime import datetime
from mentorship.models import Mentorship, MentorshipSession
from trunk.utils import format_datetime
from typing import Dict, List, Tuple


class MentorshipSessionPrinter:

    default_attrs = {
        'single_mentorship': [
            'is_team_session()',
            'mentorship.mentor',
            'mentorship.student',
            'mentorship.id',
            'datetime',
            'is_mentor_accepted',
            'is_admin_accepted',
            'is_cancelled',
        ],
        'team_mentorship': [
            'is_team_session()',
            'teammentorshipsession.team',
            'teammentorshipsession.get_mentors()',
            'teammentorshipsession.members_attending.all()',
            'datetime',
            'is_mentor_accepted',
            'is_admin_accepted',
            'is_cancelled',
        ]
    }

    def __init__(self, attrs=None, tab_level=0):
        self.attrs = attrs if attrs else self.default_attrs
        self.tab_level = tab_level
        self.min_label_width = self.get_min_label_width()

    def get_min_label_width(self):
        """ Gets width of label that is the longest. """
        max_label__single_mentorship = max(self.attrs['single_mentorship'], key=len)
        max_label__team_mentorship = max(self.attrs['team_mentorship'], key=len)
        min_label_width = max(len(max_label__single_mentorship), len(max_label__team_mentorship))
        return min_label_width

    def print_session(self, session: MentorshipSession):
        """ Print all the attributes (specified by attrs) of the session. """
        indent = '\t' * self.tab_level

        def print_attr_cmd(attr):
            padding = self.get_padding(attr)
            return f"print(indent + '\tsession.{attr}{padding} = ' + MentorshipSessionPrinter.to_string(session.{attr}))"

        print(indent + 'session.id = ' + str(session.id))

        attrs = self.attrs['team_mentorship'] if session.is_team_session() else self.attrs['single_mentorship']

        for attr in attrs:
            exec(print_attr_cmd(attr))

        print('')

    def get_padding(self, attr):
        """ Gets padding needed to make proper alignment. """
        padding_width = max(0, self.min_label_width - len(attr))
        padding = ' ' * padding_width
        return padding

    @staticmethod
    def to_string(obj):
        """ returns string representation of input. If it is a datetime, pretty-print it. """
        if isinstance(obj, datetime):
            return format_datetime(obj)
        return str(obj)


SessionsByDatetimeDict = Dict[datetime, List[MentorshipSession]]  # New Type


class GetDuplicateSessions:
    def __init__(self, mentorships=None):
        """ mentorships is an iterable of Mentorship objects.
        If none is supplied, we iterate over all Mentorship objects."""
        self.mentorships = mentorships if mentorships else Mentorship.objects.all()
        self.sessions_dict = self.__make_sessions_dict()

    @staticmethod
    def __group_by_dt(mentorship):
        """ returns a dictionary of mentorship sessions grouped by key datetime"""

        d: SessionsByDatetimeDict = defaultdict(list)  # Dict[datetime, List[MentorshipSession]]

        session: MentorshipSession
        for session in MentorshipSession.objects.filter(mentorship=mentorship, is_cancelled=False):
            key = session.datetime
            d[key].append(session)

        return d

    def __make_sessions_dict(self):
        """
        Returns a 2-level dictionary.

        return_type: Dict[
            Mentorship,
            Dict[
                datetime,
                List[MentorshipSession]
            ]
        ]

        Returns a dictionary of active MentorshipSession objects, first grouped by datetime in the inner dictionary
        and then grouped by Mentorship in the outer dictionary.

        A duplicate exists if there are two MentorshipSession objects where is_cancelled=False, and both have
        same Mentorship, datetime fields.
        """

        # Dict[MentorshipSession, Dict[datetime, List[MentorshipSession]]]
        d: Dict[MentorshipSession, SessionsByDatetimeDict] = dict()

        for mentorship in self.mentorships:
            d[mentorship] = GetDuplicateSessions.__group_by_dt(mentorship)

        return d

    def get(self):
        """
        returns a dictionary keyed of type Tuple[Mentorship, datetime], and value of type List[MentorshipSession]

        The dictionary only contains an entry if the Mentorship and datetime of the key has duplicate active
        MentorshipSessions.
        """

        d: Dict[Tuple[Mentorship, datetime], List[MentorshipSession]] = dict()

        for (mentorship, sessions_by_dt) in self.sessions_dict.items():
            for (dt, sessions) in sessions_by_dt.items():
                if len(sessions) > 1:
                    d[(mentorship, dt)] = sessions

        return d