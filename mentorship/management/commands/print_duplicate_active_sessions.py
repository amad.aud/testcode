from django.core.management import BaseCommand
from trunk.utils import format_datetime
from mentorship.management.commands.utils import MentorshipSessionPrinter, GetDuplicateSessions


class Command(BaseCommand):
    """
    Pretty prints out the duplicate sessions, grouped by mentorship/datetime.

    Two sessions are duplicates if they have is_cancelled=False and have the same mentorship and datetime fields.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.print_session = MentorshipSessionPrinter(tab_level=1).print_session

    def process_duplicates(self, dt, session_duplicates, mentorship):
        """ given a list of session duplicates, pretty print them if len > 1 """
        if len(session_duplicates) > 1:
            print(f'Duplicates for Mentorship ID {mentorship.id} and Datetime {format_datetime(dt)}: ')
            for session in session_duplicates:
                self.print_session(session)

    def handle(self, *args, **options):
        duplicate_sessions = GetDuplicateSessions().get()

        for ((mentorship, dt), sessions) in duplicate_sessions.items():
            self.process_duplicates(dt, sessions, mentorship)
