from django.core.management import BaseCommand
from mentorship.models import MentorshipSession
from StartupTree.utils.pretty import printer


class Command(BaseCommand):
    def handle(self, *args, **options):
        session: MentorshipSession = MentorshipSession.objects.last()
        if session.is_team_session():
            printer(session.teammentorshipsession)
        else:
            printer(session)