from django.core.management import BaseCommand
from mentorship.models import MentorshipSession
from datetime import timedelta
from mentorship.tasks import check_recent_mentorship_sessions
from django.utils import timezone
from trunk.utils import format_datetime


class Command(BaseCommand):
    """
    python3 manage.py set_meeting_time <meeting_id> <minutes_ago> <apply__check_recent_mentorship_sessions>

    Takes the MentorshipSession object whose id is <meeting_id>,
    and sets 'date_time' field to such that the meeting's end time will be specified number of minutes ago.

    <apply__check_recent_mentorship_sessions> should be "True" if you want to invoke
    "check_recent_mentorship_sessions" task.

    Useful when testing platform features that require the meeting end-time
    to be a certain number of minutes in the past.
    """

    def add_arguments(self, parser):
        parser.add_argument('meeting_id')
        parser.add_argument('minutes_ago')
        parser.add_argument('apply__check_recent_mentorship_sessions')

    @staticmethod
    def get_new_datetime(session: MentorshipSession, minutes_ago):
        """
        returns a datetime such that end_time will be minutes_ago from now.
        """
        end_time = timezone.now() - timedelta(minutes=minutes_ago)

        new_start_time = end_time - timedelta(minutes=session.get_duration())

        return new_start_time

    def handle(self, *args, **options):
        meeting_id = int(options['meeting_id'])
        minutes_ago = int(options['minutes_ago'])
        apply__check_recent_mentorship_sessions = \
            options['apply__check_recent_mentorship_sessions'] in ['true', 'True', 't', 'T']

        session = MentorshipSession.objects.get(id=meeting_id)

        print('Session ID: ' + str(session.id))
        print('\tOLD session.datetime = ' + format_datetime(session.datetime))

        session.datetime = Command.get_new_datetime(session, minutes_ago)
        session.save()
        new_session = MentorshipSession.objects.get(id=meeting_id)

        print("\tNEW session.datetime = " + format_datetime(new_session.datetime))
        print("")

        if apply__check_recent_mentorship_sessions:
            print('Invoke: check_recent_mentorship_sessions.apply()')
            check_recent_mentorship_sessions.apply()
