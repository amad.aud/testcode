from django.core.management.base import BaseCommand
from mentorship.models import MentorshipSession
from django.utils import timezone

class Command(BaseCommand):

    help = "Sets all past mentorship sessions to 'Approved', so that program admins \
            do not need to manually approve all past sessions after implementation \
            of the 'Require Meeting Approval' customization option."

    def handle(self, *args, **options):
        for session in MentorshipSession.objects.filter(
                timestamp_of_scheduling__lte=timezone.now()):
            session.is_admin_accepted = True
            session.save()
