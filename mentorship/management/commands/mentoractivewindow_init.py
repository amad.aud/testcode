from django.core.management import BaseCommand
from django.utils import timezone
from mentorship.models import (
    Mentor,
    MentorshipActivity,
    MentorActiveWindow,
    MentorshipSession,
    TeamMentorshipSession
)
from datetime import timedelta
from typing import Optional

MENTOR_BECAME_ACTIVE = MentorshipActivity.MENTOR_BECAME_ACTIVE


class Command(BaseCommand):
    """ python3 manage.py init_mentoractivewindow

    This command is meant to be run only once. (Please review MentorActiveWindow specs for more info.)

    This command assumes that there aren't any MentorActiveWindow objects in existence yet.

    This command initiates window objects for current mentors by guessing when they were active, per the
    guessing algorithm specified in MentorActiveWindow specs.
    """
    def handle(self, *args, **options):

        current_windows = MentorActiveWindow.objects.all()
        if current_windows.exists():
            print("Error: Attempting to initiate MentorActiveWindow objects when they already exist. "
                  "Aborting. No Action Done.")
            return

        mentors = Mentor.objects.all()

        for mentor in mentors:
            process_mentor(mentor)

        success = 0
        fail = 0
        for mentor in mentors:
            if check_mentor(mentor):
                success += 1
            else:
                fail += 1

        print(f"There are {mentors.count()} mentors. "
              f"\nSuccess: {success}"
              f"\nFail: {fail}")


def process_mentor(mentor: Mentor):
    """" Given a mentor, initialize a MentorActivityWindow record if applicable, based on
    instructions from MentorActivityWindow specs.

    Scenarios:

    1. Mentor is Active
    2. Mentor is inactive, no meetings
    3. Mentor is inactive, a 1:1 meeting, no team meeting
    4. Mentor is inactive, no 1:1 meeting, a team meeting
    5. Mentor is inactive, a 1:1 and a team meeting; 1:1 most recent
    6. Mentor is inactive, a 1:1 and a team meeting; team most recent
    """
    if mentor.is_active:
        MentorActiveWindow.objects.create(
            activity_type=MENTOR_BECAME_ACTIVE,
            mentor=mentor,
            timestamp=mentor.assigned_on,
            is_timestamp_estimated=True,
            platform=mentor.platform,
        )  # active_end is set null
    else:
        last_meeting = get_last_meeting(mentor)
        if last_meeting:
            active_end = last_meeting.datetime + timedelta(days=1)
            MentorActiveWindow.objects.create(
                activity_type=MENTOR_BECAME_ACTIVE,
                mentor=mentor,
                timestamp=mentor.assigned_on,
                is_timestamp_estimated=True,
                active_end=active_end,
                platform=mentor.platform
            )
        else:
            return  # Inactive Mentor never had any meetings, so we treat as always inactive


def check_mentor(mentor: Mentor) -> bool:
    """ Checks that the migration succeeded as expected and checks that the MentorActiveWindow invariant
    is satisfied. """
    migration_success = check_mentor_migration_success(mentor)

    if not migration_success:
        print(f"Migration failed for mentor id {mentor.id}")

    try:
        MentorActiveWindow.objects.check_invariant(mentor)
        invariant_success = True
    except AssertionError as e:
        print(str(e))
        invariant_success = False

    return migration_success and invariant_success


def check_mentor_migration_success(mentor: Mentor) -> bool:
    """ Checks that the exact operations performed in process_mentor has exactly the correct results. """
    windows = MentorActiveWindow.objects.filter(mentor_id=mentor.id)

    if mentor.is_active:
        if windows.count() == 1:
            window: MentorActiveWindow = windows.first()
            return window.timestamp == mentor.assigned_on and window.active_end is None
        else:
            return False
    else:
        sessions = MentorshipSession.objects.filter(
            mentorship__mentor_id=mentor.id, datetime__lt=yesterday()).order_by('datetime')
        team_sessions = TeamMentorshipSession.objects.filter(
            mentorships__mentor_id=mentor.id, datetime__lt=yesterday()).order_by('datetime')

        last_onetoone_session: Optional[MentorshipSession] = sessions.last() if sessions.exists() else None
        last_team_session: Optional[TeamMentorshipSession] = team_sessions.last() if team_sessions.exists() else None

        if not last_onetoone_session and not last_team_session:
            return windows.count() == 0

        if windows.count() == 1:
            window: MentorActiveWindow = windows.first()
            last_session: MentorshipSession
            if last_onetoone_session and not last_team_session:
                last_session = last_onetoone_session
            elif not last_onetoone_session and last_team_session:
                last_session = last_team_session.mentorshipsession_ptr
            else:
                # last_onetoone_session and last_team_session exist
                if last_onetoone_session.datetime >= last_team_session.datetime:
                    last_session = last_onetoone_session
                else:
                    last_session = last_team_session.mentorshipsession_ptr

            return window.timestamp == mentor.assigned_on \
                and window.active_end == (last_session.datetime + timedelta(days=1))

        else:
            return False


def get_last_meeting(mentor: Mentor) -> Optional[MentorshipSession]:
    """ Returns last scheduled (1:1 or team) meeting that Mentor had that was before yesterday.
    Regardless of 1:1 or team, we return a MentorshipSession object.

    We use yesterday, because we set active_end for inactive mentors to be 1 day after the last meeting,
    but we don't want any active_end to be in the future, so we only count meetings more than a day in the past.
    """
    last_onetoone_meeting = get_last_onetoone_meeting(mentor)
    last_team_meeting = get_last_team_meeting(mentor)

    if last_onetoone_meeting and last_team_meeting:
        return max(last_onetoone_meeting, last_team_meeting.mentorshipsession_ptr, key=lambda session: session.datetime)
    elif last_onetoone_meeting and not last_team_meeting:
        return last_onetoone_meeting
    elif not last_onetoone_meeting and last_team_meeting:
        return last_team_meeting.mentorshipsession_ptr
    else:
        # both are none
        return None


def get_last_onetoone_meeting(mentor: Mentor) -> Optional[MentorshipSession]:
    sessions = MentorshipSession.objects.filter(mentorship__mentor_id=mentor.id, datetime__lt=yesterday())
    sessions = sessions.order_by('datetime')
    return sessions.last()


def get_last_team_meeting(mentor: Mentor) -> Optional[TeamMentorshipSession]:
    team_sessions = TeamMentorshipSession.objects.filter(mentorships__mentor_id=mentor.id, datetime__lt=yesterday())
    team_sessions = team_sessions.order_by('datetime')
    return team_sessions.last()


def yesterday():
    return timezone.now() - timedelta(days=1)
