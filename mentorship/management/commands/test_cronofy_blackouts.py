from django.core.management import BaseCommand
from mentorship.models import Mentor, MentorScheduleBlackOut
from mentorship.st_cronofy import (
    is_cronofy_linked,
    QueryPeriods,
    read_availability
)
from datetime import datetime, timedelta
import datetime as dt
from trunk.utils import format_datetime_12hr
from dateutil import parser
import pprint
import pytz


pp = pprint.PrettyPrinter()

times_of_day = [0, 6, 12, 18, 23]  # hours of the day (in mentor's timezone) to check


class Command(BaseCommand):
    """
    The purpose of this command is to test converting blackout dates into cronofy query_periods

    python3 manage.py test_cronofy_blackouts <mentor_email>

    This command will generate query_periods based on mentor_email blackouts, and will
    perform exaustive checking to ensure that all the appropriate dates are included and ommitted in
    the query_periods array. Furtheremore, the test will verify that the API call succeeds.

    However, it is up to the tester to manually add blackout dates before performing this test.
    """

    @staticmethod
    def dtstr_to_fmt(dtstr):
        """ Takes raw ISO 8601 string and makes pretty"""
        dt = parser.parse(dtstr)
        return format_datetime_12hr(dt)

    @staticmethod
    def map_sets_of_qp(sets_of_query_periods, fun):
        """
        Takes a list of list of query_periods and
        makes a list of list of something new based on fun applied to the query_period
        """
        new_sets = []
        for set_of_query_periods in sets_of_query_periods:
            new_set = []
            for query_period in set_of_query_periods:
                new_set.append(fun(query_period))
            new_sets.append(new_set)
        return new_sets

    @staticmethod
    def format_query_period(query_period):
        """ Returns a copy but with dtstr_to_fmt applied to all datetimes """
        return {
            # 'start' is changed to 'begin' so that it appears alphabetically before 'end' in pretty print
            'begin': Command.dtstr_to_fmt(query_period['start']),
            'end': Command.dtstr_to_fmt(query_period['end'])
        }

    @staticmethod
    def format_sets_of_qp(sets_of_query_periods):
        """ Returns a copy but with dtstr_to_fmt applied to all datetimes """
        return Command.map_sets_of_qp(sets_of_query_periods, Command.format_query_period)

    @staticmethod
    def convert_to_dt(sets_of_query_periods):
        """ Returns a copy, but with all datetimes in object form."""
        def fun(query_period):
            return {
                'start': parser.parse(query_period['start']),
                'end': parser.parse(query_period['end'])
            }
        return Command.map_sets_of_qp(sets_of_query_periods, fun)

    @staticmethod
    def deep_all(list_of_lists, condition):
        """Returns true if all elements of the list of lists returns True."""
        return all(all(condition(elt) for elt in list_) for list_ in list_of_lists)

    @staticmethod
    def deep_any(list_of_lists, condition):
        """Returns true if any elements of the list of lists returns True."""
        return any(any(condition(elt) for elt in list_) for list_ in list_of_lists)

    @staticmethod
    def test1(sets_of_query_periods, verbose=False):
        """Returns true if all query_periods are of duration less than 35 days. (Also prints out list of list of
        respective durations. """
        def get_duration(query_period):
            return query_period['end'] - query_period['start']
        sets_of_durations = Command.map_sets_of_qp(sets_of_query_periods, get_duration)

        if verbose:
            print(f'\t\tdurations = {sets_of_durations}')

        return Command.deep_all(sets_of_durations, lambda duration: duration < timedelta(days=35))

    @staticmethod
    def is_conflict(blackout: MentorScheduleBlackOut, query_period, verbose=False):
        """
        Returns whether there is any time overlap between a blackout and query_period.

        Allows for time interval to touch, but not overlap.
        """
        b_start = blackout.get_start_dt()
        b_end = blackout.get_day_after_end_dt()
        q_start = query_period['start']
        q_end = query_period['end']

        if verbose:
            print(f'\t\tblackout = {blackout}')
            print(f'\t\tquery_period = {query_period}')
            print(f'\t\t\tb_start = {format_datetime_12hr(b_start)}')
            print(f'\t\t\tb_end = {format_datetime_12hr(b_end)}')
            print(f'\t\t\tq_start = {format_datetime_12hr(q_start)}')
            print(f'\t\t\tq_end = {format_datetime_12hr(q_end)}')
            print(f'\t\t\tb_start {format_datetime_12hr(b_start)} < q_end {format_datetime_12hr(q_end)} = {b_start < q_end}')
            print(f'\t\t\tq_start {format_datetime_12hr(q_start)} < b_end {format_datetime_12hr(b_end)} = {q_start < b_end}')
            print(f'\t\t\tb_start < q_end and q_start < b_end = {b_start < q_end and q_start < b_end}')

        return b_start < q_end and q_start < b_end

    @staticmethod
    def test2(mentor: Mentor, sets_of_query_periods):
        blackouts = MentorScheduleBlackOut.objects.filter(mentor=mentor)
        for blackout in blackouts:
            is_no_conflict = Command.deep_all(sets_of_query_periods,
                                              lambda query_period: not Command.is_conflict(blackout, query_period))
            if not is_no_conflict:
                return False
        return True

    @staticmethod
    def is_date_in_blackouts(mentor, date):
        """
        returns whethere the datetime.date object is in a mentor's blackout
        """
        blackouts = MentorScheduleBlackOut.objects.filter(mentor=mentor)

        def is_date_in_blackout(blackout):
            return blackout.start_date <= date <= blackout.end_date

        return any(is_date_in_blackout(blackout) for blackout in blackouts)

    @staticmethod
    def is_date_in_sets_of_query_periods(sets_of_query_periods, date, mentor):
        def make_dt(hour: int):
            return pytz.timezone(mentor.timezone).localize(
                datetime.combine(date, dt.time(hour=hour))
            )

        def is_date_in_query_period(query_period):
            def is_dt_in_query_period(input_dt, verbose=False):
                if verbose:
                    print(f"\t\tquery_period['start'] = {format_datetime_12hr(query_period['start'])}")
                    print(f"\t\tquery_period['end'] = {format_datetime_12hr(query_period['end'])}")
                    print(f"\t\tinput_dt = {format_datetime_12hr(input_dt)}")
                    print(f"\t\tquery_period['start'] <= input_dt <= query_period['end'] = "
                          f"{query_period['start'] <= input_dt <= query_period['end']}")
                return query_period['start'] <= input_dt <= query_period['end']
            return all(is_dt_in_query_period(make_dt(hour)) for hour in times_of_day)
        return Command.deep_any(sets_of_query_periods, is_date_in_query_period)

    @staticmethod
    def test_day(mentor, sets_of_query_periods, date, verbose=False):
        """
        If the day in in blackout, day does not appear in query periods
        If day not in blackout, day appears in query periods

        When checking query_period, we check each hour of day specified in times_of_day
        """

        in_blackouts = Command.is_date_in_blackouts(mentor, date)
        in_query_periods = Command.is_date_in_sets_of_query_periods(sets_of_query_periods, date, mentor)

        if in_blackouts:
            result = not in_query_periods
        else:
            result = in_query_periods

        if verbose:
            print(f'\t\tDate = {date}, '
                  f'in_blackouts = {in_blackouts}, '
                  f'in_query_periods = {in_query_periods}, '
                  f'result = {result}')

        return result

    @staticmethod
    def test3(mentor, sets_of_query_periods, start: dt.datetime, end: dt.datetime):
        """ Currently, this does not test the first and last days of the request, but we do test every day in the middle.
        """
        # Test beginning

        # Test middle
        first_full_day = (start.astimezone(pytz.timezone(mentor.timezone)) + timedelta(days=1)).date()
        last_full_day = (end.astimezone(pytz.timezone(mentor.timezone)) - timedelta(days=1)).date()
        current_day = first_full_day
        while current_day <= last_full_day:
            if not Command.test_day(mentor, sets_of_query_periods, current_day):
                return False
            current_day += timedelta(days=1)

        # Test end

        return True

    @staticmethod
    def test4(sets_of_query_periods, verbose=False):
        def get_span(set_of_query_periods):
            max_end = max(set_of_query_periods, key=lambda qp: qp['end'])['end']
            min_start = min(set_of_query_periods, key=lambda qp: qp['start'])['start']
            return max_end - min_start
        if verbose:
            print(f"\t\tSpans = {[get_span(qps) for qps in sets_of_query_periods]}")
        return all(get_span(set_of_qp) < timedelta(days=35) for set_of_qp in sets_of_query_periods)

    @staticmethod
    def test_result_validity(mentor, sets_of_query_periods, start, end):
        """ Test the validity of query_periods """

        # convert query_periods back to dt objects
        sets_of_query_periods = Command.convert_to_dt(sets_of_query_periods)

        print("TEST 1: Verify periods are strictly less than 35 days")
        if Command.test1(sets_of_query_periods):
            print("\tSUCCESS")
        else:
            print("\tFAIL")

        print("TEST 2: Verify that all blackout dates are omitted")
        if Command.test2(mentor, sets_of_query_periods):
            print("\tSUCCESS")
        else:
            print("\tFAIL")

        print("TEST 3: Verify that all dates are properly included or omitted.")
        if Command.test3(mentor, sets_of_query_periods, start, end):
            print("\tSUCCESS")
        else:
            print("\tFAIL")

        print("TEST 4: Verify that each set of query_periods span less than 35 days.")
        if Command.test4(sets_of_query_periods):
            print("\tSUCCESS")
        else:
            print("\tFAIL")

    @staticmethod
    def test_api_call(mentor, start, end, verbose=False):
        slots = read_availability(mentor.profile.user, mentor.platform, is_mentor=True, range=(start, end))
        if verbose:
            print(slots)

        print("TEST API CALL")
        if len(slots) > 1:
            print(f"\tSUCCESS: API CALL SUCCEED.")
        else:
            print(f"\tFAIL: API CALL FAILED.")

    def handle(self, *args, **options):
        mentor_email = options['mentor_email']

        mentor = Mentor.objects.get(profile__user__email=mentor_email)
        print(f'mentor = {mentor}')

        start = datetime.utcnow()
        end = start + timedelta(days=150)

        if is_cronofy_linked(mentor.profile.user):
            sets_of_query_periods = QueryPeriods.get_sets_of_query_periods(mentor, (start, end))
            sets_formatted = Command.format_sets_of_qp(sets_of_query_periods)

            print("Sets of Query Periods")
            pp.pprint(sets_formatted)

            Command.test_result_validity(mentor, sets_of_query_periods, start, end)
            Command.test_api_call(mentor, start, end)
        else:
            print('NOT CRONOFY LINKED')

    def add_arguments(self, parser):
        parser.add_argument('mentor_email')
