from django.core.management import BaseCommand
from mentorship.models import MentorshipSession
from django.utils import timezone
from datetime import timedelta


class Command(BaseCommand):
    """
    python3 manage.py set_meeting_date <meeting_id> <days_ago>

    Takes the MentorshipSession object whose id is <meeting_id>,
    and sets 'date_time' field to the specified number of days ago (while preserving the time).

    Useful when testing platform features that require the meeting datetime to be in the past.
    """

    def add_arguments(self, parser):
        parser.add_argument('meeting_id')
        parser.add_argument('days_ago')

    @staticmethod
    def get_new_datetime(input_dt, target_days):
        x = (timezone.now() - input_dt).days - target_days
        return input_dt + timedelta(days=x)

    def handle(self, *args, **options):
        meeting_id = int(options['meeting_id'])
        days_ago = int(options['days_ago'])

        session = MentorshipSession.objects.get(id=meeting_id)

        print('Session ID: ' + str(session.id))
        print('\tOLD session.datetime = ' + str(session.datetime))
        session.datetime = Command.get_new_datetime(session.datetime, days_ago)
        session.save()
        new_session = MentorshipSession.objects.get(id=meeting_id)
        print("\tNEW session.datetime = " + str(new_session.datetime))
        print("")
