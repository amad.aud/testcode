from django.core.management.base import BaseCommand
from mentorship.models import Mentor, MentorSchedule

class Command(BaseCommand):

    help = "If a mentor adds availability where end time is less than or equal to start time " + \
    "(a bad schedule object) then there will be a serious issue when our platform interprets available slots based on " + \
    "this schedule object. This command returns the number of mentors and schedule objects that " + \
    "have this issue and deletes the bad schedule objects if <perform_cleanup> is true. \n" + \
    "Run [bad_availability_read_or_delete false] to output info/stats on bad schedule objects. \n" + \
    "Run [bad_availability_read_or_delete true] to output info/stats and delete bad schedule objects. "

    def check_error_schedule_object(sched):
        """ Returns whether a schedule object is bad: end time is less than or
        equal to start time. """
        return sched.end_time <= sched.start_time

    def get_or_delete_bad_slots(mentor, is_delete):
        """ Returns the number of bad schedule objects associated with this mentor."""
        schedules = MentorSchedule.objects.filter(mentor=mentor)
        count = 0
        for sched in schedules:
            if Command.check_error_schedule_object(sched):
                count += 1
                if is_delete:
                    sched.delete()
        return count

    def add_arguments(self, parser):
        ''' If your command does not take any extra arguments,
        then this entire method can be deleted. Otherwise,
        add as many arguments as you need, like the example below.
        '''
        parser.add_argument('perform_cleanup')

    def read_or_delete(is_delete):
        """ If not is_delete, then just output the stats/info of bad schedule objects
        and the effected mentors. If is_delete, then also delete the bad schedule objects. """
        all_mentors = Mentor.objects.all()

        print("Inspecting " + str(all_mentors.count()) + " for availability issues.")

        total_bad_slots = 0
        total_effected_mentors = 0
        verb = "Deleted " if is_delete else "Detected "

        for mentor in all_mentors:
            bad_slots = Command.get_or_delete_bad_slots(mentor, is_delete)

            if bad_slots > 0:
                print(verb + str(bad_slots) + " bad slots for mentor: " + mentor.profile.full_name)
                total_bad_slots += bad_slots
                total_effected_mentors += 1

        print("TOTAL BAD SLOTS " + verb.upper() + ": " + str(total_bad_slots))
        print("TOTAL EFFECTED MENTORS: " + str(total_effected_mentors))

    def handle(self, *args, **options):
        perform_cleanup = options['perform_cleanup']
        is_delete = perform_cleanup == "true"

        # Always perform a read operation
        Command.read_or_delete(False)

        if is_delete:
            # If deleting bad objects, perform the delete, then perform a read operation.
            Command.read_or_delete(True)
            Command.read_or_delete(False)
