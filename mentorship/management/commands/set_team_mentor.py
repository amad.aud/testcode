from django.core.management.base import BaseCommand
from mentorship.models import Mentor

class Command(BaseCommand):

    help = "set_team_mentor <mentor_id> <true/false/True/False/T/F>"

    def add_arguments(self, parser):
        parser.add_argument('mentor_id')
        parser.add_argument('new_bool')

    @staticmethod
    def print_mentor(mentor):
        print(mentor)
        print("ID = " + str(mentor.id))
        print("Is Team Mentor: " + str(mentor.is_team_mentor))
        print("")

    @staticmethod
    def get_bool(str_bool):
        if str_bool == "true" or str_bool == "True" or str_bool == "T":
            return True
        elif str_bool == "false" or str_bool == "False" or str_bool == "F":
            return False
        else:
            return None

    def handle(self, *args, **options):
        mentor_id = options['mentor_id']
        new_bool = self.get_bool(options['new_bool'])

        if new_bool is None:
            print("Invalid bool. Must be: true / false / True / False / T / F")
            return

        mentor_old = Mentor.objects.get(id=mentor_id)

        print("BEFORE update: ")
        self.print_mentor(mentor_old)

        mentor_old.is_team_mentor = new_bool
        mentor_old.save()

        mentor_new = Mentor.objects.get(id=mentor_id)

        print("AFTER update: ")
        self.print_mentor(mentor_new)
