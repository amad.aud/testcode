from django.core.management.base import BaseCommand
from mentorship.models import MentorCronofySchedule, Mentor
from branch.management.commands.utils import get_bool

class Command(BaseCommand):
    """For MentorCronofySchedule objects whose `type` attribute is None,
    set it to mentor.default_meeting_type. This command only needs to be run once
    as the bug that caused the objects' attribute to become None will be fixed.

    Takes one argument ('true', 'True', 't', 'T', 'false', 'False', 'f', 'F'):
        A boolean for whether to print verbose: print out all of the changes made.

    Definitions:

        Error mentor: a mentor with a MentorCronofySchedule object whose `type` field is None.

        Visible Error mentor: an error mentor that has Cronofy sync enabled. (Because this bug is not
            really an issue for error mentors that don't have Cronofy sync enabled.)

        Error MentorCronofySchedule object: One whose `type` field is None.

        Visible Error MentorCronofySchedule object: An error MentorCronofySchedule object that is associated with
            a mentor that has cronofy sync enabled.
    """

    def add_arguments(self, parser):
        parser.add_argument('print_verbose')


    @staticmethod
    def print_mcs_by_id(mcs_id):
        mcs = MentorCronofySchedule.objects.get(id=mcs_id)
        print("\tID = " + str(mcs.id))
        print("\tmentor = " + str(mcs.mentor))
        print("\tday = " + str(mcs.day))
        print("\ttype = " + str(mcs.type))
        print("\tlocation = " + str(mcs.location))


    @staticmethod
    def print_counts(counts):
        print("\tAll mentors: " + str(counts['all_mentor_count']))
        print("\tError mentors: " + str(counts['error_mentor_count']))
        print("\tVisible error mentors: " + str(counts['visible_error_mentor_count']))
        print("\tAll MentorCronofySchedule objects: " + str(counts['all_mcs_count']))
        print("\tError MentorCronofySchedule objects: " + str(counts['error_mcs_count']))
        print("\tVisible error MentorCronofySchedule objects: " + str(counts['visible_error_mcs_count']))


    @staticmethod
    def get_counts():
        error_mentor = Mentor.objects.filter(cronofy_office_hours_schedule__isnull=False, cronofy_office_hours_schedule__type__isnull=True).distinct()
        all_mentor_count = Mentor.objects.all().count()
        error_mentor_count = error_mentor.count()
        visible_error_mentor = error_mentor.filter(
            profile__user__cronofy_access_token__isnull=False,
            profile__user__cronofy_refresh_token__isnull=False,
            profile__user__cronofy_token_expiration__isnull=False)
        visible_error_mentor_count = visible_error_mentor.count()

        error_mcs = MentorCronofySchedule.objects.filter(type__isnull=True)
        all_mcs_count = MentorCronofySchedule.objects.all().count()
        error_mcs_count = error_mcs.count()
        visible_error_mcs_count = error_mcs.filter(mentor__in=visible_error_mentor).count()

        counts = {
            "all_mentor_count": all_mentor_count,
            "error_mentor_count": error_mentor_count,
            "visible_error_mentor_count": visible_error_mentor_count,
            "all_mcs_count": all_mcs_count,
            "error_mcs_count": error_mcs_count,
            "visible_error_mcs_count": visible_error_mcs_count,
        }

        return counts


    def handle(self, *args, **options):
        # Get print_verbose arg
        print_verbose = get_bool(options['print_verbose'])
        if print_verbose is None:
            print("Argument must be boolean ('true', 'True', 't', 'T', 'false', 'False', 'f', 'F')")
            return

        # Get and print before counts
        before_counts = Command.get_counts()
        print("BEFORE COUNTS: ")
        Command.print_counts(before_counts)
        print("")

        # Fix error MentorCronofySchedule objects
        error_mcs = MentorCronofySchedule.objects.filter(type__isnull=True)
        for mcs in error_mcs:
            if print_verbose:
                print("BEFORE MentorCronofySchedule object: ")
                Command.print_mcs_by_id(mcs.id)
            mcs.type = mcs.mentor.default_meeting_type
            mcs.save()
            if print_verbose:
                print("AFTER MentorCronofySchedule object: ")
                Command.print_mcs_by_id(mcs.id)
                print("")

        # conveniently reprint the before counts so user does not
        # need to scroll up.
        if print_verbose:
            print("BEFORE COUNTS: ")
            Command.print_counts(before_counts)
            print("")

        # Get and print after counts
        after_counts = Command.get_counts()
        print("AFTER COUNTS: ")
        Command.print_counts(after_counts)
        print("")
