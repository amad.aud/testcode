from django.core.management import BaseCommand
from mentorship.models import MentorshipSession
from StartupTree.utils.pretty import printer
from mentorship.management.commands.set_meeting_date import Command as Command2


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('days_ago')

    def handle(self, *args, **options):
        """
        python3 manage.py set_last_meeting_date <days_ago>

        sets the datetime field of the most recent single/team mentorship session
        to be the specified number of days ago (while preserving the original time of day).
        """
        days_ago = int(options['days_ago'])
        session: MentorshipSession = MentorshipSession.objects.last()

        if session.is_team_session():
            session = session.teammentorshipsession

        print('OLD SESSION: ')
        printer(session)
        print('')

        new_dt = Command2.get_new_datetime(session.datetime, days_ago)
        session.datetime = new_dt
        session.save()

        session.refresh_from_db()

        print('NEW SESSION: ')
        printer(session)

