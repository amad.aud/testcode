from django.core.management import BaseCommand
from mentorship.management.commands.utils import MentorshipSessionPrinter
from mentorship.models import MentorshipSession


class Command(BaseCommand):
    """
    python3 manage.py print_session <session_id>

    pretty prints that MentorshipSession object

    """
    def add_arguments(self, parser):
        parser.add_argument('session_id')

    def handle(self, *args, **options):
        session_id = options['session_id']
        session = MentorshipSession.objects.get(id=session_id)

        print_session = MentorshipSessionPrinter().print_session
        print_session(session)
