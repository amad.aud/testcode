from django.core.management import BaseCommand
from mentorship.models import (
    Mentor,
    MentorCronofySchedule,
    PREFERRED_CONTACT,
)


class Command(BaseCommand):
    """
    Command: print_mentorcronofyschedules <mentor email>
    Results: Pretty prints out all MentorCronofySchedule objects associated with Mentor.
    """

    def add_arguments(self, parser):
        parser.add_argument('mentor_email')

    @staticmethod
    def print_sched(sched):
        def to_string(kind, val):
            if kind == 'day':
                return dict(MentorCronofySchedule.DAYS)[val]
            elif kind == 'type':
                return dict(PREFERRED_CONTACT)[val]
            else:
                return str(val)

        print_attr = lambda attr: f"print('\tsched.{attr} = ' + to_string('{attr}', sched.{attr}))"

        attrs = [
            'mentor',
            'day',
            'type',
            'location',
        ]

        print('MentorCronofySchedule ID: ' + str(sched.id))

        for attr in attrs:
            exec(print_attr(attr))


    def handle(self, *args, **options):
        print('Hello World')

        mentor_email = options['mentor_email']
        # mentor_email = 'tm1@z.com'

        mentor = Mentor.objects.get(profile__user__email=mentor_email)

        scheds = MentorCronofySchedule.objects.filter(mentor=mentor)

        print('Mentor = ' + str(mentor))

        for sched in scheds:
            Command.print_sched(sched)

        print('DONE')
