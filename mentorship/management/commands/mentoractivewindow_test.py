from django.core.management import BaseCommand
from django.utils import timezone
from mentorship.models import Mentor, MentorActiveWindow
from StartupTree.utils import QuerySetType
from StartupTree.utils.pretty import printer
from datetime import date, timedelta


class Command(BaseCommand):
    """
    python3 manage.py mentoractivewindow_test <mentor_email> <is_set_active True/False> <year> <month> <day>

    Deletes all window objects associated with the mentor.

    Sets the mentor to be active or inactive.

    Creates a MentorActiveWindow that starts a day before the date specified.

    If mentor is to be active, the window will have None active_end.
    If mentor is to be inactive, the window will have active_end be one day after the date specified.
    """
    def handle(self, *args, **options):
        mentor = Mentor.objects.get(profile__user__email=options['mentor_email'])
        is_set_active = options['is_set_active'] in ['t', 'T', 'true', 'True', 'TRUE']
        year = int(options['year'])
        month = int(options['month'])
        day = int(options['day'])

        mentor.is_active = is_set_active
        mentor.save()

        old_windows: QuerySetType[MentorActiveWindow] = MentorActiveWindow.objects.filter(mentor=mentor)
        old_windows.delete()

        now = timezone.now()

        timestamp = now.replace(year=year, month=month, day=day) - timedelta(days=1)

        if is_set_active:
            active_end = None
        else:
            active_end = now.replace(year=year, month=month, day=day) + timedelta(days=1)

        window = MentorActiveWindow.objects.create(
            activity_type=MentorActiveWindow.MENTOR_BECAME_ACTIVE,
            mentor=mentor,
            timestamp=timestamp,
            active_end=active_end,
            platform=mentor.platform,
        )

        printer(window)

    def add_arguments(self, parser):
        parser.add_argument('mentor_email')
        parser.add_argument('is_set_active')
        parser.add_argument('year')
        parser.add_argument('month')
        parser.add_argument('day')