from django_elasticsearch_dsl import fields


MENTOR_FORMAT = {
    'platform': fields.ObjectField(properties={'id': fields.IntegerField()}),
    'is_active': fields.BooleanField(),
    'is_complete': fields.BooleanField(),
    'id': fields.IntegerField()
}
