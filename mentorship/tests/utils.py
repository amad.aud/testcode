from mentorship.models import Mentor, MentorshipSession, MentorScheduleBlackOut, CronofyCacheSlot
from trunk.models import UniversityStyle, University
import datetime as dt
from datetime import datetime
from django.urls import reverse


def activate_admin_approval(mentor: Mentor):
    mentor.allow_office_hour = True
    mentor.save()
    uni = University.objects.get(id=mentor.platform.id)
    uni_style = UniversityStyle.objects.get(_university=uni)
    uni_style.require_meeting_approval = 2
    uni_style.save()


def activate_mentor_approval(mentor: Mentor):
    mentor.approve_meetings = True
    mentor.save()


def accept_session_admin(mentor_session: MentorshipSession):
    mentor_session.is_admin_accepted = True
    mentor_session.save()


def reject_session_admin(mentor_session: MentorshipSession):
    mentor_session.is_mentor_accepted = False
    mentor_session.save()


def get_mentor_blackout_mentor(mentor: Mentor):
    return MentorScheduleBlackOut.objects.get(mentor=mentor)


def activate_mentor_cronofy(mentor: Mentor):
    if CronofyCacheSlot.objects.filter(mentor_user=mentor.profile.user, is_active=True).exists():
        return True
    else:
        CronofyCacheSlot.objects.create(mentor_user=mentor.profile.user,
                                        start_datetime=dt.date.today(),
                                        end_datetime=dt.date.today() - dt.timedelta(hours=-3),
                                        is_active=True)


def is_mentor_cronofy(mentor: Mentor):
    if CronofyCacheSlot.objects.filter(mentor_user=mentor.profile.user, is_active=True).exists():
        return True
    else:
        return False


def get_meeting_time():
    """
    it will return the meeting time, started from
    very next hour of current time
    """
    time = (datetime.now() - dt.timedelta(minutes=-30)).strftime("%b %d %Y %I:%M%p").split(" ")[-1]
    just_time = time.split(":")[0]
    just_time = int(just_time) + 1
    if just_time > 12:
        time_zone = "PM"
    else:
        time_zone = "AM"
    just_time = str(just_time) + ":00" + time_zone
    return just_time


def initiate_meeting(client, mentor: Mentor):
    # / mentorship / schedule / meeting
    data = {
        'count': 6,
        'm_id': mentor.profile.url_name,
        'start_time': get_meeting_time(),
        'duration': 2,
        'year': datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[2],
        'date': datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[0]
                + ' ' + datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[1],
        'day': 'null',
        'type': 4,
        'location': "None",
        'needs_approval': False,
        'meeting_topic': 'undefined',
        'mentee_timezone': 'Asia/Karachi'
    }
    response = client.post(reverse("mentorship-schedule-meeting"), data)
    return response


def admin_set_meeting(client, mentee, mentor: Mentor):
    payload = {
        "start_time": get_meeting_time(),
        "duration": "2",
        "year": datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[2],
        "date": datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[0]
                + ' ' + datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[1],
        "meeting_type": 3,
        "location": "Video Call",
        "selected_mentee_id": mentee.id,
        "selected_mentor_id": mentor.id
    }
    response = client.post(reverse("mentorship-schedule-add-by-ids"), payload)
    return response


def mentor_response_action(client, mentorship, action, reason=""):
    # action = decline,approve
    payload = {
        "meeting_id": mentorship.id,
        "decision": action,
        "reason": reason
    }
    response = client.post(reverse("mentorship-decide-meeting"), payload)
    return response


def admin_response_action_mentorship(client, mentorship_request, action, reason=""):
    # action = true, false
    payload = {
        "r_id": mentorship_request.id,
        "is_accept": action,
        "reason": reason
    }
    response = client.post(reverse("uni-mentorship-decide"), payload)
    return response


def mentee_request(client, mentor: Mentor):
    payload = {
        "m_id": mentor.profile.url_name,
        "venture-name-id": "",
        "pitch": "abcd testing",
        "other": "xyz"
    }
    response = client.post(reverse("mentorship-request"), payload)
    return response


def admin_response_action(client, mentorship, action, reason=""):
    # action = decline,approve
    payload = {
        "r_id": mentorship.id,
        "is_accept": action,
        "reason": reason
    }
    response = client.post(reverse("uni-meeting-decide"), payload)
    return response