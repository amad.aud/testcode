from auto_test_utility.utills import AbastractUniTestBase
from rest_framework.test import APIClient
from django.contrib import auth

from mentorship.factory.mentorship_new import GenerateAll, is_mentorship,  \
    get_mentorship_request,  get_mentorship, is_mentorship_active, \
    is_mentorship_session,  get_mentorship_session
from mentorship.tests.utils import *


class TestMenteeMentorRequestsRelationship(AbastractUniTestBase):

    def setUp(self):
        super(TestMenteeMentorRequestsRelationship, self).setUp()
        # generating 1 mentors, mentees
        self.platform_obj = GenerateAll(1, 1, self.cornell)
        self.platform_obj.generate()
    #
    # def login__user(self, user):
    #     response = self.platform_obj.login_user(self.client, user)
    #     self.assertEqual(200, response.status_code)
    #     user = auth.get_user(self.client)
    #     self.set_session(self.client, 'django_timezone', 'US/Eastern')
    #
    # def login_admin(self):
    #     response = self.platform_obj.login_user(self.client, self.admin.user.email)
    #     self.assertEqual(200, response.status_code)
    #     user = auth.get_user(self.client)
    #     self.set_session(self.client, 'django_timezone', 'US/Eastern')
    #
    def admin_action_meeting(self, session, action):
        self.login_user(self.client, self.admin.user.email, '1234')
        # admin responding on meeting
        response = admin_response_action(self.client, session, action)
        self.assertEqual(200, response.status_code)
        # Checking admin response
        # self.assertEqual(get_mentorship_session(self.platform_obj.get_mentee(),
        # self.platform_obj.get_mentor()).is_admin_accepted, action)

    def mentor_action_meeting(self, session, action):
        self.login_user(self.client, self.platform_obj.get_mentor().profile.user.email, '1234')

        response = mentor_response_action(self.client, session, action)
        # self.assertEqual(200, response.status_code)

    def mentee_setup_meeting(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')

        # Mentorship Session is not available till now, before requesting
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # Initiate meeting by mentee
        response = initiate_meeting(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Final check
        # Now, Query the backend to see if a MentorshipSession object exists
        self.assertTrue(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    # Testcases are number according to file
    def test_test_id_1_1_1(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')
        # pre conditions
        # Admin approval for requests turned on,

        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        # platform is already creating mentorship, so deleting them for now.
        self.platform_obj.deleteMentorship()
        # NOT ANY PRIOR MENTORSHIP EXISTS
        self.assertFalse(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # mentee requesting for meeting
        response = mentee_request(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # Final Check,
        # MentorshipRequest object created
        self.assertTrue(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_1_2(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')
        # pre conditions
        # Mentor approval for requests turned on,

        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # platform is already creating mentorship, so deleting them for now.
        self.platform_obj.deleteMentorship()
        # NOT ANY PRIOR MENTORSHIP EXISTS
        self.assertFalse(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # mentee requesting for meeting
        response = mentee_request(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # Final Check,
        # MentorshipRequest object created
        self.assertTrue(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_1_3(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')
        # pre conditions
        # Mentor approval for requests turned on,

        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # platform is already creating mentorship, so deleting them for now.
        self.platform_obj.deleteMentorship()
        # NOT ANY PRIOR MENTORSHIP EXISTS
        self.assertFalse(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # mentee requesting for meeting
        response = mentee_request(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # Final Check,
        # MentorshipRequest object created
        self.assertTrue(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_3_2(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')
        # pre conditions
        # Mentor approval for requests turned on,

        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # platform is already creating mentorship, so deleting them for now.
        self.platform_obj.deleteMentorship()
        # NOT ANY PRIOR MENTORSHIP EXISTS
        self.assertFalse(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # mentee requesting for meeting
        response = mentee_request(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # Final Check,
        # MentorshipRequest object created
        self.assertTrue(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        self.logout_user(self.client)
        self.login_user(self.client, self.admin.user.email, '1234')

        admin_response_action_mentorship(self.client, get_mentorship_request(self.platform_obj.get_mentee(),
                                                                             self.platform_obj.get_mentor()), True)
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_3_1(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')

        # pre conditions
        # Mentor approval for requests turned on,

        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # platform is already creating mentorship, so deleting them for now.
        self.platform_obj.deleteMentorship()
        # NOT ANY PRIOR MENTORSHIP EXISTS
        self.assertFalse(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # mentee requesting for meeting
        response = mentee_request(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # Final Check,
        # MentorshipRequest object created
        self.assertTrue(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        self.logout_user(self.client)
        self.login_user(self.client, self.admin.user.email, '1234')

        admin_response_action_mentorship(self.client, get_mentorship_request(self.platform_obj.get_mentee(),
                                                                             self.platform_obj.get_mentor()), False)

        self.assertFalse(is_mentorship_active(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_4_1(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # One time availability for open dlot
        self.platform_obj.generate_mentorship_schedule(False)

        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # mentee login, request meeting.
        self.mentee_setup_meeting()
        self.assertTrue(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_4_2(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # One time availability for repeating slot
        self.platform_obj.generate_mentorship_schedule(True)

        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # mentee login, request meeting.
        self.mentee_setup_meeting()
        self.assertTrue(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_4_3(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # Setting cronofy
        self.platform_obj.activate_mentor_cronofy(self.platform_obj.get_mentor())

        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # mentee login, request meeting.
        self.mentee_setup_meeting()

        self.assertTrue(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_test_id_1_5_1(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # mentee login, request meeting.
        self.mentee_setup_meeting()

        # created, unapproved
        self.assertIsNone(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)

        # Admin accepting request
        self.admin_action_meeting(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), True)

        # Final Check, MentorshipSession Update - is_admin_accepted = True
        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted,
            True)

    def test_test_id_1_5_2(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # mentee login, request meeting.
        self.mentee_setup_meeting()

        # created, unapproved
        self.assertIsNone(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)

        # Admin accepting request
        self.admin_action_meeting(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), False)

        # Final Check, MentorshipSession Update - is_admin_accepted = False
        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted,
            False)

    #
    def test_test_id_1_6_1(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # mentee login, request meeting.
        self.mentee_setup_meeting()

        # unapproved
        self.assertIsNone(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)

        # Admin accepting request
        self.admin_action_meeting(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), True)

        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(),
                                   self.platform_obj.get_mentor()).is_admin_accepted,
                                    True)

        # till now first step done, now, mentor will accept or reject it.

        self.logout_user(self.client)
        self.mentor_action_meeting(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), 'approve')
        # Final Check, MentorshipSession Update - is_mentor_accepted = True
        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_mentor_accepted,
            True)

    def test_test_id_1_6_2(self):
        self.platform_obj.activate_admin_approval(self.platform_obj.get_mentor())
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        self.platform_obj.activate_mentor_approval(self.platform_obj.get_mentor())
        # mentee login, request meeting.
        self.mentee_setup_meeting()

        # unapproved
        self.assertIsNone(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)
        # Admin accepting request
        self.admin_action_meeting(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), True)
        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted,
            True)

        # till now first step done, now, mentor will accept or reject it.

        self.logout_user(self.client)
        self.mentor_action_meeting(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), 'decline')
        # Final Check, MentorshipSession Update - is_mentor_accepted = False
        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_mentor_accepted,
            False)