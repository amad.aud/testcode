from auto_test_utility.utills import AbastractUniTestBase

from mentorship.factory.mentorship_new import GenerateAll, is_mentorship, get_mentorship, \
    get_mentorship_request,  is_mentorship_session, get_mentorship_session, \
    set_mentor_for_approval
from mentorship.tests.utils import *


class TestMenteeMentorRequestsRelationship(AbastractUniTestBase):

    def setUp(self):
        super(TestMenteeMentorRequestsRelationship, self).setUp()
        # generating 1 mentors, mentees
        self.platform_obj = GenerateAll(1, 1, self.cornell)
        self.platform_obj.generate()

    def test_mentee_request_mentor(self):
        self.login_user(self.client,self.platform_obj.get_mentee().user.email,'1234')
        self.assertFalse(self.platform_obj.get_mentor().allow_office_hour)
        # platform is already creasting mentorship, so deleting them for now.
        self.platform_obj.deleteMentorship()
        # NOT ANY PRIOR MENTERSHIP EXISTS
        self.assertFalse(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # mentee requesting for meeting
        response = mentee_request(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # Final Check,
        # if a MentorshipRequest object exists
        self.assertTrue(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # and is unapproved (ie: mentor has not rejected or approved it)
        self.assertEqual(get_mentorship_request(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_mentor_accepted, False)

    def test_mentee_request_meeting(self):
        self.login_user(self.client, self.platform_obj.get_mentee().user.email, '1234')
        self.assertFalse(self.platform_obj.get_mentor().allow_office_hour)
        # Checking wither mentorship created b/w mentee and mentor
        self.assertTrue(is_mentorship(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

        # Mentorship Session is not available till now, before requesting
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        self.platform_obj.generate_mentorship_schedule(False)

        # Initiate meeting by mentee
        response = initiate_meeting(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Final check
        # Now, Query the backend to see if a MentorshipSession object exists
        self.assertTrue(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        # unapproved (ie: mentor & admin have not rejected or approved it)
        self.assertFalse(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_approved())



    def admin_action_meeting(self, session, action):
        self.login_user(self.client,self.admin.user.email, '1234')
        # admin responding on meeting
        response = admin_response_action(self.client, session, action)
        self.assertEqual(200, response.status_code)
        # Checking admin response
        # self.assertEqual(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted, action)

    def mentor_action_meeting(self, session, action):
        self.login_user(self.client,self.platform_obj.get_mentor().profile.user.email, '1234')

        response = mentor_response_action(self.client, session, action)
        # self.assertEqual(200, response.status_code)

    def mentee_setup_meeting(self):
        self.login_user(self.client,self.platform_obj.get_mentee().user.email, '1234')

        # allow office hours
        self.assertFalse(self.platform_obj.get_mentor().allow_office_hour)
        # Mentorship Session is not available till now, before requesting
        self.assertFalse(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))
        self.platform_obj.generate_mentorship_schedule(False)

        # Initiate meeting by mentee
        response = initiate_meeting(self.client, self.platform_obj.get_mentor())
        self.assertEqual(200, response.status_code)
        # Final check
        # Now, Query the backend to see if a MentorshipSession object exists
        self.assertTrue(is_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()))

    def test_admin_accept_meeting(self):
        # metee login, request meeting.
        self.mentee_setup_meeting()

        # unapproved
        self.assertIsNone(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)

        # Admin accepting request
        self.admin_action_meeting(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), True)

        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted,
            True)

    def test_admin_reject_meeting(self):
        # metee login, request meeting.
        self.mentee_setup_meeting()

        # unapproved
        self.assertIsNone(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)

        # Admin accepting request
        self.admin_action_meeting(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), False)

        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted,
            False)

    def test_mentor_meeting_accept(self):
        # metee login, request meeting.
        self.mentee_setup_meeting()

        # unapproved
        self.assertIsNone(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted)
        # logging out mentee as user
        self.logout_user(self.client)

        # Admin accepting request
        self.admin_action_meeting(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), True)

        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_admin_accepted,
            True)

        # till now first step done, now, mentor will accept or reject it.

        self.logout_user(self.client)

        self.mentor_action_meeting(get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()), 'approve')

        self.assertEqual(
            get_mentorship_session(self.platform_obj.get_mentee(), self.platform_obj.get_mentor()).is_mentor_accepted,
            True)