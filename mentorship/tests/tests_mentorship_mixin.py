from django.test import TestCase, Client
from django.test.utils import override_settings
from django.urls import reverse
from mentorship.models import Mentor, MentorshipRequest, MentorSchedule, \
    MentorshipSession, Mentorship
from trunk.models import University
from StartupTree.settings import PROJECT_ROOT
from StartupTree.strings import *
import json
import datetime
import os


class MentorshipMixinTest(TestCase):
    """
    Test for mixin of user and admin such as request granted and
    create new session, etc.
    """
    serialized_rollback = True

    def setUp(self):
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)
        self.student_email = "mjurai@sunghopark.com"
        self.admin_email = "cornell@startuptree.co"
        self.password = "1234"
        self.platform = University.objects.get(short_name='cornell')
        self.mentor = Mentor.objects.select_related('profile').filter(platform=self.platform)[0]

    def mentor_create_timeslot(self):
        tmp = self.mentor.profile.user
        tmp.set_password(self.password)
        tmp.save()
        self.client.post(reverse('regular_login'),
                         {'email': self.mentor.profile.user.email,
                          'password': self.password})
        slots = [{"day": 3, "hour": 15, "minute": 0, "duration": 30},
                 {"day": 3, "hour": 16, "minute": 0, "duration": 30},
                 {"day": 1, "hour": 12, "minute": 30, "duration": 30}]
        payload = {"timeslots": json.dumps(slots)}
        response = self.client.post(reverse('mentor-timeslot-create'), payload)
        json = json.loads(response.content)
        self.assertEquals(json[PAYLOAD_STATUS], 200)
        self.assertTrue(MentorSchedule.objects.get(mentor=self.mentor, day=3, time="15:00", duration=2))
        self.assertTrue(MentorSchedule.objects.get(mentor=self.mentor, day=3, time="16:00", duration=2))
        self.assertTrue(MentorSchedule.objects.get(mentor=self.mentor, day=1, time="12:30", duration=2))
        self.client.post(reverse('logout'))
        return True

    def create_request(self):
        """
        Student create mentorship request
        :return: MentorshipRequest object that just got created.
        """
        self.client.post(reverse('regular_login'),
                         {'email': self.student_email,
                          'password': self.password})
        title = "Requesting Mentor!"
        content = "I demand you to be my mentor! I demand this to you!" \
                  "You have to listen to me! Otherwise something weird will" \
                  "happen. Please..."
        payload = {
            "m_id": self.mentor.profile.url_name,
            "title": title,
            "text": content
        }
        response = self.client.post(reverse('mentorship-request'), payload)
        json = json.loads(response.content)
        self.assertEquals(json[PAYLOAD_STATUS], 200)
        self.client.post(reverse('logout'))
        return MentorshipRequest.objects.get(id=json["n_rid"])

    def grant_mentorship(self):
        """
        See if mentorship request is accepted as it should.
        :return: Mentorship object that has just been granted.
        """
        mentor_request = self.create_request()
        self.assertIsNone(mentor_request.requested_mentorship.accepted_request)
        self.client.post(reverse('regular_login'),
                         {'email': self.admin_email,
                          'password': self.password})
        payload = {"r_id": mentor_request.id,
                   "a": True}
        response = self.client.post(reverse('uni-mentorship-decide'),
                                    payload)
        json = json.loads(response.content)
        self.assertEquals(json[PAYLOAD_STATUS], 200)
        updated_request = MentorshipRequest.objects.get(id=mentor_request.id)
        self.assertIsNotNone(updated_request.requested_mentorship.accepted_request)
        self.client.post(reverse('logout'))
        return updated_request.requested_mentorship

    # def test_schedule_success(self):
    #     """
    #     Student select a timeslot from available schedule and make an appointment.
    #     :return:
    #     """
    #     self.mentor_create_timeslot()
    #     mentorship = self.grant_mentorship()
    #     self.client.post(reverse('regular_login'),
    #                      {'email': self.student_email,
    #                       'password': self.password})
    #     date = datetime.date.today() + datetime.timedelta(days=5)
    #     date = date.isoformat()
    #     selected_slot = MentorSchedule.objects.all()[0]
    #     payload = {"date": date,
    #                "slot_id": selected_slot.id}
    #     response = self.client.post(reverse('mentorship-schedule-add',
    #                                         kwargs={"mentorship_id": mentorship.id}), payload)
    #     json = json.loads(response.content)
    #     self.assertEquals(json[PAYLOAD_STATUS], 200, "{0}".format(json[PAYLOAD_MSG]))
    #     self.assertTrue(MentorshipSession.objects.filter(id=json["s_id"]).exists())
