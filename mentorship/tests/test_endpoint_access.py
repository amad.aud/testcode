from auto_test_utility.utills import EndpointAccessTest
from django.urls.base import reverse
from app.user_types import *


class MentorshipEndpointAccessTest(EndpointAccessTest):
    def test_mentorship_url(self):
        self.check_endpoint_permission(reverse('mentor-dashboard'), permissions=(USER_TYPE_MENTOR, ))
        # TODO SW-3254

    def test_team_mentorship_url(self):
        pass
