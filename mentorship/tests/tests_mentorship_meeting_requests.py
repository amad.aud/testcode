from auto_test_utility.utills import AbastractUniTestBase
from mentorship.factory.mentorship_new import GenerateAll,  generate_mentorship
from mentorship.models import MentorshipSession, MentorSchedule
from mentorship.tests.utils import *


class TestMeetingRequests(AbastractUniTestBase):
    """
    Disabled - NA = 1
    Enabled - Approved = 2
    Enabled - Rejectec = 3
    """

    def setUp(self):
        super(TestMeetingRequests, self).setUp()

        self.mentor_gen = GenerateAll(20, 20, self.cornell)
        self.mentor_gen.generate()

    def test_mentee_request_9(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(0)
        generate_mentorship(mentee, mentor)

        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(2, 1, 1, 1, True, mentee, mentor)

    # # # a => admin, ac=> accept
    def test_mentee_request_10(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(1)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(2, 1, 1, 1, True,mentee, mentor)
    #
    def test_mentee_request_11(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(2)
        mentee = self.mentor_gen.get_mentee_single(2)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(3, 1, 1, 1, True, mentee,mentor)

    def test_mentee_request_12(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(3)
        mentee = self.mentor_gen.get_mentee_single(3)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(1, 2, 1, 1, True,mentee, mentor)

    def test_mentee_request_13(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(4)
        mentee = self.mentor_gen.get_mentee_single(4)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(2, 2, 1, 1, True, mentee, mentor)
    #
    def test_mentee_request_14(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(5)
        mentee = self.mentor_gen.get_mentee_single(5)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(3, 2, 1, 1, True, mentee, mentor)
    #
    # # #
    def test_mentee_request_15(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(6)
        mentee = self.mentor_gen.get_mentee_single(6)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(1, 3, 1, 1, True, mentee, mentor)

    def test_mentee_request_16(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(6)
        mentee = self.mentor_gen.get_mentee_single(6)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 3, 1, 1, True, mentee, mentor)

    def test_mentee_request_17(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(7)
        mentee = self.mentor_gen.get_mentee_single(7)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 3, 1, 1, True, mentee, mentor)

    def test_mentee_request_18(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(8)
        mentee = self.mentor_gen.get_mentee_single(8)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 1, 2, 1, True, mentee, mentor)

    def test_mentee_request_19(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(9)
        mentee = self.mentor_gen.get_mentee_single(9)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 1, 2, 1, True, mentee, mentor)

    def test_mentee_request_20(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(10)
        mentee = self.mentor_gen.get_mentee_single(10)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 1, 2, 1, True, mentee, mentor)

    def test_mentee_request_21(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(11)
        mentee = self.mentor_gen.get_mentee_single(12)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(1, 2, 2, 1, True, mentee, mentor)

    def test_mentee_request_22(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(12)
        mentee = self.mentor_gen.get_mentee_single(12)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 2, 2, 1, True, mentee, mentor)

    def test_mentee_request_23(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(13)
        mentee = self.mentor_gen.get_mentee_single(13)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 2, 2, 1, True, mentee, mentor)

    def test_mentee_request_24(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(14)
        mentee = self.mentor_gen.get_mentee_single(14)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 3, 2, 1, True, mentee, mentor)

    def test_mentee_request_25(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(15)
        mentee = self.mentor_gen.get_mentee_single(15)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 3, 2, 1, True, mentee, mentor)
    #
    def test_mentee_request_26(self):
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(16)
        mentee = self.mentor_gen.get_mentee_single(16)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 3, 2, 1, True, mentee, mentor)

    def test_mentee_request_27(self):
        # Disabled - NA	Disabled - NA	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(17)
        mentee = self.mentor_gen.get_mentee_single(17)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 1, 3, 1, True, mentee, mentor)

    def test_mentee_request_28(self):
        # Enabled - Approved	Disabled - NA	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(18)
        mentee = self.mentor_gen.get_mentee_single(18)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 1, 3, 1, True, mentee, mentor)

    def test_mentee_request_29(self):
        # Enabled - Rejectec	Disabled - NA	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(19)
        mentee = self.mentor_gen.get_mentee_single(19)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 1, 3, 1, True, mentee, mentor)

    def test_mentee_request_30(self):
        # Disabled - NA	Enabled - Approved	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(18)
        mentee = self.mentor_gen.get_mentee_single(19)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 2, 3, 1, True, mentee, mentor)

    def test_mentee_request_31(self):
        # Enabled - Approved	Enabled - Approved	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(1)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 2, 3, 1, True, mentee, mentor)

    def test_mentee_request_32(self):
        # Enabled - Rejectec	Enabled - Approved	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        # mentor n mentee for this test case
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(2)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 2, 3, 1, True, mentee, mentor)

    def test_mentee_request_33(self):
        # Disabled - NA	Enabled - Rejected	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(3)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(1, 3, 3, 1, True, mentee, mentor)

    def test_mentee_request_34(self):
        # Enabled - Approved	Enabled - Rejected	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(4)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(2, 3, 3, 1, True, mentee, mentor)

    def test_mentee_request_35(self):
        # Enabled - Rejected	Enabled - Rejected	Cronofy	Has Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(5)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(3, 3, 3, 1, True, mentee, mentor)

    def test_mentee_request_36(self):
        # Disabled - NA	Disabled - NA	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(6)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(1, 1, 1, 2, False, mentee, mentor)

    def test_mentee_request_37(self):
        # Enabled - Approved	Disabled - NA	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(7)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(2, 1, 1, 2, False, mentee, mentor)

    def test_mentee_request_38(self):
        # Enabled - Rejectec	Disabled - NA	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(9)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(3, 1, 1, 2, True, mentee, mentor)

    def test_mentee_request_39(self):
        # Disabled - NA	Enabled - Approved	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(10)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(1, 2, 1, 2, False, mentee, mentor)

    #
    def test_mentee_request_40(self):
        # Enabled - Approved	Enabled - Approved	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(11)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)

        self.meeting_test_case(2, 2, 1, 2, False, mentee, mentor)

    def test_mentee_request_41(self):
        # Enabled - Rejectec	Enabled - Approved	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(12)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 2, 1, 2, True, mentee, mentor)

    def test_mentee_request_42(self):
        # Enabled - Approved	Enabled - Approved	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(13)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 3, 1, 2, True, mentee, mentor)

    def test_mentee_request_43(self):
        # Enabled - Approved	Enabled - Rejected	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(14)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 3, 1, 2, True, mentee, mentor)

    def test_mentee_request_44(self):
        # Enabled - Rejectec	Enabled - Rejected	One-Time
        # No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(15)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 3, 1, 2, True, mentee, mentor)

    def test_mentee_request_45(self):
        # Disabled - NA	Disabled - NA	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(16)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 1, 2, 2, False, mentee, mentor)

    def test_mentee_request_46(self):
        # Enabled - Approved	Disabled - NA	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(17)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 1, 2, 2, False, mentee, mentor)

    def test_mentee_request_47(self):
        # Enabled - Rejectec	Disabled - NA	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(18)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 1, 2, 2, True, mentee, mentor)

    def test_mentee_request_48(self):
        # Disabled - NA	Enabled - Approved	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(0)
        mentee = self.mentor_gen.get_mentee_single(19)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 2, 2, 2, False, mentee, mentor)

    def test_mentee_request_49(self):
        # Enabled - Approved	Enabled - Approved	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(2)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 2, 2, 2, False, mentee, mentor)

    def test_mentee_request_50(self):
        # Enabled - Rejectec	Enabled - Approved	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(3)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 2, 2, 2, True, mentee, mentor)

    def test_mentee_request_51(self):
        # Disabled - NA	Enabled - Rejected	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(4)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 3, 2, 2, True, mentee, mentor)

    def test_mentee_request_52(self):
        # Enabled - Approved	Enabled - Rejected	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(5)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 3, 2, 2, True, mentee, mentor)

    def test_mentee_request_53(self):
        # Enabled - Rejectec	Enabled - Rejected	Weekly	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(6)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 3, 2, 2, True, mentee, mentor)

    def test_mentee_request_54(self):
        # Disabled - NA	Disabled - NA	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(7)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 1, 3, 2, False, mentee, mentor)

    def test_mentee_request_55(self):
        # Enabled - Approved	Disabled - NA	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(8)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 1, 3, 2, False, mentee, mentor)

    def test_mentee_request_56(self):
        # Enabled - Rejectec	Disabled - NA	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(9)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 1, 3, 2, True, mentee, mentor)

    def test_mentee_request_57(self):
        # Disabled - NA	Enabled - Approved	Cronofy	No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=False

        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(10)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 2, 3, 2, False, mentee, mentor)

    def test_mentee_request_58(self):
        # Enabled - Approved	Enabled - Approved	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=False

        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(11)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 2, 3, 2, False, mentee, mentor)

    def test_mentee_request_59(self):
        # Enabled - Rejectec	Enabled - Approved	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(12)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 2, 3, 2, True, mentee, mentor)

    #
    def test_mentee_request_60(self):
        # Disabled - NA	Enabled - Rejected	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True
        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(13)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(1, 3, 3, 2, True, mentee, mentor)

    def test_mentee_request_61(self):
        # Enabled - Approved	Enabled - Rejected	Cronofy	No Blackout Day	Mentee requests meeting	Mentorshipsession object attr is_cancelled=True

        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(14)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(2, 3, 3, 2, True, mentee, mentor)

    def test_mentee_request_62(self):
        # Enabled - Rejectec	Enabled - Rejected	Cronofy	No Blackout Day
        # Mentee requests meeting	Mentorshipsession object attr is_cancelled=True

        mentor = self.mentor_gen.get_mentor_single(1)
        mentee = self.mentor_gen.get_mentee_single(16)
        generate_mentorship(mentee, mentor)
        # setting up schduling
        self.mentor_gen.generate_mentorship_schedule_mentor(mentor, False)
        self.mentor_schedule = MentorSchedule.objects.get(mentor=mentor)

        self.login_user(self.client,mentee.user.email, '1234')
        # adding blackout day
        self.mentor_gen.generate_mentorship_schedule_blackout_mentor(mentor)
        self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)
        self.meeting_test_case(3, 3, 3, 2, True, mentee, mentor)

    def meeting_test_case(self, admin_approval, mentor_approval, mentor_availability, is_blackout, final_result, mentee, mentor):
        response = initiate_meeting(self.client, mentor)
        self.assertEqual(200, response.status_code)

        self.mentor_session = MentorshipSession.objects.get(mentorship=self.mentor_gen.get_mentorship_bw(mentee, mentor))
        # pre work for admin_approval

        if mentor_approval == 2:  # accepted
            activate_mentor_approval(mentor)
            self.mentor_session.mentor_accept_session()
        elif mentor_approval == 3:  # rejected
            activate_mentor_approval(mentor)
            self.mentor_session.mentor_reject_session()

        if admin_approval == 2:
            activate_admin_approval(mentor)
            accept_session_admin(self.mentor_session)
        elif admin_approval == 3:
            activate_admin_approval(mentor)
            self.mentor_session.admin_reject_session()

        if mentor_availability == 2:
            self.mentor_schedule.is_repeating = True
            self.mentor_schedule.save()
            self.assertEqual(self.mentor_schedule.is_repeating, True)
        elif mentor_availability == 3:
            activate_mentor_cronofy(mentor)
            self.assertEqual(is_mentor_cronofy(mentor), True)
        elif mentor_availability == 1:
            self.mentor_schedule.is_repeating = False
            self.mentor_schedule.save()
            self.assertEqual(self.mentor_schedule.is_repeating, False)
        if is_blackout == 1:
            self.mentor_gen.blackout_check(self.mentor_session)
            self.assertEqual(get_mentor_blackout_mentor(mentor).is_expired(), False)


        self.assertEqual(self.mentor_session.is_cancelled, final_result)


