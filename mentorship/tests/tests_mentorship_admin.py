from django.test import TestCase, Client
from django.test.utils import override_settings
from django.urls import reverse
from mentorship.models import Mentor, MentorshipRequest
from trunk.models import University
from branch.models import StartupMember
from StartupTree.settings import PROJECT_ROOT
from StartupTree.strings import *
import json
import os


@override_settings(
    IS_TEST=True,
    EMAIL_BACKEND='django.core.mail.backends.filebased.EmailBackend',
    EMAIL_FILE_PATH=os.path.join(PROJECT_ROOT, 'tmp'),
)
class MentorAdminTest(TestCase):
    serialized_rollback = True

    def setUp(self):
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)
        self.admin_email = "cornell@startuptree.co"
        self.password = "1234"
        self.client.post(reverse('regular_login'),
                         {'email': self.admin_email,
                          'password': self.password})
        self.platform = University.objects.get(short_name='cornell')
        # self.mentor = Mentor.objects.select_related('profile').filter(platform=self.platform)[0]
        self.student_email = "gintoki@sunghopark.com"
        self.new_mentor = "mentormentor@sunghopark.com"

    def create_mentor(self):
        new_mentor = self.new_mentor
        payload = {
            "mentor_email": new_mentor,
            "mentor_first_name": "Mentor",
            "mentor_last_name": "Park",
        }
        self.assertFalse(Mentor.objects.filter(profile__user__email=new_mentor).exists())
        response = self.client.post(reverse("manage-mentor-add"), payload)
        json = json.loads(response.content)
        self.assertEquals(json[PAYLOAD_STATUS], 200)
        self.assertTrue(Mentor.objects.filter(profile__user__email=new_mentor).exists())

    def test_invite_success(self):
        new_mentor = self.new_mentor
        payload = {
            "mentor_email": new_mentor,
            "mentor_first_name": "Mentor",
            "mentor_last_name": "Park",
        }
        self.create_mentor()

        self.assertFalse(Mentor.objects.filter(profile__user__email=self.student_email).exists())
        payload["mentor_email"] = self.student_email
        response = self.client.post(reverse("manage-mentor-add"), payload)
        json = json.loads(response.content)
        self.assertEquals(json[PAYLOAD_STATUS], 200)
        self.assertTrue(Mentor.objects.filter(profile__user__email=self.student_email).exists())

    def test_update_mentor(self):
        self.create_mentor()
        mentor = Mentor.objects.filter(profile__user__email=self.new_mentor)[0]
        payload = {
            "m_id": mentor.id
        }
        # disable mentor
        self.client.post(reverse("manage-mentor-update"), payload)
        latest_mentor = Mentor.objects.get(id=mentor.id)
        self.assertFalse(latest_mentor.is_active)
        # enable mentor
        self.client.post(reverse("manage-mentor-update"), payload)
        latest_mentor = Mentor.objects.get(id=mentor.id)
        self.assertTrue(latest_mentor.is_active)
