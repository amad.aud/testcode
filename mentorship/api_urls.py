from django.conf.urls import url, include
from mentorship.views import api as men_view

urlpatterns = [
    url(r'^discover$', men_view.discover_mentor_view, name='api_discover_mentors'),

]
