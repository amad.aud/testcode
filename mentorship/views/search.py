from django.http import JsonResponse
from elasticsearch_dsl import Q
from branch.documents import StartupMemberDocument
from mentorship.documents import MentorDocument
from StartupTree.utils.search import escape_query
from django.contrib.auth.decorators import login_required
from trunk.decorators import only_university


@only_university
@login_required
def mentor_autocomplete(request, *args, **kwargs):
    query = request.GET.get("term", '')
    query = escape_query(query)
    if not query:
        query = request.GET.get("q", '')
        query = escape_query(query)
    suggestions = []
    if not query:
        return JsonResponse(suggestions, safe=False)
    uni = request.university

    if "search_member" in kwargs and kwargs["search_member"]:
        sq = StartupMemberDocument().search() \
            .query(Q("nested",
                     path="university",
                     query=Q("term", university__id=uni.id)))
    else:
        sq = MentorDocument().search()\
            .query(Q("nested",
                     path="university",
                     query=Q("term", university__id=uni.id)))

    if "team_mentor" in kwargs and kwargs["team_mentor"]:
        sq = sq.filter('match', is_team_mentor=True)
    queries = query.split(" ")
    query_string = ""
    size = len(queries)
    for idx, tmp in enumerate(queries):
        if idx == size - 1:
            query_string += "+{0}*".format(tmp)
        else:
            query_string += "+{0} ".format(tmp)

    sq = sq.filter('query_string', fields=['suggest'], query=query_string)[:5]
    response = sq.execute()
    for result in response:
        try:
            email = result.email
        except AttributeError:
            email = result.user.email
        data = {
            'name': result.name,
            'url_id': result.url_name,
            'value': result.url_name,
            'img': result.image,
            'email': email,
            'bio': result.bio
        }
        suggestions.append(data)
    return JsonResponse(suggestions, safe=False)
