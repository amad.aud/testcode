# django imports
import json
from datetime import datetime
# startuptree imports
from django.core.cache import cache
from django.db import transaction
from django.db.models import Q
from django.db.models.query import QuerySet
from django.utils import timezone

from StartupTree.loggers import app_logger
from StartupTree.strings import *
# branch imports
from branch.models import Startup
from emailuser.models import UserEmail
# forms imports
from forms.parser import CustomAnswersParser
# mentor imports
from mentorship.constants import MENTORSHIPSESSION_LOCK
from mentorship.models import (
    PHONE, SKYPE, IN_PERSON, TO_BE_DETERMINED,
    Mentorship,
    MentorshipActivity,
    MentorshipSession,
    TeamMentorshipActivity,
    TeamMentorship,
    TeamMentorshipSession,
    Mentor,
    MentorshipRequest,
    MentorshipSessionSet,
)
from mentorship.strings import *
from mentorship.tasks import (
    session_change_noti_mentee,
    session_change_noti_mentor,
    session_request_noti_uni,
    session_request_noti_mentor,
    team_session_changed_noti_admin,
)
from mentorship.utils import localize, get_formatted_time
from trunk.perms import is_admin_authorized
from mentorship.views.gcal import make_gcal_event
from mentorship.st_cronofy import get_cronofy_meeting_type_location
from mentorship.views.teams import TeamMentorshipView
# trunk imports
from trunk.models import (
    University,
    UniversityStyle,
    AccessLevel,
)
# venture imports
from venture.models import MemberToAdmin


PAST_TIMESLOT_MSG = "You cannot reserve the past timeslot."
PENDING_TIMESLOT_MSG = "There is a pending meeting request. Try reserving other slots."
RESERVED_TIMESLOT_MSG = 'The timeslot is already taken.'


def is_duplicate_team_mentorship_session(team: Startup,
                                         scheduled_datetime: datetime,
                                         mentorships: QuerySet):
    """
    A team mentorship session is a duplicate if there exists another team mentorship session such thast:
       - Team and datetime are equal
       - is_cancelled is False
       - at least one mentor overlaps in the mentorships field.
    """
    query = TeamMentorshipSession.objects.filter(
        team=team,
        datetime=scheduled_datetime,
        is_cancelled=False)
    expression = Q()
    for mentorship in mentorships:
        expression |= Q(mentorships=mentorship)
    return query.filter(expression).exists()


def create_scheduled_session(mentorship, location, duration, year, selected_day,
                             selected_date, selected_start_time, session_timezone,
                             platform, meeting_type, meeting_topic, is_team=False, team=None, creator=None, mentee_ids=None):
    """
    If regular mentorship: `mentorship` is of type Mentorship
    If team mentorship: `mentorship` is of type QuerySet of TeamMentorship

    mentee_ids: list of mentee url_names to be added as attending users to team mentorship session.
        Applies only to team mentorship.
        If omitted, then all team mentees (page owners of the venture) are added as attending users.
    """

    duration = int(duration)

    if meeting_type == 'null':
        meeting_type = ''

    if meeting_type == '':
        #  meeting_type == '' => this is a cronofy scheduled meeting
        if not is_team:
            meeting_type, location = get_cronofy_meeting_type_location(mentorship, location, selected_day)
        else:
            # if nothing specified, to be determined
            meeting_type = '5'
    tmp_date, tmp_time = get_formatted_time(year, selected_date, selected_start_time)

    # pytz issue:
    # https://stackoverflow.com/questions/35462876/python-pytz-timezone-function-returns-a-timezone-that-is-off-by-9-minutes
    scheduled_datetime = localize(datetime(
        tmp_date.year,
        tmp_date.month,
        tmp_date.day,
        tmp_time.hour,
        tmp_time.minute
    ), session_timezone)

    if scheduled_datetime <= timezone.now():
        app_logger.info('past timeslot : {0}'.format(scheduled_datetime))
        raise ValueError(PAST_TIMESLOT_MSG)

    # Is requested meeting time available?
    # Checks if a session already exists

    if not is_team:
        timeslot_taken = MentorshipSession.objects.select_related('mentorship').filter(duration=duration,
                                                                                       datetime=scheduled_datetime,
                                                                                       is_cancelled=False,
                                                                                       mentorship__mentor_id=mentorship.mentor_id)
    # for team mentorship, have to check regular & team mentorship time taken of the mentor.
    else:
        # if is_team, mentorship is a TeamMentorship Queryset.
        mentor_ids = mentorship.values_list('mentor_id', flat=True)
        timeslot_taken = TeamMentorshipSession.objects.select_related('team')\
            .filter(duration=duration, datetime=scheduled_datetime, is_cancelled=False)\
            .filter(Q(mentorship__mentor_id__in=mentor_ids) | Q(mentorships__mentor_id__in=mentor_ids))
    if timeslot_taken.exists():
        if timeslot_taken.filter(Q(is_admin_accepted__isnull=True) | Q(is_mentor_accepted__isnull=True)).exists():
            app_logger.info('pending timeslot : {0}'.format(scheduled_datetime))
            raise ValueError(PENDING_TIMESLOT_MSG)
        app_logger.info('already taken timeslot : {0}'.format(scheduled_datetime))
        raise ValueError(RESERVED_TIMESLOT_MSG)

    try:
        day_of_week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat']
        location = json.loads(location)
        location = location[day_of_week[scheduled_datetime.weekday()]]
    except:
        pass

    if location == 'None':
        location = None

    phone_number = None
    video_id = None
    if int(meeting_type) == PHONE:
        phone_number = location
        location = None
    elif int(meeting_type) == SKYPE:
        video_id = location
        location = None
    elif int(meeting_type) == TO_BE_DETERMINED:
        location = None
    if meeting_topic == 'None' or meeting_topic == 'undefined' or meeting_topic == 'null' or meeting_topic == '':
        meeting_topic = None

    # app_logger.info("{0} | {1} | {2} | {3} | {4} | {5} | {6}".format(mentorship,
    #     duration,
    #     scheduled_datetime,
    #     mentorship_activity,
    #     meeting_type,
    #     location,
    #     meeting_topic))

    with cache.lock(MENTORSHIPSESSION_LOCK, expire=10):
        if is_team:
            if not is_duplicate_team_mentorship_session(team, scheduled_datetime, mentorship):
                mentorship_activity = TeamMentorshipActivity.objects.create_activity(MentorshipActivity.SESSION_SCHEDULED, team, platform)
                new_session = TeamMentorshipSession.objects.create(
                    team=team,
                    creator=creator,
                    timezone=session_timezone,
                    duration=duration,
                    datetime=scheduled_datetime,
                    mentorship_activity=mentorship_activity,
                    meeting_type=int(meeting_type),
                    location=location,
                    phone_number=phone_number,
                    video_id=video_id,
                    meeting_topic=meeting_topic
                )
                if mentee_ids:
                    for mentee_user in UserEmail.objects.filter(startup_member__url_name__in=mentee_ids):
                        new_session.members_attending.add(mentee_user)
                else:
                    for admin in MemberToAdmin.objects.select_related('member', 'member__user').filter(startup_id=team.id):
                        new_session.members_attending.add(admin.member.user)
                new_session.mentorships.add(*mentorship)
            else:
                raise ValueError("Duplicate Team Mentorship Session request.")
        else:
            if not MentorshipSession.objects.filter(mentorship=mentorship, datetime=scheduled_datetime, is_cancelled=False).exists():
                mentorship_activity = MentorshipActivity.objects.make_mentorship_activity(MentorshipActivity.SESSION_SCHEDULED, platform)
                new_session = MentorshipSession.objects.create(
                    mentorship=mentorship,
                    duration=duration,
                    datetime=scheduled_datetime,
                    mentorship_activity=mentorship_activity,
                    meeting_type=int(meeting_type),
                    location=location,
                    phone_number=phone_number,
                    video_id=video_id,
                    meeting_topic=meeting_topic
                )
            else:
                raise ValueError("Duplicate Mentorship Session request.")
    return new_session


def session_scheduling(selected_date, selected_start_time, duration, year, selected_day,
                meeting_type, location, meeting_topic, mentorship, platform: University, platform_style: UniversityStyle, is_admin,
                django_timezone, post_data, post_files, is_team=False, is_multiple=False, creator=None, team=None, mentee_ids=None):
    """
    If regular mentorship: `mentorship` is of type Mentorship
    If team mentorship: `mentorship` is of type QuerySet of TeamMentorship

    mentee_ids: list of mentee url_names to be added as attending users to team mentorship session.
        Applies only to team mentorship.
        If omitted, then all team mentees (page owners of the venture) are added as attending users.

    is_multiple is whether the user selected more than one timeslot.
        True:
            This occurs only when Mentor has "Review Meetings" enabled AND mentee has selected multiple timeslots.
        False:
            This occurs primarily when Mentor does not have "Review Meetings" enabled.
            This also occurs as a special case when Mentor does have "Review Meetings" enabled, but the number of
            selected meetings is one.
    """

    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    if is_multiple:
        if (not is_team and (None in selected_date or None in selected_start_time or \
                None in duration or None in year or ('' in selected_day and '' in meeting_type))) and \
                (is_team and (None in selected_date or None in selected_start_time or \
                None in duration or ('' in selected_day and '' in meeting_type))):
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = 'Invalid parameters.'
            return payload
    else:
        if (not is_team and (selected_start_time is None or selected_date is None or duration is None or
                year is None or (meeting_type == '' and selected_day == ''))) and \
                (is_team and (selected_start_time is None or selected_date is None or duration is None or
                (meeting_type == '' and selected_day == ''))):
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = 'Invalid parameters.'
            return payload

    mentorship_activity = None
    if not is_team:
        mentor = mentorship.mentor
        platform = mentor.platform
        # TODO: move these to approve_schedule

        """
        Important step to activate to mentor's timezone then go back to user timezone to avoid saving schedules based on user's timezone, not mentor's timezone
        """
        if mentor.profile.user.cronofy_account_id:
            mentee_timezone = post_data.get("mentee_timezone", None)
            if not mentee_timezone is None:
                session_timezone = mentee_timezone
            else:
                session_timezone = django_timezone
        else:
            session_timezone = mentorship.mentor.timezone
    else:
        session_timezone = django_timezone

    new_session = None
    session_set = None
    try:
        if is_multiple:
            session_set = MentorshipSessionSet.objects.create(mentorship=mentorship)
            for idx in range(len(selected_date)):
                session = create_scheduled_session(
                    mentorship, location[idx], duration[idx], year[idx],
                    selected_day[idx], selected_date[idx], selected_start_time[idx],
                    session_timezone, platform, meeting_type[idx], meeting_topic,
                    is_team=is_team, creator=creator, team=team, mentee_ids=mentee_ids)
                session_set.sessions.add(session)
            new_session = session_set.sessions.first()
        else:
            new_session = create_scheduled_session(mentorship, location, duration, year, selected_day, selected_date,
                                                   selected_start_time, session_timezone, platform,
                                                   meeting_type, meeting_topic,
                                                   is_team=is_team, creator=creator, team=team, mentee_ids=mentee_ids)
    except ValueError as e:
        app_logger.exception(e)
        if mentorship_activity:
            mentorship_activity.delete()
        if is_multiple:
            if session_set:
                session_set.sessions.all().delete()
                session_set.delete()
        else:
            if new_session:
                new_session.delete()
        err_msg = str(e)
        if err_msg != PAST_TIMESLOT_MSG and err_msg != PENDING_TIMESLOT_MSG and err_msg != RESERVED_TIMESLOT_MSG:
            err_msg = 'Error during the slot reserving.'
        payload[PAYLOAD_MSG] = err_msg
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return payload

    # Parse custom question for meeting schedule if exists.
    request_parser = CustomAnswersParser(
        post_data, platform, None, None, file_data=post_files, meeting=new_session)
    try:
        with transaction.atomic():
            request_parser.parse_answers()
            errors = request_parser.errors
            if errors != {}:
                raise ValueError()
            errors = None
    except ValueError:
        errors = request_parser.get_errors()
        app_logger.exception("ERROR: {0}".format(errors))
        # Session not created. delete
        if mentorship_activity:
            mentorship_activity.delete()
        if is_multiple:
            session_set.sessions.all().delete()
            session_set.delete()
        else:
            new_session.delete()
        msg = ''
        for error in errors:
            msg += "{1} (Q.{0})".format(error, errors[error]['msg'])
        payload[PAYLOAD_MSG] = msg
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return payload

    # Prep the session id to be sent.
    payload["s_id"] = new_session.id
    if not is_team:
        m_request = MentorshipRequest.objects.filter(requested_mentorship__id=mentorship.id).order_by('-created_on').first()
    # If the session requires no approval, approve it.
    # Otherwise, send a session request to the admin.

    is_approved = False
    needs_admin_approval = False
    needs_mentor_approval = False
    university_meeting_setting = platform_style.require_meeting_approval
    if not is_admin:
        if is_team:
            # TODO: admin approval steps
            # if university_meeting_setting == UniversityStyle.ALL_MEETINGS:
            #    needs_admin_approval = True
            pass
        else:
            # setting checks
            if university_meeting_setting == UniversityStyle.ALL_MEETINGS:
                needs_admin_approval = True
            if university_meeting_setting == UniversityStyle.OFFICE_HOURS_ONLY and mentor.allow_office_hour:
                needs_admin_approval = True
            if not mentorship.is_active or not mentorship.accepted_request:
                needs_admin_approval = True

            if mentor.approve_meetings_or_admin_override:
                needs_mentor_approval = True

    if not needs_admin_approval and not needs_mentor_approval:
        is_approved = True

    current_site_id = platform.site.id
    if is_approved and not is_multiple:
        new_session.admin_accept_session()
        new_session.mentor_accept_session()
        if not is_team:
            MentorshipRequest.objects.mentorship_accept(m_request)

            # Does mentor have Google Calendar enabled?
            if mentorship.mentor.gcal_email:
                make_gcal_event(new_session)
        # Notify both mentor and mentee.
        session_change_noti_mentor(new_session, current_site_id, SESSION_CREATED)
        session_change_noti_mentee(new_session, current_site_id, SESSION_CREATED)

        if is_team and platform_style.admin_notify_of_team_meeting_scheduled:
            team_session_changed_noti_admin(new_session, platform, state=SESSION_CREATED)

    else:
        '''Cases:
            is_approved & is_multiple
            (not is_approved) & is_multiple
            (not is_approved) & (not is_multiple)
        '''
        if is_multiple:
            '''Cases:
                is_approved & is_multiple
                (not is_approved) & is_multiple

            (Because is_multiple=True, user may have supplied a meeting_topic)
            '''
            session_request_noti_mentor(mentorship, session_set, current_site_id,
                                        is_multiple=True, meeting_topic=meeting_topic)
        else:
            ''' Case: (not is_approved) & (not is_multiple)'''

            # Meeting request notification on: admin receives email.
            if needs_admin_approval and platform_style.meeting_request_noti:
                session_request_noti_uni(new_session, current_site_id)
            elif needs_mentor_approval:
                '''SPECIAL CASE: Mentors with "Review Meetings" enabled, but only allow 1 possible meeting.
                '''
                if not new_session.is_admin_accepted and not needs_admin_approval:
                    new_session.admin_accept_session()
                    MentorshipRequest.objects.mentorship_accept(m_request)
                session_request_noti_mentor(mentorship, new_session, current_site_id,
                                            is_multiple=False, meeting_topic=meeting_topic)
    return payload


def prepare_office_hour_scheduling(mentor_url_name, current_member, platform):
    # Check if mentor has office hour enabled
    if mentor_url_name is None or current_member is None or platform is None:
        return None
    mentor = Mentor.objects.get(profile__url_name=mentor_url_name, platform=platform)
    mentorship = None
    if mentor.allow_office_hour:
        # Automatically accept the mentor request if the meeting is approved.
        # FIXME: merge with send_mentor_request
        student_profile = current_member
        if student_profile.id == mentor.profile_id:
            raise ValueError("Cannot schedule officehour to self.")
        via_meeting = True
        mentorship = Mentorship.objects.create_new_mentorship(student_profile, mentor)
        # Create a new mentorship request.
        new_request = MentorshipRequest.objects.create_new_request(mentorship, None,
                                                                   '', '', via_meeting=via_meeting)
        if new_request.is_admin_accepted:
            # If the request has already been accepted, leave it alone.
            pass
        else:
            # If the request has been previously declined, reopen it.
            # Should have updated time as well.
            new_request.is_admin_accepted = None
            new_request.save()
        MentorshipRequest.objects.mentorship_accept(new_request)

    return mentorship


def prepare_session_scheduling(post_data, django_timezone, platform, platform_style, current_member,
                               post_files=None, mentorship=None, is_admin=False):
    """
    # Replacing mentorshipsession_add logic
    :param post_data: request.POST from the view
    :param django_timezone: request.session['django_timezone']
    :param platform:
    :param platform_style:
    :param current_member:
    :param mentorship: Set for platform_admin_schedule_session, which does not have in_multiple. If this is set,
        use this mentorship for argument of session_scheduling()
    :return: dictionary containing status code and msg if needed to explain error.
    """
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}

    # global parameters need to identify type of requests.
    mentor_ids = post_data.get('mentors')  # Required if team mentorship
    mentee_ids = post_data.get('mentees')  # Optional; applies only for team mentorship
    # # initiator_is_mentor: optional and applies only for team mentorship. If omitted, then initiator is assumed to be mentee.
    initiator_is_mentor = post_data.get('initiator_is_mentor', False)
    team_url_name = post_data.get('team')  # Required if team mentorship
    mentor_url_name = post_data.get('m_id')  # Required if single mentorship

    is_team = False
    team = None
    if mentor_ids and team_url_name:
        is_team = True

        mentor_ids = json.loads(mentor_ids)
        if not isinstance(mentor_ids, list) or len(mentor_ids) == 0:
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = 'Invalid parameters.'
            return payload
        mentor_ids = list(map(lambda x: str(x), mentor_ids))

        if mentee_ids:
            mentee_ids = json.loads(mentee_ids)
            mentee_ids = list(map(str, mentee_ids))
        else:
            mentee_ids = []

        team = Startup.objects.get(url_name=team_url_name)
        mentorship = TeamMentorship.objects.select_related('mentor', 'mentor__profile').filter(
            team=team, mentor__profile__url_name__in=mentor_ids)
    else:
        # Team mentorship cannot have office hours.
        # Check for office hour and setup Mentorship properly
        if not mentorship:
            current_user_id = current_member.user.id
            try:
                mentorship = Mentorship.objects.get(
                    student_id=current_member.id,
                    mentor__profile__url_name=mentor_url_name,
                    mentor__platform=platform
                )
            except Mentorship.DoesNotExist:
                mentorship = prepare_office_hour_scheduling(mentor_url_name, current_member, platform)
    if not mentorship:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = 'No Mentorship.'
        return payload
    if 'is_multiple' in post_data:
        # Is multiple session
        """
        When mentee has selected multiple time slots. 
        This occurs only when Mentor has "Review Meetings" enabled AND mentee has selected multiple timeslots.
        
        At the moment, this only applies to single-mentorship. 
        """
        selected_dates = post_data.get('date').split(',')
        selected_start_times = post_data.get('start_time').split(',')
        durations = post_data.get('duration').split(',')
        years = post_data.get('year').split(',')

        selected_days = []
        for idx in range(len(selected_dates)):
            date, _ = get_formatted_time(years[idx], selected_dates[idx], selected_start_times[idx])
            selected_days.append(date.strftime("%a"))
        meeting_types = post_data.get('type').split(',')
        locations = post_data.get('location').split(',')
        meeting_topic = post_data.get('meeting_topic', None)
        payload = session_scheduling(selected_dates, selected_start_times, durations, years, selected_days,
            meeting_types, locations, meeting_topic, mentorship,  platform, platform_style, is_admin, django_timezone,
            post_data, post_files, is_team=is_team, is_multiple=True, creator=current_member, team=team)
    else:
        """
        When mentee has selected only a single time slot. 
        This occurs primarily when Mentor does not have "Review Meetings" enabled.
        This also occurs as a special case when Mentor does have "Review Meetings" enabled, but the number of
        selected meetings is one.
        """
        selected_date = post_data.get('date', None)
        selected_start_time = post_data.get('start_time', None)
        duration = post_data.get('duration', None)
        year = post_data.get('year', None)
        # Day of the week (for scheduling through cronofy)
        selected_day = post_data.get('day', None)
        meeting_type = post_data.get('type', '')
        location = post_data.get('location', None)
        meeting_topic = post_data.get('meeting_topic', None)

        if selected_day is None:
            date, _ = get_formatted_time(year, selected_date, selected_start_time)
            selected_day = date.strftime("%a")

        payload = session_scheduling(selected_date, selected_start_time, duration, year, selected_day,
                                     meeting_type, location, meeting_topic, mentorship, platform, platform_style,
                                     is_admin, django_timezone, post_data, post_files, is_team=is_team,
                                     creator=current_member, team=team, mentee_ids=mentee_ids)
    if is_team:
        if payload[PAYLOAD_STATUS] == 200:
            if initiator_is_mentor:
                payload['upcoming'] = TeamMentorshipView.get_upcomming_sessions__mentor(team, platform)
            else:
                raw_upcoming = TeamMentorshipSession.objects \
                    .select_related('team', 'creator', 'creator__user') \
                    .filter(team=team,
                            datetime__gte=timezone.now(), is_cancelled=False) \
                    .prefetch_related('mentorships', 'mentorships__mentor', 'mentorships__mentor__profile') \
                    .order_by('datetime')

                upcoming_meetings = []
                user_id = current_member.user.id
                for upcoming in raw_upcoming:
                    if upcoming.member_is_attending(user_id) or upcoming.member_is_creator(current_member.user):
                        mentors = [mentorship.mentor.profile.get_full_name() for mentorship in upcoming.mentorships.all()]
                        creator = upcoming.creator
                        members_attending = upcoming.members_attending.count()
                        mentors_attending = upcoming.mentorships.count()
                        venture = {
                            'name': upcoming.team.name,
                            'url_name': upcoming.team.url_name
                        }
                        raw = upcoming.get_datetime()
                        day = raw.date().strftime('%a')
                        date = raw.date().strftime("%B %d %Y")
                        month = raw.date().strftime("%B")
                        daynumber = raw.date().strftime("%d")
                        start_time = raw.time().strftime("%I:%M")
                        end_time = upcoming.get_endtime().time().strftime("%I:%M%p")
                        time_zone = str(upcoming.get_timezone())
                        tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone
                        meeting_type = upcoming.get_meeting_type()
                        meeting_location = upcoming.get_meeting_location()
                        if not upcoming.member_is_attending(user_id):
                            is_attending = False
                        else:
                            is_attending = True
                        if upcoming.member_is_creator(user_id):
                            is_creator = True
                        else:
                            is_creator = False
                        upcoming_meetings.append({
                            'mentors': mentors,
                            'is_creator': is_creator,
                            'is_attending': is_attending,
                            'venture': venture,
                            'members_count': members_attending,
                            'mentors_count': mentors_attending,
                            'tag': tag,
                            'id': upcoming.id,
                            'month': month[:3],
                            'daynumber': daynumber,
                            'start_time': start_time,
                            'end_time': end_time,
                            'timezone': time_zone,
                            'date': raw.date().strftime('%m/%d/%Y'),
                            'duration': upcoming.duration,
                            'meeting_type': meeting_type,
                            'meeting_location': meeting_location
                        })
                payload['upcoming'] = json.dumps(upcoming_meetings)
    return payload


def platform_admin_schedule_session(post_data, django_timezone, platform, platform_style, current_member,
                                    is_admin, is_super, access_levels, is_st_staff):
    """
    Perform actions required for platform admin trying to create session.
    After proper setup, prepare_session_scheduling will be called.
    :param post_data:
    :param django_timezone: request.session['django_timezone']
    :param platform:
    :param platform_style:
    :param current_member:
    :param is_admin:       request.is_university
    :param is_super:       request.is_super
    :param access_levels:  request.access_levels
    :param is_st_staff:    request.is_startuptree_staff
    :return: dictionary indicating the result.
    """
    payload = {}
    is_admin_mentorship = is_admin_authorized(is_admin, is_super,
                                              access_levels, is_st_staff,
                                              AccessLevel.MENTORSHIP)
    if not is_admin_mentorship:
        payload[PAYLOAD_STATUS] = INVALID_PERMISSION_CODE
        payload[PAYLOAD_MSG] = 'Invalid user.'
        return payload
    selected_mentor_id = post_data.get('selected_mentor_id')
    selected_mentee_id = post_data.get("selected_mentee_id")

    mentorship = Mentorship.objects.get(
        mentor__id=selected_mentor_id,
        student__id=selected_mentee_id,
        is_active=True)
    return prepare_session_scheduling(post_data, django_timezone, platform, platform_style, current_member,
                                      mentorship=mentorship, is_admin=True)
