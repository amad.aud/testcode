# django imports
from django.http import JsonResponse
from django.db import transaction
from django.db.models import ObjectDoesNotExist, Q
# mentorship
from mentorship.models import *
from mentorship.modelforms import MentorContactForm, MentorDefaultForm
from mentorship.serializers import mentor_to_dict
from mentorship.tasks import uni_mentor_request_noti, refresh_cronofy_cache__user
from mentorship import views as men_views
from mentorship.st_cronofy import get_possible_calendar_schedules_cronofy, is_cronofy_linked
# branch
from branch.models import StartupMember, Startup
# form
from forms.parser import CustomAnswersParser
# trunk
from trunk.models import (
    University,
    UniversityStyle
)
# activity
# startuptree
from StartupTree.settings import DEBUG
from StartupTree.strings import *
from StartupTree.utils import time_minute_diff, time_minute_diff_datetimes
from StartupTree.loggers import app_logger
import datetime as dt
from datetime import datetime, timedelta, time
from dateutil import parser


def get_active_mentors(uni):
    return Mentor.objects.select_related('profile', 'profile__user').filter(
            id__in=MentorSchedule.objects.all().values_list('mentor_id', flat=True),
            platform_id=uni.id,
            is_active=True).distinct('profile__user').order_by('profile__user', 'profile__first_name')


def get_all_mentors(uni):
    return Mentor.objects.select_related('profile', 'profile__user').filter(
            platform_id=uni.id,
            is_active=True).distinct('profile__user').order_by('profile__user', 'profile__first_name')


def mentors_prefetch_related(mentors):
    return mentors.prefetch_related(
        'profile__skills',
        'profile__tags', 'profile__labels', 'profile__skill_groups'
    )


def mentor_has_info(mentor):
    return (mentor.profile.bio is not None and len(mentor.profile.bio) > 0) or \
                mentor.profile.skill_groups.all().count() > 0 or mentor.profile.skills.all().count() > 0


def get_requests_sent(uid):
    return MentorshipRequest.objects.select_related('requested_mentorship')\
            .filter(Q(is_admin_accepted=True) | Q(is_admin_accepted__isnull=True),
                    requested_mentorship__student__user_id=uid)\
            .values_list('requested_mentorship__mentor_id', flat=True)


def get_mentors(user, uni, is_authenticated, is_archived, get_active=False):
    """
    get mentor data for a given request
    *only returns mentors who have filled out bio, skills, and roles
    :param user: UserEmail object
    :param uni: University Object
    :param is_authenticated: Boolean
    :return: mentor_list, requests_sent
    """
    if get_active:
        mentors = get_active_mentors(uni)
    else:
        mentors = get_all_mentors(uni)

    mentors = mentors_prefetch_related(mentors)

    requests_sent = []
    mentor_list = []
    for mentor in mentors:
        if mentor_has_info(mentor):
            mentor_list.append(mentor_to_dict(mentor, uni.id))

    if is_authenticated and not is_archived:
        uid = user.id
        requests_sent = \
            get_requests_sent(uid)

    return mentor_list, list(requests_sent), mentors


def no_startup(startup_id):
    return startup_id == '<>' or startup_id is None or len(startup_id) == 0


def send_mentor_request(user, data, files, uni: University, via_meeting):
    mentor_url_id = data.get("m_id")
    startup_id = data.get('venture-name-id')
    pitch = data.get('pitch')
    other = data.get('other')

    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}

    if not mentor_url_id:
        # Mentor url id is null.
        raise ValueError()
    else:
        uid = user.id
        if no_startup(startup_id):
            # Startup doesn't exist.
            startup = None
        else:
            # Startup does exist.
            try:
                # Try to get the startup.
                startup_id = startup_id.replace('/company/', '')
                startup = Startup.objects.get(url_name=startup_id, university=uni)
            except Startup.DoesNotExist:
                # Startup could not be found and therefore does not exist.
                startup = None

        try:
            # Get the student's profile.
            student_profile = StartupMember.objects.get_user(user=user)
        except StartupMember.DoesNotExist:
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "Bad Request. If you are encountering this error, please contact admin."
            return JsonResponse(payload)

        try:
            # Get the mentor.
            mentor = Mentor.objects.get_mentor_by_id(mentor_url_id, uni.site)

            # Store whether the mentor can schedule meetings or not.
            payload["can_schedule"] = mentor.allow_office_hour and (mentor.has_schedule() or mentor.has_onetimes())

        except ObjectDoesNotExist:
            # Either student or mentor does not exist.
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "The selected mentor does not exist."
            return JsonResponse(payload)

        # Process the custom answers first before creating a new mentorship. (If custom answers are invalid,
        # mentee will send a new mentorship request.)
        request_parser = CustomAnswersParser(
            data, uni, student_profile, None, file_data=files, mentor=mentor)
        try:
            with transaction.atomic():
                request_parser.parse_answers()
                errors = request_parser.errors
                if errors != {}:
                    raise ValueError()
                errors = None
        except ValueError:
            errors = request_parser.get_errors()
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload['errors'] = errors
            return JsonResponse(payload)

        try:
            # We have all the necessary pieces to create a mentorship.
            new_mentorship = Mentorship.objects.create_new_mentorship(student_profile, mentor)
        except ValueError:
            # Cannot create the new mentorship.
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "The requested mentor is already mentoring you."
            return JsonResponse(payload)

        if new_mentorship.blocked:
            # Mentor has blocked the requesting user.
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "The mentor has blocked the mentorship request."
            return JsonResponse(payload)

        try:
            # Create a new mentorship request.
            new_request = MentorshipRequest.objects.create_new_request(new_mentorship, startup,
                                                                       pitch, other, via_meeting=via_meeting)
            if new_request.is_admin_accepted:
                # If the request has already been accepted, leave it alone.
                pass
            else:
                # If the request has been previously declined, reopen it.
                # Should have updated time as well.
                new_request.is_admin_accepted = None
                new_request.save()
            payload["n_rid"] = new_request.id

            needs_approval = uni.style.require_meeting_approval == UniversityStyle.ALL_MEETINGS or \
                (uni.style.require_meeting_approval == UniversityStyle.OFFICE_HOURS_ONLY and mentor.allow_office_hour)

            # If this request was a consequence of a scheduled meeting (which was accepted),
            # then automatically accept this request.
            if mentor.allow_office_hour and via_meeting and not needs_approval:
                MentorshipRequest.objects.mentorship_accept(new_request)
                mentorship = new_request.requested_mentorship

                platform = mentorship.mentor.platform

                # Connection has been made.
                mentorship_activity = MentorshipActivity.objects.make_mentorship_activity(MentorshipActivity.CONNECTION_MADE, platform)
                mentorship.mentorship_activity = mentorship_activity

                # Save the request and the mentorship.
                mentorship.save()
                new_request.save()
            else:
                # If mentor request notification is on, admin will receive email notification.
                if uni.style.mentor_request_noti:
                    uni_mentor_request_noti(student_profile, uni.site)

        except ValueError:
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "Please provide an appropriate request."
            return JsonResponse(payload)

    return JsonResponse(payload)


def is_overlapping_mentorschedule(mentor: Mentor, meeting_day: int, meeting_date: dt.date, start_time: dt.time, end_time: dt.time) -> bool:
    """TODO
    Returns whether mentor already has a MentorSchedule that conflicts with this new rule.

    To avoid case of submitting 10AM-11AM not working because 9AM-10AM and 11AM-12PM considered overlapping
    """
    return False

    start_time_range = start_time + timedelta(seconds=1)
    end_time_range = end_time - timedelta(seconds=1)

    return not MentorSchedule.objects.filter(
        Q(mentor=mentor),
        (Q(day=meeting_day) & Q(is_repeating=True)) | (Q(date=meeting_date) & Q(is_repeating=False)),
        (Q(start_time__range=(start_time, end_time_range)) | Q(end_time__range=(start_time_range, end_time))))


def set_mentor_settings(request, mentor_id):  # HERE now
    uni = University.objects.get(site=request.current_site)
    mentor = Mentor.objects.get(id=mentor_id, platform_id=uni.id)
    admin_fields = ['mentor-id-defaults', 'mentor-id-avail', 'mentor-id-bo']
    default_fields = ['default_meeting_type', 'default_location', 'default_duration',
                      'allow_office_hour', 'approve_meetings', 'number_meeting_times', 'availability_note']
    contact_fields = ['email', 'phone', 'skype', 'g']  # g is for mentor signup
    availability_fields = ['id', 'is_add', 'day', 'end-day', 'start-time', 'end-time', 'duration',
                           'meeting_type', 'location', 'phone_number', 'video_id',
                           'is_repeating', 'notes']
    blackout_fields = ['start_date-', 'end_date-']
    gcal_fields = ['gcal-enabled', 'gcal-email']
    cronofy_fields = ['Sun_type', 'Sun_location', 'Mon_type', 'Mon_location', 'Tue_type', 'Tue_location',
                      'Wed_type', 'Wed_location', 'Thu_type', 'Thu_location', 'Fri_type', 'Fri_location',
                      'Sat_type', 'Sat_location', 'cronofy_variable_duration']
    counter = 0
    schedule = None
    error = None
    is_saved = False
    is_admin = False
    default_dict = dict()
    contact_dict = dict()
    availability_dict = dict()
    blackout_dict = dict()
    if request.method == 'POST':
        for q, v in request.POST.items(): #FIXME: this is problematic.. it goes through ALL query params
            if q == 'unlink-calendars' or q in cronofy_fields:
                continue #FIXME: makes it impossible to add new query params without doing this
            if q in admin_fields:
                is_admin = True
            if q in default_fields:
                default_dict[q] = v
            if q in contact_fields:
                contact_dict[q] = v
            if q in availability_fields:
                availability_dict[q] = v
            if q not in default_fields and q not in contact_fields and \
                    q not in availability_fields and q not in admin_fields and \
                    q not in gcal_fields and \
                    q != 'edit-confirm' and q != 'delete-confirm' and \
                    q != 'csrfmiddlewaretoken' and q != 'new_tz':
                # user has added blackout dates
                counter += 1
                blackout_dict[q] = v
        contact_form = MentorContactForm(contact_dict)
        default_form = MentorDefaultForm(default_dict)
        # if 'allow_office_hour' in request.POST:
        #     default_form = MentorDefaultForm(default_dict)
        # else:
        #     default_form = AdminMentorDefaultForm(default_dict)

        ##########################
        # CONTACT & DEFAULT INFO #
        ##########################
        if contact_form.is_valid():
            contact_form.save(mentor)
            is_saved = True
        if default_form.is_valid():
            default_form.save(mentor)
            is_saved = True

        if not mentor.approve_meetings_or_admin_override:
            mentor.number_meeting_times = 1
            mentor.save()

        ###################
        # GOOGLE CALENDAR #
        ###################
        if request.POST.get('gcal-enabled', None):
            mentor.gcal_email = request.POST.get('gcal-email', None)
        else:
            if 'edit-confirm' not in request.POST and 'delete-confirm' not in request.POST:
                mentor.gcal_email = None

        ################
        # AVAILABILITY #
        ################
        if len(availability_dict) > 0:
            meeting_id = availability_dict.get('id')
            is_add = availability_dict.get('is_add')
            meeting_date = availability_dict.get('day')
            meeting_end_date = availability_dict.get('end-day')
            if meeting_date is None:
                return JsonResponse({'status': 401, "msg": "Please provide meeting date"})
            start_time = availability_dict.get('start-time', None)
            end_time = availability_dict.get('end-time', None)
            duration_minutes = availability_dict.get('duration')
            duration = MentorSchedule.get_minute_choice(duration_minutes)
            meeting_type = availability_dict.get('meeting_type')
            location = availability_dict.get('location')
            phone_number = availability_dict.get('phone_number')
            video_id = availability_dict.get('video_id')
            is_repeating = availability_dict.get('is_repeating')
            notes = availability_dict.get('notes')
            if meeting_id != 'None':
                try:
                    schedule = MentorSchedule.objects.get(id=int(meeting_id), mentor=mentor)
                except MentorSchedule.DoesNotExist:
                    pass
            if start_time != '' and end_time != '' and meeting_date:
                try:
                    time_error = False
                    end_before_start = False
                    start_time = datetime.strptime(start_time.replace(" ", ""), '%I:%M%p')
                    end_time = datetime.strptime(end_time.replace(" ", ""), '%I:%M%p')
                    if end_time <= start_time:
                        end_before_start = True
                        raise ValueError
                except ValueError:
                    if end_before_start:
                        error = "End time should not be before start time. "
                    else:
                        error = "Time should be in HH:MM pm format"
                    time_error = True
                if not time_error:
                    meeting_date_obj = parser.parse(meeting_date)
                    meeting_end_date_obj = parser.parse(meeting_end_date) if meeting_end_date else None

                    if is_add == 'true':
                        is_add = True
                    else:
                        is_add = False
                    if is_repeating == 'on':
                        is_repeating = True
                        meeting_day = meeting_date_obj.weekday()
                        meeting_day += 1
                    else:
                        is_repeating = False
                        meeting_day = None
                    if 'delete-confirm' in request.POST:
                        if schedule:
                            schedule.delete()
                    else:
                        if is_add:
                            # check if there already exists existing schedule and throw error if it does.
                            with transaction.atomic():
                                if not is_overlapping_mentorschedule(
                                        mentor, meeting_day, meeting_date_obj, start_time, end_time):

                                    MentorSchedule.objects.create(
                                        mentor=mentor,

                                        day=meeting_day,
                                        date=meeting_date_obj,
                                        end_date=meeting_end_date_obj,
                                        start_time=start_time,
                                        end_time=end_time,
                                        duration=duration,

                                        meeting_type=meeting_type,
                                        location=location,
                                        phone_number=phone_number,
                                        video_id=video_id,

                                        is_repeating=is_repeating,
                                        notes=notes)
                                else:
                                    app_logger.info('hit')
                        else:
                            schedule.day = meeting_day
                            schedule.date = meeting_date_obj
                            schedule.end_date = meeting_end_date_obj
                            schedule.start_time = start_time
                            schedule.end_time = end_time
                            schedule.duration = duration
                            schedule.meeting_type = meeting_type
                            schedule.location = location
                            schedule.phone_number = phone_number
                            schedule.video_id = video_id
                            schedule.is_repeating = is_repeating
                            schedule.notes = notes
                            schedule.save()
                    is_saved = True
            elif (not meeting_date and (start_time or end_time)) or \
                (not start_time and (meeting_date or end_time)) or \
                (not end_time and (meeting_date or start_time)):
                error = "Please fill in date, start time and end time"

        ##################
        # BLACKOUT DATES #
        ##################
        # remove user's deleted blackout intervals
        if not is_admin and not 'edit-confirm' in request.POST and not 'delete-confirm' in request.POST:
            MentorScheduleBlackOut.objects.filter(mentor=mentor).delete()
        if len(blackout_dict) > 0:
            # if is_admin:
            #     MentorScheduleBlackOut.objects.filter(mentor=mentor).delete()
            # handling for if user deletes a blackout interval from the middle of the list
            lister = []
            for keys in (list(blackout_dict.keys())):
                num = keys[keys.index("-") + 1:]
                lister.append(num)
            highest = max(lister)
            counter = (int(highest) + 1) * 2
            # add new blackout dates
            # users can now add in unlmited slots of blackout dates
            for i in range(int(counter)):
                start_date = blackout_dict.get('start_date-{0}'.format(i))
                end_date = blackout_dict.get('end_date-{0}'.format(i))
                if start_date == None or end_date == None or \
                        start_date == '' or end_date == '':
                    continue
                start_date = datetime.strptime(start_date, '%m/%d/%Y')
                end_date = datetime.strptime(end_date, '%m/%d/%Y')
                delta = (start_date.date() - end_date.date()).days
                if delta <= 0:
                    MentorScheduleBlackOut.objects.get_or_create(
                        start_date=start_date, end_date=end_date, mentor=mentor)
                else:
                    # should never get here, since submit button becomes unclickable
                    error = "Invalid blackout date interval"

        is_saved = True
        mentor.save()

        if is_cronofy_linked(mentor.profile.user):
            transaction.on_commit(lambda: refresh_cronofy_cache__user(mentor.id))
    else:
        # below is separate from startupmember because mentor
        # might want to have different contact options than what is on
        # profile. by default, it will fill from their stmember profile
        if not mentor.email:
            mentor.email = mentor.profile.user.email
        if not mentor.phone:
            mentor.phone = mentor.profile.phone
        if not mentor.skype:
            mentor.skype = mentor.profile.skype
        contact_form = MentorContactForm(instance=mentor)
        default_form = MentorDefaultForm(instance=mentor)

    defaults = MentorshipDefaults.objects.filter(mentor_id=mentor.id)
    schedules = MentorSchedule.objects.filter(
        mentor_id=mentor.id).order_by('date', 'start_time')
    blackout_dates = MentorScheduleBlackOut.objects.filter(
        mentor_id=mentor.id).order_by('start_date')
    onetimes = MentorSchedule.objects.filter(
        mentor_id=mentor.id, is_repeating=False, date__isnull=False).order_by('date')

    # remove expired blackout dates and onetimes
    today = dt.date.today()
    for blackout in blackout_dates:
        if blackout.end_date < today:
            blackout.delete()
    for onetime in onetimes:
        if onetime.date < today:
            onetime.delete()

    onetimes = format_onetimes(onetimes)

    user_timezone = pytz.timezone(request.session['django_timezone'])
    if is_cronofy_linked(mentor.profile.user):
        sched = get_cronofy_possible_calendar_schedules(mentor, 1, user_timezone=user_timezone)
    else:
        sched = get_possible_calendar_schedules(mentor, 1, True)

    return mentor, schedules, sched, onetimes, blackout_dates, is_saved, error


def format_onetimes(onetimes):
    ots = {}
    for ot in onetimes:
        key = ot.date
        if key in ots.keys():
            d = ots[key]
            d['starts'].append(ot.start_time)
            d['ends'].append(ot.end_time)
        else:
            ots[key] = {'duration': ot.duration,
                        'starts': [ot.start_time],
                        'ends': [ot.end_time]
                        }
    onetimes = []
    for key in ots.keys():
        onetimes.append(
            {
                'date': key,
                'duration': ots[key]['duration'],
                'times': zip(ots[key]['starts'], ots[key]['ends'])
            })
    return onetimes


def get_calendar_scheduled_sessions(mentor, datetime_range):
    return MentorshipSession.objects.filter(mentorship__mentor_id=mentor.id,
                                            is_cancelled=False,
                                            teammentorshipsession__isnull=True,
                                            datetime__range=datetime_range)


def get_calendar_scheduled_team_sessions(mentor, datetime_range):
    return TeamMentorshipSession.objects.filter(mentorships__mentor_id=mentor.id,
                                                is_cancelled=False,
                                                datetime__range=datetime_range)


def get_calendar_onetimes(mentor):
    return MentorSchedule.objects.filter(
            mentor_id=mentor.id,
            is_repeating=False,
            is_inactive=False,
            date__isnull=False
        ).order_by('date', 'start_time')


def add_scheduled_sessions(availability, mentor, range, user_timezone=None):
    """ Helper function to get_possible_calendar_schedules and get_possible_calendar_schedules_cronofy

    Finds all scheduled mentoring sessions associated with the mentor in the given
    time range, and appends/modifies it to the availability list,
    with the free boolean of the time slot set to false, and the mentee
    (or team, if team mentoring meeting) noted.

    Side-effects: modifies the appropriate timeslots of `availability` to show
    which slots are occupied by mentoring meetings meetings with who (mentee or team).
    If no such slot exists, then one is appended. An additional string, the mentoring type
    ('mentee' or 'team') is appended.
    """
    scheduled_sessions = MentorshipSession.objects.filter(mentorship__mentor=mentor,
                                                          is_cancelled=False,
                                                          datetime__range=range)

    associated_team_mentorships = TeamMentorship.objects.filter(mentor=mentor)
    scheduled_team_sessions = TeamMentorshipSession.objects.filter(
        mentorships__in=associated_team_mentorships,
        is_cancelled=False,
        datetime__range=range)

    additional_slot = []
    for session in scheduled_sessions:
        mentee = session.get_mentee()
        timeslot = session.get_datetime(mentor, user_timezone)  # timezone converted for view
        end_time = session.get_endtime(mentor, user_timezone)
        day = timeslot.weekday()
        end_time = end_time.time()
        start_time = timeslot.time()
        meeting_topic = session.meeting_topic
        meeting_id = session.id
        session_type = session.meeting_type
        session_location = session.get_meeting_location()
        # bug is prob here
        filled_success = False
        for slot in availability[day][2]:
            slot_start = datetime.strptime(slot[0], "%I:%M%p").time()
            slot_end = datetime.strptime(slot[1], "%I:%M%p").time()
            if slot_start >= start_time and slot_end <= end_time:
                slot[2] = False
                slot.append(mentee)
                slot.append('mentee')
                slot.append(meeting_topic)
                slot.append(meeting_id)
                slot.append(session_type)
                slot.append(session_location)
                filled_success = True
            elif slot_start >= start_time and (slot_end > end_time > slot_start):
                slot[2] = False
                slot.append(mentee)
                slot.append('mentee')
                slot.append(meeting_topic)
                slot.append(meeting_id)
                slot.append(session_type)
                slot.append(session_location)
                filled_success = True
            elif slot_end >= start_time and (start_time < slot_end <= end_time):
                slot[2] = False
                slot.append(mentee)
                slot.append('mentee')
                slot.append(meeting_topic)
                slot.append(meeting_id)
                slot.append(session_type)
                slot.append(session_location)
                filled_success = True
        if not filled_success:
            additional_slot.append((
                day,
                [timeslot.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), False,
                 session.duration, mentee, 'mentee', meeting_topic, meeting_id, session_type, session_location]
                )
           )

    for session in scheduled_team_sessions:
        team = session.team
        timeslot = session.get_datetime(user_timezone=user_timezone)  # timezone converted for view
        end_time = session.get_endtime(user_timezone=user_timezone)
        day = timeslot.weekday()
        end_time = end_time.time()
        start_time = timeslot.time()
        meeting_topic = None  # session.meeting_topic  # doesn't exist for team mentorship
        meeting_id = session.id
        session_type = session.meeting_type
        session_location = session.get_meeting_location()
        # bug is prob here
        filled_success = False
        for slot in availability[day][2]:
            slot_start = datetime.strptime(slot[0], "%I:%M%p").time()
            slot_end = datetime.strptime(slot[1], "%I:%M%p").time()
            if slot_start >= start_time and slot_end <= end_time:
                slot[2] = False
                slot.append(team)
                slot.append('team')
                slot.append(meeting_topic)
                slot.append(meeting_id)
                slot.append(session_type)
                slot.append(session_location)
                filled_success = True
            elif slot_start >= start_time and (slot_end > end_time > slot_start):
                slot[2] = False
                slot.append(team)
                slot.append('team')
                slot.append(meeting_topic)
                slot.append(meeting_id)
                slot.append(session_type)
                slot.append(session_location)
                filled_success = True
            elif slot_end >= start_time and (start_time < slot_end <= end_time):
                slot[2] = False
                slot.append(team)
                slot.append('team')
                slot.append(meeting_topic)
                slot.append(meeting_id)
                slot.append(session_type)
                slot.append(session_location)
                filled_success = True
        if not filled_success:
            additional_slot.append((
                day,
                [timeslot.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), False,
                 session.duration, team, 'team', meeting_topic, meeting_id, session_type, session_location]
                )
           )

    for additional in additional_slot:
        day, slot = additional
        availability[day][2].append(slot)


def get_cronofy_possible_calendar_schedules(mentor, page, has_weekday_decimal=False, show_past_timeslots=False, user_timezone=None):
        # If the mentor is synced via cronofy, then immediately run the
        # cronofy version of this method.
        availability, availability_range = get_possible_calendar_schedules_cronofy(mentor, page, user_timezone)
        add_scheduled_sessions(availability, mentor, availability_range, user_timezone)
        return availability


def get_possible_calendar_schedules(mentor, page, has_weekday_decimal=False, show_past_timeslots=False, user_timezone=None):
    """
    Get open timeslots of a mentor for a given week in page.

    Example of an entry in the output:

        [
            ...
            ['Fri', 'Sep 13', [
              ['01:00 PM', '01:30 PM', False, 2, <Startup: 17E Technologies>, 'team'],
              ['01:30 PM', '02:00 PM', True, 2],
            ], 2019, 4, 'Room 305', datetime.date(2019, 9, 13)],
            ...
        ]

        An entry represents a day in the desired week, and includes a list of timeslots associated with that day.
        The integer component of the day entry at index 4 (eg the number 4), is the meeting type (codes are mentorship.models.PREFERRED_CONTACT) for that day.
        The string that follows at index 5 is the meeting location.

        The boolean in each timeslot at index 2 is whether the timeslot is free (True) or taken by a mentee or team (False).
        The integer that follows at index 3 is the duration of the meeting (codes are mentorship.models.DURATIONS)
        If the timeslot is free, there are no more components in the timeslot.
        If the timeslot is taken by a mentee (via regular mentorship) or team (via team mentorship), then the object at index 4 is the mentee or team object.
        The string following the mentee/team object at index is a string that denotes
        whether the session is a regular mentorship session ('mentee') or a team mentorship session ('team').

        TODO FIXME: it is redundant to need a string to denote whether this a regular mentorship or
        team mentorship session because that can be inferred by getting the the type of the object
        at index 4 of the timeslot.



    IMPORTANT: if modifying the output format of this method, make sure to also
    modify the corresponding output format of get_possible_calendar_schedules_cronofy, which is
    the corresponding method for cronofy users.

    :param mentor_id:
    :param page:
    :param view:
    :return:
    """
    # add this second check to not affect other calls to this method
    if page < 1 and not show_past_timeslots:
        page = 1
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentor_timezone = pytz.timezone(mentor.timezone)

    today = datetime.today().date()
    days_to_next_monday = 0 - today.weekday() + (7 * (page-1))
    next_monday = today + timedelta(days_to_next_monday)
    next_sunday = next_monday + timedelta(6)
    datetime_range = (next_monday, datetime.combine(next_sunday, time.max))

    # we could do the cancellation thing here... but would probably affect a large amount of the site.

    weekly_schedule = MentorSchedule.objects.filter(
        mentor_id=mentor.id,
        is_repeating=True,
        is_inactive=False
    ).order_by('day', 'start_time')
    blackout_ranges = MentorScheduleBlackOut.objects.filter(
        (Q(start_date__gte=next_monday) | Q(start_date__lte=next_sunday)),
        (Q(end_date__lte=next_monday) | Q(end_date__gte=next_sunday) | Q(end_date__lte=next_sunday)),
        mentor_id=mentor.id
    )
    one_times = MentorSchedule.objects.filter(
        mentor_id=mentor.id,
        is_repeating=False,
        is_inactive=False,
        date__isnull=False
    ).order_by('date', 'start_time')
    possible_dates = [next_monday + timedelta(i) for i in range(7)]
    #Set the valid interval of blackout dates to maximum one year.
    blackout_dates = [False for _ in range(365)]
    for blackout_range in blackout_ranges:
        start_date = blackout_range.start_date
        end_date = blackout_range.end_date
        for i in range((end_date - start_date).days + 1):
            tmp = start_date + timedelta(i)
            if tmp > next_sunday:
                break
            if tmp in possible_dates:
                blackout_dates[tmp.weekday()] = True
    availability = []
    tmp_date = next_monday
    dates_this_week = []

    now = timezone.now().astimezone(mentor_timezone)
    if user_timezone:
        now = timezone.localtime(now, user_timezone)

    # ['Monday', 'April 12', [[2:00, 2:15, True, 1],[2:15: 2:30, True, 3],...], 2016, 'In Person', '409 College Ave', '2016-04-12']
    # The boolean in the availability time slots appear to be True if it is free (not occupied by a mentee)
    # and is False if it is occupied by a mentee.
    while tmp_date <= next_sunday:
        if has_weekday_decimal:
            availability.append([[int(tmp_date.strftime('%w')), tmp_date.strftime('%a')], tmp_date.strftime('%b %d'), [], tmp_date.year,
                                mentor.default_meeting_type, mentor.default_location, tmp_date])
        else:
            availability.append([tmp_date.strftime('%a'), tmp_date.strftime('%b %d'), [], tmp_date.year,
                                mentor.default_meeting_type, mentor.default_location, tmp_date])
        dates_this_week.append(tmp_date)

        tmp_date += timedelta(1)
    for schedule in weekly_schedule:
        start_date = schedule.date
        weekday = schedule.day - 1
        duration = schedule.get_duration()
        start_time = schedule.start_time
        end_time = schedule.end_time
        type = schedule.meeting_type
        location = schedule.location
        phone_number = schedule.phone_number
        video_id = schedule.video_id
        if blackout_dates[weekday] or \
           (start_date and start_date > dates_this_week[weekday]) or \
           (not has_weekday_decimal and dates_this_week[weekday] < today) or \
           (schedule.end_date and schedule.end_date < dates_this_week[weekday]):
            possible_slots = 0
        else:
            possible_slots = time_minute_diff(start_time, end_time)
            possible_slots /= duration
            possible_slots = int(possible_slots)

        slots = []
        for i in range(possible_slots):
            if start_date is not None:
                start_datetime = mentor_timezone.localize(datetime.combine(dates_this_week[weekday], start_time))
                if user_timezone:
                    start_datetime = timezone.localtime(start_datetime, user_timezone)
                end_datetime = start_datetime + timedelta(minutes=duration)
                end_time = end_datetime.time()
                if start_datetime > now:
                    slots.append([start_datetime.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), True, schedule.duration])
                if user_timezone:
                    # need to convert back to mentor timezone for next iteration.
                    start_time = timezone.localtime(end_datetime, mentor_timezone).time()
                else:
                    start_time = end_time
        availability[weekday][2] += slots
        availability[weekday][4] = type
        if type == 1:
            availability[weekday][5] = phone_number
        elif type == 3:
            availability[weekday][5] = video_id
        else:
            availability[weekday][5] = location
    for one_time in one_times:
        date = one_time.date
        if date is not None:
            date_str = date.strftime("%Y %b %d")
            year, month, day = str(date_str).split(' ')
            date_str = month + ' ' + day
            start_time = one_time.start_time
            end_time = one_time.end_time
            duration = one_time.get_duration()
            type = one_time.meeting_type
            location = one_time.location
            phone_number = one_time.phone_number
            video_id = one_time.video_id

            possible_slots = time_minute_diff(start_time, end_time)
            possible_slots /= duration
            possible_slots = int(possible_slots)

            slots = []
            for i in range(possible_slots):
                if date is not None:
                    start_datetime = mentor_timezone.localize(datetime.combine(date, start_time))
                    if user_timezone:
                        start_datetime = timezone.localtime(start_datetime, user_timezone)
                    end_datetime = start_datetime + timedelta(minutes=duration)
                    end_time = end_datetime.time()
                    if start_datetime > now:
                       temp = [start_datetime.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), True, one_time.duration]
                       if not temp in availability[date.weekday()][2] and availability[date.weekday()][1] == date_str:
                           slots.append(temp)
                    if user_timezone:
                        # need to convert back to mentor timezone for next iteration.
                        start_time = timezone.localtime(end_datetime, mentor_timezone).time()
                    else:
                        start_time = end_time
            availability[date.weekday()][2] += slots
            availability[date.weekday()][4] = type
            if type == 1:
                availability[date.weekday()][5] = phone_number
            elif type == 3:
                availability[date.weekday()][5] = video_id
            else:
                availability[date.weekday()][5] = location

    add_scheduled_sessions(availability, mentor, datetime_range, user_timezone)

    return availability


def filter_after_next_monday(availability):
    """ Filters out any days before next monday
    """
    today = datetime.today().date()
    next_monday = today + timedelta(7 - today.weekday())
    make_date = lambda entry: datetime.strptime(entry[1]+' '+str(entry[3]), '%b %d %Y').date()
    f = lambda entry: make_date(entry) >= next_monday
    return list(filter(f, availability))


def list_sched_date_info(upcoming_week, mentor_timezone, tz):
    total_days = 7 * upcoming_week
    today = mentor_timezone.localize(datetime.today()).astimezone(tz).date()
    today_weekday = today.weekday()
    end_day = today + timedelta(total_days)
    datetime_range = (today, datetime.combine(end_day, time.max))

    return total_days, today, today_weekday, end_day, datetime_range


def get_scheduled_sessions(mentor, datetime_range):
    return get_calendar_scheduled_sessions(mentor, datetime_range).iterator()


def get_scheduled_team_sessions(mentor, datetime_range):
    return get_calendar_scheduled_team_sessions(mentor, datetime_range).iterator()


def get_weekly_schedule(mentor):
    return MentorSchedule.objects.filter(
            mentor_id=mentor.id,
            is_repeating=True,
            is_inactive=False
        ).order_by('day', 'start_time')


def get_blackout_ranges(today, end_day, mentor):
    # TODO FIXME: There may be a simpler query for this: https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    return MentorScheduleBlackOut.objects.filter(
            (Q(start_date__gte=today) | Q(start_date__lte=end_day)),
            (Q(end_date__lte=today) | Q(end_date__gte=end_day) | Q(end_date__lte=end_day)),
            mentor_id=mentor.id
        )


def get_one_times(mentor, today):
    return MentorSchedule.objects.filter(
            mentor_id=mentor.id,
            date__gte=today,
            is_repeating=False,
            is_inactive=False
        ).order_by('date', 'start_time')


def get_possible_list_schedules(mentor, has_weekday_decimal=False, upcoming_week=12, show_this_week=False, request=None):
    """
    get the next 12 weeks worth of mentor's schedule.
    THis is somewhat specific to Philsbury's requirement.
    :param mentor_id:
    :return: at most 30 upcoming schedules
    """

    mentor_timezone = pytz.timezone(mentor.timezone)
    if request is None:
        tz = mentor_timezone
    else:
        tz = pytz.timezone(request.session['django_timezone'])

    total_days, today, today_weekday, end_day, datetime_range = list_sched_date_info(upcoming_week, mentor_timezone, tz)
    scheduled_sessions = get_scheduled_sessions(mentor, datetime_range)
    scheduled_team_sessions = get_scheduled_team_sessions(mentor, datetime_range)
    weekly_schedule = get_weekly_schedule(mentor)
    # blackout_ranges is MentorScheduleBlackOut objects that are applicable between today  and end_day
    blackout_ranges = get_blackout_ranges(today, end_day, mentor)
    one_times = get_one_times(mentor, today)

    # possible_dates is an array of datetimes from today to the last day in upcoomming weeks
    possible_dates = [today + timedelta(i) for i in range(total_days)]
    # a boolean array corresponding to whether the entry in possible_dates is blacked out.
    blackout_dates = [False for _ in range(total_days)]

    for blackout_range in blackout_ranges:
        start_date = blackout_range.start_date
        end_date = blackout_range.end_date

        for i in range((end_date - start_date).days + 1):
            tmp = start_date + timedelta(i)
            if tmp > end_day:
                break
            if tmp in possible_dates:
                days = (tmp - today).days
                blackout_dates[days] = True

    availability = []
    tmp_date = today
    dates_this_week = []
    # [[1, 'Mon'], 'April 12', [[2:00, 2:15, True, 1],[2:15: 2:30, True, 3],...], 2016, 'In Person', '409 College Ave']
    while tmp_date <= end_day:
        """ At the end of execution of this while loop,
        `availability` is an array where each entry corresponds to a day
        between today and end_day (regardless of whether mentor is available or not).

        An entry looks like:
        [[1, 'Mon'], 'Apr 12', [], 2016, mentor.default_meeting_type, mentor.default_location]

        `dates_this_week` will be an array containing date objects between today and end_day
        """
        meeting_info = [[int(tmp_date.strftime('%w')), tmp_date.strftime('%a')], tmp_date.strftime('%b %d'), [], tmp_date.year,
                            mentor.default_meeting_type, mentor.default_location]
        availability.append(meeting_info)
        dates_this_week.append(tmp_date)
        tmp_date += timedelta(1)
    has_schedule = False

    now = timezone.now().astimezone(tz)

    for week in range(upcoming_week):
        for schedule in weekly_schedule:
            # current_day:
            #   used to index into `dates_this_week` array; index refers to the day number between today and end_day
            #   represents the day number corresponding to the date in "week" that "schedule" refers to.
            #   eg: if schedule is a weekly tuesday availability, then current_day refers to the tuesday of "week"
            #   (in terms of number of days since today).
            current_day = (schedule.day - 1) + (week * 7) - today_weekday
            current_date = dates_this_week[current_day]  # date object corresponding to "current_day"
            if schedule.end_date and schedule.end_date < current_date:
                # This schedule object has an end date that is before this week. We can skip to next schedule obj.
                continue

            start_date = schedule.date
            if not start_date:
                start_date = dates_this_week[current_day] + timedelta(weeks=week)
            start_time = schedule.start_time
            start_datetime = datetime(start_date.year, start_date.month, start_date.day, start_time.hour,start_time.minute, start_time.second)
            start_datetime = mentor_timezone.localize(start_datetime).astimezone(tz)
            start_date = start_datetime.date()
            start_time = start_datetime.timetz()
            end_time = schedule.end_time
            end_datetime = datetime(start_date.year, start_date.month, start_date.day, end_time.hour, end_time.minute, end_time.second)
            end_datetime = mentor_timezone.localize(end_datetime).astimezone(tz)
            if end_datetime < start_datetime:
                end_datetime = end_datetime + timedelta(1)
            end_time = end_datetime.timetz()

            duration = schedule.get_duration()

            if current_day < 0:
                # current_day < 0 means we are referring to a day before today,
                # which is not relevant, and is out of bounds of `availability` array.
                continue

            type = schedule.meeting_type
            location = None
            phone_number = None
            video_id = None

            if schedule.location != '':
                location = schedule.location
            elif mentor.default_location:
                location = mentor.default_location
            if schedule.phone_number:
                phone_number = schedule.phone_number
            elif mentor.phone:
                phone_number = mentor.phone
            elif mentor.phone_number:
                phone_number = mentor.phone_number
            if schedule.video_id:
                video_id = schedule.video_id
            elif mentor.skype:
                video_id = mentor.skype

            if blackout_dates[current_day] or \
                    (start_date and start_date > dates_this_week[current_day]) or \
                    (not has_weekday_decimal and dates_this_week[current_day] < today):
                # If MentorSchedule object doesn't apply to this week
                possible_slots = 0
            else:
                # Problem here for timezones that cross the dateline
                # possible_slots comes out negative and following for loop doesn't iterate
                possible_slots = time_minute_diff_datetimes(start_datetime, end_datetime)
                possible_slots /= duration
                possible_slots = int(possible_slots)
                # possible_slots is the number of possible slots based on
                # start/end time and duration. (We round down.)

            slots = []
            overflow_slots = []
            perm_start_time = start_time
            for i in range(possible_slots):
                if start_date is not None:
                    end_time = (datetime.combine(start_date, start_time) + timedelta(minutes=duration)).timetz()
                    start_datetime = datetime.combine(dates_this_week[current_day], start_time)
                    if start_datetime > now:
                        # TODO: fix this variable name so it's not a python builtin...
                        if type != 4 :  # not in person show the type
                            slot_location = 'Phone' if type == 1 else 'Video Call'
                        else:  # in person - use the location
                            slot_location = location
                        if start_time >= perm_start_time:
                            slot_info = [start_time.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), True, schedule.duration, slot_location]
                            slots.append(slot_info)
                        else:
                            overflow_info = [start_time.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), True, schedule.duration, slot_location]
                            overflow_slots.append(overflow_info)
                    start_time = end_time

            if slots != []:
                has_schedule = True

            # need to shift availability index based on the current date
            avail_index = current_day
            availability[avail_index][2] += slots
            availability[avail_index + 1][2] += overflow_slots
            availability[avail_index][4] = type
            if type == 1:
                availability[avail_index][5] = phone_number
            elif type == 3:
                availability[avail_index][5] = video_id
            else:
                availability[avail_index][5] = location

    # if we don't have any repeated schedule in a given period and no one time availability, at least try to get
    # any onetime availability
    if not has_schedule and one_times.count() == 0:
        availability = []

    for one_time in one_times:
        date = one_time.date
        start_time = one_time.start_time
        start_datetime = datetime(date.year, date.month, date.day, start_time.hour, start_time.minute, start_time.second)
        start_datetime = mentor_timezone.localize(start_datetime).astimezone(tz)
        start_time = start_datetime.timetz()
        date = start_datetime.date()

        date_str = date.strftime("%Y %b %d")

        year, month, day = str(date_str).split(' ')
        date_str = month + ' ' + day

        end_time = one_time.end_time
        end_datetime = datetime(date.year, date.month, date.day, end_time.hour, end_time.minute, end_time.second)
        end_datetime = mentor_timezone.localize(end_datetime).astimezone(tz)
        end_time = end_datetime.timetz()
        duration = one_time.get_duration()

        type = one_time.meeting_type
        location = None

        if type == 1:
            if one_time.phone_number:
                location = one_time.phone_number
            elif mentor.phone:
                location = mentor.phone
            elif mentor.phone_number:
                location = mentor.phone_number
        elif type == 3:
            if one_time.video_id:
                location = one_time.video_id
            elif mentor.skype:
                location = mentor.skype
        else:
            if one_time.location != '':
                location = one_time.location
            elif mentor.default_location:
                location = mentor.default_location

        possible_slots = time_minute_diff_datetimes(start_datetime, end_datetime)
        possible_slots /= duration
        possible_slots = int(possible_slots)
        slots = []
        overflow_slots = []
        perm_start_time = start_time
        index = (date - today).days

        if DEBUG and index < 0:
            raise IndexError("One-times processed here should be in the future, not past.")

        if (today + timedelta(index)) != date:
            continue

        for i in range(possible_slots):
            if date is not None:
                end_time = (datetime.combine(date, start_time) + timedelta(minutes=duration)).timetz()
                start_datetime = datetime.combine(date, start_time)
                if start_datetime > now:
                    if type != 4 :  # not in person show the type
                        slot_location = 'Phone' if type == 1 else 'Video Call'
                    else:  # in person - use the location
                        slot_location = location
                    temp = [start_time.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), True, one_time.duration, slot_location]
                    if not has_schedule or (not temp in availability[index][2] and availability[index][1] == date_str):
                        if start_time >= perm_start_time:
                            slots.append(temp)
                        else:
                            overflow_slots.append(temp)
                start_time = end_time

        if not has_schedule:
            if has_weekday_decimal:
                avail_info = [[int(date.strftime('%w')), date.strftime('%a')], date.strftime('%b %d'), slots, date.year, type, location]
                availability.append(avail_info)
            else:
                if index >= total_days:
                    availability.append([[int(date.strftime('%w')), date.strftime('%a')], date.strftime('%b %d'), slots, date.year, type, location])
                    if overflow_slots != []:
                        over_date = (start_datetime + timedelta(1)).date()
                        availability.append([[int(over_date.strftime('%w')), over_date.strftime('%a')], over_date.strftime('%b %d'), overflow_slots, over_date.year, type, location])
                else:
                    availability[index][2] += slots
                    availability[index + 1][2] += overflow_slots
                    availability[index][4] = type
                    availability[index][5] = location
        else:
            if index >= total_days:
                availability.append([[int(date.strftime('%w')), date.strftime('%a')], date.strftime('%b %d'), slots, date.year, type, location])
                if overflow_slots != []:
                    over_date = (start_datetime + timedelta(1)).date()
                    availability.append([[int(over_date.strftime('%w')), over_date.strftime('%a')], over_date.strftime('%b %d'), overflow_slots, over_date.year, type, location])
            else:
                availability[index][2] += slots
                # availability[index + 1][2] += overflow_slots
                availability[index][4] = type
                availability[index][5] = location

    # perhaps here is where you could add in who the meeting is with ??
    # availability[day][2] add mentor field?
    additional_slot = []

    for session in scheduled_team_sessions:
        team = session.get_team()
        timeslot = session.get_datetime().astimezone(tz)
        end_time = session.get_endtime().astimezone(tz)
        day = timeslot.weekday()
        end_time = end_time.timetz()
        start_time = timeslot.timetz()
        index = (timeslot.date() - today).days
        if DEBUG and index < 0:
            raise IndexError("Team Mentoring Sessions processed here should be in the future, not past.")
        if not availability:
            continue
        filled_success = False
        for slot in availability[index][2]:
            slot_start = datetime.strptime(slot[0], "%I:%M%p").timetz()
            slot_end = datetime.strptime(slot[1], "%I:%M%p").timetz()
            if slot_start >= start_time and slot_end <= end_time:
                slot[2] = False
                slot.append(team)
                filled_success = True
                continue
            if slot_start >= start_time and (slot_end > end_time > slot_start):
                slot[2] = False
                slot.append(team)
                filled_success = True
                continue
            if slot_end >= start_time and (start_time < slot_end <= end_time):
                slot[2] = False
                slot.append(team)
                filled_success = True
                continue
        if not filled_success:
            additional_slot.append((
                index,
                [timeslot.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), False, session.duration, session.get_location_or_type(), team]
                )
            )

    for session in scheduled_sessions:
        mentee = session.get_mentee()
        timeslot = session.get_datetime(mentor).astimezone(tz)
        end_time = session.get_endtime(mentor).astimezone(tz)
        day = timeslot.weekday()
        end_time = end_time.timetz()
        start_time = timeslot.timetz()
        index = (timeslot.date() - today).days

        if DEBUG and index < 0:
            raise IndexError("Mentoring Sessions processed here should be in the future, not past.")

        if not availability:
            continue

        filled_success = False
        for slot in availability[index][2]:
            slot_start = datetime.strptime(slot[0], "%I:%M%p").timetz()
            slot_end = datetime.strptime(slot[1], "%I:%M%p").timetz()
            if slot_start >= start_time and slot_end <= end_time:
                slot[2] = False
                slot.append(mentee)
                filled_success = True
                continue

            if slot_start >= start_time and (slot_end > end_time > slot_start):
                slot[2] = False
                slot.append(mentee)
                filled_success = True
                continue

            if slot_end >= start_time and (start_time < slot_end <= end_time):
                slot[2] = False
                slot.append(mentee)
                filled_success = True
                continue

        if not filled_success:
            additional_slot.append((
                index,
                [timeslot.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), False, session.duration, session.get_location_or_type(), mentee]
                )
            )

    for additional in additional_slot:
        index, slot = additional
        availability[index][2].append(slot)

    # below line gets rid of empty days when there is no availability (x[2] is empty)
    # not needed for list view anyway
    availability = (list(filter(lambda x: x[2] != [], availability)))
    #  sort one time and recurring together
    for entry in availability:
        unsorted_times = entry[2]
        # sort it by the first entry of time period, will always be the earliest.
        sorted_times = sorted(unsorted_times, key = lambda x: datetime.strptime(x[0], '%I:%M%p'))
        entry[2] = sorted_times

    if not show_this_week:
        availability = filter_after_next_monday(availability)
    return availability
