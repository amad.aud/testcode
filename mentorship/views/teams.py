# django imports
import datetime as dt
import json
from datetime import timedelta
from pathlib import Path
from typing import Iterable
# startuptree imports
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import ObjectDoesNotExist, Q
from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import Http404, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.http import require_POST

from StartupTree.loggers import app_logger
from StartupTree.strings import *
from StartupTree.utils import QuerySetType
# branch imports
from branch.models import StartupMember, Startup
from branch.views.abstract import AbstractDiscoverPeople
# forms imports
from forms.models import Form
from forms.serializer.forms import custom_member_dict
from mentorship.models import (
    MentorshipActivity,
    TeamMentorshipActivity,
    TeamMentorship,
    TeamMentorshipSession,
    MentorshipSessionFeedback,
    TeamNote,
    TeamGoal,
    Mentor,
    MentorshipFile,
)
from mentorship.strings import *
from mentorship.tasks import \
    team_session_change_notify_venture, \
    team_session_change_notify_mentor, \
    team_session_changed_noti_admin, \
    notify_members_team_mentor_removed, \
    notify_mentor_team_mentor_removed, \
    notify_admin_team_removed
from mentorship.utils import is_user_authorized_team_mentorship
from trunk.perms import is_admin_authorized
from mentorship.st_cronofy import is_cronofy_linked, get_cronofy_calendar_id, cronofy_delete_from_calendar
from mentorship.modelforms import MentorshipFileForm
from trunk.decorators import only_university
# trunk imports
from trunk.models import (
    University,
    UniversityStyle,
    AccessLevel,
    TeamLandingPage
)
# venture imports
from venture.models import MemberToAdmin
from django.views import View
from django.utils.decorators import method_decorator


class TeamMentorshipView(View):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TeamMentorshipView, self).dispatch(*args, **kwargs)

    def get_teams(self, request, team_url_id=None):
        # TEAMS
        member_id = request.current_member.id
        admin_pages = MemberToAdmin.objects.select_related('startup', 'startup__style').filter(member_id=member_id, startup__team_mentorship_participant=True)
        teams = []
        team = None
        for admin_page in admin_pages:
            startup = admin_page.startup
            if team_url_id and startup.url_name == team_url_id:
                team = startup
            teams.append(startup)
        return teams, team

    def get_mentor_teams(self, request, team_url_id=None):
        current_mentor_profile = request.current_member
        current_mentor_obj = Mentor.objects.get(profile=current_mentor_profile, platform_id=request.university.id)
        mentorships = TeamMentorship.objects.filter(mentor=current_mentor_obj)
        teams = []
        current_team = None
        for mentorship in mentorships:
            team = mentorship.team
            if team_url_id == team.url_name:
                current_team = team
            teams.append({
                'name': team.name,
                'url_name': team.url_name,
                'image': team.get_image(),
                'session_cnt': TeamMentorshipSession.objects.filter(mentorships=mentorship, is_cancelled=False).count(),
                'goal_complete_cnt': TeamGoal.objects.filter(mentorship=mentorship, completed=True).count(),
                'goal_incomplete_cnt': TeamGoal.objects.filter(mentorship=mentorship, completed=False).count(),
                'note_cnt': TeamNote.objects.filter(team=mentorship.team).count()
            })
        return teams, current_team

    def get_team_mentors(self, request, team, team_filter=False, date_filter=False, complete_filter=False):
        # MENTORS
        team_ments_tmp = []
        my_team_mentors = []
        my_team_mentorships = TeamMentorship.objects.select_related(
            'mentor', 'mentor__profile').filter(team=team, is_active=True)

        mentors = []
        for team_mentorship in my_team_mentorships:
            mentor = team_mentorship.mentor
            mentors.append({
                'name': mentor.profile.get_full_name(),
                'url_name': mentor.profile.url_name,
                'image': mentor.profile.get_image(),
                'email': mentor.email
            })
        return mentors

    def tmp_get_team_mentors(self, request, team, team_filter=False, date_filter=False, complete_filter=False):
        team_ments_tmp = []
        my_team_mentors = []
        my_team_mentorships = TeamMentorship.objects.select_related(
            'mentor', 'mentor__profile').filter(team=team, is_active=True)
        # Cylce through the mentorships
        # Get the mentor for each of these mentorships, placing them in an array w/o duplicates
        # Filter for team mentorships w/ the users teams and this mentor
        #
        for mentorship in my_team_mentorships:
            if mentorship.mentor not in team_ments_tmp:
                team_ments_tmp.append(mentorship.mentor)

        for index, mentor in enumerate(team_ments_tmp):
            mentorships = TeamMentorship.objects.filter(team=team, is_active=True, mentor=mentor)
            ventures = [mentorship.team for mentorship in mentorships]

            goals = TeamGoal.objects.filter(mentorship__in=mentorships).order_by('due_date', '-id')
            goals_arr = []
            goals_completed_recently_arr = []
            if date_filter:
                date_from_str = request.GET.get('date_from')
                date_to_str = request.GET.get('date_to')

                date_from = dt.datetime.strptime(date_from_str, '%m/%d/%Y')
                date_to = dt.datetime.strptime(date_to_str, '%m/%d/%Y')

                goals = goals.filter(due_date__gte=date_from, due_date__lte=date_to)

            if complete_filter:
                completeness = request.GET.get('completeness', "none")

                if completeness == "complete":
                    goals_completed_recently = goals.exclude(completed=False).filter(
                        completed_on__gte=timezone.now() - timedelta(days=7)).order_by('-completed_on')

                    goals_completed_recently_arr = [goal for goal in goals_completed_recently]
                    goals_arr = []
                elif completeness == "incomplete":
                    goals_completed_recently_arr = []
                    goals_arr = [goal for goal in goals]

            else:
                goals_completed_recently = goals.exclude(completed=False).filter(
                    completed_on__gte=timezone.now() - timedelta(days=7)).order_by('-completed_on')[:5]

                goals_completed_recently_arr = [goal for goal in goals_completed_recently]
                goals_arr = [goal for goal in goals]

            can_schedule = mentor.has_schedule() or mentor.has_onetimes()
            team_ments_tmp[index] = {
                'teams': ventures,
                'mentor': mentor,
                'can_schedule': can_schedule,
                'goals': goals_arr,
                'goals_completed': goals_completed_recently_arr
            }

        for item in team_ments_tmp:
            my_team_mentors.append({
                'mentor_id': item['mentor'].id,
                'ventures': item['teams'],
                'goals': item['goals'],
                'goals_completed': item['goals_completed'],
                'can_schedule': item['can_schedule'],
                'profile': item['mentor'].profile,
                'preferred_contact': item['mentor'].default_meeting_type,
                'get_preferred_contact': item['mentor'].get_preferred_contact(),
            })

        return my_team_mentors

    def get_team_notes(self, current_team, current_member):
        notes = []
        for note in TeamNote.objects.select_related('session', 'team', 'creator').filter(team=current_team).order_by(
                '-session__datetime'):
            notes.append(note.to_dict(current_member))
        return json.dumps(notes)

    def get_team_files(self, current_team):
        files = []
        for file in MentorshipFile.objects.filter(team=current_team).order_by('-created_on'):
            files.append({
                'created': file.created_on.strftime('%b-%d-%Y %H:%M'),
                'comment': file.comment,
                'file_id': file.id
            })
        return json.dumps(files)

    def mentor_to_dict(self, mentor : Mentor):
        dict = {
            'name': mentor.profile.get_full_name()
        }
        return dict

    def get_team_goals(self, current_team, current_member):
        goals = []
        for goal in TeamGoal.objects.select_related('team', 'creator').filter(team=current_team).order_by('-due_date', 'datetime'):
            goals.append(goal.to_dict(current_member.id))
        return json.dumps(goals)

    @staticmethod
    def meetings_to_dict(meeting_objs : QuerySetType[TeamMentorshipSession], platform : University):
        meetings = []

        for meeting in meeting_objs:
            meeting_custom = custom_member_dict(
                None, platform.id, Form.MENTORMEETING, meeting=meeting)
            meeting_json = meeting.to_dict()
            meeting_json['custom'] = meeting_custom
            meetings.append(meeting_json)
        return json.dumps(meetings)

    def get_mentees(self, team):
        mentees = []
        for admin in MemberToAdmin.objects.select_related('member', 'member__user').filter(startup=team):
            mentee = admin.member
            mentee_image = mentee.get_image()
            mentees.append({
                'name': mentee.get_full_name(),
                'url_name': mentee.url_name,
                'image': mentee_image if mentee_image else '',
                'email': mentee.user.email
            })
        return mentees

    @staticmethod
    def get_sessions(current_team, university, additional_filters: Q):
        raw_sessions = TeamMentorshipSession.objects \
            .select_related('team', 'creator', 'creator__user') \
            .filter(team=current_team, is_cancelled=False) \
            .filter(additional_filters) \
            .prefetch_related('mentorships', 'mentorships__mentor', 'mentorships__mentor__profile') \
            .distinct().order_by('datetime')

        return TeamMentorshipView.meetings_to_dict(raw_sessions, university)

    @staticmethod
    def get_upcomming_sessions__mentee(current_member, current_team, university):
        additional_filters = Q(datetime__gte=timezone.now()) \
                             & (Q(members_attending=current_member.user) | Q(creator_id=current_member.id))
        return TeamMentorshipView.get_sessions(current_team, university, additional_filters=additional_filters)

    @staticmethod
    def get_upcomming_sessions__mentor(current_team, university):
        return TeamMentorshipView.get_sessions(current_team, university, Q(datetime__gte=timezone.now()))

    @staticmethod
    def get_past_sessions(current_team: Startup, university: University):
        return TeamMentorshipView.get_sessions(current_team, university, Q(datetime__lt=timezone.now()))


class TeamMenteeDashboard(TeamMentorshipView):
    template = 'mentorship/team-mentee-dashboard.html'
    PAGE_TYPE_NOTE = 'notes'
    PAGE_TYPE_GOAL = 'goals'
    PAGE_TYPE_SESSIONS = 'sessions'
    PAGE_TYPE_DEFAULT = 'mentors'

    def get(self, request, *args, **kwargs):
        url_id = kwargs.get('url_name')
        page_type = kwargs.get('page_type', None)
        teams, current_team = self.get_teams(request, url_id)
        if not current_team:
            if len(teams) == 0:
                # view no team mentorship page
                return TemplateResponse(
                    request,
                    self.template,
                    {
                        'page_name': 'team-mentee-dashboard',
                        'no_team': True
                    }
                )
            else:
                if not url_id:
                    return HttpResponseRedirect(reverse("team-mentee-dashboard-specific", kwargs={'url_name': teams[0].url_name}))
                else:
                    raise Http404('No Team mentorship for {0}'.format(url_id))
        teams_dict = []
        for team in teams:
            teams_dict.append({'name': team.name, 'url_name': team.url_name, 'image': team.get_image()})
        teams_dict = json.dumps(teams_dict)
        context = {
            'page_name': 'team-mentee-dashboard',
            'my_teams': teams_dict,
            'current_team': {
                'name': current_team.name,
                'url_name': current_team.url_name
            },
            'menu': self.PAGE_TYPE_DEFAULT,
            'mentors': [],
            'notes': [],
            'upcoming_meetings': [],
            'note_files': [],
            'teamMenteeOptOut': request.university_style.team_mentee_opt_out == UniversityStyle.ALLOW_OPT_OUT
        }
        if page_type is None:
            team_mentors = self.get_team_mentors(request, current_team)
            context['mentors'] = json.dumps(team_mentors)
            context['mentees'] = self.get_mentees(current_team)
            context['upcoming_meetings'] = TeamMentorshipView.get_upcomming_sessions__mentee(
                request.current_member, current_team, request.university)
            context['customQuestions'] = json.dumps(custom_member_dict(None, request.university.id,
                                                                      Form.MENTORMEETING, is_me=True))
        elif page_type == self.PAGE_TYPE_NOTE:
            context['menu'] = self.PAGE_TYPE_NOTE
            context['notes'] = self.get_team_notes(current_team, request.current_member)
            context['note_files'] = self.get_team_files(current_team)

            my_sessions = []
            for session in TeamMentorshipSession.objects.filter(team=current_team, is_cancelled=False).order_by('datetime'):
                mentors = []
                for mentorship in session.mentorships.all():
                    mentors.append(mentorship.mentor.profile.get_full_name())
                my_sessions.append({
                    'id': session.id,
                    'datetime': session.get_datetime().strftime("%Y-%m-%d %-I:%M %p %Z"),
                    'mentors': (",").join(mentors)
                })
            context['sessions'] = my_sessions

        elif page_type == self.PAGE_TYPE_GOAL:
            context['menu'] = self.PAGE_TYPE_GOAL
            team_mentors = self.get_team_mentors(request, current_team)
            context['mentors'] = json.dumps(team_mentors)
            context['goals'] = self.get_team_goals(current_team, request.current_member)

        elif page_type == self.PAGE_TYPE_SESSIONS:
            context['menu'] = self.PAGE_TYPE_SESSIONS
            context['upcoming_meetings'] = TeamMenteeDashboard.get_upcomming_sessions__mentee(
                request.current_member, current_team, request.university)
            context['past_sessions'] = TeamMentorshipView.get_past_sessions(current_team, request.university)
        else:
            raise Http404('Page not found')

        return TemplateResponse(
            request,
            self.template,
            context
        )


class TeamCreateNote(TeamMentorshipView):
    def post(self, request, *args, **kwargs):

        team_url_name = request.POST.get('team_id')
        note_text = request.POST.get('note_text')
        if not team_url_name or not note_text:
            return JsonResponse({}, status=403)

        creator = request.current_member
        current_team = Startup.objects.get(url_name=team_url_name)
        is_authorized = False
        for team_mentorship in TeamMentorship.objects.select_related('mentor',
                                                                     'mentor__profile',
                                                                     'team').filter(team=current_team):
            if is_user_authorized_team_mentorship(request.is_university,
                                                  request.access_levels,
                                                  team_mentorship,
                                                  request.user.id):
                is_authorized = True
                break
        if not is_authorized:
            app_logger.error("Not authorized")
            raise Http404()
        session = TeamMentorshipSession.objects.select_related('team').get(id=int(request.POST.get('session')))
        if session.team.url_name != team_url_name:
            app_logger.error("Incorrect argument")
            raise Http404()

        is_by_mentor = False
        if TeamMentorship.objects.select_related('mentor', 'mentor__profile', 'team').filter(
                team=current_team,
                mentor__profile_id = creator.id):
            is_by_mentor = True

        activity = TeamMentorshipActivity.objects.create_activity(MentorshipActivity.NOTE_CREATED, current_team, request.university)
        TeamNote.objects.create(
            creator=creator,
            is_by_mentor=is_by_mentor,
            team=current_team,
            text=note_text,
            session=session,
            mentorship_activity=activity)
        notes = self.get_team_notes(current_team, creator)
        return JsonResponse({"status": 200, 'notes': notes})


class TeamCreateFile(TeamMentorshipView):
    def post(self, request, *args, **kwargs):
        team_url_name = request.POST.get('team', None)
        if not team_url_name:
            raise Http404()

        current_team = Startup.objects.get(url_name=team_url_name)
        is_authorized = False
        for team_mentorship in TeamMentorship.objects.filter(team=current_team):
            if is_user_authorized_team_mentorship(request.is_university, request.access_levels, team_mentorship,
                                                  request.user.id):
                is_authorized = True
                break

        if not is_authorized:
            raise Http404()

        file_form = MentorshipFileForm(request.POST, request.FILES)
        if file_form.is_valid():
            file = file_form.cleaned_data['file']
            comment = ''
            if file_form.cleaned_data.get('comment'):
                comment = file_form.cleaned_data['comment']
            file_name = Path(file.name).name
            MentorshipFile.objects.create(
                team=current_team,
                file=file,
                file_name=file_name,
                comment=comment
            )
        else:
            app_logger.info(file_form.errors)
            return JsonResponse({'status': 403, 'error': "Invalid file submission"})
        files = self.get_team_files(current_team)
        return JsonResponse({"status": 200, 'files': files})


class TeamCreateGoal(TeamMentorshipView):
    def post(self, request, *args, **kwargs):
        team_url_name = request.POST.get('team')
        goal_text = request.POST.get('goal')
        due_date = request.POST.get("due_date")
        if not team_url_name or not goal_text or not due_date:
            return JsonResponse({}, status=403)

        due_date = dt.datetime.strptime(due_date, '%Y-%m-%d').date()
        creator = request.current_member
        current_team = Startup.objects.get(url_name=team_url_name)
        is_authorized = False
        for team_mentorship in TeamMentorship.objects.select_related('mentor',
                                                                     'mentor__profile',
                                                                     'team').filter(team=current_team):
            if is_user_authorized_team_mentorship(request.is_university,
                                                  request.access_levels,
                                                  team_mentorship,
                                                  request.user.id):
                is_authorized = True
                break
        if not is_authorized:
            app_logger.error("Not authorized")
            raise Http404()

        activity = TeamMentorshipActivity.objects.create_activity(MentorshipActivity.GOALSET_CREATED, current_team, request.university)
        new_goal = TeamGoal.objects.create(
            creator=creator,
            due_date=due_date,
            note=goal_text,
            team=current_team,
            datetime=timezone.now(),
            mentorship_activity=activity)
        goals = self.get_team_goals(current_team, request.current_member)
        return JsonResponse({"status": 200, 'goals': goals})


class TeamEditGoal(TeamMentorshipView):
    def post(self, request, *args, **kwargs):
        team_url_name = request.POST.get('team')
        goal_id = request.POST.get('goal_id')
        if not team_url_name or not goal_id:
            return JsonResponse({}, status=403)

        creator = request.current_member
        current_team = Startup.objects.get(url_name=team_url_name)
        is_authorized = False
        for team_mentorship in TeamMentorship.objects.select_related('mentor',
                                                                     'mentor__profile',
                                                                     'team').filter(team=current_team):
            if is_user_authorized_team_mentorship(request.is_university,
                                                  request.access_levels,
                                                  team_mentorship,
                                                  request.user.id):
                is_authorized = True
                break
        if not is_authorized:
            app_logger.error("Not authorized")
            raise Http404()

        goal = TeamGoal.objects.get(id=int(goal_id))
        if goal.creator_id != creator.id:
            return HttpResponseForbidden()
        completed = request.POST.get('completed')
        if completed and completed == 'complete':
            goal.completed = True
            goal.completed_on = timezone.now()
        goal_text = request.POST.get('goal')
        if goal_text and len(goal_text.strip()) > 0:
            goal.note = goal_text
        due_date = request.POST.get("due_date")
        if due_date:
            due_date = dt.datetime.strptime(due_date, '%Y-%m-%d').date()
            goal.due_date = due_date
        goal.save()
        goal_json = goal.to_dict(creator.id)
        return JsonResponse({"status": 200, 'goal': goal_json})


class TeamMentorDashboard(TeamMentorshipView):
    template = 'mentorship/team-mentor-dashboard.html'
    PAGE_TYPE_NOTE = 'notes'
    PAGE_TYPE_GOAL = 'goals'
    PAGE_TYPE_SESSIONS = 'sessions'
    PAGE_TYPE_DEFAULT = 'teams'

    def get(self, request, *args, **kwargs):
        url_id = kwargs.get('url_name')
        page_type = kwargs.get('page_type', None)
        teams, current_team = self.get_mentor_teams(request, url_id)
        if not current_team:
            if len(teams) == 0:
                # view no team mentorship page
                return TemplateResponse(
                    request,
                    self.template,
                    {
                        'page_name': 'team-mentor-dashboard',
                        'no_team': True
                    }
                )
            else:
                if not url_id:
                    return HttpResponseRedirect(reverse("team-mentor-dashboard-specific", kwargs={'url_name': teams[0]['url_name']}))
                else:
                    raise Http404('No Team mentorship for {0}'.format(url_id))

        context = {
            'page_name': 'team-mentor-dashboard',
            'my_teams': json.dumps(teams),
            'current_team': {
                'name': current_team.name,
                'url_name': current_team.url_name
            },
            'menu': self.PAGE_TYPE_DEFAULT,
            'mentors': [],
            'notes': [],
            'upcoming_meetings': [],
            'note_files': [],
            'teamMenteeOptOut': request.university_style.team_mentee_opt_out == UniversityStyle.ALLOW_OPT_OUT
        }
        if page_type is None:
            team_mentors = self.get_team_mentors(request, current_team)
            context['mentors'] = json.dumps(team_mentors)
            context['mentees'] = self.get_mentees(current_team)
            context['upcoming_meetings'] = TeamMentorDashboard\
                .get_upcomming_sessions__mentor(current_team, request.university)
            context['customQuestions'] = json.dumps(custom_member_dict(None, request.university.id,
                                                                       Form.MENTORMEETING, is_me=True))

        elif page_type == self.PAGE_TYPE_NOTE:
            context['menu'] = self.PAGE_TYPE_NOTE
            context['notes'] = self.get_team_notes(current_team, request.current_member)
            context['note_files'] = self.get_team_files(current_team)

            my_sessions = []
            for session in TeamMentorshipSession.objects.filter(team=current_team, is_cancelled=False).order_by('datetime'):
                mentors = []
                for mentorship in session.mentorships.all():
                    mentors.append(mentorship.mentor.profile.get_full_name())
                my_sessions.append({
                    'id': session.id,
                    'datetime': session.get_datetime().strftime("%Y-%m-%d %-I:%M %p %Z"),
                    'mentors': (",").join(mentors)
                })
            context['sessions'] = my_sessions

        elif page_type == self.PAGE_TYPE_GOAL:
            context['menu'] = self.PAGE_TYPE_GOAL
            team_mentors = self.get_team_mentors(request, current_team)
            context['mentees'] = self.get_mentees(current_team)
            context['goals'] = self.get_team_goals(current_team, request.current_member)

        elif page_type == self.PAGE_TYPE_SESSIONS:
            context['menu'] = self.PAGE_TYPE_SESSIONS
            context['upcoming_meetings'] = TeamMentorshipView\
                .get_upcomming_sessions__mentor(current_team, request.university)
            context['past_sessions'] = TeamMentorshipView.get_past_sessions(current_team, request.university)
        else:
            raise Http404('Page not found')
        return TemplateResponse(
            request,
            self.template,
            context
        )


@login_required
@require_POST
def team_mentorshipsession_cancel(request, session_id, auto=False):
    """
    Cancel a upcoming mentorship session
    :param request:
    :param mentorship_id:
    :return:
    """
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE,
               PAYLOAD_MSG: ""}
    try:
        session = TeamMentorshipSession.objects.get(id=int(session_id))
        is_authorized = False
        for team_mentorship in session.mentorships.all():
            if is_user_authorized_team_mentorship(request.is_university, request.access_levels, team_mentorship,
                                                  request.user.id):
                is_authorized = True
                break
        if not is_authorized:
            raise Http404()
        mentorships = session.mentorships.iterator()

        change = "cancel"
        if auto:
            change = "auto cancel"

        current_user_id = request.user.id
        current_site_id = request.current_site.id
        transaction.on_commit(lambda: team_session_change_notify_venture.delay(current_user_id, current_site_id, session_id, change))
        transaction.on_commit(lambda: team_session_change_notify_mentor.delay(current_user_id, current_site_id, session_id, change))

        uni_style: UniversityStyle = request.university_style
        if uni_style.admin_notify_of_team_meeting_cancelled:
            team_session_changed_noti_admin(session, request.university, state=SESSION_CANCELLED)

        session.is_cancelled = True

        cancel_reason = request.POST.get('reason')
        session.cancel_reason = cancel_reason

        event_id = session.id
        for mentorship in mentorships:
            mentor_user = mentorship.mentor.profile.user
            has_cronofy = is_cronofy_linked(mentor_user)
            if has_cronofy:
                calendar_id = get_cronofy_calendar_id(mentor_user)
                cronofy_delete_from_calendar(mentor_user, calendar_id, event_id, refresh_cronofy_cache=True)

        for user in session.members_attending.all():
            if is_cronofy_linked(user):
                calendar_id = get_cronofy_calendar_id(user)
                cronofy_delete_from_calendar(user, calendar_id, event_id, refresh_cronofy_cache=False)

        session.save()

    except ObjectDoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid request."
    return JsonResponse(payload)


@login_required
@require_POST
def drop_team_meeting(request, session_id):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE,
               PAYLOAD_MSG: ""}
    try:
        session = TeamMentorshipSession.objects.get(id=int(session_id))

        is_authorized = False
        team = session.team
        current_user = request.user
        for team_mentorship in session.mentorships.all():
            if is_user_authorized_team_mentorship(request.is_university, request.access_levels, team_mentorship,
                                                  current_user.id):
                is_authorized = True
                break
        if not is_authorized:
            raise Http404()

        profile = request.current_member
        is_mentor = False
        try:
            mentor = Mentor.objects.get(profile=profile, is_team_mentor=True, platform=request.university)
            mentorship = TeamMentorship.objects.get(mentor=mentor, team=team)
            is_mentor = mentorship.exists()
        except:
            is_mentor = False

        if is_mentor:
            session.mentorships.remove(mentorship)

            if session.mentorships.count() == 0:
                # Cancel
                team_mentorshipsession_cancel(request, session_id, True)
            else:
                has_cronofy = is_cronofy_linked(current_user)
                current_user_id = request.user.id
                current_site_id = request.current_site.id
                transaction.on_commit(
                    lambda: team_session_change_notify_venture.delay(current_user_id, current_site_id, session_id,
                                                                     "drop"))
                transaction.on_commit(
                    lambda: team_session_change_notify_mentor.delay(current_user_id, current_site_id, session_id,
                                                                    "drop"))
                if has_cronofy:
                    calendar_id = get_cronofy_calendar_id(current_user)
                    cronofy_delete_from_calendar(current_user, calendar_id, session.id, refresh_cronofy_cache=True)
        else:
            session.member_drop(current_user)

            if session.members_attending.count() == 0:
                # Cancel
                team_mentorshipsession_cancel(request, session_id, auto=True)
            else:
                current_user_id = request.user.id
                current_site_id = request.current_site.id
                transaction.on_commit(lambda: team_session_change_notify_venture.delay(current_user_id, current_site_id, session_id, "drop"))

                if is_cronofy_linked(current_user):
                    calendar_id = get_cronofy_calendar_id(current_user)
                    cronofy_delete_from_calendar(current_user, calendar_id, session.id, refresh_cronofy_cache=False)

    except ObjectDoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid request."
    return JsonResponse(payload)


def team_mentorship_landing(request):

    is_edit = is_admin_authorized(request.is_university, request.is_super,
                                  request.access_levels, request.is_startuptree_staff, AccessLevel.TEAM_MENTORSHIP)

    try:
        team_landing = TeamLandingPage.objects.get(university=request.university)
    except TeamLandingPage.DoesNotExist:
        return HttpResponseRedirect(reverse('discover_people', kwargs={'page_name': AbstractDiscoverPeople.TEAM_MENTOR_PAGE}))

    return TemplateResponse(
        request,
        'mentorship/team_mentorship_landing.html',
        {
            'is_edit': is_edit,
            'landing': team_landing
        }
    )


@login_required
def remove_team_mentee(request):
    payload = {"status": 200}

    current_user = request.user
    profile = request.current_member
    mentor = Mentor.objects.get(profile=profile, is_team_mentor=True)

    team = request.POST.get('team_id')
    mentorship = TeamMentorship.objects.get(mentor=mentor, team_id=int(team))
    team_obj = Startup.objects.get(id=int(team))
    transaction.on_commit(lambda: notify_admin_team_removed(request, team_obj, mentor))
    mentorship.delete()

    return JsonResponse(payload)


@login_required
def remove_team_mentor(request):
    mentor_id = request.POST.get('mentor_id')
    team_url_id = request.POST.get('team_url_id')
    mentor = Mentor.objects.get(id=mentor_id)
    team = Startup.objects.get(url_name=team_url_id)
    try:
        team_mentorship = TeamMentorship.objects.get(team=team, mentor=mentor)
        transaction.on_commit(lambda: notify_mentor_team_mentor_removed(request, team, mentor))
        transaction.on_commit(lambda: notify_members_team_mentor_removed(request, team, mentor))

        team_mentorship.delete()
    except TeamMentorship.DoesNotExist:
        app_logger.error('Invalid team mentorship')
        raise Http404()
    return JsonResponse({'status': 200})


@require_POST
@login_required
@only_university
def team_mentorshipsession_add__by_team_id(request, university):
    from mentorship.views.mixins.scheduling import prepare_session_scheduling  # import here to prevent circular import
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
        request.access_levels, request.is_startuptree_staff, AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        return HttpResponseForbidden()
    return JsonResponse(prepare_session_scheduling(request.POST, request.session['django_timezone'], university,
                                      request.university_style, request.current_member,
                                      is_admin=True))


def get_team_session__help(current_member: StartupMember, session_id: int, is_mentee: bool):
    """ Returns a TeamMentorshipSession object if one exists where current member is
     an appropriate attendee type (mentor or mentee) of that session.

     Otherwise, raises TeamMentorshipSession.DoesNotExist"""
    team_session = TeamMentorshipSession.objects.get(id=session_id)
    mentors = team_session.get_mentors()
    from_member = current_member
    if is_mentee:
        if from_member.user not in team_session.members_attending.all():
            # User is not an attending mentee
            raise TeamMentorshipSession.DoesNotExist
    else:
        if not mentors.filter(profile=from_member).exists():
            # User is not an attendee mentor
            raise TeamMentorshipSession.DoesNotExist

    return team_session


def get_team_session_info(current_member: StartupMember, request_data):
    """
    Returns a team session info if a team session exists where current member is
     an appropriate attendee type (mentor or mentee) of that session.

    Otherwise, returns a JsonResponse, with the invalid request payload

    request_data is either request.GET or request.POST

    Required request Parameters:
        sessionId: (string)
        isMentee: (JSON.stringify() of a boolean)

    return format: dictionary
    {
        "team_session": (TeamMentorshipSession)
        "to_members": (QuerySet[StartupMembers])
        "is_menttee": (bool)
    }

    to_members: Set of possible people to give feedback to. if mentee, then set of attending mentors.
    if mentor, then set of attending mentees.
    """
    payload = {}
    try:
        session_id = int(request_data.get('sessionId'))
        is_mentee = json.loads(request_data.get('isMentee'))
        if not isinstance(is_mentee, bool):
            raise TypeError
    except TypeError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid request Param Format."
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)
    try:
        team_session = get_team_session__help(current_member, session_id, is_mentee)
    except TeamMentorshipSession.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid Session ID."
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)

    mentors = team_session.get_mentors()

    if is_mentee:
        to_members = StartupMember.objects.filter(mentor_profile__in=mentors)
    else:
        to_members = StartupMember.objects.filter(user__in=team_session.members_attending.all())

    app_logger.debug(to_members)

    return {
        'team_session': team_session,
        'to_members': to_members,
        'is_mentee': is_mentee,
    }


def get_feedbacks_dict(feedbacks: Iterable[MentorshipSessionFeedback]):
    """ Returns dictionary of feedbacks keyed by to-member id.
    """
    return {
        feedback.to_member.id: {
            'rating': feedback.rating,
            'feedbackText': feedback.feedback,
            'feedbackId': feedback.id,
        } for feedback in feedbacks
    }


@login_required
def get_team_session_feedback(request):
    """
    Required Parameters:
        sessionId (string)
        isMentee (JSON.stringify() of a boolean)
    """
    info = get_team_session_info(request.current_member, request.GET)
    if isinstance(info, JsonResponse):
        return info  # Return JsonResponse error

    team_session = info['team_session']
    to_members = info['to_members']

    from_member = request.current_member

    to_members_dict = {member.id: member.get_full_name()
                       for member in to_members}

    feedbacks = MentorshipSessionFeedback.objects\
        .filter(session__teammentorshipsession=team_session, from_member=from_member)

    feedbacks_dict = get_feedbacks_dict(feedbacks)

    return JsonResponse({
        'toMembers': json.dumps(to_members_dict),
        'feedbacks': json.dumps(feedbacks_dict),
    })


@login_required
@require_POST
def submit_team_session_feedback(request):
    info = get_team_session_info(request.current_member, request.POST)
    if isinstance(info, JsonResponse):
        return info  # Return JsonResponse error

    team_session = info['team_session']
    to_members = info['to_members']
    is_mentee = info['is_mentee']
    from_member = request.current_member

    payload = {}

    try:
        to_member_id = int(request.POST.get('toMemberId'))
        feedback_id = request.POST.get('feedbackId')  # None means this is a new feedback. Not none means editing current.
        if feedback_id:
            feedback_id = int(feedback_id)
        rating = int(request.POST.get('rating'))
        if not 0 <= rating <= 5:
            raise TypeError
        feedback_text = request.POST.get('feedbackText')
    except TypeError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid Form Format."
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)

    try:
        to_member = StartupMember.objects.get(id=to_member_id)
        if to_member not in to_members:
            raise StartupMember.DoesNotExist
    except StartupMember.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid To-Member ID."
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)

    if feedback_id:
        try:
            feedback = MentorshipSessionFeedback.objects.get(
                id=feedback_id, session_id=team_session.id, from_member=from_member,
                to_member=to_member, is_from_mentor=(not is_mentee))
        except MentorshipSessionFeedback.DoesNotExist:
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "Invalid Feedback ID."
            return JsonResponse(payload, status=INVALID_REQUEST_CODE)
    else:
        feedback = MentorshipSessionFeedback(
            session_id=team_session.id, from_member=from_member,
            to_member=to_member, is_from_mentor=(not is_mentee))

    feedback.rating = rating
    feedback.feedback = feedback_text
    feedback.save()

    feedbacks = MentorshipSessionFeedback.objects.filter(session_id=team_session.id, from_member=from_member)
    feedbacks_dict = get_feedbacks_dict(feedbacks)

    return JsonResponse({
        'feedbacks': json.dumps(feedbacks_dict),
    })


@login_required
@require_POST
def delete_team_session_feedback(request):
    info = get_team_session_info(request.current_member, request.POST)
    if isinstance(info, JsonResponse):
        return info  # Return JsonResponse error

    team_session = info['team_session']
    from_member = request.current_member

    payload = {}

    try:
        feedback_id = int(request.POST.get('feedbackId'))
    except TypeError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid POST Param Format."
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)

    try:
        feedback = MentorshipSessionFeedback.objects.get(
            id=feedback_id, session_id=team_session.id, from_member=from_member)
    except MentorshipSessionFeedback.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = "Invalid Session ID."
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)

    feedback.delete()

    feedbacks = MentorshipSessionFeedback.objects.filter(session_id=team_session.id, from_member=from_member)
    feedbacks_dict = get_feedbacks_dict(feedbacks)

    return JsonResponse({
        'feedbacks': json.dumps(feedbacks_dict),
    })
