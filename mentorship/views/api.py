import json

from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from api.authentication import BaseAPIAuthentication
from api.decorators import authenticate
from api.jresponse import FORBIDDEN_REQUEST
from mentorship.views.mixins import get_mentors


@authenticate(BaseAPIAuthentication)
def discover_mentor_view(request, auth):
    if auth is None or auth[0] is None:
        return FORBIDDEN_REQUEST
    (user, university, profile, auth_token) = auth
    mentor_list, requests_sent, _ = get_mentors(user, university, True, request.is_archived)
    return JsonResponse({'status': 200,
                         'mentor_list': json.dumps(mentor_list),
                         'requests_sent': json.dumps(requests_sent)})


@authenticate(BaseAPIAuthentication)
@csrf_exempt
def mentor_request(request, auth):
    if auth is None:
        return FORBIDDEN_REQUEST
    (user, university, profile, auth_token) = auth
