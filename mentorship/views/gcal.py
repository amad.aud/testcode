from mentorship.models import MentorshipSession, DURATIONS
from app.g_calendar import (
    get_credentials,
    create_event_sendNotifications,
    delete_event_sendNotifications)
from StartupTree.loggers import app_logger
from datetime import timedelta

def make_event_object(session):

    timeZone = session.get_timezone()
    start_datetime = session.datetime
    end_datetime = start_datetime + timedelta(minutes=DURATIONS[(session.duration - 1)][1])

    start_time = start_datetime.strftime('%l:%M %p')

    start_date = start_datetime.strftime('%Y-%m-%d')
    start_dateTime_iso = start_datetime.isoformat()

    end_date = end_datetime.strftime('%Y-%m-%d')
    end_dateTime_iso = end_datetime.isoformat()

    student_name = session.mentorship.student.get_full_name()
    student_email = session.mentorship.student.user.email

    mentor_name = session.mentorship.mentor.profile.get_full_name()
    mentor_gcal_email = session.mentorship.mentor.gcal_email

    summary = "Mentorship w/ " + student_name + " (" + start_time + " " + timeZone + ")"

    description = "Mentorship Meeting between " + mentor_name + " and " + student_name + " (" + student_email + "). "

    object = {
        "summary": summary, # Title of the event.
        "attendees": [
          {
            "displayName": mentor_name,
            "email": mentor_gcal_email, # For sending google event invite
          },
        ],
        "start": { # The (inclusive) start time of the event.
          "timeZone": timeZone, # The time zone in which the time is specified. (Formatted as an IANA Time Zone Database name, e.g. "Europe/Zurich".) For recurring events this field is required and specifies the time zone in which the recurrence is expanded. For single events this field is optional and indicates a custom time zone for the event start/end.
          "dateTime": start_dateTime_iso, # The time, as a combined date-time value (formatted according to RFC3339). A time zone offset is required unless a time zone is explicitly specified in timeZone.
        },
        "source": { # Source from which the event was created. For example, a web page, an email message or any document identifiable by an URL with HTTP or HTTPS scheme. Can only be seen or modified by the creator of the event.
          "url": 'https://www.startuptree.co/', # URL of the source pointing to a resource. The URL scheme must be HTTP or HTTPS.
          "title": "StartupTree: The Leading Platform for University Entrepreneurship", # Title of the source; for example a title of a web page or an email subject.
        },
        "description": description, # Description of the event. Optional.
        "end": { # The (exclusive) end time of the event. For a recurring event, this is the end time of the first instance.
          "timeZone": timeZone, # The time zone in which the time is specified. (Formatted as an IANA Time Zone Database name, e.g. "Europe/Zurich".) For recurring events this field is required and specifies the time zone in which the recurrence is expanded. For single events this field is optional and indicates a custom time zone for the event start/end.
          "dateTime": end_dateTime_iso, # The time, as a combined date-time value (formatted according to RFC3339). A time zone offset is required unless a time zone is explicitly specified in timeZone.
        },
      }

    return object


def make_gcal_event(session):
    ''' Creates a new event object representing the mentorship session that
    was just created (session), and adds the event to the mentor's google
    calendar. The resulting gcal event id then gets stored as a field in the
    session object.
    '''

    event = make_event_object(session)

    created_event = create_event_sendNotifications(event)

    session.event_id = created_event['id']
    session.save()


def remove_gcal_event(session):
    ''' Deletes the gcal event associated with this cancelled session.
    The session's gcal event id field is then set to null.
    requires: session.event_id is not None
    '''

    event_id = session.event_id

    delete_event_sendNotifications(event_id)
    session.event_id = None
    session.save()
