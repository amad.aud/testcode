from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from forms.models import (
    Form,
)
from forms.serializer.forms import custom_member_dict

from branch.models import StartupMember
from emailuser.models import UserEmail
from mentorship.models import Mentor
from mentorship.st_cronofy import (
    get_element_token_general,
    handle_unlink_all_calendars,
    is_cronofy_linked,
    mentor_cronofy_authorization_finished,
)
from mentorship.types.mentor_signup import (
    CronofySyncInfo,
    MentorContactInfo,
    MentorContactInfoFields,
    MentorSignupStep,
    POSTResponse,
    TextFieldSizeLimits,
)
from StartupTree.loggers import app_logger
from StartupTree.settings import CRONOFY_CLIENT_ID
from trunk.models import University, UniversityStyle
from trunk.utils import get_primary_color

import json
import pytz
from typing import List, Optional, Tuple


class MentorSignup(View):
    def get(self, request):
        uni: University = request.university
        style: UniversityStyle = uni.style
        mentor = get_mentor(request)
        member: StartupMember = request.current_member
        user: UserEmail = member.user
        signup_steps = []
        if style.signup_step1:
            signup_steps.append("Step 1")
        if style.signup_step2 or style.signup_tags:
            signup_steps.append("Step 2")
        if style.signup_step3:
            signup_steps.append("Step 3")
        custom = []
        custom = custom_member_dict(None, uni.id, Form.MENTOR, None, is_signup=True)

        if custom:
            signup_steps.append("Step 4")


        init_step = json.dumps(get_init_step(request))
        signup_steps.append("ContactInfo")
        signup_steps.append("CalendarSync")
        print("signup steps ", signup_steps)

        #  3 = ContactInfo
        # 4 = Calender Sync
        primary_color = get_primary_color(style)
        init_mentor_contact_info = json.dumps(get_mentor_contact_info(mentor))
        init_cronofy_sync_info = json.dumps(get_cronofy_sync_info(user, request.get_host()))
        text_field_size_limits = json.dumps(get_text_field_size_limits())
        return TemplateResponse(
            request,
            'mentorship/mentor_signup.html',
            {
                'init_step': init_step,
                'all_steps': json.dumps(signup_steps),
                'init_mentor_contact_info': init_mentor_contact_info,
                'init_cronofy_sync_info': init_cronofy_sync_info,
                'text_field_size_limits': text_field_size_limits,
                'primary_color': primary_color,
                'finish_url': reverse('mentorship-setting'),
            }
        )

    def post(self, request):
        """
        Handles all posts requests from Mentor Signup Form.

        Each post request should have a step param.

        Side Effects:

            step == MentorSignupStep.CONTACT_INFO:
                If inputs are valid, updates the mentor's contact info.
                'responseData' is the updated MentorContactInfo. (or None if errors).

            step == MentorSignupStep.CALENDAR_SYNC:
                See handle_calendar_sync_action() specs.
                'responseData' is the updated CronofySyncInfo. (or None if errors).

        Each post request returns a tuple of 'responseData' and 'errors' (list of strings).
        """
        mentor = get_mentor(request)
        step = request.POST.get('step')

        data = None

        if step == MentorSignupStep.CONTACT_INFO:
            mentor_contact_info, error_list = get_and_validate_mentor_contact_info(request)
            if not error_list:
                update_mentor_contact_info(mentor, mentor_contact_info)
            data = mentor_contact_info

        elif step == MentorSignupStep.CALENDAR_SYNC:
            data, error_list = handle_calendar_sync_action(request, mentor)

        else:
            error = f"Invalid 'step' value: '{step}'."
            error_list = [error]
            app_logger.error(error)

        response: POSTResponse = {
            'errors': error_list,
            'responseData': data,
        }

        return JsonResponse(response)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


def get_mentor(request) -> Mentor:
    """
    Get mentor from request.
    """
    uni: University = request.university
    member: StartupMember = request.current_member
    return Mentor.objects.get(profile=member, platform=uni)


def get_init_step(request) -> Optional[str]:
    """ Given a request object (namely, it's get params), return the step identifier
    (the string version of the step enum in /scripts/javascript/react/components/mentorship/user/signup/types.ts).
    If a valid step is not in get params, then return None.
    """
    step = request.GET.get('step')

    if step in MentorSignupStep.STEPS:
        return step
    else:
        if step is not None:
            app_logger.error(f'INVALID STEP VALUE: {step}')
        return None


def get_mentor_contact_info(mentor: Mentor) -> MentorContactInfo:
    """
    Given a mentor, return the requested MentorDBContactInfo needed for frontend rendering.
    """
    return {
        'mentorContactInfoFields': {
            'email': mentor.email if mentor.email else mentor.profile.user.email,
            'phone': mentor.phone,
            'video': mentor.skype,
            'timezone': mentor.timezone,
        },
        'allTimezones': pytz.common_timezones,
    }


def get_and_validate_mentor_contact_info(request) -> Tuple[Optional[MentorContactInfoFields], List[str]]:
    """
    Given a request object (with POST params), extract the data to build a MentorContactInfoFields object
    and make make sure it is valid.

    Also return a list of errors.
    If there are no errors:
        Return the MentorContactInfoFields object and an empty list for errors.
    If there are errors:
        Return None (instead of MentorContactInfoFields object) and a list of string errors.
    """
    email = request.POST.get('mentor-email')
    phone = request.POST.get('mentor-phone')
    video = request.POST.get('mentor-video')
    mentor_timezone = request.POST.get('mentor-timezone')

    error_list = []

    if mentor_timezone not in pytz.common_timezones:
        error_list.append(f'Inputted timezone ({mentor_timezone}) is not a valid timezone.')

    if error_list:
        return None, error_list
    else:
        mentor_contact_info = {
            'email': email,
            'phone': phone,
            'video': video,
            'timezone': mentor_timezone,
        }

        return mentor_contact_info, []


def update_mentor_contact_info(mentor: Mentor, mentor_contact_info: MentorContactInfoFields):
    """
    Given a mentor object and a mentor_contact_info (originating from new user input),
    update the mentor object's fields accordingly.
    """
    is_changed = False

    # Map mentor_contact_info key to Mentor attr.
    # Eg: mentor_contact_info['video'] corresponds to Mentor.skype
    source_key_to_model_attr = dict(
        [('email', 'email'), ('phone', 'phone'), ('video', 'skype'), ('timezone', 'timezone')])

    for key, new_value in mentor_contact_info.items():
        # for each entry in mentor_contact_info, update the corresponding field in mentor (if different).
        model_attr = source_key_to_model_attr[key]
        old_value = getattr(mentor, model_attr)  # EG: old_value = mentor.skype

        if new_value != old_value:
            setattr(mentor, model_attr, new_value)  # EG: mentor.skype = new_value
            is_changed = True

    if is_changed:
        mentor.save()


def get_cronofy_sync_info(user: UserEmail, request__get_host: str) -> CronofySyncInfo:
    """
    Returns a CronofySyncInfo object, given the user.
    request__get_host is the result of request.get_host().
    """
    if is_cronofy_linked(user):
        element_token = get_element_token_general(user, request__get_host)
    else:
        element_token = None

    return {'clientId': CRONOFY_CLIENT_ID,
            'elementToken': element_token, }


def get_text_field_size_limits() -> TextFieldSizeLimits:
    """
    Programmatically evaluated the database size limits for the fields and return them.
    """
    return {'email': Mentor._meta.get_field('email').max_length,
            'phone': Mentor._meta.get_field('phone').max_length,
            'video': Mentor._meta.get_field('skype').max_length, }


def handle_calendar_sync_action(request, mentor) -> Tuple[Optional[CronofySyncInfo], List[str]]:
    """
    Handles post requests in the Calendar Sync page of Mentor Signup Page.

    Side Effects:

        action == 'FirstProfileSynced':
            Performs a cronofy API call to obtain authorization credentials.
            Updates the user's db record to hold these credentials.

            Other side effects, related to mentorship scheduling:
                asynchronously refreshes the db cronofy cache. See mentorship.cronofy_cache for specs.
                Makes cronofy API calls to set up the cronofy availability rules.

        action == 'LastProfileRevoked':
            Opts the mentor out of cronofy sync.
            See mentorship.st_cronofy.handle_unlink_all_calendars specs.
                - Cronofy API call to delete availability rules.
                - Cronofy API call to revoke the user's authorization.
                - asynchronously refreshes (clears) their db cronofy cache. See mentorship.cronofy_cache for specs.

    Returns:
        A tuple of optional CronofySyncInfo and list of errors.
            If there are no errors, the CronofySyncInfo is defined.
            If there are errors,  the CronofySyncInfo may be None.
    """
    action = request.POST.get('action')
    user: UserEmail = mentor.profile.user

    def return_success():
        user.refresh_from_db()
        data = get_cronofy_sync_info(user, request.get_host())
        return data, []  # no errors

    if action == 'FirstProfileSynced':
        uni: University = request.university
        redirect_uri = request.POST.get('redirect_uri')
        cronofy_access_token = request.POST.get('code')
        mentor_cronofy_authorization_finished(user, uni, cronofy_access_token, redirect_uri)
        return return_success()

    elif action == 'LastProfileRevoked':
        if handle_unlink_all_calendars(user):
            return return_success()
        else:
            return None, ['Error while attempting to opt out of calendar sync.']
