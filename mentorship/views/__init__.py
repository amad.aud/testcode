# django imports
import datetime as dt
import json
import traceback
import uuid
from datetime import datetime
from datetime import timedelta
import dateutil

from bs4 import BeautifulSoup

import pytz
from boto.s3.connection import S3Connection
# startuptree imports
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import ObjectDoesNotExist, Q
from django.http import JsonResponse, HttpResponsePermanentRedirect, HttpResponse, \
    HttpResponseGone, HttpResponseForbidden
from django.shortcuts import Http404, HttpResponseRedirect, get_object_or_404
from django.template.loader import get_template
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.http import require_POST
from django.views.generic import RedirectView
from ics import Calendar
from ics import Event as icsEvent
from pycronofy.exceptions import PyCronofyRequestError

from StartupTree.loggers import app_logger
from StartupTree.strings import *
from StartupTree.utils import time_minute_diff, QuerySetType
from app.modelforms import TimeForm
from branch.modelforms import TextareaForm, DateTimeForm
# branch imports
from branch.models import StartupMember, Startup
from branch.serializer.member import get_experience_list, get_degree_list, sm_to_dict
from branch.serializer.startup import startup_to_dict_basic
from branch.views.abstract import AbstractDiscoverPeople
from emailuser.conf import PROTOCOL
from emailuser.models import UserEmail
from forms.modelforms import AnserFileForm
# forms imports
from forms.models import Form, Answer
from forms.parser import CustomAnswersParser
from forms.serializer.forms import custom_member_dict, custom_meeting_dict
from inbox.models import InboxConversation
# inbox imports
from inbox.views import InboxView
# mentor imports
from mentorship.cronofy_cache import refresh_cronofy_cache__user
from mentorship.models import (
    PHONE, SKYPE, IN_PERSON, PREFERRED_CONTACT,
    Mentorship,
    MentorshipActivity,
    MentorshipSession,
    TeamMentorshipActivity,
    TeamMentorship,
    TeamMentorshipSession,
    Note,
    TeamNote,
    Goal,
    TeamGoal,
    Mentor,
    MentorSchedule,
    DURATIONS,
    MentorshipFile,
    NoteAttachment,
    MentorshipRequest,
    MentorshipSessionSet,
    MentorScheduleBlackOut,
    IN_PERSON,
    MentorCronofySchedule,
    PREFERRED_CONTACT,
    GoalSet,
    NO_SHOW,
    MentorshipDefaults
)
from mentorship.strings import *
from mentorship.tasks import (
    session_change_cronofy_mentor,
    session_change_noti_mentee,
    session_change_noti_mentor,
    session_request_noti_uni,
    send_goal_completed_noti,
    render_custom_email_variables,
)
from mentorship.utils import localize, is_user_authorized_team_mentorship, get_formatted_time
from trunk.perms import is_admin_authorized
from mentorship.st_cronofy import is_cronofy_linked, get_cronofy_calendar_id, revoke_cronofy_auth, \
    authenticate_cronofy, delete_availability_rules, cronofy_delete_from_calendar, \
    get_cronofy_user_calendars, generate_cronofy_url, create_or_update_availabilty_rules, \
    get_cronofy_account_info, generate_element_token, cronofy_publish_to_calendar, slot_time_translator, \
    get_element_token_general, mentor_cronofy_authorization_finished, handle_unlink_all_calendars
from mentorship.views.gcal import make_gcal_event, remove_gcal_event
from mentorship.views.mixins import get_mentors, send_mentor_request, \
    get_possible_calendar_schedules, get_possible_list_schedules, \
    set_mentor_settings, get_cronofy_possible_calendar_schedules
from mentorship.constants import NOT_FOUND, ONE_YEAR_AGO
from mentorship.tasks import session_change_cronofy_admins__async
from trunk.decorators import only_university
# trunk imports
from trunk.models import (
    University,
    UniversityStyle,
    UniversityStaff,
    PointOfContact,
    AccessLevel,
    TeamLandingPage
)
# venture imports
from venture.models import MemberToAdmin
from mentorship.views.mixins.scheduling import prepare_session_scheduling, platform_admin_schedule_session


def get_mfile_authorized_users(mfile):
    """ Semantics: Returns a sequence of StartupMembers (mentees) and Mentors
    and Admins (UserEmails)who should have access to download or delete mfile.

    If the file corresponds to a single mentorship,
    then the student and mentor are authorized. If the file corresponds to a
    team mentorship, then the student-owners of the team (as specified by MemberToAdmin)
    and the team mentors of the team (as specified by TeamMentorship) are authorized.

    Format: Returns a triple of sequences of StartupMember and Mentor and UserEmail objects,
    with the StartupMember objects representing the authorized mentees, the Mentor objects
    representing the authorized mentors, and the UserEmail objects representing
    thee authorized platform admins.

    The sequences of StartMember and Mentor objects may be querysets or singleton sets.
    The UserEmail objects are always querysets.
    """

    if mfile.team:
        mentors = Mentor.objects.filter(mentorship__teammentorship__team=mfile.team)
        mentees = StartupMember.objects.filter(membertoadmin__startup=mfile.team)
        universities = mfile.team.university.all()
        admins = UserEmail.objects.filter(universitystaff__university__in=universities,
            universitystaff__access_level=UniversityStaff.SUPER)
    else:
        mentors = [mfile.single_mentorship.mentor]
        mentees = [mfile.single_mentorship.student]
        admins = UserEmail.objects.filter(universitystaff__university=mfile.single_mentorship.mentor.platform,
            universitystaff__access_level=UniversityStaff.SUPER)

    return mentees, mentors, admins


def is_mfile_authorized_user(mfile, member):
    """ Returns whether member is authorized to download or delete the MentorshipFile.

    Super-level admins of the platform are authorized.

    If the file corresponds to a single mentorship,
    then the student and mentor are authorized. If the file corresponds to a
    team mentorship, then the student-owners of the team (as specified by MemberToAdmin)
    and the team mentors of the team (as specified by TeamMentorship) are authorized.
    """

    authorized_mentees, authorized_mentors, authorized_admins = get_mfile_authorized_users(mfile)

    # First check if user is a mentee with access
    if member in authorized_mentees:
        return True

    # Then check if user is mentor with access
    member_mentors = Mentor.objects.filter(profile=member)
    for member_mentor in member_mentors:
        if member_mentor in authorized_mentors:
            return True

    # Finally check if user is super-level admin of university
    if member.user in authorized_admins:
        return True

    return False


class MentorshipFileDownloadView(RedirectView):
    """MentorshipFile Download view:
    acknowledgement:
    http://www.gyford.com/phil/writing/2012/09/26/django-s3-temporary.php

    Note: This class is almost identical to NoteAttachedFileView.
    If modifying this class, consider making analogous changes to the other.
    """
    permanent = False

    def get_redirect_url(self, **kwargs):
        s3 = S3Connection(settings.AWS_ACCESS_KEY_ID,
                          settings.AWS_SECRET_ACCESS_KEY,
                          is_secure=True)
        bucket = s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
        key = bucket.get_key(str(kwargs['filepath']))
        return key.generate_url(expires_in=300)

    def get(self, request, *args, **kwargs):
        if not request.is_valid_platform:
            return HttpResponseRedirect("/")
        mfile = get_object_or_404(MentorshipFile, pk=kwargs['pk'])
        u = request.user
        if u.is_authenticated and not request.is_archived and \
                is_mfile_authorized_user(mfile, request.current_member):
            if mfile.file:
                file_path = str(mfile.file)
                if settings.DEBUG and not settings.IS_STAGING:
                    url = settings.MEDIA_DIRECTORY + file_path
                else:
                    url = self.get_redirect_url(filepath=file_path)
                # The below is taken straight from RedirectView.
                if url:
                    if self.permanent:
                        return HttpResponsePermanentRedirect(url)
                    else:
                        return HttpResponseRedirect(url)
                else:
                    # TODO run a test that reaches this line of execution
                    app_logger.warning('Gone: %s', self.request.path,
                                       extra={
                                           'status_code': 410,
                                           'request': self.request
                                       })
                    return HttpResponseGone()
            else:
                raise Http404('Page not found')
        else:
            raise Http404('Page not found')


@login_required
def display_multiple_mentors(request):
    mentor_id = request.POST.get('mentor_id')

    mentor = Mentor.objects.get(id=int(mentor_id))
    full_name = mentor.profile.full_name
    image_url = mentor.profile.image_url

    if image_url is None:
        image_url = settings.STATIC_URL + "img/icon_person.jpg"

    url = mentor.profile.get_url()
    email = mentor.profile.user.email

    return JsonResponse({
        "status": 200,
        "name": full_name,
        "email": email,
        "image_url": image_url,
        "url": url,
    })


def get_meeting_type_string(code):
    """ Given a meeting type code, return the corresponding string.
    Uses mentorship.models.PREFERRED_CONTACT
    """
    dictionary = dict(PREFERRED_CONTACT)
    return dictionary[code]


def get_meta_data(date_info, slot_info):
    """
    Inputs:

    date_info = [
        [6, 'Sat'],
        'Nov 30',
        [
            slot1_info,
            slot2_info,
            ...
        ],
        2019,
        4, # Meeting type
        'Gates 310' # Location
    ]

    slot_info = [
        '03:00 PM',
        '03:30 PM',
        True, # Whether slot is free (or taken by a mentee)
        2, # Duration code
        'Gates 310' # Location
    ]

    Returns a dictionary containing requisite meta date.

    At the moment, this is:

    slot_data = {
        "date": "Dec 19",
        "year": 2019
        "start_time": "03:00 PM"
        "end_time": "03:30 PM"
        "duration_code": 2, # (eg 2 represents 30 minutes) See mentorship.models.DURATIONS for codes
        "meeting_type": 4, # (eg 4 represents "In Person") See mentorship.models.PREFERRED_CONTACT for codes
        "location": "Gates 310"
    }

    A note about location:
    By convention, location is relevant only if meeting type is in person.
    Otherwise, location may be none, or the non-in-person meeting type (eg "phone" or "video call").
    """
    date = date_info[1]
    year = date_info[3]
    start_time = slot_info[0]
    end_time = slot_info[1]
    duration_code = slot_info[3]

    meeting_type = date_info[4]

    if meeting_type == IN_PERSON:
        location = slot_info[4]
    else:
        location = get_meeting_type_string(meeting_type)

    return {
        "date": date,
        "year": year,
        "start_time": start_time,
        "end_time": end_time,
        "duration_code": duration_code,
        "meeting_type": meeting_type,
        "location": location
    }


def get_datetime_from_slot_data(slot_data):
    """
    Given a slot data dictionary like this:

    slot_data = {
        "date": "Dec 19",
        "year": 2019
        "start_time": "03:00 PM"
        ...
    }

    Return a (naive) datetime object for start time (for purposes of sorting)
    """
    start_datetime_str = slot_data["date"] + " " + str(slot_data["year"]) + " " + slot_data["start_time"] # eg 'Dec 19 2019 03:00 PM'
    start_datetimee = datetime.strptime(start_datetime_str, "%b %d %Y %I:%M%p")
    return start_datetimee


def single_mentorship_schedule(request, mentor_id, upcoming_week=12):
    """ Returns a list of available upcomming slots for the mentor.

    An entry in the list is a dictionary of data.

    slot_data = {
        "date": "Dec 19",
        "year": 2019
        "start_time": "03:00 PM"
        "end_time": "03:30 PM"
        "duration_code": 2, # (eg 2 represents 30 minutes) See mentorship.models.DURATIONS for codes
        "meeting_type": 4, # (eg 4 represents "In Person") See mentorship.models.PREFERRED_CONTACT for codes
        "location": "Gates 310"
    }

    A note about location:
    By convention, location is relevant only if meeting type is in person.
    Otherwise, location may be none, or the non-in-person meeting type (eg "phone" or "video call").
    """

    mentor = Mentor.objects.get(id=mentor_id)

    uni = request.university
    show_this_week = (uni.style.mentor_schedule_start == UniversityStyle.THIS_WEEK)

    if not is_cronofy_linked(mentor.profile.user):
        slots_raw = get_possible_list_schedules(mentor, show_this_week=show_this_week, upcoming_week=upcoming_week, request=request)
    else:
        slots_raw = slot_time_translator(request, mentor.profile.user, is_mentor=True)


    """
    slots_raw = [
        date1_info,
        date2_info,
        ...
    ]

    date_info = [
        [6, 'Sat'],
        'Nov 30',
        [
            slot1_info,
            slot2_info,
            ...
        ],
        2019,
        4, # Meeting type
        'Gates 310' # Location
    ]

    slot_info = [
        '03:00 PM',
        '03:30 PM',
        True, # Whether slot is free (or taken by a mentee)
        2, # Duration code
        'Gates 310' # Location
    ]
    """

    slot_data_list = []

    for date_info in slots_raw:
        for slot_info in date_info[2]:
            if slot_info[2]: # only append the slot data if the slot is free.
                slot_data_list.append(get_meta_data(date_info, slot_info))

    slot_data_list.sort(key=get_datetime_from_slot_data)

    return slot_data_list


def multiple_mentor_schedules__arr(request, mentor_ids, mentee_ids=None):
    """ Returns an array, arr, of available slots that are common between all mentors
    in mentor_ids and all (cronofy-synced) mentees in mentee_ids

    arr = [
        {
            'start': start_time,  #%I:%M%p
            'end': end_time,  #%I:%M%p
            'duration': duration,
            'date': start_datetime.date().isoformat(),  # YYYY-MM-DD
            'start_datetime': start_datetime.isoformat(),  # YYYY-MM-DDTHH:MM:SS
            'timeslot-str': '{0} {1} {2} {3}'.format(date, year, start_time, end_time)  # Nov 28 2019 02:30PM 03:00PM
        }
    ]
    The returned array is sorted by date/time.
    """
    uni = request.university
    mentors_arr = Mentor.objects.select_related('profile').filter(profile__url_name__in=mentor_ids, platform=uni)
    show_this_week = (uni.style.mentor_schedule_start == UniversityStyle.THIS_WEEK)

    mentees_arr: QuerySetType[StartupMember] = StartupMember.objects.none()
    if mentee_ids:
        mentees_arr = StartupMember.objects.filter(url_name__in=mentee_ids)

    """
    people_w_sched = [
        person1_avail,
        person2_avail,
        ...
    ]

    person_avail = [
        date1_info,
        date2_info,
        ...
    ]

    date_info = [
        [6, 'Sat'],
        'Nov 30',
        [
            slot1_info,
            slot2_info,
            ...
        ],
        2019,
        4, # Meeting type
        'Gates 310' # Location
    ]

    slot_info = [
        '03:00 PM',
        '03:30 PM',
        True, # Whether slot is free (or taken by a mentee)
        2, # Duration code
        'Gates 310' # Location
    ]
    """

    # TODO: find better way to find overlapping schedules between mentors' schedules
    mentors_w_sched = []
    for mentor in mentors_arr:
        mentor_user = mentor.profile.user
        if not is_cronofy_linked(mentor_user):
            sched = get_possible_list_schedules(mentor, show_this_week=show_this_week, upcoming_week=5, request=request)
        else:
            sched = slot_time_translator(request, mentor_user, is_mentor=True)
        mentors_w_sched.append(sched)

    mentees_w_sched = []
    for mentee in mentees_arr:
        mentee_user = mentee.user
        if is_cronofy_linked(mentee_user):
            sched = slot_time_translator(request, mentee_user, is_mentor=False)
            mentees_w_sched.append(sched)

    people_w_sched = mentors_w_sched + mentees_w_sched
    people_count = len(people_w_sched)

    """
    
    all_date_objs = {
        comp_str1: {
            'cnt': 5,  # Denotes that there are 5 people who are available during the slot represented by time_obj
            'obj': time_obj
        }
    }
    
    time_obj = {
        'start': '03:00 PM',
        'end': '03:30 PM',
        'duration': 2, # Duration code
        'date': start_datetime.date().isoformat(),  # YYYY-MM-DD
        'start_datetime': start_datetime.isoformat(),  # YYYY-MM-DDTHH:MM:SS
        'timeslot-str': 'Nov 28 2019 02:30PM 03:00PM',
    }
    """
    all_dates_objs = {}
    for person_avail in people_w_sched:
        # `person_avail`: see person_avail spec above
        for date_info in person_avail:
            # `date_info`: see date_info spec above
            date = date_info[1]
            for slot_info in date_info[2]:
                # `slot_info`: see slot_info spec above
                if slot_info[2]:
                    start_time = slot_info[0]
                    end_time = slot_info[1]
                    year = date_info[3]
                    duration = slot_info[3]
                    comp_str = str(duration) + date + str(year) + start_time + end_time
                    if comp_str in all_dates_objs:
                        all_dates_objs[comp_str]['cnt'] += 1
                    else:
                        start_datetime = dt.datetime.strptime("{0} {1} {2}".format(year, date, start_time), '%Y %b %d %I:%M%p')
                        time_obj = {
                            'start': start_time,
                            'end': end_time,
                            'duration': duration,
                            'date': start_datetime.date().isoformat(),  # YYYY-MM-DD
                            'start_datetime': start_datetime.isoformat(),  # YYYY-MM-DDTHH:MM:SS
                            'timeslot-str': '{0} {1} {2} {3}'.format(date, year, start_time, end_time)  # Nov 28 2019 02:30PM 03:00PM
                        }
                        all_dates_objs[comp_str] = {
                            'cnt': 1,
                            'obj': time_obj
                        }
    result = []
    for key in all_dates_objs:
        all_dates_obj = all_dates_objs[key]
        if all_dates_obj['cnt'] == people_count:  # Only if all people are available, dues the slot get appended to result
            result.append(all_dates_obj['obj'])
    result.sort(key=lambda all_dates_obj: all_dates_obj['start_datetime'])

    return result


@login_required
def multiple_mentor_schedules(request):
    mentor_ids = json.loads(request.POST.get('selected_mentors'))
    mentee_ids = json.loads(request.POST.get('selected_mentees'))
    user_timezone = request.session['django_timezone']
    result = multiple_mentor_schedules__arr(request, mentor_ids, mentee_ids=mentee_ids)

    # We have the common availablities as strings, now to display them

    return JsonResponse({'status': 200, 'sched_days': result, 'timezone': str(user_timezone) })


def check_mentors_schedules(request):
    mentor_ids = json.loads(request.POST.get('selected_mentors'))
    mentors_arr = [Mentor.objects.get(id=mentor_id) for mentor_id in mentor_ids]

    _, all_mentors_with_teams = get_team_mentor_info(request)
    teams = []
    for mentor in all_mentors_with_teams:
        if str(mentor['mentor_id']) in mentor_ids:
            teams.append(mentor['ventures'])

    inter = set(teams[0]).intersection(*teams)

    teams_info = []
    for item in inter:
        teams_info.append({'name': item.name, 'url_id': item.url_name})

    # Check for common team
    return JsonResponse({'intersection': teams_info, 'status': 200})


def get_team_mentor_info(request, team_filter=False, date_filter=False, complete_filter=False):
    # TEAMS
    user_id = request.user.id
    admin_pages = MemberToAdmin.objects.filter(member__user_id=user_id)
    teams = [admin_page.startup for admin_page in admin_pages if admin_page.startup.team_mentorship_participant]

    if team_filter:
        teams_to_filter = request.POST.getlist('team')
        teams_to_filter = [int(team) for team in teams_to_filter]
        teams = [team for team in teams if team.id in teams_to_filter]

    # MENTORS
    team_ments_tmp = []
    my_team_mentors = []
    my_team_mentorships = TeamMentorship.objects.filter(team__in=teams, is_active=True)

    # Cylce through the mentorships
        # Get the mentor for each of these mentorships, placing them in an array w/o duplicates
        # Filter for team mentorships w/ the users teams and this mentor
        #
    for mentorship in my_team_mentorships:
        if mentorship.mentor not in team_ments_tmp:
            team_ments_tmp.append(mentorship.mentor)

    for index, mentor in enumerate(team_ments_tmp):
        mentorships = TeamMentorship.objects.filter(team__in=teams, is_active=True, mentor=mentor)
        ventures = [mentorship.team for mentorship in mentorships]

        goals = TeamGoal.objects.filter(mentorship__in=mentorships).order_by('due_date', '-id')

        if date_filter:
            date_from_str = request.POST.get('date_from')
            date_to_str = request.POST.get('date_to')

            date_from = dt.datetime.strptime(date_from_str, '%m/%d/%Y')
            date_to = dt.datetime.strptime(date_to_str, '%m/%d/%Y')

            goals = goals.filter(due_date__gte=date_from, due_date__lte=date_to)

        if complete_filter:
            completeness = request.POST.get('completeness', "none")

            if completeness == "complete":
                goals_completed_recently = goals.exclude(completed=False).filter(completed_on__gte=timezone.now() - timedelta(days=7)).order_by('-completed_on')

                goals_completed_recently_arr = [goal for goal in goals_completed_recently]
                goals_arr = []
            elif completeness == "incomplete":
                goals_completed_recently_arr = []
                goals_arr = [goal for goal in goals]

        else:
            goals_completed_recently = goals.exclude(completed=False).filter(completed_on__gte=timezone.now() - timedelta(days=7)).order_by('-completed_on')[:5]

            goals_completed_recently_arr = [goal for goal in goals_completed_recently]
            goals_arr = [goal for goal in goals]

        can_schedule = mentor.has_schedule() or mentor.has_onetimes()
        team_ments_tmp[index] = {
            'teams': ventures,
            'mentor': mentor,
            'can_schedule': can_schedule,
            'goals': goals_arr,
            'goals_completed': goals_completed_recently_arr
        }

    for item in team_ments_tmp:
        my_team_mentors.append({
            'mentor_id': item['mentor'].id,
            'ventures': item['teams'],
            'goals': item['goals'],
            'goals_completed': item['goals_completed'],
            'can_schedule': item['can_schedule'],
            'profile': item['mentor'].profile,
            'preferred_contact': item['mentor'].default_meeting_type,
            'get_preferred_contact': item['mentor'].get_preferred_contact(),
        })

    return teams, my_team_mentors


def get_my_mentorships(request):
    return MentorshipRequest.objects \
            .select_related('requested_mentorship',
                            'requested_mentorship__student',
                            'startup') \
            .filter(is_admin_accepted=True,
                    requested_mentorship__mentor__profile__user_id=request.user.id,
                    requested_mentorship__is_active=True,
                    requested_mentorship__student__is_archived=False) \
            .order_by('requested_mentorship__student__first_name')


def get_my_mentorships_mentee(request):
    return MentorshipRequest.objects \
        .select_related('requested_mentorship',
                        'requested_mentorship__mentor__profile',
                        'startup') \
        .filter(is_admin_accepted=True,
                requested_mentorship__student__user_id=request.user.id,
                requested_mentorship__is_active=True,
                requested_mentorship__mentor__profile__is_archived=False) \
        .distinct('requested_mentorship') \
        .order_by('requested_mentorship',
                  'requested_mentorship__mentor__profile__first_name')


def get_raw_upcoming(request):
    return MentorshipSession.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(mentorship__mentor__profile__user_id=request.user.id,
                is_admin_accepted=True,
                is_mentor_accepted=True,
                is_cancelled=False,
                teammentorshipsession__isnull=True,
                datetime__gte=timezone.now()) \
        .order_by('datetime')


def get_raw_upcoming_mentee(request):
    return MentorshipSession.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(mentorship__student__user_id=request.user.id,
                is_admin_accepted=True,
                is_mentor_accepted=True,
                is_cancelled=False,
                teammentorshipsession__isnull=True,
                datetime__gte=timezone.now()) \
        .order_by('datetime')


def get_raw_upcoming_json(mentorship):
    return MentorshipSession.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(mentorship=mentorship,
                is_cancelled=False,
                teammentorshipsession__isnull=True,
                datetime__gte=timezone.now()) \
        .order_by('datetime')


def get_raw_pending(request):
    pending_sets = get_raw_pending_sets(request).values_list('sessions', flat=True)
    return MentorshipSession.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(Q(is_admin_accepted__isnull=True) |
                Q(is_mentor_accepted__isnull=True),
                mentorship__mentor__profile__user_id=request.user.id,
                is_cancelled=False,
                teammentorshipsession__isnull=True,
                datetime__gte=timezone.now()) \
        .exclude(id__in=pending_sets) \
        .order_by('datetime')


def get_raw_pending_mentee(request):
    pending_sets = get_raw_pending_sets_mentee(request).values_list('sessions', flat=True)
    return MentorshipSession.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(Q(is_mentor_accepted__isnull=True) |
                Q(is_admin_accepted__isnull=True),
                mentorship__student__user_id=request.user.id,
                teammentorshipsession__isnull=True,
                is_cancelled=False,
                datetime__gte=timezone.now()) \
        .exclude(id__in=pending_sets) \
        .order_by('datetime')


def get_raw_pending_sets(request):
    return MentorshipSessionSet.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(mentorship__mentor__profile__user_id=request.user.id,
                decided__isnull=True)


def get_raw_pending_sets_mentee(request):
    return MentorshipSessionSet.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(mentorship__student__user_id=request.user.id,
                decided__isnull=True)


def get_raw_notable(mentorship):
    return MentorshipSession.objects \
        .select_related('mentorship', 'mentorship__mentor', 'mentorship__mentor__profile',
                        'mentorship__mentor__profile__user', 'mentorship__student', 'mentorship__student__user') \
        .filter(mentorship=mentorship,
                is_cancelled=False,
                datetime__lte=timezone.now(),
                teammentorshipsession__isnull=True,
                datetime__gte=ONE_YEAR_AGO) \
        .order_by('-datetime')


@login_required
def mentor_dashboard(request):
    """
    Mentorship main page for mentor
    :param request:
    :return:
    """
    page_name = 'mentor-dashboard'
    template = 'mentorship/mentor-dashboard.html'

    user_id, current_site = request.main_user.id, request.current_site
    uni = request.university
    current_member = request.current_member

    messages_tmp = InboxView().get_inbox_items(current_member, current_site, None,
                                               request.is_university, None, '', False, None, None, True)

    messages = {}
    for msg in messages_tmp:
        msg_type = InboxConversation.get_view_type(msg)
        if msg.latest_inbox is not None and \
                msg.latest_inbox.member_obj is not None and \
                not msg_type == InboxConversation.INBOX_GROUP:
            outgoing = (current_member.id == msg.latest_inbox.member_obj_id)
            for p_email in msg.participants.all():
                if p_email not in messages or outgoing:
                    messages[p_email.id] = {
                        'msg': msg.latest_inbox.message.content, 'out': outgoing}

    mentor_exists = Mentor.objects.filter(profile__user_id=request.user.id, platform__site_id=current_site.id).exists()
    if mentor_exists:
        # Update Mentor's timezone
        mentor = Mentor.objects.select_related('profile').get(
            profile__user_id=user_id, platform__site_id=current_site.id)

        my_mentees = []
        my_mentorships = get_my_mentorships(request)
        raw_upcoming = get_raw_upcoming(request)
        raw_pending = get_raw_pending(request)
        raw_pending_sets = get_raw_pending_sets(request)

        if uni.short_name == 'bu' and 'Expert Coach' in current_member.labels.all().values_list('title', flat=True):
            template = 'mentorship/bu_expert_coach_dashboard.html'

        is_add_note = True if request.GET.get('add_note') == 'true' else False
        notable_session = None
        notable_mentorship_id = None
        if is_add_note:
            notable_session_id = request.GET.get('session_id', None)
            try:
                notable_session = MentorshipSession.objects.get(id=int(notable_session_id),
                                                                mentorship__mentor=mentor,
                                                                is_cancelled=False,
                                                                datetime__lte=timezone.now(),
                                                                datetime__gte=ONE_YEAR_AGO)
                notable_mentorship_id = notable_session.mentorship.id
            except MentorshipSession.DoesNotExist:
                is_add_note = False

        upcoming_meetings = []
        for upcoming in raw_upcoming:
            mentee = upcoming.get_mentee()

            if my_mentorships.filter(requested_mentorship__student=mentee, is_admin_accepted=True).exists():
                venture = my_mentorships.filter(requested_mentorship__student=mentee, is_admin_accepted=True).first().startup
            else:
                venture = ''

            meeting_custom = custom_meeting_dict(upcoming)

            raw = upcoming.get_datetime()
            day = raw.date().strftime('%a')
            date = raw.date().strftime("%B %d %Y")
            month = raw.date().strftime("%B")
            daynumber = raw.date().strftime("%d")
            start_time = raw.time().strftime("%I:%M%p")
            end_time = upcoming.get_endtime().time().strftime("%I:%M%p")
            time_zone = upcoming.get_timezone()
            tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone
            contact_info = upcoming.get_type_and_contact_info()
            type = upcoming.meeting_type
            if type == 1:
                location = upcoming.phone_number
            elif type == 3:
                location = upcoming.video_id
            else:
                location = upcoming.location
            topic = upcoming.meeting_topic
            upcoming_meetings.append({
                'mentee': mentee,
                'venture': venture,
                'tag': tag,
                'start_time': start_time,
                'end_time': end_time,
                'timezone': time_zone,
                'month': month[:3],
                'daynumber': daynumber,
                'id': upcoming.id,
                'date': raw.date().strftime('%m/%d/%Y'),
                'full_date': date,
                'duration': upcoming.duration,
                'contact_info': contact_info,
                'type': type,
                'location': location,
                'custom': meeting_custom,
                'topic': topic
            })

        pending_meetings = []
        for pending in raw_pending:
            mentee = pending.get_mentee()

            if my_mentorships.filter(requested_mentorship__student=mentee, is_admin_accepted=True).exists():
                venture = my_mentorships.filter(requested_mentorship__student=mentee, is_admin_accepted=True).first().startup
            else:
                venture = ''

            meeting_custom = custom_meeting_dict(pending)

            raw = pending.get_datetime()
            day = raw.date().strftime('%a')
            date = raw.date().strftime("%B %d %Y")
            month = raw.date().strftime("%B")
            daynumber = raw.date().strftime("%d")
            start_time = raw.time().strftime("%I:%M")
            end_time = pending.get_endtime().time().strftime("%I:%M%p")
            time_zone = pending.get_timezone()
            tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone
            topic = pending.meeting_topic
            append_pending = False
            if not pending.needs_admin_approval():
                if not pending.is_mentor_accepted:
                    append_pending = True
            else:
                if (pending.is_admin_accepted and not pending.is_mentor_accepted) or \
                        (not pending.is_admin_accepted and pending.is_mentor_accepted):
                    append_pending = True
            if append_pending:
                pending_meetings.append({
                    'mentee': mentee,
                    'venture': venture,
                    'tag': tag,
                    'start_time': start_time,
                    'end_time': end_time,
                    'timezone': time_zone,
                    'month': month[:3],
                    'daynumber': daynumber,
                    'id': pending.id,
                    'date': raw.date().strftime('%m/%d/%Y'),
                    'full_date': date,
                    'duration': pending.duration,
                    'custom': meeting_custom,
                    'topic': topic,
                    'is_admin_pending': pending.is_mentor_accepted and not pending.is_admin_accepted
                })

        pending_meeting_sets = []
        for set in raw_pending_sets:
            if set.is_contains_future_session():
                set_info = []
                mentee = set.mentorship.student

                if my_mentorships.filter(requested_mentorship__student=mentee, is_admin_accepted=True).exists():
                    venture = my_mentorships.filter(requested_mentorship__student=mentee, is_admin_accepted=True).first().startup
                else:
                    venture = ''

                set_custom = custom_meeting_dict(set.sessions.first())
                set_topic = set.sessions.first().meeting_topic

                set_info.append({
                    'id': set.id,
                    'mentee': mentee,
                    'venture': venture,
                    'custom': set_custom,
                    'topic': set_topic
                })

                meeting_times = []
                for session in set.sessions.all():
                    raw = session.get_datetime()
                    day = raw.date().strftime('%a')
                    date = raw.date().strftime("%B %d %Y")
                    month = raw.date().strftime("%B")
                    daynumber = raw.date().strftime("%d")
                    start_time = raw.time().strftime("%I:%M")
                    end_time = session.get_endtime().time().strftime("%I:%M%p")
                    time_zone = session.get_timezone()
                    tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone

                    meeting_times.append({
                        'tag': tag,
                        'start_time': start_time,
                        'end_time': end_time,
                        'timezone': time_zone,
                        'month': month[:3],
                        'daynumber': daynumber,
                        'id': session.id,
                        'date': raw.date().strftime('%m/%d/%Y'),
                        'duration': session.duration
                    })
                set_info.append(meeting_times)
                pending_meeting_sets.append(set_info)

        for mentorship_req in my_mentorships:
            mentorship = mentorship_req.requested_mentorship
            men_req_id = mentorship_req.id
            venture = mentorship_req.startup
            student = mentorship.student
            url_name = student.url_name
            notes = Note.objects.filter(mentorship_id=mentorship.id)
            recent_note = notes.latest('timestamp') if len(notes) > 0 else None
            goal_sets = GoalSet.objects.filter(mentorship_id=mentorship.id)
            recent_set = goal_sets.latest(
                'timestamp') if len(goal_sets) > 0 else None
            mentee_tag = student.bio if not student.bio or len(
                student.bio) < 100 else student.bio[:100] + "..."

            if recent_set is None:
                recent_goals = []
            else:
                recent_goals = Goal.objects.select_related('creator').filter(goal_set=recent_set)

            raw_notable = get_raw_notable(mentorship)

            notable_meetings = []
            for notable in raw_notable:
                raw = notable.get_datetime()
                day = raw.date().strftime('%a')
                date = raw.date().strftime("%B %d %Y")
                start_time = raw.time().strftime("%I:%M")
                end_time = notable.get_endtime().time().strftime("%I:%M%p")
                tag = day + ", " + date + ", " + start_time + "-" + end_time
                is_selected = False
                if is_add_note:
                    is_selected = notable.id == notable_session.id
                notable_meetings.append({
                    'tag': tag,
                    'id': notable.id,
                    'date': raw.date().strftime('%m/%d/%Y'),
                    'duration': notable.duration,
                    'is_selected': is_selected
                })

            if recent_note:
                attachments = NoteAttachment.objects.filter(note=recent_note)
            else:
                attachments = None

            if student.user in messages:
                msg_obj = messages[student.user]
                lead = "You" if msg_obj['out'] else student.first_name
                recent_message = "{0}: {1}".format(lead, msg_obj['msg'])
            else:
                recent_message = None

            mentee_custom = custom_member_dict(
                student, uni.id, Form.MENTORREQUEST, mentor=mentor)

            my_mentees.append({
                'mentorship_id': mentorship.id,
                'men_req_id': men_req_id,
                'mentee_tag': mentee_tag,
                'recent_message': recent_message,
                'student': student,
                'url_name': url_name,
                'venture': venture,
                'recent_note': recent_note,
                'attachments': attachments,
                'recent_goals': recent_goals,
                'recent_goal_set': recent_set,
                'notable_meetings': json.dumps(notable_meetings),
                'custom': mentee_custom
            })

        has_pending_meetings = mentor.approve_meetings_or_admin_override and (pending_meetings != [] or pending_meeting_sets != [])
        mentor = current_member
        mentor_member = Mentor.objects.get(profile=mentor, platform__site_id=current_site.id)
        mentor_name = mentor.first_name + " " + mentor.last_name
        return TemplateResponse(
            request,
            template,
            {
                'page_name': page_name,
                'my_mentees': my_mentees,
                'mentor_name': mentor_name,
                'is_add_note': is_add_note,
                'notable_mentorship_id': notable_mentorship_id,
                'upcoming_meetings': upcoming_meetings,
                'pending_meetings': pending_meetings,
                'pending_meeting_sets': pending_meeting_sets,
                'has_pending_meetings': has_pending_meetings
            }
        )


@login_required
def mentee_dashboard(request, is_team=False):
    """
    Mentorship main page for mentee
    :param request:
    :return:
    """
    page_name = 'mentee-dashboard'

    user_id, current_site = request.main_user.id, request.current_site
    uni = request.university
    current_member = request.current_member

    student_name = None
    my_mentors = []
    mentorship_venture_dict = {}
    if is_team:
        my_teams = Startup.objects.filter(id__in=MemberToAdmin.objects.filter(
            member=current_member,
            startup__team_mentorship_participant=True).values_list('startup_id', flat=True))
        my_mentorships = get_my_mentorships_mentee(request)
        raw_upcoming = get_raw_upcoming_mentee(request)
        raw_pending = get_raw_pending_mentee(request)
        raw_pending_sets = get_raw_pending_sets_mentee(request)
    else:
        my_mentorships = get_my_mentorships_mentee(request)
        raw_upcoming = get_raw_upcoming_mentee(request)
        raw_pending = get_raw_pending_mentee(request)
        raw_pending_sets = get_raw_pending_sets_mentee(request)

    is_add_note = True if request.GET.get('add_note') == 'true' else False
    notable_session = None
    notable_mentorship_id = None
    if is_add_note:
        notable_session_id = request.GET.get('session_id', None)
        try:
            notable_session = MentorshipSession.objects.get(id=int(notable_session_id),
                                                            mentorship__student=request.current_member,
                                                            is_cancelled=False,
                                                            datetime__lte=timezone.now(),
                                                            datetime__gte=ONE_YEAR_AGO)
            notable_mentorship_id = notable_session.mentorship.id
        except MentorshipSession.DoesNotExist:
            is_add_note = False

    upcoming_meetings = []
    for upcoming in raw_upcoming:
        mentor = upcoming.mentorship.mentor

        if my_mentorships.filter(requested_mentorship__mentor=mentor).exists():
            venture = my_mentorships.filter(requested_mentorship__mentor=mentor).last().startup
        else:
            venture = ''

        meeting_custom = custom_meeting_dict(upcoming)

        raw = upcoming.get_datetime()
        day = raw.date().strftime('%a')
        date = raw.date().strftime("%B %d %Y")
        month = raw.date().strftime("%B")
        daynumber = raw.date().strftime("%d")
        start_time = raw.time().strftime("%I:%M")
        end_time = upcoming.get_endtime().time().strftime("%I:%M%p")
        time_zone = upcoming.get_timezone()
        tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone
        contact_info = upcoming.get_type_and_contact_info()
        topic = upcoming.meeting_topic
        upcoming_meetings.append({
            'mentor': mentor,
            'venture': venture,
            'tag': tag,
            'id': upcoming.id,
            'month': month[:3],
            'daynumber': daynumber,
            'start_time': start_time,
            'end_time': end_time,
            'timezone': time_zone,
            'date': raw.date().strftime('%m/%d/%Y'),
            'full_date': date,
            'duration': upcoming.duration,
            'contact_info': contact_info,
            'topic': topic,
            'custom': meeting_custom
        })

    pending_meetings = []
    for pending in raw_pending:
        mentor = pending.mentorship.mentor

        if my_mentorships.filter(requested_mentorship__mentor=mentor).exists():
            venture = my_mentorships.filter(requested_mentorship__mentor=mentor).last().startup
        else:
            venture = ''

        meeting_custom = custom_meeting_dict(pending)

        raw = pending.get_datetime()
        day = raw.date().strftime('%a')
        date = raw.date().strftime("%B %d %Y")
        month = raw.date().strftime("%B")
        daynumber = raw.date().strftime("%d")
        start_time = raw.time().strftime("%I:%M")
        end_time = pending.get_endtime().time().strftime("%I:%M%p")
        time_zone = pending.get_timezone()
        tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone
        topic = pending.meeting_topic
        approval_type = ''
        if not pending.is_mentor_accepted or not pending.is_admin_accepted:
            pending_meetings.append({
                'mentor': mentor,
                'venture': venture,
                'tag': tag,
                'id': pending.id,
                'month': month[:3],
                'daynumber': daynumber,
                'start_time': start_time,
                'end_time': end_time,
                'timezone': time_zone,
                'date': raw.date().strftime('%m/%d/%Y'),
                'full_date': date,
                'duration': pending.duration,
                'topic': topic,
                'approval_type': approval_type,
                'custom': meeting_custom,
                'is_mentor_pending': not pending.is_mentor_accepted,
                'is_admin_pending': not pending.is_admin_accepted
            })

    pending_meeting_sets = []
    for set in raw_pending_sets:
        if set.is_contains_future_session():
            set_info = []
            mentor = set.mentorship.mentor

            if my_mentorships.filter(requested_mentorship__mentor=mentor, is_admin_accepted=True).exists():
                venture = my_mentorships.filter(requested_mentorship__mentor=mentor,
                                                is_admin_accepted=True).first().startup
            else:
                venture = ''

            set_custom = custom_meeting_dict(set.sessions.first())
            set_topic = set.sessions.first().meeting_topic

            set_info.append({
                'id': set.id,
                'mentor': mentor,
                'venture': venture,
                'custom': set_custom,
                'topic': set_topic
            })

            meeting_times = []
            all_sessions = set.sessions.select_related('mentorship', 'mentorship__mentor',
                                                       'mentorship__mentor__profile', 'mentorship__student').all()
            for session in all_sessions:
                raw = session.get_datetime()
                day = raw.date().strftime('%a')
                date = raw.date().strftime("%B %d %Y")
                month = raw.date().strftime("%B")
                daynumber = raw.date().strftime("%d")
                start_time = raw.time().strftime("%I:%M")
                end_time = session.get_endtime().time().strftime("%I:%M%p")
                time_zone = session.get_timezone()
                tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone

                meeting_times.append({
                    'tag': tag,
                    'start_time': start_time,
                    'end_time': end_time,
                    'timezone': time_zone,
                    'month': month[:3],
                    'daynumber': daynumber,
                    'id': session.id,
                    'date': raw.date().strftime('%m/%d/%Y'),
                    'duration': session.duration
                })
            set_info.append(meeting_times)
            pending_meeting_sets.append(set_info)

    for mentorship_req in my_mentorships:
        mentorship = mentorship_req.requested_mentorship
        men_req_id = mentorship_req.id
        mentor = mentorship.mentor
        profile = mentor.profile
        experiences = get_experience_list(profile, style=False)
        degrees = get_degree_list(profile)
        student = mentorship.student

        is_ee = False
        if uni.short_name == 'ucf' and 'Mentors' in \
           mentor.profile.labels.all().values_list('title', flat=True):
            is_ee = True

        venture = mentorship_req.startup
        mentor_tag = profile.bio if not profile.bio or len(
            profile.bio) < 100 else profile.bio[:100] + "..."
        goal_sets = GoalSet.objects.filter(mentorship_id=mentorship.id)

        recent_set = goal_sets.latest(
            'timestamp') if len(goal_sets) > 0 else None

        recent_goals = []
        for gs in goal_sets:
            goals_raw = Goal.objects.select_related('creator').filter(goal_set=gs)
            for goal in goals_raw:
                creator = None
                if goal.creator:
                    creator = goal.creator.get_full_name()
                recent_goals.append({
                    'note': goal.note,
                    'id': goal.id,
                    'creator': creator,
                    'completed': goal.completed,
                })

        raw_notable = get_raw_notable(mentorship)

        notable_meetings = []
        for notable in raw_notable:
            raw = notable.get_datetime()
            day = raw.date().strftime('%a')
            date = raw.date().strftime("%B %d %Y")
            start_time = raw.time().strftime("%I:%M")
            end_time = notable.get_endtime().time().strftime("%I:%M%p")
            tag = day + ", " + date + ", " + start_time + "-" + end_time
            is_selected = False
            if is_add_note:
                is_selected = notable.id == notable_session.id
            notable_meetings.append({
                'tag': tag,
                'id': notable.id,
                'date': raw.date().strftime('%m/%d/%Y'),
                'duration': notable.duration,
                'is_selected': is_selected
            })

        raw_upcoming_json = get_raw_upcoming_json(mentorship)

        upcoming_meetings_json = []
        for upcoming in raw_upcoming_json:
            raw = upcoming.get_datetime()
            day = raw.date().strftime('%a')
            date = raw.date().strftime("%B %d %Y")
            month = raw.date().strftime("%B")
            daynumber = raw.date().strftime("%d")
            start_time = raw.time().strftime("%I:%M")
            end_time = upcoming.get_endtime().time().strftime("%I:%M%p")
            time_zone = upcoming.get_timezone()
            tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + time_zone
            upcoming_meetings_json.append({
                'tag': tag,
                'id': upcoming.id,
                'month': month[:3],
                'daynumber': daynumber,
                'start_time': start_time,
                'end_time': end_time,
                'timezone': time_zone,
                'date': raw.date().strftime('%m/%d/%Y'),
                'duration': upcoming.duration
            })

        can_schedule = mentor.has_schedule() or mentor.has_onetimes()

        mentee_custom = custom_member_dict(
            student, uni.id, Form.MENTORREQUEST, mentor=mentor)

        if not (uni.short_name == 'bu' and 'Expert Coach' in \
                mentor.profile.labels.all().values_list('title', flat=True)):
            my_mentors.append({
                'mentorship_id': mentorship.id,
                'men_req_id': men_req_id,
                'mentor_id': mentor.id,
                'mentee_id': request.user.id,
                'mentor_tag': mentor_tag,
                'is_ee': is_ee,
                'student': student,
                'venture': venture,
                'mentor_rating': mentorship.mentor_rating,
                'can_schedule': can_schedule,
                'profile': profile,
                'preferred_contact': mentor.default_meeting_type,
                'get_preferred_contact': mentor.get_preferred_contact(),
                'recent_goals': recent_goals,
                'recent_goal_set': recent_set,
                'notable_meetings': json.dumps(notable_meetings),
                'upcoming_meetings_json': json.dumps(upcoming_meetings_json),
                'experiences': experiences,
                'degrees': degrees,
                'custom': mentee_custom
            })
    has_pending_meetings = pending_meetings != [] or pending_meeting_sets != []
    ### BEGIN CRONOFY ###
    user = current_member.user
    student = current_member
    student_name = student.first_name + " " + student.last_name
    cronofy_url = generate_cronofy_url(request, reverse('mentee-dashboard'))
    cronofy_user_info = None
    if user.cronofy_refresh_token:
        try:
            cronofy_user_info = get_cronofy_user_calendars(user)
        except PyCronofyRequestError:
            cronofy_user_info = None
    if 'unlink-calendars' in request.POST:
        revoke_cronofy_auth(user)

    if request.GET.get('code', None) and not user.cronofy_refresh_token:
        authenticate_cronofy(request, user, reverse('mentee-dashboard')) # we have to authenticate them via OAuth
        return HttpResponseRedirect('/mentorship/mentee-dashboard')
    is_cronofy_authenticated = cronofy_user_info is not None
    ### END CRONOFY ###

    return TemplateResponse(
        request,
        'mentorship/mentee-dashboard.html',
        {
            'page_name': page_name,
            'my_mentors': my_mentors,
            'student': student,
            'student_name': student_name,
            'is_add_note': is_add_note,
            'notable_mentorship_id': notable_mentorship_id,
            'upcoming_meetings': upcoming_meetings,
            'pending_meetings': pending_meetings,
            'pending_meeting_sets': pending_meeting_sets,
            'has_pending_meetings': has_pending_meetings,
            'is_cronofy_authenticated': is_cronofy_authenticated,
            'cronofy_url': cronofy_url,
            'cronofy_user_info': cronofy_user_info,
        }
    )


@login_required
def decide_meeting_request(request):
    """
    Menntor decide meeting if mentor's meeting approval setting is enabled
    :param request:
    :return:
    """
    if Mentor.objects.filter(profile__user_id=request.user.id).exists():
        current_site_id = request.current_site.id
        curr_mentor = Mentor.objects.get(profile__user_id=request.user.id, platform=request.university)
        curr_mentor_user = curr_mentor.profile.user
        if not curr_mentor.approve_meetings_or_admin_override:
            return HttpResponseRedirect(reverse('mentor-dashboard'))

        session_id = request.POST.get('meeting_id', None)
        session = None
        if session_id is not None:
            try:
                session = MentorshipSession.objects.select_related('mentorship')\
                    .get(id=int(session_id), mentorship__mentor=curr_mentor)
            except MentorshipSession.DoesNotExist:
                return HttpResponseRedirect(reverse('mentor-dashboard'))
        decision = request.POST.get('decision')
        if decision == 'approve':
            if 'is_multiple' in request.POST:
                set_id = request.POST.get('set_id')
                set = MentorshipSessionSet.objects.get(id=set_id)
                sessions = set.sessions.all()
                if session in sessions:
                    # If meeting question for this exists, switch meeting object to selected session
                    Answer.objects.filter(meeting__in=sessions).update(meeting=session)
                    other_sessions = set.sessions.exclude(id=session.id)
                    mentor_is_cronofy_linked = is_cronofy_linked(curr_mentor_user)
                    if mentor_is_cronofy_linked:
                        cronofy_calendar_id = get_cronofy_calendar_id(curr_mentor_user)
                    for other in other_sessions:
                        other.mentor_reject_session()
                        other.admin_reject_session()
                        if mentor_is_cronofy_linked:
                            cronofy_delete_from_calendar(curr_mentor_user, cronofy_calendar_id, other.id, refresh_cronofy_cache=True)
                    set.decided = timezone.now()
                    set.save()
                    if curr_mentor.platform.style.require_meeting_approval != UniversityStyle.NO_APPROVAL:
                        session_request_noti_uni(session, current_site_id)
            session.mentor_accept_session()
            if curr_mentor.platform.style.require_meeting_approval == UniversityStyle.NO_APPROVAL:
                session.admin_accept_session()
            if session.is_admin_accepted:
                if curr_mentor.gcal_email:
                    make_gcal_event(session)
                if is_cronofy_linked(curr_mentor_user):
                    session_change_cronofy_mentor(session, current_site_id, SESSION_CREATED, curr_mentor_user)

                session_change_noti_mentor(session, current_site_id, SESSION_CREATED, admin_decision=True)
                session_change_noti_mentee(session, current_site_id, SESSION_CREATED, admin_decision=True)
                session_change_cronofy_admins__async(request.university, session, SESSION_CREATED)

        elif decision == 'decline':
            session.mentor_reject_session()
            session.admin_reject_session()
            decline_reason = request.POST.get('reason')
            session.cancel_reason = decline_reason
            session.save()
            if is_cronofy_linked(curr_mentor_user):
                session_change_cronofy_mentor(session, current_site_id, SESSION_CANCELLED, curr_mentor_user)

            session_change_noti_mentor(session, current_site_id, SESSION_CANCELLED, admin_decision=True)
            session_change_noti_mentee(session, current_site_id, SESSION_CANCELLED, admin_decision=True)
        elif decision == 'decline_all':
            set_id = request.POST.get('set_id')
            set = MentorshipSessionSet.objects.get(id=set_id)
            decline_reason = request.POST.get('reason')
            for session in set.sessions.all():
                session.mentor_reject_session()
                session.admin_reject_session()
                session.cancel_reason = decline_reason
                session.save()
                if is_cronofy_linked(curr_mentor_user):
                    session_change_cronofy_mentor(session, current_site_id, SESSION_CANCELLED, curr_mentor_user)

            session_change_noti_mentor(set, current_site_id, SESSION_CANCELLED, admin_decision=True)
            session_change_noti_mentee(set, current_site_id, SESSION_CANCELLED)
            set.decided = timezone.now()
            set.save()

        return HttpResponseRedirect(reverse('mentor-dashboard'))

    return HttpResponseRedirect('/')


def fetch_possible_meeting_times(request):
    curr_uni = request.university
    mentor_user_id = request.POST.get('mentor_id', None)
    if mentor_user_id is None:
        mentor_user_id = request.user.id
    if Mentor.objects.filter(profile__user_id=mentor_user_id, platform=curr_uni).exists():
        mentor_id = Mentor.objects.get(profile__user_id=mentor_user_id, platform=curr_uni).id
        times = single_mentorship_schedule(request, mentor_id, upcoming_week=4)

        return JsonResponse({
            "slot_data_list": times
        })


@login_required
@require_POST
def update_meeting_info(request, meeting_id):
    is_admin = True if request.POST.get('is_admin') == 'true' else False
    is_ajax = request.POST.get('is_ajax', False)
    if is_ajax:
        is_ajax = True
    try:
        meeting = MentorshipSession.objects\
            .select_related('mentorship', 'teammentorshipsession', 'mentorship__mentor').get(id=int(meeting_id))
    except MentorshipSession.DoesNotExist:
        if is_ajax:
            return JsonResponse({}, status=404)
        if is_admin:
            return HttpResponseRedirect(reverse('uni-mentorship-list'))
        return HttpResponseRedirect(reverse('mentor-dashboard'))

    is_team = False
    is_authorized = is_admin
    if meeting.is_team_session():
        meeting = meeting.teammentorshipsession
        is_team = True
        if not is_authorized:
            for team_mentorship in meeting.mentorships.all():
                if is_user_authorized_team_mentorship(request.is_university, request.access_levels, team_mentorship,
                                                      request.user.id):
                    is_authorized = True
                    break
    else:
        mentorship = meeting.mentorship
        mentor = mentorship.mentor
        if not is_authorized:
            current_member = request.current_member
            if mentor.profile_id == current_member.id or mentorship.student_id == current_member.id:
                is_authorized = True
    if not is_authorized:
        return HttpResponseForbidden()
    new_date = request.POST.get('new_date', None)
    new_time = request.POST.get('new_time', None)
    new_duration = request.POST.get('new_duration', None)

    timezone = request.session['django_timezone']

    if new_date is not None and new_time is not None:
        # We get both formats, 'Jun 01 2020 01:00PM' and 'June 01 2020 01:00PM', so we use dateutil parser
        new_datetime = localize(
            dateutil.parser.parse(f'{new_date} {new_time}'),
            timezone
        )
        meeting.datetime = new_datetime
        if new_duration is not None:
            meeting.duration = int(new_duration)
        meeting.save()

    new_type = request.POST.get('new_type', None)
    new_location = request.POST.get('new_location', None)
    if new_type is not None and new_location is not None:
        if new_type == '1':
            meeting.meeting_type = 1
            meeting.phone_number = new_location
        elif new_type == '3':
            meeting.meeting_type = 3
            meeting.video_id = new_location
        elif new_type == '4':
            meeting.meeting_type = 4
            meeting.location = new_location
        elif new_type == '5':
            meeting.meeting_type = 5
            meeting.location = None
        meeting.save()
    current_site_id = request.current_site.id
    session_change_noti_mentor(meeting, current_site_id, SESSION_CHANGED)
    session_change_noti_mentee(meeting, current_site_id, SESSION_CHANGED)

    if is_ajax:
        return JsonResponse({})
    if is_admin:
        return HttpResponseRedirect(reverse('uni-mentorship-list'))
    return HttpResponseRedirect(reverse('mentor-dashboard'))


@login_required
def update_mentee_info(request, mentee_id):
    uni = request.university
    mentee = StartupMember.objects.get(id=int(mentee_id))
    mentor_id = request.POST.get('mentor_id', None)
    meeting_id = request.POST.get('meeting_id', None)
    set_id = request.POST.get('set_id', None)

    mentor = None
    if mentor_id is not None and mentor_id != '':
        mentor = Mentor.objects.get(id=int(mentor_id))
    meeting = None
    if meeting_id is not None and meeting_id != '':
        meeting = MentorshipSession.objects.get(id=int(meeting_id))
    elif set_id is not None and set_id != '':
        set = MentorshipSessionSet.objects.get(id=int(set_id))
        meeting = set.sessions.first()

    custom_parser = None
    if meeting:
        custom_parser = CustomAnswersParser(
            request.POST, uni, mentee, None, file_data=request.FILES, mentor=mentor, meeting=meeting)
    elif mentor:
        custom_parser = CustomAnswersParser(
            request.POST, uni, mentee, None, file_data=request.FILES, mentor=mentor)
    if custom_parser is not None:
        try:
            with transaction.atomic():
                custom_parser.parse_answers()
                errors = custom_parser.errors
                if errors != {}:
                    raise ValueError()
                errors = None
        except ValueError:
            errors = custom_parser.get_errors()
            app_logger.exception("ERROR: {0}".format(errors))
            return HttpResponseRedirect('/mentorship/mentee-dashboard')

    return HttpResponseRedirect(reverse('mentee-dashboard'))


def is_show_mentor_get_started(mentor):
    """Returns whether to show the 'Get Started' button on mentorship scheduling page.

    Returns true for:
        mentor has never set any in-house availability and
        is not cronofy linked and
        has not entered a phone/email contact info.
    """
    return \
        not MentorSchedule.objects.filter(mentor_id=mentor.id).exists() and \
        not is_cronofy_linked(mentor.profile.user) and \
        not mentor.phone and \
        not mentor.skype


@login_required
def mentorship_setting(request, mentor_id=None):
    """
    Mentorship settings page.
    :param request:
    :param mentor_id: optional url for university admin to access mentor's setting.
    :return:

    Note: this view function is called for the following cases:
        Mentor goes to Mentorship Settings
        Admin views Mentor's Mentorship Settings (via Manage Mentors Table)
    """
    page_name = 'mentorship-setting'

    uni = request.university
    try:
        is_uni_admin_dashboard = False
        is_cronofy_refresh_invalid = False
        if mentor_id:
            mentor = Mentor.objects.select_related('profile').filter(
                id=mentor_id, platform_id=uni.id)

            if not request.is_university or not mentor.exists() or uni.id != request.is_university.id:
                raise Http404()
            mentor = mentor.first()
            is_uni_admin_dashboard = True
        else:
            mentor = Mentor.objects.select_related('profile').get(
                profile__user_id=request.user.id, platform_id=uni.id)

        if request.method == 'POST':
            mentor.set_timezone(request.POST.get('new_tz'))
            if request.POST.get('tzrefresh'):
                return JsonResponse({'tz': mentor.timezone})

        mentor, schedules, schedule, onetimes, blackout_dates, \
            is_saved, error = set_mentor_settings(request, mentor.id)

        gcal_enabled = True if mentor.gcal_email else False
        gcal_email = mentor.gcal_email

        ### BEGIN CRONOFY ###
        # FIXME It appears that cronofy code below is running needlessly when
        # mentor is not cronofy linked.
        user = mentor.profile.user
        cronofy_url = generate_cronofy_url(request, reverse('mentorship-setting'))

        cronofy_user_info = None
        if user.cronofy_refresh_token:
            try:
                cronofy_user_info = get_cronofy_user_calendars(user)
            except PyCronofyRequestError:
                is_cronofy_refresh_invalid = True
                cronofy_user_info = None
        if 'unlink-calendars' in request.POST:
            if handle_unlink_all_calendars(user):
                cronofy_user_info = None
        # no refresh token == never synched. is_cronofy_refresh_invalid for cronofy error
        if request.GET.get('code', None):  # and (not user.cronofy_refresh_token or is_cronofy_refresh_invalid):
            redirect_uri = request.build_absolute_uri(reverse('mentorship-setting'))
            cronofy_access_token = request.GET.get('code')
            mentor_cronofy_authorization_finished(user, uni, cronofy_access_token, redirect_uri)
            return HttpResponseRedirect('/mentorship/settings')
        is_cronofy_authenticated = cronofy_user_info is not None
        element_token = None
        if not is_cronofy_refresh_invalid and is_cronofy_authenticated:
            ## Begin Availability UI Element
            element_token = get_element_token_general(user, request.get_host())
        ### END CRONOFY ###

        ### Begin Day-Location ###
        cronofy_office_hours = []
        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        if request.method == 'POST':
            for day in range(0,7):
                created = False
                if not MentorCronofySchedule.objects.filter(mentor=mentor, day=day).exists():
                    sched_obj = MentorCronofySchedule.objects.create(mentor=mentor, day=day)
                else:
                    try:
                        sched_obj = MentorCronofySchedule.objects.get(mentor=mentor, day=day)
                    except MentorCronofySchedule.MultipleObjectsReturned:
                        sched_obj = MentorCronofySchedule.objects.filter(mentor=mentor, day=day).order_by('-id')[0]
                    created = True

                sched_obj.type = request.POST.get('{0}_type'.format(days[day]))
                sched_obj.location = request.POST.get('{0}_location'.format(days[day]))
                sched_obj.save()

            cronofy_variable_duration = request.POST.get('cronofy_variable_duration')
            if cronofy_variable_duration and cronofy_variable_duration == "on":
                user.cronofy_variable_duration = True
            else:
                user.cronofy_variable_duration = False
            user.save()
        for day in range(0,7):
            try:
                # When loading MCS object from backend, if type is None or location is None, '', or 'None' then apply default.
                curr_day_data = MentorCronofySchedule.objects.get(mentor=mentor, day=day)
                updated_mcs = False
                if curr_day_data.type is None:
                    curr_day_data.type = mentor.default_meeting_type
                    updated_mcs = True
                if curr_day_data.location is None or curr_day_data.location in ['', 'None']:
                    meeting_type = curr_day_data.type
                    if meeting_type == PHONE:
                        new_location = mentor.phone
                    elif meeting_type == SKYPE:
                        new_location = mentor.skype
                    elif meeting_type == IN_PERSON:
                        new_location = mentor.default_location
                    else:
                        app_logger.error(f'INVALID MEETING TYPE: {meeting_type}')
                        new_location = mentor.default_location
                    curr_day_data.location = new_location
                    updated_mcs = True
                if updated_mcs:
                    curr_day_data.save()
                cronofy_office_hours.append([days[day], curr_day_data.type, curr_day_data.location])
            except ObjectDoesNotExist:
                curr_day_data = MentorCronofySchedule.objects.create(mentor=mentor, day=day)
                cronofy_office_hours.append([days[day], curr_day_data.type, curr_day_data.location])

        ### End Day-Location ###

        # prevent form resubmission, which may lead to resubmitting availability upon refresh (F5 or ctrl + R)
        if is_saved:
            if is_uni_admin_dashboard:
                return HttpResponseRedirect(reverse('uni-manage-mentors', args=(mentor.id,)))
            else:
                return HttpResponseRedirect(reverse('mentorship-setting'))
        return TemplateResponse(
            request,
            'mentorship/mentor-settings.html',
            {
                'page_name': page_name,
                'is_cronofy_authenticated': is_cronofy_authenticated,
                'user': user,
                'protocol': PROTOCOL,
                'cronofy_url': cronofy_url,
                'cronofy_client': settings.CRONOFY_CLIENT_ID,
                'element_token' : element_token,
                'cronofy_office_hours': cronofy_office_hours,
                'schedules': schedules,
                'schedule_page': {'prev': 0, 'cur': 1, 'next': 2},
                'schedule': schedule,
                'sched_count': schedules.count(),
                'mentor': mentor,
                'mentor_id': mentor.id,
                'timezones': pytz.common_timezones,
                'days': MentorSchedule.DAYS,
                'durations': DURATIONS,
                'is_saved': is_saved,
                # 'default': defaults[0],
                'onetime': onetimes,
                'blackout_date': blackout_dates,
                'error': error,
                'gcal_enabled': gcal_enabled,
                'gcal_email': gcal_email,
                'is_uni_admin_dashboard': is_uni_admin_dashboard,
                'is_cronofy_refresh_invalid': is_cronofy_refresh_invalid,
                'is_show_mentor_get_started': is_show_mentor_get_started(mentor) and not is_uni_admin_dashboard,
            }
        )

    except Mentor.DoesNotExist:
        if mentor_id:
            return HttpResponseRedirect(reverse('uni-manage-mentors'))
        else:
            return HttpResponseRedirect('/mentorship/mentee-dashboard')


def split_recurring_avail(sched: MentorSchedule, day: dt.date, del_future_weeks=False):
    """
    WARNING: day must fall on weekday of sched

    Takes a recurring schedule and splits or deletes it such that
    mentor has the same recurring availability as before, except the availability on `day` (and all subsequent weeks
    if del_future_weeks==True) are removed.

    """
    if sched.date > day:
        raise ValueError('sched start cannot be after `day`')
    if sched.end_date and day > sched.end_date:
        raise ValueError('`day` cannot be after sched end')
    if sched.date.weekday() != day.weekday():
        raise ValueError('`day` must fall on the same weekday as sched date')

    prev_occurrence = day - timedelta(days=7)
    next_occurrence = day + timedelta(days=7)

    # CASE 1: We delete the first occurrence
    #   1A: del_future_weeks = True => we delete the entire rule
    #   1B: del_future_weeks = False
    #       1B-A: There are subsequent recurrences => we adjust the start date
    #       1B-B: There are not subsequent recurrences => we delete the entire rule
    if sched.date == day:
        if del_future_weeks:
            sched.delete()
        else:
            if sched.end_date is None or next_occurrence <= sched.end_date:
                sched.date = next_occurrence
                sched.save()
            else:
                sched.delete()

    # CASE 2: We delete the last occurrence (del_future_weeks doesn't matter)
    #   2A: There are previous occurrences  => we adjust the end date
    #   2B: There are not previous occurrences  => we delete the entire rule
    elif sched.end_date and next_occurrence > sched.end_date:
        if prev_occurrence >= sched.date:
            sched.end_date = prev_occurrence
            sched.save()
        else:
            sched.delete()

    # CASE 3: We delete a middle occurrence
    #   3A: del_future_weeks = True => we adjust the end date
    #   3B: del_future_weeks = False => we split the availability rule
    else:
        if del_future_weeks:
            sched.end_date = prev_occurrence
            sched.save()
        else:
            # make clone of object (https://docs.djangoproject.com/en/2.2/topics/db/queries/#copying-model-instances)
            sched_1_id = sched.id
            sched.pk = None
            sched.save()
            sched_2_id = sched.id

            sched_1 = MentorSchedule.objects.get(id=sched_1_id)
            sched_2 = MentorSchedule.objects.get(id=sched_2_id)

            sched_1.end_date = prev_occurrence
            sched_1.save()

            sched_2.date = next_occurrence
            sched_2.save()


@require_POST
@login_required
def avail_delete(request):
    sched_id = request.POST.get('sched_id')
    action = request.POST.get('del-action')
    iso_day = request.POST.get('iso_day')

    sched = MentorSchedule.objects.get(id=sched_id)
    day = dt.date.fromisoformat(iso_day)

    if action == "del-one-time":
        sched.delete()
    elif action == "del-weekly":
        weekly_action = request.POST.get('weekly-action')
        if weekly_action == "del-every-week":
            sched.delete()
        elif weekly_action == "del-this-week":
            split_recurring_avail(sched, day)
        elif weekly_action == "del-this-and-future-weeks":
            split_recurring_avail(sched, day, del_future_weeks=True)
    else:
        raise ValueError(f'invalid action: {action}')

    return HttpResponseRedirect(reverse('mentorship-setting'))


@require_POST
@login_required
def refresh_cronofy_cache_view(request):
    mentor_id = request.POST.get('mentor_id')
    mentor = Mentor.objects.get(id=mentor_id)
    mentor_user = mentor.profile.user

    if not (request.user.id == mentor_user.id or
            is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                request.is_startuptree_staff, AccessLevel.MENTORSHIP)):
        return HttpResponseForbidden()

    refresh_cronofy_cache__user(mentor_user)

    return JsonResponse({'status': 200})


@require_POST
@login_required
@transaction.atomic
def mentor_request(request, via_meeting=False):
    """AJAX
    Send mentorship request

    @param request:

    @param via_meeting: User scheduled a meeting and this request was made automatically.

    return:
    """

    if not request.is_valid_platform:
        payload = {
            PAYLOAD_STATUS: INVALID_REQUEST_CODE,
            PAYLOAD_MSG: "The mentorship request cannot be proceeded. Please contact admin.",
        }
        return JsonResponse(payload)

    return send_mentor_request(request.user, request.POST, request.FILES, request.university, via_meeting)


def time_from_req(request, is_multiple):
    if is_multiple:
        selected_dates = request.POST.getlist('date[]', [])
        selected_start_times = request.POST.getlist('start_time[]', [])
        durations = request.POST.getlist('duration[]', [])
        years = request.POST.getlist('year[]', [])

        selected_days = []
        for idx in range(len(selected_dates)):
            date, _ = get_formatted_time(years[idx], selected_dates[idx], selected_start_times[idx])
            selected_days.append(date.strftime("%a"))

        return selected_dates, selected_start_times, durations, years, selected_days
    else:
        selected_date = request.POST.get('date', None)
        selected_start_time = request.POST.get('start_time', None)
        duration = request.POST.get('duration', None)
        year = request.POST.get('year', None)
        # Day of the week (for scheduling through cronofy)
        selected_day = request.POST.get('day', None)

        if selected_day is None:
            date, _ = get_formatted_time(year, selected_date, selected_start_time)
            selected_day = date.strftime("%a")

        return selected_date, selected_start_time, duration, year, selected_day


def session_info_from_req(request, is_multiple):
    # TODO: Fix this - need better way of creating/displaying mentor schedules and sessions
    # Maybe get rid of sessiosn and have schedule handle all of this? Too many weird edge cases
    # because they are separate - also need tests...
    # this is for SW-2544
    if is_multiple:
        meeting_types = request.POST.getlist('type[]', [])
        locations = request.POST.getlist('location[]', [])
        meeting_topic = request.POST.get('meeting_topic', None)

        return meeting_types, locations, meeting_topic
    else:
        meeting_type = request.POST.get('type', '')
        location = request.POST.get('location', None)
        meeting_topic = request.POST.get('meeting_topic', None)

        return meeting_type, location, meeting_topic


def improper_time_selection(request):
    if 'is_multiple' in request.POST:
        selected_dates, selected_start_times, durations, years, selected_days = time_from_req(request, True)
        meeting_types, _, _ = session_info_from_req(request, True)
        return (None in selected_dates) or \
               (None in selected_start_times) or \
               (None in durations) or \
               (None in years) or \
               ('' in selected_days and '' in meeting_types)
    else:
        selected_date, selected_start_time, duration, year, selected_day = time_from_req(request, False)
        meeting_type, _, _ = session_info_from_req(request, False)
        return selected_start_time is None or \
            selected_date is None or \
            duration is None or \
            year is None or \
            (meeting_type == '' and selected_day == '')


@require_POST
@login_required
@only_university
def mentorshipsession_add__by_ids(request, university):
    return JsonResponse(platform_admin_schedule_session(request.POST, request.session['django_timezone'],
                                                        university, request.university_style,
                                                        request.current_member, request.is_university, request.is_super,
                                                        request.access_levels, request.is_startuptree_staff))


@login_required
def mentorshipsession_cancel(request, session_id):
    """
    Cancel a upcoming mentorship session
    :param request:
    :param mentorship_id:
    :return:
    """
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE,
               PAYLOAD_MSG: ""}

    session = None
    set = None
    if 'multiple' in request.POST:
        try:
            set = MentorshipSessionSet.objects.get(id=int(session_id))
        except MentorshipSessionSet.DoesNotExist:
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "Invalid request."
            return JsonResponse(payload)
        mentorship = set.mentorship
    else:
        try:
            session = MentorshipSession.objects.get(id=int(session_id))
        except MentorshipSession.DoesNotExist:
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = "Invalid request."
            return JsonResponse(payload)
        mentorship = session.mentorship

    platform = mentorship.mentor.platform
    if platform.id != request.university.id:
        # Invalid platform
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    if mentorship.mentor.profile_id == request.current_member.id:
        is_by_mentor = True
    elif mentorship.student_id == request.current_member.id:
        is_by_mentor = False
    else:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    current_site_id = request.current_site.id
    if session is not None:
        # If the mentor cancelled.
        if is_by_mentor:
            session.is_cancelled_by_student = False
        else:
        # If the student cancelled.
            session.is_cancelled_by_student = True

        if session.event_id:
            remove_gcal_event(session)

        session.is_cancelled = True
        session.cancel_reason = request.POST.get('reason')
        session.save()

        session_change_noti_mentor(session, current_site_id, SESSION_CANCELLED)
        session_change_noti_mentee(session, current_site_id, SESSION_CANCELLED)
    elif set is not None:
        for session in set.sessions.all():
            # If the mentor cancelled.
            if is_by_mentor:
                session.is_cancelled_by_student = False
            else:
                # If the student cancelled.
                session.is_cancelled_by_student = True

            session.is_cancelled = True
            session.cancel_reason = request.POST.get('reason')
            session.save()
        session_change_noti_mentor(set, current_site_id, SESSION_CANCELLED)
        session_change_noti_mentee(set, current_site_id, SESSION_CANCELLED)
        set.decided = timezone.now()
        set.save()

    return JsonResponse(payload)


# @require_POST
# @login_required
# def mentor_edit_timeslots(request):
#     """AJAX
#     Add or delete available timeslots of the mentor.
#     :param request:
#         Expects post request with parameter "timeslots" that is a list of
#         available timeslots for the mentor each element of the list is
#         a json object in the following format:
#         {"day": 1-5, "time": hh:mm, "duration": 15 || 30 || 45 || 60 }
#         day is Monday - Friday, hour is 24 hour format,
#         duration is in minutes.
#     :return:
#     """
#
#     mentor_id = request.POST.get("mentor_id")
#
#     payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
#     try:
#         if mentor_id:
#             mentor_id = int(mentor_id)
#             mentor = Mentor.objects.get(id=mentor_id)
#         else:
#             mentor = Mentor.objects.get(profile__user_id=request.user.id, platform__site_id=request.current_site.id)
#     except Mentor.DoesNotExist:
#         payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
#         return JsonResponse(payload)
#
#     is_authorized = False
#     if mentor.profile.user_id == request.user.id:
#        is_authorized = True
#     else:
#         if request.is_university:
#             if AccessLevel.ALL in request.access_levels or \
#                     AccessLevel.MENTORSHIP in request.access_levels or \
#                     request.access_levels in AccessLevel.TEAM_MENTORSHIP:
#                 is_authorized = True
#     if not is_authorized:
#         raise Http404()
#
#     input_days = json.loads(request.POST.get("days"))
#     error_days = []
#     try:
#         with transaction.atomic():
#
#             for input_day in input_days:
#                 start_times = input_day.get('start_times', [])
#                 end_times = input_day.get('end_times', [])
#                 day = input_day['day']
#                 is_inactive = input_day['is_inactive']
#                 if len(start_times) == 0 or len(end_times) == 0:
#                     MentorSchedule.objects.filter(
#                         day=day, mentor_id=mentor.id).delete()
#                     continue
#                 duration = int(input_day.get('duration', 30))
#                 # data is inconsistent
#                 if len(start_times) != len(end_times):
#                     app_logger.warning(
#                         'unmatched length: {0}, {1}, {2}'.format(
#                             start_times, end_times, duration))
#                     error_days.append(day)
#                     continue
#                 if is_inactive and MentorSchedule.objects.filter(
#                         day=day, mentor_id=mentor.id).exists():
#                     MentorSchedule.objects.filter(
#                         day=day, mentor_id=mentor.id).update(
#                         is_inactive=True)
#                     continue
#                 length = len(start_times)
#                 MentorSchedule.objects.filter(
#                     day=day, mentor_id=mentor.id).delete()
#                 for i in range(length):
#                     start_time = TimeForm({"time": start_times[i]})
#                     end_time = TimeForm({"time": end_times[i]})
#                     is_st_valid = start_time.is_valid()
#                     is_et_valid = end_time.is_valid()
#                     # invalid time was given
#                     if not is_st_valid or not is_et_valid:
#                         error_days.append(day)
#                         continue
#                     start_time = start_time.cleaned_data['time']
#                     end_time = end_time.cleaned_data['time']
#                     if MentorSchedule.objects.filter(
#                             Q(start_time=start_time) & Q(end_time=end_time),
#                             mentor_id=mentor.id, day=day,
#                     ).exists():
#                         continue
#
#                     possible_slots = \
#                         time_minute_diff(start_time, end_time)
#                     possible_slots /= duration
#                     possible_slots = int(possible_slots)
#
#                     if possible_slots <= 0:
#                         # no possible slot.
#                         app_logger.warning(
#                             "Wrong time choice choices : {0}, {1}".format(
#                                 start_time, end_time))
#                         error_days.append(day)
#                         continue
#                     try:
#                         duration_choice = MentorSchedule.get_minute_choice(
#                             duration)
#                     except ValueError:
#                         # invalid duration
#                         app_logger.warning(
#                             "Wrong duration choice : {0}".format(duration))
#                         error_days.append(day)
#                         continue
#
#                     # invalid timeslot was given (overlapping schedule found)
#                     if not MentorSchedule.objects.validate_timeslot(
#                             mentor, day, start_time, end_time):
#                         app_logger.warning(
#                             "Invalid schedule 0x1: {0} - {1}".format(day, start_time))
#                         error_days.append(day)
#                         continue
#                     time_range = (start_time, end_time)
#                     if MentorSchedule.objects.filter(
#                             Q(start_time__range=time_range) | Q(end_time__range=time_range),
#                             mentor=mentor, day=day,
#                     ).exists():
#                         app_logger.warning(
#                             "Invalid schedule 0x2: {0} - {1}".format(day, start_time))
#                         error_days.append(day)
#                         continue
#
#                     sid = MentorSchedule.objects.create(
#                         mentor=mentor,
#                         day=day,
#                         duration=duration_choice,
#                         start_time=start_time,
#                         is_inactive=is_inactive,
#                         is_repeating=True,
#                         end_time=end_time).id
#                     payload['sid'] = sid
#
#             if len(error_days) > 0:
#                 raise Exception()
#     except BaseException:
#         payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
#         payload['error_days'] = error_days
#
#     return JsonResponse(payload)


def mentor_or_mentee(m_request, request):
    is_mentor = m_request.requested_mentorship.mentor.profile.user_id == request.user.id
    is_mentee = m_request.requested_mentorship.student.user_id == request.user.id

    return is_mentor, is_mentee


@login_required
def request_detail(request, request_id):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}

    # Get mentor request by ID.
    # Gives access to the mentorship, the student, and the mentor.
    m_request = MentorshipRequest.objects \
        .select_related('requested_mentorship',
                        'requested_mentorship__student',
                        'requested_mentorship__mentor__profile') \
        .get(id=int(request_id))

    # Is the current user the mentor?
    # Is the current user the student/mentee?
    is_mentor, is_mentee = mentor_or_mentee(m_request, request)

    # User is neither the mentor nor the mentee.
    if not is_mentor and not is_mentee:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        app_logger.error('User is not a part of this mentorship.')

        return JsonResponse(payload)

    try:
        """
        Tries to get information about venture.
        """
        # Get the Startup by ID and convert its basic information to a
        # dictionary.
        startup = Startup.objects.get(id=m_request.startup_id)
        startup = startup_to_dict_basic(startup, current_site=request.current_site, is_admin=True)

        # Add the name and url_name to the payload.
        payload['startup_name'] = startup['name']
        payload['startup_urlname'] = startup['url_name']

    except ObjectDoesNotExist:
        """
        No venture yet.
        """
        payload['startup_name'] = "No {0} yet".format(request.university.style.venture_word)
        payload['startup_urlname'] = ''

    # Additional information about venture.
    # Venture might or might not exist, but this does not matter in this case.
    payload['pitch'] = m_request.pitch
    payload['other'] = m_request.other
    payload['date'] = m_request.created_on.date().isoformat()

    # If the admin rejected the mentorship, store the reason.
    if request.GET.get('is_cancelled', False):
        payload['reason'] = m_request.admin_reject_reason

    return JsonResponse(payload)


@login_required
def get_student_notes_gen(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mid = int(request.GET.get('mid'))
    sid = int(request.GET.get('sid'))
    mentorship = Mentorship.objects \
        .select_related('mentor__profile') \
        .get(id=mid)
    if mentorship.mentor.profile.user_id != request.user.id:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    if mentorship.student_id != sid:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    notes = mentorship.mentor_notes
    notes = "" if notes is None else notes

    payload['notes'] = notes

    return JsonResponse(payload)


@require_POST
@login_required
def set_student_notes_gen(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}

    try:
        mid = int(request.POST.get('mid'))
        sid = int(request.POST.get('sid'))
    except TypeError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    mentorship = Mentorship.objects \
        .select_related('mentor__profile') \
        .get(id=mid)

    if mentorship.mentor.profile.user_id != request.user.id:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    if mentorship.student_id != sid:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    notes = request.POST.get('notes')

    mentorship.mentor_notes = notes
    mentorship.save()

    return JsonResponse(payload)


@login_required
def get_student_notes(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}

    try:
        mid = int(request.GET.get('mid'))
        sid = int(request.GET.get('sid'))
    except TypeError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    mentorship = Mentorship.objects \
        .select_related('mentor__profile') \
        .get(id=mid)

    if mentorship.mentor.profile.user_id != request.user.id:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    if mentorship.student_id != sid:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    gen_notes = mentorship.mentor_notes
    gen_notes = "" if gen_notes is None else gen_notes

    student_mentorships = Mentorship.objects.filter(
        student_id=sid).values_list('id', flat=True)
    student_sessions = \
        MentorshipSession.objects \
            .select_related('mentorship',
                            'mentorship__mentor__profile') \
            .filter(mentorship_id__in=student_mentorships,
                    note__isnull=False)
    notes = []
    for session in student_sessions:
        goals_list = Goal.objects.filter(session_id=session.id)
        goals = []
        for goal in goals_list:
            goals.append({
                'goal': goal.note,
                'completed': goal.completed,
            })
        note = {
            'title': '{0}: session with {1}'.format(
                session.datetime.date().isoformat(),
                session.mentorship.mentor.profile.get_full_name()),
            'content': session.note,
            'goals': goals,
        }
        notes.append(note)
    payload['notes'] = notes
    payload['gen_notes'] = gen_notes

    return JsonResponse(payload)


@login_required
def get_session_note(request, session_id):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    sid = int(session_id)
    session = MentorshipSession.objects \
        .select_related('mentorship',
                        'mentorship__mentor__profile') \
        .get(id=sid)
    if session.mentorship.mentor.profile.user_id != request.user.id:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    goals_list = Goal.objects.filter(session_id=session.id)
    goals = []
    for goal in goals_list:
        goals.append({
            'goal': goal.note,
            'completed': goal.completed,
        })
    note = {
        'title': '{0}: session with {1}'.format(
            session.datetime.date().isoformat(),
            session.mentorship.mentor.profile.get_full_name()),
        'content': session.note,
        'goals': goals,
    }
    payload['note'] = note

    return JsonResponse(payload)


@login_required
def get_session_goals(request, session_id):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    sid = int(session_id)
    session = MentorshipSession.objects \
        .select_related('mentorship',
                        'mentorship__mentor__profile') \
        .get(id=sid)
    if session.mentorship.student.user.id != request.user.id:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE

        return JsonResponse(payload)

    goals_list = Goal.objects.filter(session_id=session.id)
    goals = []
    for goal in goals_list:
        goals.append({
            'goal': goal.note,
            'completed': goal.completed,
            'gid': goal.id,
        })
    payload['goals'] = goals

    return JsonResponse(payload)


@require_POST
@login_required
def post_session_goals(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    session = MentorshipSession.objects.get(id=request.POST.get('id'))
    goals = request.POST.getlist('goals[]')
    for i in range(0, len(goals), 2):
        goal = Goal.objects.get(id=goals[i])
        goal.completed = True if goals[i + 1] == "true" else False
        goal.save()

    return JsonResponse(payload)


@require_POST
@login_required
def edit_blackout(request):
    """AJAX
    Blackout date setting
    """
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentor_id = request.POST.get("mentor_id")
    try:
        if mentor_id:
            mentor_id = int(mentor_id)
            mentor = Mentor.objects.get(id=mentor_id)
        else:
            mentor = Mentor.objects.get(profile__user_id=request.user.id, platform__site_id=request.current_site.id)
    except Mentor.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    is_authorized = False
    if mentor.profile.user_id == request.user.id:
        is_authorized = True
    else:
        if request.is_university:
            if AccessLevel.ALL in request.access_levels or \
                    AccessLevel.MENTORSHIP in request.access_levels or \
                    request.access_levels in AccessLevel.TEAM_MENTORSHIP:
                is_authorized = True
    if not is_authorized:
        raise Http404()
    dates = json.loads(request.POST.get('dates'))
    start_dates = dates.get('interval_starts')
    end_dates = dates.get('interval_ends')
    if len(start_dates) != len(end_dates):
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    length = len(start_dates)
    try:
        MentorScheduleBlackOut.objects.filter(mentor_id=mentor.id).delete()
        with transaction.atomic():
            for i in range(length):
                start_date = start_dates[i]
                end_date = end_dates[i]
                if start_date == '' or end_date == '':
                    continue
                start_date = datetime.strptime(start_date, '%m/%d/%Y')
                end_date = datetime.strptime(end_date, '%m/%d/%Y')
                MentorScheduleBlackOut.objects.get_or_create(
                    start_date=start_date, end_date=end_date, mentor=mentor)
    except BaseException:
        app_logger.warning(
            'blackout date process error - {0}:{1}'.format(start_dates[i], end_dates[i]))
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE

    return JsonResponse(payload)


@require_POST
@login_required
def edit_defaults(request):
    """AJAX
    Mentorship default settings
    """
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentor_id = request.POST.get("mentor_id")
    try:
        if mentor_id:
            mentor_id = int(mentor_id)
            mentor = Mentor.objects.get(id=mentor_id)
        else:
            mentor = Mentor.objects.get(profile__user_id=request.user.id, platform__site_id=request.current_site.id)
    except Mentor.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    MentorshipDefaults.objects.filter(mentor_id=mentor.id).delete()

    defaults = json.loads(request.POST.get('defaults'))
    location = defaults.get('location')
    preferred = defaults.get('preferred')
    duration_minutes = defaults.get('duration')
    duration = MentorSchedule.get_minute_choice(duration_minutes)
    officehours = defaults.get('officehours')

    MentorshipDefaults.objects.get_or_create(
        location=location, preferred_contact=preferred,
        is_officehours=officehours, duration=duration, mentor=mentor)

    return JsonResponse(payload, safe=False)


@require_POST
@login_required
def edit_one_time(request):
    """AJAX
    One-Time Availibility settings
    """
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentor_id = request.POST.get("mentor_id")
    try:
        if mentor_id:
            mentor_id = int(mentor_id)
            mentor = Mentor.objects.get(id=mentor_id)
            MentorSchedule.objects.filter(mentor_id=mentor.id, date__isnull=False, is_repeating=False).delete()

        else:
            mentor = Mentor.objects.get(profile__user_id=request.user.id, platform__site_id=request.current_site.id)
            MentorSchedule.objects.filter(mentor_id=mentor.id, date__isnull=False, is_repeating=False).delete()
    except Mentor.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    is_authorized = False
    if mentor.profile.user_id == request.user.id:
        is_authorized = True
    else:
        if request.is_university:
            if AccessLevel.ALL in request.access_levels or \
                    AccessLevel.MENTORSHIP in request.access_levels or \
                    request.access_levels in AccessLevel.TEAM_MENTORSHIP:
                is_authorized = True
    if not is_authorized:
        raise Http404()

    time_errors = []
    duration_errors = []
    other_errors = []
    gen_error = None
    dates = json.loads(request.POST.get('dates'))
    try:
        with transaction.atomic():
            for raw_date in dates.keys():
                date = datetime.strptime(raw_date, '%m/%d/%Y')
                obj = dates.get(raw_date)
                starts = obj.get('starts', [])
                ends = obj.get('ends', [])
                duration = int(obj.get('dur', 30))

                if not len(starts) == len(ends):
                    app_logger.warning(
                        'unmatched length: {0}, {1}'.format(
                            starts, ends))
                    other_errors.append(raw_date)
                    continue

                for i in range(len(starts)):
                    start_time = TimeForm({"time": starts[i]})
                    end_time = TimeForm({"time": ends[i]})

                    if not start_time.is_valid() or not end_time.is_valid():
                        time_errors.append(raw_date)
                        continue

                    start_time = start_time.cleaned_data['time']
                    end_time = end_time.cleaned_data['time']

                    slots = time_minute_diff(start_time, end_time)
                    slots = int(slots / duration)

                    if slots <= 0:
                        app_logger.warning(
                            "Wrong time choice choices : {0}, {1}".format(
                                start_time, end_time))
                        time_errors.append(raw_date)
                        continue

                    try:
                        duration_choice = MentorSchedule.get_minute_choice(
                            duration)
                    except ValueError:
                        # invalid duration
                        app_logger.warning(
                            "Wrong duration choice : {0}".format(duration))
                        duration_errors.append(duration)
                        continue

                    MentorSchedule.objects.get_or_create(
                        mentor=mentor,
                        start_time=start_time,
                        end_time=end_time,
                        date=date,
                        duration=duration_choice,
                        is_repeating=False)
    except BaseException:
        app_logger.warning("Error creating onetime object")
        error_gen = "Sorry, your one time availibilities could not be completed at this time"
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE

    error = ""
    if gen_error:
        error = gen_error
    elif len(time_errors) > 0:
        error += "Invalid times for date(s): " + ",".join(time_errors)
    elif len(duration_errors) > 0:
        if not error == "":
            error += ", "
        error += "Invalid durations for date(s): " + ",".join(duration_errors)
    elif len(other_errors) > 0:
        if not error == "":
            error += ", "
        error += "Error for date(s): " + ",".join(other_errors)

    if not error == "":
        payload['error'] = error
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
    else:
        payload['error'] = None

    return JsonResponse(payload)


@require_POST
@login_required
def create_note(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentorship_id = int(request.POST.get('mentorship_id'))
    session_id = int(request.POST.get('session_id'))
    rating = int(request.POST.get('rating'))
    offline_date = request.POST.get('offline_date')
    offline_duration = int(request.POST.get('offline_duration'))
    text = request.POST.get('note', '')
    try:
        mentorship = Mentorship.objects.select_related('mentor', 'mentor__platform', 'student').get(id=mentorship_id)
    except Mentorship.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    platform = mentorship.mentor.platform
    if platform.id != request.university.id:
        # Invalid platform
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    if mentorship.mentor.profile_id == request.current_member.id:
        is_by_mentor = True
    elif mentorship.student_id == request.current_member.id:
        is_by_mentor = False
    else:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    if offline_date == "":
        offline_date = None
    else:
        offline_date = datetime.strptime(request.POST.get('offline_date'), '%m/%d/%Y').date()

    text_form = TextareaForm({'text': text})
    date_form = DateTimeForm({'datetime': offline_date})

    if not text_form.is_valid() or (not date_form.is_valid() and offline_date):
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    try:
        if session_id == NOT_FOUND:
            session = None
        else:
            session = MentorshipSession.objects.get(
                id=session_id, mentorship=mentorship)

    except MentorshipSession.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    if session:
        # TODO: These are at Note object. Deprecate the below fields and get data from relevant notes
        if is_by_mentor:
            session.mentor_feedback = rating
        else:
            session.student_feedback = rating
        session.note_created = timezone.now()
        session.save()

    offline_duration = None if offline_duration == NOT_FOUND else offline_duration
    rating = None if rating == NOT_FOUND else rating

    mentorship_activity = MentorshipActivity.objects.make_mentorship_activity(MentorshipActivity.NOTE_CREATED, platform)

    just_created = Note.objects.create(
        mentorship=mentorship,
        is_by_mentor=is_by_mentor,
        text=text,
        session=session,
        offline_date=offline_date,
        offline_duration=offline_duration,
        rating=rating,
        mentorship_activity=mentorship_activity)

    the_files = request.FILES
    if the_files:
        errors = {'all': False, 'specific': []}

        payload['file_ids'] = []
        for file_key in the_files:
            the_file = the_files[file_key]
            file_form = AnserFileForm({}, {'uploaded_file': the_file})
            if file_form.is_valid() or the_file.size > 15728640:
                file_name = file_form.cleaned_data['uploaded_file']
                na = NoteAttachment.objects.create(
                    note=just_created, file_name=the_file.name)
                na.attachment = file_name
                na.save()
                payload['file_ids'].append(na.id)
            else:
                errors['specific'].append(the_file.name)
                payload = {PAYLOAD_STATUS: INVALID_REQUEST_CODE}

        payload['errors'] = json.dumps(errors)

    return JsonResponse(payload)


class NoteAttachedFileView(RedirectView):
    """Attached File view:
    acknowledgement:
    http://www.gyford.com/phil/writing/2012/09/26/django-s3-temporary.php

    Note: This class is almost identical to MentorshipFileDownloadView.
    If modifying this class, consider making analogous changes to the other.
    """
    permanent = False

    def get_redirect_url(self, **kwargs):
        s3 = S3Connection(settings.AWS_ACCESS_KEY_ID,
                          settings.AWS_SECRET_ACCESS_KEY,
                          is_secure=True)
        bucket = s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
        key = bucket.get_key(str(kwargs['filepath']))
        return key.generate_url(expires_in=300)

    def get(self, request, *args, **kwargs):
        if not request.is_valid_platform:
            return HttpResponseRedirect("/")
        note_attachment = get_object_or_404(NoteAttachment, pk=kwargs['pk'])
        u = request.user
        mentorship = note_attachment.note.mentorship
        if u.is_authenticated and not request.is_archived:
            current_member = request.current_member
            if mentorship.student_id != current_member.id and mentorship.mentor.profile_id != current_member.id:
                if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
                    raise Http404()
            if note_attachment.attachment:
                file_path = str(note_attachment.attachment)
                if settings.DEBUG and not settings.IS_STAGING:
                    url = settings.MEDIA_DIRECTORY + file_path
                else:
                    url = self.get_redirect_url(filepath=file_path)
                # The below is taken straight from RedirectView.
                if url:
                    if self.permanent:
                        return HttpResponsePermanentRedirect(url)
                    else:
                        return HttpResponseRedirect(url)
                else:
                    app_logger.warning('Gone: %s', self.request.path,
                                       extra={
                                           'status_code': 410,
                                           'request': self.request
                                       })
                    return HttpResponseGone()
            else:
                raise Http404('Page not found')
        else:
            raise Http404('Page not found')


@login_required
@require_POST
def create_goals(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentorship_id = int(request.POST.get('mentorship_id'))
    due_date = request.POST.get('due_date')

    if due_date == "":
        due_date = None
    else:
        due_date = datetime.strptime(due_date, '%m/%d/%Y').date()

    goals = json.loads(request.POST.get('goals'))

    try:
        mentorship = Mentorship.objects.select_related('mentor', 'mentor__platform', 'student').get(id=mentorship_id)
    except Mentorship.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        app_logger.debug('Mentorship does not exist')
        return JsonResponse(payload)

    platform = mentorship.mentor.platform
    if platform.id != request.university.id:
        # Invalid platform
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        app_logger.debug('Invalid mentorship platform')
        return JsonResponse(payload)
    if mentorship.mentor.profile_id == request.current_member.id:
        is_by_mentor = True
    elif mentorship.student_id == request.current_member.id:
        is_by_mentor = False
    else:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    mentorship_activity = MentorshipActivity.objects.make_mentorship_activity(MentorshipActivity.GOALSET_CREATED, platform)

    goal_set = GoalSet.objects.create(
        mentorship=mentorship,
        due_date=due_date,
        mentorship_activity=mentorship_activity
    )

    for goal in goals:
        text_form = TextareaForm({'text': goal})

        if not text_form.is_valid():
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            return JsonResponse(payload)

        Goal.objects.create(
            goal_set=goal_set,
            note=goal,
            creator=request.current_member,
            datetime=timezone.now()
        )

    return JsonResponse(payload)


@login_required
@require_POST
def update_goal(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    goal_id = int(request.POST.get('goal_id'))
    is_checked = json.loads(request.POST.get('is_checked'))

    try:
        goal = Goal.objects.get(
            id=goal_id,
            goal_set__mentorship__student__user_id=request.user.id)
    except Goal.DoesNotExist:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    goal.completed = is_checked
    goal.save()

    if goal.completed:
        mentorship = goal.goal_set.mentorship
        send_goal_completed_noti.delay(
            request.current_site.id,
            mentorship.mentor_id,
            mentorship.student.get_full_name(),
            goal.note
        )

    return JsonResponse(payload)


@login_required
@require_POST
def rate_mentor(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    mentorship_id = int(request.POST.get('mentorship_id'))
    mentor_rating = int(request.POST.get('mentor_rating'))

    try:
        mentorship = Mentorship.objects.get(
            id=mentorship_id, student__user_id=request.user.id)
    except BaseException:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    mentorship.mentor_rating = mentor_rating
    mentorship.save()

    return JsonResponse(payload)


def view_notes_goals_dict(
        men_reqs,
        start_date=None,
        end_date=None,
        include_goals=True,
        include_notes=True,
        show_complete=True,
        show_incomplete=True):
    mentors = []

    for req in men_reqs:
        mentorship = req.requested_mentorship
        if include_notes:
            raw_notes = Note.objects.filter(mentorship=mentorship).order_by('timestamp')
        else:
            raw_notes = []

        if include_goals:
            goal_sets = GoalSet.objects.filter(
                mentorship=mentorship).order_by('timestamp')
        else:
            goal_sets = []

        data = []

        for note in raw_notes:

            note_attachments = []
            for attached in NoteAttachment.objects.filter(note=note):
                note_attachments.append({
                    'file_name': attached.file_name,
                    'id': attached.id
                })

            if note.session:
                timestamp = note.session.get_datetime()
                date_tag = timestamp.strftime('%a') + ", " + \
                           timestamp.strftime("%B %d %Y") + ", " + \
                           timestamp.strftime("%I:%M") + "-" + \
                           note.session.get_endtime().strftime("%I:%M%p") + \
                           " - StartupTree Scheduling"
            elif note.offline_date:
                timestamp = note.offline_date
                date_tag = timestamp.strftime('%a') + ", " + \
                           timestamp.strftime("%B %d %Y") + \
                           " - Offline Meeting"
            else:
                timestamp = note.get_timestamp()
                date_tag = timestamp.strftime('%a') + ", " + \
                           timestamp.strftime("%B %d %Y, %I:%M%p")

            if note.rating:
                # FIXME: rating is getting saved as the star rating + 10 for some reason
                if note.rating > 10:
                    rating = note.rating - 10
                else:
                    rating = note.rating
            else:
                rating = None

            timestamp_edited = note.get_timestamp_edited()
            if timestamp_edited and note.is_edited:
                date_tag_edited = timestamp_edited.strftime('%D %I:%M%p')
            else:
                date_tag_edited = None

            data.append({
                'is_note': True,
                'timestamp': timestamp,
                'date_tag': date_tag,
                'rating': rating,
                'note': note.text,
                'attachments': note_attachments,
                'goals': None,
                'timestamp_edited': timestamp_edited,
                'date_tag_edited': date_tag_edited,
                'note_pk': note.pk,
                'is_by_mentor': note.is_by_mentor
            })
        count = 0
        for gs in goal_sets:
            timestamp = gs.timestamp
            date_tag = timestamp.date().strftime('%a') + ", " + \
                       timestamp.date().strftime("%B %d %Y, %I:%M%p")

            if gs.due_date:
                date_tag += " - Due Date: " + gs.due_date.strftime("%B %d %Y")

            goals_raw = Goal.objects.filter(goal_set=gs).order_by('id')
            goals = []

            for goal in goals_raw:
                count = count + 1
                goals.append({
                    'text': goal.note,
                    'id': goal.id,
                    'completed': goal.completed,
                    'count': count,
                })
            # removes incomplete or complete based on filter values
            for g in list(goals):
                status = g['completed']
                if not show_complete and status:
                    goals.remove(g)
                if not show_incomplete and not status:
                    goals.remove(g)

            if len(goals) > 0:
                data.append({
                    'is_note': False,
                    'timestamp': timestamp,
                    'date_tag': date_tag,
                    'note': None,
                    'attachments': None,
                    'goals': goals
                })

        if start_date and end_date:
            data = list(
                filter(
                    lambda x: (
                            x['timestamp'].date() >= start_date and x['timestamp'].date() <= end_date),
                    data))

        data.sort(key=lambda x: x['timestamp'], reverse=True)

        if len(data) > 0:
            mentors.append({
                'mentor_id': mentorship.mentor.profile.user_id,
                'mentor_url_name': mentorship.mentor.profile.url_name,
                'mentor_image': mentorship.mentor.profile.get_image(),
                'mentor_name': mentorship.mentor.profile.get_full_name(),
                'data': data
            })

    return mentors


@login_required
def view_notes(request, mentee_id):
    try:
        mentor = Mentor.objects.get(profile__user_id=request.user.id, platform__site_id=request.current_site.id)
        request_mentee = False
    except BaseException:
        mentor = None
        request_mentee = True

    mentee = StartupMember.objects.get_user(user=UserEmail.objects.get(id=mentee_id))
    if not (mentor or mentee.user_id == request.user.id or request.is_university):
        raise Http404()
    mentee_dict = sm_to_dict(mentee, simple=True, degree=False)

    # TODO FIXME refactor view_notes_goals_dict so that it takes a set of Mentorships, not MentorshipRequests
    # for purpose of optimizing, and simplifying the code.
    mentorship_reqs = MentorshipRequest.objects \
        .select_related('requested_mentorship',
                        'requested_mentorship__student',
                        'requested_mentorship__mentor__profile') \
        .filter(requested_mentorship__student__user_id=mentee_id,
                is_admin_accepted=True) \
        .order_by('requested_mentorship__mentor__profile__first_name')

    notes_dict = view_notes_goals_dict(mentorship_reqs)

    earliest = sorted(
        mentorship_reqs,
        key=lambda x: x.requested_mentorship.created_on)
    earliest = earliest[0].requested_mentorship.created_on.date() if len(
        earliest) > 0 else None

    dates = []
    for mentor in notes_dict:
        for item in mentor['data']:
            date = item['timestamp'].date().isoformat()
            if date not in dates:
                dates.append(date)

    dates.sort()

    if earliest is None:
        slider_data = None
    else:
        slider_data = {
            'start': earliest.isoformat(),
            'today': timezone.now().date().isoformat(),
            'dates': dates
        }

    user_id, current_site = request.user.id, request.current_site
    is_mentor = Mentor.objects.filter(profile__user_id=request.user.id, platform__site_id=current_site.id).exists()

    return TemplateResponse(
        request,
        'mentorship/mentorship-view-notes.html',
        {
            'mentors_context': notes_dict,
            'mentee': mentee,
            'mentee_dict': mentee_dict,
            'slider_data': json.dumps(slider_data),
            'should_slider': len(dates) > 1,
            'request_mentee': request_mentee,
            'is_mentor': is_mentor,
        }
    )


@login_required
@require_POST
def view_notes_filter(request, mentee_id):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    try:
        mentor = Mentor.objects.get(profile__user_id=request.user.id, platform__site_id=request.current_site.id)
        request_mentee = False
    except BaseException:
        mentor = None
        request_mentee = True

    mentee = StartupMember.objects.get_user(user=UserEmail.objects.get(id=mentee_id))
    if not (mentor or mentee.user_id == request.user.id or request.is_university):
        raise Http404()

    mentor_ids = json.loads(request.POST.get('mentor_ids'))
    start_date = datetime.strptime(
        request.POST.get('start_date'), '%Y-%m-%d').date()
    end_date = datetime.strptime(
        request.POST.get('end_date'), '%Y-%m-%d').date()
    show_goals = request.POST.get('show_goals') == 'true'
    show_notes = request.POST.get('show_notes') == 'true'
    show_complete = request.POST.get('show_complete') == 'true'
    show_incomplete = request.POST.get('show_incomplete') == 'true'

    mentorship_reqs = MentorshipRequest.objects \
        .select_related('requested_mentorship',
                        'requested_mentorship__student',
                        'requested_mentorship__mentor__profile') \
        .filter(requested_mentorship__student__user_id=mentee_id,
                requested_mentorship__mentor__profile__user_id__in=mentor_ids,
                is_admin_accepted=True) \
        .order_by('requested_mentorship__mentor__profile__first_name')

    data = view_notes_goals_dict(mentorship_reqs, start_date, end_date, show_goals,
                                 show_notes, show_complete, show_incomplete)

    template = get_template('mentorship/includes/mentorship-notes-data.html')
    rendered_template = template.render({'mentors': data, 'goals_and_notes': True})

    payload['html'] = rendered_template

    return JsonResponse(payload)


@login_required
@require_POST
def edit_note(request):
    note_content_edited = request.POST.get('note_content_edited')
    note_pk = request.POST.get('note_pk')
    note = Note.objects.select_related('mentorship', 'mentorship__mentor').get(id=int(note_pk))
    current_member = request.current_member

    can_edit = False
    if hasattr(note, 'teamnote'):
        # team note
        note = TeamNote.objects.get(id=note.id)
        if note.creator_id == current_member.id:
            can_edit = True
    else:
        mentorship = note.mentorship
        is_mentor = mentorship.mentor.profile_id == current_member.id
        is_student = mentorship.student_id == current_member.id
        if not is_student and not is_mentor:
            return HttpResponseForbidden()
        if (note.is_by_mentor and is_mentor) or (not note.is_by_mentor and is_student):
            can_edit = True
    if not can_edit:
        return JsonResponse({'msg': 'Edit Not allowed'}, status=403)
    note.text = note_content_edited
    note.is_edited = True
    note.save()

    return JsonResponse({})


@login_required
def view_goals(request):
    mentee = request.current_member

    mentorship_reqs = MentorshipRequest.objects \
        .select_related('requested_mentorship',
                        'requested_mentorship__student',
                        'requested_mentorship__mentor__profile') \
        .filter(requested_mentorship__student__user_id=request.user.id,
                is_admin_accepted=True) \
        .order_by('requested_mentorship__mentor__profile__first_name')

    goals_dict = view_notes_goals_dict(
        mentorship_reqs,
        include_notes=False,
        include_goals=True)

    return TemplateResponse(
        request,
        'mentorship/mentorship-view-goals.html',
        {
            'mentors_context': goals_dict,
            'mentee': mentee
        }
    )


@login_required
def remove_mentee(request, mentee_id):
    mentee = StartupMember.objects.get_user(user=UserEmail.objects.get(id=mentee_id))
    mentor = Mentor.objects.get(profile__user_id=request.user.id)
    try:
        mentorship = Mentorship.objects.get(mentor=mentor, student=mentee)
        mentorship.is_active = False
        mentorship.save()
        # The below will cause total deletion of the data. We may want to rethink about this.
        # for session in MentorshipSession.objects.filter(mentorship=mentorship, datetime__gt=datetime.now()):
        #     session.delete()
        # clerk = UserActivityClerk(request.current_site, request.user)
        # clerk.erase_mentor_unassigned(mentee, mentor)
    except Mentorship.DoesNotExist:
        app_logger.error('Invalid mentorship')
        raise Http404()
    return HttpResponseRedirect('/mentorship/mentor-dashboard')


@login_required
def remove_mentor(request, mentor_id):
    mentee = request.current_member
    mentor = Mentor.objects.get(id=mentor_id)
    try:
        mentorship = Mentorship.objects.filter(mentor=mentor, student=mentee)
        for m in mentorship:
            m.is_active = False
            m.save()
        # The below will cause total deletion of the data. We may want to rethink about this.
        #     for session in MentorshipSession.objects.filter(mentorship=m, datetime__gt=datetime.now()):
        #         session.delete()
        # clerk = UserActivityClerk(request.current_site, mentor.profile.user)
        # clerk.erase_mentor_unassigned(mentee, mentor)
    except Mentorship.DoesNotExist:
        app_logger.error('Invalid mentorship')
        raise Http404()
    return HttpResponseRedirect('/mentorship/mentee-dashboard')


def mentor_office_hours(request, mentor_id, page):
    is_auth = request.user.is_authenticated and not request.is_archived
    uni = request.university

    page_num = int(page)
    men_id = int(mentor_id)

    mentorship_id = None

    if is_auth:
        mentorships = Mentorship.objects.filter(
            mentor_id=mentor_id, student__user_id=request.user.id)
        if mentorships.exists():
            mentorship_id = mentorships[0].id

    mentor = Mentor.objects.select_related('profile').get(id=mentor_id)
    member = mentor.profile
    schedules = MentorSchedule.objects.filter(
        mentor_id=mentor_id).order_by('date', 'start_time')

    sched_count = schedules.count()
    needs_approval = uni.style.require_meeting_approval == UniversityStyle.ALL_MEETINGS or \
        (uni.style.require_meeting_approval == UniversityStyle.OFFICE_HOURS_ONLY and mentor.allow_office_hour)

    schedule_lowest_page = 1 if uni.style.mentor_schedule_start == UniversityStyle.THIS_WEEK else 2

    user_timezone = pytz.timezone(request.session['django_timezone'])
    if request.GET.get('page-title') == 'mentorship-settings':
        if is_cronofy_linked(mentor.profile.user):
            data = get_cronofy_possible_calendar_schedules(mentor, page_num, user_timezone=user_timezone)
        else:
            data = get_possible_calendar_schedules(mentor, page_num, True)
        template = 'mentorship/mentor-settings-officehours.html'
        context = {
            'is_auth': is_auth,
            'schedule': data,
            'schedules': schedules,
            'sched_count': sched_count,
            'mentor_id': men_id,
            'schedule_page': {
                'prev': page_num - 1,
                'curr': page_num,
                'next': page_num + 1},
            'schedule_lowest_page': schedule_lowest_page,
            'mentorship_id': mentorship_id}
    else:
        if is_cronofy_linked(mentor.profile.user):
            data = get_cronofy_possible_calendar_schedules(mentor, page_num, user_timezone=user_timezone)
        else:
            data = get_possible_calendar_schedules(mentor, page_num)
        template = 'mentorship/includes/mentorship-officehours.html'
        context = {
            'is_auth': is_auth,
            'member': member,
            'schedule': data,
            'needs_approval': needs_approval,
            'mentor_id': men_id,
            'mentor': mentor,
            'schedule_page': {
                'prev': page_num - 1,
                'curr': page_num,
                'next': page_num + 1},
            'schedule_lowest_page': schedule_lowest_page,
            'mentorship_id': mentorship_id,
            'calendar': True}

    return TemplateResponse(request, template, context)


def mentorship_id_from_req(request):
    """
    Get the mentorship id from the request.
    If it is not found, return NOT_FOUND.

    @param request
    """
    return int(request.POST.get('mentorship_id', NOT_FOUND))


def url_name_from_req(request):
    return request.POST.get('m_id')


@require_POST
@login_required
def try_schedule_meeting(request):
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}

    if not request.is_valid_platform:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = 'Sorry, you are not permitted to schedule a meeting with this mentor.'
    else:
        payload = prepare_session_scheduling(request.POST, request.session['django_timezone'], request.university,
                                             request.university_style, request.current_member, post_files=request.FILES)

    return JsonResponse(payload)


@login_required
def export_session_to_ics(request, session_id):
    '''
    Generate ics file for adding mentorship session to iCal or Outlook
    API reference: https://icspy.readthedocs.io/en/stable/api.html
    '''
    uni = request.university
    uni_style = UniversityStyle.objects.get(_university=uni)
    session: MentorshipSession = MentorshipSession.objects.select_related('mentorship__mentor',
                                                       'mentorship__mentor__profile',
                                                       'mentorship__student') \
            .get(id=session_id)
    if (session.mentorship.mentor.profile_id != request.current_member.id and \
            session.mentorship.student_id != request.current_member.id) and \
            UniversityStaff.objects.filter(university=uni, admin=request.user).exists() is None:
        raise Http404()

    mentor_name = session.mentorship.mentor.profile.get_full_name()
    mentee_name = session.mentorship.student.get_full_name()
    mentorship_word = uni.style.mentorship_word
    if mentorship_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        mentorship_article = 'an'
    else:
        mentorship_article = 'a'

    is_mentor = False
    is_mentee = False
    if session.mentorship.mentor.profile.user_id == request.user.id:
        is_mentor = True
    else:
        is_mentee = True

    if is_mentor:
        email_cal_title = '{0} Session With {1}'.format(mentorship_word.capitalize(), mentee_name)
    elif is_mentee:
        email_cal_title = '{0} Session With {1}'.format(mentorship_word.capitalize(), mentor_name)
    else:
        email_cal_title = '{0} Session'.format(mentorship_word.capitalize())

    if is_mentor:
        email_cal_desc = uni_style.session_scheduled_mentor_email.replace("<br>", "\n")
        email_cal_desc = render_custom_email_variables(email_cal_desc, session, uni)
        email_cal_desc = BeautifulSoup(email_cal_desc).get_text()
    elif is_mentee:
        email_cal_desc = uni_style.session_scheduled_mentee_email.replace("<br>", "\n")
        email_cal_desc = render_custom_email_variables(email_cal_desc, session, uni)
        email_cal_desc = BeautifulSoup(email_cal_desc).get_text()
    else:
        email_cal_desc = f'{mentor_name} will hold {mentorship_article} {mentorship_word} session with {mentee_name}.'

    type_and_location = session.get_type_and_contact_info()
    if type_and_location:
        email_cal_desc += f'\n\n{type_and_location}'

    cal = Calendar()

    ics_event = icsEvent()
    ics_event.name = email_cal_title

    start = session.datetime
    duration = session.get_duration()
    end = session.datetime + timedelta(minutes=duration)
    ics_event.begin = start
    ics_event.end = end
    ics_event.description = email_cal_desc

    export_location = session.get_export_location()
    if export_location:
        ics_event.location = export_location

    cal.events.add(ics_event)

    filename = email_cal_title.replace(' ', '_')
    response = HttpResponse(str(cal), content_type="text/calendar")
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.ics'
    return response
