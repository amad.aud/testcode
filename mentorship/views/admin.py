# Django imports
import calendar
import datetime
import datetime as dt
from dateutil import parser
import json
import uuid
import xlsxwriter
from io import BytesIO
from operator import itemgetter
from typing import Tuple
from urllib import parse
from emailuser.conf import (
    PROTOCOL,
    EMAIL_INVITATION_URL
)
import django_tables2 as tables
import pytz
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import transaction, IntegrityError
from django.db.models import Count, Q
from django.db.models.functions import ExtractMonth
from django.http import JsonResponse, HttpResponseRedirect, Http404, HttpResponseBadRequest, HttpResponse
from django.shortcuts import redirect
from django.template.loader import get_template
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.views.decorators.http import require_POST
from django_tables2 import RequestConfig
from django_tables2.utils import A  # alias for Accessor
from pytz import common_timezones

# StartupTree imports
from StartupTree.loggers import app_logger
from StartupTree.strings import *
from StartupTree.utils import diggy_paginator, month_delta, get_static_url, QuerySetType, custom_variable_extractor, \
    custom_variable_highlight, get_media_url
from activity.clerk import (
    UserActivityClerk,
)
from branch.modelforms import SignupAdditionalForm, EmailForm, EmailEditorForm, EmailEditorForm2, MentorshipRequestNoti, \
    MentorshipSessionScheduleMentor, MentorshipSessionScheduleMentee, SessionReminderMentee, SessionReminderMentor
from branch.modelforms import TextEditorForm2
# branch imports
from branch.models import StartupMember, Experience, Label, Startup
from branch.serializer.startup import startup_to_dict
from branch.utils.tags import get_manageable_tags_titles_and_objs
from branch.views import _mentor_is_complete_check
# emailuser imports
from emailuser.models import UserEmail
# forms imports
from forms.models import Form
from forms.serializer.forms import custom_member_dict
# mentorship imports
from mentorship.models import (
    Mentorship,
    MentorshipActivity,
    MentorshipSession,
    TeamMentorshipActivity,
    TeamMentorship,
    TeamMentorshipSession,
    Note,
    Goal,
    Mentor,
    MentorSchedule,
    DURATIONS,
    MentorshipFile,
    NoteAttachment,
    MentorshipRequest,
    MentorshipSessionSet,
    MentorScheduleBlackOut,
    MentorshipEmailTemplate,
    SendAllPostMeetingRemindersTimestamp,
)
from mentorship.serializers import mentor_to_dict
from mentorship.strings import *
from mentorship.tasks import (
    mentee_mentorship_decision_noti,
    mentor_mentorship_accepted_noti,
    mentorship_rec_noti,
    notify_team_add_mentorship_feature,
    send_mentor_invitation,
    send_session_followup_to_mentee,
    send_session_followup_to_mentor,
    session_change_noti_mentor,
    session_change_noti_mentee,
    session_request_noti_mentor,
    team_mentorship_notify_venture,
    team_mentorship_notify_mentor, notify_session_change_mentor,
    notify_session_change_mentee, get_template_for_uni, render_custom_email_variables,
)
from mentorship.utils import string_to_bool, localize
from trunk.perms import is_admin_authorized
from mentorship.views import (
    multiple_mentor_schedules__arr,
    single_mentorship_schedule,
    get_meeting_type_string
)
from mentorship.st_cronofy import is_cronofy_linked
from mentorship.views.gcal import make_gcal_event
from mentorship.views.mixins import get_possible_calendar_schedules, \
    set_mentor_settings, get_cronofy_possible_calendar_schedules
# trunk imports
from trunk.decorators import only_university, only_team_mentorship_admin, only_mentorship_admin
from trunk.models import (
    AccessLevel,
    TeamLandingPage, UniversityStyle, CustomVariable, University,
)
from trunk.models import UniversityStyle as uni_style
from trunk.utils.default_mentorship_emails import (
    get_new_session_mentee_email_body,
    get_new_session_mentor_email_body,
    get_session_reminder_mentee_email_body,
    get_session_reminder_mentor_email_body
)
from trunk.views.abstract import UniversityDashboardView
from branch.modelforms import TinyMCEWidget
from trunk.export.utils import make_words
from trunk.modelforms import EventImageForm
from trunk.perms import is_admin_authorized_mentorship
from trunk.tasks import create_user_invite
from trunk.utils import format_datetime_12hr


@only_university
@login_required
def dashboard(request, university):
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    mentors_count = Mentor.objects.filter(platform=university).count()
    # sessions_count = MentorshipSession.objects.filter(mentorship__mentor)
    sessions_count = 0
    return TemplateResponse(
        request,
        'mentorship/admin/dashboard.html',
        {
            'page_name': 'dashboard-mentors',
            'total_mentors': mentors_count,
            'total_sessions': sessions_count,
            'university': university,
        }
    )


@only_university
@login_required
def manage_metrics(request, university):
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    now = timezone.now()
    five_months_back = month_delta(now, -5)
    mentors = Mentor.objects.select_related('profile').filter(platform=university)
    mentor_ids = mentors.values_list('id', flat=True)
    sessions = MentorshipSession.objects \
        .select_related('mentorship') \
        .filter(mentorship__mentor_id__in=mentor_ids,
                datetime__lte=now,
                is_cancelled=False)
    introductions = Mentorship.objects.select_related('mentor').filter(mentor_id__in=mentor_ids,
                                                                       teammentorship__isnull=True).count()

    top_mentors = mentors.filter(mentorship__mentorshipsession__is_cancelled=False) \
                      .annotate(Count('mentorship__mentorshipsession')) \
                      .order_by('-mentorship__mentorshipsession__count')[:5]

    total_hours = 0
    for session in sessions.values('duration'):
        duration = session['duration']
        if duration == 1:
            total_hours += 15
        elif duration == 2:
            total_hours += 30
        elif duration == 3:
            total_hours += 45
        elif duration == 4:
            total_hours += 60
    # duration is in minutes, need to convert to hours
    total_hours = total_hours / 60
    sessions_by_month = sessions.annotate(month=ExtractMonth('datetime')).values('month').annotate(
        count=Count('id')).order_by('month')

    officehours_mentors = Mentor.objects.filter(platform=university, allow_office_hour=True)
    officehours_mentees = Mentorship.objects.select_related('mentor').filter(
        mentor__in=officehours_mentors, teammentorship__isnull=True
    ).distinct('student')
    num_oo_mentors = officehours_mentors.count()
    num_oo_mentees = officehours_mentees.count()

    total_meetings = MentorshipSession.objects.filter(mentorship__mentor__platform=university,
                                                      teammentorshipsession__isnull=True).count()

    months = []
    for i in range(6):
        tmp = (five_months_back.month + i) % 12
        if tmp == 0:
            tmp = 12
        months.append(tmp)
    months = list(map(lambda i: calendar.month_name[i], months))
    style = university.style
    show_pending_meetings = \
        style.require_meeting_approval == uni_style.OFFICE_HOURS_ONLY or \
        style.require_meeting_approval == uni_style.ALL_MEETINGS

    uni_labels, uni_labels_objs = get_manageable_tags_titles_and_objs(request)

    # Metric related
    activities = MentorshipActivity.objects.filter(platform_id=university.id,
                                                   teammentorshipactivity__isnull=True).count()
    connections = Mentorship.objects.filter(mentor__platform_id=university.id,
                                            teammentorship__isnull=True,
                                            accepted_request__is_admin_accepted=True).count()
    mentors = Mentor.objects.filter(platform_id=university.id).count()

    actions = activities + connections

    # Recent Activity
    n = 20
    mentorship_activity = MentorshipActivity.objects.all_platform_regular_activities(university)
    mentorship_activity = MentorshipActivity.objects.order_activities(mentorship_activity)[:n]
    return TemplateResponse(
        request,
        'trunk/manage/mentorship_metrics.html',
        {
            'page_name': 'mentors-metrics',
            'mentorship_title': style.mentorship_word,
            'months': months,
            'top_mentors': top_mentors,
            'num_oo_mentors': num_oo_mentors,
            'num_oo_mentees': num_oo_mentees,
            'total_meetings': total_meetings,
            'sessions_by_month': sessions_by_month,
            'total_hours': total_hours,
            'introductions': introductions,
            'show_pending_meetings': show_pending_meetings,
            'university': university,
            'uni_labels': uni_labels,
            'uni_labels_objs': uni_labels_objs,
            'num_actions': actions,
            'num_connections': connections,
            'num_mentors': mentors,
            'num_hours': total_hours,
            'mentorship_activity': mentorship_activity
        }
    )


def format_onetimes(onetimes):
    ots = {}
    for ot in onetimes:
        key = ot.date
        if key in ots.keys():
            d = ots[key]
            d['starts'].append(ot.start_time)
            d['ends'].append(ot.end_time)
        else:
            ots[key] = {'duration': ot.duration,
                        'starts': [ot.start_time],
                        'ends': [ot.end_time]
                        }
    onetimes = []
    for key in ots.keys():
        onetimes.append(
            {
                'date': key,
                'duration': ots[key]['duration'],
                'times': zip(ots[key]['starts'], ots[key]['ends'])
            })
    return onetimes


def get_last_meeting(mentor, university, team_mentoring=False):
    '''
    Get the last COMPLETED meeting a mentor had/was supposed to have.
    Note if the meeting was cancelled and by whom.
    '''
    now = timezone.now()

    if not team_mentoring:
        last_meeting = MentorshipSession.objects.select_related('mentorship', 'mentorship__student').filter(
            mentorship__mentor_id=mentor.id, is_cancelled=False, datetime__lte=now).order_by(
            '-datetime').first()
    else:
        teams = Startup.objects.filter(teammentorship__mentor=mentor)
        last_meeting = TeamMentorshipSession.objects.select_related('team').filter(
            team__in=teams, is_cancelled=False, datetime__lte=now).order_by(
            '-datetime').first()

    last_meeting_dict = {}

    if last_meeting:
        last_meeting_dict['date_time'] = last_meeting.get_datetime()
        if not team_mentoring:
            last_meeting_dict['student'] = last_meeting.mentorship.student
        else:
            last_meeting_dict['team'] = last_meeting.team

    return last_meeting_dict


def process_mentor(mentor, university, team_mentoring=False, user_timezone=None):
    '''
    Returns an object containing useful information about the mentor
    as needed for the manage mentors page table.
    '''
    data = MentorshipSession.objects.filter(
        mentorship__mentor_id=mentor.id).values('is_cancelled').annotate(
        Count('is_cancelled'))
    schedules = MentorSchedule.objects.filter(
        mentor_id=mentor.id).order_by(
        'day', 'start_time')
    blackout_dates = MentorScheduleBlackOut.objects.filter(
        mentor_id=mentor.id).order_by('start_date')
    onetimes = MentorSchedule.objects.filter(
        mentor_id=mentor.id, date__isnull=False, is_repeating=False).order_by('date')

    if is_cronofy_linked(mentor.profile.user):
        sched = get_cronofy_possible_calendar_schedules(mentor, 1, user_timezone=user_timezone)
    else:
        sched = get_possible_calendar_schedules(mentor, 1, True, user_timezone=user_timezone)

    last_meeting = get_last_meeting(mentor, university, team_mentoring)

    tmp = mentor_to_dict(mentor, university.id)
    rate = 0
    total_sesssions_until_today = MentorshipSession.objects.select_related('mentorship').filter(
        mentorship__mentor_id=mentor.id, datetime__lte=timezone.now()).count()
    total_cancelled = MentorshipSession.objects.select_related('mentorship').filter(
        mentorship__mentor_id=mentor.id, datetime__lte=timezone.now(), is_cancelled=True).count()

    if total_sesssions_until_today > 0:
        rate = round(float(total_cancelled) / total_sesssions_until_today * 100, 1)

    tmp['cancellation_rate'] = rate
    tmp['displayed'] = mentor.profile.bio is not None and \
                       len(mentor.profile.bio) > 0 or \
                       mentor.profile.skill_groups.all().count() > 0 or \
                       mentor.profile.skills.all().count() > 0
    tmp['schedule'] = sched
    tmp['schedules'] = schedules
    tmp['durations'] = DURATIONS
    tmp['onetime'] = onetimes
    tmp['blackout_date'] = blackout_dates
    tmp['last_meeting'] = last_meeting

    tmp['gcal_email'] = mentor.gcal_email

    # contact / default info
    tmp['id'] = mentor.id
    tmp['email'] = mentor.email
    tmp['phone_number'] = mentor.phone
    tmp['skype'] = mentor.skype
    tmp['timezone'] = mentor.timezone
    tmp['default_meeting_type'] = mentor.default_meeting_type
    tmp['default_location'] = mentor.default_location
    tmp['default_duration'] = mentor.default_duration

    return tmp


def make_mentor_table_entry(member, university, user_timezone=None):
    """
    Returns a 'member' dictionary with the necessary keys to
    generate a row of mentor information in the MentorTable.
    """
    member = process_mentor(member, university, user_timezone=user_timezone)
    status = ""

    # To avoid INCOMPLETE status, have at least a bio or some skills.
    if not member['displayed']:
        status = "INCOMPLETE"
    elif not member['is_active']:
        status = "INACTIVE"
    else:
        status = "ACTIVE"

    member['status'] = status
    member['office_hours'] = 'On' if member['allow_office_hour'] else 'Off'
    member['percent_cancelled'] = str(member['cancellation_rate']) + ' %'

    if member['last_meeting']:
        # here
        meeting_datetime = str(member['last_meeting']['date_time'])
        meeting_year = str(member['last_meeting']['date_time'].year)

        today_date = str(datetime.datetime.today().date())
        today_year = str(datetime.datetime.today().date().year)

        if meeting_datetime == today_date:
            formatted_date = datetime.datetime.strftime(member['last_meeting']['date_time'], '%a %b %d, %I:%M%p')
        elif meeting_year == today_year:
            formatted_date = datetime.datetime.strftime(member['last_meeting']['date_time'], '%a, %b %d')
        else:
            formatted_date = datetime.datetime.strftime(member['last_meeting']['date_time'], '%b %d, %Y')

        member['last_meeting'] = formatted_date
    else:
        member['last_meeting'] = 'None'

    return member


# Column for MentorTable which passes the mentor's id
## into the HTML tag for easier data retrieval.
class CheckBoxColumn(tables.Column):
    attrs = {
        'td': {
            'class': 'checkbox',
            'data-id': lambda record: record['id'],  # the member.id
        },
        'th': {'class': 'checkbox'}
    }


# Column for MentorTable which passes mentor data
## into the HTML tag for eaiser data retrieval.
class MentorHTMLColumn(tables.Column):
    attrs = {
        'th': {'class': 'actions'},
        'td': {
            'class': 'cell-buttons actions',
            'data-status': lambda record: record['status'],
            'data-allow_office_hour': lambda record: record['allow_office_hour'],
            'data-is_active': lambda record: record['is_active'],
            'data-last_login': lambda record: record['last_login'],
            'data-name': lambda record: record['name'],
            'data-mid': lambda record: record['id'],
            'data-p_cancelled': lambda record: record['percent_cancelled'],
            'data-last_meeting': lambda record: record['last_meeting'],
            'data-email': lambda record: record['email'],
            'data-phone': lambda record: record['phone_number'],
            'data-skype': lambda record: record['skype'],
            'data-timezone': lambda record: record['timezone'],
            'data-gcal_email': lambda record: record['gcal_email']
        }
    }


# django-tables2: table class to generate table of Mentors
## for Manage Mentors
class MentorTable(tables.Table):
    checkbox = CheckBoxColumn()
    # see templates/mentorship/admin/mentor_table.html to change header names

    name = tables.LinkColumn('member-profile', args=[A('url_name')],
                             attrs={'a': {'target': '_blank'}, 'td': {'class': 'name'}, 'th': {'class': 'name'}}
                             )
    status = tables.Column(attrs={'td': {'class': 'status'}})
    office_hours = tables.Column(attrs={'td': {'class': 'office_hours'}, 'th': {'class': 'office_hours'}})
    last_meeting = tables.Column(attrs={'td': {'class': 'last_meeting'}, 'th': {'class': 'last_meeting'}})
    percent_cancelled = tables.Column(
        attrs={'td': {'class': 'percent_cancelled'}, 'th': {'class': 'percent_cancelled'}})

    actions = MentorHTMLColumn(verbose_name="Actions")

    class Meta:
        orderable = False
        attrs = {
            'id': 'mentor-profiles',
            'class': 'table--mentors'
        }
        template_name = 'mentorship/admin/mentor_table_template.html'


def get_mentor_list(request, university, team_mentoring=False):
    """
    Filters mentors based on activity status.
    Returns a dictionary of the list of mentors, type of mentor, number inactive and active mentors.
    """
    ACTIVE = 'active'

    mentor_type = request.GET.get('mentor', ACTIVE)
    search_string = request.GET.get('query', None)
    sort_direction = request.GET.get('sort_direction')
    is_descending = False
    if sort_direction == 'descending':
        is_descending = True

    if mentor_type == ACTIVE:
        if team_mentoring:
            mentors = Mentor.objects.select_related(
                'profile', 'profile__user', 'platform').filter(
                platform_id=university.id, is_active=True, is_team_mentor=True)
            num_active = mentors.count()
            num_inactive = Mentor.objects.filter(
                platform_id=university.id, is_active=False, is_team_mentor=True).count()
        else:
            mentors = Mentor.objects.select_related(
                'profile', 'profile__user', 'platform').filter(
                platform_id=university.id, is_active=True)
            num_active = mentors.count()
            num_inactive = Mentor.objects.filter(
                platform_id=university.id, is_active=False).count()
    elif mentor_type == 'inactive':
        if team_mentoring:
            mentors = Mentor.objects.select_related(
                'profile', 'profile__user', 'platform').filter(
                platform_id=university.id, is_active=False, is_team_mentor=True)
            num_inactive = len(mentors)
            num_active = Mentor.objects.filter(
                platform_id=university.id, is_active=True, is_team_mentor=True).count()
        else:
            mentors = Mentor.objects.select_related(
                'profile', 'profile__user', 'platform').filter(
                platform_id=university.id, is_active=False)
            num_inactive = len(mentors)
            num_active = Mentor.objects.filter(
                platform_id=university.id, is_active=True).count()
    else:
        if team_mentoring:
            mentors = Mentor.objects.select_related(
                'profile', 'profile__user', 'platform').filter(
                platform_id=university.id, is_team_mentor=True)
            num_inactive = Mentor.objects.filter(
                platform_id=university.id, is_active=False, is_team_mentor=True).count()
            num_active = Mentor.objects.filter(
                platform_id=university.id, is_active=True, is_team_mentor=True).count()
        else:
            mentors = Mentor.objects.select_related(
                'profile', 'profile__user', 'platform').filter(
                platform_id=university.id)
            num_inactive = Mentor.objects.filter(
                platform_id=university.id, is_active=False).count()
            num_active = Mentor.objects.filter(
                platform_id=university.id, is_active=True).count()

    if search_string and len(search_string) > 0:
        mentors = mentors.filter(
            Q(profile__first_name__icontains=search_string) | Q(profile__last_name__icontains=search_string))

    if is_descending:
        mentors = mentors.order_by('-profile__first_name')
    else:
        mentors = mentors.order_by('profile__first_name')

    return {
        'mentors': mentors,
        'mentor_type': mentor_type,
        'num_active': num_active,
        'num_inactive': num_inactive
    }


## RELATES TO LAST_MEETING SORT
def get_date_format(mentor, university):
    last_meeting = get_last_meeting(mentor, university)
    result = 'None'

    if last_meeting:
        meeting_datetime = str(last_meeting['date_time'])
        meeting_year = str(last_meeting['date_time'].year)

        today_date = str(datetime.datetime.today().date())
        today_year = str(datetime.datetime.today().date().year)

        if meeting_datetime == today_date:
            result = '%a %b %d, %I:%M%p'
        elif meeting_year == today_year:
            result = '%a, %b %d'
        else:
            result = '%b %d, %Y'

    return result


def last_meeting_filter(x, date_format):
    if x == 'None':
        return (True, datetime.datetime.max)
    else:
        formatted_date = datetime.datetime.strptime(x, date_format)
        return (False, formatted_date)


def reverse_last_meeting_filter(x):
    if x == 'None':
        return True
    else:
        return False


def last_meeting_sort(mentor, university, direction='ascending'):
    last_meeting = get_last_meeting(mentor, university)
    date_format = get_date_format(mentor, university)
    result = 'None'

    if last_meeting:
        formatted_date = datetime.datetime.strftime(last_meeting['date_time'], date_format)
        result = formatted_date

    if direction == 'descending':
        return reverse_last_meeting_filter(result)

    return last_meeting_filter(result, date_format)


def sort_by_last_meeting(mentor_list, university, direction):
    reversing = False
    if direction == 'descending':
        reversing = True

    # Sort the mentors by last meeting, reversing if prompted.
    tmp = sorted(mentor_list, key=lambda mentor: last_meeting_sort(mentor, university), reverse=reversing)

    # If the sort has been reversed, moved the mentors with 'last_meeting' as 'None' to the bottom.
    if reversing == True:
        tmp = sorted(tmp, key=lambda mentor: last_meeting_sort(mentor, university, direction=direction))

    return tmp


### END OF LAST_MEETING SORT FUNCTIONS

@only_university
@login_required
def get_team_ventures_table(request, university, **kwargs):
    if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    all_ventures = Startup.objects.filter(team_mentorship_participant=True, university=university)

    teams_type = request.GET.get('teams', 'approved')

    if teams_type == 'approved':
        ventures = all_ventures.filter(status=Startup.APPROVED)
    elif teams_type == 'pending':
        ventures = all_ventures.filter(status=Startup.PENDING)
    elif teams_type == 'all':
        ventures = all_ventures
    else:
        app_logger.warning("ERROR: invalid GET parameter for teams")

    venture_table = TeamMentorshipStartups(ventures)

    RequestConfig(request).configure(venture_table)

    return TemplateResponse(
        request,
        'mentorship/admin/team_venture_table.html',
        {
            'venture_table': venture_table,
        }
    )


@only_university
@login_required
def get_mentor_table(request, university, **kwargs):
    '''
    Returns django-tables2 table of desired mentors, based on
    the user's request between Active, Inactive, and Total mentors.
    '''
    if 'team' in kwargs:
        if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
        tmp = get_mentor_list(request, university, True)
    else:
        if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
        tmp = get_mentor_list(request, university)

    mentors = tmp['mentors']
    list_of_mentors = list(mentor for mentor in mentors)
    num_active = tmp['num_active']
    num_inactive = tmp['num_inactive']
    sort_name = request.GET.get('sort_name')
    sort_direction = request.GET.get('sort_direction')
    query = request.GET.get('query')

    if sort_name == 'last_meeting':
        mentors = sort_by_last_meeting(list_of_mentors, university, sort_direction)

    # django pagination
    page = int(request.GET.get('page', 1))
    frag = 10
    p = Paginator(mentors, frag)
    cur_page = p.page(page)
    tmp = []

    user_timezone = pytz.timezone(request.session['django_timezone'])
    for mentor in cur_page.object_list:
        tmp.append(make_mentor_table_entry(mentor, university, user_timezone=user_timezone))
    lst = [None for _ in range((page - 1) * frag)] + tmp + [None for _ in range((p.num_pages - page) * frag)]
    table = MentorTable(lst)
    table.paginate(page=page, per_page=frag)

    if 'team' in kwargs:
        return TemplateResponse(
            request,
            'mentorship/admin/team_mentor_table.html',
            {
                'page_name': 'manage-team-mentors-table',
                'sort_name': sort_name,
                'sort_direction': sort_direction,
                'university': university,
                'mentors': mentors,
                'mentor_table': table,
                'num_active': num_active,
                'num_inactive': num_inactive,
                'mentors_ser': tmp,
                'timezones': pytz.common_timezones,
                'schedule_page': {'prev': 0, 'cur': 1, 'next': 2},
                'cur_page': page,
                'team_mentoring': 'true'
            }
        )
    else:
        return TemplateResponse(
            request,
            'mentorship/admin/mentor_table.html',
            {
                'page_name': 'manage-mentors-table',
                'sort_name': sort_name,
                'sort_direction': sort_direction,
                'university': university,
                'mentors': mentors,
                'mentor_table': table,
                'num_active': num_active,
                'num_inactive': num_inactive,
                'mentors_ser': tmp,
                'timezones': pytz.common_timezones,
                'schedule_page': {'prev': 0, 'cur': 1, 'next': 2},
                'cur_page': page,
                'query': query
            }
        )


@only_university
@login_required
def admin_team_overview(request, university, url_name):
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
                                                  request.access_levels, request.is_startuptree_staff,
                                                  AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        raise PermissionDenied
    venture = Startup.objects.get(url_name=url_name)
    startup = startup_to_dict(venture, request.current_site, aggregate=True, is_admin=True)
    files = MentorshipFile.objects.filter(team=venture)
    return TemplateResponse(
        request,
        'mentorship/admin/dashboard/team_admin_team_overview.html',
        {
            'startup': startup,
            'files': files,
        }
    )


@only_university
@login_required
def admin_new_team_meeting(request, university, mentor_id):
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
                                                  request.access_levels, request.is_startuptree_staff,
                                                  AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        raise PermissionDenied
    mentor = Mentor.objects.select_related('profile').get(id=mentor_id)
    teams = Startup.objects.filter(
        teammentorship__mentor=mentor,
        teammentorship__is_active=True,
        university=university)
    return TemplateResponse(
        request,
        'modals/admin_new_team_meeting/select-team.html',
        {
            'mentor': mentor,
            'teams': teams
        }
    )


@only_university
@login_required
def admin_new_team_meeting_addtl_mentors(request, university):
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
                                                  request.access_levels, request.is_startuptree_staff,
                                                  AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        raise PermissionDenied
    selected_mentor_id = request.POST.get("selected_mentor_id")
    selected_team_id = request.POST.get("selected_team_id")

    selected_mentor = Mentor.objects.select_related('profile').get(id=selected_mentor_id)
    selected_team = Startup.objects.get(url_name=selected_team_id)

    additional_mentors = Mentor.objects.select_related('profile').filter(
        mentorship__teammentorship__team=selected_team,
        mentorship__teammentorship__is_active=True,
        profile__universities=university).exclude(
        id=selected_mentor.id)

    return TemplateResponse(
        request,
        'modals/admin_new_team_meeting/addtl-mentors.html',
        {
            'selected_mentor': selected_mentor,
            'selected_team': selected_team,
            'additional_mentors': additional_mentors
        }
    )


@only_university
@login_required
def admin_new_team_meeting_select_time(request, university):
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
                                                  request.access_levels, request.is_startuptree_staff,
                                                  AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        raise PermissionDenied
    mentor_list = json.loads(request.POST.get('mentor_list'))

    times = multiple_mentor_schedules__arr(request, mentor_list)
    if times:
        min_date = times[0]['date']
        max_date = times[-1]['date']
    else:
        min_date = ""
        max_date = ""

    return JsonResponse({
        "sched_days": times,
        "min_date": min_date,  # YYYY-MM-DD
        "max_date": max_date
    })


@only_university
@login_required
def admin_new_team_meeting_review_meeting(request, university):
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
                                                  request.access_levels, request.is_startuptree_staff,
                                                  AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        raise PermissionDenied
    mentor_list = json.loads(request.POST.get('mentor_list'))
    selected_team_id = request.POST.get("selected_team_id")
    timeslot = json.loads(request.POST.get('timeslot'))

    mentors = Mentor.objects.select_related('profile').filter(profile__url_name__in=mentor_list)
    team = Startup.objects.get(url_name=selected_team_id)

    return TemplateResponse(
        request,
        'modals/admin_new_team_meeting/review-meeting.html',
        {
            "mentors": mentors,
            "team": team,
            "timeslot": timeslot
        }
    )


@only_university
@login_required
def admin_new_meeting(request, university, mentor_id):
    is_admin_mentorship = is_admin_authorized(request.is_university, request.is_super,
                                              request.access_levels, request.is_startuptree_staff,
                                              AccessLevel.MENTORSHIP)
    if not is_admin_mentorship:
        raise PermissionDenied
    mentor = Mentor.objects.select_related('profile').get(id=mentor_id)
    mentees = StartupMember.objects.filter(
        mentorship__mentor=mentor,
        mentorship__is_active=True,
        universities=university)
    return TemplateResponse(
        request,
        'modals/admin_new_meeting/select-mentee.html',
        {
            'mentor': mentor,
            'mentees': mentees
        }
    )


@only_university
@login_required
def admin_new_meeting_select_time(request, university):
    is_admin_mentorship = is_admin_authorized(request.is_university, request.is_super,
                                              request.access_levels, request.is_startuptree_staff,
                                              AccessLevel.MENTORSHIP)
    if not is_admin_mentorship:
        raise PermissionDenied
    mentor_id = json.loads(request.POST.get('mentor_id'))

    times = single_mentorship_schedule(request, mentor_id)

    return JsonResponse({
        "slot_data_list": times
    })


@only_university
@login_required
def admin_new_meeting_review_meeting(request, university):
    is_admin_mentorship = is_admin_authorized(request.is_university, request.is_super,
                                              request.access_levels, request.is_startuptree_staff,
                                              AccessLevel.MENTORSHIP)
    if not is_admin_mentorship:
        raise PermissionDenied
    selected_mentor_id = request.POST.get('selected_mentor_id')
    selected_mentee_id = request.POST.get("selected_mentee_id")
    timeslot = json.loads(request.POST.get('timeslot'))

    timeslot["meeting_type_str"] = get_meeting_type_string(timeslot["meeting_type"])

    mentor = Mentor.objects.get(id=selected_mentor_id)
    mentee = StartupMember.objects.get(id=selected_mentee_id)

    return TemplateResponse(
        request,
        'modals/admin_new_meeting/review_meeting.html',
        {
            "mentor": mentor,
            "mentee": mentee,
            "timeslot": timeslot
        }
    )


# class UpperColumn(tables.Column):
#     def render(self, value):
#         return value.upper()
#
# class ItalicColumn(tables.Column):
#     def render(self, value):
#         return format_html('<em>{}</em>', value)

class TeamMentorshipStartups(tables.Table):
    # row_number = tables.Column(empty_values=())

    # def __init__(self, *args, **kwargs):
    #     super(TeamMentorshipStartups, self).__init__(*args, **kwargs)
    #     self.counter = itertools.count()
    #
    # def render_row_number(self):
    #     return 'Row %d' % next(self.counter)

    url_name = tables.Column(verbose_name="Tools")

    def render_url_name(self, value):
        return format_html(
            "<a class='btn admin-team-overview-button' \
            data-url_name='{}'>\
            <i class='uk-icon-list'></i>&nbsp; Overview\
            </a>\
            \
            <a class='btn--default manage-button' \
            href='/venture/{}/' target='_blank'>\
            <i class='uk-icon-pencil'></i>&nbsp; Edit\
            </a>",
            value, value
        )

    class Meta:
        model = Startup
        fields = ('name', 'status',
                  'team_founder_name', 'team_founder_email',
                  'created_on', 'url_name')
        template_name = 'django_tables2/bootstrap.html'
        attrs = {
            'id': 'venture-profiles-table',
            'class': 'table--mentors'
        }


def get_default_email_subject_and_body(university, team_mentoring):
    style = university.style

    mentor_word = style.mentor_word
    if team_mentoring:
        mentor_word = 'a team ' + mentor_word
    else:
        if mentor_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
            mentor_word = 'an ' + mentor_word
        else:
            mentor_word = 'a ' + mentor_word

    mentee_word = style.mentee_word
    if team_mentoring:
        mentee_word = 'a team or ' + mentee_word
    else:
        if mentee_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
            mentee_word = 'an ' + mentee_word
        else:
            mentee_word = 'a ' + mentee_word

    if university.program_name:
        program_name = university.program_name
    else:
        program_name = university.name

    default_subject = "{0} is inviting you to be {1}".format(
        program_name, mentor_word)

    default_invitation = "We would like to invite you to be {0} for the \
            entrepreneurs at {1}.<br><br>\
            Please fill out your Availability during the sign up process \
            to be assigned {2}.<br><br>\
            We thank you for serving as a valuable resource for your community." \
        .format(mentor_word, program_name, mentee_word)

    return default_subject, default_invitation


@only_university
@login_required
def manage_mentors(request, university, **kwargs):
    """
    Returns template for Manage Mentors admin page, and
    handles POST request when admin alters a mentor's Availability
    and Office Hours.
    """
    team_mentoring = False
    if 'team' in kwargs and kwargs['team']:
        if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
        team_mentoring = True
    else:
        if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
    page = request.GET.get('page')

    if MentorshipEmailTemplate.objects.filter(platform=university).exists():
        invite_email_template = MentorshipEmailTemplate.objects.filter(platform=university).first()
    else:
        invite_email_template = MentorshipEmailTemplate.objects.create(platform=university)
    email_subject = invite_email_template.subject
    email_body = invite_email_template.email_body
    default_subject, default_invitation = get_default_email_subject_and_body(university, team_mentoring)

    if email_subject is None or email_subject == '':
        invitation_email_subject = default_subject
    else:
        invitation_email_subject = email_subject
    if email_body is None or email_body == '':
        invitation_email_body = default_invitation
    else:
        invitation_email_body = email_body

    if request.method == 'POST':
        checkbox_setting = request.POST.get('checkboxSetting')
        action = request.POST.get('action')
        if action == 'resend':
            mentor_id = int(request.POST.get('mid'))
            mentor = Mentor.objects.select_related('profile', 'profile__user').get(id=mentor_id)
            _mentor_is_complete_check(mentor.profile, university.id)
            if university.short_name == 'columbia':
                mentor.profile.is_public = False
                mentor.profile.save()
            send_mentor_invitation.delay(
                mentor.profile.user.id,
                mentor.profile.first_name,
                university.site.id,
                request.session.get('user_name'),
                university.name,
                university.program_name,
                False,
                False
            )
            _mentor_is_complete_check(mentor.profile, university.id)
            return JsonResponse({})

        if checkbox_setting == 'on':
            # group user action with checkbxes
            mentor_ids = json.loads(request.POST.get('mids'))

            for mentor_id in mentor_ids:
                mentor = Mentor.objects.select_related('profile', 'profile__user').get(id=int(mentor_id))

                if action == 'enable':
                    mentor.is_active = True
                elif action == 'office':
                    mentor.allow_office_hour = True
                elif action == 'office-off':
                    mentor.allow_office_hour = False
                else:
                    # if not active, no office hour too.
                    mentor.is_active = False
                    mentor.allow_office_hour = False

                mentor.save()
            return JsonResponse({})

        # single user action in modal
        else:
            # modal toggle action
            mentor_id = request.POST.get('mid')
            activity_action = request.POST.get('activity_action')
            office_hours_action = request.POST.get('office_hour_action')
            calendar_action = request.POST.get('calendar_action')
            gcal_email = request.POST.get('mentor_google')

            email = request.POST.get('mentor_email')
            phone = request.POST.get('mentor_phone')
            skype = request.POST.get('mentor_skype')
            timezone = request.POST.get('mentor_timezone')

            mentor = Mentor.objects.select_related('profile', 'profile__user').get(id=mentor_id)

            if activity_action == 'enable':
                mentor.is_active = True
            else:
                # if not active, no office hour too.
                mentor.is_active = False
                mentor.allow_office_hour = False

            if office_hours_action == 'office':
                mentor.allow_office_hour = True
            else:
                mentor.allow_office_hour = False

            if calendar_action == 'enable':
                mentor.gcal_email = gcal_email
            else:
                mentor.gcal_email = None

            mentor.email = email
            mentor.phone = phone
            mentor.skype = skype
            mentor.set_timezone(timezone)

            mentor.save()
            return JsonResponse({})

    # FIXME is `tmp` used at all?
    tmp = get_mentor_list(request, university, team_mentoring)
    mentors = tmp['mentors']
    num_active = tmp['num_active']
    num_inactive = tmp['num_inactive']
    mentor_type = tmp['mentor_type']

    if team_mentoring:
        all_ventures = Startup.objects.filter(team_mentorship_participant=True, university=university)
        num_team_approved = all_ventures.filter(status=Startup.APPROVED).count()
        num_team_pending = all_ventures.filter(status=Startup.PENDING).count()

        teams_type = request.GET.get('teams', 'approved')

        if teams_type == 'approved':
            ventures = all_ventures.filter(status=Startup.APPROVED)
        elif teams_type == 'pending':
            ventures = all_ventures.filter(status=Startup.PENDING)
        elif teams_type == 'all':
            ventures = all_ventures
        else:
            app_logger.warning("ERROR: invalid GET parameter for teams")
            return HttpResponseBadRequest()

        venture_table = TeamMentorshipStartups(ventures)
        RequestConfig(request).configure(venture_table)

        return TemplateResponse(
            request,
            'mentorship/admin/team_mentor_dash.html',
            {
                'page_name': 'manage-team-mentors',
                'mentors': mentors,
                'university': university,
                'mentor_type': mentor_type,
                'num_active': num_active,
                'availab_mentors': True,
                'num_inactive': num_inactive,
                'timezones': common_timezones,
                'venture_table': venture_table,
                'num_team_approved': num_team_approved,
                'num_team_pending': num_team_pending,
                'invitation_email_subject': invitation_email_subject,
                'invitation_email_body': invitation_email_body,

                # STARTUP MANAGING
                # 'startups': startup_table['startups'],
                # 'industries': startup_table['industries'],
                # 'active_industry': startup_table['active_industry'],
                # 'total_startups': startup_table['total_startups'],
                # 'curr_pages': startup_table['curr_pages'],
                # 'prev_page': startup_table['prev_page'],
                # 'next_page': startup_table['next_page'],
                # 'curr_page': 1,
                # 'count_called': 1,
                # 'name_order': 'ascend',
                # 'industry_order': 'none',
                # 'funding_order': 'none',
                # 'exit_order': 'none',
                # 'labels': startup_table['labels']
            }
        )
    else:
        return TemplateResponse(
            request,
            'mentorship/admin/manage_mentors.html',
            {
                'page_name': 'manage-mentors',
                'mentors': mentors,
                'university': university,
                'mentor_type': mentor_type,
                'num_active': num_active,
                'availab_mentors': True,
                'num_inactive': num_inactive,
                'timezones': common_timezones,
                'invitation_email_subject': invitation_email_subject,
                'invitation_email_body': invitation_email_body,
            }
        )


class ManageMentorsDashTable(tables.Table):
    """
    Django-tables2 table class for manage mentorships to
    display in the admin mentorship dashboard.
    """
    name = tables.LinkColumn('member-profile', args=[A('url_name')],
                             attrs={'a': {'target': '_blank'}}
                             )
    last_meeting = tables.Column()

    # actions = tables.Column(verbose_name="Actions",
    #                             attrs = {
    #                             'td' : {
    #                                 'class': 'cell-buttons mm-dash',
    #                                 'data-name' : lambda record : record['name'],
    #                                 'data-mid' : lambda record : record['id']
    #                             },
    #                             'th' : {
    #                                 'class' : 'manage-actions-header'
    #                             }
    #                         })

    class Meta:
        orderable = False
        attrs = {
            'class': 'table--mentors mentors-dashboard',
        }
        template_name = 'mentorship/admin/mentor_table_template.html'


def make_manage_mentors_entry(member, university, team_mentoring=False, timezone=None):
    """
    Returns a 'member' dictionary for the ManageMentorsDash
    dashboard table.
    """
    member = process_mentor(member, university, team_mentoring, user_timezone=timezone)

    if member['last_meeting']:
        member['last_meeting'] = member['last_meeting']['date_time']
    else:
        member['last_meeting'] = None

    return member


def get_formatted_meeting_date(mentor):
    """
    For the Manage Mentors full table and modified table in the Admin Dashboard.
    Returns the meeting date formatted based on the current date:
    If the meeting date is today, display as a time.
    If '' is not today but from this year, display as Day of week Month, Year.
    If '' is not this year, display as Month Date, Year.
    """
    meeting_datetime = str(mentor['last_meeting'])
    meeting_year = str(mentor['last_meeting'].year)

    today_date = str(datetime.datetime.today().date())
    today_year = str(datetime.datetime.today().date().year)

    if meeting_datetime == today_date:
        formatted_date = datetime.datetime.strftime(mentor['last_meeting'], '%a %b %d, %I:%M%p')
    elif meeting_year == today_year:
        formatted_date = datetime.datetime.strftime(mentor['last_meeting'], '%a, %b %d')
    else:
        formatted_date = datetime.datetime.strftime(mentor['last_meeting'], '%b %d, %Y')

    return formatted_date


@only_university
@login_required
def get_manage_mentors_table(request, university, team_mentoring=False):
    '''
    Returns manage mentors table section on single mentorship and team mentorship
    admin dashboards
    '''

    if not team_mentoring:
        access_level = AccessLevel.MENTORSHIP
    else:
        access_level = AccessLevel.TEAM_MENTORSHIP

    if access_level not in request.access_levels and not request.is_super:
        raise Http404()

    tmp = get_mentor_list(request, university, team_mentoring)
    mentors = tmp['mentors']

    # django pagination
    page = int(request.GET.get('page', 1))
    frag = 10
    p = Paginator(mentors, frag)
    cur_page = p.page(page)
    tmp = []
    timezone = pytz.timezone(request.session['django_timezone'])
    for mentor in cur_page.object_list:
        tmp.append(make_manage_mentors_entry(mentor, university, team_mentoring, timezone=timezone))
    lst = [None for _ in range((page - 1) * frag)] + tmp + [None for _ in range((p.num_pages - page) * frag)]
    dash_table = ManageMentorsDashTable(lst)
    dash_table.paginate(page=page, per_page=frag)

    return TemplateResponse(
        request,
        'mentorship/admin/dashboard/manage_mentors_table_dash.html',
        {
            'table': dash_table,
            'team_mentoring': team_mentoring
        }
    )


class PendingMentorshipsTable(tables.Table):
    """
    Django-tables2 table class for pending mentorships to
    display in the admin mentorship dashboard.
    """
    mentee = tables.LinkColumn('member-profile', text=lambda record: record['mentee'], args=[A('mentee.url_name')],
                               attrs={'a': {'target': '_blank'}}
                               )
    mentor = tables.LinkColumn('member-profile', text=lambda record: record['mentor'], args=[A('mentor.url_name')],
                               attrs={'a': {'target': '_blank'}}
                               )
    venture_proj = tables.Column(verbose_name='Venture/Project')
    description = tables.Column()
    status = tables.Column()
    timestamp = tables.Column()

    # actions column will have the approve and decline buttons.
    actions = tables.Column(verbose_name="Actions",
                            attrs={
                                'td': {
                                    'class': 'cell-buttons pending-mentorship-dash',
                                    'data-rid': lambda record: record['request_id'],
                                    'data-status': lambda record: record['status']
                                },
                                'th': {
                                    'id': 'mentorship-actions-header'
                                }
                            })

    class Meta:
        orderable = False
        attrs = {
            'id': 'pending-mentorships',
            'class': 'table--pending'
        }
        template_name = 'mentorship/admin/mentor_table_template.html'


class PendingMeetingsTable(tables.Table):
    """
    Django-tables2 table class for pending meetings to
    display in the admin mentorship dashboard.
    """
    mentee = tables.LinkColumn('member-profile', text=lambda record: record['mentee'], args=[A('mentee.url_name')],
                               attrs={'a': {'target': '_blank'}}
                               )
    mentor = tables.LinkColumn('member-profile', text=lambda record: record['mentor'], args=[A('mentor.url_name')],
                               attrs={'a': {'target': '_blank'}}
                               )
    datetime = tables.Column(verbose_name='Meeting Date/Time')
    type = tables.Column(verbose_name='Meeting Type')
    status = tables.Column()
    timestamp = tables.Column(verbose_name='Date/Time Scheduled')

    # actions column will have the approve and decline buttons.
    actions = tables.Column(verbose_name="Actions",
                            attrs={
                                'td': {
                                    'class': 'cell-buttons pending-meeting-dash',
                                    'data-rid': lambda record: record['request_id'],
                                    'data-status': lambda record: record['status']
                                },
                                'th': {
                                    'id': 'meeting-actions-header'
                                }
                            })

    class Meta:
        orderable = False
        attrs = {
            'id': 'pending-meetings',
            'class': 'table--pending'
        }
        template_name = 'mentorship/admin/mentor_table_template.html'


def make_pending_connection_table_entry(queue, university):
    """
    Make single entry for the pending mentorships table.
    """
    mentee = queue.requested_mentorship.student
    mentor = queue.requested_mentorship.mentor.profile
    venture_proj = queue.startup
    if not venture_proj:
        venture_proj = "None yet"

    description = queue.pitch
    if not description:
        description = "N/A"

    status = queue.is_admin_accepted
    if status is None:
        status = "Open"
    elif not status:
        status = "Declined"
    else:
        status = "Approved"

    timestamp = localize(queue.created_on, university.style.timezone)
    request_date = str(datetime.datetime.strftime(timestamp, '%Y-%m-%d'))
    request_year = str(timestamp.year)

    today_date = str(datetime.datetime.today().date())
    today_year = str(datetime.datetime.today().date().year)

    if request_date == today_date:
        timestamp = datetime.datetime.strftime(timestamp, '%-I:%M %p')
    elif request_year == today_year:
        timestamp = datetime.datetime.strftime(timestamp, '%a, %b %d')
    else:
        timestamp = datetime.datetime.strftime(timestamp, '%b %d, %Y')

    request_id = queue.id

    pending_request = {
        'mentee': mentee,
        'mentor': mentor,
        'venture_proj': venture_proj,
        'description': description,
        'status': status,
        'timestamp': timestamp,
        'request_id': request_id
    }

    return pending_request


def make_pending_meeting_table_entry(queue, university, tzname):
    """
    Make single entry for the pending meetings table.
    """
    mentee = queue.mentorship.student
    mentor = queue.mentorship.mentor.profile
    session_datetime = localize(queue.datetime, queue.mentorship.mentor.timezone)
    type = queue.meeting_type
    if type == 1:
        type = "Phone"
    elif type == 3:
        type = "Video Call"
    elif type == 4:
        type = "In Person"

    status = queue.is_admin_accepted
    if status is None:
        status = "Open"
    elif not status:
        status = "Declined"
    else:
        status = "Approved"

    timestamp = queue.timestamp_of_scheduling
    if timestamp is not None:
        new_timestamp = localize(timestamp, tzname)

        request_date = str(new_timestamp.strftime('%Y-%m-%d'))
        request_year = str(new_timestamp.year)

        today_date = str(session_datetime.today().date())
        today_year = str(session_datetime.today().date().year)

        if request_date == today_date:
            timestamp = new_timestamp.strftime('%-I:%M %p')
        elif request_year == today_year:
            timestamp = new_timestamp.strftime('%a, %b %d')
        else:
            timestamp = new_timestamp.strftime('%b %d, %Y')
    else:
        timestamp = 'N/A'

    request_id = queue.id

    pending_request = {
        'mentee': mentee,
        'mentor': mentor,
        'datetime': session_datetime,
        'type': type,
        'status': status,
        'timestamp': timestamp,
        'request_id': request_id
    }

    return pending_request


def filter_mentorship_requests(request, request_queues):
    """
    Filters mentorship request statuses of all, open, approved, or declined.
    """
    result = request_queues
    filter_status = request.GET.get("status")
    if filter_status == 'all' or filter_status is None:
        pass
    elif filter_status == 'o':
        result = request_queues.filter(is_admin_accepted__isnull=True)
    elif filter_status == 'a':
        result = request_queues.filter(is_admin_accepted=True)
    elif filter_status == 'd':
        result = request_queues.filter(is_admin_accepted=False)

    return result


def filter_meeting_requests(request, request_queues):
    """
    Filters meeting request statuses of all, open, approved, or declined.
    """
    result = request_queues
    filter_status = request.GET.get("status")
    if filter_status == 'all' or filter_status is None:
        pass
    elif filter_status == 'o':
        result = request_queues.filter(is_admin_accepted__isnull=True)
    elif filter_status == 'a':
        result = request_queues.filter(is_admin_accepted=True)
    elif filter_status == 'd':
        result = request_queues.filter(is_admin_accepted=False)

    return result, filter_status


@only_university
@login_required
def get_pending_connection_table(request, university):
    '''
    Returns django-tables2 table of pending mentorships to be approved by admin.
    '''
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    context = _mentorship_approval_context(request, university, None)
    request_queues = context['request_queues']
    frag = 10

    # for all_request_table
    all_requests = []
    all_request_obj = request_queues.exclude(
        Q(is_admin_accepted__isnull=True) & Q(Q(via_meeting__isnull=True) | Q(via_meeting=False)))
    all_req_page = int(request.GET.get('allreqpage', 1))
    p = Paginator(all_request_obj, frag)
    cur_page = p.page(all_req_page)
    for queue in cur_page.object_list:
        all_requests.append(make_pending_connection_table_entry(queue, university))
    lst = [None for _ in range((all_req_page - 1) * frag)] + all_requests + [None for _ in
                                                                             range((p.num_pages - all_req_page) * frag)]
    all_requests_table = PendingMentorshipsTable(lst, prefix='allreq')
    all_requests_table.paginate(page=all_req_page, per_page=frag)

    # for open_request_table
    open_requests = []
    open_requests_obj = request_queues.filter(
        Q(is_admin_accepted__isnull=True) & Q(Q(via_meeting__isnull=True) | Q(via_meeting=False)))
    open_req_page = int(request.GET.get('openreqpage', 1))
    p = Paginator(open_requests_obj, frag)
    cur_page = p.page(open_req_page)
    for queue in cur_page.object_list:
        open_requests.append(make_pending_connection_table_entry(queue, university))
    lst = [None for _ in range((open_req_page - 1) * frag)] + open_requests + [None for _ in range(
        (p.num_pages - open_req_page) * frag)]
    open_requests_table = PendingMentorshipsTable(lst, prefix='openreq')
    open_requests_table.paginate(page=open_req_page, per_page=frag)

    view_all = request.GET.get("viewAll", "open")

    # open_count = len(open_requests_table)
    # all_count = len(all_requests_table)

    return TemplateResponse(
        request,
        'mentorship/admin/dashboard/pending_connection_table.html',
        {
            'page_name': 'pending-table',
            'all_table': all_requests_table,
            'open_table': open_requests_table,
            'view_all': view_all
        })


@only_university
@login_required
def get_pending_meeting_table(request, university):
    '''
    Returns django-tables2 table of pending mentorship meetings to be approved by admin.
    '''
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    context = _meeting_approval_context(request, university, None)
    request_queues = context['request_queues']

    frag = 10
    tzname = request.session.get('django_timezone')
    # for all_request_table
    all_requests = []
    all_request_obj = request_queues.exclude(is_admin_accepted__isnull=True)
    all_req_page = int(request.GET.get('allreqpage', 1))
    p = Paginator(all_request_obj, frag)
    cur_page = p.page(all_req_page)
    for queue in cur_page.object_list:
        all_requests.append(make_pending_meeting_table_entry(queue, university, tzname))
    lst = [None for _ in range((all_req_page - 1) * frag)] + all_requests + [None for _ in
                                                                             range((p.num_pages - all_req_page) * frag)]
    all_requests_table = PendingMeetingsTable(lst, prefix='allreq')
    all_requests_table.paginate(page=all_req_page, per_page=frag)

    # for open_request_table
    open_requests = []
    open_requests_obj = request_queues.filter(is_admin_accepted__isnull=True)
    open_req_page = int(request.GET.get('openreqpage', 1))
    p = Paginator(open_requests_obj, frag)
    cur_page = p.page(open_req_page)
    for queue in cur_page.object_list:
        open_requests.append(make_pending_meeting_table_entry(queue, university, tzname))
    lst = [None for _ in range((open_req_page - 1) * frag)] + open_requests + [None for _ in range(
        (p.num_pages - open_req_page) * frag)]
    open_requests_table = PendingMeetingsTable(lst, prefix='openreq')
    open_requests_table.paginate(page=open_req_page, per_page=frag)

    view_all = request.GET.get("viewAll", "open")

    return TemplateResponse(
        request,
        'mentorship/admin/dashboard/pending_meeting_table.html',
        {
            'page_name': 'pending-table',
            'all_table': all_requests_table,
            'open_table': open_requests_table,
            'view_all': view_all
        })


def request_queues_helper(uni):
    return MentorshipRequest.objects \
        .select_related("requested_mentorship__student",
                        "startup",
                        "requested_mentorship__mentor__profile") \
        .filter(
        requested_mentorship__mentor__platform_id=uni.id,
        # is_admin_accepted__isnull=True
    ).order_by('-created_on')


def request_helper(request_id):
    return MentorshipRequest.objects \
        .select_related("requested_mentorship__student",
                        "startup",
                        "requested_mentorship__mentor__profile") \
        .get(id=int(request_id))


def _mentorship_approval_context(request, university, req_id):
    # Get the requests for this university and order them by when they were created.
    request_queues = request_queues_helper(university)
    request_queues = filter_mentorship_requests(request, request_queues)

    req = None
    req_custom = None
    if req_id:
        # A request ID has been passed in. Get the associated MentorshipRequest.
        req = request_helper(req_id)
        req_custom = custom_member_dict(
            req.requested_mentorship.student, university.id, Form.MENTORREQUEST, mentor=req.requested_mentorship.mentor)
    context = {
        'university': university,
        'request': req,
        'request_queues': request_queues,
        'mentor_request_custom': req_custom}

    return context


@only_university
@login_required
def mentorship_approval(request, university, req_id=None):
    """
    Returns template of all mentorship requests: open, approved, and declined.
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    context = _mentorship_approval_context(request, university, req_id)
    context['page_name'] = 'mentorship-approval'

    req = context['request']
    if req_id:
        platform_id = req.requested_mentorship.mentor.platform_id
        if not mentor_on_same_platform(platform_id, university.id):
            # You cannot request a mentor from another university.
            return HttpResponseBadRequest()

    return TemplateResponse(
        request,
        'mentorship/admin/mentorship_approval.html',
        context
    )


def mentor_on_same_platform(platform_id, uni_id):
    return platform_id == uni_id


def meeting_set_request_queues(university):
    return MentorshipSessionSet.objects \
        .select_related("mentorship__mentor") \
        .prefetch_related("sessions") \
        .filter(
        mentorship__mentor__platform_id=university.id,
        decided__isnull=True) \
        .values_list('sessions', flat=True)


def meeting_request_queues(university):
    return MentorshipSession.objects \
        .select_related("mentorship__student",
                        "mentorship__mentor__profile") \
        .filter(
        mentorship__mentor__platform_id=university.id) \
        .exclude(
        id__in=meeting_set_request_queues(university))


def meeting_request_helper(request_id):
    return MentorshipSession.objects \
        .select_related("mentorship__student",
                        "mentorship__mentor__profile") \
        .get(id=int(request_id))


def _meeting_approval_context(request, university, req_id):
    request_queues = meeting_request_queues(university)

    # backwards compatibility for requests without timestamps
    sort_by_timestamp = True
    for session in request_queues:
        if session.timestamp_of_scheduling is None:
            sort_by_timestamp = False
            break

    if sort_by_timestamp:
        request_queues = request_queues.order_by('-timestamp_of_scheduling')
    else:
        request_queues = request_queues.order_by('-datetime')

    if university.style.require_meeting_approval == uni_style.OFFICE_HOURS_ONLY:
        request_queues = request_queues.filter(mentorship__mentor__allow_office_hour=True)

    request_queues, filter_status = filter_meeting_requests(request, request_queues)

    req = None
    req_custom = None
    if req_id:
        req = meeting_request_helper(req_id)
        req_custom = custom_member_dict(
            req.mentorship.student, university.id, Form.MENTORMEETING, mentor=req.mentorship.mentor, meeting=req)
    context = {
        'university': university,
        'request': req,
        'request_queues': request_queues,
        'mentor_meeting_custom': req_custom,
        'filter_status': filter_status
    }
    return context


@only_university
@login_required
def meeting_approval(request, university, req_id=None):
    """
    Returns template of all meeting requests: open, approved, and declined.
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    context = _meeting_approval_context(request, university, req_id)
    context['page_name'] = 'meeting-approval'

    req = context['request']
    if req_id:
        platform_id = req.mentorship.mentor.platform_id
        if not mentor_on_same_platform(platform_id, university.id):
            return HttpResponseBadRequest()
    return TemplateResponse(
        request,
        'mentorship/admin/meeting_approval.html',
        context
    )


def decision_info(request):
    req_id = request.POST.get("r_id")
    accepted = request.POST.get("is_accept")
    reason = request.POST.get("reason", None)
    has_reason = reason is not None and len(reason.strip()) > 0

    return req_id, accepted, reason, has_reason


@require_POST
@only_university
@login_required
def decide_mentorship_request(request, university):
    """
    Completes POST request for admin to approve or decline a mentorship request.
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    request_id, is_accept, reason, has_reason = decision_info(request)

    try:
        request_id = int(request_id)

        # Convert is_accept to boolean.
        is_accept = string_to_bool(is_accept)
        if is_accept is None:
            raise ValueError('is_accept could not be converted from string to boolean')

    except ValueError:
        # Invalid request because is_accept was not 'true' or 'false'
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = INVALID_REQUEST
        app_logger.info(
            'invalid parameters: {0}, {1}'.format(
                request_id, is_accept))
        return JsonResponse(payload)

    request_exists = MentorshipRequest.objects.request_exists(request_id, university.id)

    if not request_exists:
        payload[PAYLOAD_STATUS] = INVALID_PERMISSION_CODE
        payload[PAYLOAD_MSG] = INVALID_PERMISSION
        app_logger.info('requested id does not exist - {0}'.format(request_id))
        return JsonResponse(payload)

    # Get the mentorship request because we know that it exists.
    m_request = MentorshipRequest.objects.get(id=request_id)

    # Get the mentorship associated with that request.
    mentorship = m_request.requested_mentorship

    if is_accept:
        # The request has been accepted, mark it as so.
        MentorshipRequest.objects.mentorship_accept(m_request)
    else:
        # The request has been declined.
        m_request.is_admin_accepted = False

        if has_reason:
            # Save the reject reason to the request
            m_request.admin_reject_reason = reason
        else:
            # Reject reason is required.
            payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
            payload[PAYLOAD_MSG] = REQUEST_REJECT_REQUIRE_REASON
            app_logger.info('no reject reason')
            return JsonResponse(payload)

        # Register the mentorship as not active.
        MentorshipRequest.objects.mentorship_reject(m_request)

    # Save the request information.
    m_request.save()

    if m_request.is_admin_accepted:
        # Mentorship request has been accepted.
        # Tell the Clerk to record this mentorship.
        clerk = UserActivityClerk(university.site, mentorship.mentor.profile.user)
        clerk.record_mentor_assigned(mentorship.student, mentorship.mentor)

        # Notify mentor that they have a new mentee.
        mentor_mentorship_accepted_noti(m_request, university)

    # Notify the student whether their request was accepted or declined.
    mentee_mentorship_decision_noti(m_request, university)

    return JsonResponse(payload)


@require_POST
@only_university
@login_required
def decide_meeting_request(request, university):
    """
    Completes POST request for admin to approve or decline a meeting request.
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    request_id = request.POST.get("r_id")
    is_accept = request.POST.get("is_accept")
    decline_reason = request.POST.get("reason", "")

    try:
        request_id = int(request_id)
        is_accept = string_to_bool(is_accept)

        if is_accept is None:
            raise ValueError('Invalid is_accept value')
    except ValueError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        payload[PAYLOAD_MSG] = INVALID_REQUEST
        app_logger.info(
            'invalid parameters: {0}, {1}'.format(
                request_id, is_accept))
        return JsonResponse(payload)

    mentorshipsession_exists = MentorshipSession.objects.filter(
        id=request_id,
        mentorship__mentor__platform_id=university.id).exists()

    if not mentorshipsession_exists:
        payload[PAYLOAD_STATUS] = INVALID_PERMISSION_CODE
        payload[PAYLOAD_MSG] = INVALID_PERMISSION
        app_logger.info('requested meeting id does not exist - {0}'.format(request_id))
        return JsonResponse(payload)

    session = MentorshipSession.objects.get(id=request_id)
    mentorship = session.mentorship
    m_request = MentorshipRequest.objects.filter(requested_mentorship__id=mentorship.id).order_by('-created_on').first()

    if is_accept:
        session.admin_accept_session()
        if not session.needs_mentor_approval():
            session.mentor_accept_session()
    else:
        session.admin_reject_session()
        session.mentor_reject_session()
        session.cancel_reason = decline_reason
        session.save()

    # Changing of the session's status will no longer alter the mentorship.
    m_request.via_meeting = False
    m_request.save()
    current_site_id = request.current_site.id
    if session.is_admin_accepted and session.is_mentor_accepted:
        if mentorship.mentor.gcal_email:
            make_gcal_event(session)

        MentorshipRequest.objects.mentorship_accept(m_request)

        session_change_noti_mentor(session, current_site_id, SESSION_CREATED, admin_decision=True)
        session_change_noti_mentee(session, current_site_id, SESSION_CREATED, admin_decision=True)
    elif session.is_admin_accepted and not session.is_mentor_accepted:
        session_request_noti_mentor(mentorship, session, current_site_id, is_multiple=False, meeting_topic=None)
    else:
        MentorshipRequest.objects.mentorship_reject(m_request, via_meeting=True)

        session_change_noti_mentor(session, current_site_id, SESSION_CANCELLED, admin_decision=True)
        session_change_noti_mentee(session, current_site_id, SESSION_CANCELLED, admin_decision=True)

    return JsonResponse(payload)


@require_POST
@only_university
@login_required
def add_mentor(request, university, **kwargs):
    """ Invites an email address to be a mentor or a team mentor.
    If the email is not associated with an ST account, one will be made.
    Regardless of whether 'team' = True in kwargs, the user will be invited to be a mentor (unless already one).
    If 'team' = True in kwargs, then the user will be invited to be a team mentor
    """
    if 'team' in kwargs:
        if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
    else:
        if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    msg = ""
    team_mentor = False

    if 'team' in kwargs:
        team_mentor = kwargs['team']

    if MentorshipEmailTemplate.objects.filter(platform=university).exists():
        invite_email_template = MentorshipEmailTemplate.objects.filter(platform=university).first()
    else:
        invite_email_template = MentorshipEmailTemplate.objects.create(platform=university)
    default_subject, default_invitation = get_default_email_subject_and_body(university, team_mentor)

    try:
        with transaction.atomic():
            action = request.POST.get('action')
            if action == 'save_email':
                invitation_subject = request.POST.get('email_subject')
                if invitation_subject == '':
                    invitation_email_subject = default_subject
                else:
                    invitation_email_subject = invitation_subject
                invitation_email = TextEditorForm2({'text': request.POST.get('email_body')})
                if invitation_email.is_valid():
                    invitation_email_body = invitation_email.cleaned_data['text']
                    if invitation_email_body == '<br>':
                        invitation_email_body = default_invitation
                else:
                    invitation_email_body = default_invitation
                invite_email_template.subject = invitation_email_subject
                invite_email_template.email_body = invitation_email_body
                invite_email_template.save()
            elif action == 'add_mentor':
                email = request.POST.get('mentor_email')
                first_name = request.POST.get('mentor_first_name')
                last_name = request.POST.get('mentor_last_name')
                office_hours = request.POST.get('enable_office_hours')
                if office_hours == 'true':
                    office_hours = True
                else:
                    office_hours = False
                profile_form = SignupAdditionalForm({'first_name': first_name,
                                                     'last_name': last_name})
                email_form = EmailForm({"email": email})
                mentor_word = university.style.mentor_word
                if team_mentor:
                    mentor_word = 'team ' + mentor_word
                if mentor_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
                    article = 'an'
                else:
                    article = 'a'

                if email_form.is_valid():
                    email = email.lower()
                    try:
                        new_user = UserEmail.objects.get(email__iexact=email)
                        created = False
                    except UserEmail.DoesNotExist:
                        created = True
                        new_user = UserEmail.objects.create(email=email)
                        new_user.set_password(uuid.uuid4())
                        new_user.save()
                    is_existing = False
                    if created:
                        if profile_form.is_valid():
                            new_profile = profile_form.save(new_user, university)
                        else:
                            msg = "Invalid first name and last name given for invited mentor."
                            raise ValueError(msg)
                    else:
                        try:
                            new_profile = StartupMember.objects.get_user(new_user)
                            if university.id not in new_profile.universities.all().values_list("id",
                                                                                               flat=True):
                                new_profile.universities.add(university)
                            is_existing = True
                        except StartupMember.DoesNotExist:
                            msg = "Something went wrong when we tried to create new {0}. " \
                                  "Please retry or contact StartupTree staff.".format(mentor_word)
                            raise ValueError("Email exists but not the user?")

                    new_mentor, created = Mentor.objects.get_or_create_mentor(
                        new_profile, university)

                    if created:
                        new_mentor.allow_office_hour = office_hours
                        new_mentor.save()
                    if team_mentor:
                        new_mentor.make_team_mentor()
                    if not created and not team_mentor:
                        msg = "{0} is already {1} {2} of {3}" "If the {2} is inactive, you can simply activate the {2} again.".format(
                            new_profile.get_full_name(), article, mentor_word, university.name)
                        raise ValueError("User is already {0} {1}!".format(article, mentor_word))
                    elif created and is_existing:
                        clerk = UserActivityClerk(
                            request.current_site, new_user, new_profile, False)
                        clerk.record_mentor_created()
                    if university.short_name == 'columbia':
                        new_profile.is_public = False
                        new_profile.save()
                    first_name = new_profile.first_name
                    program_name = None
                    if university.program_name:
                        program_name = university.program_name
                else:
                    msg = "Please provide valid name and email of the {0}.".format(mentor_word)
                    raise ValueError("Invalid info provided.")
                _mentor_is_complete_check(new_profile, university.id)
                send_email = request.POST.get('send_email', 'false')
                if send_email == 'true':
                    transaction.on_commit(lambda: send_mentor_invitation.delay(
                        new_user.id,
                        first_name,
                        university.site_id,
                        university.name,
                        university.name,
                        program_name,
                        is_existing,
                        team_mentor
                    ))
    except ValueError as e:
        app_logger.error(e)
        payload[PAYLOAD_STATUS] = SERVER_ERROR
    except IntegrityError as e:
        app_logger.error(e)
        payload[PAYLOAD_STATUS] = SERVER_ERROR
        msg = "??"
    payload[PAYLOAD_MSG] = msg

    return JsonResponse(payload)


@require_POST
@only_university
@login_required
def update_mentor(request, university):
    """
    Update the status of the mentor. Disable or Enable Mentor.
    If requested mentor is active, make it inactive, vice versa.
    :param request:
    :param university:
    :return:
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    try:
        m_id = int(request.POST.get("m_id"))
    except ValueError:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    mentor = Mentor.objects.get(id=m_id)
    if mentor.platform_id != university.id:
        payload[PAYLOAD_STATUS] = INVALID_PERMISSION_CODE
        return JsonResponse(payload)
    if mentor.is_active:
        mentor.is_active = False
    else:
        mentor.is_active = True
    mentor.save()
    return JsonResponse(payload)


@require_POST
@only_university
@login_required
def add_team_to_mentoring(request, university):
    if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    msg = ""

    url_id = request.POST.get('team_url_id')

    # GET TEAM
    team = Startup.objects.get(university=university, url_name=url_id)

    if team.team_mentorship_participant == False:
        # SET TEAM TO PARTICIPATE IN TEAM MENTORING
        team.team_mentorship_participant = True
        # SAVE TEAM
        team.save()

        transaction.on_commit(lambda: notify_team_add_mentorship_feature.delay(
            university.site_id,
            url_id
        ))
    else:
        msg = "This team has already been added to the feature."
        payload[PAYLOAD_MSG] = msg
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
    return JsonResponse(payload)


def match_team_to_mentor(request):
    is_admin_teammentorship = is_admin_authorized(request.is_university, request.is_super,
                                                  request.access_levels, request.is_startuptree_staff,
                                                  AccessLevel.TEAM_MENTORSHIP)
    if not is_admin_teammentorship:
        raise PermissionDenied
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    msg = ""

    mentor_first_name = request.POST.get('mentor_f_name')
    mentor_last_name = request.POST.get('mentor_l_name')
    mentor_email = request.POST.get('mentor_email')

    team_url_id = request.POST.get('team_url_id')

    # get team
    # Check if team is participating
    team = Startup.objects.filter(url_name=team_url_id).first()
    if team.team_mentorship_participant == False:
        msg = "Error: Make sure the team has been added to the feature."
        payload[PAYLOAD_MSG] = msg
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    # get mentor
    # Check if mentor is a team mentor
    mentor = Mentor.objects.filter(profile__user__email=mentor_email).first()
    if mentor.is_team_mentor == False:
        msg = "Error: Make sure the mentor has been added to the feature."
        payload[PAYLOAD_MSG] = msg
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)

    if TeamMentorship.objects.mentorship_exists(team, mentor):
        # Mentorship already created
        msg = "Error: This match has already been made."
        payload[PAYLOAD_MSG] = msg
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
    else:
        new_mentorship = TeamMentorship.objects.create_new_mentorship(team, mentor, request.university)

    # NOTIFY TEAM OF MATCH
    transaction.on_commit(lambda: team_mentorship_notify_venture.delay(
        request.current_site.id,
        team_url_id,
        mentor_email
    ))
    # NOTIFY MENTOR OF MATCH
    transaction.on_commit(lambda: team_mentorship_notify_mentor.delay(
        request.current_site.id,
        team_url_id,
        mentor_email
    ))
    payload["mentor"] = mentor_first_name + " " + mentor_last_name
    payload["venture"] = team.name

    if mentor.profile.get_image() is None:
        payload["mentor_image"] = get_static_url() + "img/icon_person.jpg"
    else:
        payload["mentor_image"] = mentor.profile.get_image()

    if team.get_image() is None:
        payload["venture_image"] = get_static_url() + "img/icon_startup.jpg"
    else:
        payload["venture_image"] = team.get_image()

    return JsonResponse(payload)


def update_mentor_settings(request):
    """
    Update the mentor's contact and availability settings.

    TODO FIXME: Add security check (is_admin_authorized call)
    """
    mentor_id = None
    if 'mentor-id-defaults' in request.POST:
        mentor_id = request.POST.get('mentor-id-defaults')
    elif 'mentor-id-bo' in request.POST:
        mentor_id = request.POST.get('mentor-id-bo')
    elif 'mentor-id-avail' in request.POST:
        mentor_id = request.POST.get('mentor-id-avail')
    else:
        raise ValueError('no valid mentor id was provided')

    set_mentor_settings(request, mentor_id)

    return HttpResponseRedirect(reverse('uni-manage-mentors'))


def get_emails(request):
    mentor_email = request.POST.get('mentor_email')
    mentee_email = request.POST.get('mentee_email')
    rec_msg = request.POST.get('rec_msg')

    return mentor_email, mentee_email, rec_msg


def handle_articles(mentor_word, mentee_word):
    if mentor_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        mentor_article = 'an'
    else:
        mentor_article = 'a'
    if mentee_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        mentee_article = 'an'
    else:
        mentee_article = 'a'

    return mentor_article, mentee_article


def get_user(email):
    """ Returns the useremail associated with the email, if it exists, otherwise returns None. """
    try:
        user = UserEmail.objects.get(email__iexact=email)
    except UserEmail.DoesNotExist:
        user = None
    return user


def get_user_or_invite(email_address, first_name, last_name, university, invitor_name):
    """ Returns the useremail associated with the email. If it doesn't exist, then send a platform
    invite and return the generated useremail"""
    user = get_user(email_address)

    if user:
        return user

    university_style = university.style
    email_subject = university_style.email_invite_user_subject \
        if university_style.email_invite_user_subject \
        else university_style.generate_default_email_invite_user_subject()
    email_body = university_style.email_invite_user_body \
        if university_style.email_invite_user_body \
        else university_style.generate_default_email_invite_user_body()

    create_user_invite(email_address, [], [],
                       first_name, last_name, university.id, send_invite=True,
                       invitor_name=invitor_name, email_subject=email_subject, email_body=email_body)

    return get_user(email_address)


@require_POST
@only_university
@login_required
def recommend(request, university):
    """
    Make recommendation on a mentor-mentee pair

    If mentee does not exist on platform, automatically send invite.

    :param request:
    :param university:
    :return:
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    msg = ""
    try:
        with transaction.atomic():
            mentor_email, mentee_email, rec_msg = get_emails(request)
            mentor_email_form = EmailForm({"email": mentor_email})
            mentee_email_form = EmailForm({"email": mentee_email})

            mentor_word = university.style.mentor_word
            mentee_word = university.style.mentee_word

            mentorship_word = university.style.mentorship_word
            mentor_article, mentee_article = handle_articles(mentor_word, mentee_word)

            if mentor_email_form.is_valid() and mentee_email_form.is_valid():
                mentor_email = mentor_email.lower()
                mentee_email = mentee_email.lower()

                mentor_first = request.POST.get('mentor_first_name', '')
                mentor_last = request.POST.get('mentor_last_name', '')
                mentor_full_name = f'{mentor_first} {mentor_last}'
                mentee_first = request.POST.get('mentee_first_name', '')
                mentee_last = request.POST.get('mentee_last_name', '')
                invitor_name = request.session.get('user_name')

                new_mentor = get_user(mentor_email)
                '''
                Checks if invited mentor exists as a StartupMember and  Mentor
                '''
                if new_mentor:
                    try:
                        new_mentor_profile = StartupMember.objects.get_user(new_mentor)
                        mentor = Mentor.objects.select_related('profile').get(
                            profile_id=new_mentor_profile.id, platform_id=university.id)
                    except Mentor.DoesNotExist:
                        msg = f"The invited user ({mentor_full_name}) is not {mentor_article} {mentor_word} " \
                              f"on StartupTree. Please add them as a {mentor_word} before recommending."
                        raise ValueError("Can't recommend a non mentor")
                else:
                    msg = f"{mentor_full_name} is not yet on on StartupTree. Please add them as {mentor_article} " \
                          f"{mentor_word} before recommending."
                    raise ValueError("Invited mentor is not on StartupTree")

                mentee_user = get_user_or_invite(mentee_email, mentee_first, mentee_last, university, invitor_name)
                mentee = StartupMember.objects.get_user(mentee_user)

                if university.id not in new_mentor_profile.universities.all().values_list("id", flat=True):
                    new_mentor_profile.universities.add(university)

                if university.id not in mentee.universities.all().values_list("id", flat=True):
                    mentee.universities.add(university)

                mentee_url = mentee.get_url()
                mentor_url = new_mentor_profile.get_url()

                mentorship_rec_noti(mentor_email, mentor, university, request, True, rec_msg, mentee_url)
                mentorship_rec_noti(mentee_email, mentee, university, request, False, rec_msg, mentor_url)

                try:
                    mentorship = Mentorship.objects.create_new_mentorship(mentee, mentor)
                    m_request = MentorshipRequest.objects.create_new_request(
                        mentorship, None, "", "", via_meeting=False)

                    m_request.is_admin_accepted = True
                    m_request.requested_mentorship = mentorship
                    m_request.save()

                    mentorship.accepted_request = m_request
                    mentorship.is_active = True
                    platform = mentorship.mentor.platform
                    mentorship_activity = MentorshipActivity.objects.make_mentorship_activity(
                        MentorshipActivity.CONNECTION_MADE, platform)
                    mentorship.mentorship_activity = mentorship_activity
                    mentorship.save()
                except ValueError:
                    msg = "{0} is already active.".format(mentorship_word)
                    raise ValueError("Already active mentorship")

            else:
                if not mentor_email_form.is_valid():
                    msg = "Please provide valid name and email of the {0}.".format(mentor_word)
                    raise ValueError("Invalid mentor info provided.")

                if not mentee_email_form.is_valid() or mentee_email_form is None:
                    msg = "Please provide valid name and email of the {0}.".format(mentee_word)
                    raise ValueError("Invalid mentee info provided.")
    except ValueError as e:
        app_logger.error(e)
        payload[PAYLOAD_STATUS] = SERVER_ERROR
    except IntegrityError as e:
        app_logger.error(e)
        payload[PAYLOAD_STATUS] = SERVER_ERROR
        msg = "??"
    payload[PAYLOAD_MSG] = msg

    return JsonResponse(payload)


def get_schedule_structure():
    """
    Returns empty schedule structure.
    """
    schedules = {'Mon': {'day': '', 'times': []},
                 'Tue': {'day': '', 'times': []},
                 'Wed': {'day': '', 'times': []},
                 'Thu': {'day': '', 'times': []},
                 'Fri': {'day': '', 'times': []},
                 'Sat': {'day': '', 'times': []},
                 'Sun': {'day': '', 'times': []},
                 }

    return schedules


def sort_final(fin):
    fin2 = []
    for i, element in enumerate(fin):
        mid = element['day'].find(',')
        if element['day'] == '':
            date = datetime.datetime.strptime(
                "3005 Tue, May 05", '%Y %a, %b %d')
        elif mid == -1:
            date = datetime.datetime.strptime(fin[i]['day'], '%a %b %d')
        else:
            date = datetime.datetime.strptime(fin[i]['day'], '%a, %b %d')
        fin2.append({'day': date, 'times': i})
    fin2 = sorted(fin2, key=itemgetter('day'))
    return fin2


def conv_to_time(time_str):
    # mid = time_str.find('-')
    time = datetime.datetime.strptime(time_str[0:5] + time_str[6:8], '%I:%M%p').time()

    return time


def sorted_timeslot(time_lst):
    for i in range(0, len(time_lst)):
        time_lst[i]['time'] = conv_to_time(time_lst[i]['time'])
        time_lst[i]['idx'] = i
    time_lst = sorted(time_lst, key=itemgetter('time'))

    return time_lst


def sort_dates(sched):
    """
    Sort the list of dictionaries by date ascending.
    """
    sorted_sched = sorted(sched, key=lambda date: date['date_obj'])  # TODO test

    return sorted_sched


def get_timeslots(mentor_list, timezone, filter_slot, page_num=1, start_to_end=None):
    """

    The return format final_sorted is:

    [
        ...,
        {
            'day': <String, eg "Fri, Sep 13">,
            'date_obj': <datetime.date object>,
            'times': [
                ...,
                <Dictionary as specified in code below>,
                ...,
            ],
        },
        ...,
    ]


    """
    schedules = get_schedule_structure()

    final = []
    for mentor in mentor_list:
        if is_cronofy_linked(mentor.profile.user):
            schedule = get_cronofy_possible_calendar_schedules(mentor, page_num, user_timezone=timezone)
        else:
            schedule = get_possible_calendar_schedules(mentor, page_num, user_timezone=timezone)
        if start_to_end is None:
            start_to_end = "{0} - {1}".format(schedule[0][1], schedule[6][1])
        for day in schedule:
            if day[2]:
                timelist = []
                for times in day[2]:
                    session_type = day[4]
                    session_location = day[5]
                    if len(times) >= 5:
                        mentee = times[4]
                        is_team_mentoring = (times[5] == 'team')
                        meeting_topic = times[6]
                        meeting_id = times[7]
                        if times[8]:
                            session_type = times[8]
                        if times[9]:
                            session_location = times[9]
                    else:
                        mentee = None
                        is_team_mentoring = None
                        meeting_topic = None
                        meeting_id = None
                    if filter_slot is not None and (filter_slot == 'true' and mentee is None) or (
                            filter_slot == 'false' and mentee is not None):
                        continue  # don't add the time slot
                    timeslot = times[0] + '-' + times[1]
                    time = {'time': timeslot, 'start_time': times[0], 'duration': times[3],
                            'type': session_type, 'location': session_location,
                            'mentor': mentor.profile, 'mentee': mentee, 'is_team_mentoring': is_team_mentoring,
                            'meeting_topic': meeting_topic, 'meeting_id': meeting_id,
                            'mentor_obj': mentor}
                    timelist.append(time)

                schedules[day[0]]['day'] = day[0] + ", " + day[1]
                schedules[day[0]]['date_obj'] = day[6]
                schedules[day[0]]['times'].append(timelist)

    for day in schedules:
        if schedules[day]['times']:
            final.append(schedules[day])
    final_sorted = sort_dates(final)

    return final_sorted, schedules, start_to_end


class GetTimeslotsByDateRange:
    """ This class utilizes get_timeslots() to provide timeslots specified by date range (as opposed to by page number).

    The elegant solution would have been to refactor get_timeslots(). However, in the interest of time, this
    quicker solution gets the job done.
    """

    @staticmethod
    def get_timeslots_by_date(
            mentor_list,
            time_zone,
            filter_slot,
            date_range: Tuple[dt.date, dt.date]):
        """ Exactly like get_timeslots() except, slots are chosen by a date range (as opposed to the page week).
        Assumes date_range is valid (start is not after end, and both are not before this monday.)
        """

        # First get timeslots by combining week pages
        first_page, last_page = GetTimeslotsByDateRange.__date_range_to_pages(date_range)
        curr_page = first_page
        timeslots = []
        while curr_page <= last_page:
            page_timeslots, _, _ = get_timeslots(mentor_list, time_zone, filter_slot, curr_page)
            timeslots += page_timeslots
            curr_page += 1

        # Trim off excess dates from front and back
        timeslots = GetTimeslotsByDateRange.__trim_timeslots(timeslots, date_range)

        return timeslots

    @staticmethod
    def __date_range_to_pages(date_range: Tuple[dt.date, dt.date]) -> Tuple[int, int]:
        """ Given a date range, convert it to the tightest range of page numbers that cover the date range. """
        start_date = date_range[0]
        end_date_inclusive = date_range[1]

        def get_monday(day: dt.date):
            return day - datetime.timedelta(days=day.weekday())

        this_monday = get_monday(datetime.date.today())

        def get_page(day: dt.date):
            that_monday = get_monday(day)
            return int((that_monday - this_monday).days / 7) + 1

        page_start = get_page(start_date)
        page_end = get_page(end_date_inclusive)

        return page_start, page_end

    @staticmethod
    def __trim_timeslots(timeslots, date_range: Tuple[dt.date, dt.date]):
        """ Given a set of timeslots, trim off extraneous dates, given the specified date range. """

        def is_in_date_range(timeslots_elt):
            date_obj = timeslots_elt['date_obj']
            start_date = date_range[0]
            end_date_inclusive = date_range[1]
            return start_date <= date_obj <= end_date_inclusive

        return filter(is_in_date_range, timeslots)


def _view_list_context(request, university, is_team=False, page_num=1):
    """

    Notes regarding filtering :
        Abstractly, we are either filtering by mentor or by tag (it doesn't make sense to filter by both).
        Meaning, if a tag is selected, when a mentor is already selected, then the mentor selection is reset.
        Similarly, if a mentor is selected when a tag is already selected, then the tag selection is reset.

        When a tag is selected, then the calendar data is filtered down to that of mentors with the designated tag.
        Furthermore, the available mentors in the mentor filter dropdown will also narrow down to those that have the tag.

        When a mentor is selected, then the calendar data is filtered down to only that of the selected mentor.
        Furthermore, the tag selection is reset.

        The frontend should only allow either a mentor filter or a tag filter to be specified (not both).
        In the backend, if both are specified, this scenario is handled as if only a mentor is specified
        (and an app_logger.error message is quietly raised).

    """
    # FIXME: needs pagination!!!
    page = {
        'prev': page_num - 1 if page_num > 1 else 1,
        'curr': page_num,
        'next': page_num + 1,
    }

    # filter by timeslot
    filter_slot = request.GET.get('filter_slot_type', None)
    filtered_mentor_id = request.GET.get('filter_mentor', None)  # Empty str or None denotes "All"
    filtered_tag_id = request.GET.get('filter_tag', None)  # Empty str or None denotes "All"

    current_user = request.current_member

    all_mentors = Mentor.objects.select_related(
        'profile', 'profile__user').filter(
        platform=university, is_active=True).order_by('profile__first_name')
    all_tags = Label.objects.filter(university_id=university.id)

    data_mentors = all_mentors  # queryset of mentors to get calendar data for.
    dropdown_mentors = all_mentors  # queryset of mentors for dropdown
    filtered_tag = None
    filtered_mentor = None
    if filtered_mentor_id:
        if filtered_tag_id:
            app_logger.error(
                "Frontend Bug: both 'filter_tag' and 'filter_mentor' are both defined when only one should be. "
                "Assuming only 'filter_mentor' selection.")
        data_mentors = all_mentors.filter(id=filtered_mentor_id)
        filtered_mentor = data_mentors.first()

    elif filtered_tag_id:
        filtered_tag = all_tags.get(id=filtered_tag_id)
        data_mentors = all_mentors.filter(profile__labels__id=filtered_tag.id)
        dropdown_mentors = data_mentors

    # in else case, no filters are set and default variable values apply here.

    start_to_end = None
    user_timezone = pytz.timezone(request.session['django_timezone'])
    final_sorted, schedules, start_to_end = get_timeslots(data_mentors, user_timezone, filter_slot, page_num,
                                                          start_to_end)

    mentor_word = request.university_style.mentor_word
    mentee_word = request.university_style.mentee_word

    return {
        'university': university,
        'start_to_end': start_to_end,
        'tags': all_tags,
        'dropdown_mentors': dropdown_mentors,
        'mentor_word': mentor_word,
        'mentee_word': mentee_word,
        'final': final_sorted,
        'schedules': schedules,
        'page': page,
        'current_tag': filtered_tag,
        'current_mentor': filtered_mentor,
        'filter_slot': filter_slot,
        'current_user': current_user
    }


@only_university
@login_required
def view_list(request, university, *args, **kwargs):
    """
    View list of mentors with scheduled office hours for that week.

    or fetch just a page

    FIXME: This has not yet been implemented for team mentorship
    """
    is_team = kwargs.get('is_team', False)
    is_fetch = request.GET.get('is_fetch') == 'true'
    page = int(kwargs.get('page', 1))
    if is_team:
        if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()
    else:
        if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404()

    context = _view_list_context(request, university, page_num=page)
    context['is_team'] = is_team

    if is_fetch:
        template = get_template('mentorship/admin/calendar/table_inner.html')
        rendered_template = template.render(context)
        return JsonResponse({'html': rendered_template})

    return TemplateResponse(
        request,
        'mentorship/admin/calendar/view_full.html',
        context
    )


@only_university
@login_required
def dash_cal(request, university):
    """
    Returns template with data for office hours calendar to be displayed
    in the upgraded dashboard.
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()

    context = _view_list_context(request, university)

    return TemplateResponse(
        request,
        'mentorship/admin/calendar/table_outer.html',
        context
    )


@only_university
@login_required
@require_POST
def export_calendar(request, university):
    """
    Creates a downloadable spreadsheet of the current calendar data.
    """
    uni_style = request.university_style
    timeslot_type = request.POST.get("timeslot_type")
    tag_id = request.POST.get("tag_id")
    mentor_id = request.POST.get("mentor_id")
    start_date = request.POST.get("start_date")
    end_date = request.POST.get("end_date")

    def get_date(date_str: str) -> dt.date:
        return parser.parse(date_str).date()

    date_range = (get_date(start_date), get_date(end_date))

    all_mentors = Mentor.objects.select_related(
        'profile', 'profile__user').filter(
        platform=university, is_active=True).order_by('profile__first_name')
    if tag_id != '':
        tag = Label.objects.get(university_id=university.id, id=tag_id)
        all_mentors = all_mentors.filter(profile__labels__id=tag.id)
        title_str = tag.title.lower().replace(' ', '_')
    elif mentor_id != '':
        all_mentors = all_mentors.filter(id=mentor_id)
        title_str = all_mentors.first().profile.get_full_name().lower().replace(' ', '_')
    else:
        plural_mentors = make_words(uni_style)['plural_mentors']
        title_str = 'all_{0}'.format(plural_mentors.lower())

    filename = str(
        uni_style.mentorship_word.lower()) + "_calendar_export_" + title_str + "_" + datetime.date.today().strftime(
        "%m.%d.%y") + ".xlsx"
    attachment = BytesIO()
    workbook = xlsxwriter.Workbook(attachment)
    calendar_sheet = workbook.add_worksheet('{0} Calendar'.format(uni_style.mentorship_word.capitalize()))
    wrap_format = workbook.add_format()
    wrap_format.set_text_wrap()

    platform_timezone = pytz.timezone(uni_style.timezone)

    schedule = GetTimeslotsByDateRange.get_timeslots_by_date(all_mentors, platform_timezone, timeslot_type, date_range)
    custom = custom_member_dict(sm=None, uni_id=university.id, form_type=Form.MENTORMEETING)

    # Get column heads
    mentor_word = uni_style.mentor_word.capitalize()
    mentee_word = uni_style.mentee_word.capitalize()
    col_head = ["{0} Name".format(mentor_word), "Date", "Time",
                "{0} Timezone".format(mentor_word), "Platform Timezone",
                "Status/{0} Information".format(mentee_word), "Meeting Location",
                "Meeting Topic"]
    for q in custom:
        col_head.append("{0}".format(q['q_text']))
    for offset, entry in enumerate(col_head):
        calendar_sheet.write(0, offset, entry)

    # Now fill spreadsheet
    row_cnt = 1
    for day in schedule:
        for time in day['times']:
            for slot in time:
                data = ["{0}".format(slot['mentor']), "{0}".format(day['day']),
                        "{0} ({1})".format(slot['time'], platform_timezone), "{0}".format(slot['mentor_obj'].timezone),
                        "{0}".format(platform_timezone)]
                if slot['mentee']:
                    data.append("{0} ({1})".format(slot['mentee'], slot['mentee'].user.email))
                    if slot['type'] == 1:
                        type_str = "Phone"
                    elif slot['type'] == 3:
                        type_str = "Video Call"
                    elif slot['type'] == 4:
                        type_str = "In Person"
                    data.append("{0} - {1}".format(type_str, slot['location']))
                    data.append(slot['meeting_topic'])
                    meeting = MentorshipSession.objects.get(id=slot['meeting_id'])
                    custom_answers = custom_member_dict(sm=slot['mentee'], uni_id=university.id,
                                                        form_type=Form.MENTORMEETING, meeting=meeting)
                else:
                    data.append("Open")
                    data.append("N/A")
                    data.append("N/A")
                    custom_answers = []
                for q in custom_answers:
                    if not q['a_id']:
                        data.append("None")
                    elif q['q_type'] == 'Paragraph':
                        data.append("{0}".format(q['a_text']))
                    elif q['q_type'] == 'Dropdown' or q['q_type'] == 'Multiple Choice':
                        no_response = True
                        selected_choices = ""
                        for choice in q['q_choices']:
                            if choice['chosen']:
                                no_response = False
                                if len(selected_choices) != 0:
                                    selected_choices += "\n"
                                selected_choices += choice['text']
                        if no_response:
                            selected_choices = "None"
                        data.append(selected_choices)
                    elif q['q_type'] == 'Date Time':
                        data.append("{0}".format(q['a_date']))
                    elif q['q_type'] == 'File Upload':
                        if q['a_url'] is None:
                            data.append("No")
                        else:
                            data.append("Yes")
                for offset, entry in enumerate(data):
                    calendar_sheet.write(row_cnt, offset, entry, wrap_format)
                row_cnt += 1
    workbook.close()
    attachment.seek(0)
    response = HttpResponse(
        attachment.read(),
        content_type="application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
    attachment.close()
    return response


@only_mentorship_admin
@only_university
@login_required
def manage_activities(request, university, is_team=False):
    page = request.GET.get('page')
    filter_start_date = request.GET.get('start-date')
    filter_end_date = request.GET.get('end-date')
    filter_note = request.GET.get('note') == 'true'
    filter_goal = request.GET.get('goal') == 'true'
    filter_meeting = request.GET.get('meeting') == 'true'
    filter_mentorship = request.GET.get('mentorship') == 'true'
    filter_mentee = request.GET.get('mentee')
    filter_mentor = request.GET.get('mentor')

    if is_team:
        activities_obj = TeamMentorshipActivity.objects.all_platform_regular_activities(university)
    else:
        activities_obj = MentorshipActivity.objects.all_platform_regular_activities(university)

    if filter_start_date:
        start_date = datetime.datetime.strptime(filter_start_date, '%Y-%m-%d')
        activities_obj = activities_obj.filter(timestamp__gte=start_date)
    if filter_end_date:
        end_date = datetime.datetime.strptime(filter_end_date, '%Y-%m-%d')
        activities_obj = activities_obj.filter(timestamp__lte=end_date)

    type_filters = None
    mentorship_filters = None
    if filter_note:
        note_filter = Q(activity_type=MentorshipActivity.NOTE_CREATED)
        if type_filters:
            type_filters = type_filters | note_filter
        else:
            type_filters = note_filter
    if filter_goal:
        goal_filter = Q(activity_type=MentorshipActivity.GOALSET_CREATED)
        if type_filters:
            type_filters = type_filters | goal_filter
        else:
            type_filters = goal_filter
    if filter_meeting:
        meeting_filter = Q(activity_type=MentorshipActivity.SESSION_SCHEDULED)
        if type_filters:
            type_filters = type_filters | meeting_filter
        else:
            type_filters = meeting_filter
    if filter_mentorship:
        mentorship_filter = Q(activity_type=MentorshipActivity.CONNECTION_MADE)
        if type_filters:
            type_filters = type_filters | mentorship_filter
        else:
            type_filters = mentorship_filter

    mentor = None
    mentee = None
    mentorships = None
    if filter_mentor:
        mentor = Mentor.objects.select_related('profile').filter(
            profile__url_name=filter_mentor, platform=university)
        filter_mentor = {
            'url_id': filter_mentor,
            'value': filter_mentor
        }
        if mentor.exists():
            mentor = mentor.first()
            filter_mentor['name'] = mentor.profile.get_full_name()
        else:
            mentor = None
    if filter_mentee:
        if is_team:
            mentee = Startup.objects.filter(url_name=filter_mentee)
            filter_mentee = {
                'url_id': filter_mentee,
                'value': filter_mentee
            }
            if mentee.exists():
                mentee = mentee.first()
                filter_mentee['name'] = mentee.name
            else:
                mentee = None
        else:
            mentee = StartupMember.objects.filter(url_name=filter_mentee)
            filter_mentee = {
                'url_id': filter_mentee,
                'value': filter_mentee
            }
            if mentee.exists():
                mentee = mentee.first()
                filter_mentee['name'] = mentee.get_full_name()
            else:
                mentee = None
    if mentor and mentee:
        if is_team:
            mentorships = TeamMentorship.objects.filter(mentor=mentor, team=mentee)
        else:
            mentorships = Mentorship.objects.filter(mentor=mentor, student=mentee)
    elif mentor:
        if is_team:
            mentorships = TeamMentorship.objects.filter(mentor=mentor)
        else:
            mentorships = Mentorship.objects.filter(mentor=mentor)
    elif mentee:
        if is_team:
            mentorships = TeamMentorship.objects.filter(team=mentee)
        else:
            mentorships = Mentorship.objects.filter(student=mentee)

    if mentorships is not None:
        if is_team:
            mentorship_filters = Q(team=mentee)
        else:
            mentorship_filters = (
                    Q(mentorship__in=mentorships) |
                    Q(goalset__mentorship__in=mentorships) |
                    Q(note__mentorship__in=mentorships) |
                    Q(mentorshipsession__mentorship__in=mentorships)
            )
    if type_filters and mentorship_filters:
        activities_obj = activities_obj.filter(type_filters & mentorship_filters)
    elif type_filters:
        activities_obj = activities_obj.filter(type_filters)
    elif mentorship_filters:
        activities_obj = activities_obj.filter(mentorship_filters)

    if is_team:
        activities_obj = TeamMentorshipActivity.objects.order_activities(activities_obj)
    else:
        activities_obj = MentorshipActivity.objects.order_activities(activities_obj)
    pagniator = Paginator(activities_obj, 8)
    try:
        activities = pagniator.page(page)
        page = int(page)
    except PageNotAnInteger:
        activities = pagniator.page(1)
        page = 1
    except EmptyPage:
        activities = pagniator.page(pagniator.num_pages)
        page = pagniator.num_pages

    prev_page, cur_pages, next_page = diggy_paginator(pagniator, page)
    activities_json = []
    for activity in activities:
        activities_json.append(activity.to_dict())
    context = {'is_team': is_team}
    context['activities'] = json.dumps(activities_json)
    context['page_dict'] = json.dumps({
        'cur_page': page,
        'prev_page': prev_page,
        'next_page': next_page,
        'cur_pages': cur_pages
    })
    context['filters'] = json.dumps({
        'startDate': filter_start_date,
        'endDate': filter_end_date,
        'filterNote': filter_note,
        'filterGoal': filter_goal,
        'filterMeeting': filter_meeting,
        'filterMentorship': filter_mentorship,
        'filterMentee': filter_mentee,
        'filterMentor': filter_mentor,
    })
    if request.META.get('HTTP_X_PJAX', False):
        context['next_uri'] = request.META.get('REQUEST_URI')
        return JsonResponse(context)
    context['university'] = university
    if SendAllPostMeetingRemindersTimestamp.objects.filter(is_team=is_team, platform=university).exists():
        timestamp_obj = SendAllPostMeetingRemindersTimestamp.objects.filter(is_team=is_team, platform=university) \
            .order_by('timestamp').last()
        context['lastSendAllPostMeetingRemindersTimestamp'] = format_datetime_12hr(timestamp_obj.timestamp,
                                                                                   university.style.timezone)
    context['activity_types_dict'] = dict(MentorshipActivity.MENTORSHIP_ACTIVITY_TYPES)
    return TemplateResponse(
        request,
        'mentorship/admin/activities.html', context
    )


def send_post_meeting_reminder__helper(session: MentorshipSession):
    """ Requires: Security check has already been done.
    Sends a post-meeting email reminder to all mentors and mentees attending the session.
    This applies to both mentorship and team mentorship.
    """
    send_session_followup_to_mentor.delay(session.id)
    send_session_followup_to_mentee.delay(session.id)


@only_university
@login_required
@require_POST
def send_all_post_meeting_reminders(request, university):
    """ Send post meeting reminders for all past sessions. """
    authorization = is_admin_authorized_mentorship(request.POST, 'isTeam', request.is_university, request.is_super,
                                                   request.access_levels, request.is_startuptree_staff)
    if authorization['invalid_response']:
        return authorization['invalid_response']
    is_team = authorization['is_team']

    new_timestamp = SendAllPostMeetingRemindersTimestamp.objects.create(platform=request.university, is_team=is_team)

    now = timezone.now()

    # For now (as of Nov 2020), reminders are sent out for ALL meetings in the past. After Theresa consults with
    # Columbia business school, she will determine what kind of date-range functionality to implement.
    common_query = Q(datetime__lt=now,
                     is_cancelled=False,
                     is_admin_accepted=True,
                     is_mentor_accepted=True)

    sessions: QuerySetType[MentorshipSession]

    if not is_team:
        sessions = MentorshipSession.objects.filter(common_query, mentorship__mentor__platform=university)
    else:
        sessions = TeamMentorshipSession.objects.filter(common_query, team__university=university)

    for session in sessions:
        send_post_meeting_reminder__helper(session)

    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE,
               PAYLOAD_MSG: 'Success',
               'new_timestamp': format_datetime_12hr(new_timestamp.timestamp, university.style.timezone)}
    return JsonResponse(payload, status=VALID_REQUEST_CODE)


@only_university
@login_required
@require_POST
def send_post_meeting_reminder(request, university):
    authorization = is_admin_authorized_mentorship(request.POST, 'isTeam', request.is_university, request.is_super,
                                                   request.access_levels, request.is_startuptree_staff)
    if authorization['invalid_response']:
        return authorization['invalid_response']
    is_team = authorization['is_team']

    try:
        activity_id = int(request.POST.get('activity_id'))
        activity = MentorshipActivity.objects.get(id=activity_id)
        if activity.mentorshipsession:
            session = activity.mentorshipsession
        else:
            raise MentorshipSession.DoesNotExist
    except TypeError:
        payload = {PAYLOAD_STATUS: INVALID_REQUEST_CODE,
                   PAYLOAD_MSG: 'Invalid Request Param Format', }
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)
    except MentorshipActivity.DoesNotExist:
        payload = {PAYLOAD_STATUS: INVALID_REQUEST_CODE,
                   PAYLOAD_MSG: 'Invalid Request Param (Activity Does Not Exist)', }
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)
    except MentorshipSession.DoesNotExist:
        payload = {PAYLOAD_STATUS: INVALID_REQUEST_CODE,
                   PAYLOAD_MSG: 'Session Does Not Exist', }
        return JsonResponse(payload, status=INVALID_REQUEST_CODE)

    # The following errors should be prevented by frontend; but in case they are not, we provide error.
    is_error = False
    error_msg = ''
    if session.is_cancelled:
        error_msg = f'Session ID {session.id} was cancelled. No post-meeting reminder sent.'
        is_error = True
    if session.datetime >= timezone.now():
        error_msg = f'Session ID {session.id} is in future. No post-meeting reminder sent.'
        is_error = True
    if not session.is_admin_accepted:
        error_msg = f'Session ID {session.id} was never approved by admin. No post-meeting reminder sent.'
        is_error = True
    if not session.is_admin_accepted:
        error_msg = f'Session ID {session.id} was never approved by mentor. No post-meeting reminder sent.'
        is_error = True

    if not is_error:
        send_post_meeting_reminder__helper(session)
        final_status = VALID_REQUEST_CODE
        final_msg = 'Success'
    else:
        final_status = INVALID_ACTION
        final_msg = error_msg

    payload = {PAYLOAD_STATUS: final_status,
               PAYLOAD_MSG: final_msg}
    return JsonResponse(payload, status=final_status)


@only_mentorship_admin
@only_university
@login_required
def manage_activities_detail(request, university, activity_id, is_team=False):
    context = {}
    try:
        if is_team:
            activity = TeamMentorshipActivity.objects.activity_select_related().get(id=int(activity_id))
        else:
            activity = MentorshipActivity.objects.activity_select_related().get(id=int(activity_id))
    except:
        return JsonResponse(context, status=400)
    if activity.platform_id != university.id:
        return JsonResponse(context, status=403)
    result = activity.get_body(full=True)
    activity_type = result['activity_type']
    if activity_type == MentorshipActivity.SESSION_SCHEDULED:
        if is_team:
            meeting = activity.mentorshipsession
            result['full_body']['custom'] = custom_member_dict(
                None, university.id, Form.MENTORMEETING, meeting=meeting)
        else:
            meeting = activity.mentorshipsession
            result['full_body']['custom'] = custom_member_dict(
                None, university.id, Form.MENTORMEETING, meeting=meeting)
    elif activity_type == MentorshipActivity.CONNECTION_MADE:
        if is_team:
            result['full_body']['custom'] = []
        else:
            mentorship = activity.mentorship
            result['full_body']['custom'] = custom_member_dict(
                mentorship.student, university.id, Form.MENTORMEETING, mentor=mentorship.mentor)
    context = {'result': json.dumps(result)}
    return JsonResponse(context)


@only_university
@login_required
def get_session_detail(request, university):
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    sid = request.GET.get('sid', None)
    info = request.GET.get('info', None)
    payload = {PAYLOAD_STATUS: VALID_REQUEST_CODE}
    if sid is None or info is None:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE
        return JsonResponse(payload)
    session = MentorshipSession.objects.get(id=int(sid))

    if info == 'feedback':
        # TODO: These are at Note object. Deprecate the below fields and get data from relevant notes
        feedback = {
            'rating': session.student_feedback,
            'rating_note': session.student_feedback_note,
            'mentor_feedback': session.mentor_feedback
        }
        payload['body'] = feedback
    elif info == 'notes':
        date_created = 'Not Created Yet.'
        # TODO: These are at Note object. Deprecate the below fields and get data from relevant notes
        if session.note_created:
            date_created = session.note_created.strftime('%Y-%m-%d')
        note = {'note': session.note,
                'created': date_created,
                'rating': session.mentor_feedback}
        payload['body'] = note

        mentorship = session.mentorship
        gen_notes = mentorship.mentor_notes
        gen_notes = "" if gen_notes is None else gen_notes
        payload['gen_notes'] = gen_notes

        goals = []
        for goal in Goal.objects.filter(session=session):
            tmp = {
                'goal': goal.note,
                'completed': goal.completed,
            }
            goals.append(tmp)
        payload['goals'] = goals

    else:
        payload[PAYLOAD_STATUS] = INVALID_REQUEST_CODE

    return JsonResponse(payload)


@only_university
@login_required
def get_mentor_metrics(request, university):
    """ Analogous to Team Mentorship's: get_team_mentorship_chart
    """
    if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {'status': 200}
    now = datetime.datetime.now()
    five_months_back = month_delta(now, -5)
    mentors = Mentor.objects.select_related('profile').filter(
        platform_id=university.id,
        assigned_on__range=(five_months_back, now)) \
        .order_by('assigned_on') \
        .prefetch_related('profile__skills')

    mentors_by_month = {}
    months = []
    top_skills = {}
    user_ids = []
    for i in range(6):
        tmp = (five_months_back.month + i) % 12
        if tmp == 0:
            tmp = 12
        months.append(tmp)
    for mentor in mentors:
        month = mentor.assigned_on.month
        user_ids.append(mentor.profile_id)
        skills = mentor.profile.skills.all()
        if month in mentors_by_month:
            if mentor.is_active:
                mentors_by_month[month][0] += 1
            else:
                mentors_by_month[month][1] += 1
        else:
            if mentor.is_active:
                mentors_by_month[month] = [1, 0]
            else:
                mentors_by_month[month] = [0, 1]
        for skill in skills:
            s_name = skill.name.lower().title()
            if s_name in top_skills:
                top_skills[s_name] += 1
            else:
                top_skills[s_name] = 1
    payload['top_skills'] = sorted(
        top_skills,
        key=top_skills.get,
        reverse=True)[
                            :5]

    prev_month_mentors = [0, 0]
    total_mentors = []
    active_mentors = []
    inactive_mentors = []
    month_in_string = []
    for month in months:
        if month in mentors_by_month:
            active, inactive = mentors_by_month[month]
            prev_month_mentors[0] += active
            prev_month_mentors[1] += inactive
        total_mentors.append(prev_month_mentors[0] + prev_month_mentors[1])
        active_mentors.append(prev_month_mentors[0])
        inactive_mentors.append(prev_month_mentors[1])
        month_in_string.append(datetime.date(1900, month, 1).strftime('%B'))

    mentors_act = Mentor.objects.select_related(
        'profile', 'profile__user').filter(
        platform=university, is_active=True)
    num_active = mentors_act.count()

    mentors_inact = Mentor.objects.select_related(
        'profile', 'profile__user').filter(
        platform=university, is_active=False)
    num_inact = mentors_inact.count()

    num_total = num_active + num_inact

    l = len(total_mentors)

    total_mentors[l - 1] = num_total
    active_mentors[l - 1] = num_active
    inactive_mentors[l - 1] = num_inact

    payload['months'] = month_in_string
    payload['total_mentors'] = total_mentors
    payload['active_mentors'] = active_mentors
    payload['inactive_mentors'] = inactive_mentors

    experiences = Experience.objects \
        .select_related('role') \
        .filter(member_id__in=user_ids)
    top_roles = {}
    for exper in experiences:
        if exper.role:
            role = exper.role.name.lower().title()
            if role in top_roles:
                top_roles[role] += 1
            else:
                top_roles[role] = 1
    payload['top_roles'] = sorted(
        top_roles,
        key=top_roles.get,
        reverse=True)[
                           :5]

    return JsonResponse(payload)


@only_university
@login_required
def get_recent_team_activity_full(request, university):
    if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    n = 20
    teams = university.get_team_mentorship_teams()
    mentorship_activity = TeamMentorshipActivity.objects.filter(team__in=teams).order_by('-timestamp')[:n]

    return TemplateResponse(
        request,
        'mentorship/admin/recent_activity_full.html',
        {
            'variable': 'value',
            'mentorship_activity': mentorship_activity
        }

    )


@only_university
@login_required
def get_team_mentorship_chart(request, university):
    """ Analogous to Single Mentorship's: get_mentor_metrics

    (Commented out section outlines what remains be done for
    addint top skills and roles later)
    """
    if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    payload = {'status': 200}
    now = datetime.datetime.now()
    five_months_back = month_delta(now, -5)

    mentors = Mentor.objects.select_related('profile').filter(
        platform_id=university.id,
        is_team_mentor=True,
        assigned_on__range=(five_months_back, now)) \
        .order_by('assigned_on') \
        #     .prefetch_related('profile__skills')

    # mentors_by_month[5] = [10, 20] means
    # in month 5 (May), there are 10 active mentors and 20 inactive mentors added
    mentors_by_month = {}
    months = []
    # top_skills = {}
    # user_ids = []
    for i in range(6):
        tmp = (five_months_back.month + i) % 12
        if tmp == 0:
            tmp = 12
        months.append(tmp)
    for mentor in mentors:
        month = mentor.assigned_on.month
        # user_ids.append(mentor.profile_id)
        #     skills = mentor.profile.skills.all()
        if month in mentors_by_month:
            if mentor.is_active:
                mentors_by_month[month][0] += 1
            else:
                mentors_by_month[month][1] += 1
        else:
            if mentor.is_active:
                mentors_by_month[month] = [1, 0]
            else:
                mentors_by_month[month] = [0, 1]
    #     for skill in skills:
    #         s_name = skill.name.lower().title()
    #         if s_name in top_skills:
    #             top_skills[s_name] += 1
    #         else:
    #             top_skills[s_name] = 1
    # payload['top_skills'] = sorted(
    #     top_skills,
    #     key=top_skills.get,
    #     reverse=True)[
    #     :5]
    #
    prev_month_mentors = [0, 0]
    total_team_mentors = []
    active_team_mentors = []
    inactive_team_mentors = []
    month_in_string = []
    for month in months:
        if month in mentors_by_month:
            active, inactive = mentors_by_month[month]
            prev_month_mentors[0] += active
            prev_month_mentors[1] += inactive
        total_team_mentors.append(prev_month_mentors[0] + prev_month_mentors[1])
        active_team_mentors.append(prev_month_mentors[0])
        inactive_team_mentors.append(prev_month_mentors[1])
        month_in_string.append(datetime.date(1900, month, 1).strftime('%B'))

    team_mentors_act = Mentor.objects.select_related(
        'profile', 'profile__user').filter(
        platform=university, is_active=True, is_team_mentor=True)
    num_active = team_mentors_act.count()

    team_mentors_inact = Mentor.objects.select_related(
        'profile', 'profile__user').filter(
        platform=university, is_active=False, is_team_mentor=True)
    num_inact = team_mentors_inact.count()

    num_total = num_active + num_inact

    l = len(total_team_mentors)

    total_team_mentors[l - 1] = num_total
    active_team_mentors[l - 1] = num_active
    inactive_team_mentors[l - 1] = num_inact

    payload['months'] = month_in_string
    payload['total_team_mentors'] = total_team_mentors
    payload['active_team_mentors'] = active_team_mentors
    payload['inactive_team_mentors'] = inactive_team_mentors

    # experiences = Experience.objects \
    #     .select_related('role') \
    #     .filter(member_id__in=user_ids)
    # top_roles = {}
    # for exper in experiences:
    #     if exper.role:
    #         role = exper.role.name.lower().title()
    #         if role in top_roles:
    #             top_roles[role] += 1
    #         else:
    #             top_roles[role] = 1
    # payload['top_roles'] = sorted(
    #     top_roles,
    #     key=top_roles.get,
    #     reverse=True)[
    #     :5]

    return JsonResponse(payload)


@only_university
@login_required
def get_team_mentorship_metrics(request, university):
    if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
        raise Http404()
    connections = TeamMentorship.objects.filter(mentor__platform_id=university.id).count()

    # HOURS
    mentors = Mentor.objects.filter(platform=university, is_team_mentor=True).count()
    mentor_ids = Mentor.objects.filter(platform=university, is_team_mentor=True) \
        .values_list('id', flat=True)
    now = datetime.datetime.now()
    sessions = TeamMentorshipSession.objects \
        .filter(mentorships__mentor_id__in=mentor_ids,
                datetime__lte=now,
                is_cancelled=False)
    total_minutes = 0
    for session in sessions:
        number_mentors = session.mentorships.count()
        total_minutes += (session.get_duration() * number_mentors)
    hours = total_minutes / 60

    # ACTIONS
    activities = TeamMentorshipActivity.objects.filter(team__university=university).count()
    actions = activities + connections

    return TemplateResponse(
        request,
        'mentorship/admin/dashboard/team_mentorship_metrics.html',
        {
            'num_actions': actions,
            'num_connections': connections,
            'num_mentors': mentors,
            'num_hours': hours
        }
    )


@only_team_mentorship_admin
@only_university
@login_required
def team_manage_metrics(request, university):
    now = timezone.now()
    mentor_ids = Mentor.objects.filter(platform=university).values_list('id', flat=True)
    sessions = TeamMentorshipSession.objects \
        .prefetch_related('mentorships') \
        .filter(mentorships__mentor_id__in=mentor_ids,
                datetime__lte=now,
                is_cancelled=False)

    team_mentor_sessions = {}
    for session in sessions:
        for mentorship in session.mentorships.all():
            mentor_id = mentorship.mentor_id
            if mentor_id in team_mentor_sessions:
                team_mentor_sessions[mentor_id] += 1
            else:
                team_mentor_sessions[mentor_id] = 1

    top_team_mentor_ids = sorted(
        team_mentor_sessions,
        key=team_mentor_sessions.get,
        reverse=True)[:5]

    top_team_mentors = Mentor.objects \
        .filter(id__in=top_team_mentor_ids)

    # dictionary with key: mentor, val: # sessions
    top_team_mentors_dict = {}
    for mentor in top_team_mentors:
        top_team_mentors_dict[mentor] = team_mentor_sessions[mentor.id]

    # Office Hours Metrics
    officehours_team_mentors = Mentor.objects.select_related('platform').filter(
        platform=university, allow_office_hour=True, is_team_mentor=True)
    officehours_teams = Startup.objects \
        .prefetch_related('teammentorship', 'teammentorship__mentor') \
        .filter(teammentorship__mentor__in=officehours_team_mentors).distinct()
    officehours_team_mentees = StartupMember.objects \
        .prefetch_related('membertoadmin', 'membertoadmin__startup') \
        .filter(membertoadmin__startup__in=officehours_teams).distinct()
    num_oo_team_mentors = officehours_team_mentors.count()
    num_oo_team_ventures = officehours_teams.count()
    num_oo_team_mentees = officehours_team_mentees.count()

    total_team_meetings = sessions.count()
    uni_labels, uni_labels_objs = get_manageable_tags_titles_and_objs(request)

    user_timezone = request.session.get('django_timezone')
    team_mentorships = TeamMentorship.objects.filter(mentor_id__in=mentor_ids).values_list('id', flat=True)
    sessions = TeamMentorshipSession.objects.prefetch_related('mentorships', 'team').filter(
        mentorships__id__in=team_mentorships,
        is_cancelled=False, datetime__gte=now).order_by('datetime')[:20]
    entry = []
    for session in sessions:
        team = session.team
        timeslot = session.get_datetime(user_timezone=user_timezone)  # timezone converted for view
        end_time = session.get_endtime(user_timezone=user_timezone)
        date = timeslot.date()
        start_time = timeslot.time()
        end_time = end_time.time()
        mentors = session.get_mentors()
        mentor_names = []
        for mentor in mentors:
            mentor_names.append(mentor.profile.get_full_name())
        entry.append({
            'mentors': ','.join(mentor_names),
            'date': date,
            'start_time': start_time,
            'end_time': end_time,
            'team': {
                'url_name': team.url_name,
                'name': team.name,
                'team_founder_name': team.team_founder_name,
                'team_founder_email': team.team_founder_email
            }
        })

    return TemplateResponse(
        request,
        'trunk/manage/team_manage_metrics.html',
        {
            'university': university,
            "top_team_mentors_dict": top_team_mentors_dict,
            'num_oo_team_mentors': num_oo_team_mentors,
            'num_oo_team_ventures': num_oo_team_ventures,
            'num_oo_team_mentees': num_oo_team_mentees,
            'total_team_meetings': total_team_meetings,
            'entry': entry,
            'uni_labels': uni_labels,
            'uni_labels_objs': uni_labels_objs,
        }
    )


class CustomizeMentorshipEmails(UniversityDashboardView):
    template = 'trunk/manage/custom_mentorship_emails.html'

    def post(self, request, university):
        custom_variables_list = CustomVariable.objects.all().order_by('pk')
        university_style = request.university_style
        default_new_session_mentee = get_new_session_mentee_email_body(university_style)
        default_new_session_mentor = get_new_session_mentor_email_body(university_style)
        default_session_reminder_mentee = get_session_reminder_mentee_email_body(university_style)
        default_session_reminder_mentor = get_session_reminder_mentor_email_body(university_style)

        new_session_mentee_email = MentorshipSessionScheduleMentee(request.POST)
        if new_session_mentee_email.is_valid():
            new_session_mentee_email_body = new_session_mentee_email.cleaned_data[
                'email_text_schedule_mentee']
            if new_session_mentee_email_body == '<br>':
                new_session_mentee_email_body = default_new_session_mentee
        else:
            new_session_mentee_email_body = default_new_session_mentee

        new_session_mentor_email = MentorshipSessionScheduleMentor(request.POST)
        if new_session_mentor_email.is_valid():
            new_session_mentor_email_body = new_session_mentor_email.cleaned_data[
                'email_text_schedule_mentor']
            if new_session_mentor_email_body == '<br>':
                new_session_mentor_email_body = default_new_session_mentor
        else:
            new_session_mentor_email_body = default_new_session_mentor

        session_reminder_mentee_email = SessionReminderMentee(request.POST)
        if session_reminder_mentee_email.is_valid():
            session_reminder_mentee_email_body = session_reminder_mentee_email.cleaned_data[
                'email_text_reminder_mentee']
            if session_reminder_mentee_email_body == '<br>':
                session_reminder_mentee_email_body = default_session_reminder_mentee
        else:
            session_reminder_mentee_email_body = default_session_reminder_mentee


        session_reminder_mentor_email = SessionReminderMentor(request.POST)
        if session_reminder_mentor_email.is_valid():
            session_reminder_mentor_email_body = session_reminder_mentor_email.cleaned_data[
                'email_text_reminder_mentor']
            if session_reminder_mentor_email_body == '<br>':
                session_reminder_mentor_email_body = default_session_reminder_mentor
        else:
            session_reminder_mentor_email_body = default_session_reminder_mentor

        university_style.session_scheduled_mentee_email = new_session_mentee_email_body
        university_style.session_scheduled_mentor_email = new_session_mentor_email_body
        university_style.session_reminder_mentee_email = session_reminder_mentee_email_body
        university_style.session_reminder_mentor_email = session_reminder_mentor_email_body
        university_style.save()

        return self.get(request, university)

    def get(self, request, university):
        custom_variables_list = CustomVariable.objects.all().order_by('pk')
        university_style = request.university_style
        default_new_session_mentee = get_new_session_mentee_email_body(university_style)
        default_new_session_mentor = get_new_session_mentor_email_body(university_style)
        default_session_reminder_mentee = get_session_reminder_mentee_email_body(university_style)
        default_session_reminder_mentor = get_session_reminder_mentor_email_body(university_style)

        if university_style.session_scheduled_mentee_email is not None:
            new_session_mentee_email = university_style.session_scheduled_mentee_email
        else:
            new_session_mentee_email = default_new_session_mentee

        if university_style.session_scheduled_mentor_email is not None:
            new_session_mentor_email = university_style.session_scheduled_mentor_email
        else:
            new_session_mentor_email = default_new_session_mentor

        if university_style.session_reminder_mentee_email is not None:
            session_reminder_mentee_email = university_style.session_reminder_mentee_email
        else:
            session_reminder_mentee_email = default_session_reminder_mentee

        if university_style.session_reminder_mentor_email is not None:
            session_reminder_mentor_email = university_style.session_reminder_mentor_email
        else:
            session_reminder_mentor_email = default_session_reminder_mentor

        new_session_mentee_email = custom_variable_highlight(new_session_mentee_email)
        new_session_mentee_email = MentorshipSessionScheduleMentee(
            {'email_text_schedule_mentee': new_session_mentee_email})

        new_session_mentor_email = custom_variable_highlight(new_session_mentor_email)
        new_session_mentor_email = MentorshipSessionScheduleMentor(
            {'email_text_schedule_mentor': new_session_mentor_email})

        session_reminder_mentee_email = custom_variable_highlight(session_reminder_mentee_email)
        session_reminder_mentee_email = SessionReminderMentee(
            {'email_text_reminder_mentee': session_reminder_mentee_email})

        session_reminder_mentor_email = custom_variable_highlight(session_reminder_mentor_email)
        session_reminder_mentor_email = SessionReminderMentor(
            {'email_text_reminder_mentor': session_reminder_mentor_email})

        return TemplateResponse(
            request,
            self.template,
            {
                'page_name': 'uni-customize-mentorship-emails',
                'university': university,
                'new_session_mentee_email_body': new_session_mentee_email,
                'new_session_mentor_email_body': new_session_mentor_email,
                'session_reminder_mentee_email_body': session_reminder_mentee_email,
                'session_reminder_mentor_email_body': session_reminder_mentor_email,
                'custom_variables': custom_variables_list
            }
        )


@only_university
@login_required
def email_preview(request, university, email_type):
    uni = University.objects.get(id=university.id)
    current_site = uni.site
    uni_style = UniversityStyle.objects.get(_university=uni)

    protocol = PROTOCOL
    url = "{0}://{1}/{2}".format(
        protocol,
        current_site.domain,
        "mentorship/mentee-dashboard"
    )
    # Getting the template of saved email
    email_body = get_template_for_uni(uni_style, email_type)
    session = MentorshipSession.objects.select_related('mentorship__mentor__profile__user',
                                                       'mentorship__mentor__profile',
                                                       'mentorship__mentor',
                                                       'mentorship__student__user',
                                                       'mentorship__student')[0]
    # replacing custom variables with their actual values
    email_body = render_custom_email_variables(email_body, session, uni)
    return TemplateResponse(
        request,
        'email_templates/mentorship/Custom_Email_Base_Template.html',
        {
            'email_body': email_body,
            'uni_logo': uni_style.get_logo(),
            "MEDIA_URL": get_media_url(),
            "STATIC_URL": get_static_url(),
            "mentor_word": uni_style.mentor_word
        }
    )


class TeamLandingEdit(UniversityDashboardView):
    template = 'mentorship/admin/team_mentorship_landing_edit.html'

    def post(self, request, university):

        # get description
        team_landing_description = TinyMCEWidget(request.POST)
        if team_landing_description.is_valid():
            description = team_landing_description.cleaned_data['content']
        else:
            app_logger.error('TextEditorForm2 object from POST is invalid')
            description = ''

        title = request.POST.get('team_landing_title')
        image = request.FILES.get('team_landing_image')
        image_valiadte = EventImageForm(files={'event_image': image})

        team_landing, created = TeamLandingPage.objects.get_or_create(
            university=university,
            defaults={
                'title': title,
                'description': description
            }
        )

        if not created:
            team_landing.title = title
            team_landing.description = description
            team_landing.save()

        if image_valiadte.is_valid():
            uid = uuid.uuid4()
            image_name = parse.quote("{0}_TeamLandingImage_{1}.png".format(university.short_name, uid))
            team_landing.image.save(image_name, image)
        else:
            app_logger.info(image_valiadte.errors)

        return TemplateResponse(
            request,
            self.template,
            {
                'is_saved': True,
                'team_landing_description': team_landing_description,
                'landing': team_landing
            }
        )

    def get(self, request, university):

        try:
            team_landing = TeamLandingPage.objects.get(university=university)
        except TeamLandingPage.DoesNotExist:
            # Intentionally not using TeamLandingPage.objects.create()
            # because we don't want to do a database save until after the POST submission.
            # Instead, we want to pass a dummy object with default values.
            team_landing = TeamLandingPage(
                title='',
                description=''
            )

        description = team_landing.description
        team_landing_description = TinyMCEWidget({'content': description})

        return TemplateResponse(
            request,
            self.template,
            {
                'team_landing_description': team_landing_description,
                'landing': team_landing
            }
        )
