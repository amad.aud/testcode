from django.conf.urls import url, include
from mentorship import views as men_views
from mentorship.views.mentor_signup import MentorSignup
from mentorship.views import teams as team_men_views


urlpatterns = [
    # url(r'^discover$', men_views.discover, name='discover_mentors'),
    url(r'^mentor-dashboard$', men_views.mentor_dashboard, name="mentor-dashboard"),
    url(r'^mentee-dashboard$', men_views.mentee_dashboard, name="mentee-dashboard"),
    url(r'^settings$', men_views.mentorship_setting, name="mentorship-setting"),
    url(r'^request$', men_views.mentor_request, name="mentorship-request"),
    url(r'^request/(?P<request_id>[0-9]+)$', men_views.request_detail, name="mentorship-request-detail"),
    url(r'^manage/mentor/metrics$', men_views.admin.get_mentor_metrics, name="manage-mentor-metrics"),
    url(r'^manage/mentor/add$', men_views.admin.add_mentor, name="manage-mentor-add"),
    url(r'^manage/mentor/team/add$', men_views.admin.add_mentor, {'team': True}, name="manage-team-mentor-add"),
    url(r'^manage/mentor/add_team_to_mentoring$', men_views.admin.add_team_to_mentoring, name="manage-team-add"),
    url(r'^manage/mentor/match_team$', men_views.admin.match_team_to_mentor, name="team-mentor-match"),
    url(r'^manage/mentor/recommend$', men_views.admin.recommend, name="uni-mentorship-recommend"),
    url(r'^manage/mentor/update$', men_views.admin.update_mentor, name="manage-mentor-update"),
    url(r'^manage/mentor/update-settings$', men_views.admin.update_mentor_settings, name="manage-mentor-settings"),
    url(r'^manage/mentor/connect$', men_views.admin.decide_mentorship_request, name="manage-mentor-connect"),
    url(r'^manage/mentor/meeting$', men_views.admin.decide_meeting_request, name="manage-mentor-meeting"),
    # url(r'^mentor/timeslot/edit', men_views.mentor_edit_timeslots, name="mentor-timeslot-edit"),  # deprecated
    url(r'^mentor/timeslot/blackout/edit', men_views.edit_blackout, name='mentorship-schedule-blackout-edit'),
    url(r'^mentor/timeslot/defaults/edit', men_views.edit_defaults, name='mentorship-schedule-defaults-edit'),
    url(r'^mentor/timeslot/one-time/edit', men_views.edit_one_time, name='mentorship-schedule-one-time-edit'),
    url(r'^mentor/timeslot/delete', men_views.avail_delete, name='mentor-avail-delete'),
    url(r'^schedule_by_ids$', men_views.mentorshipsession_add__by_ids, name="mentorship-schedule-add-by-ids"),
    url(r'^session/(?P<session_id>[0-9]+)/',
        include([url(r'^cancel$',
                     men_views.mentorshipsession_cancel,
                     name='mentorship-schedule-cancel'),
                 ])),
    url(r'^mentor/office-hours/(?P<mentor_id>[0-9]+)/(?P<page>[0-9]+)$', men_views.mentor_office_hours,
        name='mentorship-office-hours'),
    url(r'^schedule/meeting$', men_views.try_schedule_meeting, name='mentorship-schedule-meeting'),
    url(r'^session/detail$', men_views.admin.get_session_detail),
    url(r'^dashboard/edit-mentee-details/(?P<mentee_id>[0-9]+)$', men_views.update_mentee_info,
        name='mentee-update-info'),
    url(r'^dashboard/edit-meeting-details/(?P<meeting_id>[0-9]+)$', men_views.update_meeting_info,
        name='update-meeting-info'),
    url(r'^dashboard/fetch-upcoming-times$', men_views.fetch_possible_meeting_times,
        name='fetch-upcoming-meeting-times'),
    url(r'^decide-meeting$', men_views.decide_meeting_request, name='mentorship-decide-meeting'),
    url(r'^create-note$', men_views.create_note, name='mentorship-create-note'),
    url(r'^attached-note/(?P<pk>[0-9]+)$', men_views.NoteAttachedFileView.as_view(), name="mentorship-note-attached"),
    url(r'^create-goals$', men_views.create_goals, name='mentorship-create-goals'),
    url(r'^rate-mentor$', men_views.rate_mentor, name='mentorship-rate-mentor'),
    url(r'^update-goal$', men_views.update_goal, name='mentorship-update-goal'),

    # View_Notes URLS --> mentee_id/mentor_id/start_date/end_date
    url(r'^view-notes/(?P<mentee_id>[0-9]+)/', include([
        url(r'^$', men_views.view_notes, name='mentorship-view-notes'),
        url(r'^filter/$', men_views.view_notes_filter, name='mentorship-view-notes-filter'),
    ])),
    url(r'^note/edit$', men_views.edit_note, name='mentorship-edit-note'),
    url(r'^view-goals$', men_views.view_goals, name='mentorship-view-goals'),
    # url(r'^notes$', men_views.get_student_notes),
    # url(r'^notes/(?P<session_id>[0-9]+)$', men_views.get_session_note),
    # url(r'^notes-gen$', men_views.get_student_notes_gen),
    # url(r'^notes-gen-set$', men_views.set_student_notes_gen),
    url(r'^goals/(?P<session_id>[0-9]+)$', men_views.get_session_goals),
    url(r'^post-goals$', men_views.post_session_goals),
    url(r'^remove-mentee/(?P<mentee_id>[0-9]+)$', men_views.remove_mentee, name='remove-mentee'),
    url(r'^remove-mentor/(?P<mentor_id>[0-9]+)$', men_views.remove_mentor, name='remove-mentor'),
    url(r'^signup$', MentorSignup.as_view(), name='mentor-signup'),

    # legacy URL for backward compatability for mentor invite emails generated before April 15, 2021.
    url(r'^signup/mentor$', MentorSignup.as_view(), name='mentor-signup-legacy'),

    url(r'^schedule-checks', men_views.check_mentors_schedules, name="mentor-sched-checks"),
    url(r'^sched-cross-ref', men_views.multiple_mentor_schedules, name="cross-ref-sched"),
    url(r'^display-multiple-mentors', men_views.display_multiple_mentors, name="display-multiple-mentors"),

    url(r'^team/mentee-dashboard$', team_men_views.TeamMenteeDashboard.as_view(),
        name="team-mentee-dashboard"),
    url(r'^team/mentee-dashboard/(?P<url_name>[-@\w]+)$', team_men_views.TeamMenteeDashboard.as_view(),
        name="team-mentee-dashboard-specific"),
    url(r'^team/mentee-dashboard/(?P<url_name>[-@\w]+)/(?P<page_type>notes|goals|sessions)$',
        team_men_views.TeamMenteeDashboard.as_view(), name="team-mentee-dashboard-type"),
    url(r'^team/notes/add$', team_men_views.TeamCreateNote.as_view(), name="create-team-note"),
    url(r'^team/files/add$', team_men_views.TeamCreateFile.as_view(),
        name="team-dash-mentorshipfile-upload"),
    url(r'^team/note-attached/(?P<pk>[0-9]+)$', men_views.MentorshipFileDownloadView.as_view(),
        name="mentorshipfile-download"),
    url(r'team/goals/add$', team_men_views.TeamCreateGoal.as_view(), name="team-create-goals"),
    url(r'team/goals/edit$', team_men_views.TeamEditGoal.as_view(), name="team-edit-goals"),

    url(r'^team/get-team-session-feedback$', team_men_views.get_team_session_feedback, name='get-team-session-feedback'),
    url(r'^team/submit-team-session-feedback$',
        team_men_views.submit_team_session_feedback, name='submit-team-session-feedback'),
    url(r'^team/delete-team-session-feedback$',
        team_men_views.delete_team_session_feedback, name='delete-team-session-feedback'),

    url(r'^team/mentor-dashboard$', team_men_views.TeamMentorDashboard.as_view(), name="team-dash-mentor"),
    url(r'^team/mentor-dashboard/(?P<url_name>[-@\w]+)$', team_men_views.TeamMentorDashboard.as_view(),
        name="team-mentor-dashboard-specific"),
    url(r'^team/mentor-dashboard/(?P<url_name>[-@\w]+)/(?P<page_type>notes|goals|sessions)$',
        team_men_views.TeamMentorDashboard.as_view(), name="team-mentor-dashboard-type"),

    url(r'^team/schedule_by_team_id$', team_men_views.team_mentorshipsession_add__by_team_id,
        name="add-team-session-by-team-id"),
    url(r'^team/session/(?P<session_id>[0-9]+)/',
        include([url(r'^cancel$',
                     team_men_views.team_mentorshipsession_cancel,
                     name='team-mentorship-schedule-cancel'),
                 url(r'^drop$',
                     team_men_views.drop_team_meeting,
                     name='team-mentorship-session-drop')
                 ])),
    url(r'^remove-team-mentor$', team_men_views.remove_team_mentor, name="remove-team-mentor"),
    url(r'^remove-team$', team_men_views.remove_team_mentee, name="remove-team"),
    url(r'^team/landing$', team_men_views.team_mentorship_landing, name="team_mentorship_landing"),

    url(r'^session/(?P<session_id>[0-9]+)/ics_export$', men_views.export_session_to_ics, name="session_ics_export"),
    url(r'^refresh-cronofy-cache$', men_views.refresh_cronofy_cache_view, name='refresh-cronofy-cache'),
]
