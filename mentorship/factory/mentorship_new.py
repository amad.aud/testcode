import datetime as dt
from branch.models import StartupMember
from emailuser.factory import UserEmailFactory
from emailuser.models import UserEmail
from mentorship.models import Mentor, MentorSchedule, Mentorship, MentorshipSession, MentorshipRequest, \
    MentorScheduleBlackOut, CronofyCacheSlot
from trunk.models import University, UniversityStyle
from typing import Iterable, List
from datetime import datetime
from django.urls import reverse
from django.contrib import auth


def generate_member(first_name: str,
                    last_name: str,
                    email_address: str,
                    universities: Iterable[University]) -> StartupMember:
    if UserEmail.objects.filter(email=email_address).exists():
        UserEmail.objects.get(email=email_address).delete()

    user_email = UserEmailFactory(email=email_address)
    return StartupMember.objects.create_sm(
        first_name, last_name, user_email=user_email, universities=universities)


def generate_mentor(profile: StartupMember, platform: University) -> Mentor:
    mentor, _ = Mentor.objects.get_or_create_mentor(profile, platform)
    return mentor


def generate_mentorship(mentee: StartupMember, mentor: Mentor):
    if Mentorship.objects.filter(student=mentee, mentor=mentor).exists():
        Mentorship.objects.get(student=mentee, mentor=mentor).delete()
    mentorship = Mentorship.objects.create(student=mentee, mentor=mentor, is_active=True)
    if MentorshipRequest.objects.filter(requested_mentorship=mentorship):
        MentorshipRequest.objects.get(requested_mentorship=mentorship).delete()
    MentorshipRequest.objects.create(requested_mentorship=mentorship, is_admin_accepted=True)
    return mentorship


def is_mentorship(mentee: StartupMember, mentor: Mentor):
    return Mentorship.objects.filter(student=mentee, mentor=mentor).exists()



def is_mentorship_active(mentee: StartupMember, mentor: Mentor):
    if Mentorship.objects.filter(student=mentee, mentor=mentor).exists():
        return Mentorship.objects.get(student=mentee, mentor=mentor).is_active
    return False


def get_mentorship(mentee: StartupMember, mentor: Mentor):
    if Mentorship.objects.filter(student=mentee, mentor=mentor).exists():
        return Mentorship.objects.get(student=mentee, mentor=mentor)
    return False


def get_mentorship_request(mentee: StartupMember, mentor: Mentor):
    if Mentorship.objects.filter(student=mentee, mentor=mentor).exists():
        mentorship = Mentorship.objects.get(student=mentee, mentor=mentor)
        MentorshipRequest.objects.filter(requested_mentorship=mentorship).exists()
        mentorship_request = MentorshipRequest.objects.get(requested_mentorship=mentorship)
        return mentorship_request
    return False


def is_mentorship_session(mentee: StartupMember, mentor: Mentor):
    if Mentorship.objects.filter(student=mentee, mentor=mentor).exists():
        mentorship = Mentorship.objects.get(student=mentee, mentor=mentor)
        if MentorshipSession.objects.filter(mentorship=mentorship).exists():
            return True
        return False
    return False


def get_mentorship_session(mentee: StartupMember, mentor: Mentor):
    if Mentorship.objects.filter(student=mentee, mentor=mentor).exists():
        mentorship = Mentorship.objects.get(student=mentee, mentor=mentor)
        if MentorshipSession.objects.filter(mentorship=mentorship).exists():
            return MentorshipSession.objects.get(mentorship=mentorship)

    else:
        return False


def mentee_request(client, mentor):
    payload = {
        "m_id": mentor.profile.url_name,
        "venture-name-id": "",
        "pitch": "abcd testing",
        "other": "xyz"
    }
    response = client.post(reverse("mentorship-request"), payload)
    return response


def admin_set_meeting(client, mentee, mentor):
    payload = {
        "start_time": get_meeting_time(),
        "duration": "2",
        "year": datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[2],
        "date": datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[0]
                + ' ' + datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[1],
        "meeting_type": 3,
        "location": "Video Call",
        "selected_mentee_id": mentee.id,
        "selected_mentor_id": mentor.id
    }
    response = client.post(reverse("mentorship-schedule-add-by-ids"), payload)
    return response



def set_mentor_for_approval(mentor: Mentor):
    mentor.approve_meetings = True
    mentor.save()



def mentor_response_action(client, mentorship, action, reason=""):
    # action = decline,approve
    payload = {
        "meeting_id": mentorship.id,
        "decision": action,
        "reason": reason
    }
    response = client.post(reverse("mentorship-decide-meeting"), payload)
    return response


def admin_response_action(client, mentorship, action, reason=""):
    # action = decline,approve
    payload = {
        "r_id": mentorship.id,
        "is_accept": action,
        "reason": reason
    }
    response = client.post(reverse("uni-meeting-decide"), payload)
    return response


def admin_response_action_mentorship(client, mentorshipRequest, action, reason=""):
    # action = true, false
    payload = {
        "r_id": mentorshipRequest.id,
        "is_accept": action,
        "reason": reason
    }
    response = client.post(reverse("uni-mentorship-decide"), payload)
    return response


def generate_mentor_schedule(mentor: Mentor,
                             date: dt.date,
                             start_time: dt.datetime,
                             end_time: dt.datetime,
                             duration: int,
                             is_repeating: bool):
    return MentorSchedule.objects.create(
        mentor=mentor, date=date, start_time=start_time, end_time=end_time,
        duration=duration, is_repeating=is_repeating)


def generate_mentorship_session(mentorship: Mentorship, datetime: dt.datetime, duration: int):
    return MentorshipSession.objects.create(mentorship=mentorship, datetime=datetime, duration=duration)


def generate_mentorship_schedule_blackout(mentor: Mentor, start_date: dt.date, end_date: dt.date):
    return MentorScheduleBlackOut.objects.create(mentor=mentor, start_date=start_date, end_date=end_date)



def get_meeting_time():
    """
    it will return the meeting time, started from
    very next hour of current time
    """
    time = (datetime.now() - dt.timedelta(minutes=-30)).strftime("%b %d %Y %I:%M%p").split(" ")[-1]
    just_time = time.split(":")[0]
    just_time = int(just_time) + 1
    if just_time > 12:
        time_zone = "PM"
    else:
        time_zone = "AM"
    just_time = str(just_time) + ":00" + time_zone
    return just_time


def initiate_meeting(client, mentor):
    # / mentorship / schedule / meeting
    data = {
        'count': 6,
        'm_id': mentor.profile.url_name,
        'start_time': get_meeting_time(),
        'duration': 2,
        'year': datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[2],
        'date': datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[0]
                + ' ' + datetime.now().strftime("%b %d %Y %I:%M%p").split(" ")[1],
        'day': 'null',
        'type': 4,
        'location': "None",
        'needs_approval': False,
        'meeting_topic': 'undefined',
        'mentee_timezone': 'Asia/Karachi'
    }
    response = client.post(reverse("mentorship-schedule-meeting"), data)
    return response


class GenerateAll:
    """
    Generates Mentors and Mentees with the following name/email format (where number is the mentor/mentee number).
        Example Mentor:
            first_name: 'FMentor2'
            last_name: 'LMentor2'
            email: 'mentor2@hello.com'
            password: 1234
        Example Mentee:
            first_name: 'FMentee3'
            last_name: 'LMentee3'
            email: 'mentee3@hello.com'
            password: 1234
    Also generates Mentorships such that:
        Mentor #0 gets connected with all Generated Mentees.
        Mentee #0 gets connected with all Generated Mentors.
    generate() returns a dictionary of created objects.
    delete() deletes all created objects.
    """

    def __init__(self, num_mentors: int, num_mentees: int, university: University):
        self.num_mentors: int = num_mentors
        self.num_mentees: int = num_mentees
        self.university: University = university
        self.mentors: List[Mentor] = []
        self.mentees: List[StartupMember] = []
        self.mentorships: List[Mentorship] = []

    def generate(self):
        self.__generate_mentors()
        self.__generate_mentees()
        self.__generate_mentorships()

        return {
            'mentors': self.mentors,
            'mentees': self.mentees,
            'mentorships': self.mentorships,
        }

    def delete(self):
        for mentor in self.mentors:
            mentor.delete()
        for mentee in self.mentees:
            mentee.delete()
        for mentorship in self.mentorships:
            mentorship.delete()

    def deleteMentorship(self):
        for mentorship in self.mentorships:
            mentorship.delete()

    @staticmethod
    def __make_name(i: int, is_first_name: bool, is_mentor: bool):
        return f"{'F' if is_first_name else 'L'}{'Mentor' if is_mentor else 'Mentee'}{i}"

    @staticmethod
    def __make_email(i: int, is_mentor: bool):
        return f"{'mentor' if is_mentor else 'mentee'}{i}@hello.com"

    def __make_person(self, i: int, is_mentor: bool):
        first_name = GenerateAll.__make_name(i, is_first_name=True, is_mentor=is_mentor)
        last_name = GenerateAll.__make_name(i, is_first_name=False, is_mentor=is_mentor)
        email = GenerateAll.__make_email(i, is_mentor)
        return generate_member(first_name, last_name, email, [self.university])

    def __generate_mentors(self):
        for i in range(self.num_mentors):
            mentor = generate_mentor(self.__make_person(i, is_mentor=True), self.university)
            self.mentors.append(mentor)

    def __generate_mentees(self):
        for i in range(self.num_mentees):
            mentee = self.__make_person(i, is_mentor=False)
            self.mentees.append(mentee)

    def __generate_mentorships(self):
        """Mentee 0 connected with all Mentors. Mentor 0 connected with all Mentees."""
        mentee = StartupMember.objects.get(user__email=GenerateAll.__make_email(0, is_mentor=False))
        mentor = Mentor.objects.get(
            profile__user__email=GenerateAll.__make_email(0, is_mentor=True))
        for mentor in self.mentors:
            self.mentorships.append(generate_mentorship(mentee, mentor))
        for mentee in self.mentees:

            self.mentorships.append(generate_mentorship(mentee, mentor))


    def generate_mentorship_schedule(self, is_repeating=False):
        """
        generating single mentorship schedule for testing
        """

        mentor = Mentor.objects.get(
            profile__user__email=GenerateAll.__make_email(0, is_mentor=True))
        generate_mentor_schedule(mentor, dt.date.today(),

                                 datetime.strptime(datetime.now().strftime("%b %d %Y %I:%M%p"), '%b %d %Y %I:%M%p'),
                                 datetime.strptime((datetime.now() -
                                                    dt.timedelta(hours=-3)).strftime("%b %d %Y %I:%M%p"),
                                                   '%b %d %Y %I:%M%p'), 2, is_repeating)

    def generate_mentorship_schedule_mentor(self, mentor: Mentor, is_repeating=False):
        """
        generating single mentorship schedule for testing
        """
        generate_mentor_schedule(mentor, dt.date.today(),
                                 datetime.strptime(datetime.now().strftime("%b %d %Y %I:%M%p"), '%b %d %Y %I:%M%p'),
                                 datetime.strptime((datetime.now() -
                                                    dt.timedelta(hours=-3)).strftime("%b %d %Y %I:%M%p"),
                                                   '%b %d %Y %I:%M%p'), 2, is_repeating)

    def generate_mentorship_schedule_blackout(self):
        """
        generating single mentorship schedule for blackout day
        """

        mentor = Mentor.objects.get(
            profile__user__email=GenerateAll.__make_email(0, is_mentor=True))
        # blackout is for next 3 days
        generate_mentorship_schedule_blackout(mentor, dt.date.today(), dt.date.today() + dt.timedelta(days=3))


    def generate_mentorship_schedule_blackout_mentor(self, mentor):

        # blackout is for next 3 days

        generate_mentorship_schedule_blackout(mentor, dt.date.today(), dt.date.today() + dt.timedelta(days=3))


    def generate_mentorship_session(self):
        """
        generating single mentorship Session for testing
        """

        mentee = StartupMember.objects.get(user__email=GenerateAll.__make_email(0, is_mentor=False))
        mentor = Mentor.objects.get(
            profile__user__email=GenerateAll.__make_email(0, is_mentor=True))
        mentorship = Mentorship.objects.get(student=mentee, mentor=mentor, is_active=True)

        generate_mentorship_session(mentorship,
                                    datetime.strptime(datetime.now().strftime("%b %d %Y %I:%M%p"), '%b %d %Y %I:%M%p'),
                                    2)

    def get_mentorship(self):
        """
        Getting single object of mentorship model
        """

        mentee = self.mentees[0]
        mentor = self.mentors[0]

        mentorship = Mentorship.objects.get(student=mentee, mentor=mentor, is_active=True)
        return mentorship

    def get_mentor(self):
        return self.mentors[0]

    def get_mentor_single(self, number):
        return self.mentors[number]

    def get_mentee(self):
        return self.mentees[0]

    def get_mentee_single(self, number):
        return self.mentees[number]

    def get_mentor_blackout(self):

        mentor = Mentor.objects.get(
            profile__user__email=GenerateAll.__make_email(0, is_mentor=True))
        return MentorScheduleBlackOut.objects.get(mentor=mentor)


    def get_mentor_blackout_for_mentor(self, mentor):
        return MentorScheduleBlackOut.objects.get(mentor=mentor)

    def get_mentor_blackout_mentor(self, mentor):
        return MentorScheduleBlackOut.objects.get(mentor=mentor)


    def login_user(self, client, username):
        response = client.post(reverse("api-default-login"),
                               {'email': username,
                                'password': '1234',
                                'device_id': 12243}, format='json')
        return response

    def activate_admin_approval(self, mentor):
        mentor.allow_office_hour = True
        mentor.save()
        uni = University.objects.get(id=mentor.platform.id)
        uni_style = UniversityStyle.objects.get(_university=uni)
        uni_style.require_meeting_approval = 2
        uni_style.save()

    def activate_mentor_approval(self, mentor):
        mentor.approve_meetings = True
        mentor.save()

    def accept_session_admin(self, mentor_session):
        mentor_session.is_admin_accepted = True
        mentor_session.save()

    def reject_session_admin(self, mentor_session):
        mentor_session.is_mentor_accepted = False
        mentor_session.save()

    def blackout_check(self, mentor_session):
        if not self.get_mentor_blackout_for_mentor(mentor_session.mentorship.mentor).is_expired():
            mentor_session.is_cancelled = True
            mentor_session.save()

    def activate_mentor_cronofy(self, mentor):
        if CronofyCacheSlot.objects.filter(mentor_user=mentor.profile.user, is_active=True).exists():
            return True
        else:
            CronofyCacheSlot.objects.create(mentor_user=mentor.profile.user,
                                            start_datetime=dt.date.today(),
                                            end_datetime=dt.date.today() - dt.timedelta(hours=-3),
                                            is_active=True)

    def is_mentor_cronofy(self, mentor):
        if CronofyCacheSlot.objects.filter(mentor_user=mentor.profile.user, is_active=True).exists():
            return True
        else:
            return False