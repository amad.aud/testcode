from branch.models import StartupMember, UniversityAffiliation
from branch.factory import StartupMemberFactory
from branch.factory.user_data import generate_member
from mentorship.models import (
    Mentor,
    MentorManager,
    Mentorship,
    MentorshipManager,
    MentorshipRequest,
    MentorshipSession
)
from trunk.factory import UniversityFactory
from django.utils import timezone
import factory


class MentorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Mentor
    profile = factory.SubFactory(StartupMemberFactory)
    platform = factory.SubFactory(UniversityFactory)


class MentorshipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Mentorship
    student = factory.SubFactory(StartupMemberFactory)
    mentor = factory.SubFactory(MentorFactory)


class MentorshipRequestFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MentorshipRequest
    requested_mentorship = factory.SubFactory(MentorshipFactory)


class MentorshipSessionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MentorshipSession
    mentorship = factory.SubFactory(MentorshipFactory)
    datetime = factory.LazyFunction(timezone.now)


def generate_student(firstname, lastname, useremail, university):
    #Upadted the arguements here from useremail to email and added uni, not sure if there being used still in generate_member, need revision
    new_member = generate_member(
        uni=university,
        firstname=firstname,
        lastname=lastname,
        email=useremail)

    new_student = UniversityAffiliation.objects.create(
        member=new_member,
        university=university,
        affiliation=UniversityAffiliation.UNIVERSITY_STUDENT)

    return new_student;


def generate_mentor(firstname, lastname, useremail, university):
    new_member = generate_member(
        firstname=firstname,
        lastname=lastname,
        useremail=useremail)

    new_mentor = Mentor.objects.get_or_create_mentor(
        profile=new_member,
        platform=university)

    return new_mentor


def generate_mentorship(fn_mentor, ln_mentor, ue_mentor, uni_mentor, fn_mentee, ln_mentee, ue_mentee, uni_mentee):
    new_student = generate_student(
        firstname=fn_mentee,
        lastname=ln_mentee,
        useremail=ue_mentee,
        university=uni_mentee)

    new_mentor = generate_mentor(
        firstname=fn_mentor,
        lastname=ln_mentor,
        useremail=ue_mentor,
        university=uni_mentor)

    new_mentorship = Mentorship.objects.create_new_mentorship(
        student=new_student,
        mentor=new_mentor)

    return new_mentorship
