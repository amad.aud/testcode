from datetime import datetime, timedelta
from dateutil import parser
from django.db import transaction
from django.utils import timezone
from emailuser.models import UserEmail
from mentorship.constants import IS_CRONOFY_LINKED_QUERY, CRONOFY_CACHE_DAYS_FUTURE
from mentorship.models import (
    CronofyCacheSlot,
    Mentor,
)
from hamster.config import Roborovski


def process_raw_slot(mentor_user, raw_slot):
    """
    Given a cronofy slot (output from st_cronofy.get_slots()), make the corresponding
    CronofyCache object.
    """
    start_raw = raw_slot['start']
    start_dt = parser.parse(start_raw)

    end_raw = raw_slot['end']
    end_dt = parser.parse(end_raw)

    if CronofyCacheSlot.objects.filter(mentor_user=mentor_user, start_datetime=start_dt).exists():
        # Because of uniqueness constraint, we can use `get`.
        cache_slot = CronofyCacheSlot.objects.get(mentor_user=mentor_user, start_datetime=start_dt)
        cache_slot.end_datetime = end_dt
        cache_slot.timestamp = timezone.now()
        cache_slot.is_active = True
        cache_slot.save()
    else:
        CronofyCacheSlot.objects.create(
            mentor_user=mentor_user,
            start_datetime=start_dt,
            end_datetime=end_dt,
            is_active=True,
        )


def refresh_cronofy_cache__user(mentor_user, skip_cache_deactivation=False):
    """
    refreshes the cache of cronofy availability for a specific mentor.

    See CronofyCacheSlot specs for more info.

    (If user is not mentor, we simply return without doing anything.)

    By default, we deactivate future slots before re-evaluating them. However, we can skip
    this deactivation if it has already been done (eg during refresh_cronofy_cache() call.)

    If mentor is not cronofy-synced, then we just delete future cronofy cache slots (assuming not skipping cleanup) and
    don't create any new CronofyCacheSlot objects.
    """
    from mentorship import st_cronofy  # imported here to prevent circular import

    # Don't do anything if this is not a mentor.
    if not Mentor.objects.filter(profile__user=mentor_user).exists():
        return

    if not skip_cache_deactivation:
        CronofyCacheSlot.objects.filter(mentor_user=mentor_user, start_datetime__gt=timezone.now()).update(is_active=False)

    # If mentor no longer cronofy-linked, no need to check for future availability.
    if not st_cronofy.is_cronofy_linked(mentor_user):
        return

    start = datetime.utcnow()
    end = start + timedelta(days=CRONOFY_CACHE_DAYS_FUTURE)

    uni = Mentor.objects.filter(profile__user=mentor_user).first().platform
    raw_slots = st_cronofy.get_slots(mentor_user, uni, is_mentor=True, range=(start, end))

    for raw_slot in raw_slots:
        process_raw_slot(mentor_user, raw_slot)


def refresh_cronofy_cache():
    """ Refreshes the cache of cornofy availability.

    See CronofyCacheSlot specs for more info.
    """

    # Inactivate all future slots before re-evaluating them.
    CronofyCacheSlot.objects.filter(start_datetime__gt=timezone.now()).update(is_active=False)

    mentor_users = UserEmail.objects.filter(IS_CRONOFY_LINKED_QUERY, startup_member__mentor_profile__is_active=True)

    for mentor_user in mentor_users:
        # We skip cache deactivation because we already did that in
        # CronofyCacheSlot.objects...update(is_active=False) call earlier
        refresh_cronofy_cache__user(mentor_user, skip_cache_deactivation=True)


@Roborovski.task
def task__refresh_cronofy_cache__user(user_id):
    mentor_user = UserEmail.objects.get(id=user_id)
    refresh_cronofy_cache__user(mentor_user)


def async__refresh_cronofy_cache__user(user):
    transaction.on_commit(lambda: task__refresh_cronofy_cache__user.delay(user.id))