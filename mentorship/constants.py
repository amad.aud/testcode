from datetime import timedelta
from django.utils import timezone
from django.db.models import Q


NOT_FOUND = -1
ONE_YEAR_AGO = timezone.now() - timedelta(days=365)

MENTORSHIPSESSION_LOCK = "mentorshipsession-create-lock"

# eg:  UserEmail.objects.filter(IS_CRONOFY_LINKED_QUERY) returns queryset of all UserEmail objects that we
# consider to be cronofy-linked.
IS_CRONOFY_LINKED_QUERY = Q(cronofy_access_token__isnull=False) & Q(cronofy_refresh_token__isnull=False) \
                          & Q(cronofy_token_expiration__isnull=False)

# According to https://docs.cronofy.com/developers/api/scheduling/availability/
# participants.members.available_periods.start Must be between 1 minute and 35 days after the corresponding start.
#
# The next largest integer is 34 (as 35 is not allowed). For simplicity, instead of 34.99.. days, we use 34 days.
CRONOFY_QUERY_SIZE_MAX_DAYS = 34

# How far into the future we keep the cache for.
CRONOFY_CACHE_DAYS_FUTURE = 34  # Due to error in Cronofy UI. Instaed of 84 days

