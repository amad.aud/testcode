# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-13 12:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentorship', '0013_auto_20180808_1134'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mentorscheduleonetime',
            name='mentor',
        ),
        migrations.DeleteModel(
            name='MentorScheduleOneTime',
        ),
    ]
