# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-03-05 15:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mentorship', '0021_mentor_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='mentorshipsession',
            name='is_admin_accepted',
            field=models.NullBooleanField(default=None),
        ),
    ]
