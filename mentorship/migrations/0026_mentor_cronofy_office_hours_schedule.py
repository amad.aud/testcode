# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-05-03 15:20
from __future__ import unicode_literals

from django.db import migrations
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mentorship', '0025_merge_20190329_1717'),
    ]

    operations = [
        migrations.AddField(
            model_name='mentor',
            name='cronofy_office_hours_schedule',
            field=picklefield.fields.PickledObjectField(editable=False, null=True),
        ),
    ]
