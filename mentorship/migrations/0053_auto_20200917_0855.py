# Generated by Django 2.2.15 on 2020-09-17 12:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('branch', '0052_auto_20200824_2002'),
        ('mentorship', '0052_auto_20200707_2031'),
    ]

    operations = [
        migrations.CreateModel(
            name='MentorshipSessionFeedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('rating', models.PositiveSmallIntegerField(choices=[(0, 'No input yet'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, 'No Show')], default=0)),
                ('feedback', models.TextField(null=True)),
                ('is_from_mentor', models.BooleanField(default=True)),
                ('from_member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='from_startupmember', to='branch.StartupMember')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mentorship_session', to='mentorship.MentorshipSession')),
                ('to_member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='to_startupmember', to='branch.StartupMember')),
            ],
        ),
        migrations.AddConstraint(
            model_name='mentorshipsessionfeedback',
            constraint=models.UniqueConstraint(fields=('session', 'to_member', 'from_member'), name='unique_session_feedback'),
        ),
    ]
