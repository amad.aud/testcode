# Generated by Django 2.2.6 on 2019-12-17 19:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mentorship', '0038_remove_teammentorshipactivity_is_timestamp_estimated'),
    ]

    operations = [
        migrations.AddField(
            model_name='mentor',
            name='approve_meetings',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mentorshipsession',
            name='is_mentor_accepted',
            field=models.NullBooleanField(default=None),
        ),
    ]
