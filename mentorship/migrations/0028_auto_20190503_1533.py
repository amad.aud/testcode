# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-05-03 15:33
from __future__ import unicode_literals

from django.db import migrations
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mentorship', '0027_auto_20190503_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mentor',
            name='cronofy_office_hours_schedule',
            field=picklefield.fields.PickledObjectField(default={'Fri': '', 'Mon': '', 'Sat': '', 'Sun': '', 'Thu': '', 'Tue': '', 'Wed': ''}, editable=False),
        ),
    ]
