# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-10 06:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('branch', '0001_initial'),
        ('trunk', '0001_initial'),
        ('mentorship', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mentor',
            name='platform',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trunk.University'),
        ),
        migrations.AddField(
            model_name='mentor',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='branch.StartupMember'),
        ),
        migrations.AddField(
            model_name='goal',
            name='session',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mentorship.MentorshipSession'),
        ),
    ]
