# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-04-01 15:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mentorship', '0008_auto_20171219_0659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mentor',
            name='preferred_contact',
            field=models.SmallIntegerField(choices=[(1, 'Phone'), (2, 'StartupTree Messaging'), (3, 'Video Call'), (4, 'In Person')], default=2),
        ),
        migrations.AlterField(
            model_name='mentorshipdefaults',
            name='preferred_contact',
            field=models.SmallIntegerField(choices=[(1, 'Phone'), (2, 'StartupTree Messaging'), (3, 'Video Call'), (4, 'In Person')], default=2),
        ),
    ]
