from django import forms
from pytz import common_timezones
from mentorship.models import Mentor, MentorshipFile
from StartupTree.utils import file_whitelist_validator
from django.utils.translation import gettext_lazy as _


class MentorContactForm(forms.ModelForm):

    class Meta:
        model = Mentor
        fields = ['email', 'phone', 'skype']

        widgets = {
            'email': forms.TextInput(attrs={'class': 'mentor_form mentor_form--input'}),
            'phone': forms.TextInput(attrs={'class': 'mentor_form mentor_form--input'}),
            'skype': forms.TextInput(attrs={'class': 'mentor_form mentor_form--input'})
        }

    def save(self, mentor):
        mentor.email = self.cleaned_data['email']
        mentor.phone = self.cleaned_data['phone']
        mentor.skype = self.cleaned_data['skype']
        mentor.save()


class MentorDefaultForm(forms.ModelForm):
    '''Form for mentor to set default settings'''
    class Meta:
        model = Mentor
        fields = ['default_meeting_type', 'default_location', 'default_duration',
                  'allow_office_hour', 'approve_meetings', 'number_meeting_times',
                  'availability_note']

        widgets = {
            'default_meeting_type': forms.Select(attrs={'class': 'mentor_form mentor_form--input'}),
            'default_location': forms.TextInput(attrs={'class': 'mentor_form mentor_form--input'}),
            'default_duration': forms.Select(attrs={'class': 'mentor_form mentor_form--input'}),
            'allow_office_hour': forms.CheckboxInput(),
            'approve_meetings': forms.CheckboxInput(),
            'number_meeting_times': forms.NumberInput(attrs={'class': 'mentor_form mentor_form--input'}),
            'availability_note': forms.TextInput(attrs={'class': 'mentor_form mentor_form--input'}),
        }

    def save(self, mentor):
        mentor.default_meeting_type = self.cleaned_data['default_meeting_type']
        mentor.default_location = self.cleaned_data['default_location']
        mentor.default_duration = self.cleaned_data['default_duration']
        mentor.allow_office_hour = self.cleaned_data['allow_office_hour']
        mentor.approve_meetings = self.cleaned_data['approve_meetings']
        mentor.number_meeting_times = self.cleaned_data['number_meeting_times']
        mentor.availability_note = self.cleaned_data['availability_note']
        mentor.save()


class AdminMentorDefaultForm(forms.ModelForm):
    '''Form for an admin to set a mentor's default settings'''
    class Meta:
        model = Mentor
        fields = ['default_meeting_type', 'default_location', 'default_duration']

        widgets = {
            'default_meeting_type': forms.Select(attrs={'class': 'mentor_form mentor_form--input'}),
            'default_location': forms.TextInput(attrs={'class': 'mentor_form mentor_form--input'}),
            'default_duration': forms.Select(attrs={'class': 'mentor_form mentor_form--input'})
        }

    def save(self, mentor):
        mentor.default_meeting_type = self.cleaned_data['default_meeting_type']
        mentor.default_location = self.cleaned_data['default_location']
        mentor.default_duration = self.cleaned_data['default_duration']
        mentor.save()


class MentorshipFileForm(forms.ModelForm):
    class Meta:
        model = MentorshipFile
        fields = ['file', 'comment']