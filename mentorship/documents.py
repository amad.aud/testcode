from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from StartupTree.config import search as sconfig
from mentorship.models import Mentor
from trunk.documents_format import UNIVERSITY_FORMAT, SIMPLE_UNIVERSITY_FORMAT
from StartupTree.utils.search import ngram_analyzer, html_strip
from branch.documents_format import SKILL_FORMAT, TAG_FORMAT ,GROUP_FORMAT, ROLE_FORMAT


@registry.register_document
class MentorDocument(Document):
    university = fields.NestedField(attr='platform', properties=SIMPLE_UNIVERSITY_FORMAT)
    url = fields.KeywordField()
    image = fields.KeywordField()
    name = fields.KeywordField()
    url_name = fields.KeywordField()

    bio = fields.TextField(analyzer=html_strip)
    skills = fields.NestedField(properties=SKILL_FORMAT)
    tags = fields.NestedField(properties=TAG_FORMAT)
    groups = fields.NestedField(properties=GROUP_FORMAT)
    roles = fields.NestedField(properties=ROLE_FORMAT)

    email = fields.KeywordField()

    doc_type = fields.KeywordField()
    suggest = fields.TextField(analyzer=ngram_analyzer)

    class Index:
        name = sconfig.MENTOR_DOCUMENT

    class Django:
        model = Mentor
        fields = [
            'is_active',
            'is_complete',
            'is_team_mentor'
        ]

    def get_queryset(self):
        return super(MentorDocument, self).get_queryset().select_related('profile', 'profile__user')

    def prepare_suggest(self, instance):
        return instance.profile.get_full_name()

    def prepare_doc_type(self, instance):
        return sconfig.MENTOR_DOCUMENT

    def prepare_name(self, instance):
        return instance.profile.get_full_name()

    def prepare_image(self, instance):
        return instance.profile.get_image()

    def prepare_url(self, instance):
        return instance.profile.get_url()

    def prepare_url_name(self, instance):
        return instance.profile.url_name

    def prepare_tags(self, instance):
        return list(instance.profile.tags.all().values())

    def prepare_groups(self, instace):
        return list(instace.profile.get_groups().all().values())

    def prepare_skills(self, instance):
        return list(instance.profile.skills.all().values())

    def prepare_roles(self, instance):
        return list(instance.profile.get_roles().all().values())

    def prepare_email(self, instance):
        return instance.profile.user.email

    def prepare_bio(self, instance):
        return instance.profile.bio
