import datetime
from dateutil.relativedelta import relativedelta
from mentorship.models import (Mentorship)



def get_list_of_mentorship_users(university_id,start_date_filter=None,end_date_filter=None):
    '''
        # List of users (mentee's and mentors) that
        # -> had a MentorshipSession
        # -> on a given site
        # -> inside a certain date range
    '''
    if(not(start_date_filter and end_date_filter)):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
    mentee_users = Mentorship.objects.filter(mentorshipsession__datetime__range=[start_date_filter, end_date_filter], \
                                                mentor__platform__id=university_id) \
                                                .distinct('student_id') \
                                                .values_list('student_id',flat=True)
    mentor_users = Mentorship.objects.filter(mentorshipsession__datetime__range=[start_date_filter, end_date_filter], \
                                                mentor__platform__id=university_id) \
                                                .distinct('mentor__profile_id') \
                                                .values_list('mentor__profile_id',flat=True)
    return list(mentee_users) + list(mentor_users)
