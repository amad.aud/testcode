from branch.serializer.member import sm_to_dict
from StartupTree.loggers import app_logger


def mentor_to_dict(mentor, university_id):
    profile = sm_to_dict(mentor.profile, university_id, simple=True, degree=False)
    profile["image"] = mentor.profile.get_image()
    profile["preferred_contact"] = mentor.get_preferred_contact()
    profile["is_active"] = mentor.is_active
    profile["timezone"] = mentor.timezone
    profile["id"] = mentor.id
    profile['last_login'] = mentor.profile.user.last_login
    profile['available'] = mentor.has_schedule() or mentor.has_onetimes()
    profile['allow_office_hour'] = mentor.allow_office_hour
    return profile


def scores_to_dict(mentor, university, score, matched_roles, matched_skills):
    profile = sm_to_dict(mentor.profile, university.id, simple=True, degree=True)
    profile['score'] = score
    profile['matched_roles'] = matched_roles
    profile['matched_skills'] = matched_skills
    return profile
