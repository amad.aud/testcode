from __future__ import unicode_literals
import boto
from datetime import timedelta
import datetime as dt
from django.db import models, IntegrityError, transaction
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from pytz import common_timezones
import pytz
from typing import Optional

# Project Imports
from branch.models import StartupMember, Startup
from emailuser.models import UserEmail
from forms.utils import get_attached_file_url
from mentorship.utils import get_mentorship_file_url, get_session_type_and_location
from StartupTree import strings, settings
from StartupTree.loggers import app_logger
from StartupTree.utils import check_weekly_date, file_whitelist_validator, QuerySetType
from trunk.models import University, UniversityStyle
from trunk.utils import format_datetime_12hr
from venture.models import MemberToAdmin

# in minutes
FIFTEEN = 1
FIFTEEN_MINUTE = 15
THIRTY = 2
THIRTY_MINUTE = 30
FOURTYFIVE = 3
FOURTYFIVE_MINUTE = 45
SIXTY = 4
SIXTY_MINUTE = 60
DURATIONS = (
    # Note: Parts of our codebase assumes that duration code is single digit.
    (FIFTEEN, FIFTEEN_MINUTE),
    (THIRTY, THIRTY_MINUTE),
    (FOURTYFIVE, FOURTYFIVE_MINUTE),
    (SIXTY, SIXTY_MINUTE)
)

# ratings
NOT_YET = 0
ONE_STAR = 1
TWO_STAR = 2
THREE_STAR = 3
FOUR_STAR = 4
FIVE_STAR = 5
NO_SHOW = 6
FEEDBACKS = (
    (NOT_YET, "No input yet"),
    (ONE_STAR, "1"),
    (TWO_STAR, "2"),
    (THREE_STAR, "3"),
    (FOUR_STAR, "4"),
    (FIVE_STAR, "5"),
    (NO_SHOW, "No Show")
)

# preferred contacts
PHONE = 1
PHONE_STRING = "Phone"
EMAIL = 2  # This option is deprecated
EMAIL_STRING = "StartupTree Messaging"
SKYPE = 3
SKYPE_STRING = "Video Call"
IN_PERSON_STRING = "In Person"
IN_PERSON = 4
TO_BE_DETERMINED_STRING = "To be determined"
TO_BE_DETERMINED = 5

EASTERN = 'US/Eastern'

PREFERRED_CONTACT = (
    (PHONE, PHONE_STRING),
    (SKYPE, SKYPE_STRING),
    (IN_PERSON, IN_PERSON_STRING),
    (TO_BE_DETERMINED, TO_BE_DETERMINED_STRING)
)

DURATION_OPTIONS = (
        (15, "15 minutes"),
        (30, "30 minutes"),
        (45, "45 minutes"),
        (60, "1 hour")
    )


# LOCATION_BY_DAY = {'Sun': '', 'Mon': '', 'Tue': '', 'Wed': '', 'Thu': '', 'Fri': '', 'Sat': ''}


class MentorManager(models.Manager):
    def get_or_create_mentor(self, profile, platform):
        if self.mentor_exists(profile, platform):
            return self.get(profile=profile, platform=platform), False
        return self.create(
            profile=profile,
            platform=platform
        ), True

    def mentor_exists(self, profile, platform):
        return self.filter(profile=profile, platform=platform).exists()

    def is_mentor(self, user_id):
        return self.filter(profile__user_id=user_id).exists()

    def get_mentor_by_id(self, url_id, site):
        return self.get(profile__url_name=url_id, platform__site_id=site.id)

    @staticmethod
    def query_date_range(start_date: dt.date, end_date: dt.date) -> Q:
        """ Returns a Q object that selects all Mentors that have a MentorActiveWindow range that
        intersects the input date range.

        It is assumed that start_date and end_date are both inclusive bounds.

        Requires: start_date <= end_date

        Example: Mentor.objects.filter(query_date_range(start_date, end_date))
        """
        return Q(mentoractivewindow__timestamp__date__lte=end_date,
                 mentoractivewindow__active_end__date__gte=start_date) \
            | Q(mentoractivewindow__timestamp__date__lte=end_date,
                mentoractivewindow__active_end__isnull=True)


class Mentor(models.Model):
    """
    A mentor on the platform assigned by university admins.

    Important:
        Use Mentor.objects.get_or_create_mentor() instead of Mentor.objects.create()

        Also, DO NOT bulk save/create, as that may prevent our overridden save() or get_or_create_mentor() methods
        from being called.

    Warning about `approve_meetings` field:
        This setting only applies if admin has not overridden by setting require_mentor_review_meetings on.
        If admin require_mentor_review_meetings is off, then approve_meetings is whether mentor
        prefers to review meetings before they are officially scheduled.

        For most intents and purposes, DO NOT USE approve_meetings. Instead, use
        property `approve_meetings_or_admin_override` to get whether mentor
        needs to approve meetings.

        approve_meetings is only used in setting the mentor's preference when require_mentor_review_meetings is off.
    """

    profile = models.ForeignKey(StartupMember, related_name="mentor_profile", on_delete=models.CASCADE)
    viewed_tutorial = models.BooleanField(default=False)
    platform = models.ForeignKey(University, on_delete=models.CASCADE)
    assigned_on = models.DateTimeField(auto_now_add=True)
    default_meeting_type = models.SmallIntegerField(choices=PREFERRED_CONTACT,
                                                    default=IN_PERSON)
    # WARNING: use Mentor.set_timezone() to change timezone (so that cronofy availability timezone can update too).
    TIMEZONES = [(t, t) for t in common_timezones]
    timezone = models.CharField(
        max_length=100,
        choices=TIMEZONES,
        default=EASTERN)

    is_active = models.BooleanField(default=True)

    # This controls whether a mentor is a TEAM mentor or not
    is_team_mentor = models.BooleanField(default=False)

    is_complete = models.BooleanField(default=False)
    default_duration = models.SmallIntegerField(choices=DURATION_OPTIONS,
                                                  default=30)
    default_location = models.CharField(max_length=100, null=True, blank=True)
    allow_office_hour = models.BooleanField(default=False)
    approve_meetings = models.BooleanField(default=False) # WARNING: Read note in specification above before using!
    number_meeting_times = models.PositiveSmallIntegerField(default=1)
    availability_note = models.CharField(max_length=1000, blank=True, default="")
    email = models.CharField(max_length=100, null=True, blank=True)
    # phone_number is deprecated, use phone instead
    phone_number = PhoneNumberField(null=True, blank=True, default=None, region="US")
    phone = models.CharField(max_length=20, null=True, blank=True)
    skype = models.CharField(max_length=100, null=True, blank=True, default=None)
    # cronofy_office_hours_schedule = PickledObjectField(default=LOCATION_BY_DAY)
    objects = MentorManager()

    # Email used for automatic mentorship session events to google calendar. None if not enabled.
    gcal_email = models.CharField(max_length=80, default=None, null=True)

    def save(self, *args, **kwargs):
        try:
            initial_mentor = Mentor.objects.get(id=self.id)
        except Mentor.DoesNotExist:
            initial_mentor = None

        # if initial_mentor is None, then we are creating a new mentor

        if initial_mentor:
            # Update Mentor Active Window records (pre-save).
            MentorActiveWindow.objects.update_records(new_mentor=self, initial_mentor=initial_mentor)
        else:
            # For new mentors, initializing MentorActiveWindow must happen post-save.
            pass

        super().save(*args, **kwargs)

        if not initial_mentor:
            # Initialize MentorActiveWindow post-save.
            MentorActiveWindow.objects.init_records(new_mentor=self)

    @property
    def approve_meetings_or_admin_override(self):
        return self.approve_meetings or self.platform.style.require_mentor_review_meetings__bool

    def __str__(self):
        return self.profile.first_name + " " + self.profile.last_name

    def get_preferred_contact(self):
        preferred_contact = self.default_meeting_type
        if preferred_contact == PHONE:
            return PHONE_STRING
        elif preferred_contact == EMAIL:
            return EMAIL_STRING
        elif preferred_contact == SKYPE:
            return SKYPE_STRING
        elif preferred_contact == IN_PERSON:
            return IN_PERSON_STRING
        raise ValueError(
            "Invalid value for preferred contact {0}".format(preferred_contact))

    def get_address(self):
        return {
            'name': 'StartupTree',
            'streetAddress': '325 College Ave',
            'addressLocality': 'Ithaca',
            'addressRegion': 'NY',
            'postalCode': '14850',
            'addressCountry': 'US'
        }

    def set_timezone(self, new_timezone):
        ''' Sets the timezone if it's changed. '''
        # import here prevents circular import
        from mentorship.st_cronofy import is_cronofy_linked, update_cronofy_timezone_availability
        if new_timezone and self.timezone != new_timezone:
            self.timezone = new_timezone
            self.save()

            if is_cronofy_linked(self.profile.user):
                update_cronofy_timezone_availability(self)

    def get_timezone(self):
        if self.timezone:
            return self.timezone
        return self.platform.style.timezone

    def get_schedule(self):
        return MentorSchedule.objects.filter(mentor_id=self.id)

    def has_schedule(self, mentor_id=None):
        if not mentor_id:
            mentor_id = self.id
        from mentorship.st_cronofy import is_cronofy_linked
        if is_cronofy_linked(self.profile.user):
            return True
        return self.has_onetimes(mentor_id) or MentorSchedule.objects.filter(
            mentor_id=mentor_id, date__isnull=False, is_repeating=True).exists()

    def get_onetimes(self, mentor_id=None):
        if not mentor_id:
            mentor_id = self.id
        return MentorSchedule.objects.filter(
            mentor_id=mentor_id, date__isnull=False, is_repeating=False, date__gte=timezone.now().date())

    def has_onetimes(self, mentor_id=None):
        return self.get_onetimes(mentor_id).exists()

    def make_team_mentor(self):
        self.is_team_mentor = True
        self.save()

    def team_mentor_status(self):
        return self.is_team_mentor

    def get_matchings(self, user):
        """
        Calculates a score [0,100] based on matches roles and skills.
        Roles and skills are weighted 70 : 30 respectively.
        Returns a tuple of score, matched roles, and matched skills
        param: user
        param: self mentor to find a matching for
        """
        score = 0
        profile = self.profile
        user_roles = user.skill_groups.all()
        user_skills = user.skills.all()
        mentor_roles = profile.skill_groups.all()
        mentor_skills = profile.skills.all()
        # Get recommendations for the user
        # represents the people recommended to the user
        recommended_mentors = []
        role_mentors = []

        matched_roles = []
        matched_skills = []

        for role in user_roles:
            if role in mentor_roles:
                matched_roles.append(role.name)
        for skill in user_skills:
            for mentor_skill in mentor_skills:
                if skill.name.upper() == mentor_skill.name.upper():
                    matched_skills.append(skill.name)
        if len(user_roles) == 0 or len(mentor_roles) == 0:
            role_score = 0
        else:
            role_score = len(matched_roles) / \
                min(len(user_roles), len(mentor_roles))
        if len(user_skills) == 0 or len(mentor_skills) == 0:
            skill_score = 0
        else:
            skill_score = len(matched_skills) * 1.0 / \
                min(len(user_skills), len(mentor_skills))

        # if match is 100%
        if role_score == 1 and skill_score == 1:
            score = 100
        # else weighted score
        else:
            prev_score = (role_score * 70 + skill_score * 30)
            score = prev_score * 0.8 + 20

        return score, matched_roles, matched_skills


class MentorshipActivityManager(models.Manager):
    def make_mentorship_activity(self, activity_type, university):
        now = timezone.now()
        return self.create(timestamp=now, activity_type=activity_type, platform=university)

    def activity_select_related(self):
        return self.select_related(
            'mentorship',
            'mentorship__mentor',
            'mentorship__mentor__profile',
            'mentorship__student',
            'goalset',
            'goalset__mentorship',
            'goalset__mentorship__mentor',
            'goalset__mentorship__mentor__profile',
            'goalset__mentorship__student',
            'note',
            'note__mentorship',
            'note__mentorship__mentor',
            'note__mentorship__mentor__profile',
            'note__mentorship__student',
            'mentorshipsession',
            'mentorshipsession__mentorship',
            'mentorshipsession__mentorship__mentor',
            'mentorshipsession__mentorship__mentor__profile',
            'mentorshipsession__mentorship__student'
        )

    def all_platform_regular_activities(self, platform: University):
        return self.activity_select_related().filter(platform_id=platform.id, teammentorshipactivity__isnull=True)

    def order_activities(self, query):
        return query.order_by('-timestamp')


class MentorshipActivity(models.Model):
    '''
    An object representing an item to appear on the admin mentorship activity feed.

    Each activity corresponds to a particular mentorship item being created.

    Activities should have a one-to-one relation to models (ie note creation, goal setting, meeting scheduled)

    In order to add a new activity type to this feed:

    1. See if it has an 'activity type code' (constants at top). If not, give it one.
    2. Fill out case for each getter (get_activity_name, get_body, get_body_html)
    3. Add one-to-one field to model (and then run makemigrations/migrate).
        eg: mentorship_activity = models.OneToOneField(MentorshipActivity, null=True)
    4. For method corresponding to activity (eg create_note() ), add code to
        add a call to mentorship.views.make_mentorship_activity(activity_type)
        and update that make_mentorship_activity to hande new activity type.
    5. If there is a way to recreate the past activities of this type, create and run a migration script to do so.
        (see branch.management.commands.init_admin_mentorship_recent_activity for an example).

    is_timestamp_estimated is true for SESSION_SCHEDULED events before we kept track
    of timestamps, so we estimate sessions to have been scheduled one week before the meeting.
    Null is_timestamp_estimated can be assumed to be False.
    '''
    # mentorship activity_type codes (applies to single mentorship and team mentorship)
    GOALSET_CREATED = 1
    GOAL_COMPLETED = 2
    NOTE_CREATED = 3
    SESSION_SCHEDULED = 4
    SESSION_CANCELLED = 5  # Not yet implemented
    CONNECTION_MADE = 6
    CONNECTION_ENDED = 7  # Not yet implemented
    MENTOR_BECAME_ACTIVE = 8

    GOALSET_CREATED_STR = "Set of Goals Created"
    GOAL_COMPLETED_STR = "Goal Completed"
    NOTE_CREATED_STR = "Note Created"
    SESSION_SCHEDULED_STR = "Meeting Scheduled"
    SESSION_CANCELLED_STR = "Meeting Cancelled"
    CONNECTION_MADE_STR = "Connection Made"
    CONNECTION_ENDED_STR = "Connection Ended"
    MENTOR_BECAME_ACTIVE_STR = "Mentor Became Active"  # WARNING: for front-end, use correct Mentor word.

    MENTORSHIP_ACTIVITY_TYPES = (
        (GOALSET_CREATED, GOALSET_CREATED_STR),
        (GOAL_COMPLETED, GOAL_COMPLETED_STR),
        (NOTE_CREATED, NOTE_CREATED_STR),
        (SESSION_SCHEDULED, SESSION_SCHEDULED_STR),
        (SESSION_CANCELLED, SESSION_CANCELLED_STR),
        (CONNECTION_MADE, CONNECTION_MADE_STR),
        (CONNECTION_ENDED, CONNECTION_ENDED_STR),
        (MENTOR_BECAME_ACTIVE, MENTOR_BECAME_ACTIVE_STR),
    )

    timestamp = models.DateTimeField(editable=False)
    activity_type = models.IntegerField(choices=MENTORSHIP_ACTIVITY_TYPES)
    is_timestamp_estimated = models.BooleanField(default=False)
    platform = models.ForeignKey(University, null=True, on_delete=models.CASCADE)

    objects = MentorshipActivityManager()

    def to_dict(self):
        deleted, body = self.get_body(mark_deleted=True)

        session_info = None
        if self.activity_type == MentorshipActivity.SESSION_SCHEDULED:
            try:
                session = self.mentorshipsession
                session_datetime = session.datetime.isoformat()
                session_approved = session.is_approved()
                session_info = {
                    'session_datetime': session_datetime,
                    'session_approved': session_approved,
                    'session_cancelled': session.is_cancelled,
                }
            except MentorshipActivity.mentorshipsession.RelatedObjectDoesNotExist:
                session_info = {
                    'session_datetime': None,
                    'session_approved': None,
                    'session_cancelled': True,
                }

        return {
            'timestamp': timezone.localtime(self.timestamp).strftime('%Y-%m-%d %H:%M'),
            'session_info': session_info,
            'activity_type': self.activity_type,
            'icon': self.get_icon(),
            'body': body,
            'deleted': deleted,
            'id': self.id
        }

    def get_time(self):
        ''' string representation of the time of the activity
        '''
        localtime = timezone.localtime(self.timestamp)

        return localtime.strftime('%a %b %d, %I:%M%p')

    def get_activity_name(self):
        """name of activity (suitable for frontend) """
        if self.activity_type == MentorshipActivity.MENTOR_BECAME_ACTIVE:
            uni_style: UniversityStyle = self.platform.universitystyle_set.first()
            mentor_word_cap = uni_style.mentor_word.capitalize()
            return f"{mentor_word_cap} Became Active"
        return dict(MentorshipActivity.MENTORSHIP_ACTIVITY_TYPES)[self.activity_type]

    def get_body(self, full=False, current_member=None, mark_deleted=False):
        """
        Returns a string representation of the activity (without time).

        Optional Parameters:

            full:
                If true, we don't return a string representation, but rather a dictionary.

                The dictionary is essentially the result of self.to_dict(), with the following additional fields: {
                    'full_body': <a dictionary representation of the related object; eg note.to_dict()>
                    'deleted': <boolean of whether the activity refers to a deleted item>

            current_member:
                When full=True, we may need to know who the current member is to generate the `full_body` dictionary.

            mark_deleted:
                It may be possible that the corresponding object (eg note or session) was deleted,
                in which case it is not possible to generate the details needed for the string;
                in this case, the string representation will be simple, like "Deleted Note".

                If mark_deleted is true, then we return both the string, and a boolean indicating whether
                the activity is referring to a deleted item.


        Always make sure to call MentorshipActivity with proper select_related
        Ex)
            full_list = MentorshipActivity.objects.select_related(
                'mentorship',
                'mentorship__mentor',
                'mentorship__mentor__profile',
                'mentorship__student',
                'goalset',
                'goalset__mentorship',
                'goalset__mentorship__mentor',
                'goalset__mentorship__mentor__profile',
                'goalset__mentorship__student',
                'note',
                'note__mentorship',
                'note__mentorship__mentor',
                'note__mentorship__mentor__profile',
                'note__mentorship__student',
                'mentorshipsession',
                'mentorshipsession__mentorship',
                'mentorshipsession__mentorship__mentor',
                'mentorshipsession__mentorship__mentor__profile',
                'mentorshipsession__mentorship__student'
            ).filter(platform_id=university.id).order_by('-timestamp')
        :return:
        """
        full_body = {}
        result = None
        deleted = False
        if full:
            full_body = self.to_dict()
        current_member_id = current_member.id if current_member else -1
        if self.activity_type == MentorshipActivity.NOTE_CREATED:
            try:
                note = self.note
                if full:
                    result = note.to_dict(current_member)
                else:
                    mentor_name = note.mentorship.mentor.profile.get_full_name()
                    student_name = note.mentorship.student.get_full_name()

                    if note.is_by_mentor:
                        writer = mentor_name
                        writee = student_name
                    else:
                        writer = student_name
                        writee = mentor_name
                    result = "Note created by " + writer + " to " + writee + ". "
            except models.ObjectDoesNotExist:
                result = "Deleted Note"
                deleted = True
        elif self.activity_type == MentorshipActivity.GOALSET_CREATED:
            try:
                goalset = self.goalset
                if full:
                    result = goalset.to_dict(current_member_id)
                else:
                    amount = Goal.objects.filter(goal_set_id=goalset.id).count()
                    mentor_name = goalset.mentorship.mentor.profile.get_full_name()
                    student_name = goalset.mentorship.student.get_full_name()
                    goal_word = "goal" if amount == 1 else "goals"
                    result = str(amount) + " " + goal_word + " set between " + mentor_name + " and " + student_name + "."
            except models.ObjectDoesNotExist:
                result = "Deleted Goals"
                deleted = True
        elif self.activity_type == MentorshipActivity.SESSION_SCHEDULED:
            try:
                session = self.mentorshipsession
                if full:
                    result = session.to_dict()
                else:
                    mentor_name = session.mentorship.mentor.profile.get_full_name()
                    student_name = session.mentorship.student.get_full_name()
                    datetime_str = format_datetime_12hr(session.datetime, self.platform.style.timezone)
                    result = f"Meeting scheduled between { mentor_name } and { student_name } for {datetime_str}."
            except models.ObjectDoesNotExist:
                result = "Deleted Meeting"
                deleted = True
        elif self.activity_type == MentorshipActivity.CONNECTION_MADE:
            try:
                mentorship = self.mentorship
                if full:
                    result = mentorship.to_dict(current_member_id)
                else:
                    mentor_name = mentorship.mentor.profile.get_full_name()
                    student_name = mentorship.student.get_full_name()
                    result = "Connection made between " + mentor_name + " and " + student_name + "."
            except models.ObjectDoesNotExist:
                result = "Deleted connection"
                deleted = True
        elif self.activity_type == MentorshipActivity.MENTOR_BECAME_ACTIVE:
            try:
                window: MentorActiveWindow = self.mentoractivewindow
                mentor = window.mentor
                if full:
                    result = None  # TODO in the future
                else:
                    mentor_word = self.platform.style.mentor_word.capitalize()
                    mentor_name = mentor.profile.get_full_name()
                    result = f"{mentor_word}, {mentor_name}, became active."
            except models.ObjectDoesNotExist:
                result = "Deleted Mentor"
                deleted = True
        if full:
            full_body['deleted'] = deleted
            full_body['full_body'] = result
            result = full_body
        if mark_deleted:
            return deleted, result
        return result

    def get_icon(self):
        icon_name = ""

        if self.activity_type == MentorshipActivity.NOTE_CREATED:
            icon_name = "file-o"
        elif self.activity_type == MentorshipActivity.GOALSET_CREATED:
            icon_name = "check-square-o"
        elif self.activity_type == MentorshipActivity.SESSION_SCHEDULED:
            icon_name = "calendar-check-o"
        elif self.activity_type == MentorshipActivity.CONNECTION_MADE:
            icon_name = "exchange"
        elif self.activity_type == MentorshipActivity.MENTOR_BECAME_ACTIVE:
            icon_name = "user-plus"

        return icon_name


class MentorActiveWindowManager(models.Manager):
    @staticmethod
    def init_records(new_mentor: Mentor):
        """ Meant to be called each time a mentor is created (in MentorManager.get_or_create_mentor().
        For a newly created mentor that is active, we initiate a new window object (with end null).

        This method most be called post-save to prevent this error:
            ValueError: save() prohibited to prevent data loss due to unsaved related object 'mentor'.
        """
        if MentorActiveWindow.objects.filter(mentor=new_mentor).exists():
            # Error Check
            app_logger.error(f"MentorActiveWindow records already exist for new mentor (id {new_mentor.id}). "
                             f"Attempting to update records. ")
            MentorActiveWindow.objects.update_records(new_mentor=new_mentor, initial_mentor=None)

        if new_mentor.is_active:
            MentorActiveWindow.objects.create(
                activity_type=MentorshipActivity.MENTOR_BECAME_ACTIVE,
                mentor=new_mentor,
                timestamp=timezone.now(),
                platform=new_mentor.platform,
            )

        # No records need to be created if new mentor starts out as inactive.

        MentorActiveWindowManager.check_invariant_logger(new_mentor)

    @staticmethod
    def update_records(new_mentor: Mentor, initial_mentor: Optional[Mentor] = None):
        """
        Meant to be called each time mentor is saved (in Mentor.save()).

        Update thee MentorActiveWindow records accordingly according to new is_active status.

        Params:
            new_mentor: The mentor object with updated fields pre-save.
            initial_mentor: The original version of the mentor (as fetched from database with original field values)

        If initial_mentor is provided,
            We check if the status has changed. We don't do anything if the new status hasn't changed.
            We also check that the initial mentor satisfied
                the MentorActiveWindow invariant (Out of abundance of caution.)

        If initial_mentor is not provided, we cannot check that the status is changed and we assume that the prior
            status is different.
        """
        if initial_mentor is not None and initial_mentor.is_active == new_mentor.is_active:
            return  # Status did not change

        if initial_mentor:
            # Check that the mentor, before the update, satisfied the invariant.
            MentorActiveWindowManager.check_invariant_logger(mentor=initial_mentor)

        windows = MentorActiveWindow.objects.filter(mentor_id=new_mentor.id)
        last_window: Optional[MentorActiveWindow] = windows.last() if windows.exists() else None

        if new_mentor.is_active:
            # Mentor status changed to active.
            now = timezone.now()

            if last_window:
                if not last_window.active_end:
                    # Error Check
                    app_logger.error(f"Mentor (id {new_mentor.id}) is changing from inactive to active, "
                                     f"but their last MentorActiveWindow (id {last_window.id}) did not have "
                                     f"an end date. No new record is created. ")
                    return

                # boundary of last window to next window may touch.
                last_window.active_end = now
                last_window.save()

            MentorActiveWindow.objects.create(
                activity_type=MentorshipActivity.MENTOR_BECAME_ACTIVE,
                mentor=new_mentor,
                timestamp=now,
                platform=new_mentor.platform
            )
        else:
            # Mentor status changed to inactive
            if last_window:
                if last_window.active_end:
                    app_logger.error(f"Mentor (id {new_mentor.id}) is changing from active to inactive,"
                                     f"but their last MentorActiveWindow is already closed (active_end is defined). "
                                     f"No record is modified. ")
                else:
                    last_window.active_end = timezone.now()
                    last_window.save()
            else:
                app_logger.error(f"Mentor (id {new_mentor.id}) is changing from active to inactive, "
                                 f"but they don't any past MentorActiveWindow records. "
                                 f"No record is modified. ")
                return

        MentorActiveWindowManager.check_invariant_logger(new_mentor)

    @staticmethod
    def check_invariant_logger(mentor: Mentor):
        """ Checks the invariants defined in MentorActiveWindow specs.
        Prints to app_logger.error if the invariant is broken (code is not terminated). """
        try:
            MentorActiveWindowManager.check_invariant(mentor)
        except AssertionError as e:
            app_logger.error(str(e))

    @staticmethod
    def check_invariant(mentor: Mentor):
        """ Checks the invariants defined in MentorActiveWindow specs.
        Raises Assertion Error with message if invariant is broken. """

        windows: QuerySetType[MentorActiveWindow] = MentorActiveWindow.objects\
            .filter(mentor_id=mentor.id).order_by('timestamp')

        if mentor.is_active and not windows.exists():
            raise AssertionError(
                f'MentorActiveWindow Invariant Failure: Active mentor {mentor.id} does not have any windows.')

        for idx, window in enumerate(windows):
            if not window.timestamp <= timezone.now():
                raise AssertionError(f'MentorActiveWindow Invariant Failure: '
                                     f'The start of the window (id: {window.id}) cannot be in the future.')

            if window.active_end:
                if not window.active_end <= timezone.now():
                    raise AssertionError(f'MentorActiveWindow Invariant Failure: '
                                         f'The end of the window (id: {window.id}) cannot be in the future.')

                if not window.timestamp < window.active_end:
                    raise AssertionError(f'MentorActiveWindow Invariant Failure: '
                                         f'The start of a window (id: {window.id}) must be before the end ')

            if idx < windows.count() - 1:
                # This is not the most recent window

                if window.active_end is None:
                    raise AssertionError(f'MentorActiveWindow Invariant Failure: '
                                         f'Field active_end is undefined for not-most-recent window (id: {window.id}).')

                next_window = windows[idx + 1]
                no_overlap = window.active_end <= next_window.timestamp  # boundary may touch
                if not no_overlap:
                    raise AssertionError(f'MentorActiveWindow Invariant Failure: Window (id {window.id}) overlaps '
                                         f'with window (id {next_window.id}')

            else:
                # This is the most recent window

                if mentor.is_active and window.active_end is not None:
                    raise AssertionError(f'MentorActiveWindow Invariant Failure: Field active_end is defined for '
                                         f'most recent window (id: {window.id}) of active mentor.')

                if not mentor.is_active and window.active_end is None:
                    raise AssertionError(f'MentorActiveWindow Invariant Failure: Field active_end is undefined for '
                                         f'most recent window (id: {window.id}) of inactive mentor.')


class MentorActiveWindow(MentorshipActivity):
    """
    A record of each window of time that the mentor was active.

    Time Window Fields:
        timestamp: the start of the time window (inclusive)
        active_end: the end of the time window (exclusive), and may be None if window is not closed.

    If active_end is defined:
        The mentor was active every moment from timestamp (inclusive) up until active_end (exclusive).

    If active_end is None:
        The mentor is currently active and has been active since timestamp (inclusive).

    Any time period that is not covered by MentorActiveWindow is interpreted as the mentor being inactive.
        Thus, A mentor without any MentorActiveWindow objects is interpreted as being inactive for all time.

    WARNING INSTRUCTION:
        There should not be a need to directly create or modify MentorActiveWindow objects. That work is handled by
        MentorActiveWindowManager.init_records() and MentorActiveWindowManager.update_records(), which
        should only need to be called in overridden Mentor.save()

    Note: This model only keeps track of Mentor.is_active field.
    It is up to the user to decide whether to consider Mentor.profile.is_archived

    Records after October 2020 are truly accurate.

    Records before October 2020 are estimated via a migration script (python3 manage.py mentoractivewindow_init)
    that ran in Oct 2020 as follows:
        For all mentors active in October 2020:
            The mentor is assumed to have been continuously active from the time they were created to present.
                A single window object is created, with active_start (MentorshipActivity.timestamp field) being
                set equal to Mentor.assigned_on field. active_end is set to null.

        For mentors inactive in October 2020:
            The mentor is assumed to have been active from the time they were created until 1 day after their last
            scheduled meeting, last_meeting, (whose datetime is more than 1 day before the time of running the script).
                A single window object is created, with active_start (MentorshipActivity.timestamp field) being
                set equal to Mentor.assigned_on field. active_end is set to last_session.datetime + timedelta(days=1)
                For simplicity sake, any past meeting object will count (including cancelled / un-approved / etc)
                towards mentor being active.

        For mentors inactive in October 2020 that never had a meeting:
            We treat as inactive the entire time.
                No window objects are created in this case.

    Invariants (Constraints):
        Two time windows of the same mentor cannot overlap.

        The start of a window must be before the end if the end is defined.

        The start and end (if defined) for any window must be in the past.

        For all active mentors:
            They have at least one MentorActiveWindow object. Only the most recent window object has active_end being
            null. All other window objects of the mentor (if any other) must have active_end defined.

        For all inactive mentors:
            They can any number of windows (including 0).
            All window objects of the mentor (if any) must have active_end defined.

        NOTE: These invariants are checked in MentorActiveWindowManager.check_invariant(). If these invariant
        specifications change, that invariant checker method should be revised too.
    """
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    active_end = models.DateTimeField(null=True)

    objects = MentorActiveWindowManager()


class MentorGroup(models.Model):
    """
    Permission groups for mentors.
    DEPRECATED: left out for keeping old records.
    """
    display_name = models.CharField(max_length=150, blank=False)
    members = models.ManyToManyField(Mentor)
    uni = models.ForeignKey(University, on_delete=models.CASCADE)


class MentorScheduleManager(models.Manager):
    pass


class MentorSchedule(models.Model):
    """
    Recurring Mentor schedules.
    """
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7
    DAYS = (
        (MONDAY, strings.MONDAY),
        (TUESDAY, strings.TUESDAY),
        (WEDNESDAY, strings.WEDNESDAY),
        (THURSDAY, strings.THURSDAY),
        (FRIDAY, strings.FRIDAY),
        (SATURDAY, strings.SATURDAY),
        (SUNDAY, strings.SUNDAY)
    )
    WEEK_DAYS = (
        (MONDAY, strings.MONDAY),
        (TUESDAY, strings.TUESDAY),
        (WEDNESDAY, strings.WEDNESDAY),
        (THURSDAY, strings.THURSDAY),
        (FRIDAY, strings.FRIDAY)
    )
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    meeting_type = models.PositiveSmallIntegerField(
        choices=PREFERRED_CONTACT, default=IN_PERSON)
    # weekday, for repeating timeslot
    day = models.PositiveSmallIntegerField(choices=DAYS, blank=True, null=True)
    # date for one-time timeslot, start date for repeating timeslot
    date = models.DateField(null=True)
    end_date = models.DateField(null=True)  # inclusive end date for repeating timeslot; none means repeat forever
    start_time = models.TimeField()
    end_time = models.TimeField()
    duration = models.PositiveSmallIntegerField(
        choices=DURATIONS, default=THIRTY)
    location = models.CharField(max_length=100, null=True, default=None)
    phone_number = models.CharField(max_length=100, null=True, blank=True)
    video_id = models.CharField(max_length=100, null=True, blank=True, default=None)
    is_repeating = models.BooleanField(default=False)
    is_inactive = models.BooleanField(default=False)
    notes = models.TextField(null=True)
    objects = MentorScheduleManager()

    @classmethod
    def get_minute_choice(cls, minutes):
        minutes = int(minutes)
        for duration in DURATIONS:
            if duration[1] == minutes:
                return duration[0]

        raise ValueError("Invalid minute: {0}".format(minutes))

    def get_day(self):
        return self.DAYS[(self.day - 1)][1]

    def get_duration(self):
        return DURATIONS[(self.duration - 1)][1]


class MentorScheduleBlackOut(models.Model):
    """
    Blackout dates of recurring schedule.

    NOTE: These are applicable to BOTH non-cronofy and cronofy availability.
    """
    start_date = models.DateField()
    end_date = models.DateField()
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)

    def get_start_dt(self):
        return pytz.timezone(self.mentor.timezone).localize(dt.datetime.combine(self.start_date, dt.datetime.min.time()))

    def get_day_after_end_dt(self):
        """
        Returns the datetime of the end of the blackout, rounded to the next day.

        Eg. If mentor is in ET timezone, with blackout end date January 10, technically
        the end of the blackout period is January 10, 11:59:59PM ET. For elegance,
        we round to January 11, 12:00 AM ET.
        """
        return pytz.timezone(self.mentor.timezone).localize(
            dt.datetime.combine(self.end_date + timedelta(days=1), dt.datetime.min.time()))

    def is_expired(self):
        """
        Corner case. Suppose it is exactly Jan 11 at 12AM.
        A blackout date ending on Jan 10 is expired, but get_day_after_end_dt return Jan 11 at 12 AM.
        Thus, we use inclusive inequality.
        """
        return self.get_day_after_end_dt() <= timezone.now()


class MentorCronofySchedule(models.Model):
    """
    Holds availability data (type, location)
    specific to mentor's Cronofy schedule.
    """
    DAYS = (
        (0, "Sun"),
        (1, "Mon"),
        (2, "Tue"),
        (3, "Wed"),
        (4, "Thu"),
        (5, "Fri"),
        (6, "Sat")
    )

    mentor = models.ForeignKey(Mentor, related_name='cronofy_office_hours_schedule', on_delete=models.CASCADE)
    day = models.PositiveSmallIntegerField(choices=DAYS)
    type = models.PositiveSmallIntegerField(choices=PREFERRED_CONTACT,
                                            null=True, default=IN_PERSON)
    # location char limit used to be 100. It was changed to 1000 because mentors wanted to
    # note different locations for different hours of the day. When we eventually
    # add the cabability for mentors to specify location by hour (instead of just by day),
    # then we can set location limit back to 100
    # (both limits of MentorshipSession.location & MentorCronofySchedule.location)
    location = models.CharField(max_length=1000, null=True, default='')


class MentorshipManager(models.Manager):
    def create_new_mentorship(self, student, mentor):
        try:
            existing = self.get(student=student,
                                mentor=mentor)
            if existing.is_active:
                raise ValueError("Already active mentorship.")
            else:
                existing.is_active = True
                existing.save()
            return existing
        except Mentorship.DoesNotExist:
            return self.create(student=student,
                               mentor=mentor)


class MentorshipDefaults(models.Model):
    """
    Default settings for mentorship scheduling
    """
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    preferred_contact = models.SmallIntegerField(choices=PREFERRED_CONTACT,
                                                 default=EMAIL)
    duration = models.PositiveSmallIntegerField(
        choices=DURATIONS, default=THIRTY)
    is_officehours = models.BooleanField()
    location = models.CharField(max_length=512, null=True, default=None)


class Mentorship(models.Model):
    """
    Mentorship between a mentor and a student

    NOTE: This model also serves as parent class to class TeamMentorship. In that case, student field is
    always None. The relevant fields are TeamMentorship.mentor (inherited) and TeamMentorship.team.

    """
    student = models.ForeignKey(StartupMember, on_delete=models.CASCADE, null=True)  # Null if team mentorship
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    # if request has been accepted, then accepted request will be shared.
    accepted_request = models.OneToOneField(
        "MentorshipRequest", null=True, default=None, on_delete=models.CASCADE)
    mentorship_activity = models.OneToOneField(
        MentorshipActivity, on_delete=models.SET_NULL, null=True)
    # mentor blocked this mentorship
    blocked = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)  # state of mentorship
    created_on = models.DateTimeField(auto_now_add=True)
    # should be set when request is accepted.
    started_on = models.DateTimeField(null=True, default=None)
    # should be set when request is deactivated.
    ended_on = models.DateTimeField(null=True, default=None)

    objects = MentorshipManager()
    mentor_notes = models.TextField(null=True, default=None)

    mentor_rating = models.PositiveSmallIntegerField(
        choices=FEEDBACKS, default=NOT_YET, null=True)

    def get_student_mentor_dict(self, current_member_id : int):
        mentor_profile = self.mentor.profile
        mentor = {
            'name': mentor_profile.get_full_name(),
            'image': mentor_profile.get_image(),
            'canEdit': mentor_profile.id == current_member_id
        }
        student = self.student
        student = {
            'name': student.get_full_name(),
            'image': student.get_image(),
            'canEdit': student.id == current_member_id
        }
        return student, mentor

    def to_dict(self, current_member_id : int):
        student, mentor = self.get_student_mentor_dict(current_member_id)
        result = {
            'mentor': mentor,
            'mentee': student,
            'is_active': self.is_active,
            'created': self.created_on.strftime("%Y-%m-%d %-I:%M %p"),
            'started': self.started_on.strftime("%Y-%m-%d %-I:%M %p") if self.started_on else None,
            'ended': self.ended_on.strftime("%Y-%m-%d %-I:%M %p") if self.ended_on else None
        }
        return result


class MentorshipRequestManager(models.Manager):
    def create_new_request(self, mentorship, startup, pitch, other, via_meeting):
        # if title is None or len(title) > 100:
        #     raise ValueError("Title must be less than 100 characters.")
        # if text is None or len(text) < 50:
        #     raise ValueError("Text must be more than 50 characters.")
        # return self.create(requested_mentorship=mentorship,
        #                    title=title,
        #                    text=text)
        if self.filter(requested_mentorship__id=mentorship.id).exists():
            existing = self.filter(requested_mentorship__id=mentorship.id).order_by('-created_on').first()
            existing.update_time()
            existing.refresh_from_db()
            return existing
        else:
            return self.create(requested_mentorship=mentorship,
                            startup=startup,
                            pitch=pitch,
                            other=other,
                            via_meeting=via_meeting)

    def request_exists(self, req_id, uni_id):
        return self.filter(id=req_id, requested_mentorship__mentor__platform_id=uni_id).exists()

    def mentorship_reject(self, m_request, via_meeting=False):
        mentorship = m_request.requested_mentorship
        if not via_meeting:
            mentorship.accepted_request = None
            mentorship.is_active = False
            m_request.is_admin_accepted = False
            mentorship.save()
        m_request.update_time()

    def mentorship_accept(self, m_request):
        mentorship = m_request.requested_mentorship
        m_request.is_admin_accepted = True
        mentorship.accepted_request = m_request
        mentorship.is_active = True
        mentorship.save()
        m_request.update_time()


class MentorshipRequest(models.Model):
    """
    Mentorship request created and sent by students.
    """
    requested_mentorship = models.ForeignKey(Mentorship, on_delete=models.CASCADE)
    startup = models.ForeignKey(Startup, null=True, default=None, on_delete=models.SET_DEFAULT)
    pitch = models.TextField()
    other = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    is_admin_accepted = models.NullBooleanField(null=True, default=None)
    admin_reject_reason = models.TextField(null=True, default=None)
    via_meeting = models.NullBooleanField(null=False, default=False)

    # just in case admin does not want to filter the request
    is_mentor_accepted = models.BooleanField(default=False)
    mentor_reject_reason = models.TextField(null=True, default=None)

    # after accepted or rejected, mentee visited its mentee page.
    read_by_mentee = models.BooleanField(default=False)

    objects = MentorshipRequestManager()

    def update_time(self):
        self.created_on = timezone.now()
        self.save()


class MentorshipEmailTemplate(models.Model):
    """
    Customizable Mentor invitation email.
    """
    subject = models.CharField(max_length=500, null=True, default=None)
    email_body = models.TextField(null=True, default=None)
    platform = models.ForeignKey(University, null=True, on_delete=models.CASCADE)
    # a tag for university so that they can save multiple templates. to be implemented later
    # tag = models.CharField(max_length=50)


class MentorshipSession(models.Model):
    """
    A session for the mentorship. At the end of each session,
    the note should be provided.

    IMPORTANT NOTE regarding location:
        For single-mentorship meetings (non-team mentorship):
            The official way our platform renders the location of the session is per
                mentorship.utils.get_session_type_and_location()
        For team mentorship meetings:
            The official way our platform renders the location of the session is
                session.location


    IMPORTANT NOTE regarding approval:
        Currently, approval from admin and mentor only applies to single-mentorship meetings.

        Our platform currently interprets any TeamMentorshipSession object created as one that
        is automatically approved (regardless of mentor approval or admin approval settings).

        If this changes, then team mentorship scheduling needs to be updated (including the helper method
        MentorshipSession.is_approved() ).


    Hint:
        The way to access the child TeamMentorshipSession object from a MentorshipSession object, session, is:
            session.teammentorshipsession

        The way to access the parent MentorshipSession object from a TeamMentorshipSession object, team_session, is:
            team_session.mentorshipsession_ptr
    """
    # null=True for TeamMentorship
    mentorship = models.ForeignKey(Mentorship, on_delete=models.CASCADE, null=True)
    # agreed schedule
    datetime = models.DateTimeField()
    duration = models.PositiveSmallIntegerField(
        choices=DURATIONS, default=THIRTY)
    meeting_type = models.PositiveSmallIntegerField(
        choices=PREFERRED_CONTACT, default=IN_PERSON)
    # location char limit used to be 100. It was changed to 1000 because mentors wanted to
    # note different locations for different hours of the day. When we eventually
    # add the cabability for mentors to specify location by hour (instead of just by day),
    # then we can set location limit back to 100
    # (both limits of MentorshipSession.location & MentorCronofySchedule.location)
    location = models.CharField(max_length=1000, null=True, default=None)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    video_id = models.CharField(max_length=100, null=True, blank=True, default=None)
    meeting_topic = models.TextField(null=True, default=None)
    is_admin_accepted = models.NullBooleanField(null=True, default=None)
    is_mentor_accepted = models.NullBooleanField(null=True, default=None)
    is_cancelled = models.BooleanField(default=False)
    is_cancelled_by_student = models.BooleanField(default=False)
    cancel_reason = models.TextField(null=True, default=None)
    #note = models.TextField(null=True, default=None)
    # to see when the note is created and compare with scheduled time.
    note_created = models.DateTimeField(null=True, default=None)
    # satisfied or not -- no longer used
    student_feedback = models.PositiveSmallIntegerField(choices=FEEDBACKS,
                                                        default=NOT_YET)
    # optional feedback to provide additional info.
    student_feedback_note = models.TextField(null=True, default=None)
    mentor_feedback = models.PositiveSmallIntegerField(choices=FEEDBACKS,
                                                       default=NOT_YET)

    mentorship_activity = models.OneToOneField(MentorshipActivity, on_delete=models.SET_NULL, null=True)
    timestamp_of_scheduling = models.DateTimeField(auto_now_add=True, null=True)

    # event_id is deprecated (previously used for google calendar sync)
    event_id = models.CharField(max_length=1024, null=True, default=None)

    def is_team_session(self):
        """ Returns whether this is the parent Mentorship object of a TeamMentorshipSession object """
        return hasattr(self, 'teammentorshipsession')

    def is_approved(self):
        if self.is_team_session():
            return True
        else:
            return self.is_admin_accepted and self.is_mentor_accepted

    def admin_accept_session(self):
        self.is_admin_accepted = True
        self.is_cancelled = False
        self.save()

    def mentor_accept_session(self):
        self.is_mentor_accepted = True
        self.is_cancelled = False
        self.save()

    def admin_reject_session(self):
        self.is_admin_accepted = False
        self.is_cancelled = True
        self.save()

    def mentor_reject_session(self):
        self.is_mentor_accepted = False
        self.is_cancelled = True
        self.save()

    def needs_admin_approval(self):
        mentor = self.mentorship.mentor
        uni_style = mentor.platform.style
        if uni_style.require_meeting_approval == UniversityStyle.ALL_MEETINGS or \
            (uni_style.require_meeting_approval == UniversityStyle.OFFICE_HOURS_ONLY and mentor.allow_office_hour):
            return True
        return False

    def needs_mentor_approval(self):
        return self.mentorship.mentor.approve_meetings_or_admin_override

    def get_datetime(self, mentor=None, user_timezone=None):
        """
        :param mentor:
        :return: Timezone aware datetime
        """
        if user_timezone:
            if isinstance(user_timezone, str):
                mentor_timezone = pytz.timezone(user_timezone)
            else:
                mentor_timezone = user_timezone
        else:
            mentor_timezone = pytz.timezone(self.get_timezone(mentor=mentor))
        try:
            result = timezone.localtime(self.datetime, mentor_timezone)
        except ValueError:
            try:
                result = timezone.make_aware(self.datetime, mentor_timezone)
            except ValueError:
                result = self.datetime

        return result

    def get_endtime(self, mentor=None, user_timezone=None):
        """
        :return: Timezone aware endtime
        """
        duration = self.get_duration()

        return self.get_datetime(mentor=mentor, user_timezone=user_timezone) + timedelta(minutes=duration)

    def get_raw_endtime(self):
        duration = self.get_duration()

        return self.get_datetime() + timedelta(minutes=duration)

    def get_timezone(self, mentor=None):
        """
        :return: Timezone of mentorship session
        """
        if mentor:
            timezone = mentor.timezone
        elif self.mentorship:
            timezone = self.mentorship.mentor.timezone
        else:
            try:
                timezone = self.teammentorshipsession.timezone
            except:
                timezone = None

        return timezone

    def get_datetime_str(self, mentor=None):
        mentor_timezone = self.get_timezone(mentor=mentor)
        try:
            result = timezone.localtime(self.datetime, pytz.timezone(mentor_timezone))
        except ValueError:
            try:
                result = timezone.make_aware(self.datetime, pytz.timezone(mentor_timezone))
            except ValueError:
                result = self.datetime

        return "{0} {1}".format(result.strftime('%b-%d-%Y %H:%M'), mentor_timezone)

    def get_mentee(self):
        """
        :return mentee of mentorship
        """
        return self.mentorship.student

    def get_duration(self):
        return DURATIONS[(self.duration - 1)][1]

    def get_meeting_type(self):
        return self.meeting_type

    def get_meeting_location(self):
        meeting_type = self.meeting_type
        if meeting_type == PHONE:
            return self.phone_number
        elif meeting_type == SKYPE:
            return self.video_id
        return self.location

    def get_location_or_type(self):
        """
        If meeting type is "In Person", returns location.
        Otherwise (when location is irrelevant), returns
        meeting type string.
        """
        if self.meeting_type == IN_PERSON:
            return self.location
        else:
            dictionary = dict(PREFERRED_CONTACT)
            return dictionary[self.meeting_type]

    def get_type_and_contact_info(self, url_only_if_video=False):
        """
        WARNING: This method is only for single-mentorship sessions.

        Returns a pretty rendering of session type and location in one string.

        Return examples:
            "In Person at 123 Main St."
            "Phone - (757) 123-4567"
            "Video Call - https://zoom.us/j/7571234567"
            "To be determined"

        If url_only_if_video (which we may prefer for location field of calendar app integration,
        return format of video call is url only. Eg:
            "https://zoom.us/j/7571234567"

        If location is undetermined, location becomes "(to be determined)"

        """
        if self.is_team_session():
            app_logger.error(f'Session ID = {self.id}: get_type_and_contact_info() can only be called '
                             f'on single-mentorship sessions, not team-mentorship sessions. Returning session.location ')
            return self.location

        session_type, session_location = get_session_type_and_location(self, self.mentorship.mentor)

        if not session_location:
            session_location = '(to be determined)'

        type_at_location = f"{session_type} at {session_location}"
        type_dash_location = f"{session_type} - {session_location}"

        if self.meeting_type == IN_PERSON:
            return type_at_location
        elif self.meeting_type == PHONE:
            return type_dash_location
        elif self.meeting_type == SKYPE:
            return session_location if url_only_if_video else type_dash_location
        elif self.meeting_type == TO_BE_DETERMINED:
            return session_type
        else:
            return session_type

    def get_export_location(self):
        """Exactly like get_type_and_contact_info, except if video call,
        only supply the link only. """
        return self.get_type_and_contact_info(url_only_if_video=True)

    def to_dict(self):
        raw = self.get_datetime()
        day = raw.date().strftime('%a')
        date = raw.date().strftime("%B %d %Y")
        month = raw.date().strftime("%B")
        daynumber = raw.date().strftime("%d")
        start_time = raw.time().strftime("%I:%M")
        end_time = self.get_endtime().time().strftime("%I:%M%p")
        timezone = ''
        if self.mentorship:
            mentor = self.mentorship.mentor
            timezone = mentor.get_timezone()
        tag = day + ", " + date + ", " + start_time + "-" + end_time + " " + timezone
        meeting_type = self.get_meeting_type()
        meeting_location = self.get_meeting_location()
        meeting = {
            'tag': tag,
            'id': self.id,
            'month': month[:3],
            'daynumber': daynumber,
            'start_time': start_time,
            'end_time': end_time,
            'timezone': timezone,
            'date': raw.date().strftime('%m/%d/%Y'),
            'duration': self.duration,
            'meeting_type': meeting_type,
            'meeting_location': meeting_location,
        }
        if self.mentorship:
            student, mentor = self.mentorship.get_student_mentor_dict(-1)  # canEdit doesn't matter. Should be False
            meeting['mentor'] = mentor
            meeting['mentee'] = student
        if self.note_set.all().exists():
            notes = []
            for note in self.note_set.all():
                notes.append(note.to_dict(None))
            meeting['feedbacks'] = notes
        return meeting


class MentorshipSessionSet(models.Model):
    mentorship = models.ForeignKey(Mentorship, on_delete=models.CASCADE)
    sessions = models.ManyToManyField(MentorshipSession)
    decided = models.DateTimeField(default=None, null=True)  # Null if and only if this is a pending set.

    def is_contains_future_session(self):
        """
        Returns true if self.sessions contains at least one session whose endtime in in the future.
        """
        for session in self.sessions.all():
            if session.get_endtime() > timezone.now():
                return True
        return False


class GoalSet(models.Model):
    timestamp = models.DateTimeField(default=timezone.now)
    mentorship = models.ForeignKey(Mentorship, on_delete=models.CASCADE)
    due_date = models.DateTimeField(default=None, null=True)
    mentorship_activity = models.OneToOneField(MentorshipActivity, on_delete=models.SET_NULL, null=True)

    def to_dict(self, current_member_id : int):
        goals = self.goal_set.select_related('creator').all()
        result = []
        for goal in goals:
            goal_json = goal.to_dict(current_member_id)
            goal_json['due_date'] = self.due_date.strftime("%Y-%m-%d") if self.due_date else ''
            goal_json['created_on'] = self.timestamp.strftime("%Y-%m-%d %-I:%M %p")
            result.append(goal_json)
        return result


class Goal(models.Model):
    completed = models.BooleanField(default=False)
    completed_on = models.DateTimeField(default=None, null=True)
    # When the goal is due
    due_date = models.DateTimeField(default=None, null=True)
    note = models.TextField(default=None, null=True)
    session = models.ForeignKey(MentorshipSession, null=True, default=None, on_delete=models.CASCADE)
    goal_set = models.ForeignKey(GoalSet, default=None, null=True, on_delete=models.CASCADE)

    creator = models.ForeignKey(StartupMember, null=True, default=None, on_delete=models.CASCADE)
    # Extra for now
    mentorship = models.ForeignKey(Mentorship, null=True, on_delete=models.CASCADE)
    # created_on
    datetime = models.DateTimeField(null=True, default=None)
    # For Admin Activity Feed
    mentorship_activity = models.OneToOneField(MentorshipActivity, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.note

    def get_creator_image(self):
        if not self.creator:
            return None
        return self.creator.get_image()

    def to_dict(self, current_member_id : int):
        goal = {
            'id': self.id,
            'completed': self.completed,
            'completed_on': self.completed_on.strftime("%Y-%m-%d %-I:%M %p") if self.completed_on else None,
            'due_date': self.due_date.strftime("%Y-%m-%d") if self.due_date else None,
            'note': self.note,
            'created_on': self.datetime.strftime("%Y-%m-%d %-I:%M %p") if self.datetime else None,
            'creator': None
        }
        if self.creator:
            goal['creator'] = {
                'name': self.creator.get_full_name(),
                'image': self.get_creator_image(),
                'canEdit': self.creator_id == current_member_id
            }
        return goal


class Note(models.Model):
    # for team mentorship, mentorship set to null and use mentorships in
    # TeamMentorshipSession to get them.
    mentorship = models.ForeignKey(Mentorship, on_delete=models.CASCADE, null=True)
    is_by_mentor = models.BooleanField(default=True)
    text = models.TextField(default="")
    timestamp = models.DateTimeField(default=timezone.now)
    session = models.ForeignKey(MentorshipSession, null=True, on_delete=models.CASCADE)
    group = models.ForeignKey(MentorGroup, null=True, on_delete=models.CASCADE)
    offline_date = models.DateTimeField(null=True, default=None)
    offline_duration = models.PositiveSmallIntegerField(
        choices=DURATIONS, default=None, null=True)
    rating = models.PositiveSmallIntegerField(
        choices=FEEDBACKS, null=True, default=NOT_YET)
    timestamp_edited = models.DateTimeField(null=True, auto_now=True)
    is_edited = models.BooleanField(default=False)

    mentorship_activity = models.OneToOneField(MentorshipActivity, on_delete=models.SET_NULL, null=True)

    def get_timestamp(self):
        return timezone.localtime(self.timestamp)

    def get_timestamp_edited(self):
        if self.timestamp_edited:
            return timezone.localtime(self.timestamp_edited)
        else:
            return None

    def to_dict(self, current_member : StartupMember):
        current_member_id = current_member.id if current_member else -1
        result = {
            'note': self.text,
            'created': self.timestamp.strftime('%b-%d-%Y %H:%M'),
            'edited': self.timestamp_edited.strftime('%b-%d-%Y %H:%M'),
            'id': self.id
        }
        if self.session:
            result['meeting'] = {
                'meeting_date': self.session.get_datetime_str(),
                'meeting_type': self.session.get_type_and_contact_info(),
                'by_mentor': self.is_by_mentor,
                'rating': self.rating
            }
        if self.mentorship:
            student, mentor = self.mentorship.get_student_mentor_dict(current_member_id)
            result['mentor'] = mentor
            result['mentee'] = student
            if self.is_by_mentor:
                creator = mentor
            else:
                creator = student
            result['creator'] = creator
        if self.noteattachment_set.all().exists():
            attachments = []
            for attachment in self.noteattachment_set.all():
                attachments.append(attachment.to_dict())
            result['attachments'] = attachments
        return result


class NoteAttachment(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    attachment = models.FileField(
        null=True,
        default=None,
        upload_to=get_attached_file_url,
        max_length=700,
        validators = [file_whitelist_validator]
    )
    file_name = models.TextField(null=True, default=None)

    def save(self, *args, **kwargs):
        """ Note: This method is almost identical to MentorshipFile.save().
        If modifying this method, consider making analogous changes to the other.
        """
        super(NoteAttachment, self).save(*args, **kwargs)
        if settings.IS_STAGING or settings.IS_PRODUCTION:
            if self.attachment:
                conn = boto.s3.connection.S3Connection(
                    settings.AWS_ACCESS_KEY_ID,
                    settings.AWS_SECRET_ACCESS_KEY)
                # If the bucket already exists, this finds that, rather than
                # creating.
                try:
                    bucket = conn.create_bucket(settings.AWS_STORAGE_BUCKET_NAME)
                except boto.exception.S3CreateError:
                    raise RuntimeError()
                k = boto.s3.key.Key(bucket)
                k.key = str(self.attachment)
                try:
                    k.set_acl('private')
                except boto.exception.S3ResponseError:
                    raise RuntimeError()

    def to_dict(self):
        return {
            'name': self.file_name,
            'attachment': reverse('mentorship-note-attached', args=(self.id,)),
            'id': self.id
        }


class MentorshipFile(models.Model):
    """ A mentorship file is either
    1. associated with a single mentor/mentee relationship (single_mentorship)
    OR
    2. associatd with a team that participates in team mentoring (and implicitly
    associated with all of the mentors in the TeamMentorship object associated
    with the team.)

    In other words, exactly one of the fields (single_mentorship or team) is not null.
    """
    single_mentorship = models.ForeignKey('Mentorship', null=True, default=None, on_delete=models.CASCADE)
    team = models.ForeignKey('branch.Startup', null=True, default=None, on_delete=models.CASCADE)
    file = models.FileField(upload_to=get_mentorship_file_url, max_length=700, validators=[file_whitelist_validator])
    file_name = models.TextField(null=True, default=None)
    created_on = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=700, blank=True)

    def save(self, *args, **kwargs):
        """ Note: This method is almost identical to NoteAttachment.save().
        If modifying this method, consider making analogous changes to the other.
        """
        super(MentorshipFile, self).save(*args, **kwargs)
        if settings.IS_STAGING or settings.IS_PRODUCTION:
            if self.file:
                conn = boto.s3.connection.S3Connection(
                    settings.AWS_ACCESS_KEY_ID,
                    settings.AWS_SECRET_ACCESS_KEY)
                # If the bucket already exists, this finds that, rather than
                # creating.
                try:
                    bucket = conn.create_bucket(settings.AWS_STORAGE_BUCKET_NAME)
                except boto.exception.S3CreateError:
                    raise RuntimeError()
                k = boto.s3.key.Key(bucket)
                k.key = str(self.file)
                try:
                    k.set_acl('private')
                except boto.exception.S3ResponseError:
                    raise RuntimeError()


class TeamMentorshipActivityManager(models.Manager):
    def create_activity(self, activity_type, team, platform):
        return self.create(activity_type=activity_type,
                           team=team,
                           platform=platform,
                           timestamp=timezone.now())

    def activity_select_related(self):
        return self.select_related(
            'mentorship',
            'mentorship__mentor',
            'mentorship__mentor__profile',
            'mentorship__student',
            'goalset',
            'goalset__mentorship',
            'goalset__mentorship__mentor',
            'goalset__mentorship__mentor__profile',
            'goalset__mentorship__student',
            'note',
            'note__mentorship',
            'note__mentorship__mentor',
            'note__mentorship__mentor__profile',
            'note__mentorship__student',
            'mentorshipsession',
            'mentorshipsession__mentorship',
            'mentorshipsession__mentorship__mentor',
            'mentorshipsession__mentorship__mentor__profile',
            'mentorshipsession__mentorship__student',
            'team'
        )

    def all_platform_regular_activities(self, platform: University):
        return self.activity_select_related().filter(platform_id=platform.id)

    def order_activities(self, query):
        return query.order_by('-timestamp')


class TeamMentorshipActivity(MentorshipActivity):
    '''
    An object representing an item to appear on the admin team mentorship activity feed.

    Each activity corresponds to a particular team mentorship item being created.

    Activities should have a one-to-one relation to models (ie note creation, goal setting, meeting scheduled)

    In order to add a new activity type to this feed:

    1. See if it has an 'activity type code' (constants at top). If not, give it one.
    2. Fill out case for each getter (get_activity_name, get_body, get_body_html)
    3. Add one-to-one field to model (and then run makemigrations/migrate).
        eg: team_mentorship_activity = models.OneToOneField(TeamMentorshipActivity, null=True)
    4. If there is a way to recreate the past activities of this type, add
        then do so via making a custom command that will traverse all objects of this type
        and create corresponding team mentorship activity objects.

    '''
    team = models.ForeignKey(Startup, null=True, on_delete=models.CASCADE)

    objects = TeamMentorshipActivityManager()

    def get_activity_name(self):
        ''' name of activity
        '''
        if self.activity_type == MentorshipActivity.NOTE_CREATED:
            return "Team Note Created"
        elif self.activity_type == MentorshipActivity.GOALSET_CREATED:
            return "Team Goal Created"
        elif self.activity_type == MentorshipActivity.SESSION_SCHEDULED:
            return "Team Mentorship Meeting Scheduled"
        elif self.activity_type == MentorshipActivity.CONNECTION_MADE:
            return "Team Mentorship Connection Made"

    def get_body(self, full=False, current_member=None, mark_deleted=False):
        '''
        string representation of activity (without time).

        Everything is in try-except blocks because the corresponding activity
        may have been deleted.
        '''
        full_body = {}
        result = None
        deleted = False
        if full:
            full_body = self.to_dict()
        current_member_id = current_member.id if current_member else -1
        if self.activity_type == MentorshipActivity.NOTE_CREATED:
            try:
                note = self.note.teamnote
                if full:
                    result = note.to_dict(current_member)
                else:
                    team_name = note.team.name
                    creator_name = note.creator.get_full_name()
                    result = "Note created by " + creator_name + " for " + team_name + ". "
            except models.ObjectDoesNotExist:
                result = "Deleted Note"
                deleted = True
        elif self.activity_type == MentorshipActivity.GOALSET_CREATED:
            try:
                goal = self.goal.teamgoal
                if full:
                    result = goal.to_dict(current_member_id)
                else:
                    team_name = goal.team.name
                    creator_name = goal.creator.get_full_name()
                    result = "Goal created by " + creator_name + " for " + team_name + ". "
            except models.ObjectDoesNotExist:
                result = "Deleted Goals"
                deleted = True
        elif self.activity_type == MentorshipActivity.SESSION_SCHEDULED:
            try:
                session: TeamMentorshipSession = self.mentorshipsession.teammentorshipsession
                if full:
                    result = session.to_dict()
                else:
                    team = session.team
                    team_name = team.name
                    mentors = session.get_mentors()

                    names = []
                    for mentor in mentors:
                        name = mentor.profile.get_full_name()
                        names.append(name)
                    mentor_names = ", ".join(names)

                    datetime_str = format_datetime_12hr(session.datetime, session.get_platform().style.timezone)

                    result = f"Meeting scheduled between {team_name} and {mentor_names} for {datetime_str}."
            except models.ObjectDoesNotExist:
                result = "Deleted Meeting"
                deleted = True
        elif self.activity_type == MentorshipActivity.CONNECTION_MADE:
            try:
                mentorship = self.mentorship.teammentorship
                if full:
                    result = mentorship.to_dict(current_member_id)
                else:
                    mentor_name = mentorship.mentor.profile.get_full_name()
                    team_name = mentorship.team.name
                    result = "Connection made between " + mentor_name + " and " + team_name + "."
            except models.ObjectDoesNotExist:
                result = "Deleted connection"
                deleted = True
        if full:
            full_body['deleted'] = deleted
            full_body['full_body'] = result
            result = full_body
        if mark_deleted:
            return deleted, result
        return result

    def get_body_html(self):
        ''' html representation of activity (eg to allow for clickable links)
        '''
        pass


class TeamMentorshipManager(models.Manager):
    def create_new_mentorship(self, team, mentor, platform):
        if self.mentorship_exists(team, mentor):
            existing = self.get(team=team, mentor=mentor)
            if existing.is_active:
                raise ValueError("Already active mentorship.")
            else:
                existing.is_active = True
                existing.save()
            return existing
        else:
            activity = TeamMentorshipActivity.objects.create_activity(MentorshipActivity.CONNECTION_MADE, team, platform)
            return self.create(team=team, mentor=mentor, mentorship_activity=activity, is_active=True)

    def mentorship_exists(self, team, mentor):
        if self.filter(team=team, mentor=mentor).exists():
            return True
        else:
            return False


class TeamMentorship(Mentorship):
    """
    Mentorship between a mentor and a student
    """
    team = models.ForeignKey(Startup, on_delete=models.CASCADE)

    objects = TeamMentorshipManager()

    def __str__(self):
        return "(" + str(self.team) + ", " + str(self.mentor) + ")" if self.is_active == True else "INACTIVE"

    def to_dict(self, current_member_id : int):
        mentor_profile = self.mentor.profile
        mentor = {
            'name': mentor_profile.get_full_name(),
            'image': mentor_profile.get_image(),
            'canEdit': mentor_profile.id == current_member_id
        }
        result = {
            'mentor': mentor,
            'team': {
                'name': self.team.name,
                'url_name': self.team.url_name,
                'image': self.team.get_image()
            },
            'is_active': self.is_active,
            'created': self.created_on.strftime("%Y-%m-%d %-I:%M %p"),
            'started': self.started_on.strftime("%Y-%m-%d %-I:%M %p") if self.started_on else None,
            'ended': self.ended_on.strftime("%Y-%m-%d %-I:%M %p") if self.ended_on else None
        }
        return result


class TeamMentorshipSession(MentorshipSession):
    """
    Session between a team and (potentially) multiple mentors


    Currently, team mentorships are restricted to mentors within the platform.
        If team mentoring is changed to allow sessions with mentors
        on different platforms, adjustments will need to be made for parts of the codebase
        that rely on knowing which platform the team mentorship session belongs to.


    Hint:
        The way to access the child TeamMentorshipSession object from a MentorshipSession object, session, is:
            session.teammentorshipsession

        The way to access the parent MentorshipSession object from a TeamMentorshipSession object, team_session, is:
            team_session.mentorshipsession_ptr
    """
    team = models.ForeignKey(Startup, on_delete=models.CASCADE)
    creator = models.ForeignKey(StartupMember, on_delete=models.CASCADE)
    members_attending = models.ManyToManyField(UserEmail)  # refers to mentees attending
    mentorships = models.ManyToManyField(TeamMentorship)
    TIMEZONES = [(t, t) for t in common_timezones]
    timezone = models.CharField(
        max_length=100,
        choices=TIMEZONES,
        default=EASTERN)

    def get_mentors(self):
        """ Returns queryset of the mentors attending this meeting.
        """
        return Mentor.objects.select_related('profile').filter(mentorship__teammentorship__teammentorshipsession__id=self.id)

    def get_datetime(self, mentor=None, user_timezone=None):
        """
        :param mentor:
        :return: Timezone aware datetime
        """
        if user_timezone:
            if isinstance(user_timezone, str):
                user_timezone = pytz.timezone(user_timezone)
            return self.datetime.astimezone(user_timezone)
        return self.datetime.astimezone(pytz.timezone(self.timezone))

    def get_endtime(self, mentor=None, user_timezone=None):
        """
        :return: Timezone aware endtime
        """
        if user_timezone:
            if isinstance(user_timezone, str):
                user_timezone = pytz.timezone(user_timezone)
            return self.datetime.astimezone(user_timezone) + timedelta(minutes=DURATIONS[(self.duration - 1)][1])
        return self.datetime.astimezone(pytz.timezone(self.timezone)) + timedelta(minutes=DURATIONS[(self.duration - 1)][1])

    def get_datetime_utc(self):
        return self.datetime

    def get_endtime_utc(self):
        return self.datetime + timedelta(minutes=DURATIONS[(self.duration - 1)][1])

    def get_team(self):
        """
        :return mentee of mentorship
        """
        return self.team

    def get_timezone(self, mentor=None):
        return self.timezone

    def member_is_creator(self, emailuser_id : int):
        return self.creator.user_id == emailuser_id

    def member_drop(self, emailuser : UserEmail):
        self.members_attending.remove(emailuser)
        self.save()

    def member_is_attending(self, emailuser_id : int):
        if self.members_attending.filter(id=emailuser_id).exists():
            return True
        else:
            return False

    def to_dict(self):
        meeting = super(TeamMentorshipSession, self).to_dict()
        mentors = [mentorship.mentor.profile.get_full_name() for mentorship in self.mentorships.all()]
        creator = self.creator
        members_attending = self.members_attending.count()
        mentors_attending = self.mentorships.count()

        timezone = str(self.get_timezone())
        meeting['timezone'] = timezone
        meeting['tag'] = meeting['tag'] + timezone

        venture = {
            'name': self.team.name,
            'url_name': self.team.url_name,
            'image': self.team.get_image()
        }
        meeting.update({
            'mentors': mentors,
            'venture': venture,
            'members_count': members_attending,
            'mentors_count': mentors_attending,
        })
        return meeting

    def get_team_session_type(self):
        """ Returns string version of type"""
        return dict(PREFERRED_CONTACT)[self.meeting_type]

    def get_platform(self):
        mentorship : TeamMentorship = self.mentorships.first()
        return mentorship.mentor.platform


class TeamGoal(Goal):
    # Completed or not
    team = models.ForeignKey(Startup, null=True, default=None, on_delete=models.CASCADE)

    def to_dict(self,  current_member_id : int):
        result = super(TeamGoal, self).to_dict(current_member_id)
        result['team'] = {
            'name': self.team.name,
            'url_name': self.team.url_name,
            'image': self.team.get_image()
        }
        return result


class TeamNote(Note):
    team = models.ForeignKey(Startup, null=True, default=None, on_delete=models.CASCADE)
    creator = models.ForeignKey(StartupMember, null=True, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.text

    def get_meeting_date(self):
        session = self.session
        dt_str = dt.datetime.strftime(session.get_datetime(), '%m/%d/%Y')
        return dt_str

    def get_meeting_type_str(self):
        session = self.session
        if session.meeting_type == PHONE:
            return PHONE_STRING
        elif session.meeting_type == SKYPE:
            return SKYPE_STRING
        else:
            return IN_PERSON_STRING

    def get_creator_image(self):
        return self.creator.get_image()

    def to_dict(self, current_member):
        result = super(TeamNote, self).to_dict(current_member)
        result['creator'] = {
            'name': self.creator.get_full_name(),
            'image': self.creator.get_image(),
            'canEdit': current_member and self.creator_id == current_member.id
        }
        result['team'] = {
            'name': self.team.name,
            'url_name': self.team.url_name,
            'image': self.team.get_image()
        }
        return result


class MentorshipSessionFeedback(models.Model):
    """
    Note About Team Mentorship Feedback:
        A TeamMentorshipSessionFeedback sub-class, would not require new fields,
        so a sub-class for team mentorship feedback is not implemented.
    To filter by TeamMentorshipSession, use
        MentorshipSessionFeedback.objects.filter(session_id=team_session.id)
    """
    session = models.ForeignKey(MentorshipSession, related_name='mentorship_session', on_delete=models.CASCADE)
    from_member = models.ForeignKey(StartupMember, related_name='from_startupmember', on_delete=models.CASCADE)
    to_member = models.ForeignKey(StartupMember, related_name='to_startupmember', on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    rating = models.PositiveSmallIntegerField(choices=FEEDBACKS, default=NOT_YET)
    feedback = models.TextField(null=True)
    is_from_mentor = models.BooleanField(default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['session', 'to_member', 'from_member'], name='unique_session_feedback')
        ]

    def is_team(self):
        return self.session.is_team_session()

    @property
    def team_session(self):
        return self.session.teammentorshipsession if self.is_team() else None


class CronofyCacheSlot(models.Model):
    """
    This model serves 2 purposes:
    - To keep a record of current cronofy availability (for discover mentors calendar view purposes)
    - To keep a record of past cronofy availability, for analytics purposes. (This won't include timeslots used up for mentorship.)

    If the object exists, is_active=True, and start_datetime is in the future,
    then our cache states that mentor_user has a cronofy available slot on start_datetime.

    If the object exists, is_active=True, and start_datetime is in the past, then our cache states that the mentor_user
    at one point had a cronofy slot available then.

    We completely ignore objects where is_active=False. (Instead of deleting a cache object, we set is_active to False.
    This helps performance, in case we want to recreate a cache object with same mentor_user and start_datetime, we can
    reuse the old object.)

    Note: For analytical purposes, if you wish to determine all of the availability that the mentor gave for mentorship
    in the past, you would add both the mentor's past CronofyCacheSlot objects with the past "active" MentorshipSession objects.
    (active MentorshipSession objects being those "is_cancelled=True")
    """
    mentor_user = models.ForeignKey(UserEmail, on_delete=models.CASCADE)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    timestamp = models.DateTimeField(auto_now_add=True)  # for debugging purposes
    is_active = models.BooleanField(default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['mentor_user', 'start_datetime'], name='unique_cronofycache_start'),
        ]


class SendAllPostMeetingRemindersTimestamp(models.Model):
    """
    Keeps track of each time a platform admin has click "Send" for "Send All Post-Meeting Reminders" in

    Admin Panel -> Manage -> Mentorship -> View All Activity
    or
    Admin Panel -> Manage -> Team Mentorship -> View All Activity
    """
    platform = models.ForeignKey(University, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_team = models.BooleanField()