# STARTUPTREE Imports
from api.settings import (
    CRONOFY_CLIENT_ID,
    CRONOFY_SECRET
)
from emailuser.models import UserEmail
from mentorship.constants import CRONOFY_QUERY_SIZE_MAX_DAYS, CRONOFY_CACHE_DAYS_FUTURE
from mentorship.cronofy_cache import async__refresh_cronofy_cache__user
from mentorship.models import (
    DURATIONS,
    Mentor,
    MentorCronofySchedule,
    MentorScheduleBlackOut,
)
from StartupTree.loggers import app_logger
from StartupTree.settings import IS_DEV, IS_STAGING
from trunk.models import UniversityStyle, University
from StartupTree.strings import AUTO_CONFERENCE_DEFAULT_EMAIL, MIDDLE_MAN_EMAILUSER_ID_DEV
from emailuser.models import UserEmail
from emailuser.conf import PROTOCOL

# LIBRARY IMPORTS
from datetime import datetime, timedelta, time
import datetime as dtime
from dateutil import parser
import dateutil.parser
import json
from pycronofy.exceptions import PyCronofyRequestError
import pycronofy
import pytz
import requests
import traceback
from django.utils import timezone
import re

CRONOFY_SCOPE = 'read_events%20create_event%20delete_event'
CRONOFY_STATE = 'tree'
cronofy = pycronofy.Client(client_id=CRONOFY_CLIENT_ID,
                           client_secret=CRONOFY_SECRET)
# TODO: Maybe move this to api utils if it will be used for more than just mentorship
CRONOFY_API_AUTH = {
    'Authorization': 'Bearer {0}'.format(CRONOFY_SECRET),
    'Content-Type': 'application/json; charset=utf-8'
}


def get_range(page):
    """ Returns the monday and next sunday corresponding to the page number
    where page 1 refers to this week, and incrementing/decrementing causes
    dates to go forward/backward in time by a week.

    range is a tuple (start, end) of datetime objects representing the very start
    and very end of the week.
    """
    today = datetime.today().date()
    days_to_next_monday = 0 - today.weekday() + (7 * (page-1))
    next_monday = today + timedelta(days_to_next_monday)
    next_sunday = next_monday + timedelta(6)

    start = datetime.combine(next_monday, time.min)
    end = datetime.combine(next_sunday, time.max)

    range = (start, end)

    return range, next_monday, next_sunday


def get_slots(user, university, is_mentor=None, range=None):
    """ Given a date range, get slot information from the cronofy user.

    Note: due to limitations with cronofy, slots in the past cannot be shown.
    """

    (start, end) = range

    # cronofy doesn't allow requests to the past. so adjust start to "now"
    # if start is before now; and return nothing if end is before now.
    utc_now = datetime.utcnow()
    if start < utc_now < end:
        range = (utc_now, end)
    elif end <= utc_now:
        return []

    try:
        slots = read_availability(user, university, is_mentor=is_mentor, range=range)
    except PyCronofyRequestError:
        # TODO USER NEEDS TO RE-AUTHENTICATE
        return []

    return slots


def get_choice_code_day(date):
    """Given a date, convert it to the correct choice code for MentorSchedule.day field. """
    dictionary = dict((day, code) for (code, day) in MentorCronofySchedule.DAYS)
    key = date.strftime("%a")
    return dictionary[key]


def get_type_location(mentor, date):
    """Given a date and a mentor, return the type/location of meeting as specified by
    mentor setting for cronofy users.

    If MentorCronofySchedule does not exist for mentor and day, or fields are empty/none, return
    mentor defaults.
    """
    day_code = get_choice_code_day(date)
    try:
        settings = MentorCronofySchedule.objects.get(mentor=mentor, day=day_code)
        result_type = settings.type if settings.type is not None else mentor.default_meeting_type
        result_location = settings.location if settings.location and settings.location not in ['', 'None'] \
            else mentor.default_location
    except MentorCronofySchedule.DoesNotExist:
        result_type, result_location = mentor.default_meeting_type, mentor.default_location
    return result_type, result_location


def get_choice_code_duration(start_time, end_time):
    """Given a start_time and end_time, return the correct duration choice code per mentorship.models.DURATIONS"""
    dictionary = dict((minutes, code) for (code, minutes) in DURATIONS)
    delta = end_time - start_time
    minutes = (delta.seconds) / 60
    return dictionary[minutes]


def process_slot(availability, slot, user_timezone):
    """Given a cronofy slot, format it and add it to the availability list.

    Side effects: mutates `availability` by adding slot info to it. """

    start_time = parser.parse(slot['start']).astimezone(user_timezone)
    end_time = parser.parse(slot['end']).astimezone(user_timezone)
    date = start_time.date()
    duration = get_choice_code_duration(start_time, end_time)

    temp = [start_time.strftime("%I:%M%p"), end_time.strftime("%I:%M%p"), True, duration]

    availability[date.weekday()][2].append(temp)


def get_possible_calendar_schedules_cronofy(mentor, page, user_timezone):
    """Exactly like mentorship.views.mixins.get_possible_calendar_schedules except
    for cronofy mentors instead of old scheduling mentors.

    Gets open timeslots of a mentor for a given week in page,
    with output formatted similarly to get_possible_calendar_schedules.

    Requires: mentor_id be a cronofy user
    from mentorship.views.mixins import add_scheduled_sessions
    Need to call add_scheduled_sessions after.
     """
    user = mentor.profile.user

    availability_range, next_monday, next_sunday = get_range(page)

    slots = get_slots(user, mentor.platform, is_mentor=True, range=availability_range)

    availability = []
    tmp_date = next_monday
    dates_this_week = []

    # Roughly speaking, the format of availability entry this, where there is
    # a corresponding entry for each day of the week:
    # ['Monday', 'April 12', [[2:00, 2:15, True, 1],[2:15: 2:30, True, 3],...], 2016, 'In Person', '409 College Ave', '2016-04-12']
    while tmp_date <= next_sunday:
        meeting_type, location = get_type_location(mentor, tmp_date)

        availability.append([tmp_date.strftime('%a'), tmp_date.strftime('%b %d'), [], tmp_date.year,
                            meeting_type, location, tmp_date])
        dates_this_week.append(tmp_date)

        tmp_date += timedelta(1)

    for slot in slots:
        process_slot(availability, slot, user_timezone)

    return availability, availability_range


def get_cronofy_meeting_type_location(mentorship, location, selected_day):
    # for scheduling through cronofy
    meeting_type = mentorship.mentor.default_meeting_type
    if location == '' and selected_day != '':
        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        for day in range(0, 7):
            if days[day] == selected_day:
                sched_obj = MentorCronofySchedule.objects.get(mentor=mentorship.mentor, day=day)
                meeting_type = sched_obj.type
                # SW-2783 fix
                if meeting_type is None:
                    meeting_type = mentorship.mentor.default_meeting_type
                    sched_obj.type = meeting_type
                    sched_obj.save()
                location = sched_obj.location if sched_obj.location and sched_obj.location not in ['', 'None'] \
                    else mentorship.mentor.default_location
                break
    return meeting_type, location


def get_element_token_general(user, request__get_host):
    account_id = get_cronofy_account_info(user)['account_id']
    try:
        element_token = generate_element_token(['managed_availability', 'account_management'], [account_id],
                                               f'https://{request__get_host}', user)
    except ValueError:
        element_token = None

    return element_token


def generate_element_token(permissions, subs, origin, user):
    '''params as described here: https://docs.cronofy.com/developers/ui-elements/authentication/
    Note that currently origin must be the base site name i.e cornell.startuptreetest.co

    This method makes an API call and raises a ValueError on failure
    (and thus calls to this method should be wrapped in a try-except that catches ValueError).
    '''
    params = {
        "version": "1",
        "permissions": permissions,
        "subs": subs,
        "origin": origin,
    }
    r = requests.post(
        'https://api.cronofy.com/v1/element_tokens', headers=CRONOFY_API_AUTH, json=params)
    if r.status_code == 422:
        establish_cronofy_for_user(user)
        r = requests.post(
            'https://api.cronofy.com/v1/element_tokens', headers=CRONOFY_API_AUTH, json=params)

    if r.status_code == 200:
        return r.json()['element_token']['token']
    else:
        msg = f'https://api.cronofy.com/v1/element_tokens returned with status code {r.status_code} for user {user}. ' \
              f'Errors = {json.loads(r.content)["errors"]}'
        app_logger.error(msg)
        raise ValueError(msg)


def establish_cronofy_for_user(user):
    ''' Establish Cronofy client, refreshing token if needed '''
    cronofy = pycronofy.Client(
        client_id=CRONOFY_CLIENT_ID,
        client_secret=CRONOFY_SECRET,
        access_token=user.cronofy_access_token,
        refresh_token=user.cronofy_refresh_token,
        token_expiration=dateutil.parser.parse(user.cronofy_token_expiration),
    )
    if cronofy.is_authorization_expired():
        return refresh_cronofy_token(user, cronofy)
    else:
        return cronofy


def generate_cronofy_url(request, redirect_uri):
    '''helper function to create url to display for authentication'
    :param: redirect_uri - the relative url (ie reverse (mentorship-setting)) '''
    CRONOFY_REDIRECT = request.build_absolute_uri(
        redirect_uri)
    app_logger.debug(CRONOFY_REDIRECT)
    if IS_DEV:  # hard to test more than one without <avoid_linking> param
        cronofy_url = 'https://app.cronofy.com/oauth/authorize?response_type=code&client_id={0}&redirect_uri={1}&scope={2}&state={3}&avoid_linking=true'.format(
            CRONOFY_CLIENT_ID,
            CRONOFY_REDIRECT,
            CRONOFY_SCOPE,
            CRONOFY_STATE
        )
    else:
        cronofy_url = 'https://app.cronofy.com/oauth/authorize?response_type=code&client_id={0}&redirect_uri={1}&scope={2}&state={3}'.format(
            CRONOFY_CLIENT_ID,
            CRONOFY_REDIRECT,
            CRONOFY_SCOPE,
            CRONOFY_STATE
        )

    return cronofy_url


def mentor_cronofy_authorization_finished(user: UserEmail,
                                          university: University,
                                          access_code: str,
                                          redirect_uri: str):
    """The steps needed after cronofy finishes authorizing.
    (Eg updating UserEmail cronofy credentials and cronofy availability rules.)

    redirect_uri should be absolute. if not, then absolute uri can be evaluated via:
        request.build_absolute_uri(<relative_uri>)
    """
    authenticate_cronofy_help(user, access_code, redirect_uri)  # we have to authenticate them via OAuth
    create_or_update_availabilty_rules(user, university)  # also create availability rule
    return


def authenticate_cronofy(request, user, relative_redirect_uri):
    '''Used for initial cronofy oauth authentication for user, calls update_cronofy_credentials
    for saving in db

    This method makes an API call, and thus calls to this method should be wrapped in a try-except that catches PyCronofyRequestError.

    Note: Redirect URI is necessary; if the original redirect URI does not have search params,
    then it is fine to supply the empty string as redirect_uri.
    However, if the original redirect URI has search params, then then we must supply
    the full uri (with search params).
    '''
    access_code = request.GET.get('code')
    absolute_redirect_uri = request.build_absolute_uri(relative_redirect_uri)
    authenticate_cronofy_help(user, access_code, absolute_redirect_uri)


def authenticate_cronofy_help(user, access_code, redirect_uri):
    auth = cronofy.get_authorization_from_code(access_code, redirect_uri=redirect_uri)
    update_cronofy_credentials(user, auth)

    async__refresh_cronofy_cache__user(user)


def update_cronofy_credentials(user, auth):
    '''Initialize cronofy client and save new credentials in db
    Requires request to not be None if mentor is syncing with personal
    calendar for the first time (user.cronofy_account_id is None).
    '''

    cronofy = pycronofy.Client(
        client_id=CRONOFY_CLIENT_ID,
        client_secret=CRONOFY_SECRET,
        access_token=auth['access_token'],
        refresh_token=auth['refresh_token'],
        token_expiration=dateutil.parser.parse(auth['token_expiration']),
    )

    user.cronofy_access_token = auth['access_token']
    user.cronofy_refresh_token = auth['refresh_token']
    user.cronofy_token_expiration = auth['token_expiration']
    if not user.cronofy_account_id:
        account_info = cronofy.account()
        user.cronofy_account_id = account_info['account_id']
    user.save()

    return cronofy


def is_error_require_user_reauth(e):
    '''
    Takes a PyCronofyRequestError (as `e`) and returns whether comes from a refresh_token request
    that returned an invalid_grant error, and thus requires Mentor to reauthenticate
    '''
    response_body = json.loads(e.response.text)
    request_body = json.loads(e.request.body)
    return request_body['grant_type'] == 'refresh_token' and response_body["error"] == 'invalid_grant'


def refresh_cronofy_token(user, cronofy):
    '''If access token is expired, this method is used to retrieve a new one and save it in db

    This method makes an API call, and thus calls to this method should be wrapped in a try-except that catches PyCronofyRequestError.

    Also, this method throws a ValueError("user_reauthenticate") in case Mentor MUST reauthenticate
    Per https://docs.cronofy.com/developers/api/authorization/refresh-token/#invalid-grant

    Thus, calls to this method should also catch ValueError.
    '''
    try:
        auth = cronofy.refresh_authorization()
        return update_cronofy_credentials(user, auth)
    except PyCronofyRequestError as e:
        if is_error_require_user_reauth(e):
            # Mentor MUST reauthenticate. We log an error message indicating this, and propagate the error
            app_logger.error(
                "cronofy.refresh_authorization() returned with 'invalid_grant'; mentor MUST reauthenticate.")
        raise e


def handle_unlink_all_calendars(user) -> bool:
    """
    The steps needed for mentor to opt out of cronofy sync.

        - Cronofy API call to delete availability rules.
        - Cronofy API call to revoke the user's authorization.
        - asynchronously refreshes (clears) their db cronofy cache. See mentorship.cronofy_cache for specs.
    """
    try:
        delete_availability_rules(user)
    except PyCronofyRequestError:
        pass
    try:
        revoke_cronofy_auth(user)
        return True
    except TypeError:
        app_logger.error(traceback.format_exc())
        return False


def revoke_cronofy_auth(user):
    '''If user requests to unlink calendars remove their credentials

    This method makes an API call, and thus calls to this method should be wrapped in a try-except that catches PyCronofyRequestError.
    '''
    if is_cronofy_linked(user):
        try:
            cronofy = establish_cronofy_for_user(user)
            cronofy.revoke_authorization()
        except PyCronofyRequestError:
            pass
        user.cronofy_access_token, user.cronofy_refresh_token, user.cronofy_token_expiration, user.cronofy_account_id = None, None, None, None
        user.save()

        async__refresh_cronofy_cache__user(user)  # call has effect of clearing any cronofy-cache objects set in the future.
    else:
        app_logger.warn('Attempting to revoke authorization for an account that has already been revoked')


def get_cronofy_user_calendars(user):
    '''Returns information about which calendars (profiles) user has linked
    format: {
      "provider_name": "google",
      "profile_id": "pro_n23kjnwrw2",
      "profile_name": "example@cronofy.com",
      "calendar_id": "cal_n23kjnwrw2_jsdfjksn234",
      "calendar_name": "Home",
      "calendar_readonly": false,
      "calendar_deleted": false,
      "calendar_primary": true,
      "permission_level": "sandbox"
    }'''
    cronofy = establish_cronofy_for_user(user)

    return cronofy.list_calendars()


def get_cronofy_account_info(user):
    '''Returns account information about a user in format:
    {
    "account_id": "acc_5700a00eb0ccd07000000000",
    "email": "janed@company.com",
    "name": "Jane Doe",
    "scope": "read_events create_event delete_event",
    "default_tzid": "Europe/London"
    }'''
    cronofy = establish_cronofy_for_user(user)

    return cronofy.account()


def create_or_update_availabilty_rules(user, university):
    '''beta feature

    This method makes an API call and raises a ValueError on failure
    (and thus calls to this method should be wrapped in a try-except that catches ValueError).
    '''
    all_calendar_ids = [calendar['calendar_id']
                        for calendar in get_cronofy_user_calendars(user)]
    availability_id = str(user.id)
    try:
        mentor = Mentor.objects.get(profile__user_id=user.id, platform_id=university.id)
        tzid = mentor.timezone
    except Mentor.DoesNotExist:
        tzid = mentor.timezone
    days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']
    start_time = '09:00'
    end_time = '17:00'
    weekly_periods = [
        {
            'day': day,
            'start_time': start_time,
            'end_time': end_time
        } for day in days
    ]
    data = {
        'availability_rule_id': availability_id,
        'tzid': tzid,
        'calendar_ids': all_calendar_ids,
        'weekly_periods': weekly_periods,
    }
    ACCESS_AUTH = {
        'Authorization': 'Bearer {0}'.format(establish_cronofy_for_user(user).auth.access_token),
        'Content-Type': 'application/json; charset=utf-8'

    }
    r = requests.post(
        'https://api.cronofy.com/v1/availability_rules', json=data, headers=ACCESS_AUTH)

    async__refresh_cronofy_cache__user(user)

    if r.status_code < 300:
        return r.content
    else:
        msg = f'https://api.cronofy.com/v1/availability_rules returned with status code {r.status_code} for user {user}. ' \
              f'Errors = {json.loads(r.content)["errors"]}'
        app_logger.error(msg)
        raise ValueError(msg)


def update_cronofy_timezone_availability(mentor):
    """
    Updates all availbility rules to the mentor's current timezone (mentor.timezone).

    However, the clock-face time of each availability rule stays the same (not converted to new timezone.)

    This method makes an API call and raises a ValueError on failure
    (and thus calls to this method should be wrapped in a try-except that catches ValueError).
    """
    user = mentor.profile.user
    data = json.loads(read_availability_rules(user))['availability_rule']

    data['tzid'] = mentor.timezone

    ACCESS_AUTH = {
        'Authorization': 'Bearer {0}'.format(establish_cronofy_for_user(user).auth.access_token),
        'Content-Type': 'application/json; charset=utf-8'

    }
    r = requests.post(
        'https://api.cronofy.com/v1/availability_rules', json=data, headers=ACCESS_AUTH)

    async__refresh_cronofy_cache__user(user)

    if r.status_code < 300:
        return r.content
    else:
        msg = f'https://api.cronofy.com/v1/availability_rules returned with status code {r.status_code} for user {user}. ' \
              f'Errors = {json.loads(r.content)["errors"]}'
        app_logger.error(msg)
        raise ValueError(msg)


def read_availability_rules(user):
    '''beta feature

    This method makes an API call and raises a ValueError on failure
    (and thus calls to this method should be wrapped in a try-except that catches ValueError).
    '''
    ACCESS_AUTH = {
        'Authorization': 'Bearer {0}'.format(establish_cronofy_for_user(user).auth.access_token),
        'Content-Type': 'application/json; charset=utf-8'

    }
    r = requests.get(
        'https://api.cronofy.com/v1/availability_rules/{0}'.format(user.id), headers=ACCESS_AUTH)

    if r.status_code < 300:
        return r.content
    else:
        msg = f'https://api.cronofy.com/v1/availability_rules returned with status code {r.status_code} for user {user}. ' \
              f'Errors = {json.loads(r.content)["errors"]}'
        app_logger.error(msg)
        raise ValueError(msg)


def delete_availability_rules(user):
    '''beta feature
    This method makes an API call and raises a ValueError on failure
    (and thus calls to this method should be wrapped in a try-except that catches ValueError).
    '''
    ACCESS_AUTH = {
        'Authorization': 'Bearer {0}'.format(establish_cronofy_for_user(user).auth.access_token),
        'Content-Type': 'application/json; charset=utf-8'

    }
    try:
        r = requests.delete(
            'https://api.cronofy.com/v1/availability_rules/{0}'.format(user.id), headers=ACCESS_AUTH)
        return r.status_code
    except requests.HTTPError:
        pass


def is_cronofy_linked(user):
    '''Return true if user has linked their calendar via cronofy'''
    return all([user.cronofy_access_token, user.cronofy_refresh_token, user.cronofy_token_expiration])


def get_cronofy_calendar_id(user):
    '''Returns the cronofy calendar id for a user'''
    to_return = None
    idx = 0
    for calendar in get_cronofy_user_calendars(user):
        if calendar['calendar_readonly']:
            app_logger.debug(
                'Warning: Calendar is read only, cannot publish events')
            continue
        elif calendar['calendar_deleted']:
            app_logger.debug('Warning: Calendar has been deleted')
            continue
        if calendar['calendar_primary']:
            return calendar['calendar_id']
        if idx == 0:
            to_return = calendar['calendar_id']
            idx += 1
    return to_return


def resync_calendar_events(user):
    cronofy = establish_cronofy_for_user(user)
    from_date = timezone.now()
    events = cronofy.read_events(from_date=from_date)
    for event in events:
        if event.get('event_id'):
            calendar_id = event['calendar_id']
            cronofy_event = {
                'event_id': event['event_id'],
                'summary': event['summary'],
                'description': event['description'],
                'start': event['start'],
                'end': event['end'],
                'location': event['location'],
            }
            cronofy.upsert_event(calendar_id=calendar_id, event=cronofy_event)


def cronofy_publish_to_calendar(user, calendar_id, event, refresh_cronofy_cache):
    '''Pushes the event to target user's calendar
    This method makes an API call, and thus calls to this method should be wrapped in a try-except that catches PyCronofyRequestError.

    If adding to a mentor's calendar, refresh_cronofy_cache should be true (since that would effect the mentor's availability).

    '''
    cronofy = establish_cronofy_for_user(user)
    result = cronofy.upsert_event(calendar_id=calendar_id, event=event)

    if refresh_cronofy_cache:
        async__refresh_cronofy_cache__user(user)

    return result


def cronofy_delete_from_calendar(user, calendar_id, event_id, refresh_cronofy_cache):
    '''Delete's the event from the user's calendar
    This method makes an API call, and thus calls to this method should be wrapped in a try-except that catches PyCronofyRequestError.

    If deleting from a mentor's calendar, refresh_cronofy_cache should be true (since that would effect the mentor's availability).
    If deleting from a non-mentor calendar (eg, admin calendar), refresh_cronofy_cache should be false.
    '''
    cronofy = establish_cronofy_for_user(user)
    cronofy.delete_event(calendar_id, event_id)

    if refresh_cronofy_cache:
        async__refresh_cronofy_cache__user(user)


def get_personal_events(user, from_date, to_date):
    """
    This method makes an API call, and thus calls to this method should be wrapped in a try-except that catches PyCronofyRequestError.
    """
    cronofy = establish_cronofy_for_user(user)
    events = cronofy.read_events(from_date=from_date, to_date=to_date)
    # In UTC
    # Each element is (start_time, end_time) for an event
    event_times = [(event['start'], event['end']) for event in events]
    event_times_objs = [
        (dtime.datetime.strptime(time[0], "%Y-%m-%dT%H:%M:%SZ"), dtime.datetime.strptime(time[1], "%Y-%m-%dT%H:%M:%SZ"))
        for time in event_times]

    return event_times_objs


def is_dt_aware(dt_input: dtime.datetime):
    """ Returns whether a datetime object is aware"""
    return dt_input.tzinfo is not None and dt_input.tzinfo.utcoffset(dt_input) is not None


def dt_to_iso8601(dt_input: dtime.datetime):
    """
    If input is naive, we assume timezone is UTC
    """
    iso_format = '%Y-%m-%dT%H:%M:%SZ'

    if is_dt_aware(dt_input):
        return dtime.datetime.strftime(dt_input.astimezone(pytz.utc), iso_format)
    else:
        return dtime.datetime.strftime(dt_input, iso_format)


class QueryPeriods:
    """
    Module for obtaining query_periods for cronofy API calls, taking into account MentorBlackouts.

    Use get_sets_of_query_periods for Cronofy Availability API calls (mentorship.st_cronofy.read_availability)
    Use get_query_periods_for_availability_viewer for Cronofy Availability Viewer API

    """
    @staticmethod
    def __get_query_periods__help(mentor: Mentor, dt_range):
        """
        Intermediate step before `get_query_periods`.

        dt_range should be timezone aware datetime bojects.

        Returns query_periods, except ignoring 35-day limit on period duration, and datetimes are in object form.

        Side effect: also deletes any expired blackout dates
        """

        blackouts = MentorScheduleBlackOut.objects.filter(mentor=mentor).order_by('start_date')

        (start, end) = dt_range

        query_periods = []
        next_query_start = start
        for blackout in blackouts:
            """
            Case 1:
                Blackout period ends before the next query period's start => blackout is irrelevant
            Case 2:
                next_query_start occurs between blackout start and end => next_query_start should be pushed to blackout end
            Case 3:
                blackout start occurs after next_query_start =>
                    query period can be fit between next_query_start and blackout start
                    next_query_start should be pushed to blackout end
            """
            b_end = blackout.get_day_after_end_dt()
            if b_end <= next_query_start:
                # Case 1
                # Because b_end is actually rounded up, then the case b_end == next_query_start applies here.
                # Eg. Suppose next_query_start is Jan 3 12AM. A blackout that is from Jan 1 to Jan 2 is irrelevant.
                # That said, b_end for this blackout is Jan 3 at 12 AM (equal to next_query_start).
                continue
            b_start = blackout.get_start_dt()
            if b_start > next_query_start:
                # Case 3
                query_periods.append({
                    'start': next_query_start,
                    'end': b_start,
                })
            next_query_start = b_end  # Case 2 and 3

        if next_query_start < end:
            query_periods.append({
                'start': next_query_start,
                'end': end,
            })

        return query_periods

    @staticmethod
    def __process_query_period(query_period):
        """ Returns a list of query periods covering the same interval, but each no longer than 35 days.

        Datetimes are in timezone-aware object form.
        To simplify arithmetic, the boundaries between spliced query-periods may touch.

        Base Case:
            If the original query period is not longer than 35 days, then we return a list containing the original.
        Recursive Case:
            Returns
        """
        start = query_period['start']
        end = query_period['end']

        if end - start <= timedelta(days=CRONOFY_QUERY_SIZE_MAX_DAYS):
            return [query_period]
        else:
            head_end = start + timedelta(days=CRONOFY_QUERY_SIZE_MAX_DAYS)
            next_start = head_end  # To simplify arithmetic, the boundaries between spliced query-periods may touch.

            head_query_period = {
                'start': start,
                'end': head_end,
            }

            tail_query_period = {
                'start': next_start,
                'end': end,
            }

            return [head_query_period] + QueryPeriods.__process_query_period(tail_query_period)

    @staticmethod
    def __group_query_periods(query_periods):
        """ Assumes query periods are non-overlapping and chronologically ordered.
        Groups a list of query_periods into list of list of query_periods such that
        each list of query_periods do not span 35 days.

        Datetimes should be in object form.
        """
        groups = []
        current_group = []

        def add_query_period_to_group(query_period):
            """ If appending the query_period to current_group would not violate the 35 day limit, it proceeds
            with appending, and return True. Otherwise, current_group is unchanged and returns False."""
            if len(current_group) == 0 or \
                    (query_period['end'] - current_group[0]['start'] < timedelta(days=35)):
                current_group.append(query_period)
                return True
            else:
                return False

        for query_period in query_periods:
            if not add_query_period_to_group(query_period):
                # current_group is at capacity and query_period not added
                # => current_group should be added to groups, while query_period heads a new current_group
                groups.append(current_group)
                current_group = [query_period]

        if len(current_group) > 0:
            groups.append(current_group)

        return groups

    @staticmethod
    def __query_periods_to_iso8601(query_periods):
        """Given a list of query_periods, whose datetimes are in object form,
        convert them to ISO 8601 form.

        This function mutates the original input.
        """
        for query_period in query_periods:
            query_period['start'] = dt_to_iso8601(query_period['start'])
            query_period['end'] = dt_to_iso8601(query_period['end'])

    @staticmethod
    def __get_query_periods(mentor, dt_range):
        """Given a cronofy-synced mentor, return a flat list of `query_periods` taking into account Mentor's blackout dates.

        dt_range is a tuple (a, b) where `a` and `b` are datetime.datetime objects
        representing the start/end times of desired availability range.

        `a` and `b` should be dt naive objects that we interpret as timezone UTC.

        A "set" of query_periods is a list that can be used as "query_periods" parameter for Cronofy API call.

        This is an intermediate step before get_sets_of_query_periods or get_query_periods_for_availability_viewer
        """
        (start, end) = dt_range
        start = start.astimezone(pytz.utc)
        end = end.astimezone(pytz.utc)

        # Intervals may be wider than 35 days
        query_periods = QueryPeriods.__get_query_periods__help(mentor, (start, end))

        # Intervals no wider than 35 days.
        query_periods_new = []
        for query_period in query_periods:
            query_periods_new += QueryPeriods.__process_query_period(query_period)

        return query_periods_new

    @staticmethod
    def get_sets_of_query_periods(mentor, dt_range):
        """Given a cronofy-synced mentor, return sets of `query_periods` taking into account Mentor's blackout dates.

        dt_range is a tuple (a, b) where `a` and `b` are datetime.datetime objects
        representing the start/end times of desired availability range.

        `a` and `b` should be dt naive objects that we interpret as timezone UTC.

        A "set" of query_periods is a list that can be used as "query_periods" parameter for Cronofy API call.

        Multiple "sets" may be returned if the range is so wide (wider than 35 days) that we need to split our
        request into multiple API calls; each set being appropriate for 1 API call.

        return format: list of list of query_periods

        This is a helper function for read_availability
        """
        query_periods = QueryPeriods.__get_query_periods(mentor, dt_range)

        # Groups of query sets (each group spanning less than 35 days).
        sets_of_query_periods = QueryPeriods.__group_query_periods(query_periods)

        # Convert to ISO 8601
        for set_of_qp in sets_of_query_periods:
            QueryPeriods.__query_periods_to_iso8601(set_of_qp)

        return sets_of_query_periods

    @staticmethod
    def get_sets_of_query_periods__non_mentor(dt_range):
        """
        Return sets of `query_periods` (no blackout dates considered).

        dt_range is a tuple (a, b) where `a` and `b` are datetime.datetime objects
        representing the start/end times of desired availability range.

        `a` and `b` should be dt naive objects that we interpret as timezone UTC.

        A "set" of query_periods is a list that can be used as "query_periods" parameter for Cronofy API call.

        Multiple "sets" may be returned if the range is so wide (wider than 35 days) that we need to split our
        request into multiple API calls; each set being appropriate for 1 API call.

        return format: list of list of query_periods

        This is a helper function for read_availability
        """
        (start, end) = dt_range
        start = start.astimezone(pytz.utc)
        end = end.astimezone(pytz.utc)

        query_period = {
            'start': start,
            'end': end,
        }

        # Groups of query sets (each group spanning less than 35 days).
        sets_of_query_periods = QueryPeriods.__group_query_periods([query_period])

        # Convert to ISO 8601
        for set_of_qp in sets_of_query_periods:
            QueryPeriods.__query_periods_to_iso8601(set_of_qp)

        return sets_of_query_periods

    @staticmethod
    def get_next_monday(date):
        """Given a date object, return the next Monday.
        Sunday counts as the same week.

        (In otherwords, if uni_style.mentor_schedule_start == UniversityStyle.NEXT_WEEK
        then a mentee could schedule a monday meeting two days ahead of time on Saturday.)
        """

        """
        0 => Mon => +7
        1 => Tue => +6
        2 => Wed => +5
        3 => Thu => +4
        4 => Fri => +3
        5 => Sat => +2
        6 => Sun => +1 + 7 = +8

        Add 7 - date.weekday()
        If Sunday, add an extra week."""
        weekday = date.weekday()
        if weekday == 6:
            days_to_next_monday = 8
        else:
            days_to_next_monday = 7 - weekday

        return date + timedelta(days=days_to_next_monday)

    @staticmethod
    def get_query_periods_for_availability_viewer(mentor):
        """
        Given a cronofy-linked mentor, construct a list of query_periods suitable for
        input to the Cronofy Availability viewer. (Cronofy allows Query for availability viewer
        to have query_periods that, in aggregate, span wider than 35 days.)
        https://docs.cronofy.com/developers/api/scheduling/availability/#param-query_periods

        Query Periods take into account mentor's blackout dates, and whether
        platform setting has mentor scheduling starting this week or next week.

        For now, we specify that we are only looking up to 84 days into future.
        """
        utcnow = datetime.utcnow()
        show_next_week = mentor.platform.style.mentor_schedule_start == UniversityStyle.NEXT_WEEK

        if show_next_week:
            mentor_timezone = pytz.timezone(mentor.timezone)
            now_local_date = utcnow.astimezone(pytz.timezone(mentor.timezone)).date()
            start_local_date = QueryPeriods.get_next_monday(now_local_date)
            start_local_dt = mentor_timezone.localize(datetime.combine(start_local_date, datetime.min.time()))
            start = start_local_dt.astimezone(pytz.utc)
        else:
            start = utcnow

        end = start + timedelta(days=CRONOFY_CACHE_DAYS_FUTURE)

        query_periods = QueryPeriods.__get_query_periods(mentor, (start, end))

        QueryPeriods.__query_periods_to_iso8601(query_periods)

        return query_periods


def read_availability(user, university, is_mentor=None, range=None):  # TODO handle non-mentor TODO handle non-mentor
    """ range is a tuple (a, b) where `a` and `b` are datetime.datetime objects
    representing the start/end times of desired availability range.

    `a` and `b` should be dt naive objects that we interpret as timezone UTC.

    If range is None, range is between now and 30 days later.

    This method makes an API call and raises a ValueError on failure
    (and thus calls to this method should be wrapped in a try-except that catches ValueError).
    """
    if is_mentor is None:
        app_logger.error('is_mentor argument must be provided')

    if range is None:
        utc_now = dtime.datetime.utcnow()
        range = (utc_now, utc_now + dtime.timedelta(30))

    if is_mentor:
        mentor = Mentor.objects.get(profile__user=user, platform=university)
        sets_of_query_periods = QueryPeriods.get_sets_of_query_periods(mentor, range)
        duration_minutes = mentor.default_duration
    else:
        sets_of_query_periods = QueryPeriods.get_sets_of_query_periods__non_mentor(range)
        duration_minutes = 30

    ACCESS_AUTH = {
        'Authorization': 'Bearer {0}'.format(establish_cronofy_for_user(user).auth.access_token),
        'Content-Type': 'application/json; charset=utf-8'
    }

    cronofy_account_info = get_cronofy_account_info(user)['account_id']
    cronofy_member = {
        "sub": cronofy_account_info,
    }

    if is_mentor:
        cronofy_member.update({
            "managed_availability": True,
            "availability_rule_ids": [str(user.id)]
        })

    def get_data_params(query_periods):
        data = {
            "participants": [
                {
                    "members": [cronofy_member],
                    "required": "all"
                }
            ],
            "required_duration": {"minutes": duration_minutes},
            "query_periods": query_periods,
            "response_format": "slots",
            "max_results": 512  # Max allowed by Cronofy. Worst case: 30-min slots cap at 512/2/24 = 10.6 days in future
        }

        return json.dumps(data)

    aggregated_slots = []
    for query_periods in sets_of_query_periods:
        data = get_data_params(query_periods)

        r = requests.post(
            url='https://api.cronofy.com/v1/availability', headers=ACCESS_AUTH, data=data)

        if r.status_code < 300:
            outputted_data = r.json()
            slots = outputted_data['available_slots']
            aggregated_slots += slots
        else:
            msg = f'https://api.cronofy.com/v1/availability returned with status code {r.status_code} for user {user}. ' \
                  f'Errors = {json.loads(r.content)["errors"]}'
            app_logger.error(msg)
            return []

    return aggregated_slots


def get_cronofy_slot_times(request, user, is_mentor=None):
    """ Returns availability of user in next 30 days in the form of a array
    of tuples of datetime objects:

        [(start, end),
         (start, end),
         ...]

     An entry of the array is a tuple of
     the start and end time of that availability slot.
    """
    curr_timezone = request.session['django_timezone']
    tz = pytz.timezone(curr_timezone)

    slots = read_availability(user, request.university, is_mentor=is_mentor)
    arr = [(dtime.datetime.strptime(slot['start'], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=pytz.utc).astimezone(tz),
            dtime.datetime.strptime(slot['end'], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=pytz.utc).astimezone(tz)) for slot
           in slots]

    return arr


def init_day_entry(date, user, university, is_mentor=None):
    """
    Initializes an entry for the `days` array in slot_time_translator.

    See slot_time_translator for specs.

    (requires university parameter (request.university) only because there may be multiple mentors asociated with user)
    """

    if is_mentor:
        mentor = Mentor.objects.get(profile__user=user, platform=university)
        meeting_type, location = get_type_location(mentor, date)
    else:
        meeting_type, location = None, None

    return [
        [int(dtime.datetime.strftime(date, '%w')), dtime.datetime.strftime(date, '%a')],
        dtime.datetime.strftime(date, '%b %d'),
        [],
        int(dtime.datetime.strftime(date, '%Y')),
        meeting_type,
        location
    ]


def slot_time_translator(request, user, is_mentor=None):
    """
    NOTE: This applies to getting the availability of mentor or mentee that has cronofy sync.

    TODO FIXME: Don't return availability for this week if platform "Mentor Scheduls Start"
    setting is "Next Week".

    Returns `days`, an array with an entry corresponding to each date that
    the mentor is available (for next 30 days).

    An entry of `days` is a list of data about the date:
        [
            [6, 'Sat'],
            'Nov 30',
            [...],
            2019,
            4,
            'Gates 310'
        ]

        Where the element at index 0 is a 2 item list corresponding to the weekday:
            + Weekday as a decimal number, where 0 is Sunday and 6 is Saturday.
            + Weekday as locale’s abbreviated name.
        The element at index 2 is a list of available slots (see spec below).
        The element at index 4 is the meeting type, a choice of mentorship.models.PREFERRED_CONTACT.
        The element at index 5 is location.

    An entry in the slot list in `day` is:
        [
            '03:00 PM',
            '03:30 PM',
            True,
            2,
            'Gates 310'
        ]

        Where the first two items in the entry are the start and end times of the slot.
        The item at index 2 is a boolean representing whether the slot is available (or taken by a mentee).
        This boolean is always True.
        The item at index 3 (eg, the number 2) is the duration of the meeting (codes are mentorship.models.DURATIONS).
        The item at index 4 is the location.

    A note about location:
    By convention, location is relevant only if meeting type is in person.
    Otherwise, location may be none, or the non-in-person meeting type ("eg phone or video call").
    """
    slots = get_cronofy_slot_times(request, user, is_mentor=is_mentor)
    arr = []
    for slot in slots:
        """ By the end of execution of this for loop,
        arr is a list of unique dates that the mentor is available.
        """
        slot_date = slot[0].date()
        if [slot_date] not in arr:
            arr.append([slot_date])

    days = [init_day_entry(date[0], user, request.university, is_mentor=is_mentor) for date in arr]

    # times = [[dtime.datetime.strftime(slot[0], '%H:%M %p'), dtime.datetime.strftime(slot[1], '%H:%M %p'), True, 2, None]for slot in slots]
    for day in days:
        """ Add slots to the slot list in `day`, but only the slots whose date correspond to
        the date of `day`.
        """
        tmp_date = day[1]

        for slot in slots:
            slot_date = dtime.datetime.strftime(slot[0], '%b %d')

            if slot_date == tmp_date:
                start_str = dtime.datetime.strftime(slot[0], '%I:%M%p')
                end_str = dtime.datetime.strftime(slot[1], '%I:%M%p')
                duration_code = get_choice_code_duration(slot[0], slot[1])
                day[2].append([start_str, end_str, True, duration_code, day[5]])
    return days


def create_or_update_conference_event(event, invite=None, remove=None):
    '''
    Create or update existing cronofy event
    Event is created on the middle-man email specified on this platform
    -> event: is an event object
    -> invite: is a list of useremail objects
    '''
    if IS_DEV and not IS_STAGING:
        user = UserEmail.objects.get(id=MIDDLE_MAN_EMAILUSER_ID_DEV)
        callback_url = "https://cb64287557c9c01f02b7d3b64f9ad348.m.pipedream.net"
    else:
        user = UserEmail.objects.get(email=AUTO_CONFERENCE_DEFAULT_EMAIL)
        current_site = event.university.site
        protocol = PROTOCOL
        callback_url = "{0}://{1}/webhook/events".format(
            protocol,
            current_site.domain
        )

    cronofy_calendar_id = get_cronofy_calendar_id(user)

    conferencing = {"profile_id": "integrated"}
    subscriptions = [
            {
              "type": "webhook",
              "uri": callback_url,
              "interactions": [
                {
                  "type": "conferencing_assigned"
                },
                {
                  "type": "conferencing_failing"
                }
              ]
            }
      ]
    if(event.input_own_link and event.auto_generate_link):
        input_own_link = formaturl(event.input_own_link)
        input_own_link = input_own_link.strip()
        conferencing = {
                    "profile_id": "explicit" ,
                    "provider_description": "StartupTree Custom Conferencing",
                    "join_url": input_own_link
                  }
        subscriptions = []
    if (event.auto_generate_link):
        cronofy_event = {
            'event_id': event.id,
            'summary': event.name,
            'description': event.name,
            'tzid': event.timezone,
            'start': event.start,
            'end': event.end,
            "conferencing": conferencing,
            "subscriptions": subscriptions
        }
    else:
        cronofy_event = {
            'event_id': event.id,
            'summary': event.name,
            'description': event.name,
            'tzid': event.timezone,
            'start': event.start,
            'end': event.end,
            "conferencing": {"profile_id": "none"}
            }
    if (invite is not None) or (remove is not None):
        cronofy_event["attendees"] = {}
    if invite is not None:
        cronofy_event["attendees"]["invite"] = [{"email": invite}]
    if remove is not None:
        cronofy_event["attendees"]["remove"] = [{"email": remove}]

    try:
        result = cronofy_publish_to_calendar(user, cronofy_calendar_id, cronofy_event, refresh_cronofy_cache=False)
    except:
        raise ValueError("Unable to create calendar event")

def formaturl(url):
    if not re.match('(?:http|ftp|https)://', url):
        return 'http://{}'.format(url)
    return url

def delete_conference_event(event):
    if IS_DEV and not IS_STAGING:
        user = UserEmail.objects.get(id=MIDDLE_MAN_EMAILUSER_ID_DEV)
    else:
        user = UserEmail.objects.get(email=AUTO_CONFERENCE_DEFAULT_EMAIL)

    cronofy_calendar_id = get_cronofy_calendar_id(user)
    cronofy_event = {
        'event_id': event.id,
        'summary': event.name,
        'description': event.name,
        'start': event.start,
        'end': event.end,
        'tzid': event.timezone,
        'location': event.location,
        "conferencing": {"profile_id": "default"}
    }

    try:
        cronofy_delete_from_calendar(user, cronofy_calendar_id, event.id, refresh_cronofy_cache=False)
    except:
        raise ValueError("Unable to delete calendar event")
