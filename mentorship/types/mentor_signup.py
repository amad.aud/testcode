from typing import (
    Any,
    List,
    Optional,
    TypedDict,
)


class MentorContactInfoFields(TypedDict):
    """ WARNING: Must match exactly to counter part in
    /scripts/javascript/react/components/mentorship/user/signup/types.ts"""
    email: str
    phone: str
    video: str
    timezone: str


class MentorContactInfo(TypedDict):
    """ WARNING: Must match exactly to counter part in
    /scripts/javascript/react/components/mentorship/user/signup/types.ts"""
    mentorContactInfoFields: MentorContactInfoFields
    allTimezones: List[str]


class CronofySyncInfo(TypedDict):
    """ WARNING: Must match exactly to counter part in
    /scripts/javascript/react/components/mentorship/user/signup/types.ts"""
    clientId: str
    elementToken: Optional[str]


class MentorSignupStep:
    """ String Constants.
    WARNING, must  match
    enum Step from /scripts/javascript/react/components/mentorship/user/signup/types.ts
    """
    CONTACT_INFO = "ContactInfo"
    CALENDAR_SYNC = "CalendarSync"

    STEPS = [
        CONTACT_INFO,
        CALENDAR_SYNC,
    ]


class POSTResponse(TypedDict):
    errors: List[str]
    responseData: Any


class TextFieldSizeLimits(TypedDict):
    """ WARNING: Must match exactly to counter part in
    /scripts/javascript/react/components/mentorship/user/signup/types.ts"""
    email: int
    phone: int
    video: int
