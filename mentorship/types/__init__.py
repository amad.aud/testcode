from mentorship.models import Mentor
from typing import Optional, TypedDict


class MentorInfo(TypedDict):
    curr_mentor: Optional[Mentor]
    is_mentor: bool
    has_office_hours: bool
    needs_approval: bool
