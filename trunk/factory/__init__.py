import factory
from trunk import models
from trunk.models import AccessLevel, PointOfContact
from emailuser.factory import UserEmailFactory
from StartupTree.settings import ROOT_DOMAIN
from discussion.models import Group, Channel

CORNELL_SCHOOLS = ["A&S", "AAP", "CALS",
                   "Business", "Engineering", "HumEc", "ILR"]
CORNELL_FULL = "Cornell University"
CORNELL_SHORT = "cornell"


class SiteFactory(factory.django.DjangoModelFactory):
    '''Create site'''
    class Meta:
        model = models.Site
        django_get_or_create = ('domain', 'name')
    domain = ROOT_DOMAIN
    name = 'startuptreetest'


class UniversityFactory(factory.django.DjangoModelFactory):
    '''Generates a University Object (default to cornell platform)
       To change any parameters, simply pass them into the class call, and they will
       automatically override the below'''
    class Meta:
        model = models.University
        django_get_or_create = ('short_name', 'site', 'name')
        exclude = ['tmp_name', 'tmp_domain', 'access_levels']
    name = CORNELL_FULL
    short_name = CORNELL_SHORT
    program_name = CORNELL_FULL
    tmp_name = factory.LazyAttribute(lambda uni: uni.name)
    tmp_domain = factory.LazyAttribute(lambda uni: "{0}.{1}".format(uni.short_name, ROOT_DOMAIN))
    site = factory.SubFactory(SiteFactory, name=factory.SelfAttribute('..tmp_name'),
                              domain=factory.SelfAttribute('..tmp_domain'))

    @classmethod
    def _get_or_create(cls, model_class, *args, **kwargs):
        uni = super(UniversityFactory, cls)._get_or_create(model_class, *args, **kwargs)
        dashboard, _ = AccessLevel.objects.get_or_create(name=AccessLevel.DASHBOARD, university=uni)
        users, _ = AccessLevel.objects.get_or_create(name=AccessLevel.USERS, university=uni)
        ventures, _ = AccessLevel.objects.get_or_create(name=AccessLevel.VENTURES, university=uni)
        projects, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PROJECTS, university=uni)
        groups, _ = AccessLevel.objects.get_or_create(name=AccessLevel.GROUPS, university=uni)
        tools, _ = AccessLevel.objects.get_or_create(name=AccessLevel.TOOLS, university=uni)
        tags, _ = AccessLevel.objects.get_or_create(name=AccessLevel.TAGS, university=uni)
        export, _ = AccessLevel.objects.get_or_create(name=AccessLevel.EXPORT, university=uni)
        custom_data, _ = AccessLevel.objects.get_or_create(name=AccessLevel.CUSTOM_DATA, university=uni)
        reporting, _ = AccessLevel.objects.get_or_create(name=AccessLevel.REPORTING, university=uni)
        events, _ = AccessLevel.objects.get_or_create(name=AccessLevel.EVENTS, university=uni)
        jobs, _ = AccessLevel.objects.get_or_create(name=AccessLevel.JOBS, university=uni)
        competitions, _ = AccessLevel.objects.get_or_create(name=AccessLevel.COMPETITIONS, university=uni)
        applications, _ = AccessLevel.objects.get_or_create(name=AccessLevel.APPLICATIONS, university=uni)
        surveys, _ = AccessLevel.objects.get_or_create(name=AccessLevel.SURVEYS, university=uni)
        mentorship, _ = AccessLevel.objects.get_or_create(name=AccessLevel.MENTORSHIP, university=uni)
        team_mentorship, _ = AccessLevel.objects.get_or_create(name=AccessLevel.TEAM_MENTORSHIP, university=uni)
        invite, _ = AccessLevel.objects.get_or_create(name=AccessLevel.INVITE, university=uni)
        announce, _ = AccessLevel.objects.get_or_create(name=AccessLevel.ANNOUNCE, university=uni)
        support, _ = AccessLevel.objects.get_or_create(name=AccessLevel.SUPPORT, university=uni)
        admins, _ = AccessLevel.objects.get_or_create(name=AccessLevel.ADMINS, university=uni)
        roadmap, _ = AccessLevel.objects.get_or_create(name=AccessLevel.ROADMAP, university=uni)
        program, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PROGRAM, university=uni)
        design, _ = AccessLevel.objects.get_or_create(name=AccessLevel.DESIGN, university=uni)
        platform, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PLATFORM, university=uni)
        pages, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PAGES, university=uni)
        custom_text, _ = AccessLevel.objects.get_or_create(name=AccessLevel.CUSTOM_TEXT, university=uni)
        custom_roles, _ = AccessLevel.objects.get_or_create(name=AccessLevel.CUSTOM_ROLES, university=uni)
        user_signup, _ = AccessLevel.objects.get_or_create(name=AccessLevel.USER_SIGNUP, university=uni)
        private_discussion, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PRIVATE_DISCUSSION,
                                                                  university=uni)

        # create forum community group
        community_group, _ = Group.objects.get_or_create(name=Group.PLATFORM_COMMUNITY, community_platform=uni)
        community_group.url_name = "{0}-community".format(uni.short_name)
        community_group.description = "Receive announcements and opportunities being shared in your community, and submit your own."
        community_group.save()

        for channel_name in Channel.COMMON_CHANNELS:
            new_channel, _ = Channel.objects.get_or_create(name=channel_name, group=community_group)
            new_channel.url_name = new_channel.name.lower().replace(" ", "-")
            new_channel.save()

        return uni


class UniversityStyleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.UniversityStyle
        django_get_or_create = ('_university',)
    _university = factory.SubFactory(UniversityFactory)
    enable_global_jobs = 1
    require_startup_approval = False


class UniversityStaffFactory(factory.django.DjangoModelFactory):
    '''Generates a university staff'''
    class Meta:
        model = models.UniversityStaff

    admin = factory.SubFactory(UserEmailFactory, email=factory.Sequence(
        lambda n: 'admin{}@startuptree.co'.format(n)))
    university = factory.SubFactory(UniversityFactory)
    is_super = True


class UniversitySubscriptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.UniversitySubscription
        django_get_or_create = ('university',)
    university = factory.SubFactory(UniversityFactory)


class UniversityEmailDomainFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.UniversityEmailDomain
    university = factory.SubFactory(UniversityFactory)
    domain = 'example.com'


class SchoolFactory(factory.django.DjangoModelFactory):
    class Meta:
        django_get_or_create = ('name',)
        model = models.School
    university = factory.SubFactory(UniversityFactory)
    name = 'ENG'
    displayed = 'Engineering'


class AccessLevelFactory(factory.django.DjangoModelFactory):
    class Meta:
        django_get_or_create = ('name', 'university')
        model = models.AccessLevel


class StartupTreeStaffFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.StartupTreeStaff
    staff = factory.SubFactory(UserEmailFactory, email=factory.Sequence(
        lambda n: 'staff{}@startuptree.co'.format(n)))


class UniversityGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.UniversityGroup
    name = factory.faker.Faker('sentence')
