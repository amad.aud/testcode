import factory
from trunk import models
from trunk.factory import UniversityFactory
from branch.factory import StartupMemberFactory
from django.utils import timezone


class EventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Event

    name = factory.Faker('name')
    university = factory.SubFactory(UniversityFactory)
    start = factory.LazyFunction(timezone.now)
    end = factory.LazyFunction(timezone.now)


class CompetitionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Event

    name = factory.Faker('name')
    university = factory.SubFactory(UniversityFactory)
    start = factory.LazyFunction(timezone.now)
    end = factory.LazyFunction(timezone.now)
    is_competition = True
    form = factory.SubFactory('forms.factory.FormFactory')


class JudgeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Judge

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')
    event = factory.SubFactory(EventFactory)

    @factory.post_generation
    def add_to_board(self, create, extracted, **kwargs):
        if create:
            board = self.event.judge_boards.all()[0]
            round = board.round
            self.round = round
            self.save()
            board.judges.add(self)


class JudgeBoardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.JudgeBoard

    passcode = 'passcode'
    competition = factory.SubFactory(EventFactory)

class EventRSVPFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.EventRSVP

    member = factory.SubFactory(StartupMemberFactory)
    rsvp_event = factory.SubFactory(EventFactory)

class GuestRSVPFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.GuestRSVP

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')
