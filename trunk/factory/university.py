"""
Generate Base objects used for testing.
Basically school related objects for base trunk models
Call start_factory() in setup() when using for tests
"""
from django.contrib.sites.models import Site
from StartupTree.settings import ROOT_DOMAIN
from trunk.models import University, School, Major, StartupTreeStaff, UniversitySubscription, UniversityStyle, UniversityStaff
from emailuser.factory import UserEmailFactory
from emailuser.conf import STAFF
from branch.models import StartupMember
from trunk.factory import SiteFactory


# might need to modify this later
def generate_startuptree_staff(email, university=None):
    """Create staff account/email, this is staff account, should not have profile associated with it"""
    if university is not None and not isinstance(university, University):
        raise ValueError("Must be given university object.")
    useremail = UserEmailFactory()
    staff, _ = StartupTreeStaff.objects.get_or_create(staff=useremail)
    if university:
        staff.universities.add(university)
    return staff


def generate_university_admin(sm, university):
    """Create staff account/email, this is staff account, should not have profile associated with it"""
    if university is not None and not isinstance(university, University):
        raise ValueError("Must be given university object.")
    if not isinstance(sm, StartupMember):
        raise ValueError("")
    # university.uni_staffs.add(sm.user)
    us = UniversityStaff(university=university, admin=sm.user, is_super=True)
    us.save()
    sm.user.add_group(STAFF)  # from emailuser.conf import STAFF
    sm.universities.add(university)  # just in case
    return sm

# random majors at cornell
MAJORS = ["Computer Science", "Policy Analysis and Management", "Engineering", "Finance", "History",
          "Hotel (Napkin Folding)", "Government", "Philosiphy", "Information Science", "Statistics",
          "Fashion"]


def generate_majors():
    new_obj = []
    for major in MAJORS:
        if not Major.objects.filter(name=major.lower()).exists():
            new_obj.append(Major(displayed=major, name=major.lower()))
    Major.objects.bulk_create(new_obj)


CORNELL_FULL = "Cornell University"
CORNELL_SHORT = "cornell"
CORNELL_SCHOOLS = ["A&S", "AAP", "CALS", "Business", "Engineering", "HumEc", "ILR"]


def generate_cornell_university(uni_exists=False):
    """
    Create cornell university for testing
    :return:
    """
    uni = University.objects.create_university(CORNELL_FULL, CORNELL_SHORT, uni_exists=uni_exists)
    UniversitySubscription.objects.create(university=uni)
    for school in CORNELL_SCHOOLS:
        School.objects.create_school(school, uni)
    UniversityStyle.objects.update_style(uni)
    return uni


BOSTON_FULL = "Boston University"
BOSTON_SHORT = "bu"
# pretty sure these are the 10 schools
BOSTON_SCHOOLS = ["CAS", "COM", "ENG", "CFA", "CGS", "SAR", "Global Studies", "Business", "SED", "SHA"]


def generate_boston_university():
    """
    Generate boston university. Necessary to test
    A. global
    B. BU relevant customizations
    :return:
    """
    uni = University.objects.create_university(BOSTON_FULL, BOSTON_SHORT)
    UniversitySubscription.objects.create(university=uni)
    for school in BOSTON_SCHOOLS:
        School.objects.create_school(school, uni)
    UniversityStyle.objects.update_style(uni)
    return uni


def init_localhost():
    # clear site cache from previous test run saved on redis.
    Site.objects.clear_cache()
    site = SiteFactory()
    # print(site.id)
    # try:
    #     site = Site.objects.get(id=1)
    # except Site.DoesNotExist:
    #     site = Site()
    # site.domain = ROOT_DOMAIN
    # print ("!!! {0}".format(ROOT_DOMAIN))
    # site.name = 'startuptreetest'
