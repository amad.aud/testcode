from django.db import models
from django.contrib.sites.models import Site
from StartupTree.settings import ROOT_DOMAIN, DEBUG
from StartupTree.utils import better_title, make_plural
from django.urls import reverse
from django.contrib.postgres.fields import ArrayField

from app.user_types import DASHBOARD, USERS, VENTURES, PROJECTS, GROUPS, TOOLS, TAGS, EXPORT, CUSTOM_DATA, REPORTING, \
    EVENTS, JOBS, COMPETITIONS, APPLICATIONS, SURVEYS, MENTORSHIP, TEAM_MENTORSHIP, INVITE, ANNOUNCE, SUPPORT, ADMINS, \
    ROADMAP, PROGRAM, DESIGN, PLATFORM, PAGES, CUSTOM_TEXT, CUSTOM_ROLES, USER_SIGNUP, PRIVATE_DISCUSSION, ALL
from emailuser.models import UserEmail
from trunk.utils import (
    get_uni_logo_url,
    get_uni_css_url,
    get_event_image_url,
    get_team_landing_image_url,
    get_rmblock_image_url,
    get_rmblock_file_url,
    get_mentor_banner_url
)
from StartupTree.loggers import app_logger
from StartupTree.utils import get_media_url, bleach_clean, filter_ascii, replace_mentorship_word
import uuid
from django.utils import timezone
import random
import string
import pytz
import shortuuid
from urllib.parse import quote as url_quote
from datetime import datetime, date


class UniversityStyleManager(models.Manager):
    def update_style(self, university, args={}):
        if self.filter(_university=university).exists():
            style = self.get(_university=university)
        else:
            style = self.create(_university=university)
        logo = args.get('logo')
        if logo:
            style.logo = logo
            style.save()
        return style

    def create_with_about(self, university, markdown_about):
        about = markdown_about
        style = self.create(_university=university,
                            about=about,
                            markdown_about=markdown_about)
        return style


class UniversityStyle(models.Model):
    _university = models.ForeignKey('University', on_delete=models.CASCADE)
    default_uni_name = models.CharField(max_length=100, blank=True,
                                        null=True, default=None)

    ########################
    # DESIGN CUSTOMIZATION #
    ########################
    logo = models.ImageField(upload_to=get_uni_logo_url,
                             null=True,
                             blank=True,
                             default=None,
                             max_length=700)
    ori_logo = models.ImageField(upload_to=get_uni_logo_url,
                                 null=True,
                                 blank=True,
                                 default=None,
                                 max_length=700)
    welcome_email = models.TextField(null=True, default=None)
    markdown_about = models.TextField(default='')
    homepage_blurb = models.CharField(max_length=1000, default='')
    about = models.TextField(default='')
    first_color = models.CharField(max_length=7, null=True, default=None)
    second_color = models.CharField(max_length=7, null=True, default=None)
    mentor_banner = models.ImageField(upload_to=get_mentor_banner_url,
                                      null=True,
                                      blank=True,
                                      default=None,
                                      max_length=700)
    eship_website = models.URLField(null=True, default=None, blank=True)
    uni_website = models.URLField(null=True, default=None, blank=True)
    uni_blog = models.URLField(null=True, default=None, blank=True)

    main_css = models.FileField(upload_to=get_uni_css_url,
                                null=True,
                                blank=True,
                                default=None,
                                max_length=700)

    panel_css = models.FileField(upload_to=get_uni_css_url,
                                 null=True,
                                 blank=True,
                                 default=None,
                                 max_length=700)

    # DEPRECATED: TOBE DELETED
    primary_email_url = models.URLField(blank=True, null=True, default=None)
    secondary_email_url = models.URLField(blank=True, null=True, default=None)
    third_email_url = models.URLField(blank=True, null=True, default=None)

    facebook = models.URLField(null=True, blank=True, default=None)
    twitter = models.URLField(null=True, blank=True, default=None)
    instagram = models.URLField(null=True, blank=True, default=None)
    slack = models.URLField(null=True, blank=True, default=None)

    TIMEZONES = [(t, t) for t in pytz.common_timezones]
    timezone = models.CharField(
        max_length=100,
        choices=TIMEZONES,
        default='US/Eastern')

    # custom mentorship email templates
    session_scheduled_mentee_email = models.TextField(null=True, default=None)
    session_scheduled_mentor_email = models.TextField(null=True, default=None)
    session_reminder_mentee_email = models.TextField(null=True, default=None)
    session_reminder_mentor_email = models.TextField(null=True, default=None)

    ##########################
    # PLATFORM CUSTOMIZATION #
    ##########################
    EVERYTHING = 1
    COMPANY_ONLY = 2
    FEED_CHOICES = (
        (EVERYTHING, 'All Activities'),
        (COMPANY_ONLY, 'Company Related Activities'),
    )

    feed = models.PositiveSmallIntegerField(choices=FEED_CHOICES,
                                            default=EVERYTHING)

    INCLUDE = 1
    EXCLUDE = 0
    SELF_GLOBAL_CHOICES = (
        (EXCLUDE, 'Exclude Current Platform'),
        (INCLUDE, 'Include Current Platform'),
    )

    include_self_global = models.PositiveSmallIntegerField(choices=SELF_GLOBAL_CHOICES,
                                                           default=INCLUDE)

    GLOBAL_ON = 1
    GLOBAL_OFF = 0
    GLOBAL_OPTION = (
        (GLOBAL_OFF, 'Off'),
        (GLOBAL_ON, 'On')
    )

    # Refers specifically to enable_global_feed
    enable_global = models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                                     default=GLOBAL_ON)

    enable_global_discussion = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    enable_global_people = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    enable_global_jobs = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    enable_global_events = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)
    # Refers to projects and ventures
    enable_global_startups = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    # Group platform specific
    enable_group_global_activity = models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                                     default=GLOBAL_ON)

    enable_group_global_discussion = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    enable_group_global_people = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    enable_group_global_jobs = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    enable_group_global_events = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)
    # Refers to projects and ventures
    enable_group_global_startups = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)


    GLOBAL = 1
    UNIVERSITY = 2
    GLOBAL_ONLY = 3
    GROUP_GLOBAL = 4
    VIEW_CHOICES = (
        (GLOBAL, 'Global'),
        (UNIVERSITY, 'Network'),
        (GLOBAL_ONLY, 'Display Only Global View'),
        (GROUP_GLOBAL, 'Display Group Global')
    )

    default_feed_view = \
        models.PositiveSmallIntegerField(choices=VIEW_CHOICES,
                                         default=GLOBAL)

    default_people_view = \
        models.PositiveSmallIntegerField(choices=VIEW_CHOICES,
                                         default=GLOBAL)

    default_events_view = \
        models.PositiveSmallIntegerField(choices=VIEW_CHOICES,
                                         default=GLOBAL)

    default_jobs_view = \
        models.PositiveSmallIntegerField(choices=VIEW_CHOICES,
                                         default=GLOBAL)

    default_discussion_view = \
        models.PositiveSmallIntegerField(choices=VIEW_CHOICES,
                                         default=GLOBAL)

    default_startups_view = \
        models.PositiveSmallIntegerField(choices=VIEW_CHOICES,
                                         default=GLOBAL)

    POPULAR = 1
    NEWEST = 2
    SORT_CHOICES = (
        (POPULAR, 'Popular'),
        (NEWEST, 'Newest')
    )

    default_forum_sort_view = \
        models.PositiveSmallIntegerField(choices=SORT_CHOICES,
                                         default=POPULAR)

    DISPLAY_OFF = 0
    DISPLAY_ON = 1
    DISPLAY_PRIVATE = 2
    DISPLAY_OPTION = (
        (DISPLAY_ON, 'Show'),
        (DISPLAY_OFF, 'Hide')
    )
    DISPLAY_WITH_PRIVATE_OPTION = (
        (DISPLAY_ON, 'Show'),
        (DISPLAY_OFF, 'Hide'),
        (DISPLAY_PRIVATE, 'Private')
    )
    DISPLAY_GUEST_OPTION = (
        (DISPLAY_ON, 'Show Guest Data'),
        (DISPLAY_OFF, "Don't Show Guest Data")
    )

    DISPLAY_TEAM_MENTORS_DISCOVERY_AND_LANDING = 0
    DISPLAY_TEAM_MENTORS_DISCOVERY = 1
    DISPLAY_TEAM_MENTORS_LANDING = 2
    DISPLAY_TEAM_MENTORS_OPTION = (
        (DISPLAY_TEAM_MENTORS_DISCOVERY_AND_LANDING, 'Discovery and Landing Page'),
        (DISPLAY_TEAM_MENTORS_DISCOVERY, 'Discovery View'),
        (DISPLAY_TEAM_MENTORS_LANDING, 'Landing Page View')
    )

    DEFAULT_TEAM_MENTORS_VIEW_DISCOVERY = 0
    DEFAULT_TEAM_MENTORS_VIEW_LANDING = 1
    DEFAULT_TEAM_MENTORS_VIEW_OPTION = (
        (DEFAULT_TEAM_MENTORS_VIEW_DISCOVERY, 'Discovery View'),
        (DEFAULT_TEAM_MENTORS_VIEW_LANDING, 'Landing Page View')
    )

    enable_user_dashboard = \
        models.PositiveSmallIntegerField(choices=DISPLAY_OPTION,
                                         default=DISPLAY_ON)

    display_ventures = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_projects = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_mentors = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON,
                                         blank=True)

    display_investors = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_roadmap = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_forum = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_deals = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_jobs = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_events = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_applications = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_competitions = \
        models.PositiveSmallIntegerField(choices=DISPLAY_WITH_PRIVATE_OPTION,
                                         default=DISPLAY_ON)

    display_mentor_btn = \
        models.PositiveSmallIntegerField(choices=DISPLAY_OPTION,
                                         default=DISPLAY_ON)

    display_team_mentors = \
        models.PositiveSmallIntegerField(choices=DISPLAY_TEAM_MENTORS_OPTION,
                                         default=DISPLAY_TEAM_MENTORS_DISCOVERY,
                                         blank=True)

    default_team_mentors_view = \
        models.PositiveSmallIntegerField(choices=DEFAULT_TEAM_MENTORS_VIEW_OPTION,
                                         default=DEFAULT_TEAM_MENTORS_VIEW_DISCOVERY,
                                         blank=True)

    include_mentorship_connections = \
        models.PositiveSmallIntegerField(choices=DISPLAY_OPTION,
                                         default=DISPLAY_ON)

    show_guest_data = \
        models.PositiveSmallIntegerField(choices=DISPLAY_GUEST_OPTION,
                                         default=DISPLAY_OFF)

    display_pages_fields = \
        models.PositiveSmallIntegerField(choices=DISPLAY_OPTION,
                                         default=DISPLAY_OFF)

    LIST_VIEW = 1
    CALENDAR_VIEW = 2
    EVENT_VIEW_OPTION = (
        (LIST_VIEW, "List View"),
        (CALENDAR_VIEW, "Calendar View")
    )

    event_discovery_view = \
        models.PositiveSmallIntegerField(choices=EVENT_VIEW_OPTION,
                                         default=LIST_VIEW)

    display_calendar_users = \
        models.PositiveSmallIntegerField(choices=DISPLAY_OPTION,
                                         default=DISPLAY_ON)

    display_calendar_admin = \
        models.PositiveSmallIntegerField(choices=DISPLAY_OPTION,
                                         default=DISPLAY_OFF)

    require_startup_approval = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    allow_admin_jobs = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    enable_private_discussion = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    enable_event_reminders = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    send_platform_wide_deadline_reminders = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    send_rsvp_reminders = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    send_new_user_notis = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    mentor_request_noti = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    meeting_request_noti = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    custom_questions_noti = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_OFF)

    community_forum_noti = \
        models.PositiveSmallIntegerField(choices=GLOBAL_OPTION,
                                         default=GLOBAL_ON)

    NO_APPROVAL = 0
    OFFICE_HOURS_ONLY = 1
    ALL_MEETINGS = 2
    MEETING_APPROVAL_CHOICES = (
        (NO_APPROVAL, 'No Approval Required'),
        (OFFICE_HOURS_ONLY, 'Require Approval for Office Hours'),
        (ALL_MEETINGS, 'Require Approval for All Meetings')
    )

    require_meeting_approval = models.PositiveSmallIntegerField(
        choices=MEETING_APPROVAL_CHOICES, default=NO_APPROVAL)

    REVDEF_OFF = 0
    REVDEF_ON = 1
    REQUIRE_MENTOR_REVIEW_MEETING_CHOICES = (
        (REVDEF_ON, "On"),
        (REVDEF_OFF, "Off")
    )
    require_mentor_review_meetings = models.PositiveSmallIntegerField(
        choices=REQUIRE_MENTOR_REVIEW_MEETING_CHOICES, default=REVDEF_OFF)

    THIS_WEEK = 1
    NEXT_WEEK = 2
    MENTOR_SCHEDULE_CHOICES = (
        (THIS_WEEK, 'This Week'),
        (NEXT_WEEK, 'Next Week')
    )

    mentor_schedule_start = models.PositiveSmallIntegerField(
        choices=MENTOR_SCHEDULE_CHOICES, default=NEXT_WEEK)

    REQUIRED = 1
    OPTIONAL = 0
    REQUIREMENT_CHOICES = (
        (REQUIRED, 'Required'),
        (OPTIONAL, 'Optional')
    )

    require_job_deadlines = models.PositiveSmallIntegerField(
        choices=REQUIREMENT_CHOICES, default=OPTIONAL)

    ALL_DEALS = 1
    CURATED_ONLY = 0
    DEALS_CHOICES = (
        (ALL_DEALS, 'All Deals'),
        (CURATED_ONLY, 'Curated Deals Only')
    )

    tools = models.PositiveSmallIntegerField(choices=DEALS_CHOICES,
                                             default=ALL_DEALS)

    # users_can_tag is deprecated. this option now exists on
    # a per-tag basis as users_can_tag in the Label class.
    users_can_tag = models.BooleanField(default=True)
    alphabetize_tags = models.BooleanField(default=False)
    restrict_tagged_admins = models.BooleanField(default=False)

    # SW-658
    PUBLIC = 1
    PRIVATE = 0
    PRIVACY_CHOICES = (
        (PUBLIC, 'Public'),
        (PRIVATE, 'Require login to view')
    )

    user_profile_public = models.PositiveSmallIntegerField(
        choices=PRIVACY_CHOICES, default=PUBLIC)

    venture_profile_public = models.PositiveSmallIntegerField(
        choices=PRIVACY_CHOICES, default=PUBLIC)

    mentor_url = models.URLField(null=True, blank=True, default=None)

    ALLOW_OPT_OUT = 1
    DO_NOT_ALLOW_OPT_OUT = 2
    TEAM_MENTEE_OPT_OUT_CHOICES = (
        (ALLOW_OPT_OUT, "Allow"),
        (DO_NOT_ALLOW_OPT_OUT, "Don't Allow"),
    )
    team_mentee_opt_out = models.PositiveSmallIntegerField(
        choices=TEAM_MENTEE_OPT_OUT_CHOICES, default=ALLOW_OPT_OUT)

    ENABLED = 1
    DISABLED = 0
    ENABLED_DISABLED_CHOICES = (
        (ENABLED, 'Enabled'),
        (DISABLED, 'Disabled')
    )

    admin_notify_of_team_meeting_scheduled = models.PositiveSmallIntegerField(
        choices=ENABLED_DISABLED_CHOICES, default=DISABLED)
    admin_notify_of_team_meeting_cancelled = models.PositiveSmallIntegerField(
        choices=ENABLED_DISABLED_CHOICES, default=DISABLED)

    enable_central_events_calendar = models.PositiveSmallIntegerField(
        choices=ENABLED_DISABLED_CHOICES, default=DISABLED)
    central_events_calendar = models.EmailField(null=True, blank=True, default=None)
    enable_central_mentorship_calendar = models.PositiveSmallIntegerField(
        choices=ENABLED_DISABLED_CHOICES, default=DISABLED)
    central_mentorship_calendar = models.EmailField(null=True, blank=True, default=None)

    # custom text
    mentor_word = models.CharField(max_length=50, blank=True, default='mentor')
    mentee_word = models.CharField(max_length=50, blank=True, default='mentee')
    mentorship_word = models.CharField(max_length=50, blank=True, default='mentorship')
    venture_word = models.CharField(max_length=50, blank=True, default='venture')
    project_word = models.CharField(max_length=50, blank=True, default='project')
    job_word = models.CharField(max_length=50, blank=True, default='job')
    application_word = models.CharField(max_length=50, blank=True, default='application')
    # group_word is deprecated, use page_word instead
    group_word = models.CharField(max_length=50, blank=True, default='group')
    page_word = models.CharField(max_length=50, blank=True, default='page')
    # tools_word is deprecated, use deal_word instead
    tools_word = models.CharField(max_length=50, blank=True, default='tools')
    deal_word = models.CharField(max_length=50, blank=True, default='deal')
    # for platforms that don't have the user side dashboard
    team_word = models.CharField(max_length=50, blank=True, default='build team')

    # user signup customization
    signup_step1 = models.BooleanField(default=True) # photo, gender, location
    signup_step2 = models.BooleanField(default=True) # affiliation
    signup_tags = models.BooleanField(default=False)
    signup_step3 = models.BooleanField(default=True) # roles
    signup_skills = models.BooleanField(default=False)
    signup_biography = models.BooleanField(default=False)
    signup_step4 = models.BooleanField(default=True) # skills  depricated
    signup_step5 = models.BooleanField(default=True) # biography depricated

    require_step1 = models.BooleanField(default=False) # photo, gender, location
    require_step2 = models.BooleanField(default=True) # affiliation
    require_tags = models.BooleanField(default=False)
    require_step3 = models.BooleanField(default=True) # roles
    require_skills = models.BooleanField(default=False)
    require_biography = models.BooleanField(default=False)
    require_step4 = models.BooleanField(default=True) # skills depricated
    require_step5 = models.BooleanField(default=False) # biography depricated

    enable_default_skills = models.BooleanField(default=False)

    ########################
    # Custom Emails        #
    ########################
    email_invite_user_subject = models.CharField(max_length=150, default=None, null=True)  # `None` denotes default
    email_invite_user_body = models.TextField(default=None, null=True)  # `None` denotes default

    objects = UniversityStyleManager()

    def set_about(self, mark_down_about):
        self.markdown_about = mark_down_about

    def get_logo(self):
        if self.logo:
            return get_media_url() + str(self.logo)
        return None

    @property
    def require_mentor_review_meetings__bool(self):
        return self.require_mentor_review_meetings == UniversityStyle.REVDEF_ON

    def generate_default_email_invite_user_subject(self):
        return f"Invitation to join {self._university.program_name}'s entrepreneurship community on StartupTree"

    def generate_default_email_invite_user_body(self):
        uni = self._university
        plural_mentors = make_plural(uni.style.mentor_word)
        plural_jobs = make_plural(uni.style.job_word)

        default_body = \
            f"You're invited to join {uni.program_name} Program's StartupTree,\
            a platform designed to support entrepreneurs.<br><br> \
            Once you create an account, you'll be able to add your startup, \
            find {plural_jobs} and request {plural_mentors} in the {uni.name} entrepreneurship network."
        return default_body


class UniversityManager(models.Manager):
    def create_university(self, name, short_name, user_email=None, uni_exists=False,
                          is_private_platform=False, is_none_university=False):

        root = ROOT_DOMAIN  # Site.objects.get(id=1)
        domain = "{0}.{1}".format(short_name, root)
        site = Site.objects.create(domain=domain,
                                   name=name)
        if uni_exists:
            try:
                university = self.get(short_name__iexact=short_name)
                university.name = name
            except University.DoesNotExist:
                university = self.get(name__iexact=name)
                university.short_name = short_name
            university.site = site
            university.is_private_platform = is_private_platform
            university.is_none_university = is_none_university
            university.save()
        else:
            if user_email is None:
                university = self.create(name=name,
                                         short_name=short_name,
                                         is_private_platform=is_private_platform,
                                         is_none_university=is_none_university,
                                         site=site)
            else:
                university = self.create(admin=user_email,
                                         name=name,
                                         short_name=short_name,
                                         is_private_platform=is_private_platform,
                                         is_none_university=is_none_university,
                                         site=site)

        # for non-universities, step 2 of signup is disabled by default
        if is_none_university:
            if not UniversityStyle.objects.filter(_university=university).exists():
                style = UniversityStyle.objects.create(_university=university)
                style.signup_step2 = False
                style.require_step2 = False
                style.save()
            else:
                style = UniversityStyle.objects.get(_university=university)
                style.signup_step2 = False
                style.require_step2 = False
                style.save()

        return university

    def create_empty_university(self, name):
        if self.filter(name__iexact=name).exists():
            return self.filter(name__iexact=name)[0]
        short_name = "".join(random.choice(string.ascii_letters)
                             for _ in range(19))
        while self.filter(short_name=short_name).exists():
            short_name = "".join(random.choice(string.ascii_letters)
                                 for _ in range(19))
        university = self.create(name=name,
                                 short_name=short_name)
        return university

    def reenable_university(self, name, short_name, force=False):
        university = self.get(name__iexact=name)
        if university.site is None or force:
            root = ROOT_DOMAIN
            domain = "{0}.{1}".format(short_name, root)
            site = Site.objects.create(domain=domain,
                                       name=university.name)
            university.short_name = short_name
            university.site = site
            university.save()


class University(models.Model):
    '''Sites framework has
    1. domain
    2. name
    '''
    name = models.CharField(max_length=250, unique=True)
    short_name = models.CharField(max_length=20, unique=True)
    program_name = models.CharField(max_length=100, blank=True, null=True, default=None)
    created_on = models.DateField(auto_now_add=True)
    site = models.OneToOneField(Site, null=True, default=None, on_delete=models.SET_DEFAULT)
    # list of all admins of all access levels for this university. (See UniversityStaff)
    uni_staffs = models.ManyToManyField(UserEmail, through='UniversityStaff')

    # RFS-80 closed community option
    is_private_platform = models.BooleanField(default=False)
    # RFS-90 None university user setting.
    is_none_university = models.BooleanField(default=False)
    enable_alma_matters = models.BooleanField(default=False)

    objects = UniversityManager()

    # DEPRECATED fields
    staffs = models.ManyToManyField(UserEmail, blank=True, related_name='staffs')  # USE uni_staffs instead!!!!!!!!!!!!
    # admin = models.OneToOneField(UserEmail, null=True, default=None, on_delete=models.SET_NULL)
    # road_map = models.ManyToManyField(RoadMapBlock, blank=True, related_name='road_map')
    custom_invite_email = models.TextField(default=None, null=True)  # DEPRECATED USE UniversityStyle

    # rest_framework integration trial begin
    class Meta:
        ordering = ('name',)

    # rest_framework integration trial end

    def __str__(self):
        return self.name

    def get_admin_panel_url(self, current_site=None):
        protocol = 'https'
        url = '{1}://{0}/university/dashboard'
        if current_site:
            return url.format(current_site.domain, protocol)
        return url.format(self.site.domain, protocol)

    def get_login_url(self):
        protocol = 'https'
        return '{1}://{0}/login'.format(self.site.domain, protocol)

    def get_url(self):
        protocol = 'https'
        return '{1}://{0}'.format(self.site.domain, protocol)

    def get_host(self):
        """
        returns site domain mainly used for setting HTTP_HOST for test client.
        :return:
        """
        return self.site.domain

    def get_team_mentorship_teams(self):
        """
        returns a queryset of the startups that are associated with
        the universities team mentorship program.

        this is intended to be the set of startups which the university admin
        explicitly added to their team mentorship program.
        """
        return self.startup_set.filter(team_mentorship_participant=True)

    @property
    def style(self):
        return self.universitystyle_set.first()

    def get_program_name(self):
        if self.program_name:
            return self.program_name
        return self.name

    def get_blog(self):
        if self.universitystyle_set.first() is not None:
            return self.universitystyle_set.first().uni_blog
        else:
            return None

    def get_facebook(self):
        style = self.universitystyle_set.first()
        if style.facebook is not None and 'https://' not in style.facebook and \
                'http://' not in style.facebook:
            return 'https://' + style.facebook
        return style.facebook

    def get_twitter(self):
        style = self.universitystyle_set.first()
        if style.twitter is not None and 'https://' not in style.twitter and \
                'http://' not in style.twitter:
            return 'https://' + style.twitter
        return style.twitter

    def get_instagram(self):
        style = self.universitystyle_set.first()
        if style.instagram is not None and 'https://' not in style.instagram and \
                'http://' not in style.instagram:
            return 'https://' + style.instagram
        return style.instagram

    def get_slack(self):
        style = self.universitystyle_set.first()
        if style.slack is not None and 'https://' not in style.slack and \
                'http://' not in style.slack:
            return 'https://' + style.slack
        return style.slack


class RoadMap(models.Model):
    university = models.ForeignKey(
        University, null=True, on_delete=models.SET_NULL)


class RoadMapColumn(models.Model):
    """
    One of four columns of roadmap,
    which contains blocks
    """
    name = models.CharField(max_length=50, blank=True, null=True)
    number = models.PositiveSmallIntegerField(blank=True, null=True)
    road_map = models.ForeignKey(RoadMap, null=True, on_delete=models.SET_NULL)

    def decr_num(self):
        self.number = self.number - 1
        self.save()


class RoadMapBlock(models.Model):
    """
    Represents individual block components in roadmap
    that need to be filled out by an admin / group of admins
    """
    title = models.CharField(max_length=100)
    rm_block_color = models.CharField(max_length=100, null=True, default=None)
    text = models.TextField(blank=False, null=True, default=None)
    char_limit = models.PositiveIntegerField(
        blank=True, null=True, default=None)
    # image_url = models.URLField(null=True, blank=True, default=None)
    image = models.ImageField(upload_to=get_rmblock_image_url,
                              null=True,
                              blank=True,
                              default=None,
                              max_length=700)
    uploaded_file = models.FileField(null=True,
                                     default=None,
                                     upload_to=get_rmblock_file_url,
                                     max_length=700,
                                     blank=True)
    website = models.URLField(max_length=500, null=True, default=None)
    # deprecated, all column information is stored in rm_columns
    column = models.PositiveIntegerField(blank=True, null=True, default=None)
    rm_columns = models.ManyToManyField(RoadMapColumn)
    time_created = models.DateField(null=True, blank=True, default=None)
    access_admins = models.ManyToManyField(UserEmail)
    header = models.TextField(blank=False, null=True, default=None)
    university = models.ForeignKey(University, null=True,
                                   on_delete=models.SET_NULL)  # leave this in so that all unis associated won't be deleted
    road_map = models.ForeignKey(RoadMap, null=True, on_delete=models.SET_NULL)

    def get_text(self):
        return self.text

    def get_column_num(self):
        tmp_lst = []
        for clm in self.rm_columns.all():
            tmp_lst.append(clm.number)
        return tmp_lst

    def get_admins(self):
        return self.access_admins

    def get_header(self):
        return self.header


class UniversityStaff(models.Model):
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    admin = models.ForeignKey(UserEmail, on_delete=models.CASCADE)
    is_super = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)  # just for internal record
    admin_team = models.CharField(max_length=100, null=True, default=None)

    # Admin Settings
    access_levels = models.ManyToManyField('AccessLevel')  # When changed, CALL uni_staff_access_levels__m2m_changed
    point_of_contact = models.ManyToManyField('PointOfContact')
    cal_sync_option = models.ManyToManyField('AdminCalSyncOption')
    email_noti_option = models.ManyToManyField('AdminEmailNotiOption')

    # BEGIN deprecated, use access_levels instead
    SUPER = 1
    DISCUSSION_ONLY = 2
    EVENT_ONLY = 3
    COMPETITION_ONLY = 4
    EVENT_AND_COMPETITION = 5
    ACCESS_LEVEL = (
        (SUPER, "Super User"),
        (DISCUSSION_ONLY, "Forum Contributer"),
        (EVENT_ONLY, "Event Organizer"),
        (COMPETITION_ONLY, "Competition Organizer"),
        (EVENT_AND_COMPETITION, "Event & Competition Organizer")
    )
    access_level = models.SmallIntegerField(
        choices=ACCESS_LEVEL, default=SUPER)
    # END deprecated, use access_levels instead

    def is_authorized(self, *access_levels):
        """ Returns whether self has access to access_level. """
        access_level: AccessLevel

        if self.is_super:
            return True

        self_access_levels = [access_level.name for access_level in self.access_levels.all()]
        if AccessLevel.ALL in self_access_levels:
            return True

        if set(access_levels).issubset(set(self_access_levels)):
            return True

        return False

    def uni_staff_access_levels__m2m_changed(self):
        """This method should be called after each time that UniversityStaff.access_levels is updated.

        Here, we make calls to other helper methods that should be invoked to handle cases
        where code needs to be executed when an admin gains or loses certain access levels.

        Note:
            This method is purposely not implemented as a "signal", because each "update" of
            access levels may entail many add and remove operations, which would invoke the
            signal multiple times.

            Instead, this method should be called once, at the end of all the add/remove operations
            of the update.


        Current use cases:
            Calendar Sync Options:
                If admin loses a permission (eg mentorship) that effects which kinds of events they can cronofy sync to
                (eg mentorship meeting), then remove the uni_staff.cal_sync_option, and remove future synced events.
            Email Noti Options
        """
        AdminCalSyncOptionManager.revoke_unallowed_sync_types(self)
        AdminEmailNotiOptionManager.revoke_unallowed_option_types(self)


class AccessLevel(models.Model):
    ALL = ALL
    DASHBOARD = DASHBOARD
    USERS = USERS
    VENTURES = VENTURES
    PROJECTS = PROJECTS
    GROUPS = GROUPS
    TOOLS = TOOLS
    TAGS = TAGS
    EXPORT = EXPORT
    CUSTOM_DATA = CUSTOM_DATA
    REPORTING = REPORTING
    EVENTS = EVENTS
    JOBS = JOBS
    COMPETITIONS = COMPETITIONS
    APPLICATIONS = APPLICATIONS
    SURVEYS = SURVEYS
    MENTORSHIP = MENTORSHIP
    TEAM_MENTORSHIP = TEAM_MENTORSHIP
    INVITE = INVITE
    ANNOUNCE = ANNOUNCE
    SUPPORT = SUPPORT
    ADMINS = ADMINS
    ROADMAP = ROADMAP
    PROGRAM = PROGRAM
    DESIGN = DESIGN
    PLATFORM = PLATFORM
    PAGES = PAGES
    CUSTOM_TEXT = CUSTOM_TEXT
    CUSTOM_ROLES = CUSTOM_ROLES
    USER_SIGNUP = USER_SIGNUP
    PRIVATE_DISCUSSION = PRIVATE_DISCUSSION

    ACCESS_LEVELS = (
        (DASHBOARD, "Dashboard"),
        (USERS, "Users"),
        (VENTURES, "Ventures"),
        (PROJECTS, "Projects"),
        (GROUPS, "Groups"),
        (TOOLS, "Deals"),
        (TAGS, "Tags"),
        (EXPORT, "Export"),
        (CUSTOM_DATA, "Custom Data"),
        (REPORTING, "Reporting"),
        (EVENTS, "Events"),
        (JOBS, "Jobs"),
        (COMPETITIONS, "Competitions"),
        (APPLICATIONS, "Applications"),
        (SURVEYS, "Surveys"),
        (MENTORSHIP, "Mentorship"),
        (TEAM_MENTORSHIP, "Team Mentorship"),
        (INVITE, "Invite"),
        (ANNOUNCE, "Announce"),
        (SUPPORT, "Support"),
        (ADMINS, "Admins"),
        (ROADMAP, "Roadmap"),
        (PROGRAM, "Program"),
        (DESIGN, "Design"),
        (PLATFORM, "Platform"),
        (PAGES, "Pages"),
        (CUSTOM_TEXT, "Custom Text"),
        (CUSTOM_ROLES, "Custom Roles"),
        (USER_SIGNUP, "User Signup"),
        (PRIVATE_DISCUSSION, "Forum")
    )

    name = models.CharField(max_length=25, choices=ACCESS_LEVELS)
    university = models.ForeignKey(University, on_delete=models.CASCADE)

    def __str__(self):
        if self.name == self.VENTURES:
            venture_word = self.university.style.venture_word.capitalize()
            return self.pluralize(venture_word)
        elif self.name == self.PROJECTS:
            project_word = self.university.style.project_word.capitalize()
            return self.pluralize(project_word)
        elif self.name == self.GROUPS:
            page_word = self.university.style.page_word.capitalize()
            return self.pluralize(page_word)
        elif self.name == self.TOOLS:
            deal_word = self.university.style.deal_word.capitalize()
            return self.pluralize(deal_word)
        elif self.name == self.APPLICATIONS:
            application_word = self.university.style.application_word.capitalize()
            return self.pluralize(application_word)
        elif self.name == self.MENTORSHIP:
            return self.university.style.mentorship_word.capitalize()
        elif self.name == self.TEAM_MENTORSHIP:
            mentorship_word = self.university.style.mentorship_word.capitalize()
            return "Team {0}".format(mentorship_word)
        elif self.name == self.JOBS:
            job_word = self.university.style.job_word.capitalize()
            return self.pluralize(job_word)

        return self.name.replace("_", " ").title()

    def pluralize(self, word):
        if word.endswith('y'):
            plural_word = word[:-1]
            plural_word += 'ies'
        elif word.endswith(('s', 'x', 'z', 'ch', 'sh')):
            plural_word = word + 'es'
        else:
            plural_word = word + 's'

        return plural_word


def is_comp_app_admin(request, is_application):
    if request.is_super:
        return True
    elif is_application and AccessLevel.APPLICATIONS in request.access_levels:
        return True
    elif not is_application and AccessLevel.COMPETITIONS in request.access_levels:
        return True
    return False


class PointOfContact(models.Model):
    """
    Points of contact that an admin is of.
    Client manages via Customize Access modal in manage admins page.

    To add a new point of contact:

        Add to both lists below:
            "Point of contact choice keys"
            "Point of contact choices"

        Run makemigrations and migrate.

        Then, in shell, run: PointOfContact.objects.get_or_create(name=<new_point_of_contact_key>)

        eg, to add TEAM_MENTORSHIP, run: PointOfContact.objects.get_or_create(name='team_mentorship')

    To delete a point of contact:

        In a shell, run: PointOfContact.objects.get(name=<point_of_contact_key>).delete()

        eg, to delete TEAM_MENTORSHIP, run: PointOfContact.objects.get(name='team_mentorship').delete()

        Remove it from both lists below:
            "Point of contact choice keys"
            "Point of contact choices"

        Run makemigrations and migrate.
    """

    # Point of contact choice keys
    MENTORSHIP = "mentorship"
    TEAM_MENTORSHIP = "team_mentorship"

    # Point of contact choices
    POINT_OF_CONTACT = (
        (MENTORSHIP, "Mentorship"),
        (TEAM_MENTORSHIP, "Team Mentorship"),
    )

    name = models.CharField(max_length=64, choices=POINT_OF_CONTACT)

    def __str__(self):
        return self.name.replace("_", " ").title()  # TODO FIX (mentor word)


class AdminCalSyncOptionManager(models.Manager):
    @staticmethod
    def get_allowed_sync_types(uni_staff: UniversityStaff):
        """ Given an UniStaff object, return the allowed sync types
        based on their access levels and the REQ_ACCESS_LVLS table. """
        sync_types = dict(AdminCalSyncOption.SYNC_TYPES).keys()
        return filter(lambda sync_type: AdminCalSyncOption.is_admin_authorized(uni_staff, sync_type), sync_types)

    @staticmethod
    def revoke_unallowed_sync_types(uni_staff: UniversityStaff):
        """ Given a UniStaff object, if the person has a cal_sync_option that
        is not allowed (based on get_allowed_sync_types), then remove it,
        and delete future events of the sync type from the admin's calendar. """
        from trunk.st_cronofy import AbstractAdminCalSyncUtil  # import here to prevent circular import
        from mentorship.st_cronofy import is_cronofy_linked

        allowed_sync_types = set(AdminCalSyncOptionManager.get_allowed_sync_types(uni_staff))
        current_sync_types = set(cal_option.sync_type for cal_option in uni_staff.cal_sync_option.all())
        unallowed_sync_types = current_sync_types - allowed_sync_types  # set minus

        is_admin_cronofy_linked = is_cronofy_linked(uni_staff.admin)

        for sync_type in unallowed_sync_types:
            uni_staff.cal_sync_option.remove(sync_type)
            if is_admin_cronofy_linked:
                sync_util: AbstractAdminCalSyncUtil = AdminCalSyncOption.get_sync_util(uni_staff, sync_type)
                sync_util.remove_sync()


class AdminCalSyncOption(models.Model):
    """
    Admin Calendar Sync Options.

    Client manages via their user "settings" page (in the admin-level settings section).

    To add a new admin calendar sync option:

        Add to lists below:
            "Sync Type Codes"
            "Sync Type Names"

        Run makemigrations and migrate.

        Run python3 manage.py admincaloption_refresh.py

        Update any AdminCalSyncOption methods like AdminCalSyncOption.get_public_str()
            and AdminCalSyncOption.is_admin_authorized()

        Implement a sync utility in trunk.st_cronofy (subclass AbstractAdminCalSyncUtil)

    To delete a admin calendar sync option:

        (Warning: Removing an option will also delete the corresponding setting for all admins.)

        In a shell, run: AdminCalSyncOption.objects.get(sync_type=<AdminCalOption_key>).delete()
            eg, to delete TEAM_MENTORSHIP_MEETINGS,
                run: AdminCalSyncOption.objects.get(sync_type='team_mentorship_meetings').delete()

        Remove it from both lists below:
            "Sync Type Codes"
            "Sync Type Names"

        Run makemigrations and migrate.

        Delete any obsolete code including in AdminCalSyncOption methods, and trunk.st_cronofy.py
        """

    # Sync Type Codes
    MENTORSHIP_MEETINGS = 1
    TEAM_MENTORSHIP_MEETINGS = 2

    # Sync Type Names
    # Warning: Names are internal use only; as they contain the word "Mentorship". External names
    # need to take into account school's mentorship word setting.
    SYNC_TYPES = (
        (MENTORSHIP_MEETINGS, "All Mentorship Meetings"),
        (TEAM_MENTORSHIP_MEETINGS, "All Team Mentorship Meetings"),
    )

    sync_type = models.SmallIntegerField(choices=SYNC_TYPES, primary_key=True)

    objects = AdminCalSyncOptionManager()

    def get_public_str(self, uni: University):
        private_str = dict(AdminCalSyncOption.SYNC_TYPES)[self.sync_type]

        if self.sync_type == AdminCalSyncOption.MENTORSHIP_MEETINGS:
            return replace_mentorship_word(private_str, uni)
        elif self.sync_type == AdminCalSyncOption.TEAM_MENTORSHIP_MEETINGS:
            return replace_mentorship_word(private_str, uni)
        else:
            app_logger.error(f'AdminCalOption object (id {self.id}) invalid sync_type: {self.sync_type}')
            return None

    @staticmethod
    def is_admin_authorized(uni_staff: UniversityStaff, sync_type: int):
        uni_style: UniversityStyle = uni_staff.university.style
        if sync_type == AdminCalSyncOption.MENTORSHIP_MEETINGS:
            return uni_style.display_mentors and uni_staff.is_authorized(AccessLevel.MENTORSHIP)

        elif sync_type == AdminCalSyncOption.TEAM_MENTORSHIP_MEETINGS:
            uni_subscription: UniversitySubscription = uni_staff.university.universitysubscription
            return uni_subscription.is_team_mentorship and uni_staff.is_authorized(AccessLevel.TEAM_MENTORSHIP)

        else:
            app_logger.error(f'Invalid sync_type: {sync_type}')
            return None

    @staticmethod
    def get_sync_util(uni_staff: UniversityStaff, sync_type: int):
        # prevent circular import
        from trunk.st_cronofy import AdminCalSyncUtilMentorshipMeetings, AdminCalSyncUtilTeamMentorshipMeetings

        if sync_type == AdminCalSyncOption.MENTORSHIP_MEETINGS:
            return AdminCalSyncUtilMentorshipMeetings(uni_staff)

        elif sync_type == AdminCalSyncOption.TEAM_MENTORSHIP_MEETINGS:
            return AdminCalSyncUtilTeamMentorshipMeetings(uni_staff)

        else:
            app_logger.error(f'Invalid sync_type: {sync_type}')
            return None


class AdminEmailNotiOptionManager(models.Manager):
    @staticmethod
    def get_allowed_option_types(uni_staff: UniversityStaff):
        """ Given an UniStaff object, return the allowed sync types
        based on their access levels and the REQ_ACCESS_LVLS table. """
        option_types = dict(AdminEmailNotiOption.OPTION_TYPES).keys()
        return filter(lambda option_type: AdminEmailNotiOption.is_admin_authorized(uni_staff, option_type), option_types)

    @staticmethod
    def revoke_unallowed_option_types(uni_staff: UniversityStaff):
        """ Given a UniStaff object, if the person has an email_noti_option that
        is not allowed (based on get_allowed_option_types), then remove it,
        and delete future events of the sync type from the admin's calendar. """
        allowed_option_types = set(AdminEmailNotiOptionManager.get_allowed_option_types(uni_staff))
        current_option_types = set(email_option.option_type for email_option in uni_staff.email_noti_option.all())
        unallowed_option_types = current_option_types - allowed_option_types  # set minus

        for option_type in unallowed_option_types:
            uni_staff.email_noti_option.remove(option_type)


class AdminEmailNotiOption(models.Model):
    """
    Email Notification Options for admins.

    To add a new admin calendar sync option:

        Add to lists below:
            "Option Type Codes"
            "Option Type Names"

        Optionally, you made add to "Option Help Text". This is not required. If you don't add,
        then help text for this option won't be shown on front end.

        Run makemigrations and migrate.

        Run python3 manage.py adminemailoption_refresh.py

        Update any AdminEmailNotiOption methods like AdminEmailNotiOption.get_public_str()
            and AdminEmailNotiOption.is_admin_authorized()

    To delete a admin calendar sync option:

        (Warning: Removing an option will also delete the corresponding setting for all admins.)

        In a shell, run: AdminEmailNotiOption.objects.get(option_type=<AdminEmailNotiOption_key>).delete()
            eg, to delete TEAM_MENTORSHIP_MEETINGS,
                run: AdminEmailNotiOption.objects.get(option_type='team_mentorship_meetings').delete()

        Remove it from both lists below:
            "Option Type Codes"
            "Option Type Names"

        Run makemigrations and migrate.

        Delete any obsolete code including in AdminEmailNotiOption methods
    """
    # Option Type Codes
    PERSON_APPLIED_TO_COMPETITION = 1
    PERSON_APPLIED_TO_APPLICATION = 2

    # Option Type Names
    # Warning: Names are internal use only; as they may contain the word "Mentorship". External names
    # need to take into account school's mentorship word setting. When adding to this list, add a case to
    # AdminEmailNotiOption.get_public_str()
    OPTION_TYPES = (
        (PERSON_APPLIED_TO_COMPETITION, "Whenever someone enters in a Competition"),
        (PERSON_APPLIED_TO_APPLICATION, "Whenever someone applies to an Application"),
    )

    # Option Help Text
    # Warning: Names are internal use only; as they may contain the word "Mentorship". External names
    # need to take into account school's mentorship word setting. When adding to this list, add a case to
    # AdminEmailNotiOption.get_public_help_text()
    OPTION_HELP_TEXT = (
        (PERSON_APPLIED_TO_COMPETITION, "Applies only to competitions that have at least one tag the same as you."),
        (PERSON_APPLIED_TO_APPLICATION, "Applies only to applications that have at least one tag the same as you."),
    )

    option_type = models.SmallIntegerField(choices=OPTION_TYPES, primary_key=True)

    objects = AdminEmailNotiOptionManager()

    def get_public_str(self, uni: University):
        """ uni is taken as a parameter, for cases when we need to replace 'mentorship' word with university-specified
        word.
        """
        private_str = dict(AdminEmailNotiOption.OPTION_TYPES)[self.option_type]

        if self.option_type == AdminEmailNotiOption.PERSON_APPLIED_TO_COMPETITION:
            return private_str
        elif self.option_type == AdminEmailNotiOption.PERSON_APPLIED_TO_APPLICATION:
            return private_str
        else:
            app_logger.error(f'AdminEmailNotiOption object (id {self.id}) invalid option_type: {self.option_type}')
            return None

    def get_public_help_text(self, uni: University):
        """ uni is taken as a parameter, for cases when we need to replace 'mentorship' word with university-specified
        word.
        """
        private_str = dict(AdminEmailNotiOption.OPTION_HELP_TEXT).get(self.option_type, '')

        if self.option_type == AdminEmailNotiOption.PERSON_APPLIED_TO_COMPETITION:
            return private_str
        elif self.option_type == AdminEmailNotiOption.PERSON_APPLIED_TO_APPLICATION:
            return private_str
        else:
            app_logger.error(f'AdminEmailNotiOption object (id {self.id}) invalid option_type: {self.option_type}')
            return None

    @staticmethod
    def is_admin_authorized(uni_staff: UniversityStaff, option_type: int):
        uni_style: UniversityStyle = uni_staff.university.style
        if option_type == AdminEmailNotiOption.PERSON_APPLIED_TO_COMPETITION:
            return uni_style.display_competitions and uni_staff.is_authorized(AccessLevel.COMPETITIONS)
        elif option_type == AdminEmailNotiOption.PERSON_APPLIED_TO_APPLICATION:
            return uni_style.display_applications and uni_staff.is_authorized(AccessLevel.APPLICATIONS)
        else:
            app_logger.error(f'Invalid option_type: {option_type}')
            return None


class SchoolCategoryManager(models.Manager):
    def create_category(self, name):
        name = name.lower()
        return self.get_or_create(name=name)[0]


class SchoolManager(models.Manager):
    def create_school(self, name, university, get_status=False):
        # Check for duplicates
        name_id = name.lower().strip()
        school = self.filter(name=name_id,
                             university=university)
        if len(school) > 0:
            if get_status:
                return school[0], False
            return school[0]
        school = self.create(name=name_id,
                             displayed=name.strip(),
                             university=university)
        if get_status:
            return school, True
        return school

    def get_by_name(self, name, university=None, many=False):
        name = name.lower().strip()
        if many:
            return self.filter(name__iexact=name, university=university)
        result = self.filter(name__iexact=name, university=university)
        if result.count() < 1:
            return None
        return result[0]


class School(models.Model):
    """School/College
    """
    name = models.CharField(max_length=200)
    displayed = models.CharField(max_length=200,
                                 null=True,
                                 default=None)
    university = models.ForeignKey(University, on_delete=models.CASCADE)

    objects = SchoolManager()

    def __str__(self):
        return self.get_name()

    def get_name(self):
        if self.displayed is None:
            return better_title(self.name)
        return self.displayed


class MajorManager(models.Manager):
    def create_major(self, name):
        name_id = name.lower().strip()
        if self.filter(name=name_id).exists():
            return self.filter(name=name_id)[0]
        return self.create(name=name_id,
                           displayed=name.strip())

    def get_by_name(self, name):
        name = name.lower().strip()
        return self.get(name=name)

    def goc_major(self, name):
        name_id = name.lower().strip()
        majors = self.filter(name=name_id)
        if len(majors) == 0:
            return self.create(name=name_id,
                               displayed=name.strip())
        elif len(majors) == 1:
            return majors[0]
        else:
            app_logger.error("Duplicated majors! - {0}".format(name))
            return majors[0]


class Major(models.Model):
    """Major
    """
    name = models.CharField(max_length=200, unique=True)
    displayed = models.CharField(max_length=200,
                                 null=True,
                                 default=None)

    objects = MajorManager()

    def __str__(self):
        return self.get_name()

    def get_name(self):
        if self.displayed is None:
            return better_title(self.name)
        return self.displayed


class JudgeBoard(models.Model):
    '''Board for managing judges for an applicantion event!'''
    JUDGE_PER_APP = 1
    ALL_FOR_ALL = 2
    MANUALLY_ASSIGN = 3
    APP_PER_JUDGE = 4

    ASSIGN_TYPES = (
        (APP_PER_JUDGE, 'Applicants Per Judge'),
        (JUDGE_PER_APP, 'Judges per Applicant'),
        (ALL_FOR_ALL, 'All for All'),
        (MANUALLY_ASSIGN, 'Manually Assign')
    )
    judges = models.ManyToManyField('Judge', blank=True,
                                    related_name='event_judges')
    passcode = models.CharField(null=True, blank=True, max_length=20)
    invite_email = models.TextField(null=True, default=None)
    display_invitation = models.BooleanField(default=False)
    competition = models.ForeignKey('Event', on_delete=models.CASCADE)
    # judging round
    round = models.PositiveSmallIntegerField(null=True)
    status = models.PositiveSmallIntegerField(
        choices=ASSIGN_TYPES,
        default=2,
        null=True
    )
    allotment = models.PositiveIntegerField(null=True)
    is_blind = models.BooleanField(default=False)
    is_anonymous = models.BooleanField(default=False)
    deadline = models.DateTimeField(null=True)
    TIMEZONES = [(t, t) for t in pytz.common_timezones]
    timezone = models.CharField(
        max_length=100,
        choices=TIMEZONES,
        null=True,
        default=None)
    auto_send_email = models.BooleanField(default=False)

    def add_judge(self, judge):
        self.judges.add(judge)

    def remove_judge(self, judge):
        self.judges.remove(judge)

    def deadline_passed(self):
        try:
            if self.deadline is None:
                return True
        except:
            return True
        if self.timezone is not None:
            try:
                end = timezone.localtime(self.deadline, pytz.timezone(self.timezone))
            except ValueError:
                try:
                    end = timezone.make_aware(self.deadline, pytz.timezone(self.timezone))
                except ValueError:
                    end = self.deadline
        else:
            try:
                end = timezone.localtime(self.deadline, pytz.timezone(self.competition.timezone))
            except ValueError:
                try:
                    end = timezone.make_aware(self.deadline, pytz.timezone(self.competition.timezone))
                except ValueError:
                    end = self.deadline
        now = timezone.now()
        if now > end:
            return True
        return False

    def get_end_date(self):
        if self.timezone is not None:
            try:
                end = timezone.localtime(self.deadline, pytz.timezone(self.timezone))
            except ValueError:
                try:
                    end = timezone.make_aware(self.deadline, pytz.timezone(self.timezone))
                except ValueError:
                    end = self.deadline
        else:
            try:
                end = timezone.localtime(self.deadline, pytz.timezone(self.competition.timezone))
            except ValueError:
                try:
                    end = timezone.make_aware(self.deadline, pytz.timezone(self.competition.timezone))
                except ValueError:
                    end = self.deadline
        return end.strftime('%m/%d/%Y')

    def get_end_time(self):
        if self.timezone is not None:
            try:
                end = timezone.localtime(self.deadline, pytz.timezone(self.timezone))
            except ValueError:
                try:
                    end = timezone.make_aware(self.deadline, pytz.timezone(self.timezone))
                except ValueError:
                    end = self.deadline
        else:
            try:
                end = timezone.localtime(self.deadline, pytz.timezone(self.competition.timezone))
            except ValueError:
                try:
                    end = timezone.make_aware(self.deadline, pytz.timezone(self.competition.timezone))
                except ValueError:
                    end = self.deadline
        return end.strftime('%I:%M %p')


class JudgeManager(models.Manager):
    def create_judge(self, passcode, event, platform, email, profile=None,
                     first_name='Judge', last_name=''):
        '''Create a judge'''
        if passcode == '':
            raise Exception('cannot be blank')
        judge = self.create(first_name=first_name,
                            last_name=last_name,
                            event=event,
                            profile=profile,
                            platform=platform,
                            email=email,
                            passcode=passcode)


class Judge(models.Model):
    first_name = models.CharField(max_length=50, default='Judge')
    last_name = models.CharField(max_length=50, default='', null=True)
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    # judging round
    round = models.PositiveSmallIntegerField(null=True)
    profile = models.ForeignKey('branch.StartupMember',
                                blank=True,
                                null=True,
                                related_name='judge_startupmember', on_delete=models.CASCADE)
    platform = models.ForeignKey(University, on_delete=models.CASCADE)
    email = models.EmailField(unique=False)
    applicant_user = models.ManyToManyField('branch.StartupMember',
                                            related_name='judge_applicant_user')
    anonymous_applicant = models.ManyToManyField('forms.AnonymousApplicant')
    judge_id = models.UUIDField(unique=True, default=uuid.uuid4,
                                editable=False)
    passcode = models.CharField(max_length=20, blank=True, null=True)
    ratings = models.ManyToManyField('JudgeReview',
                                     related_name='judge_all_ratings')  # not needed. deprecated SW-3281
    has_submitted = models.BooleanField(default=False)
    email_sent = models.BooleanField(default=False)
    has_logged_in = models.DateTimeField(blank=True, null=True, default=None)
    is_archived = models.BooleanField(default=False)

    objects = JudgeManager()

    def add_rating(self, rating):
        '''Add a JudgeReview for this Judge'''
        self.ratings.add(rating)

    def get_link(self):
        return "/review/login_judge/{0}/{1}".format(self.event.event_id, self.judge_id)

    def get_name(self):
        if self.profile:
            return self.profile.get_full_name()
        return "{0} {1}".format(self.first_name, self.last_name)


class JudgeRating(models.Model):
    """
    rating between 1-5 stars [DEPRECATED]
    """
    rating = models.PositiveSmallIntegerField()
    judge = models.ForeignKey('branch.StartupMember',
                              related_name="judge", null=True, on_delete=models.CASCADE)
    applicant = models.ForeignKey(
        'branch.StartupMember', related_name="competition_applicant", null=True, on_delete=models.CASCADE)
    form = models.ForeignKey('forms.Form', null=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True)


class JudgeReviewManager(models.Manager):
    def create_judge_rating(self, judge, applicant, form, event,
                            overall_rank=None, applicant_venture=None, anonymous_applicant=None):
        jr = self.create(overall_rank=overall_rank, judge=judge, status=1,
                         applicant=applicant, event=event, form=form,
                         applicant_venture=applicant_venture, anonymous_applicant=anonymous_applicant,
                         is_anonymous=anonymous_applicant is not None)
        return jr


class JudgeReview(models.Model):
    """
    Judge's feedback on the applications
    """
    NOT_STARTED = 1
    IN_PROGRESS = 2
    COMPLETE = 3

    STATUS_TYPES = (
        (NOT_STARTED, 'Not Started'),
        (IN_PROGRESS, 'In Progress'),
        (COMPLETE, 'Complete')
    )

    overall_rank = models.PositiveIntegerField(default=None, null=True)
    score = models.PositiveIntegerField(default=None, null=True)
    event = models.ForeignKey('Event',
                              null=True,
                              related_name="judge_review", on_delete=models.CASCADE)
    judge = models.ForeignKey('Judge', related_name="review_judge", on_delete=models.CASCADE)
    applicant = models.ForeignKey('branch.StartupMember',
                                  related_name="review_applicant", null=True, on_delete=models.CASCADE)
    applicant_venture = models.CharField(
        max_length=250, null=True, default=None)
    # for public competition
    is_anonymous = models.BooleanField(default=False)
    anonymous_applicant = models.ForeignKey(
        'forms.AnonymousApplicant', null=True, default=None, related_name="review_anon", on_delete=models.CASCADE)

    applicant_timestamp = models.DateTimeField(null=True, default=None)  # FIXME: not sure what this is for
    form = models.ForeignKey('forms.Form', null=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True)
    # indicates whether comment has been sent to the applicant
    is_sent = models.BooleanField(default=False)
    personal_notes = models.TextField(null=True)
    status = models.PositiveSmallIntegerField(
        choices=STATUS_TYPES,
        default=None,
        null=True)
    objects = JudgeReviewManager()

    def get_applicant(self):
        if self.applicant:
            return self.applicant
        elif self.applicant_venture:
            return self.applicant_venture
        elif self.anonymous_applicant:
            return self.anonymous_applicant
        raise ValueError(
            "Applicant does not exist for the JudegeReview {0}".format(self.id))

    def get_applicant_name(self):
        if self.applicant:
            return self.applicant.get_full_name()
        elif self.anonymous_applicant:
            return self.anonymous_applicant.name
        raise ValueError(
            "Applicant does not exist. Invalid data {0}".format(self.id))

    def get_applicant_timetamp(self):
        if self.applicant:
            try:
                return self.applicant.comp_applicant.get(event_id=self.event_id).date_applied
            except:
                return None
        elif self.anonymous_applicant:
            return self.anonymous_applicant.date_created
        raise ValueError(
            "Applicant does not exist. Invalid data {0}".format(self.id))

    def get_applicant_venture(self):
        if self.applicant:
            try:
                return self.applicant.comp_applicant.get(event_id=self.event_id).venture
            except:
                return None
        elif self.anonymous_applicant:
            return self.anonymous_applicant.venture
        raise ValueError(
            "Applicant does not exist. Invalid data {0}".format(self.id))

    def get_url_name(self):
        if self.applicant:
            return self.applicant.url_name
        elif self.anonymous_applicant:
            return str(self.anonymous_applicant.applicant_id)
        raise ValueError(
            "Applicant does not exist for the JudegeReview {0}".format(self.id))


class Round(models.Model):
    """
    Corresponds to a single round of an application or competition with multiple
    application/competition rounds.

    Attributes:
        event: the Event object associated with this round.
        number: an int representing which round this is. (first, second, etc.)
    """
    event = models.ForeignKey('Event', related_name='round_event', on_delete=models.CASCADE)
    number = models.PositiveSmallIntegerField()


class GuestRSVP(models.Model):
    """
    Stores information for a single event guest, who is a user associated
    with any event (i.e. via an RSVP, invitation, or check-in) and who is
    not signed in.

    The same GuestRSVP object is used to refer to every instance of an event
    guest with the same email. For example, if a guest used the same email to
    RSVP to two different events, these would be considered the same event guest.

    Attributes:
        is_archived: a boolean indicating whether this event guest
            has been archived by admin.
        accept_time: the date/time when the event guest was created.
        is_invited: a boolean indicating whether the event guest
            was initially invited to an event by admin.
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    is_archived = models.BooleanField(default=False)
    accept_time = models.DateTimeField(auto_now_add=True)
    is_invited = models.BooleanField(default=False)


class EventBoard(models.Model):
    """
    Manages all of the application/competition rounds that are associated with
    a single application or competition.

    Used to keep track of which Event objects are considered part of the same
    application or competition by organizing the original event and all of its
    subsequent rounds into one "board".

    Attributes:
        root: an Event object indicating the original application or
            competition that the rounds are all associated with.
        rounds: all the Round objects that are associated with the root
            application or competition.
    """
    root = models.ForeignKey('Event', on_delete=models.CASCADE)
    rounds = models.ManyToManyField('Round', blank=True)

    def add_round(self, event):
        """
        Adds a new round to the event board.

        Args:
            event: an Event object corresponding to the new application/
                competition round, from which the new Round object will be
                created.
        """
        if self.rounds.count() > 0:
            last_round = self.rounds.all().order_by('-number').first().number
            new_round = Round(event=event, number=last_round + 1)
            new_round.save()
            self.rounds.add(new_round)
        else:
            new_round = Round(event=event, number=1)
            new_round.save()
            self.rounds.add(new_round)


class EventRSVP(models.Model):
    """
    Stores information for a single RSVP (as opposed to a check-in) associated
    with a single event.

    The RSVP may refer to a StartupTree member or an event guest, who is a user
    associated with an event who is not signed in.
    An EventRSVP object only exists within the scope of the event it is associated
    with. For example, if the same member RSVPed to two different events, these
    would be considered two different EventRSVPs.

    Currently, we do not create an EventRSVP object for every StartupTree member
    who RSVPs to an event. (However, we do create an EventRSVP object for every
    guest RSVP.) An EventRSVP object will be created for a StartupTree member in
    the following cases:
        1. There are custom RSVP questions for the event.
        2. The member checks in without first RSVPing, and there are custom
        check-in questions for the event.
        3. The member joins the waitlist for the event.
        4. The member checks in as a guest, using the email associated with
        their StartupTree account.
        5. The member RSVPs as a guest, using the email associated with
        their StartupTree account.
        6. An admin edits the affiliation or school data associated with the
        member's RSVP.
        7. An admin checks in the member without them first RSVPing, and
        there are custom check-in questions for the event.
        8. An admin checks the member in as a guest, using the email
        associated with the member's StartupTree account.
    We may decide in the future to standardize this by creating an EventRSVP
    object for every StartupTree member who RSVPs to an event.

    Attributes:
        member: a StartupMember object indicating the StartupTree member who
            RSVPed, if applicable.
        guest: a GuestRSVP object indicating the event guest who RSVPed, if
            applicable.
        rsvp_event: an Event object indicating which event was RSVPed to.
        affiliation: an integer representing whether the user is a student,
            alumni, faculty, staff, or non-affiliated.
        school: a string representing the name of the school the user is
            associated with, if they are not non-affiliated.
        major: a string representing which major the user is associated with,
            if they are a student or alumni.
        waitlisted: a boolean indicating whether the user is on the waitlist
            for the event, as opposed to actually being RSVPed.

        For StartupTree members, the affiliation, school, and major attributes
        will be taken from their StartupTree profiles unless the RSVP data is
        edited by admin.
    """
    STUDENT = 1
    ALUMNI = 2
    FACULTY = 3
    STAFF = 4
    NOT_AFFILIATED = 5
    # deprecated
    FACULTY_STAFF = 6

    RSVP_AFFILIATIONS = (
        (STUDENT, 'Student'),
        (ALUMNI, 'Alumni'),
        (FACULTY, 'Faculty'),
        (STAFF, 'Staff'),
        (NOT_AFFILIATED, 'Non-Affiliated'),
        (FACULTY_STAFF, 'Faculty/Staff'),
    )

    member = models.ForeignKey('branch.StartupMember', null=True, on_delete=models.CASCADE)
    guest = models.ForeignKey(GuestRSVP, null=True, on_delete=models.CASCADE)
    rsvp_event = models.ForeignKey('trunk.Event', null=True, on_delete=models.CASCADE)
    # only fill out for Guests, for Logged in users, use what they filled out for profiles
    affiliation = models.PositiveSmallIntegerField(
        choices=RSVP_AFFILIATIONS, null=True)
    school = models.CharField(max_length=100, null=True)
    major = models.CharField(max_length=100, null=True)

    waitlisted = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("member", "rsvp_event"), ("guest", "rsvp_event"))

    def get_affiliation_str(self) -> str:
        """
        Represents the user's affiliation, which is stored as an integer, as
        a human-readable string.
        """
        return self.RSVP_AFFILIATIONS[self.affiliation - 1][1]


class EventCheckin(models.Model):
    """
    Represents a checkin of a person (member or guest) to an event.

    member xor guest is defined (exactly one, not both, fields are defined)

    member - The person checking in (null if guest check-in)
    guest - The person checking in (null if member check-in)
    checkedin_event - The event the person is checking in.
    created - When the checkin occurred (ie when object was created).
    """
    member = models.ForeignKey('branch.StartupMember', null=True, on_delete=models.CASCADE)
    guest = models.ForeignKey(GuestRSVP, null=True, on_delete=models.CASCADE)
    checkedin_event = models.ForeignKey('trunk.event', null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)


class AcceptedRSVP(models.Model):
    """
    A record of when a user RSVPed to an event.

    Attributes:
        email: the email of the RSVPed user.
        accept_time: the time of the RSVP (unless the record has been created
            retroactively, in which case this will be the time of creation).
        event: a ForeignKey to the event that was RSVPed to.
    """
    email = models.EmailField(null=True, blank=True, default=None)
    accept_time = models.DateTimeField(auto_now_add=True)
    event = models.ForeignKey('trunk.event', null=True, on_delete=models.CASCADE)


class TeamLandingPage(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField()
    university = models.OneToOneField(University, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_team_landing_image_url,
                              null=True,
                              blank=True,
                              default=None,
                              max_length=700)


class EventManager(models.Manager):
    def get_queryset(self):
        query_set = super(EventManager, self).get_queryset()
        return query_set.filter(trashed_at__isnull=True)

    def create_event(self,
                     name,
                     description,
                     start,
                     end,
                     timezone,
                     university,
                     creator=None,
                     status=1,  # FIXME: for now, allow by default.
                     event_type=7,
                     event_board=None,
                     display_attendees=True,
                     display_applicants=True,
                     display_view_count=True,
                     display_activity_feed=True,
                     pin_to_feed=False,
                     allow_checkins=False,
                     allow_rsvp=True,
                     allow_max_rsvp=False,
                     max_rsvp=None,
                     enable_waitlist=False,
                     allow_guest_rsvp=True,
                     restrict_guest_rsvp=False,
                     show_custom_rsvp=True,
                     show_custom_checkin=False,
                     allow_guest_checkins=True,
                     restrict_guest_checkins=False,
                     free_food=False,
                     free_swag=False,
                     is_published=True,
                     is_private=False,
                     is_archived_event=False,
                     is_competition=False,
                     auto_generate_link=False,
                     is_application=False,
                     is_survey=False,
                     can_edit_questions=True,
                     can_edit_rubric=True,
                     is_saved=False,
                     send_emails_days_prior=7,
                     send_updates=True,
                     location=None,
                     map_location=None,
                     latitude=-33.868,
                     longitude=151.2195,
                     event_website=None,
                     ticket_website=None,
                     is_public_competition=False,
                     is_template_only=False,
                     allow_group_sharing = False,
                     allow_global_sharing = False,
                     auto_conference_link = False,
                     chk_google_meet_link = False,
                     chk_own_link = False,
                     input_own_link = None
                     ):
        """ Create an event """
        if description is None:
            description = ''
        if chk_google_meet_link is None:
            chk_google_meet_link = False
        if chk_own_link is None:
            chk_own_link=False
                
        event = self.create(name=name,
                            description=description,
                            start=start,
                            end=end,
                            timezone=timezone,
                            location=location,
                            map_location=map_location,
                            creator=creator,
                            status=status,
                            university=university,
                            event_type=event_type,
                            display_activity_feed=display_activity_feed,
                            pin_to_feed=pin_to_feed,
                            allow_checkins=allow_checkins,
                            allow_rsvp=allow_rsvp,
                            allow_max_rsvp=allow_max_rsvp,
                            max_rsvp=max_rsvp,
                            enable_waitlist=enable_waitlist,
                            allow_guest_rsvp=allow_guest_rsvp,
                            restrict_guest_rsvp=restrict_guest_rsvp,
                            show_custom_rsvp=show_custom_rsvp,
                            show_custom_checkin=show_custom_checkin,
                            allow_guest_checkins=allow_guest_checkins,
                            restrict_guest_checkins=restrict_guest_checkins,
                            display_attendees=display_attendees,
                            display_applicants=display_applicants,
                            display_view_count=display_view_count,
                            free_food=free_food,
                            free_swag=free_swag,
                            is_published=is_published,
                            is_private=is_private,
                            is_archived_event=is_archived_event,
                            is_competition=is_competition,
                            auto_generate_link=auto_generate_link,
                            is_application=is_application,
                            is_survey=is_survey,
                            can_edit_questions=can_edit_questions,
                            can_edit_rubric=can_edit_rubric,
                            is_saved=is_saved,
                            send_emails_days_prior=send_emails_days_prior,
                            send_updates=send_updates,
                            latitude=latitude,
                            longitude=longitude,
                            event_board=event_board,
                            event_website=event_website,
                            ticket_website=ticket_website,
                            is_public_competition=is_public_competition,
                            is_template_only=is_template_only,
                            allow_group_sharing = allow_group_sharing,
                            allow_global_sharing = allow_global_sharing,
                            auto_conference_link = auto_conference_link,
                            chk_google_meet_link = chk_google_meet_link,
                            chk_own_link = chk_own_link,
                            input_own_link = input_own_link
                            )
        return event


class TrashedEventManager(models.Manager):
    def get_queryset(self):
        query_set = super(TrashedEventManager, self).get_queryset()
        return query_set.filter(trashed_at__isnull=False)


class Event(models.Model):
    """
    Event
    """
    event_id = models.UUIDField(
        db_index=True, unique=True, default=uuid.uuid4, editable=False)
    shared_url = models.CharField(max_length=400, null=True)
    name = models.CharField(max_length=500)
    description = models.TextField()
    # if None, then admin created event
    creator = models.ForeignKey('branch.StartupMember', null=True, on_delete=models.SET_NULL)
    # status of event: 0=awaiting approval, 1=approved, 2=rejected
    PENDING_APPROVAL = 0
    APPROVED = 1
    REJECTED = 2
    EVENT_STATUS = (
        (PENDING_APPROVAL, "Pending Approval"),
        (APPROVED, "Approved"),
        (REJECTED, "Rejected")
    )
    status = models.PositiveSmallIntegerField(default=APPROVED)

    start = models.DateTimeField()  # start date and time of event
    end = models.DateTimeField()  # end date and time of event
    TIMEZONES = [(t, t) for t in pytz.common_timezones]
    timezone = models.CharField(
        max_length=100,
        choices=TIMEZONES,
        default='UTC')
    location = models.CharField(max_length=400, null=True, blank=True)
    # for Google Maps API
    map_location = models.CharField(max_length=400, null=True, blank=True)
    university = models.ForeignKey(University, on_delete=models.CASCADE)

    auto_conference_link = models.URLField(max_length=500, null=True, default=False)

    attending_users = models.ManyToManyField(
        UserEmail, related_name='attending_users')
    interested_users = models.ManyToManyField(
        UserEmail, related_name='interested_users')
    invited_users = models.ManyToManyField(
        UserEmail, blank=True, related_name='restricted')

    guest_rsvps = models.ManyToManyField(GuestRSVP, blank=True)

    # if user is archived, the user will be still in attending or invited users table,
    # and also in archived_users table.
    archived_users = models.ManyToManyField(
        UserEmail, blank=True, related_name="archived_users")

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(null=True, default=None)

    # Original size
    image = models.ImageField(upload_to=get_event_image_url,
                              null=True,
                              blank=True,
                              default=None,
                              max_length=700)
    # used for activity feed and event page.
    image_large = models.ImageField(upload_to=get_event_image_url,
                                     null=True,
                                     blank=True,
                                     default=None,
                                     max_length=700)
    # used for discover pages or mobile page
    image_medium = models.ImageField(upload_to=get_event_image_url,
                              null=True,
                              blank=True,
                              default=None,
                              max_length=700)
    # used for autocomplete
    image_small = models.ImageField(upload_to=get_event_image_url,
                              null=True,
                              blank=True,
                              default=None,
                              max_length=700)
    # Deprecated, do not assign anymore
    ratings = models.ManyToManyField(
        JudgeRating, blank=True, related_name='deprecated')

    display_activity_feed = models.BooleanField(default=True, null=False)
    auto_generate_link = models.BooleanField(default=False, null=False)
    add_my_link = models.BooleanField(default=True, null=False)
    pin_to_feed = models.BooleanField(default=False, null=False)
    featured_on_unis = models.ManyToManyField(University, related_name='shared_events')
    # is_competition is to distinguish between events and comps/apps.
    # (While is_application is to distinguish between competition and applications)
    # WARNING is_competition is true for both competitions and applications.
    # We will eventually need to rename is_competition (eg to is_competition_or_application)
    is_competition = models.BooleanField(default=False, null=False)
    is_public_competition = models.BooleanField(default=False)
    # class to manage judge relation if this is competition
    # judge_board is deprecated, use judge_boards instead
    judge_board = models.ForeignKey(JudgeBoard, null=True, blank=True, on_delete=models.CASCADE)
    judge_boards = models.ManyToManyField(JudgeBoard, related_name='board_event')
    # required for competition that have rounds
    event_board = models.ForeignKey(EventBoard, null=True, blank=True, on_delete=models.CASCADE)
    allow_rsvp = models.BooleanField(default=True, null=False)
    allow_max_rsvp = models.BooleanField(default=False, null=False)
    max_rsvp = models.PositiveIntegerField(default=None, null=True)
    enable_waitlist = models.BooleanField(default=False, null=False)
    allow_checkins = models.BooleanField(default=False, null=False)
    allow_guest_rsvp = models.BooleanField(default=True, null=False)
    restrict_guest_rsvp = models.BooleanField(default=False, null=False)
    show_custom_rsvp = models.BooleanField(default=True, null=False)
    show_custom_checkin = models.BooleanField(default=False, null=False)
    allow_guest_checkins = models.BooleanField(default=False, null=False)
    restrict_guest_checkins = models.BooleanField(default=False, null=False)
    display_attendees = models.BooleanField(default=True, null=False)
    display_applicants = models.BooleanField(default=True, null=False)
    display_view_count = models.BooleanField(default=True, null=False)
    free_food = models.BooleanField(default=False, null=False)
    free_swag = models.BooleanField(default=False, null=False)
    allow_group_sharing = models.BooleanField(default=False, null=False)
    allow_global_sharing = models.BooleanField(default=False, null=False)

    checked_in = models.ManyToManyField(EventCheckin)

    form = models.ForeignKey('forms.Form', null=True,
                             blank=True, related_name='application', on_delete=models.CASCADE)

    fb_event_id = models.CharField(max_length=256, null=True, default=None)

    organization = models.ForeignKey(
        'branch.Organization', null=True, default=None, on_delete=models.CASCADE)

    view_count = models.IntegerField(default=0, null=False)
    fb_last_updated = models.DateTimeField(null=True)
    is_published = models.BooleanField(default=True)
    is_private = models.BooleanField(default=False)
    is_archived_event = models.BooleanField(default=False)
    latitude = models.FloatField(default=0.0, null=True)
    longitude = models.FloatField(default=151.2195, null=True)
    is_applicant = models.BooleanField(default=False, null=False)
    is_saved = models.BooleanField(default=False)
    is_template_only = models.BooleanField(default=False)
    send_emails = models.BooleanField(default=True, null=False) # DEPRECATED! Use send_emails_days_prior
    send_emails_days_prior = models.IntegerField(default=0)
    send_updates = models.BooleanField(default=True, null=False)
    member_rsvp_confirmation_email = models.TextField(default=None, null=True)
    guest_rsvp_confirmation_email = models.TextField(default=None, null=True)
    reminder_email = models.TextField(default=None, null=True)
    admin = models.ManyToManyField(
        UserEmail, blank=True, related_name='event_admin')
    can_edit_questions = models.BooleanField(default=True, null=False)
    can_edit_rubric = models.BooleanField(default=True, null=False)
    industries = models.ManyToManyField('branch.Industry', blank=True)
    event_website = models.URLField(max_length=500, null=True, default=None)
    ticket_website = models.URLField(max_length=500, null=True, default=None)
    # since is_competition is already used to distingush events from apps/comp, will use this.
    is_application = models.BooleanField(default=False)
    is_survey = models.BooleanField(default=False)
    tags = models.ManyToManyField('branch.Label')

    chk_google_meet_link = models.BooleanField(default=False)
    chk_own_link = models.BooleanField(default=False)
    input_own_link = models.URLField(max_length=500, null=True, default=None)

    CAREER_FAIR = 1
    CONFERENCE = 2
    NETWORKING = 3
    PRODUCT_LAUNCH = 4
    TALK = 5
    WORKSHOP = 6
    FUNRAISING = 7
    HACKATHON = 8
    COMPETITION = 9
    APPLICATION = 10
    DEMO_DAY = 11
    OTHER = 12

    EVENT_TYPES = (
        (CAREER_FAIR, 'Career Fair'),
        (CONFERENCE, 'Conference'),
        (NETWORKING, 'Networking'),
        (PRODUCT_LAUNCH, 'Product Launch'),
        (TALK, 'Talk/Discussion'),
        (WORKSHOP, 'Workshop'),
        (FUNRAISING, 'Fundraising'),
        (HACKATHON, 'Hackathon'),
        (COMPETITION, 'Competition'),
        (APPLICATION, 'Application'),
        (DEMO_DAY, 'Demo Day'),
        (OTHER, 'Other')
    )

    event_type = models.PositiveSmallIntegerField(
        choices=EVENT_TYPES, null=True, default=None)

    trashed_at = models.DateTimeField(blank=True, null=True, default=None)

    objects = EventManager()
    trash = TrashedEventManager()

    def delete(self, trash=True):
        if not self.trashed_at and trash:
            self.trashed_at = timezone.now()
            self.save()
        else:
            super(Event, self).delete()

    def restore(self, commit=True):
        self.trashed_at = None
        if commit:
            self.save()

    def get_location(self):
        if self.location:
            return self.location
        if self.map_location:
            return self.map_location
        return None

    def get_can_edit_questions(self):
        if not self.can_edit_questions:
            return False
        num_participants = self.get_attending_users_count()
        if num_participants == 0:
            return True
        else:
            self.can_edit_questions = False
            self.save()
            return False

    def get_can_edit_rubric(self):
        if self.judge_boards.all():
            for round in range(1, (self.judge_boards.all().last().round + 1)):
                if not JudgeReview.objects.filter(event=self, judge__round=round, judge__is_archived=False)\
                        .exclude(status=JudgeReview.NOT_STARTED).exists():
                    return True
                else:
                    return False
        else:
            return True

    def add_private_user(self, user):
        self.invited_users.add(user)

    '''Deprecated & will be removed later'''

    def is_event_fb_admin(self, user_email):
        if self.organization and not user_email.is_anonymous:
            if self.organization.admin.filter(user=user_email):
                return True
            else:
                return False
        else:
            return False

    def is_event_admin(self, user_email):
        if not user_email.is_anonymous:
            if self.admin.filter(email=user_email):
                return True
        return False

    def user_is_interested(self, user_email):
        if (user_email in self.interested_users.all()):
            return True
        else:
            return False

    def user_is_checked_in(self, user_email):
        checkins = self.checked_in
        for i in checkins:
            if i.member.user == user_email:
                return True
        return False

    def get_url(self):
        return reverse('view_event_title', kwargs={
            'event_short_id': self.get_short_event_id(),
            'title': self.get_url_title()})

    def get_full_url(self):
        path = self.get_url()
        host = self.university.site.domain
        return "https://{0}{1}".format(host, path)

    def get_shared_url(self):
        return "/event/shared/{0}".format(self.shared_url)

    @property
    def rsvp_url(self):
        if self.is_application:
            return reverse('application-form', kwargs={"event_id": self.event_id})
        elif self.is_competition:
            return reverse('competition-form', kwargs={"event_id": self.event_id})
        elif self.is_survey:
            return reverse('survey', kwargs={"event_id": self.event_id})
        elif self.allow_rsvp:
            return reverse('accept_event_invite', kwargs={"event_id": self.id})
        return None

    def get_rsvp_url(self):
        if self.is_application:
            return reverse('application-form', kwargs={"event_id": self.event_id})
        elif self.is_competition:
            return reverse('competition-form', kwargs={"event_id": self.event_id})
        elif self.is_survey:
            return reverse('survey', kwargs={"event_id": self.event_id})
        elif self.allow_rsvp:
            return reverse('accept_event_invite', kwargs={"event_id": self.id})
        return None

    @property
    def action_word(self):
        if self.is_application:
            return "Apply"
        elif self.is_competition:
            return "Enter"
        elif self.is_survey:
            return "Take Survey"
        elif self.university.short_name == "cornell":
            return "Register"
        return "RSVP"

    def get_image_serializer(self):
        if self.get_image() == None:
            return '/static/img/event_banner.png'
        else:
            return self.get_image()

    @property
    def image_url(self):
        if self.image:
            return get_media_url() + str(self.image)
        return None

    def get_image(self):
        if self.image:
            return get_media_url() + str(self.image)
        return None

    def get_image_large(self):
        if self.image_large:
            return get_media_url() + str(self.image_large)
        return None

    def get_image_medium(self):
        if self.image_medium:
            return get_media_url() + str(self.image_medium)
        return None

    def get_image_small(self):
        if self.image_small:
            return get_media_url() + str(self.image_small)
        return None

    @property
    def get_type(self):
        """Returns None if not applicable. """
        if self.is_application:
            return self.university.style.application_word.capitalize()
        elif self.is_competition:
            return "Competition"
        elif self.is_survey:
            return "Survey"
        else:
            return dict(Event.EVENT_TYPES).get(self.event_type)

    def get_tags(self):
        if self.university.style.alphabetize_tags:
            return self.tags.all().order_by('title')
        return self.tags.all()

    @property
    # description has HTML tags around it, so this slices it to just the content
    def stripped_description(self):
        d = str(self.description)
        while d.find('<') != -1:
            i = d.find('<')
            j = d.find('>')
            d = d[:i] + d[j + 1:]
        return d

    def get_description(self):
        return bleach_clean(self.description)

    def is_one_day_event(self):
        try:
            start = timezone.localtime(self.start, pytz.timezone(self.timezone))
        except ValueError:
            try:
                start = timezone.make_aware(self.start, pytz.timezone(self.timezone))
            except ValueError:
                start = self.start

        try:
            end = timezone.localtime(self.end, pytz.timezone(self.timezone))
        except ValueError:
            try:
                end = timezone.make_aware(self.end, pytz.timezone(self.timezone))
            except ValueError:
                end = self.end
        start = start.date()
        end = end.date()
        return start == end

    def get_attending_user_without_archive(self):
        """
        Returns a QuerySet of StartupTree members who RSVPed to the event,
        who are not archived.
        """
        return self.attending_users.select_related('startup_member').filter(startup_member__is_archived=False)\
            .exclude(id__in=self.archived_users.all().values_list('id', flat=True))

    def get_guest_rsvps_without_archive(self):
        """
        Returns a QuerySet of guest users who RSVPed to the event, who are
        not archived.
        """
        return self.guest_rsvps.filter(is_archived=False)

    def get_guest_rsvps_without_archive_count(self) -> int:
        """
        Returns the number of guest users who RSVPed to the event, who are
        not archived.
        """
        return self.get_guest_rsvps_without_archive().count()

    def get_attending_users_count(self) -> int:
        """
        Returns the total number of StartupTree members and guest users who
        RSVPed to the event, who are not archived.
        """
        cnt = self.get_attending_user_without_archive().count()
        cnt += self.get_guest_rsvps_without_archive().count()
        if self.is_public_competition or self.anonymousapplicant_set.filter(is_applied=True).count() > 0:
            cnt += self.anonymousapplicant_set.filter(is_applied=True, is_archived=False).count()
        return cnt

    def get_member_applicants(self):
        return self.memberapplicant_set.select_related('member', 'member__user').filter(is_applied=True).exclude(
            member__user_id__in=self.archived_users.all().values_list('id', flat=True)).distinct()

    def get_applicant_count(self):
        cnt = self.get_member_applicants().count()
        if self.is_public_competition or self.anonymousapplicant_set.filter(is_applied=True).count() > 0:
            cnt += self.anonymousapplicant_set.filter(is_applied=True, is_archived=False).count()
        return cnt

    def get_anonymous_applicants(self):
        return self.anonymousapplicant_set.filter(is_applied=True, is_archived=False)

    def get_total_waitlisted_count(self):
        return self.eventrsvp_set.filter(waitlisted=True).count()

    def get_waitlisted_users(self):
        user_ids = EventRSVP.objects.select_related('member') \
            .filter(rsvp_event=self, waitlisted=True, member__isnull=False) \
            .values_list('member__user_id', flat=True)
        return UserEmail.objects.filter(id__in=user_ids)

    def get_waitlisted_guests(self):
        return GuestRSVP.objects.filter(eventrsvp__rsvp_event=self, eventrsvp__waitlisted=True)

    def is_accepting_rsvp(self) -> bool:
        """
        Determines whether or not the event is currently accepting RSVPs by
        evaluating whether the event allows RSVPs and whether there is an
        RSVP cap that has been met.
        """
        num_participants = self.get_attending_users_count()
        return self.allow_rsvp and ((self.allow_max_rsvp and num_participants < self.max_rsvp) or not self.allow_max_rsvp)


    """
    Be aware of timezone issues...
    """

    def not_started(self):
        try:
            start = timezone.localtime(self.start, pytz.timezone(self.timezone))
        except ValueError:
            try:
                start = timezone.make_aware(self.start, pytz.timezone(self.timezone))
            except ValueError:
                start = self.start
        return start > timezone.now()

    def not_ended(self):
        try:
            end = timezone.localtime(self.end, pytz.timezone(self.timezone))
        except ValueError:
            try:
                end = timezone.make_aware(self.end, pytz.timezone(self.timezone))
            except ValueError:
                end = self.end
        return end > timezone.now()

    # deprecated, use deadline_passed method of JudgeBoard model instead
    def judge_not_ended(self):
        try:
            if self.judge_board.deadline is None:
                return True
        except:
            return True
        try:
            end = timezone.localtime(self.judge_board.deadline, pytz.timezone(self.timezone))
        except ValueError:
            try:
                end = timezone.make_aware(self.judge_board.deadline, pytz.timezone(self.timezone))
            except ValueError:
                end = self.judge_board.deadline
        return end > timezone.now()

    def localized_time(self):
        try:
            start = timezone.localtime(self.start, pytz.timezone(self.timezone))
        except ValueError:
            try:
                start = timezone.make_aware(self.start, pytz.timezone(self.timezone))
            except ValueError:
                start = self.start

        try:
            end = timezone.localtime(self.end, pytz.timezone(self.timezone))
        except ValueError:
            try:
                end = timezone.make_aware(self.end, pytz.timezone(self.timezone))
            except ValueError:
                end = self.end

        return start, end

    def calc_timeuntil(self):
        start, end = self.localized_time()
        start = timezone.now()
        timeuntil = end - start
        timeuntil = timeuntil.days + 1

        return timeuntil

    def localize(self, datetime, tz=None):
        if tz:
            result = datetime.astimezone(pytz.timezone(tz))
        else:
            try:
                result = timezone.localtime(datetime, pytz.timezone(self.timezone))
            except ValueError:
                try:
                    result = timezone.make_aware(datetime, pytz.timezone(self.timezone))
                except ValueError:
                    result = datetime
        return result

    @property
    def formatted_start_date(self):
        start = self.localize(self.start)
        return start.strftime("%A, %B %d, %Y")

    @property
    def formatted_end_date(self):
        end = self.localize(self.end)
        return end.strftime("%A, %B %d, %Y")

    @property
    def formatted_start_time(self):
        start = self.localize(self.start)
        return start.strftime("%-I:%M %p")

    @property
    def formatted_end_time(self):
        end = self.localize(self.end)
        return end.strftime("%-I:%M %p")

    def get_formatted_start_datetime(self, tz=None):
        start = self.localize(self.start, tz)
        return start.strftime("%A, %B %d, %Y at %-I:%M %p")

    def get_formatted_end_datetime(self, tz=None):
        end = self.localize(self.end, tz)
        return end.strftime("%A, %B %d, %Y at %-I:%M %p")

    def get_formatted_start_month_day(self):
        start = self.localize(self.start)
        return start.strftime("%B %d, %Y")

    def get_formatted_end_month_day(self):
        end = self.localize(self.end)
        return end.strftime("%B %d, %Y")

    def get_formatted_start_all(self):
        start = self.localize(self.start)
        return start.strftime("%m/%d/%Y %-I:%M %p")

    def get_formatted_end_all(self):
        end = self.localize(self.end)
        return end.strftime("%m/%d/%Y %-I:%M %p")

    """
    below are used for edit/manage events forms
    """

    def get_edit_start_date(self):
        start = self.localize(self.start)
        return start.strftime('%m/%d/%Y')

    def get_edit_end_date(self):
        end = self.localize(self.end)
        return end.strftime('%m/%d/%Y')

    def get_edit_start_time(self):
        start = self.localize(self.start)
        return start.strftime('%I:%M %p')

    def get_edit_end_time(self):
        end = self.localize(self.end)
        return end.strftime('%I:%M %p')

    def get_formatted_date_for_display(self):
        if self.start.date() != self.end.date():
            return self.get_formatted_start_month_day() + " - " + self.get_formatted_end_month_day()
        else:
            start = self.localize(self.start)
            return start.strftime("%A, %B %d from %-I:%M %p") + " - " + \
                   self.formatted_end_time + " ({0})".format(self.timezone)

    def get_formatted_start_end_date_for_display(self):
        if self.start.date() == self.end.date():
            start = self.localize(self.start)
            return start.strftime("%b %d %Y")
        else:
            start = self.localize(self.start)
            end = self.localize(self.end)
            date = start.strftime("%b %d") + " - " + end.strftime("%b %d %Y")
            return date

    def get_timezone(self):
        return self.timezone

    def is_attending(self, user_email):
        if user_email is None:
            return False
        return self.attending_users.filter(id=user_email.id).exists()

    def get_allow_rsvp(self):
        return self.allow_rsvp

    def get_allow_max_rsvp(self):
        return self.allow_max_rsvp

    def get_allow_checkins(self):
        return self.allow_checkins

    def get_allow_guest_rsvp(self):
        return self.allow_guest_rsvp

    def get_restrict_guest_rsvp(self):
        return self.restrict_guest_rsvp

    def get_show_custom_rsvp(self):
        return self.show_custom_rsvp

    def get_show_custom_checkin(self):
        return self.show_custom_checkin

    def get_allow_guest_checkins(self):
        return self.allow_guest_checkins

    def get_restrict_guest_checkins(self):
        return self.restrict_guest_checkins

    def get_is_published(self):
        return self.is_published

    def get_is_private(self):
        return self.is_private

    def publish(self, uni_clerk):
        self.is_published = True
        self.save()
        uni_clerk.record_event_updated(self)

    def unpublish(self, uni_clerk):
        self.is_published = False
        self.save()
        uni_clerk.record_event_unpublished(self)

    def get_is_archived(self):
        return self.is_archived_event

    def archive(self, uni_clerk):
        self.is_archived_event = True
        self.save()
        uni_clerk.record_event_updated(self)

    def unarchive(self, uni_clerk):
        self.is_archived_event = False
        self.save()
        uni_clerk.record_event_updated(self)

    def get_free_food(self):
        return self.free_food

    def get_free_swag(self):
        return self.free_swag

    def get_display_attendees(self):
        return self.display_attendees

    def get_display_view_count(self):
        return self.display_view_count

    def get_display_applicants(self):
        return self.display_applicants

    def get_is_saved(self):
        return self.is_saved

    def days_to_event(self):
        delta = self.start - timezone.now()
        return delta.days

    def days_to_event_end(self):
        delta = self.end - timezone.now()
        return delta.days

    def get_event_type(self):
        return self.event_type

    def get_event_website(self):
        return self.event_website

    def get_ticket_website(self):
        return self.ticket_website

    def get_creator(self):
        return self.creator

    def get_status(self):
        return self.status

    def get_groups(self):
        return self.tags

    def get_event_id(self):
        return str(self.event_id)

    def get_short_event_id(self):
        return shortuuid.encode(self.event_id)

    def get_title_nospace(self):
        return self.name.replace(' ', '-')

    def get_url_title(self):
        replace_space = self.get_title_nospace()
        return url_quote(replace_space)

    def get_participant_word(self):
        if self.is_competition: # is competition or application
            return "participant" if not self.is_application else "applicant"
        else:
            raise ValueError('event must be competition or application')


class EventsBlacklist(models.Model):
    """
    Blacklist of events which prevents future imports from facebook. Any event edited/removed on StartupTree
    will be added to this table which prevents future updates from facebook. This will ensure that deleted
    events are not reimported, and changes to events on the platform are not overridden.
    """

    fb_event_id = models.CharField(max_length=256, null=True, default=None)


class DataReportManager(models.Manager):
    def create_report(self,
                      name,
                      university,
                      semester,
                      year,
                      deadline=None,
                      is_template=False):
        report = self.create(name=name,
                             university=university,
                             semester=semester,
                             year=year,
                             deadline=deadline,
                             is_template=is_template)
        return report


class DataReport(models.Model):
    ALL = 0
    FALL = 1
    SPRING = 2
    SUMMER = 3
    WINTER = 4
    SEMESTERS = (
        (ALL, 'All Semesters'),
        (FALL, 'Fall'),
        (SPRING, 'Spring'),
        (SUMMER, 'Summer'),
        (WINTER, 'Winter')
    )

    r_id = models.UUIDField(
        db_index=True, unique=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=500)
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    semester = models.PositiveSmallIntegerField(choices=SEMESTERS, default=ALL)
    year = models.PositiveIntegerField(default=None)
    deadline = models.DateField(null=True, default=None)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(null=True, default=None)
    admins_entered = models.ManyToManyField(UniversityStaff,
        related_name='completed_report')
    admins_in_progress = models.ManyToManyField(UniversityStaff,
        related_name='in_progress_report')
    is_template = models.BooleanField(default=False)
    is_archived = models.BooleanField(default=False)
    form = models.ForeignKey('forms.Form', null=True,
                             blank=True, related_name='report', on_delete=models.CASCADE)
    tags = models.ManyToManyField('branch.Label')

    objects = DataReportManager()

    def get_semester(self):
        return self.SEMESTERS[self.semester][1]


class StartupTreeStaff(models.Model):
    """
    StartupTree Staffs who are doing customer support.
    Each staff will have admin access to universities they are assigned to.
    However, note that the staff account is separate fromtheir actual StartupTree Profile.
     Staff account should not have its own profile and should be @startuptree.co email address
    """
    staff = models.OneToOneField(UserEmail, on_delete=models.CASCADE)
    universities = models.ManyToManyField(University)


class UniversityGroup(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class UniversitySubscription(models.Model):
    """
    Basic subscription levels.
    For global feature settings and individual feature turn on/off,
    please refer to UniversityStyle
    """
    university = models.OneToOneField(University, on_delete=models.CASCADE)
    is_resource = models.BooleanField(default=True)  # tier 1
    is_track = models.BooleanField(default=True)  # tier 2
    is_grow = models.BooleanField(default=True)  # tier 3
    is_engage = models.BooleanField(default=True)  # tier 3
    # mentorship_standalone = models.BooleanField(default=False)  # tier 3
    # competition_application_standalone = models.BooleanField(default=False)  # tier 3
    is_team_mentorship = models.BooleanField(default=False) # New Team Mentorship Module
    enable_shibboleth = models.BooleanField(default=False)
    is_private_open = models.BooleanField(default=False)  # in case private platform is more of isolated platform, open to public, but no global
    is_private_allow_signup = models.BooleanField(default=False) # private platform that allows users manual signup.
    is_api_enabled = models.BooleanField(default=False)

    is_membership_fee_enabled = models.BooleanField(default=False)

    university_group = models.ForeignKey('UniversityGroup', null=True, default=None, blank=True, on_delete=models.SET_NULL)  # in case private platform that belongs to a group.

    enable_custom_emailname = models.BooleanField(default=False)
    custom_emailname = models.CharField(default='', blank=True, max_length=100)


class UniversityExportHistory(models.Model):
    """
    A UniversityExportHistory represents a record of an export.

    Specifications:

        Basic Info:
            The university that the export belongs to, and the kind of export.

        Additional Info:
            Fields derived from what users select when making export, such as the
            date range, whether place-holder users should be included,
            selected tags, whether non-tagged entities should be included in the export.

            Note: End Date is meant to be inclusive.

            Additionally, the exclude_admin_only_startup_custom_questions field is whether admin-only
            custom questions should be excluded for Venture/Project sheets (primary use case is Temple's request to have
            admin-only field excluded on exports performed by admin's that don't have the tech
            commercialization tag).


        Export Meta-data:
            Data about the export, like when it was made, whether the export is in progress or ready,
            and a deprecated field specifying the "extra-info" caption.

        The exported file:
            Stored in UniversityExportHistory.data

    About Export Category:
        See specification above UniversityExportHistory.EXPORT_CATEGORY

    About include_placeholder_members:
        Whether auto-generated startup members (with null `user` field) should be included in the export.
        This only applies for exports where users are involved.

    About tags and include_non_tagged:

        history.tags does NOT always equate to what the user selected on export modal.

        Backend Case 1: history.tags is empty => ALL profiles are included in export.
        Backend Case 2: history.tags is non-empty:
            All profiles sharing a tag with history.tags is included.
            Profiles with no tag are included if and only if history.include_non_tagged


        How to determine (from front end) how to set history.tags and history.include_non_tagged:

        Note: A tag-restricted admin is formaally defined in trunk.perms.is_tag_restricted_admin
            (At time of writing, it is any non-super admin on a platform that
            "Only allow admins to manage profiles with which they share a tag".)

        Frontend Case 1: If any admin (tag-restricted or not) chooses any tags in the export modal, then:
            ONLY the profiles with the selected tags are part of the export.
            Profiles with no tag are NOT included in the export.
            Backend Settings:
                history.include_non_tagged = False
                history.tags = non-empty selection from front-end

        Frontend Case 2: If a non-tag-restricted admin does not choose any tags in the export modal, then:
            All profiles are part of the export.
            Backend Settings:
                history.include_non_tagged = True
                history.tags = empty (ENCODES that all profiles are included)

        Frontend Case 3: If a tag-restricted admin does not choose any tags in the export modal:
            Profiles that HAVE tags that SHARE a tag with the admin ARE included in the export
            Profiles that HAVE tags that DON’T SHARE a tag with the admin ARE NOT included in the export
            Profiles that DON’T have any tag ARE included in the export.
            Backend Settings:
                history.include_non_tagged = True
                history.tags = the non-empty set of tags that the admin has

        Frontend Corner Case 4: Tag-restricted admin has no tags at all.
            In this scenario, we would only "want" ONLY untagged profiles to be included in the export.
            IMPORTANT:
                In this scenario, all profiles are included in the export because history.tags is empty.
                This VIOLATES what we "want". To fix this will require a significant refactor of oru code base
                which we may decide to do in the future.

        Frontend Corner Case 5: University has no tags at all.
            In this scenario, we would want all profiles to be included in that export.
            In that scenario, it is impossible for any admin (tag restricted or not) to select any tags,
            so all non-tagged profiles are automatically included (per Frontend Case 2 and Frontend Case 3),
            so no additional logic is needed to be implemented for this corner scenario.

    About `extra_info`:

        Semantically, "extra info" is a helpful caption about the export that helps the user
        when looking at a list of past exports. (ie Admin Panel -> Metrics -> Export)

        Historically, this was hardcoded for each export history record as UniversityExportHistory.extra_info.

        That `extra_info` field is now deprecated.

        Instead, "extra info" is procedurally generated via UniversityExportHistory.__generate_extra_info().

        To get the "extra info" of an export history object, ALWAYS call UniversityExportHistory.get_extra_info()
        which will first check if a hardcoded caption exists and returns it if it exists, otherwise returns the
        procedurally generated caption.


    About User Funding Export vs General User Export:
        User Funding Export and General User Export are almost the same, except User Funding Export contains extra
        columns with funding data.

        General User Export is found in Admin Panel -> People -> Users -> Generate Export
        User Funding Export is found in Admin Panel -> Metrics -> Export -> User Funding Export
    """

    GENERAL = 1
    USER_FUNDING = 2
    JUDGE_RUBRICS = 3
    MENTORSHIP = 4
    TEAM_MENTORSHIP = 5
    USER_GENERAL = 6

    """
    String labels of EXPORT_CATEGORY are INTERNAL USE ONLY, because the word "Mentorship" is fixed.
    For the string label that is for public use (ie uses the correct mentorship word),
    use self.public_category_str or UniversityExportHistory.__make_public_category_str
    """
    EXPORT_CATEGORY = (
        (GENERAL, 'General'),
        (USER_FUNDING, "User Funding"),
        (JUDGE_RUBRICS, "Judge Rubrics"),
        (MENTORSHIP, "Mentorship"),
        (TEAM_MENTORSHIP, "Team Mentorship"),
        (USER_GENERAL, "User")
    )

    # Basic Info
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    category = models.PositiveSmallIntegerField(default=GENERAL, choices=EXPORT_CATEGORY)

    # Additional Info
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)  # Inclusive
    tags = models.ManyToManyField('branch.Label')
    include_non_tagged = models.BooleanField(null=True)
    include_placeholder_members = models.BooleanField(null=True)
    exclude_admin_only_startup_custom_questions = models.BooleanField(default=False)  # Temple Customization
    competition = models.ForeignKey(Event, null=True, on_delete=models.CASCADE)  # for JudgeReview exports only.

    # Export Meta-data
    datetime = models.DateTimeField(auto_now_add=True)
    in_progress = models.NullBooleanField(default=True)
    extra_info = models.CharField(null=True, default=None, max_length=755)  # Deprecated. Use get_extra_info instead

    # The Exported File
    data = models.BinaryField()

    def public_category_str(self):
        """ Returns string version of export category, suitable for public use
        (because correct mentorship word is used). """
        return UniversityExportHistory.__make_public_category_str(self.category, self.university)

    def get_extra_info(self):
        """ Returns a helpful caption about the export that helps the user when looking at a list of past exports.
        Returns None if extra info is not needed. """
        if self.extra_info:
            return self.extra_info
        else:
            return self.__generate_extra_info()

    def __generate_extra_info(self):
        """
        If start and end dates are supplied, returns a string that can serve as the extra-info caption.
        Eg: "General user data from 02/17/2015 - 05/15/2020"
        """
        if self.start_date and self.end_date:
            data_fmt = '%m/%d/%Y'
            category_str = UniversityExportHistory.__make_public_category_str(self.category, self.university)
            start_date_str = self.start_date.strftime(data_fmt)
            end_date_str = self.end_date.strftime(data_fmt)
            return f'{category_str} data from {start_date_str} - {end_date_str}'
        elif self.category == UniversityExportHistory.JUDGE_RUBRICS and self.competition:
            return self.competition.name
        else:
            return None

    @staticmethod
    def __make_public_category_str(category: int, uni: University):
        """ Returns string version of export category, suitable for public use
        (because correct mentorship word is used). """
        if category == UniversityExportHistory.MENTORSHIP:
            return uni.universitystyle_set.first().mentorship_word.capitalize()
        elif category == UniversityExportHistory.TEAM_MENTORSHIP:
            return 'Team ' + uni.universitystyle_set.first().mentorship_word.capitalize()
        else:
            return UniversityExportHistory.__internal_category_str(category)

    @staticmethod
    def __internal_category_str(category: int):
        """ Returns string version of export category.
        Internal Use Only (because mentorship word changes depending on uni). """
        return dict(UniversityExportHistory.EXPORT_CATEGORY)[category]

    def generate_filename(self):
        filename = f"{self.university.get_program_name()} {self.datetime} export_{self.public_category_str()}.xlsx"
        return filter_ascii(filename)


class UniversityEmailDomainManager(models.Manager):
    def add_domain(self, university, domain, type):
        if not isinstance(university, University):
            return None
        if self.filter(university_id=university.id, domain=domain, type=type).exists():
            return self.get(university_id=university.id, domain=domain, type=type)
        return self.create(university_id=university.id, domain=domain, type=type)


class UniversityEmailDomain(models.Model):
    MEMBER = 1
    GUEST = 2

    DOMAIN_TYPES = (
        (MEMBER, "Member"),
        (GUEST, "Guest")
    )

    university = models.ForeignKey('University', on_delete=models.CASCADE)
    domain = models.URLField()
    type = models.PositiveSmallIntegerField(
        choices=DOMAIN_TYPES, default=MEMBER)

    objects = UniversityEmailDomainManager()


class MailChimpAPIManager(models.Manager):
    def create_apikey(self,
                      university,
                      api_key,
                      server_code,
                      added_by,
                      api_worked,
                      ):
        mailchimp = self.create(
            university=university,
            api_key=api_key,
            server_code=server_code,
            added_by=added_by,
            api_worked=api_worked
        )
        return mailchimp

    def update_apikey(self,
                      university,
                      api_key,
                      server_code,
                      added_by,
                      api_worked,
                      ):
        mailchimp = self.get(university=university)
        mailchimp.api_key = api_key
        mailchimp.server_code = server_code
        mailchimp.added_by = added_by
        mailchimp.api_worked = api_worked
        mailchimp.save()
        return mailchimp


class UniversityMailChimpAPI(models.Model):
    university = models.OneToOneField('University', on_delete=models.CASCADE)
    api_key = models.TextField(null=True, default=None)
    server_code = models.TextField(null=True, default=None)
    added_by = models.ForeignKey('branch.StartupMember', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)  # just for internal record
    api_worked = models.BooleanField(default=None)

    objects = MailChimpAPIManager()



class MailChimpAudienceManager(models.Manager):
    def create_audience(self,
                      mail_chimp,
                      audience_short_name,
                      audience_id,

                      ):
        mailchimp = self.create(
            mail_chimp=mail_chimp,
            audience_short_name=audience_short_name,
            audience_id=audience_id
        )
        return mailchimp

    def add_members(self,
                      mail_chimp,
                      audience_id,
                      members
                      ):
        mailchimp = self.get(mail_chimp=mail_chimp,audience_id = audience_id )
        for member in members:
            mailchimp.members.add(member)

        return mailchimp

    def add_member(self,
                      mail_chimp,
                      audience_id,
                      member
                      ):
        mailchimp = self.get(mail_chimp=mail_chimp,audience_id = audience_id )
        mailchimp.members.add(member)

        return mailchimp

class MailChimpAudience(models.Model):
    mail_chimp = models.ForeignKey('UniversityMailChimpAPI', on_delete=models.CASCADE)
    audience_short_name = models.CharField(max_length=50, null=True, default=None)
    audience_id = models.CharField(max_length=50, null=True, default=None)
    members = models.ManyToManyField('branch.StartupMember', related_name='mailchimp_users')
    timestamp = models.DateTimeField(auto_now_add=True)  # just for internal record

    objects = MailChimpAudienceManager()
class CustomVariableManager(models.Manager):
    def get_all(self):
        return self.all()


class CustomVariable(models.Model):
    """
    display_name is responsible for description
    custom_variable is actual variable name
    custom_variable starts and ends with underscore(_)
    to load data from fixtures, run this command,
    python manage.py loaddata custom_variables.json
    """


    display_name = models.CharField(max_length=100, blank=True,
                                        null=True, default=None)

    custom_variable = models.CharField(max_length=100, blank=True,
                                        null=True, default=None)

    objects = CustomVariableManager()
