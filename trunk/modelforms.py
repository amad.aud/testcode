from django import forms
from StartupTree.loggers import app_logger
from trunk.models import University, School, UniversityStyle, Event, UniversitySubscription, UniversityGroup


class UniversityForm(forms.ModelForm):
    class Meta:
        model = University
        fields = ['name', 'short_name',
                  'is_private_platform', 'is_none_university']
        labels = {
            'name': "University Name",
            'short_name': "StartupTree domain",
            'is_private_platform': "Set Platform Priavte Closed community",
            'is_none_university': "Check if this is not university community."
        }
        help_texts = {
            'short_name': "StartupTree domain is a sub-domain name \
                for your university. For example \
                if you enter myuni, your university website's domain will be \
                myuni.startuptree.co - you may be able to change this to \
                a custom domain later on."
        }

    def clean(self):
        # self.cleaned_data = super(UniversityForm, self).clean()
        self.uni_exists = False
        name = self.cleaned_data['name']
        short_name = self.cleaned_data['short_name']
        if short_name.lower() == "www" or short_name.lower() == "mail"\
                or short_name.lower() == 'static':
            self.add_error('name', "invalid short names.")
        try:
            uni = University.objects.get(name__iexact=name)
            if uni.site is not None:
                self.add_error('name',
                               "A University {0} already exists!".format(name))
            else:
                self.uni_exists = True
        except University.DoesNotExist:
            pass
        try:
            uni = University.objects.get(short_name__iexact=short_name)
            if uni.site is not None:
                self.add_error('short_name',
                               "A short name already exists!".format(short_name))
            else:
                self.uni_exists = True
        except University.DoesNotExist:
            pass
        return self.cleaned_data

    def save(self):
        name = self.cleaned_data['name']
        short_name = self.cleaned_data['short_name']
        is_private_platform = self.cleaned_data['is_private_platform']
        is_none_university = self.cleaned_data['is_none_university']
        university = \
            University.objects.create_university(
                name, short_name, uni_exists=self.uni_exists,
                is_private_platform=is_private_platform,
                is_none_university=is_none_university
            )
        UniversitySubscription.objects.create(university=university)
        return university


class ImportForm(forms.Form):
    file = forms.FileField()


class UniversityStyleForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['timezone', 'uni_website', 'eship_website', 'uni_blog']
        labels = {
            'timezone': 'Default Timezone',
            'uni_website': 'University Website',
            'eship_website': 'Eship Program Website',
            'uni_blog': 'Blog'
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityStyleForm, self).__init__(*args, **kwargs)
        self.fields['uni_website'].widget.attrs['class'] = 'uk-width-1-2'
        self.fields['eship_website'].widget.attrs['class'] = 'uk-width-1-2'
        self.fields['uni_blog'].widget.attrs['class'] = 'uk-width-1-2'
        self.fields['uni_website'].widget.attrs['placeholder'] = \
            'http://www.example.com'
        self.fields['eship_website'].widget.attrs['placeholder'] = \
            'http://www.example.com'
        self.fields['uni_blog'].widget.attrs['placeholder'] = \
            'http://www.example.com'


class UniversityFeedForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['feed', 'include_mentorship_connections', 'include_self_global']
        labels = {
            'feed': 'Activity Feed',
            'include_mentorship_connections': 'Mentorship Connections',
            'include_self_global': 'Global Feed Data'
        }
        help_texts = {
            'feed': "What you would like displayed on your StartupTree Community Page.",
            'include_mentorship_connections': "Whether mentorship connection updates \
            from your platform should be shown in the activity feed.",
            'include_self_global': "Whether activity from the current platform should \
            appear in your Global feed. This affects the current platform only."
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityFeedForm, self).__init__(*args, **kwargs)


class UniversityGlobal1Form(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['enable_global', 'enable_global_people',
                  'enable_global_events', 'enable_global_jobs',
                  'enable_global_discussion', 'enable_global_startups']
        labels = {
            'enable_global': 'Enable Global Activity Feed',
            'enable_global_people': 'Enable Global People',
            'enable_global_events': 'Enable Global Events',
            'enable_global_jobs': 'Enable Global Jobs',
            'enable_global_discussion': 'Enable Global Forum',
            'enable_global_startups': 'Enable Global Projects/Ventures'
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityGlobal1Form, self).__init__(*args, **kwargs)


class UniversityGlobal2Form(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['default_feed_view', 'default_people_view',
                  'default_events_view', 'default_jobs_view',
                  'default_discussion_view', 'default_forum_sort_view',
                  'default_startups_view']
        labels = {
            'default_feed_view': 'Default Feed View',
            'default_people_view': 'Default People View',
            'default_events_view': 'Default Events View',
            'default_jobs_view': 'Default Jobs View',
            'default_discussion_view': 'Default Forum View',
            'default_forum_sort_view': 'Default Forum Sort Option',
            'default_startups_view': 'Default Projects/Ventures View'
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        is_group = kwargs.get('is_group')
        if is_group is not None:
            del kwargs['is_group']
        super(UniversityGlobal2Form, self).__init__(*args, **kwargs)
        if not is_group:
            for field in self.fields:
                self.fields[field].choices = list(self.fields[field].choices)[:3]


class UniversityGroupGlobalForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['enable_group_global_activity', 'enable_group_global_people',
                  'enable_group_global_events', 'enable_group_global_jobs',
                  'enable_group_global_discussion', 'enable_group_global_startups']
        labels = {
            'enable_group_global_activity': 'Enable Group Activity Feed',
            'enable_group_global_people': 'Enable Group People',
            'enable_group_global_events': 'Enable Group Events',
            'enable_group_global_jobs': 'Enable Group Jobs',
            'enable_group_global_discussion': 'Enable Group Forum',
            'enable_group_global_startups': 'Enable Group Projects/Ventures'
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityGroupGlobalForm, self).__init__(*args, **kwargs)


class UniversityPageForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['display_ventures', 'display_projects',
                  'display_mentors', 'display_investors',
                  'display_roadmap', 'display_forum',
                  'display_deals',
                  'display_jobs', 'display_events',
                  'display_applications', 'display_competitions',
                  'event_discovery_view',
                  'display_calendar_users', 'display_calendar_admin',
                  'display_team_mentors',
                  'default_team_mentors_view',
        ]

        labels = {
            'display_ventures': 'Display Ventures',
            'display_projects': 'Display Projects',
            'display_mentors': 'Display Mentors',
            'display_investors': 'Display Investors',
            'display_roadmap': 'Display Roadmap',
            'display_forum': 'Display Forum',
            'display_deals': 'Display Deals',
            'display_jobs': 'Display Jobs',
            'display_events': 'Display Events',
            'display_applications': 'Display Applications',
            'display_competitions': 'Display Competitions',
            'event_discovery_view': 'Default Event Discovery View',
            'display_calendar_users': 'Display Event Calendar To Users',
            'display_calendar_admin': 'Display Event Calendar To Admins',
            'display_team_mentors': 'Display Team Mentors',
            'default_team_mentors_view': 'Default Team Mentors View',
        }
        help_texts = {
            'display_calendar_users': "Displays the calendar on Event Discovery.",
            'display_calendar_admin': "Displays the calendar on Manage Events.",
            'default_team_mentors_view': "Only applies when 'Display Team Mentors' is \
            set to 'Discovery and Landing Page'. "
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityPageForm, self).__init__(*args, **kwargs)


class UniversityEmailForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['enable_event_reminders', 'send_platform_wide_deadline_reminders',
                  'send_rsvp_reminders', 'send_new_user_notis',
                  'mentor_request_noti', 'meeting_request_noti',
                  'custom_questions_noti', 'community_forum_noti']
        labels = {
            'enable_event_reminders': 'Event, Competition, and Application Reminder Emails',
            'send_platform_wide_deadline_reminders': 'Additional Competition and Application Deadline Reminders',
            'send_rsvp_reminders': 'Event RSVP Reminder Emails',
            'send_new_user_notis': 'New User Notification Emails',
            'mentor_request_noti': 'Mentor Request Notification Emails',
            'meeting_request_noti': 'Meeting Request Notification Emails',
            'custom_questions_noti': 'New Custom Field Notification Emails',
            'community_forum_noti': 'Community Forum Group Emails'
        }
        help_texts = {
            'enable_event_reminders': 'Ability for a reminder email to be sent to all \
            users on the platform 24 hours before the event, competition, or application \
            deadline.',
            'send_platform_wide_deadline_reminders': 'Ability for an admin to enable a reminder email to be sent to all \
            users on the platform at a specified number of days before the deadline of a competition or application.',
            'send_rsvp_reminders': 'Automatically send a reminder email to users who \
            have RSVPed to an event 24 hours before the event begins.',
            'send_new_user_notis': 'Admins automatically receive an email notification \
            when a new user joins the platform.',
            'mentor_request_noti': 'Admins automatically receive an email notification \
            when a user requests a mentor.',
            'meeting_request_noti': 'Admins automatically receive an email notification \
            when a user requests a mentorship meeting.',
            'custom_questions_noti': 'All affected users on the platform automatically \
            receive an email notification when new custom fields are added under Track > \
            Custom Data, in any category.',
            'community_forum_noti': "All users on the platform are notified when there is \
            a new post in the current platform's Community Forum Group."
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityEmailForm, self).__init__(*args, **kwargs)


class UniversityPermissionForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['require_startup_approval', 'require_meeting_approval',
                  'require_mentor_review_meetings',
                  'allow_admin_jobs', 'require_job_deadlines',
                  'enable_private_discussion', 'user_profile_public',
                  'venture_profile_public']
        labels = {
            'require_startup_approval': 'Admin Approval for Startups',
            'require_meeting_approval': 'Admin Approval for Mentorship Meetings',
            'require_mentor_review_meetings': 'Require Mentor Review Meetings',
            'allow_admin_jobs': 'Admin Posting Jobs',
            'require_job_deadlines': 'Deadlines for Job Postings',
            'enable_private_discussion': 'Enable Private Forum Posts',
            'user_profile_public': 'User Profile Privacy',
            'venture_profile_public': 'Venture Profile Privacy'
        }
        help_texts = {
            'require_startup_approval': "Require all ventures and projects to be approved \
            by program admin.",
            'require_meeting_approval': "Require meetings scheduled between a mentor \
            and mentee to be approved by program admin.",
            'require_mentor_review_meetings': "Require meetings scheduled between a mentor \
            and mentee to be approved by the mentor.",
            'allow_admin_jobs': "Allow admins to post jobs for any startup.",
            'require_job_deadlines': "Choose whether all job postings on your platform \
            should be required to have deadlines.",
            'enable_private_discussion': "Ability for users to create Forum Posts which \
            other users may only access by invitation from the post author."
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityPermissionForm, self).__init__(*args, **kwargs)


class UniversityOtherForm(forms.ModelForm):
    class Meta:
        model = UniversityStyle
        fields = ['tools', 'show_guest_data', 'display_pages_fields']

        labels = {
            'tools': 'Deals',
            'show_guest_data': 'Guest Data',
            'display_pages_fields': 'Display Pages Fields'
        }
        help_texts = {
            'tools': "Which Deals you would like to be available to your members.",
            'show_guest_data': "Include guests under People > Users, and show \
            guest data as part of your user demographics.",
            'display_pages_fields': "Display fields from Version 1 of the Pages \
            module to users who are creating or editing a Page."
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(UniversityOtherForm, self).__init__(*args, **kwargs)


class EventForm(forms.ModelForm):
    start_date = forms.CharField(
        label='Start Date', widget=forms.TextInput(attrs={'class': 'dateinput'}))
    start_time = forms.CharField(label='Start Time')
    end_date = forms.CharField(
        label='End Date', widget=forms.TextInput(attrs={'class': 'dateinput'}))
    end_time = forms.CharField(label='End Time')

    class Meta:
        model = Event
        fields = ['name', 'description', 'location']
        labels = {
            'name': "Event Name",
            'description': "Description",
            'location': "Location"
        }


class FileForm(forms.Form):
    attachment = forms.FileField()


class UniversitySubscriptionForm(forms.ModelForm):
    class Meta:
        model = UniversitySubscription
        exclude = ["university"]


class UniversityGroupForm(forms.ModelForm):
    class Meta:
        model = UniversityGroup
        exclude = ['created_on']


class EventImageForm(forms.Form):
    event_image = forms.ImageField()
