from trunk.models import University, School


def get_all_schools(university_id):
    schools = School.objects.filter(university_id=university_id)
    return schools