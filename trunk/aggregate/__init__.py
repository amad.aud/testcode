from branch.models import (
    Experience,
    Degree,
    Exit,
    Startup,
    StartupMember,
    QuarterlyResource,
    UniversityAffiliation,
    Label
)
from django.db.models import Count, Max, Q
from trunk.models import School, University, UniversityStyle, EventRSVP
from django.utils import timezone
from datetime import date, datetime
from typing import Tuple


def group_members_by_school(university: University, percent=False, tags=None, include_non_tagged=False,
                            date_range: Tuple[date, date] = None, include_placeholder_members=None):
    university_id = university.id

    # Annotate count of startup members (filtered within date range and with tags, if specified) attending the school.

    query_user = get_query_user(date_range, include_placeholder_members, prefix='degree__startupmember__')
    # eg: query_user =
    #      Q(degree__startupmember__user__created_on__range=date_range)| Q(degree__startupmember__user__isnull=True)

    if tags and len(tags):
        query_user &= Q(degree__startupmember__labels__in=tags)
    annotate_param = Count('degree__startupmember__id', filter=query_user, distinct=True)
    annotate_alias = 'sm_count'

    schools = School.objects.filter(university_id=university_id, degree__startupmember__is_archived=False) \
        .annotate(**{annotate_alias: annotate_param}) \
        .order_by(annotate_alias)

    no_degree_members = StartupMember.objects.filter(
        universities__id=university_id, degrees__isnull=True, is_archived=False)
    if date_range:
        no_degree_members = no_degree_members.filter(user__created_on__range=date_range)
    if tags and len(tags) > 0:
        if include_non_tagged:
            no_degree_members = no_degree_members.annotate(num_tags=Count('labels')) \
                .filter(Q(labels__in=tags) | Q(num_tags=0)).distinct()
            schools = schools.annotate(member_num_tags=Count('degree__startupmember__labels')) \
                .filter(Q(degree__startupmember__labels__in=tags) | Q(member_num_tags=0)) \
                .distinct()
        else:
            no_degree_members = no_degree_members.filter(labels__in=tags)
            schools = schools.filter(degree__startupmember__labels__in=tags)
    result = {-1: {'name': 'not provided', 'count': no_degree_members.count()}}

    for school in schools:
        count = getattr(school, annotate_alias)  # ie: school.sm_count
        result[school.id] = {'name': school.name, 'count': count}

    if percent:
        total = 0.
        for ele in result:
            total += result[ele]['count']
        if total == 0:
            result = []
        else:
            for ele in result:
                result[ele]['count'] = int(float(result[ele]['count']) / total * 100)

    final = []
    for ele in result:
        final.append([str(result[ele]['name']), result[ele]['count']])

    if not final:
        final = [
            ['No Stats', 100]
        ]
    final = sorted(final, key=lambda x: x[1])
    return final


def group_members_by_grad_year(university_id, percent=False, two_way=False,
                               tags=None, include_non_tagged=False,
                               total_members=None):
    """

    :param university_id: id of the platform
    :param percent: return percentile instead of total count
    :param two_way: whether the result should be limited to STUDENT, ALUMNI counts or
        graduation years (others, 2011, 2012, 2013, 2014 ... so on)
    :return:

    Optional param: total_members is a queryset of StartupMember; if provided, that is the queryset we use,
    otherwise, we use StartupMember.objects.filter(universities__id=university_id, is_archived=False)
    """
    STUDENT = 'Students'
    ALUMNI = 'Alumni'
    grad_year_counts = {}
    university_id = int(university_id)
    style = UniversityStyle.objects.get(_university__id=university_id)
    if two_way:
        grad_year_counts[STUDENT] = 0
        grad_year_counts[ALUMNI] = 0

    if total_members:
        members = total_members
    else:
        members = StartupMember.objects.filter(universities__id=university_id, is_archived=False)

    affiliations = UniversityAffiliation.objects.filter(
        member__universities__id=university_id, member__is_archived=False)
    if tags and len(tags) > 0:
        if include_non_tagged:
            members = members.annotate(num_tags=Count('labels')) \
                .filter(Q(labels__in=tags) | Q(num_tags=0)).distinct()
            affiliations = affiliations.annotate(member_num_tags=Count('member__labels')) \
                .filter(Q(member__labels__in=tags) | Q(member_num_tags=0)).distinct()
        else:
            members = members.filter(labels__in=tags)
            affiliations = affiliations.filter(member__labels__in=tags)
    affiliations = affiliations.values('affiliation').annotate(Count('affiliation'))
    total_staff = 0
    total_faculty = 0
    total_community_members = 0
    for affiliation in affiliations:
        if affiliation['affiliation'] == UniversityAffiliation.UNIVERSITY_STAFF:
            total_staff = affiliation['affiliation__count']
        if affiliation['affiliation'] == UniversityAffiliation.UNIVERSITY_FACULTY:
            total_faculty = affiliation['affiliation__count']
        if affiliation['affiliation'] == UniversityAffiliation.NOT_UNIVERSITY_PERSON:
            total_community_members = affiliation['affiliation__count']

    current_year = timezone.datetime.now().year

    total = members.count()
    total_others = 0

    alumni = members.filter(universityaffiliation__affiliation=5)
    total_alumni = alumni.count()

    students = members.filter(universityaffiliation__affiliation=4)
    total_students = students.count()

    if style.show_guest_data:
        guests = EventRSVP.objects.filter(rsvp_event__university__id=university_id,
                                          guest__isnull=False, waitlisted=False) \
            .distinct('guest__email')
        total_guests = guests.count()
    else:
        total_guests = 0

    # we need to query for the max graduation year of each members.
    # if user does not have degree or year data, the user belongs to OTHERS
    member_max_degrees = members.annotate(Max('degrees__graduated_on'))\
        .values('id', 'degrees__graduated_on')
    for member in member_max_degrees:
        grad_year_max = member['degrees__graduated_on']
        if not grad_year_max:
            total_others += 1
        else:
            if grad_year_max == 0:
                total_others += 1
            else:
                if two_way:
                    if grad_year_max >= current_year:
                        grad_year_counts[STUDENT] += 1
                    else:
                        grad_year_counts[ALUMNI] += 1
                else:
                    tmp = grad_year_counts.get(grad_year_max)
                    if tmp is None:
                        grad_year_counts[grad_year_max] = 1
                    else:
                        grad_year_counts[grad_year_max] = tmp + 1

    if two_way:
        user_count_result = [
            [STUDENT, grad_year_counts[STUDENT]],
            [ALUMNI, grad_year_counts[ALUMNI]]
        ]
    else:
        user_count_result = []
        for key in grad_year_counts:
            user_count_result.append([str(key), grad_year_counts[key]])
    user_count_result.append(['Staff', total_staff])
    user_count_result.append(['Faculty', total_faculty])
    user_count_result.append(['Community', total_community_members])
    user_count_result.append(['Unaffiliated', total_others])
    user_count_result.append(['Guests', total_guests])
    total = float(total)
    if percent:
        if total == 0:
            user_count_result = []
        else:
            for ele in user_count_result:
                ele[1] = round(float(float(ele[1]) / total * 100), 2)

    if not user_count_result:
        user_count_result = [
            ['No Stats', 100]
        ]
    # user_count_result = sorted(user_count_result, key=lambda x: x[0])
    return user_count_result, total_alumni, total_students, \
        total_others, total_staff, total_faculty, total_community_members, total_guests


def group_members_by_group(university: University, percent=False, selected=None,
                           date_range: Tuple[date, date] = None, include_placeholder_members=None):
    groups = {}
    total = 0

    # All labels of this University which have been applied to at least one StartupMember
    labels = Label.objects.filter(
        university_id=university.id,
        startupmember__is_archived=False,
        field_type="groups")

    # Filter labels down to those that have been selected.
    if selected and len(selected) > 0:
        selected_list = selected.values('id')
        labels = labels.filter(id__in=selected_list)

    # label.startupmember__id__count contains count of startupmembers with the label
    annotate_alias = 'sm_count'
    query_user = get_query_user(date_range, include_placeholder_members, prefix='startupmember__')
    # eg: query_user = Q(startupmember__user__created_on__range=date_range) | Q(startupmember__user__isnull=True)
    annotate_param = Count('startupmember__id', filter=query_user)
    labels = labels.annotate(**{annotate_alias: annotate_param})

    if university.style.alphabetize_tags:
        labels = labels.order_by('title', annotate_alias)
    else:
        labels = labels.order_by(annotate_alias)

    for label in labels:
        l_id = label.label_id
        cnt = getattr(label, annotate_alias)  # ie: label.sm_count
        groups[l_id] = [label.title, cnt]
        total += cnt
    if percent:
        if total == 0:
            groups = []
        else:
            for ele in groups:
                groups[ele][1] = int(float(groups[ele][1]) / total * 100)

    final = []
    for ele in groups:
        final.append([str(groups[ele][0]), groups[ele][1]])

    if not final:
        final = [
            ['No Stats', 100]
        ]
    final = sorted(final, key=lambda x: x[1])
    return final


def calculate_user_breakdowns(data_dict):       # Calculates breakdown of diffrent user groups
    unique_user_list = []   # Stores total unique user list
    for user_group in data_dict:
        data_dict[user_group]["uul"] = []                   # Create Blank Unique User List for each group (UUL)
        for user in data_dict[user_group]["raw_data"]:
            if user not in unique_user_list:                # Add User to global unique user list
                unique_user_list.append(user)
            if user not in data_dict[user_group]["uul"]:    # Add User to local unique user list structure
                data_dict[user_group]["uul"].append(user)
    total_users = len(unique_user_list)                     # Total Users across all groups
    output = {"Total_Users":total_users,"UUL":unique_user_list}                    # Store result for output
    for user_group in data_dict:
        try:
            data_dict[user_group]["percent"] = len(data_dict[user_group]["uul"])/total_users    # Calulcate for each group percent users
        except:
            data_dict[user_group]["percent"] = 0
        output[user_group] = data_dict[user_group]["percent"]
    return output


def format_percent_with_total(percent,total):   #Takes in percent and total number and outputs it as the count | percent%
    return str(int(total*percent))+" | "+str(percent*100)[:2]+"%"


def get_query_user(date_range: Tuple[date, date] = None, include_placeholder_members=None, prefix=None):
    """
    Returns a Q object that queries startupmembers by users that are created within the date range (if provided)
    and also includes startupmembers if include_placeholder_members.

    A placeholder member is startupmember whose user is null.

    Example 1:
        query = get_query_user(date_range, True)
        # returns Q(user__created_on__range=date_range) | Q (user__isnull=True)
        StartupMember.objects.filter(query)
        # returns all startup members whose users were created within `date_range` and also all startup members whose user is null.

    Example 2:
        query = get_query_user(False)
        # returns Q(user__isnull=False)
        StartupMember.objects.filter(query)
        # returns all startup members that are not placeholder members (no users are null).

    If the Q object is being used in the filter clause of an aggregate, then prefix is used to prepend any string
    to the clause key.

    Example 3:

        query = get_query_user(date_range, True, 'startupmember__')
        # returns Q(startupmember__user__created_on__range=date_range) | Q (startupmember__user__isnull=True)
        labels = Label.objects.all()
        labels = labels.annotate(sm_count=Count('startupmember__id', filter=count_filter))
        # returns queryset of labels, annotated with a `sm_count` field that counts startupmembers with the label
        # who were created within the date range or with user null.
        label = labels.first()
        label.sm_count
        # returns the number of startupmembers that have the label and were creaated within the date range or
        # who have a user field that is null.
    """
    if prefix is None:
        prefix = ''

    if date_range:
        query = Q(**{prefix + 'user__created_on__range': date_range})
        if include_placeholder_members:
            query |= Q(**{prefix + 'user__isnull': True})
    else:
        if include_placeholder_members:
            query = Q()
        else:
            query = Q(**{prefix + 'user__isnull': False})
    return query
