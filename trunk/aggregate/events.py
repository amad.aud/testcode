import datetime
from dateutil.relativedelta import relativedelta
from trunk.models import (EventRSVP, Event)
from forms.models import (MemberApplicant)


def get_list_of_rsvp_users(university_id,start_date_filter=None,end_date_filter=None):
    '''
        # List of user_ids
        # -> who RSVP'd to event
        # -> on a given site
        # -> Inside a certain date range
    '''
    if(not(start_date_filter and end_date_filter)):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
    rsvp_users = EventRSVP.objects.filter(rsvp_event__university_id=university_id, \
                                    created__range=[start_date_filter, end_date_filter]) \
                                    .distinct('member_id') \
                                    .values_list('member_id',flat=True)
    return list(rsvp_users)
    

def get_list_of_application_users(university_id,start_date_filter=None,end_date_filter=None):
    '''
        # List of user_ids
        # -> who applied to an application/competition
        # -> on a given site
        # -> Inside a certain date range
    '''
    if(not(start_date_filter and end_date_filter)):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
    events_list = Event.objects.filter(university_id=university_id).values_list('id', flat=True)
    application_users = MemberApplicant.objects.filter(event_id__in=events_list, \
                                        date_applied__range=[start_date_filter, end_date_filter]) \
                                        .distinct('member_id') \
                                        .values_list('member_id', flat=True)

    return list(application_users)
