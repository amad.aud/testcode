from branch.models import StartupMember
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import Colors, SECTION_BREAK
# from trunk.export import GeneralExport
from trunk.models import (
    Event,
    AcceptedRSVP,
    EventRSVP,
)
from trunk.utils import format_datetime_export
from functools import partial
import pytz


class EventsSheetWriter(AbstractDataSheetWriter[Event]):
    def __init__(self, exporter):
        """
        Requires: exporter is of type GeneralExport
        """
        self.exporter = exporter
        timezone_str = exporter.uni_style.timezone

        uni = self.exporter.university
        registration_word = 'Registration' if uni.short_name == "cornell" else "RSVP"

        column_names = [
            "EVENTS DATA-->",
            "Event Unique ID",
            "Event Name",
            f"Event Date ({timezone_str})",
            "Event Location",
            "First Name",
            "Last Name",
            "Undergrad vs Graduate student",
            "Email address",
            f"{registration_word} Type (Guest or Member)",
            "Affiliation",
            "School (Most Recent)",
            "Major (Most Recent)",
            "Degree(s)",
            f"{registration_word} Date & Time ({timezone_str})",
            "Check in Date(s) & Time(s)"
        ]

        section_breaks = {0}  # the only break is the first column.

        events = Event.objects.filter(university__site_id=uni.site.id,
                                      status=Event.APPROVED,
                                      is_template_only=False).order_by('start')

        if self.exporter.date_range_exclusive:
            events = events.filter(created__range=self.exporter.date_range_exclusive)

        super().__init__(
            workbook=self.exporter.workbook,
            sheet_title="Events",
            header_rows=[column_names],
            section_breaks=section_breaks,
            elements=events,
            header_bg_color=Colors.LIGHTCORAL,
            timezone=pytz.timezone(timezone_str),
        )

        self.format_datetime_export__partial = partial(format_datetime_export, self.exporter.uni_style)

    def make_rows_from_elt(self, event: Event):
        event_id = str(event.event_id)
        event_name = event.name
        event_date = event.start.astimezone(self.timezone).date()  # In case date depends on correct timezone
        event_location = event.location

        event_column_data = [
            event_id,
            event_name,
            event_date,
            event_location
        ]

        archived_users = event.archived_users.all().values_list('id', flat=True)

        rsvp_members = event.attending_users.exclude(id__in=archived_users)
        checkin_members = event.checked_in.select_related('member').exclude(member__user_id__in=archived_users) \
            .distinct('member__user__email').exclude(member__user_id__in=rsvp_members).exclude(member=None)
        rsvp_guests = event.guest_rsvps.filter(is_archived=False)
        checkin_guests = event.checked_in.select_related('guest').filter(
            guest__isnull=False, guest__is_archived=False) \
            .exclude(guest__in=event.guest_rsvps.all()) \
            .distinct('guest__email')

        full_rows = []

        def add_to_full_rows(participant_column_data):
            full_rows.append(SECTION_BREAK + event_column_data + participant_column_data)

        for member_user in rsvp_members:
            member_column_data = self.get_member_at_event_data(member_user, event)
            add_to_full_rows(member_column_data)

        for checkin_member in checkin_members:
            member_user = checkin_member.member.user
            member_column_data = self.get_member_at_event_data(member_user, event)
            add_to_full_rows(member_column_data)

        for guest in rsvp_guests:
            guest_column_data = self.get_guest_at_event_data(guest, event)
            add_to_full_rows(guest_column_data)

        for checkin_guest in checkin_guests:
            guest = checkin_guest.guest
            guest_column_data = self.get_guest_at_event_data(guest, event)
            add_to_full_rows(guest_column_data)

        return full_rows

    def get_member_at_event_data(self, member_user, event):
        format_datetime_export__partial = self.format_datetime_export__partial
        list_str_or_none = EventsSheetWriter.list_str_or_none

        member = StartupMember.objects.get_user(user=member_user)
        member_firstname = member.first_name
        member_lastname = member.last_name
        member_highest_degree_level = member.get_highest_degree_level_str()
        member_email = member.user.email
        member_rsvp_type = "Member"
        member_affiliation_list = list_str_or_none(list(member.get_affiliation_list_str()))

        member_recent_major, member_recent_school = EventsSheetWriter.get_recent_degree_info(member)

        member_degree_list = list_str_or_none(member.get_degree_list_str())

        member_rsvps = AcceptedRSVP.objects.filter(email=member.user.email, event=event)
        if member_rsvps.exists():
            member_rsvp_datetime = self.make_naive(member_rsvps.first().accept_time)
        else:
            member_rsvp_datetime = None

        checkins = event.checked_in.all().filter(member=member) \
            .order_by('created').values_list('created', flat=True)
        member_checkins = list_str_or_none(list(map(format_datetime_export__partial, checkins)))

        member_column_data = [
            member_firstname,
            member_lastname,
            member_highest_degree_level,
            member_email,
            member_rsvp_type,
            member_affiliation_list,
            member_recent_school,
            member_recent_major,
            member_degree_list,
            member_rsvp_datetime,
            member_checkins,
        ]

        return member_column_data

    def get_guest_at_event_data(self, guest, event):
        format_datetime_export__partial = self.format_datetime_export__partial

        guest_firstname = guest.first_name
        guest_lastname = guest.last_name
        guest_email = guest.email
        guest_rsvp_type = "Guest"

        (guest_highest_degree_level, guest_affiliation_list, guest_recent_school,
         guest_recent_major, guest_degree_list) = EventsSheetWriter.get_guest_degree_info(guest, event)

        guest_rsvp_datetime = self.make_naive(guest.accept_time)

        checkins = event.checked_in.all().filter(guest=guest).order_by('created').values_list('created', flat=True)
        guest_checkins = EventsSheetWriter.list_str_or_none(list(map(format_datetime_export__partial, checkins)))

        guest_column_data = [
            guest_firstname,
            guest_lastname,
            guest_highest_degree_level,
            guest_email,
            guest_rsvp_type,
            guest_affiliation_list,
            guest_recent_school,
            guest_recent_major,
            guest_degree_list,
            guest_rsvp_datetime,
            guest_checkins,
        ]

        return guest_column_data

    @staticmethod
    def list_str_or_none(lst):
        # Returns string version of list, or "None" if empty. Uses semicolon to delimit list items.
        if len(lst) == 0:
            return None
        else:
            return "[" + '; '.join(lst) + "]"

    @staticmethod
    def get_recent_degree_info(participant):
        participant_recent_major = None
        participant_recent_school = None

        recent_degree = participant.get_most_recent_degree()

        if recent_degree is not None:
            if recent_degree.major:
                participant_recent_major = recent_degree.major.get_name()

                if recent_degree.degree:
                    participant_recent_major += " (" + recent_degree.degree + ")"

            elif recent_degree.degree:
                participant_recent_major = "(" + recent_degree.degree + ")"

            if recent_degree.school:
                participant_recent_school = recent_degree.school.get_name()

        return participant_recent_major, participant_recent_school

    @staticmethod
    def get_guest_degree_info(guest, event):
        list_str_or_none = EventsSheetWriter.list_str_or_none

        highest_degree_level = None
        affiliation_list = None
        school = None
        major = None
        degree_list = None

        if EventRSVP.objects.filter(guest=guest, rsvp_event=event, waitlisted=False).exists():
            rsvp = EventRSVP.objects.filter(guest=guest, rsvp_event=event, waitlisted=False).first()

            degree_terms = []

            if rsvp.affiliation:
                affiliation = dict(EventRSVP.RSVP_AFFILIATIONS).get(rsvp.affiliation)
                # For consistency, we provide affiliation as a list (single element), as we do with Members.
                affiliation_list = list_str_or_none([affiliation])
            if rsvp.major:
                major = rsvp.major
                highest_degree_level = "Undergrad"  # We assume that having a major implies at least undegrad-level education
                degree_terms.append(major)
            if rsvp.school:
                school = rsvp.school
                degree_terms.append(school)

            if len(degree_terms) > 0:
                degree = ', '.join(degree_terms)
                degree_list = list_str_or_none([degree])

        return highest_degree_level, affiliation_list, school, major, degree_list
