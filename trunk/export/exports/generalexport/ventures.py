from branch.models import (
    Startup,
    Exit,
    Experience,
    UniversityAffiliation
)
from django.db.models import Q, Count
from forms.models import Form
from forms.serializer.forms import custom_dict_export
from reversion.models import Version
from StartupTree.utils import number_to_dollar_string
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import Colors
# from trunk.export import GeneralExport
from trunk.export.utils import (
    append_custom_names,
    append_custom_data,
    combine_startup_and_team_rows,
    get_column_names_and_breaks,
    get_created_updated,
)
import pytz
from typing import Iterable


class VenturesSheetWriter(AbstractDataSheetWriter[Startup]):
    def __init__(self, exporter):
        """
        Requires:
            `exporter` is of type GeneralExport
        """
        self.exporter = exporter
        self.timezone_str = exporter.uni_style.timezone

        column_names, section_breaks = self.__get_column_names_and_breaks()

        super().__init__(
            workbook=self.exporter.workbook,
            sheet_title=self.exporter.words['plural_ventures'],
            header_rows=[column_names],
            section_breaks=section_breaks,
            elements=self.__get_ventures(),
            header_bg_color=Colors.THISTLE,
            timezone=pytz.timezone(self.timezone_str),
        )

    def __get_column_names_and_breaks(self):
        venture_word = self.exporter.words['venture_word']
        plural_ventures = self.exporter.words['plural_ventures']

        column_names__venture_data_section = [
            "{0} DATA-->".format(venture_word.upper()),
            "Name",
            "Tags",
            "Stage of {0}".format(venture_word),
            "Year Founded",
            "Industry",
            "Location",
            "# Employees",
            "Pitch",
            "Email",
            "Website",
            "Total Funding",
            "Exit Value",
            "Exit Date",
            f"Date Created ({self.timezone_str})",
            f"Last Updated ({self.timezone_str})",
            "Archived?"
        ]

        append_custom_names(Form.STARTUP, self.exporter.university, column_names__venture_data_section,
                            self.exporter.exclude_admin_only_startup_custom_questions)

        column_names__team_members_section = [
            "TEAM MEMBERS-->",
            "{0}".format(plural_ventures),
            "Founder",
            "First Name",
            "Last Name",
            "Email Address",
            "Affiliation",
            "Class Year",
            "School",
            "Degrees",
            "Tags (Team Member)"
        ]

        column_names__grouped = [
            column_names__venture_data_section,
            column_names__team_members_section,
        ]

        return get_column_names_and_breaks(column_names__grouped)

    def __get_ventures(self) -> Iterable[Startup]:
        history_tags = self.exporter.history_tags
        include_non_tagged = self.exporter.include_non_tagged
        current_site = self.exporter.university.site

        venture_query = Startup.objects.select_related("industry", "base_location") \
            .prefetch_related("acquiree") \
            .filter(university__site_id=current_site.id, is_project=False, status=Startup.APPROVED)

        if self.exporter.date_range_exclusive:
            venture_query = venture_query.filter(created_on__range=self.exporter.date_range_exclusive)

        if len(history_tags) > 0:
            if include_non_tagged:
                venture_query = venture_query.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=history_tags) | Q(num_tags=0)).distinct()
            else:
                venture_query = venture_query.filter(labels__in=history_tags)

        return venture_query

    def __make_team_member_rows_from_venture(self, venture: Startup) -> Iterable[list]:
        uni_style = self.exporter.uni_style
        venture_name = venture.name

        team_member_rows = []

        for member_exp in Experience.objects.select_related('member')\
                .filter(startup_id=venture.id).order_by('member__first_name'):
            is_founder = "No"
            if member_exp.is_founder:
                is_founder = "Yes"

            startupmember = member_exp.member
            first_name = startupmember.first_name
            last_name = startupmember.last_name
            email_address = ''
            if startupmember.user:
                email_address = startupmember.user.email
            affiliations = []
            for uni_aff in startupmember.universityaffiliation_set.all():
                if uni_aff.affiliation == 4:
                    if uni_aff.is_alumni:
                        affiliations.append("Alumni")
                    else:
                        affiliations.append("Student")
                else:
                    affiliations.append(
                        UniversityAffiliation.UNIVERSITY_AFFILIATIONS[
                            uni_aff.affiliation - 1][1])
            degree_obj = startupmember.degrees.first()
            degree_class_year = None
            degree_school = None
            degree_name = None
            if degree_obj is not None:
                if degree_obj.university is not None:
                    degree_school = degree_obj.university.name
                if degree_obj.degree is not None:
                    degree_name = degree_obj.degree
                if degree_obj.graduated_on is not None:
                    degree_class_year = degree_obj.graduated_on
            groups = []
            startupmember_labels = startupmember.labels.all()
            if uni_style.alphabetize_tags:
                startupmember_labels = startupmember_labels.order_by('title')
            for group in startupmember_labels:
                if group.title:
                    groups.append("{0}".format(group.title))

            team_member_row = \
                [venture_name, is_founder, first_name, last_name,
                 email_address, ",".join(affiliations),
                 degree_class_year, degree_school, degree_name,
                 ", ".join(groups)]

            team_member_rows.append(team_member_row)

        return team_member_rows

    def make_rows_from_elt(self, venture: Startup) -> Iterable[list]:
        university = self.exporter.university

        venture_name = venture.name
        venture_tags = ", ".join(map(str, venture.labels.all()))
        venture_stage = None
        venture_acquiree_query_obj_exit_type = venture.acquiree.filter(exit_type__isnull=False).first()
        exit_types = dict(Exit.EXIT_TYPES)
        if venture_acquiree_query_obj_exit_type is not None:
            venture_stage = exit_types.get(venture_acquiree_query_obj_exit_type.exit_type)
        venture_founded = venture.founded_on
        venture_industry = None
        if venture.industry is not None:
            venture_industry = venture.industry.name.title()
        venture_location = None
        if venture.base_location is not None:
            venture_location = venture.base_location.name
        venture_num_employees = venture.num_employees
        venture_pitch = venture.pitch
        venture_email = venture.email
        venture_website = venture.website
        venture_total_funding = None
        if venture.get_total_funding() != 0:
            venture_total_funding = number_to_dollar_string(venture.get_total_funding())
        venture_exit_value = None
        if venture.get_exit_value() != 0:
            venture_exit_value = number_to_dollar_string(venture.get_exit_value())
        venture_exit_date = None
        venture_acquiree_query_obj_exited_on = venture.acquiree.filter(exited_on__isnull=False).first()
        if venture_acquiree_query_obj_exited_on is not None:
            venture_exit_date = venture_acquiree_query_obj_exited_on.exited_on

        date_created, last_updated = get_created_updated(self, venture)

        if venture.is_archived:
            venture_archived = "Yes"
        else:
            venture_archived = "No"

        column_data = [
            venture_name, venture_tags, venture_stage, venture_founded, venture_industry, venture_location,
            venture_num_employees, venture_pitch, venture_email, venture_website, venture_total_funding,
            venture_exit_value, venture_exit_date, date_created, last_updated, venture_archived
        ]

        custom = custom_dict_export(Form.STARTUP, venture, university,
                                    self.exporter.exclude_admin_only_startup_custom_questions)
        append_custom_data(custom, column_data)

        team_member_rows = self.__make_team_member_rows_from_venture(venture)

        return combine_startup_and_team_rows(column_data, team_member_rows)
