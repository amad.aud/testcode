from branch.models import (
    Startup,
    Exit,
    Experience,
    UniversityAffiliation
)
from django.db.models import Q, Count
from forms.models import Form
from forms.serializer.forms import custom_dict_export
from reversion.models import Version
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import Colors
# from trunk.export import GeneralExport
from trunk.export.utils import (
    get_column_names_and_breaks,
    append_custom_data,
    append_custom_names,
    combine_startup_and_team_rows,
    get_created_updated,
)
import pytz
from typing import Iterable, List


class ProjectsSheetWriter(AbstractDataSheetWriter[Startup]):
    def __init__(self, exporter):
        """
        Requires `exporter` to be of type GeneralExport
        """
        self.exporter = exporter
        self.timezone_str = self.exporter.uni_style.timezone

        column_names, section_breaks = self.__get_column_names_and_breaks()

        super().__init__(
            workbook=self.exporter.workbook,
            sheet_title=self.exporter.words['plural_projects'],
            header_rows=[column_names],
            section_breaks=section_breaks,
            elements=self.__get_projects(),
            header_bg_color=Colors.LIGHTSKYBLUE,
            timezone=pytz.timezone(self.timezone_str)
        )

    def __get_column_names_and_breaks(self):
        """
        Requires: self.exporter is defined
        """
        university = self.exporter.university
        project_word = self.exporter.words['project_word']
        plural_projects = self.exporter.words['plural_projects']

        column_names__project_data_section = [
            "PROJECT DATA -->", "Name", "Tags",
            "Stage of {0}".format(project_word),
            "Industry", "Location", "Pitch", "Website",
            f"Date Created ({self.timezone_str})",
            f"Last Updated ({self.timezone_str})",
            "Archived?"
        ]

        append_custom_names(Form.PROJECT, university, column_names__project_data_section,
                            self.exporter.exclude_admin_only_startup_custom_questions)

        column_names__team_members_section = [
            "TEAM MEMBERS-->", "{0}".format(plural_projects), "Founder",
            "First Name", "Last Name", "Email Address", "Affiliation",
            "Class Year", "School", "Degrees", "Tags (Team Member)"
        ]

        column_names__grouped = [
            column_names__project_data_section,
            column_names__team_members_section
        ]

        return get_column_names_and_breaks(column_names__grouped)

    def __get_projects(self) -> Iterable[Startup]:
        university = self.exporter.university
        history_tags = self.exporter.history_tags
        include_non_tagged = self.exporter.include_non_tagged

        project_query = Startup.objects.select_related("industry", "base_location") \
            .prefetch_related("acquiree") \
            .filter(university=university, is_project=True, status=Startup.APPROVED)
        if self.exporter.date_range_exclusive:
            project_query = project_query.filter(created_on__range=self.exporter.date_range_exclusive)
        if len(history_tags) > 0:
            if include_non_tagged:
                project_query = project_query.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=history_tags) | Q(num_tags=0)).distinct()
            else:
                project_query = project_query.filter(labels__in=history_tags)

        return project_query

    def __make_team_member_rows_from_project(self, project: Startup) -> List[list]:
        """
        Returns: [
            [Team Member 1 Data],
            ...,
            [Team Member n Data]
        ]
        """
        uni_style = self.exporter.uni_style
        project_name = project.name

        team_member_rows = []

        for member_exp in Experience.objects.select_related('member').filter(startup_id=project.id) \
                .order_by('member__first_name'):
            is_founder = "No"
            if member_exp.is_founder:
                is_founder = "Yes"

            startupmember = member_exp.member
            first_name = startupmember.first_name
            last_name = startupmember.last_name
            email_address = ''
            if startupmember.user:
                email_address = startupmember.user.email
            affiliations = []
            for uni_aff in startupmember.universityaffiliation_set.all():
                if uni_aff.affiliation == 4:
                    if uni_aff.is_alumni:
                        affiliations.append("Alumni")
                    else:
                        affiliations.append("Student")
                else:
                    affiliations.append(
                        UniversityAffiliation.UNIVERSITY_AFFILIATIONS[
                            uni_aff.affiliation - 1][1])
            degree_obj = startupmember.degrees.first()
            degree_class_year = None
            degree_school = None
            degree_name = None
            if degree_obj is not None:
                if degree_obj.university is not None:
                    degree_school = degree_obj.university.name
                if degree_obj.degree is not None:
                    degree_name = degree_obj.degree
                if degree_obj.graduated_on is not None:
                    degree_class_year = degree_obj.graduated_on
            groups = []
            startupmember_labels = startupmember.labels.all()
            if uni_style.alphabetize_tags:
                startupmember_labels = startupmember_labels.order_by('title')
            for group in startupmember_labels:
                if group.title:
                    groups.append("{0}".format(group.title))

            team_member_row = [
                project_name, is_founder, first_name, last_name,
                email_address, ",".join(affiliations),
                degree_class_year, degree_school, degree_name,
                ", ".join(groups)
            ]

            team_member_rows.append(team_member_row)

        return team_member_rows

    def make_rows_from_elt(self, project: Startup) -> Iterable[list]:
        """
        (notation: [a, b, c][d, e, f] is [a, b, c, d, e, f])

        Section break is just a None entry.

        Project data is only shown on the first row.

        Returns: [
            [section break][... Project x Data ...][section break][... Team Member 1 Data ...],
            [section break][... blank columns  ...][section break][... Team Member 2 Data ...],
            ...
            [section break][... blank columns  ...][section break][... Team Member n Data ...],
        ]
        """
        exit_types = dict(Exit.EXIT_TYPES)
        university = self.exporter.university

        project_name = project.name
        project_tags = ", ".join(map(str, project.labels.all()))
        project_stage = None
        project_acquiree_query_obj_exit_type = \
            project.acquiree.filter(exit_type__isnull=False).first()
        if project_acquiree_query_obj_exit_type is not None:
            project_stage = \
                exit_types.get(
                    project_acquiree_query_obj_exit_type.exit_type)
        project_industry = None
        if project.industry is not None:
            project_industry = project.industry.name.title()
        project_location = None
        if project.base_location is not None:
            project_location = project.base_location.name
        project_pitch = project.pitch
        project_website = project.website

        date_created, last_updated = get_created_updated(self, project)

        if project.is_archived:
            project_archived = "Yes"
        else:
            project_archived = "No"

        column_data = \
            [project_name, project_tags, project_stage, project_industry,
             project_location, project_pitch, project_website,
             date_created, last_updated, project_archived]

        custom = custom_dict_export(Form.PROJECT, project, university,
                                    self.exporter.exclude_admin_only_startup_custom_questions)
        append_custom_data(custom, column_data)

        team_rows = self.__make_team_member_rows_from_project(project)

        return combine_startup_and_team_rows(column_data, team_rows)
