from branch.models import (
    StartupMember,
    UniversityAffiliation,
)
from django.db.models import Q, Count
from forms.models import Form
from forms.serializer.forms import custom_dict_export
from mentorship.models import Mentor
from trunk.aggregate import get_query_user
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import Colors, SECTION_BREAK
# from trunk.export import GeneralExport
from trunk.export.utils import (
    append_custom_data,
    append_custom_names,
    get_created_updated
)
from operator import attrgetter
import pytz
from typing import List, Tuple, Set, Iterable


class UsersSheetWriter(AbstractDataSheetWriter[StartupMember]):
    def __init__(self, exporter):
        """
        Requires: exporter is of type GeneralExport
        """
        self.exporter = exporter
        self.timezone_str = exporter.uni_style.timezone

        column_names, section_breaks = self.__get_column_names_and_breaks()
        startupmembers = self.__get_startupmembers()

        super().__init__(workbook=self.exporter.workbook,
                         sheet_title='Users',
                         header_rows=[column_names],
                         section_breaks=section_breaks,
                         elements=startupmembers,
                         header_bg_color=Colors.MOCCASIN,
                         timezone=pytz.timezone(self.timezone_str))

    def __get_column_names_and_breaks(self) -> Tuple[List[str], Set[int]]:
        """
        Requires: self.exporter and self.timezone_str is defined
        """
        plural_ventures = self.exporter.words['plural_ventures']
        plural_projects = self.exporter.words['plural_projects']

        column_names = [
            "USER DATA -->", "First Name", "Last Name", "Email Address",
            "Affiliation", "Mentor", "Gender", "Location", "Class Year", "School", "Degrees", "Grad/Undergrad",
            "Tags", "Skills", "Roles", "Experience", "Biography",
            "{0}".format(plural_ventures), "{0}".format(plural_projects), 'Events Attended',
            f"Date Created ({self.timezone_str})", f"Last Updated ({self.timezone_str})", "Archived?"
        ]

        append_custom_names(Form.MEMBER, self.exporter.university, column_names)

        section_breaks = {0}  # The only section break is the first column.

        return column_names, section_breaks

    def __get_startupmembers(self) -> Iterable[StartupMember]:
        university = self.exporter.university
        history_tags = self.exporter.history_tags
        include_non_tagged = self.exporter.include_non_tagged

        query_user = get_query_user(self.exporter.date_range_exclusive, self.exporter.include_placeholder_members)

        startupmember_query = StartupMember.objects.select_related("user") \
            .prefetch_related("universityaffiliation_set",
                              "labels", "skills", "skill_groups",
                              "experience_set", "admin_startups", "degrees__school") \
            .filter(query_user, universities=university)
        if len(history_tags) > 0:
            if include_non_tagged:
                startupmember_query = startupmember_query.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=history_tags) | Q(num_tags=0)).distinct()
            else:
                startupmember_query = startupmember_query.filter(labels__in=history_tags)

        return startupmember_query

    def make_rows_from_elt(self, startupmember: StartupMember) -> Iterable[list]:
        university = self.exporter.university
        uni_style = self.exporter.uni_style

        try:
            Mentor.objects.get(profile_id=startupmember.id, platform_id=university.id)
            is_mentor = 'Y'
        except Mentor.DoesNotExist:
            is_mentor = 'N'
        first_name = startupmember.first_name
        last_name = startupmember.last_name
        email_address = startupmember.user.email if startupmember.user else 'NONE (Placeholder Account)'
        affiliations = []
        for uni_aff in startupmember.universityaffiliation_set.all():
            if uni_aff.affiliation == 4:
                if uni_aff.is_alumni:
                    affiliations.append("Alumni")
                else:
                    affiliations.append("Student")
            else:
                affiliations.append(
                    UniversityAffiliation.UNIVERSITY_AFFILIATIONS[
                        uni_aff.affiliation - 1][1])
        gender = startupmember.get_gender_str()
        location = startupmember.location
        degree_obj = startupmember.degrees.first()
        degree_class_year = None
        degree_school = None
        degree_name = None
        if degree_obj is not None:
            if degree_obj.school is not None:
                degree_school = degree_obj.school.get_name()
            if degree_obj.degree is not None:
                degree_name = degree_obj.degree
            if degree_obj.graduated_on is not None:
                degree_class_year = degree_obj.graduated_on
        highest_degree_level = startupmember.get_highest_degree_level_str()
        groups = []
        startupmember_labels = startupmember.labels.all()
        if uni_style.alphabetize_tags:
            startupmember_labels = startupmember_labels.order_by('title')
        for group in startupmember_labels:
            if group.title:
                groups.append("{0}".format(group.title))
        skills = []
        for skill in startupmember.skills.all():
            if skill.name:
                skills.append("{0}".format(skill.name))
        roles = []
        for role in startupmember.skill_groups.all():
            if role.name:
                roles.append("{0}".format(role.name))
        experiences = []
        ventures = []
        projects = []
        for experience in startupmember.experience_set \
                .select_related("member", "startup", "role") \
                .filter(member__isnull=False,
                        role__isnull=False,
                        startup__isnull=False):
            experiences.append("{0}".format(experience))
            if experience.startup.is_project:
                projects.append("{0}".format(experience.startup.name))
            else:
                ventures.append("{0}".format(experience.startup.name))
        biography = startupmember.bio

        date_created, last_updated = get_created_updated(self, startupmember)

        if startupmember.is_archived:
            archived = "Yes"
        else:
            archived = "No"

        events_attended = map(attrgetter('name'), startupmember.get_events_attended(reverse_chronological=True))

        column_data = \
            [first_name, last_name, email_address, ",".join(affiliations), is_mentor,
             gender, location, degree_class_year, degree_school, degree_name, highest_degree_level,
             ", ".join(groups), ", ".join(skills), ", ".join(roles),
             ", ".join(experiences), biography, ", ".join(ventures),
             ", ".join(projects), ', '.join(events_attended), date_created, last_updated, archived]

        custom = custom_dict_export(Form.MEMBER, startupmember, university)
        append_custom_data(custom, column_data)

        column_data = SECTION_BREAK + column_data

        return [column_data]
