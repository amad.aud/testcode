from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import Colors, SECTION_BREAK
# from trunk.export import GeneralExport
from trunk.models import EventRSVP
import pytz


class GuestsSheetWriter(AbstractDataSheetWriter[EventRSVP]):
    def __init__(self, exporter):
        self.exporter = exporter
        self.uni = self.exporter.university
        timezone_str = self.exporter.uni_style.timezone

        registration_word = "Registration" if self.uni.short_name == "cornell" else "RSVP"

        column_names = [
            'GUEST USER DATA -->', 'First Name', 'Last Name', 'Email',
            f'Date of Guest {registration_word} ({timezone_str})',
            'Affiliation', 'School', 'Major'
        ]

        super().__init__(
            workbook=self.exporter.workbook,
            sheet_title="Guests",
            header_rows=[column_names],
            section_breaks={0},  # The only section break is the first column
            elements=self.__get_guests(),
            header_bg_color=Colors.AZURE,
            timezone=pytz.timezone(timezone_str),
        )

    def __get_guests(self):
        """ Returns queryset of EventRSVP objects related to all guests on platform that
        were created between start_date and end_date"""
        guests = EventRSVP.objects.select_related('guest').filter(
            rsvp_event__university__id=self.exporter.university.id,
            guest__isnull=False, waitlisted=False,
        ).distinct('guest__email')

        if self.exporter.date_range_exclusive:
            guests = guests.filter(created__range=self.exporter.date_range_exclusive)

        if len(self.exporter.history_tags) > 0 and not self.exporter.include_non_tagged:
            # if tags are selected (and not include non-tagged), then show no guests because guests can't be tagged.
            guests = EventRSVP.objects.none()

        return guests

    def make_rows_from_elt(self, guest: EventRSVP):
        first_name = guest.guest.first_name
        last_name = guest.guest.last_name
        email_address = guest.guest.email
        date_created = self.make_naive(guest.created)
        affiliation = guest.get_affiliation_str()
        school = guest.school
        major = guest.major

        guest_column_data = [
            first_name, last_name, email_address, date_created,
            affiliation, school, major
        ]

        return [SECTION_BREAK + guest_column_data]
