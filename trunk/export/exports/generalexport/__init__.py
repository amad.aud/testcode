from trunk.export.abstract import AbstractUniversityExporter, AbstractSheetWriter
from trunk.export.utils import make_words
from typing import List
from trunk.export.exports.generalexport.analytics import AnalyticsSheetWriter
from trunk.export.exports.generalexport.users import UsersSheetWriter
from trunk.export.exports.generalexport.guests import GuestsSheetWriter
from trunk.export.exports.generalexport.projects import ProjectsSheetWriter
from trunk.export.exports.generalexport.ventures import VenturesSheetWriter
from trunk.export.exports.generalexport.events import EventsSheetWriter


class GeneralExport(AbstractUniversityExporter):
    def __init__(self, history):
        super().__init__(history)
        self.words = make_words(self.uni_style)

    def get_sheet_writers(self) -> List[AbstractSheetWriter]:
        sheet_writers = [
            AnalyticsSheetWriter(self),
            UsersSheetWriter(self),
        ]

        if self.uni_style.show_guest_data:
            sheet_writers += [
                GuestsSheetWriter(self),
            ]

        sheet_writers += [
            ProjectsSheetWriter(self),
            VenturesSheetWriter(self),
            EventsSheetWriter(self),
        ]

        return sheet_writers
