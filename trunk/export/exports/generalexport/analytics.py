from branch.aggregate.funding import (
    get_sum_of_startup_funding,
    get_sum_of_startup_exits,
)
from branch.aggregate.venture import (
    get_employ_numbers,
    get_total_members,
)
from branch.models import (
    Startup,
)
from data_collection import aggregate as data_aggre
import datetime
from django.db.models import Q, Count
from mentorship.models import Mentor
from StartupTree.utils import number_to_dollar_string
from trunk.aggregate import (
    group_members_by_grad_year,
    group_members_by_group,
    group_members_by_school,
)
from trunk.export.abstract import AbstractSheetWriter
from trunk.export.constants import Colors, SECTION_BREAK
# from trunk.export import GeneralExport
from trunk.export.utils import get_column_names_and_breaks
from typing import Iterable, List, Tuple


class AnalyticsSheetWriter(AbstractSheetWriter):
    def __init__(self, exporter):
        """
        Requires: exporter is of type GeneralExport
        """
        self.exporter = exporter

        self.total_members = get_total_members(self.exporter.university.id,
                                               tags=self.exporter.history_tags,
                                               include_non_tagged=self.exporter.include_non_tagged,
                                               date_range=self.exporter.date_range_exclusive,
                                               include_placeholder_members=self.exporter.include_placeholder_members)

        # Although it is only required that we get column names during constructor call,
        # For these two sections, it is more efficient to get column names and data together.
        self.by_tags__names_and_data = self.__get_by_tags__names_and_data()  # Column Names and column data for "By Tags" section
        self.by_school__names_and_data = self.__get_by_school__names_and_data()  # Column Names and column data for "By School" section

        column_names, section_breaks = self.__get_column_names_and_breaks()

        super().__init__(workbook=self.exporter.workbook,
                         sheet_title='Analytics',
                         header_rows=[column_names],
                         section_breaks=section_breaks,
                         header_bg_color=Colors.LIGHTGREEN)

    def __get_by_tags__names_and_data(self):
        """
        Returns Column Names and column data for "By Tags" section
        Requires: self.exporter is defined
        """
        members_by_groups = group_members_by_group(
            self.exporter.university,
            selected=self.exporter.history_tags,
            date_range=self.exporter.date_range_exclusive,
            include_placeholder_members=self.exporter.include_placeholder_members)
        column_names = ["By Tags-->"]
        column_data = []

        for group_name, num_users_in_group in members_by_groups:
            if group_name == "No Stats":
                break
            else:
                column_names.append("'# {0}'".format(group_name))
                column_data.append(num_users_in_group)

        return column_names, column_data

    def __get_by_school__names_and_data(self):
        """
        Returns Column Names and column data for "By School" section
        Requires: self.exporter is defined
        """
        members_by_school = group_members_by_school(
            self.exporter.university, tags=self.exporter.history_tags,
            include_non_tagged=self.exporter.include_non_tagged,
            date_range=self.exporter.date_range_exclusive,
            include_placeholder_members=self.exporter.include_placeholder_members)
        column_names = ["By School-->"]
        column_data = []
        for school_name, num_users_in_school in members_by_school:
            if school_name == "No Stats":
                break
            else:
                column_names.append("'# {0}'".format(school_name))
                column_data.append(num_users_in_school)

        return column_names, column_data

    def __get_column_names_and_breaks(self) -> Tuple[List[str], Iterable[int]]:
        """
        Requires the following defined:
            self.exporter is defined
            self.by_tags__names_and_data
            self.by_school__names_and_data

        Returns:
            column_names: list of column header names
            section_breaks: set of column indices that are a section break (and will be filled)
        """
        university = self.exporter.university
        uni_style = self.exporter.uni_style
        venture_word = self.exporter.words['venture_word']
        plural_mentors = self.exporter.words['plural_mentors']

        column_names__analytics_data_section = [
            "ANALYTICS DATA-->", "Date of the Export", "Date Range - Start", "Date Range - End",
            "Total Startups", "Average Size", "Jobs Created", "Total Funding", "Total Exit Value",
            "Visitors", "Page Views", "User Signups", "{0} Signups".format(venture_word)
        ]

        column_names__user_demographics_section = \
            ["USER DEMOGRAPHICS-->", "Total # Users"]

        if university.short_name == 'bencolorado':
            column_names__user_demographics_section += \
                ["# Advisors", "# CEOs", "# Community Partners"]
        else:
            column_names__user_demographics_section += \
                ["# Students", "# Alumni", "# Faculty", "# Staff", "# Community Members"]

            if uni_style.show_guest_data:
                column_names__user_demographics_section += \
                    ["# Guests"]

            column_names__user_demographics_section += \
                [f"# {plural_mentors}"]

        column_names__by_tags_section = self.by_tags__names_and_data[0]
        column_names__by_school_section = self.by_school__names_and_data[0]

        column_names__grouped = [
            column_names__analytics_data_section,
            column_names__user_demographics_section,
            column_names__by_tags_section,
            column_names__by_school_section,
        ]

        return get_column_names_and_breaks(column_names__grouped)

    def __make_row__analytics_data_section(self):
        university = self.exporter.university
        history_tags = self.exporter.history_tags
        include_non_tagged = self.exporter.include_non_tagged
        current_site = university.site

        companies = Startup.objects.filter(university=university, is_project=False, status=Startup.APPROVED)

        if self.exporter.date_range_exclusive:
            companies = companies.filter(created_on__range=self.exporter.date_range_exclusive)

        if len(history_tags) > 0:
            if include_non_tagged:
                companies = companies.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=history_tags) | Q(num_tags=0)).distinct()
            else:
                companies = companies.filter(labels__in=history_tags)

        total_companies = companies.count()

        jobs_created_partition, avg_company_size = \
            get_employ_numbers(university, tags=history_tags, include_non_tagged=include_non_tagged,
                               date_range=self.exporter.date_range_exclusive)

        jobs_created = 0
        for _, jobs in jobs_created_partition:
            jobs_created += jobs

        total_funding = number_to_dollar_string(
            get_sum_of_startup_funding(current_site, tags=history_tags, include_non_tagged=include_non_tagged,
                                       date_range=self.exporter.date_range_exclusive))
        total_exit = number_to_dollar_string(
            get_sum_of_startup_exits(current_site, tags=history_tags, include_non_tagged=include_non_tagged,
                                     date_range=self.exporter.date_range_exclusive))

        client_request_qset = data_aggre.get_client_request(current_site, self.exporter.date_range_exclusive)
        total_visitors = data_aggre.get_unique_visitors(client_request_qset)
        total_page_view = data_aggre.get_total_page_views(client_request_qset)

        total_user_signups = self.total_members.count()
        total_venture_signups = total_companies

        column_data = \
            [datetime.date.today(), self.exporter.start_date, self.exporter.end_date,
             total_companies, avg_company_size, jobs_created, total_funding, total_exit,
             total_visitors, total_page_view, total_user_signups, total_venture_signups]

        return column_data

    def __make_row__user_demographics_section(self):
        university = self.exporter.university
        uni_style = self.exporter.uni_style
        history_tags = self.exporter.history_tags
        include_non_tagged = self.exporter.include_non_tagged

        column_data = [self.total_members.count()]

        if university.short_name == 'bencolorado':
            members = self.total_members
            if len(history_tags) > 0:
                if include_non_tagged:
                    members = members.annotate(num_tags=Count('labels')) \
                        .filter(Q(labels__in=history_tags) | Q(num_tags=0)).distinct()
                else:
                    members = members.filter(labels__in=history_tags)
            total_advisors = members.filter(universityaffiliation__affiliation=6).count()
            total_ceo = members.filter(universityaffiliation__affiliation=7).count()
            total_partners = members.filter(universityaffiliation__affiliation=8).count()
            column_data += \
                [total_advisors, total_ceo, total_partners]
        else:
            _, total_alumni, total_students, _, total_staff, total_faculty, \
                total_community_members, total_guests = group_members_by_grad_year(
                    university.id, False, True, tags=history_tags, include_non_tagged=include_non_tagged)

            mentors = Mentor.objects.select_related('profile').filter(
                platform=self.exporter.university, is_complete=True)
            if self.exporter.date_range_exclusive:
                mentors = mentors.filter(Mentor.objects.query_date_range(
                    self.exporter.start_date, self.exporter.end_date))
            if len(history_tags) > 0:
                if include_non_tagged:
                    total_mentors = mentors.annotate(num_tags=Count('profile__labels')) \
                        .filter(Q(profile__labels__in=history_tags) | Q(num_tags=0)).distinct().count()
                else:
                    total_mentors = mentors.filter(profile__labels__in=history_tags).count()
            else:
                total_mentors = mentors.count()

            column_data += \
                [total_students, total_alumni, total_faculty, total_staff, total_community_members]

            if uni_style.show_guest_data:
                column_data += \
                    [total_guests]

            column_data += \
                [total_mentors]

        return column_data

    def make_footer_rows(self) -> Iterable[list]:
        """
        Requires: self.exporter is defined.
        """
        analytics_data_section = self.__make_row__analytics_data_section()
        user_demographics_section = self.__make_row__user_demographics_section()
        by_tags_section = self.by_tags__names_and_data[1]
        by_school_section = self.by_school__names_and_data[1]

        column_data = \
            SECTION_BREAK + analytics_data_section \
            + SECTION_BREAK + user_demographics_section \
            + SECTION_BREAK + by_tags_section \
            + SECTION_BREAK + by_school_section

        return [column_data]
