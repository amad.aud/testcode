from django.db.models import Q, Count
from branch.models import (
    StartupMember,
    Investment
)
from forms.models import Form
from forms.serializer.forms import custom_dict_export
from StartupTree.utils import number_to_dollar_string
from trunk.aggregate import get_query_user
from trunk.export.abstract import AbstractUniversityExporter, AbstractDataSheetWriter
from trunk.export.constants import Colors
from trunk.export.utils import (
    append_custom_names,
    append_custom_data,
    get_created_updated,
)
from trunk.models import (
    EventRSVP,
    UniversityExportHistory,
)
from itertools import chain
import pytz
from typing import Iterable


class UserExport(AbstractUniversityExporter):
    def __init__(self, history: UniversityExportHistory, is_funding: bool):
        """
        USER_GENERAL export or USER_FUNDING export, depending on is_funding parameter

        Requires start_date, end_date to be passable to datetime comparison filters.
            Eg: StartupMember.objects.filter(user__created_on__gte=start_date, user__created_on__lte=end_date)
        """
        self.history = history
        self.is_funding = is_funding

        # For guests to be exported, University must enable "show guest data", and not a user funding export
        self.is_write_guests = self.history.university.style.show_guest_data and not is_funding

        super().__init__(history=history)

    def get_sheet_writers(self) -> Iterable[AbstractDataSheetWriter]:
        return [UserExportSheetWriter(self)]


class UserExportSheetWriterHelpers:
    def __init__(self, exporter, sheet_writer):
        self.exporter = exporter
        self.sheet_writer = sheet_writer

    @staticmethod
    def __get_startupmember_best_degree_info(startupmember: StartupMember):
        """ Returns best_degree_class_year, best_degree_type, undergrad_or_grad.
                if best_degree is None, then best_degree_class_year and best_degree_type are ''.
                """

        best_degree = startupmember.get_most_recent_degree()

        (best_degree_class_year, best_degree_type) = \
            (best_degree.graduated_on, best_degree.degree) if best_degree else ('', '')

        undergrad_or_grad = startupmember.get_highest_degree_level_str()

        return best_degree_class_year, best_degree_type, undergrad_or_grad

    def __get_startupmember_funding_data(self, startupmember: StartupMember):
        """ Returns column data of startupmember specific to user funding export. """
        experience_set = startupmember.experience_set.select_related("member", "startup", "role") \
            .filter(role__isnull=False, startup__isnull=False, startup__university=self.exporter.university).distinct()

        st_name = ''
        st_website = ''
        user_position = ''
        st_industry = ''
        st_stage = ''
        st_stage_source = ''
        funding = ''
        funding_source = ''

        for experience in experience_set:
            st_name = experience.startup.name
            st_website = experience.startup.website if experience.startup.website else ''
            user_position = experience.role.get_name() if experience.role else ''
            st_industry = experience.startup.industry.get_name() if experience.startup.industry else ''
            # st_stage = ",".join(experience.startup.stages.all().values_list('title', flat=True))
            st_stage_source = ''
            investments = Investment.objects.filter(startup_id=experience.startup_id)
            funding = 0
            funding_source = ''
            for invest in investments:
                if invest.amount:
                    funding = funding + invest.amount
                funding_source += "{0}, ".format(invest.get_investor_name())
            funding = number_to_dollar_string(funding)
            if investments:
                break

        return [st_name, st_website, user_position, st_industry,
                st_stage, st_stage_source, funding, funding_source]

    def make_rows_from_startup_member(self, startupmember: StartupMember):
        """ Returns all the appropriate column data of startupmember  """
        uni_style = self.exporter.uni_style

        first_name = startupmember.first_name
        last_name = startupmember.last_name
        email_address = startupmember.user.email if startupmember.user else 'None (Placeholder Account)'

        date_created, last_updated = get_created_updated(self.sheet_writer, startupmember)

        best_degree_class_year, best_degree_type, undergrad_or_grad = \
            UserExportSheetWriterHelpers.__get_startupmember_best_degree_info(startupmember)

        if self.exporter.is_funding:
            column_data = [first_name, last_name, email_address, date_created, last_updated] + \
                          self.__get_startupmember_funding_data(startupmember) + \
                          [best_degree_class_year, best_degree_type, undergrad_or_grad]
        else:
            column_data = [first_name, last_name, email_address, date_created, last_updated,
                           best_degree_class_year, best_degree_type, undergrad_or_grad]

        custom = custom_dict_export(Form.MEMBER, startupmember, self.exporter.university)
        append_custom_data(custom, column_data)

        return [column_data]

    def make_rows_from_guest(self, guest: EventRSVP):
        """
        Returns all appropriate column data for the guest.

        Requires:
            The EventRSVP object is of a guest (not a member)
            The EventRSVP object is of an actual RSVP (not a wait-list)
        """
        if not guest.guest or guest.waitlisted:
            return None

        first_name = guest.guest.first_name
        last_name = guest.guest.last_name
        email_address = guest.guest.email
        date_created = self.sheet_writer.make_naive(guest.created)
        last_updated = date_created  # It's not possible to update guest RSVP records, so we use date created
        best_degree_class_year = ''
        best_degree_type = ''
        undergrad_or_grad = guest.get_affiliation_str() + " (Guest)"

        # We don't intend to allow guests to appear on user funding exports. But in case
        # we change our mind, we make sure columns are correctly positioned.
        if self.exporter.is_funding:
            column_data = [first_name, last_name, email_address, date_created, last_updated,
                           '', '', '', '', '', '', '', '',
                           best_degree_class_year, best_degree_type, undergrad_or_grad]
        else:
            column_data = [first_name, last_name, email_address, date_created, last_updated,
                           best_degree_class_year, best_degree_type, undergrad_or_grad]

        return [column_data]


class UserExportSheetWriter(AbstractDataSheetWriter):
    def __init__(self, exporter: UserExport):
        self.exporter = exporter
        self.helpers = UserExportSheetWriterHelpers(exporter, self)
        self.timezone_str = exporter.uni_style.timezone

        super().__init__(
            workbook=self.exporter.workbook,
            sheet_title='Users',
            header_rows=[self.__get_column_names()],
            section_breaks=set(),  # Empty set; there are no section breaks
            elements=self.__get_people(),
            header_bg_color=Colors.MOCCASIN,
            timezone=pytz.timezone(self.timezone_str)
        )

    def __get_column_names(self):
        uni_style = self.exporter.uni_style
        venture_word = uni_style.venture_word.capitalize()
        project_word = uni_style.project_word.capitalize()

        column_names = ["First Name", "Last Name", "Email Address",
                        f"Date Created ({self.timezone_str})",
                        f"Last Updated ({self.timezone_str})"]

        if self.exporter.is_funding:
            column_names += [
                "{0}/{1} Name".format(venture_word, project_word), "{0}/{1} Website".format(venture_word, project_word),
                "Position Title", "Industry", "Still in business? (Company Stage)", "Still in business source",
                "Funding raised", "Funding raised source (Investor Name)",
            ]

        column_names += ["Class Year", "Degrees", "Grad or Undergrad"]

        # Add custom questions to  column_names list
        append_custom_names(Form.MEMBER, self.exporter.university, column_names)

        return column_names

    def __get_startupmembers(self):
        """ Returns all startup members within appropriate start_date and end_date
        (and includes placeholder members if applicable). """
        query_user = get_query_user(self.exporter.date_range_exclusive, self.exporter.include_placeholder_members)

        startupmembers = StartupMember.objects.select_related("user") \
            .prefetch_related("experience_set", "admin_startups", "degrees__school") \
            .filter(query_user, universities=self.exporter.university) \
            .order_by('user__created_on')

        history_tags = self.exporter.history_tags
        if len(history_tags) > 0:
            if self.exporter.include_non_tagged:
                startupmembers = startupmembers.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=history_tags) | Q(num_tags=0)).distinct()
            else:
                startupmembers = startupmembers.filter(labels__in=history_tags)

        return startupmembers

    def __get_guests(self):
        """ Returns queryset of EventRSVP objects related to all guests on platform that
                 were created between start_date and end_date"""
        guests = EventRSVP.objects.select_related('guest').filter(
            rsvp_event__university__id=self.exporter.university.id,
            guest__isnull=False, waitlisted=False,
            created__gte=self.exporter.start_date, created__lte=self.exporter.end_date) \
            .distinct('guest__email')

        if len(self.exporter.history_tags) > 0 and not self.exporter.include_non_tagged:
            # if tags are selected (and not include non-tagged), then show no guests because guests can't be tagged.
            guests = EventRSVP.objects.none()

        return guests

    def __get_people(self):
        startupmembers = self.__get_startupmembers()

        if self.exporter.is_write_guests:
            guests = self.__get_guests()
            people = chain(startupmembers, guests)
        else:
            people = startupmembers

        return people

    def make_rows_from_elt(self, person):
        if isinstance(person, StartupMember):
            return self.helpers.make_rows_from_startup_member(person)

        if isinstance(person, EventRSVP):
            return self.helpers.make_rows_from_guest(person)
