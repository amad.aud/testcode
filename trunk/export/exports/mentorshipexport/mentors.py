from forms.models import Form
from forms.serializer.forms import custom_dict_export
from mentorship.models import Mentor
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import SECTION_BREAK, Colors
from trunk.export.exports.mentorshipexport.utils import get_hours_posted
from trunk.export.utils import append_custom_names, append_custom_data


class MentorsSheetWriter(AbstractDataSheetWriter[Mentor]):
    def __init__(self, exporter):
        """
        Requires: exporter is of type MentorshipExport.
        """
        self.exporter = exporter

        super().__init__(workbook=self.exporter.workbook,
                         sheet_title=self.exporter.words['plural_mentors'],
                         header_rows=[self.__get_column_names()],
                         section_breaks={0},
                         elements=self.exporter.mentors,
                         header_bg_color=Colors.MOCCASIN)

    def __get_column_names(self):
        mentor_word = self.exporter.words['mentor_word'].upper()
        full_mentor_word = f"Team {mentor_word}" if self.exporter.is_team else mentor_word
        column_names = [
            f"BREAKDOWN BY {full_mentor_word}",
            "Name", "Tag(s)", "Hours Posted", "Hours Scheduled"
        ]
        append_custom_names(Form.MENTOR, self.exporter.university, column_names)
        return column_names

    def make_rows_from_elt(self, mentor: Mentor):
        mentor_name = mentor.profile.get_full_name()
        mentor_tags = mentor.profile.labels.all().values_list('title', flat=True)
        if self.exporter.uni_style.alphabetize_tags:
            mentor_tags = mentor_tags.order_by('title')
        hours_posted = get_hours_posted(mentor, self.exporter.start_date, self.exporter.end_date,
                                        return_text_on_cronofy='True')
        # if cronofy, hours_posted = "Calendar Synced Dynamic Availability"
        hours_scheduled = 0
        sessions = self.exporter.get_sessions(mentor)
        for session in sessions:
            if session.duration == 1:
                hours_scheduled += 0.25
            elif session.duration == 2:
                hours_scheduled += 0.5
            elif session.duration == 3:
                hours_scheduled += 0.75
            elif session.duration == 4:
                hours_scheduled += 1

        row = SECTION_BREAK + [mentor_name, ", ".join(mentor_tags), hours_posted, hours_scheduled]

        custom = custom_dict_export(Form.MENTOR, mentor.profile, self.exporter.university)
        append_custom_data(custom, row)

        return [row]
