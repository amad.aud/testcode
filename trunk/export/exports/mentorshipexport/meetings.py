from branch.models import Label
from mentorship.models import MentorshipSession, TeamMentorshipSession
from StartupTree.loggers import app_logger
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import SECTION_BREAK, Colors
import pytz
from typing import Union


class MeetingsSheetWriter(AbstractDataSheetWriter):
    def __init__(self, exporter):
        self.exporter = exporter

        mentor_word = exporter.words['mentor_word']
        mentee_word = exporter.words['mentee_word']

        full_mentor_word = f"Team {mentor_word}" if self.exporter.is_team else mentor_word
        full_mentee_word = f"Team {mentee_word}" if self.exporter.is_team else mentee_word

        timezone_str = exporter.uni_style.timezone

        column_names = [
            "BREAKDOWN BY MEETING",
            f"{full_mentor_word} Tag(s)",
            f"{full_mentor_word} Name",
            f"{full_mentee_word} Name",
            f"{full_mentee_word} Email",
            f"Date/Time ({timezone_str})",
            "Meeting Type"
        ]

        super().__init__(workbook=exporter.workbook,
                         sheet_title='Meetings',
                         header_rows=[column_names],
                         section_breaks={0},
                         elements=self.exporter.all_sessions,
                         header_bg_color=Colors.LIGHTSKYBLUE,
                         timezone=pytz.timezone(timezone_str))

    def __get_names_and_email(self, session):
        if self.exporter.is_team:
            mentor_name = ', '.join(mentor.profile.get_full_name() for mentor in session.get_mentors())
            mentees = session.members_attending.all()
            mentee_name = ', '.join(student.get_full_name() for student in mentees)
            mentee_email = ', '.join(student.email for student in mentees)
        else:
            mentor_name = session.mentorship.mentor.profile.get_full_name()
            mentee_name = session.mentorship.student.get_full_name()
            mentee_email = session.mentorship.student.user.email

        return mentor_name, mentee_name, mentee_email

    def make_rows_from_elt(self, session: Union[MentorshipSession, TeamMentorshipSession]):
        if self.exporter.is_team:
            # Every label associated with every mentor attending this team session.
            mentor_tags = Label.objects.filter(
                startupmember__mentor_profile__mentorship__teammentorship__teammentorshipsession=session)\
                .values_list('title', flat=True)
        else:
            mentor_tags = session.mentorship.mentor.profile.labels.all().values_list('title', flat=True)
        if self.exporter.uni_style.alphabetize_tags:
            mentor_tags = mentor_tags.order_by('title')
        mentor_name, mentee_name, mentee_email = self.__get_names_and_email(session)
        meeting_datetime = self.make_naive(session.datetime)
        if session.meeting_type == 1:
            meeting_type = 'Phone'
        elif session.meeting_type == 3:
            meeting_type = 'Video Call'
        elif session.meeting_type == 4:
            meeting_type = 'In Person'
        else:
            app_logger.error(f'Invalid meeting_type value ({session.meeting_type}) for session: {session}')
            meeting_type = 'n/a'

        return [SECTION_BREAK + [', '.join(mentor_tags), mentor_name, mentee_name,
                                 mentee_email, meeting_datetime, meeting_type]]
