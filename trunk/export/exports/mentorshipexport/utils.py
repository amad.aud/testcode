from django.db.models import Q
from mentorship.models import MentorSchedule, MentorScheduleBlackOut
from mentorship.st_cronofy import is_cronofy_linked
from StartupTree.utils import QuerySetType
import datetime


def weekday_count(start, end):
    """
    get number of times each day of the week occurs in given date range
    adapted from https://stackoverflow.com/a/43692648
    """
    week = {}
    for i in range((end - start).days + 1):
        day = (start + datetime.timedelta(days=i)).weekday() + 1
        week[day] = week[day] + 1 if day in week else 1
    return week


def get_total_duration(schedule):
    """
    get total length of an availability block
    """
    start_time_str = str(schedule.start_time)
    end_time_str = str(schedule.end_time)
    end_time = datetime.datetime.strptime(end_time_str, "%H:%M:%S")
    start_time = datetime.datetime.strptime(start_time_str, "%H:%M:%S")
    time_delta = (end_time - start_time).seconds / 3600

    return time_delta


def get_choice_code_day(date):
    """Given a date, convert it to the correct choice code for MentorSchedule.day field. """
    dictionary = dict((day, code) for (code, day) in MentorSchedule.DAYS)
    key = date.strftime("%A")
    return dictionary[key]


def get_recurring_sessions(mentor, start_date, end_date) -> QuerySetType[MentorSchedule]:
    """Start date and end date are meant to be an inclusive range"""
    day_code = get_choice_code_day(start_date)
    return MentorSchedule.objects.filter(
        Q(end_date__gte=start_date) | Q(end_date__isnull=True),
        mentor=mentor,
        date__lte=end_date,
        is_repeating=True,
        day=day_code)


def get_hours_posted(mentor, start_date, end_date, return_text_on_cronofy=False):
    """
    Note: Cronofy does not allow api calls to the past, so we can't get past availability for cronofy mentors.
    If cronofy user, return 0.
    If cronofy user and return_text_on_cronofy, return string "Calendar Synced Dynamic Availability"
    """
    if is_cronofy_linked(mentor.profile.user):
        if return_text_on_cronofy:
            return "Calendar Synced Dynamic Availability"
        return 0

    if isinstance(start_date, datetime.datetime):
        start_date = start_date.date()
    if isinstance(end_date, datetime.datetime):
        end_date = end_date.date()

    hours_posted = 0
    blackout_dicts = []
    # first get hours from one-time availabilities in date range
    for schedule in MentorSchedule.objects.filter(mentor=mentor,
                                                  date__gte=start_date, date__lte=end_date,
                                                  is_repeating=False):
        time_delta = get_total_duration(schedule)
        hours_posted += time_delta

    # find blackout dates within date range and create a list of dicts
    for blackout in MentorScheduleBlackOut.objects.filter(mentor=mentor):
        if (blackout.start_date >= start_date and blackout.start_date <= end_date) or \
                (blackout.end_date <= end_date and blackout.end_date >= start_date):
            blackout_dict = weekday_count(blackout.start_date, blackout.end_date)
            blackout_dicts.append(blackout_dict)

    # check for recurring availabilities that exist within date range,
    # determine what date to start building the weekday dict from
    for schedule in get_recurring_sessions(mentor, start_date, end_date):
        # overlap range
        overlap_start = max(start_date, schedule.date)
        overlap_end = end_date if schedule.end_date is None else min(end_date, schedule.end_date)

        weekday_dict = weekday_count(overlap_start, overlap_end)

        # find blackout dicts that intersect with weekday dict and remove those dates
        for blackout_dict in blackout_dicts:
            for key, value in blackout_dict.items():
                if key in weekday_dict:
                    weekday_dict[key] -= blackout_dict[key]

        # check each weekday to see if mentor has recurring availability on that weekday
        for key, value in weekday_dict.items():
            if schedule.day == key:
                time_delta = get_total_duration(schedule)
                # multiply by number of times that weekday occurs in the date range
                hours_posted += (time_delta * value)
    return hours_posted
