from trunk.export.exports.mentorshipexport.abstractmentorshipexport import AbstractMentorshipExport


class MentorshipExport(AbstractMentorshipExport):
    def __init__(self, history):
        super().__init__(history, is_team=False)


class TeamMentorshipExport(AbstractMentorshipExport):
    def __init__(self, history):
        super().__init__(history, is_team=True)
