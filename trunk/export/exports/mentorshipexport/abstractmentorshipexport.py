from django.db.models import Q, Count
from mentorship.models import Mentor, MentorshipSession, TeamMentorshipSession
from trunk.export.abstract import AbstractUniversityExporter, AbstractSheetWriter
from trunk.export.exports.mentorshipexport.overview import OverviewSheetWriter
from trunk.export.exports.mentorshipexport.mentors import MentorsSheetWriter
from trunk.export.exports.mentorshipexport.meetings import MeetingsSheetWriter
from trunk.export.utils import make_words
from typing import List


class AbstractMentorshipExport(AbstractUniversityExporter):
    def __init__(self, history, is_team: bool):
        super().__init__(history)
        self.words = make_words(self.uni_style)
        self.is_team = is_team
        self.mentors = self.__get_mentors()
        self.all_sessions = self.__get_all_sessions()  # Qset of MentorshipSession or TeamMentorshipSession

    def get_sheet_writers(self) -> List[AbstractSheetWriter]:
        return [
            OverviewSheetWriter(self),
            MentorsSheetWriter(self),
            MeetingsSheetWriter(self),
        ]

    def __get_mentors(self):
        filter_date_range = Mentor.objects.query_date_range(self.start_date, self.end_date)

        mentors = Mentor.objects.select_related('profile').filter(
            filter_date_range, platform=self.university, is_complete=True)
        if self.is_team:
            mentors = mentors.filter(is_team_mentor=True)

        history_tags = self.history_tags
        if len(history_tags) > 0:
            if self.include_non_tagged:
                mentors = mentors.annotate(num_tags=Count('profile__labels')) \
                    .filter(Q(profile__labels__in=history_tags) | Q(num_tags=0)).distinct()
            else:
                mentors = mentors.filter(profile__labels__in=history_tags)

        return mentors

    def __get_all_sessions(self):
        """
        Gets all of the sessions on the platform.
        Depending on self.is_team, returned queryset will of MentorshipSession or TeamMentorshipSession
        """
        general_q = Q(
            datetime__date__gte=self.start_date,
            datetime__date__lte=self.end_date,
            is_cancelled=False,
        )

        if not self.is_team:  # is one-to-one mentorship
            sessions = MentorshipSession.objects.select_related('mentorship').filter(
                general_q,
                mentorship__mentor__in=self.mentors,
                teammentorshipsession__isnull=True,
            )
        else:  # is team mentorship
            sessions = TeamMentorshipSession.objects.filter(
                general_q,
                mentorships__mentor__in=self.mentors,
            )

        sessions.exclude(is_admin_accepted=False)  # We are including is_admin_accepted=None

        return sessions

    def get_sessions(self, mentor: Mentor):
        """Given a mentor, return the set of session within the export's start and end.
        Depending on self.is_team, returned queryset will of MentorshipSession or TeamMentorshipSession
        """
        if not self.is_team:  # is one-to-one mentorship
            sessions = self.all_sessions.filter(
                mentorship__mentor=mentor,
            )
        else:  # is team mentorship
            sessions = self.all_sessions.filter(
                mentorships__mentor=mentor,
            )

        return sessions



