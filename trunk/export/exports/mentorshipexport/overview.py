from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import SECTION_BREAK, Colors
from trunk.export.exports.mentorshipexport.utils import get_hours_posted


class OverviewSheetWriter(AbstractDataSheetWriter):
    def __init__(self, exporter):
        """
        Requires: exporter is of type MentorshipExport.
        """
        # self.exporter: AbstractMentorshipExport = exporter
        self.exporter = exporter

        start_date_str = exporter.start_date.strftime("%b %-d, %Y")
        end_date_str = exporter.end_date.strftime("%b %-d, %Y")

        column_names = [
            f'OVERVIEW: {start_date_str} - {end_date_str}',
            'Tag',
            'Total Hours Posted',
            'Total Hours Scheduled'
        ]

        if exporter.include_non_tagged:
            elements = []
        else:
            elements = exporter.history_tags

        super().__init__(workbook=self.exporter.workbook,
                         sheet_title='Overview',
                         header_rows=[column_names],
                         section_breaks={0},
                         elements=elements,
                         header_bg_color=Colors.LIGHTGREEN)

    def __make_rows_from_mentors(self, title, mentors):
        """ Given a set of mentors, produce the data rows."""
        start_date = self.exporter.start_date
        end_date = self.exporter.end_date
        total_hours_posted = 0
        total_hours_scheduled = 0

        for mentor in mentors:
            mentor_hours_posted = get_hours_posted(mentor, start_date, end_date)
            mentor_hours_scheduled = 0
            sessions = self.exporter.get_sessions(mentor)
            for session in sessions:
                if session.duration == 1:
                    mentor_hours_scheduled += 0.25
                elif session.duration == 2:
                    mentor_hours_scheduled += 0.5
                elif session.duration == 3:
                    mentor_hours_scheduled += 0.75
                elif session.duration == 4:
                    mentor_hours_scheduled += 1

            total_hours_posted += mentor_hours_posted
            total_hours_scheduled += mentor_hours_scheduled

        return [SECTION_BREAK + [title, total_hours_posted, total_hours_scheduled]]

    def make_rows_from_elt(self, tag):
        mentor_list = []
        for mentor in self.exporter.mentors:
            if tag in mentor.profile.labels.all() and mentor not in mentor_list:
                mentor_list.append(mentor)
        return self.__make_rows_from_mentors(tag.title, mentor_list)

    def make_footer_rows(self):
        note = "(Tagged and Untagged)" if self.exporter.include_non_tagged else "(Tagged Only)"
        return self.__make_rows_from_mentors(f'All {note}', self.exporter.mentors)
