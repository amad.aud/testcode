from forms.models import (
    Rubric,
)
from trunk.export.exports.judge_export.abstract_rubics_sheet import (
    AbstractRubricsSheet,
    HEADER_COLOR_PRIMARY,
    HEADER_COLOR_QUESTION,
)
from trunk.export.exports.judge_export.types import ApplicantInfo
from trunk.models import UniversityStyle

from typing import Iterable, List, Tuple


class ByApplicantsSheet(AbstractRubricsSheet):
    def __init__(self, exporter):   # exporter is of type trunk.export.JudgeExport
        super().__init__(
            exporter=exporter,
            sheet_title='Judge Rubrics - By Applicants'
        )

    def get_elements(self) -> Iterable[ApplicantInfo]:
        return self.all_applicants

    def get_header_rows_and_header_colors(self) -> Tuple[List[list], List[list]]:
        competition = self.competition
        require_venture = self.require_venture

        # # # # # # # # # # #
        # PRE QUESTION COLS #
        # # # # # # # # # # #

        participant_word = self.participant_word

        header_first = ["Total Average Across Judges", "Total Score from Judge", f"{participant_word} Name", f"{participant_word} Email", "Judge Name"]

        if self.is_multi_judge_boards:
            header_first += ["Judge Round"]

        if require_venture:
            uni_style: UniversityStyle = competition.university.universitystyle_set.first()
            venture_word = uni_style.venture_word.capitalize()
            header_first += [f"{venture_word} Name"]

        header_second = [None] * len(header_first)

        header_colors_second = [HEADER_COLOR_PRIMARY] * len(header_first)

        # # # # # # # # #
        # QUESTION COLS #
        # # # # # # # # #

        question_labels, question_texts = self.get_question_labels_and_texts()

        header_first += question_labels
        header_second += question_texts

        header_colors_second += [HEADER_COLOR_QUESTION] * len(question_labels)

        # # # # # # # # # # # #
        # POST QUESTION ROWS  #
        # # # # # # # # # # # #

        header_first_end = ['Rank', 'Comment']

        header_first += header_first_end

        header_colors_second += [HEADER_COLOR_PRIMARY] * len(header_first_end)

        header_second += [None] * len(header_first_end)

        # # # #
        # END #
        # # # #

        header_colors_first = [HEADER_COLOR_PRIMARY] * len(header_first)

        return [header_first, header_second], \
               [header_colors_first, header_colors_second]

    def make_rows_from_elt(self, applicant_info: ApplicantInfo) -> Iterable[list]:
        rows = []

        for judge_review in applicant_info['applicant_judge_reviews']:
            row_info = self.make_row_info(applicant_info, judge_review.judge)

            # # # # # # # # # # #
            # PRE QUESTION COLS #
            # # # # # # # # # # #

            row = [
                applicant_info['total_average'], row_info['total'],
                row_info['participant_name'], row_info['participant_email'],
                row_info['judge_name']
            ]

            if self.is_multi_judge_boards:
                row += [row_info['judge_round']]

            if self.require_venture:
                row += [row_info['venture_name']]

            # # # # # # # # #
            # QUESTION COLS #
            # # # # # # # # #

            row += self.get_rubric_answer_cols(row_info)

            # # # # # # # # # # # #
            # POST QUESTION COLS  #
            # # # # # # # # # # # #

            row += row_info['rank'], row_info['comment']

            rows.append(row)

        return rows
