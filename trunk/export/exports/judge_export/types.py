from forms.models import Rubric, RubricAnswer
from StartupTree.utils import QuerySetType
from trunk.models import JudgeReview
from typing import Dict, Optional, TypedDict


class ApplicantInfo(TypedDict):
    name: str
    email: str
    venture_name: Optional[str]
    rubric_answers: QuerySetType[RubricAnswer]
    applicant_judge_reviews: QuerySetType[JudgeReview]
    total_average: float


class RubricRowInfo(TypedDict):
    participant_name: str
    participant_email: str
    judge_name: str
    judge_round: int
    venture_name: Optional[str]
    total: int
    rank: int
    comment: str
    rubric_answers: Dict[Rubric, RubricAnswer]
