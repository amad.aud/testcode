from forms.models import Rubric
from StartupTree.utils import QuerySetType
from trunk.export.exports.judge_export.abstract_rubics_sheet import (
    AbstractRubricsSheet,
    HEADER_COLOR_PRIMARY,
    HEADER_COLOR_QUESTION,
)
from trunk.export.exports.judge_export.types import ApplicantInfo
from trunk.models import Judge, UniversityStyle

from typing import Iterable, List, Tuple


class ByJudgesSheet(AbstractRubricsSheet):
    def __init__(self, exporter):  # exporter is of type trunk.export.JudgeExport
        super().__init__(
            exporter=exporter,
            sheet_title='Judge Rubrics - By Judges',
        )

    def get_elements(self) -> QuerySetType[Judge]:
        return self.judges

    def get_header_rows_and_header_colors(self) -> Tuple[List[list], List[list]]:
        competition = self.competition
        require_venture = self.require_venture

        # # # # # # # # # # #
        # PRE QUESTION COLS #
        # # # # # # # # # # #

        participant_word = self.participant_word

        header_first = [f"{participant_word} Name", f"{participant_word} Email", "Judge Name"]

        if self.is_multi_judge_boards:
            header_first += ["Judge Round"]

        if require_venture:
            uni_style: UniversityStyle = competition.university.universitystyle_set.first()
            venture_word = uni_style.venture_word.capitalize()
            header_first += [f"{venture_word} Name"]

        header_second = [None] * len(header_first)

        header_colors_second = [HEADER_COLOR_PRIMARY] * len(header_first)

        # # # # # # # # #
        # QUESTION COLS #
        # # # # # # # # #

        question_labels, question_texts = self.get_question_labels_and_texts()

        header_first += question_labels
        header_second += question_texts

        header_colors_second += [HEADER_COLOR_QUESTION] * len(question_labels)

        # # # # # # # # # # # #
        # POST QUESTION ROWS  #
        # # # # # # # # # # # #

        header_first_end = ['Total', 'Rank', 'Comment']

        header_first += header_first_end

        header_colors_second += [HEADER_COLOR_PRIMARY] * len(header_first_end)

        header_second += [None] * len(header_first_end)

        # # # #
        # END #
        # # # #

        header_colors_first = [HEADER_COLOR_PRIMARY] * len(header_first)

        return [header_first, header_second], \
               [header_colors_first, header_colors_second]

    def make_rows_from_elt(self, judge: Judge) -> Iterable[list]:
        rows = []

        def is_applicant_info_judged(applicant_info_: ApplicantInfo):
            return applicant_info_['applicant_judge_reviews'].filter(judge=judge).exists()

        applicant_infos_judged = filter(is_applicant_info_judged, self.all_applicants)

        for applicant_info in applicant_infos_judged:
            row_info = self.make_row_info(applicant_info, judge)

            # # # # # # # # # # #
            # PRE QUESTION COLS #
            # # # # # # # # # # #

            row = [row_info['participant_name'], row_info['participant_email'], row_info['judge_name']]

            if self.is_multi_judge_boards:
                row += [row_info['judge_round']]

            if self.require_venture:
                row += [row_info['venture_name']]

            # # # # # # # # #
            # QUESTION COLS #
            # # # # # # # # #

            row += self.get_rubric_answer_cols(row_info)

            # # # # # # # # # # # #
            # POST QUESTION ROWS  #
            # # # # # # # # # # # #

            row += [row_info['total'], row_info['rank'], row_info['comment']]

            rows.append(row)

        return rows
