from forms.models import (
    MemberApplicant,
    AnonymousApplicant,
    Rubric,
    RubricAnswer,
)
from StartupTree.utils import QuerySetType
from trunk.export.abstract import AbstractDataSheetWriter
from trunk.export.constants import Colors
from trunk.export.exports.judge_export.types import (
    ApplicantInfo,
    RubricRowInfo,
)
from trunk.models import (
    Judge,
    JudgeReview,
)
from abc import abstractmethod
from typing import Iterable, List, Tuple, TypeVar, Union


E = TypeVar('E')

HEADER_COLOR_PRIMARY = Colors.LIGHTGREEN
HEADER_COLOR_QUESTION = Colors.MOCCASIN


class AbstractRubricsSheet(AbstractDataSheetWriter):
    def __init__(self, exporter, sheet_title: str):
        from trunk.export import JudgeExport  # prevent circular import
        self.exporter: JudgeExport = exporter

        competition = self.exporter.competition

        self.competition = competition
        self.rubrics = competition.form.rubrics.all().order_by('round', 'number')
        self.participant_word = "Applicant" if self.competition.is_application else "Participant"
        self.require_venture = competition.form.requires_venture  # => there is a venture name column
        self.is_multi_judge_boards = competition.judge_boards.all().count() > 1

        judges = Judge.objects.filter(event_id=competition.id, is_archived=False).order_by('round', 'first_name')

        self.judges = judges
        self.rubric_answers = RubricAnswer.objects\
            .select_related('applicant', 'anonymous_applicant', 'judge', 'r')\
            .filter(judge__in=judges)
        self.judge_reviews = JudgeReview.objects\
            .select_related('applicant', 'anonymous_applicant', 'judge')\
            .filter(judge__in=judges)

        self.all_applicants = self.get_all_applicant_info()

        header_rows, header_colors = self.get_header_rows_and_header_colors()

        super().__init__(
            workbook=exporter.workbook,
            sheet_title=sheet_title,
            header_rows=header_rows,
            elements=self.get_elements(),
            header_bg_color=header_colors,
            draw_border_after_row_group=True,
        )

    @abstractmethod
    def get_elements(self) -> Iterable[E]:
        pass

    @abstractmethod
    def get_header_rows_and_header_colors(self) -> Tuple[List[list], List[list]]:
        pass

    def make_row_info(self, applicant_info: ApplicantInfo, judge: Judge) -> RubricRowInfo:
        judge_review = applicant_info['applicant_judge_reviews'].filter(judge=judge).last()
        # TODO later implement unique constraint on JudgeReview between judge applicant/anonymous_applicant

        rubric_answers = applicant_info['rubric_answers'].filter(judge=judge)

        rubric_answer: RubricAnswer

        return {
            'participant_name': applicant_info['name'],
            'participant_email': applicant_info['email'],
            'judge_name': judge.get_name(),
            'judge_round': judge.round,
            'venture_name': applicant_info['venture_name'],
            'total': judge_review.score,
            'rank': judge_review.overall_rank,
            'comment': judge_review.comment,
            'rubric_answers': {
                rubric_answer.r: rubric_answer.get_answer()
                for rubric_answer in rubric_answers
            }
        }

    def get_all_applicant_info(self) -> List[ApplicantInfo]:
        """
        Requires:
            self.rubric_answers is defined
        """
        member_applicants = self.competition.get_member_applicants()
        anonymous_applicants = self.competition.get_anonymous_applicants()

        member_applicant_infos = list(map(self.get_applicant_info, member_applicants))
        anonymous_applicant_infos = list(map(self.get_applicant_info, anonymous_applicants))

        all_infos = member_applicant_infos + anonymous_applicant_infos

        return sorted(all_infos, key=lambda info: info['name'])

    def get_applicant_info(self, applicant: Union[MemberApplicant, AnonymousApplicant]) \
            -> ApplicantInfo:
        """
        Requires:
            self.rubric_answers is defined
        """
        if isinstance(applicant, MemberApplicant):
            name = applicant.member.get_full_name()
            email = applicant.member.user.email
            venture_name = applicant.venture
            answers_filter_applicant = {'applicant': applicant.member}
            reviews_filter_applicant = {'applicant': applicant.member}

        elif isinstance(applicant, AnonymousApplicant):
            name = applicant.name
            email = applicant.email
            venture_name = applicant.venture
            answers_filter_applicant = {'anonymous_applicant': applicant}
            reviews_filter_applicant = {'anonymous_applicant': applicant}

        else:
            raise ValueError('Invalid Applicant Type.')

        rubric_answers = \
            self.rubric_answers\
                .filter(**answers_filter_applicant)\
                .order_by('r__round', 'r__number')

        applicant_judge_reviews = \
            self.judge_reviews\
                .filter(**reviews_filter_applicant)\
                .order_by('judge__round', 'judge__first_name')

        total_average = \
            self.__class__.get_applicant_total_average(applicant_judge_reviews)

        return {
            'name': name,
            'email': email,
            'venture_name': venture_name,
            'rubric_answers': rubric_answers,
            'applicant_judge_reviews': applicant_judge_reviews,
            'total_average': total_average,
        }

    def get_question_labels_and_texts(self) -> Tuple[List[str], List[str]]:
        def get_question_label(rubric: Rubric) -> str:
            if self.is_multi_judge_boards:
                return f"Round {rubric.round} - Question {rubric.number}"
            else:
                return f"Question {rubric.number}"

        def get_question_text(rubric: Rubric) -> str:
            question_text = f"{rubric.text} - {rubric.get_rubric_type()}"
            if rubric.cap:
                question_text += f" Max({rubric.cap})"
            return question_text

        labels = list(map(get_question_label, self.rubrics))
        texts = list(map(get_question_text, self.rubrics))

        return labels, texts

    def get_rubric_answer_cols(self, row_info: RubricRowInfo) -> list:
        def get_answer(rubric: Rubric):
            return row_info['rubric_answers'].get(rubric)  # returns None if judge did not submit

        return list(map(get_answer, self.rubrics))

    @staticmethod
    def get_applicant_total_average(applicant_judge_reviews: QuerySetType[JudgeReview]) -> float:
        """
        Returns 0 if number of reviews for the applicant is zero. (division by zero case).
        """
        review: JudgeReview
        valid_reviews = applicant_judge_reviews.filter(score__isnull=False)
        total = sum(review.score for review in valid_reviews)

        try:
            return total / valid_reviews.count()
        except ZeroDivisionError:
            return 0
