from trunk.export.abstract import AbstractUniversityExporter, AbstractDataSheetWriter
from trunk.export.exports.judge_export.by_applicants_sheet import ByApplicantsSheet
from trunk.export.exports.judge_export.by_judges_sheet import ByJudgesSheet
from trunk.models import UniversityExportHistory
from typing import Iterable


class JudgeExport(AbstractUniversityExporter):
    def __init__(self, history: UniversityExportHistory):
        super().__init__(history)
        self.competition = history.competition

    def get_sheet_writers(self) -> Iterable[AbstractDataSheetWriter]:
        return [
            ByApplicantsSheet(self),
            ByJudgesSheet(self),
        ]
