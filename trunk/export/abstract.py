from StartupTree.loggers import app_logger
from trunk.export.constants import (
    HEADER_FORMAT_PROPS,
    DATA_FORMAT_PROPS,
    MAX_CELL_WORD_LENGTH
)
from trunk.export.types import CellFormats, HeaderColorData
from trunk.models import UniversityExportHistory
from abc import ABC, abstractmethod
from datetime import datetime, date, tzinfo, timedelta
from io import BytesIO
from typing import Generic, Iterable, List, Optional, TypeVar, Union
import xlsxwriter
from xlsxwriter.format import Format

E = TypeVar('E')


class AbstractSheetWriter:
    """
    Abstract class for writing a sheet in a workbook.

    NOTE: This class only handles sheets that only have a header row (and optionally footer rows).
    To write a header row and data rows (and optionally footer rows),
    Implement AbstractDataSheetWriter instead

    Parameters:
        Column Names:
            A list of list of header cell names.
            (List of list, because we may want multiple rows for header).

        section_breaks:
            A list of indexes where section breaks are (for formatting purposes)

        header_bg_color:
            If provided, may be a string (of the bg color for entire header),
            or a list of list of strings (of colors for each cell of each row of the header).

            If a list is supplied, the list must be as long as the number of rows in the header,
            and each row needs to be as long as the number of columns.

        timezone:
            This parameter is only required if using AbstractSheetWriter.make_naive(dt) (writing datetime objects to excel).

    Optionally Override:
        make_footer_rows:
            A function that returns a list of footer rows.

    About Single data-row spreadsheets:
        Some spreadsheets may only require a single row to be rendered. In that case, `make_footer_rows` is defined
        to render the desired overview row. (Eg. the Analytics Sheet of General Export)

    About Dates:

        Date objects (that are not datetimes) are not timezone aware; thus we assume that any date supplied
        is correct for the intended timezone.

        For example, if the date object is derived from an aware dt, convert the dt to the intended timezone before
        supplying its date. ie instead of supplying dt.date(), supply dt.astimezone(tz).date()

    About Datetimes:

        Excel does not support timezone aware datetimes. Any tz aware datetimes should be converted to the intended
        timezone and set the tzinfo in the datetime object to None.

        A helper method AbstractSheetWriter.make_naive(dt) is provided to help.

        As a convention, it could be wise to include the string representation of the timezone as part of the
        column title. eg: f"Registration Time ({timezone})" yields "Registration Time (US/Eastern)"
    """
    def __init__(self,
                 workbook: xlsxwriter.Workbook,
                 sheet_title: str,
                 header_rows: List[List[str]],
                 section_breaks: Iterable[int] = frozenset(),
                 header_bg_color: HeaderColorData = None,
                 draw_border_after_row_group: bool = False,
                 timezone: tzinfo = None):

        self.workbook = workbook
        self.sheet = workbook.add_worksheet(sheet_title)
        self.header_rows = header_rows
        self.section_breaks = section_breaks
        self.cell_formats = self.make_cell_formats(header_bg_color=header_bg_color)
        self.cell_formats_bottom_border = self.make_cell_formats(header_bg_color=header_bg_color, bottom_border=True) \
            if draw_border_after_row_group else None
        self.timezone = timezone
        self.max_cell_word_length = MAX_CELL_WORD_LENGTH
        self.column_width = [0 for _ in header_rows[0]]
        self.base_row_number = 0

    def write(self):
        """ Writes sheet to self.workbook """
        self.write_header()
        self.write_footer()
        self.set_columns()

    def make_naive(self, dt: datetime):
        """ Helper method that takes a timezone aware dt, converts it to the intended tz (the timezone
        of the platform), and then sets the tzinfo of the datetime object to None.

        This is useful when writing datetime objects to excel cells. Excel does not support
        timezone aware datetime objects; so datetimes we give to Excel should be made naive and representing
        the time in the intended timezone.

        Use this method on all datetime aware objects you want to write to excel.

        requires: self.timezone to be defined. Otherwise, raises ValueError
         """
        is_aware = dt.tzinfo is not None and dt.tzinfo.utcoffset(dt) is not None
        if is_aware:
            if not self.timezone:
                raise ValueError("AbstractSheetWriter.timezone is undefined.")
            return dt.astimezone(self.timezone).replace(tzinfo=None)
        else:
            app_logger.warning('Supplied datetime is already Naive, and assumed represent the correct time.')
            return dt

    def make_footer_rows(self) -> Iterable[list]:
        """
        Override this method to write footers to the sheet.

        Return a list of list of entries to be written.
        """
        pass

    def make_cell_formats(self,
                          header_bg_color: Optional[HeaderColorData],
                          bottom_border: bool = False) \
            -> CellFormats:
        """
        Requires: self.workbook
        Optional: header_bg_color
        """
        data_format_props = DATA_FORMAT_PROPS.copy()
        data_format = self.workbook.add_format(data_format_props)
        if bottom_border:
            data_format.set_bottom()

        data_date_format_props = DATA_FORMAT_PROPS.copy()
        data_date_format_props['num_format'] = 'mmm d, yyyy'
        data_date_format = self.workbook.add_format(data_date_format_props)
        if bottom_border:
            data_date_format.set_bottom()

        data_datetime_format_props = DATA_FORMAT_PROPS.copy()
        data_datetime_format_props['num_format'] = 'mmm d, yyyy h:mm AM/PM'
        data_datetime_format = self.workbook.add_format(data_datetime_format_props)
        if bottom_border:
            data_datetime_format.set_bottom()

        return {
            'header': self.make_header_cell_formats(header_bg_color, bottom_border),
            'data': data_format,
            'data_date': data_date_format,
            'data_datetime': data_datetime_format,
        }

    def make_header_cell_formats(self, header_bg_color: Optional[HeaderColorData], bottom_border: bool) -> Union[Format, List[List[Format]]]:
        def make_header_cell_format(color_single: Optional[str]) -> Format:
            header_format_props = HEADER_FORMAT_PROPS.copy()
            header_format_props['bg_color'] = color_single
            header_format = self.workbook.add_format(header_format_props)
            if bottom_border:
                header_format.set_bottom()
            return header_format

        def make_header_cell_format_row(color_row: List[str]) -> List[Format]:
            return list(map(make_header_cell_format, color_row))

        if header_bg_color:
            if isinstance(header_bg_color, str):
                return make_header_cell_format(header_bg_color)
            elif isinstance(header_bg_color, list):
                return list(map(make_header_cell_format_row, header_bg_color))
        else:
            return make_header_cell_format(color_single=None)

    def get_header_cell_format(self, column_number, cell_formats):
        if isinstance(cell_formats['header'], list):
            header_format_row = self.base_row_number if self.is_row_header() else len(self.header_rows) - 1
            return cell_formats['header'][header_format_row][column_number]
        else:
            return cell_formats['header']

    def is_row_header(self):
        return self.base_row_number < len(self.header_rows)

    def write_header(self):
        self.write_rows(self.header_rows)

    def set_new_column_width(self, column_number, entry):
        entry_len = len(str(entry)) + 1
        self.column_width[column_number] = max(entry_len, self.column_width[column_number])

    def get_cell_format(self, column_number, entry, is_bottom_row):
        """
        If column number is a break column, we return the
        section break format (which happens to be the same format as header)

        If entry is a date/datetime, we return the date/datetime format.

        For all other cases, we return the regular cell data format.
        """
        cell_formats = self.cell_formats_bottom_border \
            if self.cell_formats_bottom_border and is_bottom_row \
            else self.cell_formats

        if self.is_row_header():
            return self.get_header_cell_format(column_number, cell_formats)
        if column_number in self.section_breaks:
            return self.get_header_cell_format(column_number, cell_formats)
        if isinstance(entry, datetime):
            return cell_formats['data_datetime']
        if isinstance(entry, date) and not isinstance(entry, datetime):
            return cell_formats['data_date']
        return cell_formats['data']

    def write_rows(self, data_rows):
        for row_idx, data_row in enumerate(data_rows):
            for column_number, entry in enumerate(data_row):
                is_bottom_row = row_idx == len(data_rows) - 1
                cell_format = self.get_cell_format(column_number, entry, is_bottom_row)
                self.sheet.write(self.base_row_number, column_number, entry, cell_format)
                self.set_new_column_width(column_number, entry)
            self.base_row_number += 1

    def write_footer(self):
        if self.make_footer_rows:
            footer_rows = self.make_footer_rows()
            if footer_rows:
                self.write_rows(footer_rows)

    def set_columns(self):
        for column_number, max_width in enumerate(self.column_width):
            # Set the columns to proper width, and color them if they are a section break column
            width = min(max_width, self.max_cell_word_length)
            new_cell_format = None if column_number not in self.section_breaks \
                else self.get_header_cell_format(column_number, self.cell_formats)
            self.sheet.set_column(column_number, column_number, width, new_cell_format)


class AbstractDataSheetWriter(AbstractSheetWriter, Generic[E], ABC):
    """
    elements:
            Iterable of elements, from which we make rows from.

    Required Override:
        make_rows_from_elt:
            Method that takes an element and returns rows to be written
    """
    def __init__(self,
                 workbook: xlsxwriter.Workbook,
                 sheet_title: str,
                 header_rows: List[List],
                 elements: Iterable[E],
                 section_breaks: Iterable[int] = frozenset(),
                 header_bg_color: HeaderColorData = None,
                 draw_border_after_row_group: bool = False,
                 timezone: tzinfo = None):

        super().__init__(
            workbook=workbook,
            sheet_title=sheet_title,
            header_rows=header_rows,
            section_breaks=section_breaks,
            header_bg_color=header_bg_color,
            draw_border_after_row_group=draw_border_after_row_group,
            timezone=timezone)

        self.data_elements = elements

    @abstractmethod
    def make_rows_from_elt(self, elt: E) -> Iterable[list]:
        """
        A function that takes an element and returns a list of rows.
        (it could be the case that we want to render more than one row, for each element).
        Since each row is a list of entries, the return type of make_rows_from_elt is a list of list of entries.
        """
        pass

    def write(self):
        """ Writes sheet to self.workbook """
        self.write_header()
        self.write_data()
        self.write_footer()
        self.set_columns()

    def write_data(self):
        if self.data_elements and self.make_rows_from_elt:
            for elt in self.data_elements:
                data_rows = self.make_rows_from_elt(elt)
                self.write_rows(data_rows)


class AbstractUniversityExporter(ABC):
    """
    Abstract class for creating exports

    Required Implementation:
        get_sheet_writers():
            A function that, when called, returns a list of SheetWriter objects (implementations of AbstractSheetWriter)

    About date_range_exclusive:
        A tuple (range_start, range_end).

        This tuple meant to be supplied as parameter for django date range filtering.

        e.g. StartupMember.objects.filter(user__created_on__range=self.exporter.date_range_exclusive)

        If history.start_date and history.end_date are defined,

            self.date_range_exclusive = (range_start, range_end + timedelta(days=1))

            The reason we add 1 to the range is because django range filtering is exclusive of the end date.

    """
    def __init__(self, history: UniversityExportHistory):
        self.university = history.university
        self.history = history
        self.history_tags = history.tags.all()
        self.include_non_tagged = history.include_non_tagged
        self.include_placeholder_members = history.include_placeholder_members
        self.exclude_admin_only_startup_custom_questions = history.exclude_admin_only_startup_custom_questions

        self.start_date = history.start_date
        self.end_date = history.end_date
        # Note: For date range, we add 1 to make date_range inclusive of the end date.
        self.date_range_exclusive = (self.start_date, self.end_date + timedelta(days=1)) \
            if self.start_date and self.end_date else None

        self.uni_style = self.university.universitystyle_set.first()

        self.attachment = BytesIO()
        self.workbook = xlsxwriter.Workbook(self.attachment)

    @abstractmethod
    def get_sheet_writers(self) -> Iterable[AbstractSheetWriter]:
        pass

    def __write(self):
        sheet_writers = self.get_sheet_writers()
        for sheet_writer in sheet_writers:
            sheet_writer.write()

    def __save_and_close(self):
        self.workbook.close()
        self.attachment.seek(0)
        self.history.in_progress = False
        self.history.data = self.attachment.read()
        self.history.save()
        self.attachment.close()

    def __exception_handler(self, e: Exception):
        self.history = UniversityExportHistory.objects.get(id=self.history.id)
        self.history.in_progress = None
        self.history.save()
        self.attachment.close()
        raise e

    def export(self):
        try:
            self.__write()
            self.__save_and_close()
        except Exception as e:
            self.__exception_handler(e)
