MAX_CELL_WORD_LENGTH = 50


class Colors:
    LIGHTGREEN = '#90EE90'      # General Export - Analytics Sheet  |  # Mentorship Export - Overview Sheet
    MOCCASIN = '#FFE4B5'        # General Export - Users Sheet      |  # Mentorship Export - Mentors Sheet
    AZURE = '#F0FFFF'           # General Export - Guests Sheet
    LIGHTSKYBLUE = '#87CEFA'    # General Export - Projects Sheet   |  # Mentorship Export - Meetings Sheet
    THISTLE = '#D8BFD8'         # General Export - Ventures Sheet
    LIGHTCORAL = '#F08080'      # General Export - Events Sheet


HEADER_FORMAT_PROPS = {
    'bold': True,
    'bg_color': Colors.LIGHTGREEN,
    'text_wrap': True
}

DATA_FORMAT_PROPS = {
    'text_wrap': True,
    'align': 'right'
}

SECTION_BREAK = [None]  # Section breaks are left blank (and will be formatted with color)
