from branch.models import Startup, StartupMember
from forms.models import Form, Question
from StartupTree.utils import make_plural
from trunk.export.constants import SECTION_BREAK
from trunk.export.abstract import AbstractSheetWriter
from reversion.models import Version
from typing import Iterable, List, Tuple, Union


def make_words(uni_style):
    mentor_word = uni_style.mentor_word.capitalize()
    plural_mentors = make_plural(mentor_word)

    mentee_word = uni_style.mentee_word.capitalize()
    plural_mentees = make_plural(mentee_word)

    venture_word = uni_style.venture_word.capitalize()
    plural_ventures = make_plural(venture_word)

    project_word = uni_style.project_word.capitalize()
    plural_projects = make_plural(project_word)

    words = {
        'mentor_word': mentor_word,
        'plural_mentors': plural_mentors,
        'mentee_word': mentee_word,
        'plural_mentees': plural_mentees,
        'venture_word': venture_word,
        'plural_ventures': plural_ventures,
        'project_word': project_word,
        'plural_projects': plural_projects,
    }

    return words


def get_column_names_and_breaks(column_names__grouped: Iterable[Iterable[str]]) -> Tuple[List[str], Iterable[int]]:
    """
    Given groups of column names (and assuming the beginning of each group represents a column break),
    return a flat list of all column names and a set of all column break indices.
    """
    column_names = []
    section_breaks = set()

    for column_name_group in column_names__grouped:
        # section break at beginning of each section, including index 0
        section_breaks.add(len(column_names))
        column_names += column_name_group

    return column_names, section_breaks


def append_custom_names(form, university, column_names, exclude_admin_only=False):
    """
    Appends the custom field names to the list of column names for exportt.

    column_names: a list to be mutated with custom field names appended.
    """
    if Form.objects.filter(form=form, university_id=university.id):
        form = Form.objects.get(form=form, university_id=university.id)
        custom_questions = form.questions.filter(is_custom=True)
        if exclude_admin_only:
            custom_questions = custom_questions.filter(is_uni_only=False)
        custom_questions = custom_questions.order_by('number', 'id')
        for q in custom_questions:
            column_names.append("{0}".format(q.text))


def append_custom_data(custom, column_data):
    """
    custom is returned from forms.serializers.forms.custom_dict_export()
    see custom_dict_export for formal spec on format
    """
    for item in custom:
        if item['q_type_code'] == Question.MC or item['q_type_code'] == Question.DD:
            if item['q_choices']:
                choices = [choice['text'] for choice in item['q_choices'] if choice['chosen']]
                column_data.append(', '.join(choices))
            else:
                column_data.append(None)
        elif item['q_type_code'] == Question.PP:
            column_data.append(item['a_text'])
        elif item['q_type_code'] == Question.DT:
            column_data.append(item['a_date_obj'])
        elif item['q_type_code'] == Question.FU:
            column_data.append(item['a_filename'])


def combine_startup_and_team_rows(startup_data: list, team_rows: Iterable[list]):
    """
    Returns: [
        [section break][... Startup x Data ...][section break][... Team Member 1 Data ...],
        [section break][... blank columns  ...][section break][... Team Member 2 Data ...],
        ...
        [section break][... blank columns  ...][section break][... Team Member n Data ...],
    ]
    """

    startup_data_padding = [None] * (len(SECTION_BREAK) + len(startup_data))

    full_rows = []
    for team_row in team_rows:
        if len(full_rows) == 0:
            # This is first row, so we include startup_data:
            full_row = SECTION_BREAK + startup_data + SECTION_BREAK + team_row
        else:
            full_row = startup_data_padding + SECTION_BREAK + team_row

        full_rows.append(full_row)

    if len(full_rows) == 0:
        # There were no team rows, so instead we just return startup data.
        full_rows = [SECTION_BREAK + startup_data]

    return full_rows


def get_created_updated(sheet_writer: AbstractSheetWriter, obj: Union[Startup, StartupMember]):
    """
    Given an object (Startup or StartupMember), return the date created and last updated datetime objects.
    These datetime objects will be converted to the write timezone and made naive objects (so that they are
    suitable for writing to excel, which doesn't support aware datetimes).

    sheet_writer instance is passed solely for the purpose of accessing the method sheet_writer.make_naive() to
    convert the aware datetime to a naive one with the proper timezone

    if Version objects don't exist for the object:
        last_updated is None
        date_created is obj's created_on field:
            if obj is Startup, then obj.created_on (a date field)
            if obj is StartupMember, then obj.user.created_on (a datetime field)
                But if obj.user is None (member is a placeholder), then None
    """
    available_versions = Version.objects.get_for_object(obj)

    if available_versions:
        last: Version = available_versions.last()
        first: Version = available_versions.first()
        date_created = sheet_writer.make_naive(last.revision.date_created)
        last_updated = sheet_writer.make_naive(first.revision.date_created)

    else:
        last_updated = None
        date_created = None

        if isinstance(obj, Startup):
            date_created = obj.created_on  # date instance (not datetime)
        else:
            obj: StartupMember
            if obj.user:
                date_created = sheet_writer.make_naive(obj.user.created_on)

    return date_created, last_updated
