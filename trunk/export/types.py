from typing import List, Optional, TypedDict, Union
from xlsxwriter.format import Format


HeaderColorData = Union[str, List[List[Optional[str]]]]


class CellFormats(TypedDict):
    header: Union[Format, List[List[Format]]]
    data: Format
    data_date: Format
    data_datetime: Format
