from django.core.management import BaseCommand
from trunk.models import AdminEmailNotiOption, University
from StartupTree.utils.pretty import PrettyPrinter

cornell = University.objects.get(id=1)

printer = PrettyPrinter(make_header=lambda opt: f'{opt}: {opt.get_public_str(cornell)}').printer


class Command(BaseCommand):
    """
    Utility to print all AdminCalOption objects (using custom wording from university whose id is 1 (cornell on dev platform)).
    """
    def handle(self, *args, **options):
        for opt in AdminEmailNotiOption.objects.all():
            printer(opt)
