from django.core.management import BaseCommand
from trunk.models import UniversityExportHistory
from StartupTree.utils.pretty import PrettyPrinter


class Command(BaseCommand):
    """
    This is a one-time migration of user-funding exports (UniversityExportHistory objects) to have their category field
    updated.

    Currently (10/7/2020), general user exports and user-funding exports share the same category. After creating a new
    category specific to user-funding, this command is run to migrate all user-funding exports' category field to the new
    user-funding category.
    """
    def handle(self, *args, **options):
        histories = UniversityExportHistory.objects.all()

        for history in histories:
            if history.category == UniversityExportHistory.USER_FUNDING:

                is_user_funding = Command.is_history_user_funding(history)

                if not is_user_funding:
                    printer_old(history)
                    history.category = UniversityExportHistory.USER_GENERAL
                    history.save()
                    history.refresh_from_db()
                    printer_new(history)
                    print('')

    @staticmethod
    def is_info_user_funding(extra_info: str) -> bool:
        """
        history is user funding if history.extra_info contains the word "funding".
        """
        return "funding" in extra_info.lower()

    @staticmethod
    def is_history_user_funding(history: UniversityExportHistory) -> bool:
        """ Given history object that was technically labelled user funding,
        infer whether this is semantically a user funding export or not.
        """
        if history.category == UniversityExportHistory.USER_FUNDING:
            if history.extra_info:
                return Command.is_info_user_funding(history.extra_info)
            else:
                # By default, if extra_info doesn't exist, then object is User Funding.
                return True
        else:
            raise ValueError("This function only applies to those exports "
                             "that are initially categorized as User Funding")


# Pretty Printers

def make_header_old(history):
    return f"OLD {history}: "


def make_header_new(history):
    return f"NEW {history}: "


printer_old = PrettyPrinter(empty_end_line=False, make_header=make_header_old).printer

printer_new = PrettyPrinter(empty_end_line=False, make_header=make_header_new).printer