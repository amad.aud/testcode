from django.core.management.base import BaseCommand
from trunk.models import University


class Command(BaseCommand):
    """
    update the customized text for the word "tools", which are now "deals"
    """
    def handle(self, *args, **options):
        universities = University.objects.all()
        for uni in universities:
            style = uni.style
            if style is not None and style.tools_word != 'tools':
                # because tools_word is plural and deal_word is singular,
                # need to create a grammatically correct singular of the customized
                # tools_word to use as the new deal_word
                deal_word = 'deal'
                if style.tools_word.endswith('ies'):
                    deal_word = style.tools_word[:-3]
                    deal_word += 'y'
                elif style.tools_word.endswith(('ses', 'xes', 'zes', 'ches', 'shes')):
                    deal_word = style.tools_word[:-2]
                elif style.tools_word.endswith('s'):
                    deal_word = style.tools_word[:-1]
                style.deal_word = deal_word
                style.save()
