from django.core.management.base import BaseCommand
from trunk.models import University, UniversitySubscription


class Command(BaseCommand):
    def handle(self, *args, **options):
        universities = University.objects.filter(site__isnull=False)
        for uni in universities:
            if not UniversitySubscription.objects.filter(university_id=uni.id).exists():
                UniversitySubscription.objects.create(university=uni)
