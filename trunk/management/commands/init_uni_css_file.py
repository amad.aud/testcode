from django.core.management.base import BaseCommand
from trunk.models import University, UniversityStyle
from hamster.tasks import schedule_update_css


class Command(BaseCommand):
    def handle(self, *args, **options):
        universities = University.objects.all()
        for university in universities:
            if university.site is None:
                continue
            c1 = '#0ea284'
            c2 = '#0A8D72'
            if university.style is None:
                style = UniversityStyle.objects\
                                       .create(_university=university)
                style.first_color = c1
                style.second_color = c2
            else:
                if university.style.first_color is not None:
                    c1 = university.style.first_color
                if university.style.second_color is not None:
                    c2 = university.style.second_color
            new_colors = [c1, c2]
            schedule_update_css(university.id, new_colors)
            print(c1, c2)
