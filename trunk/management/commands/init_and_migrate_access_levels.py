from django.core.management.base import BaseCommand
from trunk.models import University, UniversityStaff, AccessLevel

class Command(BaseCommand):
    def handle(self, *args, **options):
        # also see university_signup in emailuser/views/__init__.py
        for uni in University.objects.all():
            # initialize all access levels. use get_or_create to avoid duplicates
            dashboard, _ = AccessLevel.objects.get_or_create(name=AccessLevel.DASHBOARD, university=uni)
            users, _ = AccessLevel.objects.get_or_create(name=AccessLevel.USERS, university=uni)
            ventures, _ = AccessLevel.objects.get_or_create(name=AccessLevel.VENTURES, university=uni)
            projects, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PROJECTS, university=uni)
            groups, _ = AccessLevel.objects.get_or_create(name=AccessLevel.GROUPS, university=uni)
            tools, _ = AccessLevel.objects.get_or_create(name=AccessLevel.TOOLS, university=uni)
            tags, _ = AccessLevel.objects.get_or_create(name=AccessLevel.TAGS, university=uni)
            export, _ = AccessLevel.objects.get_or_create(name=AccessLevel.EXPORT, university=uni)
            custom_data, _ = AccessLevel.objects.get_or_create(name=AccessLevel.CUSTOM_DATA, university=uni)
            reporting, _ = AccessLevel.objects.get_or_create(name=AccessLevel.REPORTING, university=uni)
            events, _ = AccessLevel.objects.get_or_create(name=AccessLevel.EVENTS, university=uni)
            jobs, _ = AccessLevel.objects.get_or_create(name=AccessLevel.JOBS, university=uni)
            competitions, _ = AccessLevel.objects.get_or_create(name=AccessLevel.COMPETITIONS, university=uni)
            applications, _ = AccessLevel.objects.get_or_create(name=AccessLevel.APPLICATIONS, university=uni)
            surveys, _ = AccessLevel.objects.get_or_create(name=AccessLevel.SURVEYS, university=uni)
            mentorship, _ = AccessLevel.objects.get_or_create(name=AccessLevel.MENTORSHIP, university=uni)
            team_mentorship, _ = AccessLevel.objects.get_or_create(name=AccessLevel.TEAM_MENTORSHIP, university=uni)
            invite, _ = AccessLevel.objects.get_or_create(name=AccessLevel.INVITE, university=uni)
            announce, _ = AccessLevel.objects.get_or_create(name=AccessLevel.ANNOUNCE, university=uni)
            support, _ = AccessLevel.objects.get_or_create(name=AccessLevel.SUPPORT, university=uni)
            admins, _ = AccessLevel.objects.get_or_create(name=AccessLevel.ADMINS, university=uni)
            roadmap, _ = AccessLevel.objects.get_or_create(name=AccessLevel.ROADMAP, university=uni)
            program, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PROGRAM, university=uni)
            design, _ = AccessLevel.objects.get_or_create(name=AccessLevel.DESIGN, university=uni)
            platform, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PLATFORM, university=uni)
            pages, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PAGES, university=uni)
            custom_text, _ = AccessLevel.objects.get_or_create(name=AccessLevel.CUSTOM_TEXT, university=uni)
            custom_roles, _ = AccessLevel.objects.get_or_create(name=AccessLevel.CUSTOM_ROLES, university=uni)
            user_signup, _ = AccessLevel.objects.get_or_create(name=AccessLevel.USER_SIGNUP, university=uni)
            private_discussion, _ = AccessLevel.objects.get_or_create(name=AccessLevel.PRIVATE_DISCUSSION, university=uni)

            # for staff in UniversityStaff.objects.filter(university=uni):
            #     # first clear out any existing access levels to avoid duplicates
            #     for access_level in staff.access_levels.all():
            #         staff.access_levels.remove(access_level)
            #     # now add the appropriate access levels
            #     if staff.access_level == UniversityStaff.SUPER:
            #         staff.is_super = True
            #         staff.save()
            #     elif staff.access_level == UniversityStaff.DISCUSSION_ONLY:
            #         staff.access_levels.add(private_discussion)
            #     elif staff.access_level == UniversityStaff.EVENT_ONLY:
            #         staff.access_levels.add(events)
            #     elif staff.access_level == UniversityStaff.COMPETITION_ONLY:
            #         staff.access_levels.add(competitions)
            #         staff.access_levels.add(applications)
            #     elif staff.access_level == UniversityStaff.EVENT_AND_COMPETITION:
            #         staff.access_levels.add(events)
            #         staff.access_levels.add(competitions)
            #         staff.access_levels.add(applications)
