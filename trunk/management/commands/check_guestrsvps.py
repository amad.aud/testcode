from django.core.management.base import BaseCommand
from branch.models import *
from trunk.models import Event, EventRSVP, GuestRSVP, AcceptedRSVP


class Command(BaseCommand):
    """ Checks the following guest invariants in event rsvp objects.

    For every event:
    1. Every guest in event.guest_rsvps.all() has exactly one corresponding EventRSVP object.
    2. Every guest in event.guest_waitlists.all() has exactly one corresponding EventRSVP oobject.

    For every EventRSVP object (eventrsvp) that contains a GuestRSVP object (guest):
    1. If eventrsvp is a waitlist, guest in eventrsvp.rsvp_event.guest_waitlists.all()
    2. If eventrsvp is a rsvp, guest in eventrsvp.rsvp_event.guest_rsvps.all()

    Also returns the number of "orphans", which is a GuestRSVP object that is not
    linked to a Event object nor an EventRSVP object. 
    """

    def handle(self, *args, **options):
        guests = set()

        errors = 0

        for event in Event.objects.all():
            for guest in event.guest_rsvps.all():
                guests.add(guest)
                gs = EventRSVP.objects.filter(event=event, guest=guest, waitlisted=False)
                print("count = " + str(gs.count()))
                if gs.count() != 1:
                    errors += 1
            for guest in event.guest_waitlists.all():
                guests.add(guest)
                gs = EventRSVP.objects.filter(event=event, guest=guest, waitlisted=True)
                print("count = " + str(gs.count()))
                if gs.count() != 1:
                    errors += 1
        for eventrsvp in EventRSVP.objects.all():
            if eventrsvp.guest is not None:
                guest = eventrsvp.guest
                guests.add(guest)
                if eventrsvp.waitlisted:
                    if guest in eventrsvp.rsvp_event.guest_waitlists.all():
                        print("pass")
                    else:
                        print("fail")
                        errors += 1
                else:
                    if guest in eventrsvp.rsvp_event.guest_rsvps.all():
                        print("pass")
                    else:
                        print("fail")
                        errors += 1

        print("proper guests = " + str(guests))
        print("errors = " + str(errors))

        orphans = set()

        for guest in GuestRSVP.objects.all():
            if guest not in guests:
                orphans.add(guest)

        print("orphans = " + str(orphans))
