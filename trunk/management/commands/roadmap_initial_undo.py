from django.core.management.base import BaseCommand
from django.db import transaction
from StartupTree.loggers import app_logger
from trunk.models import (
    RoadMapBlock,
    RoadMap,
    University
)

class Command(BaseCommand):

    def add_arguments(self, parser):
        ''' If your command does not take any extra arguments,
        then this entire method can be deleted. Otherwise,
        add as many arguments as you need, like the example below.
        '''
        parser.add_argument('current_site')

    @transaction.atomic
    def handle(self, *args, **options):
        ''' The code that actually runs when you call in the web bash:
                python3 manage.py <command>
            Where <command> is the name of this file, but without the .py extension
            followed by any arguments (separated by spaces).
        '''

        site_name = options['current_site']
        current_uni = University.objects.get(short_name=site_name)
        universities = University.objects.all()
        RoadMapBlock.objects.filter(university=current_uni).all().delete()
        #RoadMapBlock.objects.all().delete()
        RoadMap.objects.all().delete()
        #RoadMapBlock.objects.all()
        # for u in universities:
        #     u.road_map = RoadMapBlock.objects.none()
        # road_map_blocks = RoadMapBlock.objects.none()
