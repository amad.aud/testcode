from django.core.management.base import BaseCommand
from trunk.models import EventRSVP


class Command(BaseCommand):
    def handle(self, *args, **options):
        '''
        update the affiliation data of EventRSVP guests who selected the
        Faculty/Staff or Non-Affiliated affiliations. The numbers associated
        with each affiliation choice have changed, so this should only be
        run once.
        '''
        for guest in EventRSVP.objects.filter(guest__isnull=False):
            if guest.affiliation == 3:  # Faculty/Staff
                guest.affiliation = 6
                guest.save()
            elif guest.affiliation == 4:    # Non-Affiliated
                guest.affiliation = 5
                guest.save()
