from django.core.management.base import BaseCommand
from trunk.models import Event
from django.conf import settings
from trunk.utils import create_event_thumbnails
from PIL import Image
import tempfile
import requests
import io


class Command(BaseCommand):
    def handle(self, *args, **options):
        events = Event.objects.filter(image__isnull=False).exclude(image='')
        for event in events.iterator():
            print (event.image)
            media_url = settings.MEDIA_URL
            if settings.IS_DEV:
                media_url = "http://startuptreetest.co:8000" + media_url
            response = requests.get("{0}{1}".format(media_url, event.image), stream=True)
            if response.status_code == 200:
                buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
                for chunk in response.iter_content():
                    buffer.write(chunk)
                buffer.seek(0)
                create_event_thumbnails(io.BytesIO(buffer.read()), event)
                buffer.close()