from django.core.management.base import BaseCommand
from django.db import transaction
from trunk.models import Event


class Command(BaseCommand):
	@transaction.atomic
	def handle(self, *args, **options):
		for event in Event.objects.all():
			event.map_location = event.location
			event.save()
