from django.core.management.base import BaseCommand
from StartupTree.loggers import app_logger
from trunk.models import UniversityStaff

class Command(BaseCommand):
    def handle(self, *args, **options):
        for staff in UniversityStaff.objects.all():
            if staff.access_level == 99:
                staff.access_level = UniversityStaff.SUPER
            elif staff.access_level == 1:
                staff.access_level = UniversityStaff.COMPETITION_ONLY
            elif staff.access_level == 2:
                staff.access_level = UniversityStaff.DISCUSSION_ONLY
            staff.save()
