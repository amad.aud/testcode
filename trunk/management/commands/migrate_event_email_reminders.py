from django.core.management.base import BaseCommand
from trunk.models import Event

class Command(BaseCommand):
    help = """Migrate Auto Email Reminder (Event.send_emails) boolean to
    Auto Email Reminder Days Prior integer (Event.send_emails_days_prior).

    True => 7 (days)
    False => 0 (days)

    Takes a single boolean argument on whether to print stats or not. (true/True/t/T/false/False/f/F)
    """


    def add_arguments(self, parser):
        parser.add_argument('verbose')


    @staticmethod
    def print_stats():
        all_events = Event.objects.all()
        print("Stats: ")
        print("\tTotal events: " + str(all_events.count()))
        print("\tTotal events w/ send_emails=True : " + str(all_events.filter(send_emails=True).count()))
        print("\tTotal events w/ send_emails=False : " + str(all_events.filter(send_emails=False).count()))
        print("\tTotal events w/ send_emails_days_prior=7 : " + str(all_events.filter(send_emails_days_prior=7).count()))
        print("\tTotal events w/ send_emails_days_prior=0 : " + str(all_events.filter(send_emails_days_prior=0).count()))


    @staticmethod
    def print_verify():
        print("Performing the following checks: ")
        print("\tEvery event with send_emails=True was given send_emails_days_prior=7")
        print("\tAnd Every event with send_emails=False was given send_emails_days_prior=0")
        print("")

        all_events = Event.objects.all()

        error_count = 0
        for event in all_events:
            if event.send_emails and event.send_emails_days_prior != 7:
                error_count += 1
            elif not event.send_emails and event.send_emails_days_prior != 0:
                error_count += 1

        print("Final Status: ")

        if error_count == 0:
            print("\tSUCCESS: Found 0 errors.")
        else:
            print("\tERROR: Found " + str(error_count) + " errors.")


    def handle(self, *args, **options):
        verbose = (options['verbose'] == "true" or
                   options['verbose'] == "True" or
                   options['verbose'] == "t" or
                   options['verbose'] == "T")

        all_events = Event.objects.all()

        print("Performing Migration... ")
        print("")

        for event in all_events:
            if event.send_emails:
                event.send_emails_days_prior = 7
            else:
                event.send_emails_days_prior = 0
            event.save()

        Command.print_verify()

        print("")

        if verbose:
            Command.print_stats()
