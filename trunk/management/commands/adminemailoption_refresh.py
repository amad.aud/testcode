from django.core.management import BaseCommand
from trunk.models import AdminEmailNotiOption


class Command(BaseCommand):
    def handle(self, *args, **options):

        choices_dict = dict(AdminEmailNotiOption.OPTION_TYPES)

        created_lst = []
        for code in choices_dict:
            obj, created = AdminEmailNotiOption.objects.get_or_create(option_type=code)
            name = choices_dict[code]
            if created:
                created_lst.append((code, name))

        print(f'Created the following options: {created_lst}')
