from django.core.management.base import BaseCommand
from StartupTree.loggers import app_logger
from trunk.models import UniversityStaff

class Command(BaseCommand):
    ''' This is a template to write custom management commands.
    Replace all comments here with your own specifications.
    (None of the specs here should exist in your final code.)

    Learn more: https://docs.djangoproject.com/en/2.0/howto/custom-management-commands/
    '''

    help = "A short description of the command, which will be printed in the \
    help message when the user runs the command python manage.py help <command>."

    def add_arguments(self, parser):
        ''' If your command does not take any extra arguments,
        then this entire method can be deleted. Otherwise,
        add as many arguments as you need, like the example below.
        '''
        parser.add_argument('email')
        parser.add_argument('new_access')

    def handle(self, *args, **options):

        email = options['email']
        new_access = options['new_access']

        app_logger.debug("EMAIL = " + email)
        app_logger.debug("NEWACC = " + new_access)


        try:
            uni_staff_rel = UniversityStaff.objects.get(user__email=email)
            app_logger.debug("Old access was: " + str(uni_staff_rel.access_level))
            uni_staff_rel.access_level = new_access
            uni_staff_rel.save()

            uni_staff_rel_new = UniversityStaff.objects.get(user__email=email)
            app_logger.debug("NEW access is: " + str(uni_staff_rel_new.access_level))

            if new_access == uni_staff_rel_new.access_level:
                app_logger.debug("SUCCESS: '" + email + "' now has access level " + str(new_access))

        except UniversityStaff.ObjectDoesNotExist:
            app_logger.debug("FAILURE: Could not find UniversityStaff: '" + email + "'")
