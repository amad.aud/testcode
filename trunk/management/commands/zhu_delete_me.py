from django.core.management.base import BaseCommand
from StartupTree.loggers import app_logger
from trunk.models import *

class Command(BaseCommand):
    ''' This is a template to write custom management commands.
    Replace all comments here with your own specifications.
    (None of the specs here should exist in your final code.)

    Learn more: https://docs.djangoproject.com/en/2.0/howto/custom-management-commands/
    '''

    help = "A short description of the command, which will be printed in the \
    help message when the user runs the command python manage.py help <command>."

    def handle(self, *args, **options):
        # x = University.objects.all()
        # app_logger.debug(x)
        # y = UserEmail.objects.all()
        # app_logger.debug(y)

        # z = UserEmail.objects.filter(staffs__isnull=False)
        # app_logger.debug(z)
        # app_logger.debug("z.count = " + str(z.count()))
        #
        # unis = University.objects.all()
        #
        # tim_count = 0
        # for uni in unis:
        #     admins = uni.staffs.all()
        #     app_logger.debug(uni.name)
        #
        #     for admin in admins:
        #         app_logger.debug("  " + admin.email)
        #         tim_count += 1
        #
        # app_logger.debug("tim_count = " + str(tim_count))

        x=UniversityStaff.objects.filter(access_level=99)
        app_logger.debug(x.count())
