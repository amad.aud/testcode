from django.core.management.base import BaseCommand
from trunk.models import UniversityStyle


class Command(BaseCommand):
    def handle(self, *args, **options):
        for style in UniversityStyle.objects.all():
            style.display_team_mentors = 0
            style.save()