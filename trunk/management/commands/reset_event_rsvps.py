from django.core.management.base import BaseCommand
from branch.models import *
from trunk.models import Event, EventRSVP, AcceptedRSVP


# event_id = '31d1a730-70b1-4ea3-a797-52f0fdedb03c' # Timmy Event
event_id = '902ad639-b891-4edb-bef6-f317e1d5d376' # Timmy New Event 2


class Command(BaseCommand):
    """ Given an event_id (as global variable),
    resets all of the rsvp/waitlists/interested users/guests to 0.
    """
    def handle(self, *args, **options):
        event = Event.objects.get(event_id=event_id)

        def remove_all(manager):
            for elt in manager.all():
                manager.remove(elt)

        remove_all(event.attending_users)
        remove_all(event.interested_users)
        remove_all(event.guest_rsvps)
        EventRSVP.objects.filter(rsvp_event=event).delete()
        AcceptedRSVP.objects.filter(event=event).delete()

        pass
