from django.core.management.base import BaseCommand
from StartupTree.loggers import app_logger
from trunk.models import Event
from trunk.utils import create_event_thumbnails


class Command(BaseCommand):
	'''
	Re-uploads the images for all existing Event objects that have associated image files,
	which addresses cases where the image file still exists but does not always display
	correctly. If an error is encountered when re-uploading the image, which may be
	the case for some very old events, the image file is assumed to be corrupted and
	the event image is removed, leading to the default event image being displayed instead.
	'''
	def handle(self, *args, **options):
		for event in Event.objects.filter(image__isnull=False):
			try:
				image = event.image
				create_event_thumbnails(image, event)
			except FileNotFoundError:
				app_logger.info('Image file corrupted for event {0} ({1}). Replacing with default image'.format(event.name, event.university))
				event.image = None
			event.save()
