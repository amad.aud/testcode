from django.core.management.base import BaseCommand
from trunk.models import UniversityEmailDomain


class Command(BaseCommand):
    def handle(self, *args, **options):
        for domain in UniversityEmailDomain.objects.all():
            domain.type = UniversityEmailDomain.MEMBER
            domain.save()
