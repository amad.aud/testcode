from django.core.management.base import BaseCommand
from trunk.models import University

class Command(BaseCommand):
    def handle(self, *args, **options):
        for uni in University.objects.filter(style__isnull=False):
            style = uni.style
            style.enable_global_discussion = style.enable_global
            style.save()