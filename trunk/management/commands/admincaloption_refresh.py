from django.core.management import BaseCommand
from trunk.models import AdminCalSyncOption


class Command(BaseCommand):
    def handle(self, *args, **options):

        choices_dict = dict(AdminCalSyncOption.SYNC_TYPES)

        created_lst = []
        for code in choices_dict:
            obj, created = AdminCalSyncOption.objects.get_or_create(sync_type=code)
            name = choices_dict[code]
            if created:
                created_lst.append((code, name))

        print(f'Created the following options: {created_lst}')
