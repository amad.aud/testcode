from django.core.management.base import BaseCommand
from trunk.models import UniversitySubscription

class Command(BaseCommand):
    def handle(self, *args, **options):
        for sub in UniversitySubscription.objects.all():
            sub.is_team_mentorship = False
            sub.save()
