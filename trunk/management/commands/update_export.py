from django.core.management.base import BaseCommand
from trunk.tasks import update_export_data

class Command(BaseCommand):
    def handle(self, *args, **options):
        update_export_data()