from django.core.management.base import BaseCommand
from trunk.models import Event, UniversityStyle


class Command(BaseCommand):
    def handle(self, *args, **options):
        for event in Event.objects.all():
            style = UniversityStyle.objects.get(_university=event.university)
            event.timezone = style.timezone
            event.save()
