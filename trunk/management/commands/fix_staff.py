from django.core.management.base import BaseCommand
from trunk.models import University, StartupTreeStaff
from emailuser.modelforms import UserEmailForm
import uuid


class Command(BaseCommand):
    """
    Creates staff account. By default the password will be set with
    random strings. At the end, new staff will get reset account email to set the password.
    By default, new staff will have admin access to all universities on the platform.
    """
    def add_arguments(self, parser):
        parser.add_argument('email', type=str)

    def handle(self, *args, **options):
        email = options.get('email')
        pwd = uuid.uuid4()
        email_form = UserEmailForm({'email': email,
                                    'password1': pwd,
                                    'password2': pwd})
        if email_form.is_valid():
            user_email = email_form.save()
            universities = University.objects.all()
            new_staff = StartupTreeStaff.objects.create(staff=user_email)
            new_staff.universities.add(*universities)
            print("New staff {0} was created.".format(user_email))

        else:
            raise ValueError("New staff {0} cannot be created: {1}".format(email, email_form.error_messages))
