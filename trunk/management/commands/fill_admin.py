from django.core.management.base import BaseCommand
from trunk.models import University
from emailuser.modelforms import UserEmailForm
import uuid


class Command(BaseCommand):
    """
    fill out admin account for new universiteis that does not have it.
    """
    def handle(self, *args, **options):
        universities = University.objects.all()
        for uni in universities:
            if uni.admin is None:
                tmp_admin_email = '{0}@startuptree.co'.format(uni.short_name)
                pwd = uuid.uuid4()
                email_form = UserEmailForm({'email': tmp_admin_email,
                                            'password1': pwd,
                                            'password2': pwd})
                if email_form.is_valid():
                    tmp_admin = email_form.save()
                    uni.admin = tmp_admin
                    uni.save()
                    print(uni)
