from django.core.management.base import BaseCommand
from trunk.models import UniversityStyle, UniversityEmailDomain


class Command(BaseCommand):
    def handle(self, *args, **options):
        for style in UniversityStyle.objects.all():
            university = style._university
            if style.primary_email_url:
                UniversityEmailDomain.objects.add_domain(university, style.primary_email_url)
                print("Added 1 {0}".format(style.primary_email_url))
            if style.secondary_email_url:
                UniversityEmailDomain.objects.add_domain(university, style.primary_email_url)
                print("Added 2 {0}".format(style.secondary_email_url))
            if style.third_email_url:
                UniversityEmailDomain.objects.add_domain(university, style.primary_email_url)
                print("Added 3 {0}".format(style.third_email_url))
