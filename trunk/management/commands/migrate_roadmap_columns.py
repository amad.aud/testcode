from django.core.management.base import BaseCommand
from trunk.models import RoadMapBlock, RoadMapColumn


class Command(BaseCommand):
    def handle(self, *args, **options):
        for block in RoadMapBlock.objects.all():
            rm_column = RoadMapColumn.objects.get(number=block.column, road_map=block.road_map)
            block.rm_columns.add(rm_column)
