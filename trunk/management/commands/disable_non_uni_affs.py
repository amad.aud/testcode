from django.core.management.base import BaseCommand
from trunk.models import University


class Command(BaseCommand):
    def handle(self, *args, **options):
        '''
        for non-university platforms, have the affiliations step
        of user signup disabled by default
        '''
        for platform in University.objects.filter(is_none_university=True):
            style = platform.style
            style.signup_step2 = False
            style.require_step2 = False
            style.save()
