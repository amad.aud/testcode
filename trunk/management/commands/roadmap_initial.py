import datetime
from django.core.management.base import BaseCommand
from django.db import transaction
from StartupTree.loggers import app_logger
from django.utils import timezone
from trunk.models import (
    RoadMap,
    RoadMapBlock,
    University
)

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('current_site')

    @transaction.atomic
    def handle(self, *args, **options):
        #initializing RoadMap
        site_name = options['current_site']
        universities = University.objects.all()
        university = University.objects.get(short_name=site_name)
        roadmap = RoadMap.objects.create(
            university=university,
            column_names= ["Header1", "Header2", "Header3", "Header4"]
        )
        roadmap.save()
        # for u in universities:
            # u.road_map = RoadMapBlock.objects.none()
        header_name = ""
        for x in range(10):
            if (x < 4):
                col = 0
                header_name = "Header 1"
            elif (x < 7):
                col = 1
                header_name = "Header 2"
            elif (x < 9):
                col = 2
                header_name = "Header 3"
            else:
                col = 3
                header_name = "Header 4"

            r = RoadMapBlock.objects.create(
                title = "My Item",
                text = "",
                char_limit = 100,
                image = None,
                column = col,
                time_created = datetime.datetime.now(),
                header = header_name,
                university = university,
                road_map = roadmap
            )
            r.access_admins = university.staffs.all()
            r.save()
