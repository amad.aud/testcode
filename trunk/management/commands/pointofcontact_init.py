from django.core.management.base import BaseCommand
from trunk.models import University, UniversityStaff, PointOfContact


class Command(BaseCommand):
    def handle(self, *args, **options):
        # simple migration for new Pointofcontact.
        # just one for global usage is enough.
        PointOfContact.objects.all().delete()
        PointOfContact.objects.get_or_create(name=PointOfContact.MENTORSHIP)
        PointOfContact.objects.get_or_create(name=PointOfContact.TEAM_MENTORSHIP)

