from django.core.management.base import BaseCommand
from branch.models import *
from trunk.models import Event, EventRSVP, GuestRSVP, AcceptedRSVP


# event_id = '31d1a730-70b1-4ea3-a797-52f0fdedb03c' # Timmy Event
event_id = '902ad639-b891-4edb-bef6-f317e1d5d376' # Timmy New Event 2


class Command(BaseCommand):
    """ Pretty prints the desired attributes of the event corresponding to
    global event_id variable. (Change this variable to the event you want to see.)

    Also pretty prints QuerySets of objects in a manner that shows the desired
    attributes of those objects.
    """

    @staticmethod
    def to_string(obj):
        queryset_type = type(Event.objects.all())

        def print_obj_attrs(obj, attrs):
            def print_attr(attr):
                """ Returns: "<attr>: <obj.attr>"
                """
                cmd = "'" + attr + " = ' + to_string_h(obj." + attr + ")"
                locals = { 'obj': obj, 'to_string_h': to_string_h }
                return eval(cmd, {}, locals)
            lst = map(print_attr, attrs)
            middle = ", ".join(lst)
            return "<" + obj.__class__.__name__ + ": " + middle + ">"

        def to_string_h(obj):
            if isinstance(obj, AcceptedRSVP):
                return print_obj_attrs(obj, ['email'])
            if isinstance(obj, GuestRSVP):
                return print_obj_attrs(obj, ['email'])
            if isinstance(obj, Event):
                return print_obj_attrs(obj, ['name', 'show_custom_rsvp'])
            if isinstance(obj, EventRSVP):
                return print_obj_attrs(obj, ['member', 'guest', 'waitlisted', 'created'])
            if isinstance(obj, queryset_type):
                middle = ", \n\t".join(map(to_string_h, obj))
                if obj.count() > 0:
                    middle = "\n\t" + middle + "\n"
                return "<QuerySet [" + middle + "]>"
            return str(obj)

        return to_string_h(obj)


    def handle(self, *args, **options):
        event = Event.objects.get(event_id=event_id)

        attending_users = event.attending_users.all()
        interested_users = event.interested_users.all()

        eventrsvps = EventRSVP.objects.filter(rsvp_event=event)

        eventrsvps_waitlisted = EventRSVP.objects.filter(rsvp_event=event, waitlisted=True).order_by('created')

        acceptedrsvps = AcceptedRSVP.objects.filter(event=event)

        print_var = lambda var: "'" + var + " = ' + Command.to_string(" + var + ") + '\\n'"

        vars = [
            'event',
            'attending_users',
            'interested_users',
            'eventrsvps',
            'eventrsvps_waitlisted',
            'acceptedrsvps',
            'event.guest_rsvps.all()',
            'event.get_total_waitlisted_count()',
            'event.get_waitlisted_users()',
            'event.get_waitlisted_guests()',
        ]

        for var in vars:
            print(eval(print_var(var)))

        return
