from django.core.management.base import BaseCommand
from trunk.models import Event, JudgeBoard


class Command(BaseCommand):
    def handle(self, *args, **options):
        for board in JudgeBoard.objects.all():
            event = board.competition
            board.round = 1
            board.save()
            event.judge_boards.add(board)
            for judge in board.judges.all():
                judge.round = 1
                judge.save()
