from django.core.management.base import BaseCommand
from trunk.models import School, Major
from branch.models import EmployeeTitle


class Command(BaseCommand):
    def handle(self, *args, **options):
        schools = School.objects.all()
        majors = Major.objects.all()
        titles = EmployeeTitle.objects.all()

        for school in schools:
            if school.displayed is None:
                school.displayed = school.get_name()
                school.save()
        for major in majors:
            if major.displayed is None:
                major.displayed = major.get_name()
                major.save()
        for title in titles:
            if title.displayed is None:
                if title.name == 'ceo':
                    title.displayed = 'CEO'
                elif title.name == 'cfo':
                    title.displayed = 'CFO'
                elif title.name == 'cto':
                    title.displayed = 'CTO'
                else:
                    title.displayed = title.name.title()
                title.save()
