from django import template
register = template.Library()


@register.filter(is_safe=True)
def get_filename(text):
	"""
	Extracts the name of an uploaded file
	:param text:
	:return:
	"""
	return text[text.rindex('/')+1:]
