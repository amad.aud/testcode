from django.core.management.base import BaseCommand
from trunk.models import UniversityStyle


class Command(BaseCommand):
    """
    update the customized text for the word "group", which is now "page"
    """
    def handle(self, *args, **options):
        for style in UniversityStyle.objects.all():
            if style.group_word != 'group':
                page_word = style.group_word
            else:
                page_word = 'page'
            style.page_word = page_word
            style.save()
