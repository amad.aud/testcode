from django.core.management.base import BaseCommand
from trunk.models import Site


class Command(BaseCommand):
    def handle(self, *args, **options):
        lst = [
            'csumb.startuptree.co',
            'csuchico.startuptree.co',
            'fullerton.startuptree.co',
            'okstate.startuptree.co',
            'wustl.startuptree.co',
            'usc.startuptree.co',
            'utdallas.startuptree.co',
            'lmu.startuptree.co',
            'cleary.startuptree.co',
            'toledo.startuptree.co',
            'notredame.startuptree.co',
            'stonybrook.startuptree.co',
            'binghamton.startuptree.co',
            'uw.startuptree.co',
            'calpoly.startuptree.co',
            'seattleu.startuptree.co',
            'nmsu.startuptree.co',
            'uwstout.startuptree.co',
            'dixie.startuptree.co',
            'ulster.startuptree.co',
            'stthomas.startuptree.co',
            'ilstu.startuptree.co',
            'chicagobooth.startuptree.co']
        # lst = ['weird.startuptreetest.co:8000']
        sites = Site.objects.get(domain__in=lst)
        sites.delete()
