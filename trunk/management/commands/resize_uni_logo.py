from django.core.management.base import BaseCommand
from trunk.models import University
from StartupTree.utils import make_thumb
from django.core.files.uploadedfile import InMemoryUploadedFile
import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        universities = University.objects.all()
        for university in universities:
            style = university.style
            if style is not None:
                ori_logo = style.ori_logo
                if ori_logo:
                    thumbnail, file_ios = make_thumb(ori_logo)
                    new_name = "thumb_" + os.path.split(ori_logo.name)[-1]
                    thumbnail_file = InMemoryUploadedFile(
                        thumbnail, None, new_name, 'image/png',
                        thumbnail.tell(), None)
                    style.logo.save(new_name, thumbnail_file)
                    style.save()
                    for file in file_ios:
                        file.close()
