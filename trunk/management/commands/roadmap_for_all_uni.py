from django.core.management.base import BaseCommand
from django.db import transaction
from StartupTree.loggers import app_logger
from django.utils import timezone
from trunk.models import (
    RoadMap,
    RoadMapBlock,
    University
)

class Command(BaseCommand):
    """
    giving all existing universities in database a default roadmap
    """
    def handle(self, *args, **options):
        universities = University.objects.all()
        for uni in universities:
            r = RoadMap.objects.create(
                university=uni
            )
            r.save()
