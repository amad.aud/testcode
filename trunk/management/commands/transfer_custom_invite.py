from django.core.management import BaseCommand
from trunk.models import University, UniversityStyle


class Command(BaseCommand):
    """
    One-time command to transfer the contents of old deprecated field University.custom_invite_email
    to the new field UniversityStyle.email_invite_user_body
    """
    def handle(self, *args, **options):
        unis = University.objects.filter(custom_invite_email__isnull=False)

        print(f'There are {unis.count()} universities whose uni.custom_invite_email field in not None.')
        print('')

        print(f'Performing Transfer on {unis.count()} University objects...')
        skipped_unis = []
        processed_unis = 0
        for uni in unis:
            style: UniversityStyle = uni.style
            if style:
                value = uni.custom_invite_email
                style.email_invite_user_body = value
                style.save()
                processed_unis += 1
            else:
                skipped_unis.append(uni)

        print(f'Universities processed = {processed_unis}')
        print(f'Universities skipped = {len(skipped_unis)}')
        print('')

        if len(skipped_unis) > 0:
            print(f'The following universities where skipped because uni.style is None: ')
            print(f'\t{skipped_unis}')
            print('')

        print(f'Performing Data Check on {unis.count()} University objects...')
        successes = 0
        skipped_unis = []
        failed_unis = []
        for uni in unis:
            style = uni.style
            if style:
                if style.email_invite_user_body == uni.custom_invite_email:
                    successes += 1
                else:
                    failed_unis.append(uni)
            else:
                skipped_unis.append(uni)

        print(f'Total successes: {successes}')
        print(f'Total skipped: {len(skipped_unis)}')
        print(f'Total failures: {len(failed_unis)}')
        print('')

        if len(failed_unis) > 0:
            print(f'Transfer failed for the following university objects: ')
            print(f'\t{failed_unis}')
            print('')

        if len(skipped_unis) > 0:
            print(f'The following universities where skipped because uni.style is None: ')
            print(f'\t{skipped_unis}')
            print('')
