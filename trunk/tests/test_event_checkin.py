from auto_test_utility.utills import AbastractUniTestBase
from branch.models import Event
from trunk.models import EventCheckin, EventRSVP
from forms.models import Form, Question, Answer
from StartupTree.utils.pretty import printer

import datetime as dt
from django.utils import timezone as django_timezone
from django.urls import reverse
import pytz
from typing import TypedDict, Set

VERBOSE = True

EVENT_NAME = "Test Event Checkin"
EVENT_DESCRIPTION = "Test description"

EVENT_CUSTOM_NAME = f'{EVENT_NAME} (custom_checkin)'
EVENT_CUSTOM_DESCRIPTION = f'{EVENT_DESCRIPTION} (custom_checkin)'


GUEST_FNAME = 'Timothy'
GUEST_LNAME = 'Zhu'
GUEST_EMAIL = 'guest@hello.com'
GUEST_MAJOR = 'Computer Science'

GUEST_AFFILIATION = str(EventRSVP.ALUMNI)
GUEST_SCHOOL = 'Cornell School'


CUSTOM_QUESTION_TEXT = 'Test Text Question'
CUSTOM_QUESTION_ANSWER = 'Test Text Answer'


class EventTimeFields(TypedDict):
    start: dt.datetime
    end: dt.datetime
    timezone: str


class TestEventCheckinHelpers(AbastractUniTestBase):
    event: Event
    event_custom_checkin: Event

    @staticmethod
    def delete_existing_events():
        """
        If events with same names exist, then delete them.
        """
        existing_events = Event.objects.filter(name__in=[EVENT_NAME, EVENT_CUSTOM_NAME])
        if existing_events.exists():
            print('delete event') if VERBOSE else None
            existing_events.delete()

    def make_event_time_fields(self) -> EventTimeFields:
        """
        Returns a dictionary that can be used as kwargs for Event.objects.create(), specifying the event
        start, end, and timezone.
        """
        tz_str = self.cornell.style.timezone
        tz_info = pytz.timezone(tz_str)
        start_date = django_timezone.now().date() + dt.timedelta(days=1)
        start_time = dt.time(hour=12, minute=0, second=0)
        duration_hours = 2
        event_start = dt.datetime.combine(start_date, start_time).astimezone(tz_info)
        event_end = event_start + dt.timedelta(hours=duration_hours)

        return { 'start': event_start,
                 'end': event_end,
                 'timezone': tz_str, }

    def make_event(self, event_time_fields: EventTimeFields) -> Event:
        """
        Returns an event with no custom questions.
        """
        return Event.objects.create_event(
            name=EVENT_NAME,
            description=EVENT_DESCRIPTION,
            university=self.cornell,
            **event_time_fields,
        )

    def make_event_custom_checkin(self, event_time_fields: EventTimeFields) -> Event:
        """
        Returns an event with one custom question.
        """
        checkin_form, _ = Form.objects.get_or_create(form=Form.EVENT, university=self.cornell)

        question = Question.objects.create_question(q_type=Question.PP, text=CUSTOM_QUESTION_TEXT, is_custom=True)

        checkin_form.questions.add(question)

        event_custom_checkin = Event.objects.create_event(
            name=EVENT_CUSTOM_NAME,
            description=EVENT_CUSTOM_DESCRIPTION,
            university=self.cornell,
            show_custom_checkin=True,
            **event_time_fields,
        )

        event_custom_checkin.form = checkin_form
        event_custom_checkin.form.save()

        return event_custom_checkin

    def assertCheckedIn(self, member_type: str, event: Event, count: int, assert_no_other_checkins: bool = True, checkin_ids: Set[int] = None):
        """
        member_type: "guest" or "member"

        Asserts that the member or guest has checked in to "event" for "count" times.

        Also asserts that event has no other checkins (unless assert_no_other_checkins is False)

        if checkin_ids are provided, asserts that the checkedin objects have ids equivalent to the provided ids.
        """
        kwargs_event = { 'checkedin_event': event }

        if member_type == "guest":
            kwargs = { 'guest__email': GUEST_EMAIL }
        elif member_type == "member":
            kwargs = { 'member': self.regular }
        else:
            raise ValueError(f'invalid member_type: {member_type}')
        kwargs.update(kwargs_event)

        all_checkins = EventCheckin.objects.filter(**kwargs)
        m2m_checkins = event.checked_in.filter(**kwargs)

        self.assertEqual(all_checkins.count(), count)
        self.assertEqual(m2m_checkins.count(), count)

        if checkin_ids:
            all_checkins_ids = set(all_checkins.values_list('id', flat=True))
            m2m_checkins_ids = set(m2m_checkins.values_list('id', flat=True))

            self.assertSetEqual(all_checkins_ids, checkin_ids)
            self.assertSetEqual(m2m_checkins_ids, checkin_ids)

        if assert_no_other_checkins:
            self.assertEqual(EventCheckin.objects.filter(**kwargs_event).count(), count)
            self.assertEqual(event.checked_in.filter(**kwargs_event).count(), count)

    def assertCustomAnswers(self, member_type: str):
        """
        Asserts that custom responses have been supplied from member_type person to self.event_custom_checkin
        """
        if member_type == "guest":
            kwargs = { 'rsvp_user__guest__email': GUEST_EMAIL }
        elif member_type == "member":
            kwargs = { 'rsvp_user__member': self.regular }
        else:
            raise ValueError(f'invalid member_type: {member_type}')

        answers = Answer.objects.filter(rsvp_user__rsvp_event=self.event_custom_checkin, **kwargs)
        self.assertEqual(answers.count(), 1)

        if answers.count() == 1:
            answer: Answer = answers.first()
            self.assertEqual(answer.text, CUSTOM_QUESTION_ANSWER)

    @staticmethod
    def get_guest_form_data() -> dict:
        """
        returns post data simulating guest info supplied
        """
        return {
            'fname': GUEST_FNAME,
            'lname': GUEST_LNAME,
            'email': GUEST_EMAIL,
            'major': GUEST_MAJOR,
            'affiliation': GUEST_AFFILIATION,
            'school': GUEST_SCHOOL,
        }

    def get_custom_form_data(self):
        """
        returns post data simulating a custom question answered.
        """
        question = self.event_custom_checkin.form.questions.first()

        return {
            'count': 1,
            'type0': question.get_question_type_str(),
            'id0': question.id,
            'pp0': CUSTOM_QUESTION_ANSWER,
        }


class TestEventCheckin(TestEventCheckinHelpers):
    def setUp(self):
        super().setUp()

        self.__class__.delete_existing_events()

        event_time_fields = self.make_event_time_fields()

        self.event = self.make_event(event_time_fields)
        self.event_custom_checkin = self.make_event_custom_checkin(event_time_fields)

        self.checkin_path = reverse('event_checkin')

    def reset(self):
        event_ids = [self.event.event_id, self.event_custom_checkin.event_id]
        EventCheckin.objects.filter(checkedin_event__event_id__in=event_ids).delete()
        EventRSVP.objects.filter(rsvp_event__event_id__in=event_ids).delete()

    def test_user_checkin(self):
        self.reset()

        self.login_user(self.client, self.regular.user.email, 1234)
        self.client.post(self.checkin_path, {'event_id': self.event.event_id})

        self.assertCheckedIn('member', self.event, count=1)

    def test_guest_checkin(self):
        self.reset()

        guest_form = self.__class__.get_guest_form_data()
        self.client.post(self.checkin_path, {'event_id': self.event.event_id, **guest_form})

        self.assertCheckedIn('guest', self.event, count=1)

    def test_multi_user_checkin(self):
        self.reset()

        self.login_user(self.client, self.regular.user.email, 1234)
        self.client.post(self.checkin_path, {'event_id': self.event.event_id})
        self.client.post(self.checkin_path, {'event_id': self.event.event_id})

        self.assertCheckedIn('member', self.event, count=2)

    def test_user_checkin_custom(self):
        self.reset()

        event = self.event_custom_checkin
        custom_form_data = self.get_custom_form_data()
        self.login_user(self.client, self.regular.user.email, 1234)
        self.client.post(self.checkin_path, {'event_id': event.event_id, **custom_form_data})

        self.assertCheckedIn('member', event, count=1)
        self.assertCustomAnswers('member')

    def test_guest_checkin_custom(self):
        self.reset()

        event = self.event_custom_checkin
        custom_form_data = self.get_custom_form_data()
        guest_form = self.__class__.get_guest_form_data()

        post_data = {
            'event_id': event.event_id,
            **guest_form,
            **custom_form_data
        }

        self.client.post(self.checkin_path, post_data)

        self.assertCheckedIn('guest', event, count=1)
        self.assertCustomAnswers('guest')


class TestAdminCheckin(TestEventCheckinHelpers):
    def setUp(self):
        super().setUp()

        self.__class__.delete_existing_events()

        event_time_fields = self.make_event_time_fields()

        self.event = self.make_event(event_time_fields)
        self.event_custom_checkin = self.make_event_custom_checkin(event_time_fields)

        self.checkin_path = reverse('event_checkin')

    def reset(self):
        event_ids = [self.event.event_id, self.event_custom_checkin.event_id]
        EventCheckin.objects.filter(checkedin_event__event_id__in=event_ids).delete()
        EventRSVP.objects.filter(rsvp_event__event_id__in=event_ids).delete()

    def test_admin_checkin_user(self):
        self.reset()

        path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        data = {
            'type': 'check-in'
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data)
        self.logout_user(self.client)

        self.assertCheckedIn('member', self.event, count=1, assert_no_other_checkins=True)

    def test_admin_checkin_guest(self):
        self.reset()
        path = reverse('admin-checkin-guest', kwargs={'event_id': self.event.event_id})

        guest_form = self.__class__.get_guest_form_data()

        data = {
            ** guest_form
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data)
        self.logout_user(self.client)

        self.assertCheckedIn('guest', self.event, count=1, assert_no_other_checkins=True)

    def test_admin_checkin_user_multiple(self):
        self.reset()

        path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        data = {
            'type': 'check-in'
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data)
        self.client.post(path, data)
        self.logout_user(self.client)

        self.assertCheckedIn('member', self.event, count=2, assert_no_other_checkins=True)

    def test_admin_checkin_user_then_user_checkin_self(self):
        self.reset()

        path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        data = {
            'type': 'check-in'
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data)
        self.logout_user(self.client)

        path = self.checkin_path
        data = {'event_id': self.event.event_id}

        self.login_user(self.client, self.regular.user.email, '1234')
        self.client.post(path, data)
        self.logout_user(self.client)

        self.assertCheckedIn('member', self.event, count=2, assert_no_other_checkins=True)

    def test_user_checkin_self_then_admin_checkin_user(self):
        path = self.checkin_path
        data = {'event_id': self.event.event_id}

        self.login_user(self.client, self.regular.user.email, '1234')
        self.client.post(path, data)
        self.logout_user(self.client)

        path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        data = {
            'type': 'check-in'
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data)
        self.logout_user(self.client)

        self.assertCheckedIn('member', self.event, count=2, assert_no_other_checkins=True)

    def test_admin_checkin_user_custom(self):
        self.reset()

        event = self.event_custom_checkin

        path = reverse('admin-checkin', kwargs={'event_id': self.event_custom_checkin.event_id, 'user_id': self.regular.user.id})

        custom_form_data = self.get_custom_form_data()

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, {'type': 'check-in', **custom_form_data})

        self.assertCheckedIn('member', event, count=1, assert_no_other_checkins=True)
        self.assertCustomAnswers('member')

    def test_admin_checkin_guest_custom(self):
        self.reset()

        event = self.event_custom_checkin

        path = reverse('admin-checkin-guest', kwargs={'event_id': self.event_custom_checkin.event_id})

        guest_form = self.__class__.get_guest_form_data()
        custom_form_data = self.get_custom_form_data()

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, {'type': 'check-in', **custom_form_data, **guest_form})

        self.assertCheckedIn('guest', event, count=1, assert_no_other_checkins=True)
        self.assertCustomAnswers('guest')

    def test_admin_checkout(self):
        """
        Tests admin checkout for user with one checkin
        """
        self.reset()

        # # # # # # # # # # # #
        # Admin Checkin User  #
        # # # # # # # # # # # #

        path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        data_checkin = {
            'type': 'check-in'
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data_checkin)

        self.assertCheckedIn('member', self.event, count=1, assert_no_other_checkins=True)

        # # # # # # # # # # # #
        # Admin Checkout User #
        # # # # # # # # # # # #

        checkin = EventCheckin.objects.get(member=self.regular, checkedin_event=self.event)

        data_checkout = {
            'type': 'check-out',
            'date': str(checkin.created),
        }

        self.client.post(path, data_checkout)
        self.logout_user(self.client)

        self.assertCheckedIn('member', self.event, count=0, assert_no_other_checkins=True)

    def test_admin_checkout_2(self):
        """
        Tests admin checkout for user with multiple checkins
        """
        self.reset()

        # # # # # # # # # # # #
        # Admin Checkin User  #
        # # # # # # # # # # # #

        path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        data_checkin = {
            'type': 'check-in'
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(path, data_checkin)
        self.client.post(path, data_checkin)
        self.client.post(path, data_checkin)

        self.assertCheckedIn('member', self.event, count=3, assert_no_other_checkins=True)

        # # # # # # # # # # # #
        # Admin Checkout User #
        # # # # # # # # # # # #

        checkins = EventCheckin.objects.filter(member=self.regular, checkedin_event=self.event)

        checkin_0: EventCheckin = checkins[0]
        checkin_1: EventCheckin = checkins[1]
        checkin_2: EventCheckin = checkins[2]

        data_checkout = {
            'type': 'check-out',
            'date': str(checkin_1.created),
        }

        self.client.post(path, data_checkout)
        self.logout_user(self.client)

        valid_checkin_ids = {checkin_0.id, checkin_2.id}
        self.assertCheckedIn('member', self.event, count=2, assert_no_other_checkins=True, checkin_ids=valid_checkin_ids)

    def admin_checkout_after_user_rsvp(self):
        self.reset()

        # # # # # # # #
        # User RSVPs  #
        # # # # # # # #

        self.assertEqual(0, self.event.get_attending_users_count())

        rsvp_path = reverse('event_rsvp')
        rsvp_data = {
            'event_id': self.event.event_id,
            'user_decision': 'yes'
        }

        self.login_user(self.client, self.regular.user.email, self.password)
        self.client.post(rsvp_path, rsvp_data)

        self.assertEqual(1, self.event.get_attending_users_count())

        # # # # # # # # # #
        # User Checks In  #
        # # # # # # # # # #

        self.client.post(self.checkin_path, {'event_id': self.event.event_id})
        self.logout_user(self.client)
        self.assertCheckedIn('member', self.event, count=1, assert_no_other_checkins=True)

        # # # # # # # # # # # # #
        # Admin Checks Out User #
        # # # # # # # # # # # # #

        admin_checkout_path = reverse('admin-checkin', kwargs={'event_id': self.event.event_id, 'user_id': self.regular.user.id})

        checkin = EventCheckin.objects.get(member=self.regular, checkedin_event=self.event)

        data_checkout = {
            'type': 'check-out',
            'date': str(checkin.created),
        }

        self.login_user(self.client, self.admin.user.email, '1234')
        self.client.post(admin_checkout_path, data_checkout)
        self.logout_user(self.client)

        self.assertCheckedIn('member', self.event, count=0, assert_no_other_checkins=True)
        self.assertEqual(1, self.event.get_attending_users_count())


class TestAllCheckin(TestEventCheckin, TestAdminCheckin):
    pass
