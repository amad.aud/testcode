from auto_test_utility.utills import AbastractUniTestBase
from branch.factory import StartupMemberFactory
from django.test import Client
from django.urls import reverse
from forms.factory import (
    QuestionFactory,
    ChoiceFactory,
    MemberApplicantFactory,
    AnonymousApplicantFactory
)
from forms.models import Question, Answer, MemberApplicant, AnonymousApplicant
from trunk.factory.event import CompetitionFactory
from trunk.models import Event
from django.utils import timezone
from datetime import timedelta, datetime


class CompetitionApplicationTest(AbastractUniTestBase):
    def setUp(self):
        super(CompetitionApplicationTest, self).setUp()
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)

        self.comp = CompetitionFactory.create(end=timezone.now() + timedelta(hours=1))
        self.mc = QuestionFactory.create()
        self.dd = QuestionFactory.create(q_type=Question.DD)
        for x in range(3):
            new_mc_choice = ChoiceFactory.create()
            new_dd_choice = ChoiceFactory.create()
            self.mc.choices.add(new_mc_choice)
            self.dd.choices.add(new_dd_choice)
        self.pp = QuestionFactory.create(q_type=Question.PP, is_required=True,
            char_limit=30)
        self.dt = QuestionFactory.create(q_type=Question.DT)
        self.fu = QuestionFactory.create(q_type=Question.FU)

        self.comp.form.questions.add(self.mc)
        self.comp.form.questions.add(self.dd)
        self.comp.form.questions.add(self.pp)
        self.comp.form.questions.add(self.dt)
        self.comp.form.questions.add(self.fu)

    def test_member_application(self):
        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm)

    def test_member_draft(self):
        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, is_draft=True)

    def test_guest_application(self):
        self.comp.is_public_competition = True
        self.comp.save()

        guest_data = {
            'first_name': 'test',
            'last_name': 'submission',
            'email': 'guest1@email.com'
        }
        self.submit_application(guest_data=guest_data)

    def test_guest_draft(self):
        self.comp.is_public_competition = True
        self.comp.save()
        guest_data = {
            'first_name': 'test',
            'last_name': 'draft',
            'email': 'guest2@email.com'
        }
        self.submit_application(guest_data=guest_data, is_draft=True)

    def test_guest_apply_to_member_only_comp(self):
        self.comp.is_public_competition = False
        self.comp.save()

        guest_data = {
            'first_name': 'test',
            'last_name': 'submission',
            'email': 'guest3@email.com'
        }
        self.submit_application(guest_data=guest_data, is_public=False)

    def test_guest_apply_no_name(self):
        self.comp.is_public_competition = True
        self.comp.save()

        guest_data = {
            'first_name': '',
            'last_name': '',
            'email': ''
        }
        self.submit_application(guest_data=guest_data)

    def test_conditional_questions(self):
        self.mc.is_conditional = True
        self.mc.save()
        first_choice = self.mc.choices.first()
        first_choice.goto_question = self.pp
        first_choice.save()

        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, requirements_met=False)
        self.submit_application(sm=sm, condition_selected=False, requirements_met=False)

    def test_too_long_response(self):
        self.comp.form.enforce_limits = True
        self.comp.form.save()

        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, too_long=True)
        self.logout_user(self.client)

        self.comp.form.enforce_limits = False
        self.comp.form.save()

        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, too_long=True)

    def test_future_comp(self):
        self.comp.start = timezone.now() + timedelta(minutes=30)
        self.comp.save()

        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, is_open=False)

    def test_past_comp(self):
        self.comp.start = timezone.now() - timedelta(hours=1)
        self.comp.end = timezone.now() - timedelta(minutes=1)
        self.comp.save()

        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, is_open=False)

    def test_private_comp(self):
        self.comp.is_private = True
        self.comp.is_public_competition = True
        self.comp.save()

        invited_sm = StartupMemberFactory.create()
        uninvited_sm = StartupMemberFactory.create()
        invited_guest = AnonymousApplicantFactory.create(
            event=self.comp, is_invited=True
        )
        self.comp.invited_users.add(invited_sm.user)

        event_url = reverse('view_event_title', kwargs={
            'event_short_id': self.comp.get_short_event_id(),
            'title': self.comp.get_url_title()
        })
        get_url = reverse('competition-form', kwargs={'event_id': self.comp.event_id})
        # invited member
        self.login_user(self.client, invited_sm.user.email, self.password)
        response = self.client.get(get_url)
        self.assertEqual(200, response.status_code)
        self.logout_user(self.client)

        # non-invited member
        self.login_user(self.client, uninvited_sm.user.email, self.password)
        with self.assertRaises(ValueError):
            self.client.get(get_url)
        self.logout_user(self.client)

        # non-invited guest
        response = self.client.get(get_url)
        self.assertEqual(event_url, response.url)

        # non-invited guest
        guest_get_url = reverse('competition-form-guest-answers', kwargs={
            'event_id': self.comp.event_id,
            'anon_applicant_id': invited_guest.applicant_id
        })
        response = self.client.get(guest_get_url)
        self.assertEqual(200, response.status_code)

    def test_missing_required_input(self):
        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm, requirements_met=False)

    def test_no_venture_name(self):
        self.comp.form.requires_venture = True
        self.comp.form.save()

        sm = StartupMemberFactory.create()
        self.login_user(self.client, sm.user.email, self.password)
        self.submit_application(sm=sm)
        self.submit_application(sm=sm, is_draft=True)
        self.submit_application(sm=sm, venture_name='test venture')


    def submit_application(self,
                           sm=None,
                           guest_data=None,
                           is_draft=False,
                           is_public=True,
                           condition_selected=True,
                           too_long=False,
                           is_open=True,
                           requirements_met=True,
                           venture_name=''):
        data = {
            'count': 5,
            'type0': 'Multiple Choice',
            'id0': self.mc.id,
            'type1': 'Dropdown',
            'id1': self.dd.id,
            'dd1': self.dd.choices.last().id,
            'type2': 'Paragraph',
            'id2': self.pp.id,
            'type3': 'Date Time',
            'id3': self.dt.id,
            'dt3': '01/01/2021',
            'type4': 'File Upload',
            'id4': self.fu.id
        }
        if guest_data is not None:
            data['first_name'] = guest_data['first_name']
            data['last_name'] = guest_data['last_name']
            data['email'] = guest_data['email']
        if is_draft:
            data['is_draft'] = is_draft
        if condition_selected:
            data['mc0'] = [self.mc.choices.first().id, self.mc.choices.last().id]
        else:
            data['mc0'] = [self.mc.choices.last().id]
        if requirements_met:
            if too_long:
                data['pp2'] = 'the quick brown fox jumps over the lazy dog'
            else:
                data['pp2'] = 'hello world'
        else:
            data['pp2'] = ''
        if self.comp.form.requires_venture:
            data['app_venture'] = venture_name
        post_url = reverse('competition-enter', kwargs={'event_id': self.comp.event_id})
        self.client.post(post_url, data)

        guest = None
        is_applied = not is_draft
        assert_value = is_open and not (too_long and self.comp.form.enforce_limits) and \
            ((requirements_met and condition_selected) or (not requirements_met and not condition_selected)) and \
            not (self.comp.form.requires_venture and not venture_name and not is_draft)
        if sm is not None:
            self.assertEqual(assert_value, MemberApplicant.objects.filter(
                member=sm, event=self.comp, is_applied=is_applied).exists()
            )
        if guest_data is not None:
            guest_email = guest_data['email']
            if assert_value and (not is_public or guest_email == ''):
                assert_value = False
            self.assertEqual(assert_value, AnonymousApplicant.objects.filter(
                email=guest_email, event=self.comp, is_applied=is_applied).exists()
            )
        if is_public and is_open and (guest_data is None or guest_email != ''):
            if guest_data is not None:
                guest = AnonymousApplicant.objects.get(
                    email=guest_email, event=self.comp, is_applied=is_applied)
            self.assertTrue(Answer.objects.filter(
                q_id=self.mc.id, member=sm, anonymous_applicant=guest, is_draft=is_draft).exists()
            )
            self.assertTrue(Answer.objects.filter(
                q_id=self.dd.id, member=sm, anonymous_applicant=guest, is_draft=is_draft,
                choices=self.dd.choices.last()).exists()
            )
            self.assertTrue(Answer.objects.filter(
                q_id=self.pp.id, member=sm, anonymous_applicant=guest, is_draft=is_draft,
                text=data['pp2']).exists()
            )
            dt = datetime.strptime('01/01/2021', '%m/%d/%Y')
            self.assertTrue(Answer.objects.filter(
                q_id=self.dt.id, member=sm, anonymous_applicant=guest, is_draft=is_draft,
                date=dt).exists()
            )
