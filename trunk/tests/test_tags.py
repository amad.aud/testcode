from django.test.utils import override_settings
from auto_test_utility.utills import AbastractUniTestBase
from django.urls import reverse
from StartupTree.loggers import app_logger

'''
test of the ability to toggle whether users can add their own profile tags
'''

class ToggleUserTagsTest(AbastractUniTestBase):
    def test_toggle(self):
        self.cornell_admin_login()

        # toggle
        self.driver.get(self.domain + reverse('uni-manage-pages'))
        # existence of this element proves that it is on
        toggle = self.driver.find_element_by_class_name("tt-switch-on")
        toggle.click()
        self.driver.find_element_by_class_name("tt-switch-off")
        self.driver.find_element_by_id("save-tags").click()

        # check that change saved
        success_msg = self.driver.find_element_by_id("success-msg")
        self.driver.find_element_by_class_name("tt-switch-off")

        # log in as regular user and verify unable to add tags
        self.driver.get(self.domain + reverse('logout'))
        self.cornell_user_login()
        self.driver.get(self.domain + reverse('edit-member-profile', kwargs={'url_name': 'jcortle'}))
        self.assertEqual(len(self.driver.find_elements_by_id("add-tags")), 0)

        # log back in as admin, toggle back, and repeat
        self.driver.get(self.domain + reverse('logout'))
        self.cornell_admin_login()
        self.driver.get(self.domain + reverse('uni-manage-pages'))
        toggle = self.driver.find_element_by_class_name("tt-switch-off")
        toggle.click()
        self.driver.find_element_by_class_name("tt-switch-on")
        self.driver.find_element_by_id("save-tags").click()

        success_msg = self.driver.find_element_by_id("success-msg")
        self.driver.find_element_by_class_name("tt-switch-on")

        self.driver.get(self.domain + reverse('logout'))
        self.cornell_user_login()
        self.driver.get(self.domain + reverse('edit-member-profile', kwargs={'url_name': 'jcortle'}))
        self.driver.find_element_by_id("add-tags")
