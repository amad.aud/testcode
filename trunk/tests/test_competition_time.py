from django.test import TestCase
from django.utils import timezone
from trunk.models import (University,Event)


class TimeTest(TestCase):

    serialized_rollback = True

    def setUp(self):
        self.university = University.objects.create_empty_university(name='test')
        self.today = timezone.now()
        self.day_diff = 18
        self.start = self.today
        self.end = self.start + timezone.timedelta(days=self.day_diff)
        # Create a competition
        self.test_event = Event.objects.create_event(name='name',
                     description='desc',
                     start=self.start,
                     end=self.end,
                     timezone='US/Eastern',
                     university=self.university,
                     creator=None,
                     status=1,  # FIXME: for now, allow by default.
                     event_type=9,
                     event_board=None,
                     display_attendees=True,
                     display_applicants=True,
                     display_view_count=True,
                     display_activity_feed=True,
                     pin_to_feed=False,
                     allow_checkins=False,
                     allow_rsvp=True,
                     allow_max_rsvp=False,
                     max_rsvp=None,
                     allow_guest_rsvp=True,
                     allow_guest_checkins=True,
                     free_food=False,
                     free_swag=False,
                     is_published=True,
                     is_private=False,
                     is_archived_event=False,
                     is_competition=True,
                     is_application=False,
                     is_survey=False,
                     can_edit_questions=True,
                     can_edit_rubric=True,
                     is_saved=False,
                     send_emails_days_prior=7,
                     send_updates=True,
                     location=None,
                     map_location=None,
                     latitude=-33.868,
                     longitude=151.2195,
                     event_website=None,
                     ticket_website=None,
                     is_public_competition=False)

    def test_competition_time(self):
        # Datetime must be AWARE for proper conversion
        timeuntil = self.test_event.calc_timeuntil()
        # Countdown for remaining days includes the current day
        self.assertEqual(timeuntil, self.day_diff)

    def test_last_day(self):
        self.test_event.end = self.today + timezone.timedelta(hours=23, minutes=59, seconds=59)
        timeuntil = self.test_event.calc_timeuntil()
        # Countdown for remaining days includes the current day
        self.assertEqual(timeuntil, 1)
