from auto_test_utility.utills import AbastractUniTestBase
from trunk.models import University, UniversityStyle, School
from branch.models import Label, StartupMember, Startup
from django.urls import reverse


class AdminDashboardListTest(AbastractUniTestBase):
    def setUp(self):
        super(AdminDashboardListTest, self).setUp()
        self.client.post(self.login_url,
                         {'email': self.admin.user.email,
                          'password': self.password})

    def tearDown(self):
        super(AdminDashboardListTest, self).tearDown()
        self.client.get(reverse('logout'))

    def test_manage_users(self):
        self.create_dummy_users(self.cornell)
        response = self.client.get(reverse('uni-manage-users'))
        # main page will have the <title>
        self.assertContains(response, 'Users | {0}'.format(self.cornell.get_program_name()), 1, 200, html=True)
        # check 1st page, which should have 15 users
        users = StartupMember.objects.filter(universities=self.cornell).values_list('user__email', 'first_name', 'last_name').order_by('first_name', 'last_name')[:15]
        for user in users:
            self.assertContains(response, user[0], 1, 200)
        response = self.client.get(reverse('uni-manage-users'), HTTP_X_PJAX='refresh')
        self.assertNotContains(response, 'Users | {0}'.format(self.cornell.get_program_name()), 200, html=True)
        for user in users:
            self.assertContains(response, user[0], 1, 200)

    def test_manage_projects(self):
        self.create_dummy_projects(self.cornell)
        response = self.client.get(reverse('uni-manage-projects'))
        # main page will have the <title>
        self.assertContains(response, 'Projects | {0}'.format(self.cornell.get_program_name()), 1, 200, html=True)
        # check 1st page, which should have 15 users
        projects = Startup.objects.filter(university=self.cornell, is_project=True).values_list('name').order_by('name')[:15]
        for project in projects:
            self.assertContains(response, "{0}</a></strong>".format(project[0]), 1, 200)
        response = self.client.get(reverse('uni-manage-projects'), HTTP_X_PJAX='refresh')
        self.assertNotContains(response, 'Projects | {0}'.format(self.cornell.get_program_name()), 200, html=True)
        for project in projects:
            self.assertContains(response, "{0}</a></strong>".format(project[0]), 1, 200)

    def test_manage_ventures(self):
        self.create_dummy_startups(self.cornell)
        response = self.client.get(reverse('uni-manage-ventures'))
        # main page will have the <title>
        self.assertContains(response, 'Ventures | {0}'.format(self.cornell.get_program_name()), 1, 200, html=True)
        # check 1st page, which should have 15 users
        ventures = Startup.objects.filter(university=self.cornell).values_list('name').order_by('name')[:15]
        for venture in ventures:
            self.assertContains(response, "{0}</a></strong>".format(venture[0]), 1, 200)
        response = self.client.get(reverse('uni-manage-ventures'), HTTP_X_PJAX='refresh')
        self.assertNotContains(response, 'Ventures | {0}'.format(self.cornell.get_program_name()), 200, html=True)
        for venture in ventures:
            self.assertContains(response, "{0}</a></strong>".format(venture[0]), 1, 200)


class TrunkCustomizationViewTest(AbastractUniTestBase):

    def setUp(self):
        super(TrunkCustomizationViewTest, self).setUp()

    def test_get_unauthorized(self):
        """
        Test if the page is only visible to university admin and staffs
        :return:
        """
        self.client.post(self.login_url,
                         {'email': self.regular.user.email,
                          'password': self.password})
        response = self.client.get(reverse('uni-customize-general'))
        # has no permission. redirect to home page.
        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, "/")

        self.client.get(reverse('logout'))

        self.client.post(self.login_url,
                         {'email': self.admin.user.email,
                          'password': self.password})
        self.assertEquals(response.status_code, 302)
        response = self.client.get(reverse('uni-customize-general'))
        self.assertEquals(response.status_code, 200)

        self.client.get(reverse('logout'))
        self.client.post(self.login_url,
                         {'email': self.admin.user.email,
                          'password': self.password})
        response = self.client.get(reverse('uni-customize-general'))
        self.assertEquals(response.status_code, 200)

    def test_change_names(self):
        self.client.post(self.login_url,
                         {'email': self.admin.user.email,
                          'password': self.password})
        current_name = self.cornell.name
        current_program_name = self.cornell.program_name
        new_uni_name = current_name + "Hello Hello Hello"
        new_program_name = current_program_name + "Hello Hello Bye"
        response = \
            self.client.post(reverse('uni-customize-general'),
                             {'uni-name': new_uni_name,
                              'program-name': new_program_name})
        self.assertContains(response, "The change was successfully saved.", 1, 200)
        self.assertEquals(University.objects.get(id=self.cornell.id).name, new_uni_name)
        self.assertEquals(University.objects.get(id=self.cornell.id).program_name, new_program_name)

    def test_email_restriction_invalid_url(self):
        self.client.post(self.login_url,
                         {'email': self.admin.user.email,
                          'password': self.password})
        invalid_domain_format = '@cornell.edu'
        response = \
            self.client.post(reverse('uni-customize-general'),
                             {'email-domains[]': [invalid_domain_format]})
        self.assertContains(response, "{0} is not a valid email address domain.".format(invalid_domain_format), 1, 200)

    def test_email_restriction_valid_url(self):
        self.client.post(self.login_url,
                         {'email': self.admin.user.email,
                          'password': self.password})
        response = \
            self.client.post(reverse('uni-customize-general'),
                             {'email-domains[]': ['cornell.edu']})
        self.assertContains(response, "The change was successfully saved.", 1, 200)
        # check if this actually works.
        self.client.get(reverse('logout'))
        email = 'vfffvsigogftt@sunghopark.com'
        password = '1234go'
        data = {
            'email': email,
            'first_name': 'h',
            'last_name': 'o',
            'password1': password,
            'password2': password,
        }
        response = self.client.post(reverse('user-signup-account'), data, follow=True)
        self.assertEquals(response.status_code, 400)
        email = 'vfffvsigogftt@cornell.edu'
        password = '1234go'
        data = {
            'email': email,
            'first_name': 'h',
            'last_name': 'o',
            'password1': password,
            'password2': password,
        }
        response = self.client.post(reverse('user-signup-account'), data, follow=True)
        self.assertEquals(response.status_code, 200)

    def test_school_change(self):
        self.client.post(self.login_url,
                 {'email': self.admin.user.email,
                  'password': self.password})
        school1 = "this is a weird school"
        school2 = "this is not a weird school"
        self.assertFalse(School.objects.filter(name=school1).exists())
        self.assertFalse(School.objects.filter(name=school2).exists())
        self.client.post(reverse('uni-customize-general'),
                         {'new_school': [school1, school2]})
        self.assertTrue(School.objects.filter(name=school1).exists())
        self.assertTrue(School.objects.filter(name=school2).exists())
        school1_obj = School.objects.get(name=school1)
        school2_obj = School.objects.get(name=school2)
        school1_new = "i hate this."
        self.client.post(reverse('uni-customize-general'),
                         {'school_id[]': [school1_obj.id, school2_obj.id],
                          'edit_school': [school1_new, school2_obj.name]})
        self.assertFalse(School.objects.filter(name=school1).exists())
        self.assertTrue(School.objects.filter(name=school2).exists())
        self.assertTrue(School.objects.filter(name=school1_new).exists())

    def test_group_change(self):
        self.client.post(self.login_url,
                 {'email': self.admin.user.email,
                  'password': self.password})
        label1 = "coca cola"
        label2 = "pepsi"
        self.assertFalse(Label.objects.filter(title=label1).exists())
        self.assertFalse(Label.objects.filter(title=label2).exists())
        self.client.post(reverse('uni-customize-general'),
                         {'label[]': [label1, label2]})
        self.assertTrue(Label.objects.filter(title=label1).exists())
        self.assertTrue(Label.objects.filter(title=label2).exists())
        label1_obj = Label.objects.get(title=label1)
        label2_obj = Label.objects.get(title=label2)
        label1_new = "diet pepsi"
        self.client.post(reverse('uni-customize-general'),
                         {'label_id[]': [label1_obj.label_id, label2_obj.label_id],
                          'edit_label': [label1_new, label2]})
        self.assertFalse(Label.objects.filter(title=label1).exists())
        self.assertTrue(Label.objects.filter(title=label2).exists())
        self.assertTrue(Label.objects.filter(title=label1_new).exists())
