from auto_test_utility.utills import AbastractUniTestBase
from django.test import Client
from django.urls import reverse
from forms.factory import (
    QuestionFactory,
    ChoiceFactory,
    RubricFactory
)
from forms.models import Question, Choice, Rubric
from trunk.factory.event import EventFactory, CompetitionFactory
from trunk.models import Event


class TemplatesTest(AbastractUniTestBase):
    def setUp(self):
        super(TemplatesTest, self).setUp()
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)

    def test_create_template_from_existing_event(self):
        self.login_user(self.client, self.admin.user.email, self.password)
        original_event = EventFactory.create()

        data = {
            'event_name': original_event.name,
            'template_name': 'event template',
            'event_start_date': original_event.start.date().strftime('%m/%d/%Y'),
            'event_start_time': original_event.start.time().strftime('%H:%M:%S'),
            'event_end_date': original_event.end.date().strftime('%m/%d/%Y'),
            'event_end_time': original_event.end.time().strftime('%H:%M:%S'),
            'save_as_template': 'true',
            'save_as_event': 'true',
            'event-type': 'other'
        }
        post_url = reverse('uni-manage-events-edit', kwargs={'event_id': original_event.event_id})
        self.client.post(post_url, data)

        # confirm that new template was created
        self.assertTrue(Event.objects.filter(name='event template', is_template_only=True).exists())
        new_template = Event.objects.get(name='event template', is_template_only=True)
        # confirm that new template name did not overwrite original event name
        self.assertNotEqual(original_event.name, new_template.name)
        # confirm that original event was not saved as a template
        self.assertFalse(original_event.is_template_only)
        self.logout_user(self.client)

    def test_create_template_from_existing_comp(self):
        self.login_user(self.client, self.admin.user.email, self.password)
        original_comp = CompetitionFactory.create()
        mc_q = QuestionFactory.create(q_type=Question.MC, is_required=False)
        pp_q_1 = QuestionFactory.create(q_type=Question.PP, is_required=True)
        pp_q_2 = QuestionFactory.create(q_type=Question.PP, is_required=False)
        original_comp.form.questions.add(mc_q)
        original_comp.form.questions.add(pp_q_1)
        original_comp.form.questions.add(pp_q_2)
        for x in range(3):
            c = ChoiceFactory.create()
            if x == 0:
                c.goto_question = pp_q_2
                c.save()
            mc_q.choices.add(c)

            r = RubricFactory.create()
            original_comp.form.rubrics.add(r)

        data = {
            'event_name': original_comp.name,
            'template_name': 'comp template',
            'template_id': original_comp.event_id,
            'event_start_date': original_comp.start.date().strftime('%m/%d/%Y'),
            'event_start_time': original_comp.start.time().strftime('%H:%M:%S'),
            'event_end_date': original_comp.end.date().strftime('%m/%d/%Y'),
            'event_end_time': original_comp.end.time().strftime('%H:%M:%S'),
            'save_as_template': 'true',
            'save_as_event': 'true',
            'event-type': 'competition'
        }
        post_url = reverse('uni-edit-competition', kwargs={'event_id': original_comp.event_id})
        self.client.post(post_url, data)

        # confirm that new template was created
        self.assertTrue(Event.objects.filter(name='comp template', is_template_only=True).exists())
        new_template = Event.objects.get(name='comp template', is_template_only=True)
        # confirm that new template name did not overwrite original event name
        self.assertNotEqual(original_comp.name, new_template.name)
        # confirm that original event was not saved as a template
        self.assertFalse(original_comp.is_template_only)
        # confirm that questions and rubrics carried over, including conditions
        self.assertTrue(3, new_template.form.questions.all().count())
        self.assertTrue(3, new_template.form.rubrics.all().count())
        self.confirm_questions_and_rubrics(original_comp.form, new_template.form)

        self.logout_user(self.client)

    def test_readymade_comp_template(self):
        self.setup_readymade()
        self.login_user(self.client, self.admin.user.email, self.password)

        post_url = reverse('competition-generate-template', kwargs={'event_id': self.readymade_comp.event_id})
        self.client.post(post_url)

        # confirm that new comp was created and exists on correct platform
        self.assertTrue(Event.objects.filter(name=self.readymade_comp.name,
            university=self.cornell, is_competition=True, is_template_only=False).exists()
        )
        new_comp = Event.objects.get(name=self.readymade_comp.name,
            university=self.cornell, is_competition=True, is_template_only=False)
        self.confirm_questions_and_rubrics(self.readymade_comp.form, new_comp.form)

        self.logout_user(self.client)


    def confirm_questions_and_rubrics(self, original_form, new_form):
        new_question_ids = new_form.questions.all().values_list('id', flat=True)
        new_rubric_ids = new_form.rubrics.all().values_list('id', flat=True)
        for q in original_form.questions.all():
            self.assertTrue(Question.objects.filter(id__in=new_question_ids,
                q_type=q.q_type, text=q.text, number=q.number,
                is_required=q.is_required).exists()
            )
            if q.q_type == Question.MC:
                new_choice_ids = new_form.questions.get(q_type=Question.MC).choices.all()
                for c in q.choices.all():
                    self.assertTrue(Choice.objects.filter(id__in=new_choice_ids, text=c.text).exists())
                    if c.goto_question:
                        new_goto_question = Question.objects.get(
                            id__in=new_question_ids, text=c.goto_question.text
                        )
                        self.assertTrue(Choice.objects.filter(
                            id__in=new_choice_ids, text=c.text,
                            goto_question=new_goto_question).exists()
                        )
        for r in original_form.rubrics.all():
            self.assertTrue(Rubric.objects.filter(id__in=new_rubric_ids,
                r_type=r.r_type, text=r.text, number=r.number,
                is_required=r.is_required).exists()
            )
