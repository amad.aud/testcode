from auto_test_utility.utills import AbastractUniTestBase
from branch.factory.user_data import generate_member
from django.core import mail
from trunk.views.eprogram import Announcement
from django.test import RequestFactory, Client
from django.urls import reverse
from inbox.models import InboxConversationGroup
import mock
from django.db import transaction


class AnnouncementTest(AbastractUniTestBase):
    def setUp(self):
        super(AnnouncementTest, self).setUp()
        # Generate users: both none verified email and verified emails
        # 2+2 verfied, 2 unverfied users
        generate_member("Sungho", "Park", "sungho@startuptree.co", self.cornell)
        generate_member("Theresa", "Kim", "theresa@startuptree.co", self.cornell, verified=False)
        generate_member("Peter", "Cortle", "peter@startuptree.co", self.cornell, verified=False)
        generate_member("Will", "Larkin", "will@startuptree.co", self.cornell)
        self.client = Client(SERVER_NAME="cornell.startuptreetest.co:8000")
        response = self.client.post(reverse("regular_login"), {"email": self.USER_EMAIL, "password": self.password})
        # self.assertRedirects(response, reverse("uni-panel"))

    def test_announcment_basic(self):
        # with mock.patch.object(transaction, 'on_commit', lambda t: t()):
        self.assertEqual(InboxConversationGroup.objects.all().count(), 0)
        response = self.client.post(reverse("uni-new-mass-msg"),
                         {"subject": "Hello from the World!",
                          "content": "I am all yours"})
        self.assertEqual(InboxConversationGroup.objects.all().count(), 1)
        self.assertRedirects(response, reverse("uni-view-messages",
                                               kwargs={"msg_id": InboxConversationGroup.objects.all()[0].conversation_id}))
        self.assertEqual(len(mail.outbox), 4)  # 4 verified users only

    def test_announcement_group(self):
        # TODO
        pass