from auto_test_utility.utills import AbastractUniTestBase, SeleniumTest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from django.urls.base import reverse
from branch.models import StartupMember
from forms.models import Answer
from trunk.models import Event
from forms.serializer.forms import custom_member_dict
from StartupTree.loggers import app_logger


class ApplicationTest(AbastractUniTestBase):
    def test_create_application(self):
        self.cornell_admin_login()
        self.driver.get(self.domain + reverse('uni-create-competition'))
        self.assertEqual(self.domain + reverse('uni-create-competition'), self.driver.current_url)

        '''
        CREATE APPLICATION STEP 1
        '''
        # application name
        name = self.driver.find_element_by_name('event_name')
        name.send_keys('test application')

        # location
        location = self.driver.find_element_by_name('event_location')
        location.send_keys('Art Gallery Road, Sydney, New South Wales, Australia')
        self.select_autocomplete()

        # date + times
        start_date = self.driver.find_element_by_name('event_start_date')
        start_date.send_keys('01/01/2018')
        start_time = self.driver.find_element_by_name('event_start_time')
        start_time.send_keys('12:00 PM')
        end_date = self.driver.find_element_by_name('event_end_date')
        end_date.send_keys('12/31/2018')
        end_time = self.driver.find_element_by_name('event_end_time')
        end_time.send_keys('12:00 PM')

        # description
        frame = self.driver.find_element_by_xpath("//iframe[@id='id_text_iframe']")
        self.driver.switch_to.frame(frame)
        self.driver.find_element_by_class_name('note-editable').send_keys('description')
        self.driver.switch_to.default_content()

        # publish application
        self.driver.find_element_by_name('toggle-feed').click()
        self.driver.find_element_by_name('toggle-publish').click()

        # submit form
        self.driver.find_element_by_id('create_event').click()

        '''
        CREATE APPLICATION STEP 2
        '''
        self.assertIn('Questions', self.driver.title)
        # multiple choice
        mc_title = self.driver.find_element_by_id('mc0')
        mc_title.send_keys('multiple choice')
        self.driver.find_element_by_xpath("//div[@data-help-id='mc0-help']").click()
        mc_help = self.driver.find_element_by_id('mc0-help')
        mc_help.send_keys('multiple choice description')
        mc_choice_1 = self.driver.find_element_by_id('mc0_c0')
        mc_choice_1.send_keys('choice 1')
        mc_choice_2 = self.driver.find_element_by_id('mc0_c1')
        mc_choice_2.send_keys('choice 2')
        self.driver.find_element_by_xpath("//input[@placeholder='+ Add Choice']").click()
        mc_choice_3 = self.driver.find_element_by_id('mc0_c2')
        mc_choice_3.send_keys('choice 3')
        self.driver.find_element_by_id('mc0_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # dropdown
        self.driver.find_element_by_class_name('question--picker--new1').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-dd1 type-dd uk-vertical-align']").click()
        dd_title = self.driver.find_element_by_id('dd1')
        dd_title.send_keys('dropdown')
        self.driver.find_element_by_xpath("//div[@data-help-id='dd1-help']").click()
        dd_help = self.driver.find_element_by_id('dd1-help')
        dd_help.send_keys('dropdown description')
        dd_choice_1 = self.driver.find_element_by_id('dd1_c0')
        dd_choice_1.send_keys('choice 1')
        dd_choice_2 = self.driver.find_element_by_id('dd1_c1')
        dd_choice_2.send_keys('choice 2')
        dd_choice_3 = self.driver.find_element_by_id('dd1_c2')
        dd_choice_3.send_keys('choice 3')
        # self.driver.find_element_by_class_name('js--add-dd').click()
        self.driver.find_element_by_xpath("//input[@class='form__input-bottom-border question--dd-choice--new dd1 js--add-dd']").click()
        dd_choice_4 = self.driver.find_element_by_id('dd1_c3')
        dd_choice_4.send_keys('choice 4')

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # text response
        self.driver.find_element_by_class_name('question--picker--new2').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-pp2 type-pp uk-vertical-align']").click()
        pp_title = self.driver.find_element_by_id('pp2')
        pp_title.send_keys('text response')
        self.driver.find_element_by_xpath("//div[@data-help-id='pp2-help']").click()
        pp_help = self.driver.find_element_by_id('pp2-help')
        pp_help.send_keys('text response description')
        char_lim = self.driver.find_element_by_id('pp2-cl')
        char_lim.clear()
        char_lim.send_keys('10')
        self.driver.find_element_by_id('pp2_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # date choice
        self.driver.find_element_by_class_name('question--picker--new3').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-dt3 type-dt uk-vertical-align']").click()
        dt_title = self.driver.find_element_by_id('dt3')
        dt_title.send_keys('date choice')
        self.driver.find_element_by_xpath("//div[@data-help-id='dt3-help']").click()
        dt_help = self.driver.find_element_by_id('dt3-help')
        dt_help.send_keys('date choice description')
        self.driver.find_element_by_id('dt3_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # file upload
        self.driver.find_element_by_class_name('question--picker--new4').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-fu4 type-fu uk-vertical-align']").click()
        fu_title = self.driver.find_element_by_id('fu4')
        fu_title.send_keys('file upload')
        self.driver.find_element_by_xpath("//div[@data-help-id='fu4-help']").click()
        fu_help = self.driver.find_element_by_id('fu4-help')
        fu_help.send_keys('file upload description')

        # submit and go through modal
        self.driver.find_element_by_id('next-btn').click()
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.ID, 'submit-questions')))
        self.driver.find_element_by_class_name('js--submit-form').click()
        wait.until(EC.title_contains(('Rubric')))

        '''
        CREATE APPLICATION STEP 3
        '''
        self.assertIn('Rubric', self.driver.title)
        # multiple choice
        r_mc_title = self.driver.find_element_by_id('mc0')
        r_mc_title.send_keys('multiple choice')
        self.driver.find_element_by_xpath("//div[@data-help-id='mc0-help']").click()
        r_mc_help = self.driver.find_element_by_id('mc0-help')
        r_mc_help.send_keys('multiple choice description')
        r_mc_choice_1 = self.driver.find_element_by_id('mc0_c0')
        r_mc_choice_1.send_keys('choice 1')
        r_mc_choice_2 = self.driver.find_element_by_id('mc0_c1')
        r_mc_choice_2.send_keys('choice 2')
        self.driver.find_element_by_xpath("//input[@placeholder='+ Add Choice']").click()
        r_mc_choice_3 = self.driver.find_element_by_id('mc0_c2')
        r_mc_choice_3.send_keys('choice 3')
        self.driver.find_element_by_id('mc0_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # text response
        self.driver.find_element_by_class_name('question--picker--new1').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-pp1 type-pp uk-vertical-align']").click()
        r_pp_title = self.driver.find_element_by_id('pp1')
        r_pp_title.send_keys('text response')
        self.driver.find_element_by_xpath("//div[@data-help-id='pp1-help']").click()
        r_pp_help = self.driver.find_element_by_id('pp1-help')
        r_pp_help.send_keys('text response description')
        r_char_lim = self.driver.find_element_by_id('pp1-cl')
        r_char_lim.clear()
        r_char_lim.send_keys('10')
        self.driver.find_element_by_id('pp1_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # linear scale
        self.driver.find_element_by_class_name('question--picker--new2').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-sc2 type-sc uk-vertical-align']").click()
        sc_title = self.driver.find_element_by_id('sc2')
        sc_title.send_keys('linear scale')
        self.driver.find_element_by_xpath("//div[@data-help-id='sc2-help']").click()
        sc_help = self.driver.find_element_by_id('sc2-help')
        sc_help.send_keys('linear scale description')
        maximum = self.driver.find_element_by_id('sc2-cp')
        maximum.click()
        maximum.send_keys('6')
        self.driver.find_element_by_id('sc2_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # forced ranking
        self.driver.find_element_by_class_name('question--picker--new3').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-fr3 type-fr uk-vertical-align']").click()
        fr_title = self.driver.find_element_by_id('fr3')
        fr_title.send_keys('forced ranking')
        self.driver.find_element_by_xpath("//div[@data-help-id='fr3-help']").click()
        fr_help = self.driver.find_element_by_id('fr3-help')
        fr_help.send_keys('forced ranking description')

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # slider
        self.driver.find_element_by_class_name('question--picker--new4').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-sl4 type-sl uk-vertical-align']").click()
        sl_title = self.driver.find_element_by_id('sl4')
        sl_title.send_keys('slider')
        self.driver.find_element_by_xpath("//div[@data-help-id='sl4-help']").click()
        sl_help = self.driver.find_element_by_id('sl4-help')
        sl_help.send_keys('slider description')
        out_of = self.driver.find_element_by_id('sl4-cp')
        out_of.send_keys('9')
        self.driver.find_element_by_id('sl4_req').click()

        self.driver.find_element_by_class_name('form__content--question-add--new').click()
        # star rating
        self.driver.find_element_by_class_name('question--picker--new5').click()
        self.driver.find_element_by_xpath("//li[@class='type-picker--img js--display-rt5 type-rt uk-vertical-align']").click()
        rt_title = self.driver.find_element_by_id('rt5')
        rt_title.send_keys('star rating')
        self.driver.find_element_by_xpath("//div[@data-help-id='rt5-help']").click()
        rt_help = self.driver.find_element_by_id('rt5-help')
        rt_help.send_keys('star rating description')
        self.driver.find_element_by_id('rt5_req').click()

        # submit
        self.driver.find_element_by_id('next-btn').click()
        wait.until(EC.title_contains(('Judges')))

        '''
        CREATE APPLICATION STEP 4
        '''
        self.assertIn('Judges', self.driver.title)
        judge_end_date = self.driver.find_element_by_name('judge_end_date')
        judge_end_date.send_keys('01/31/19')
        judge_end_time = self.driver.find_element_by_name('judge_end_time')
        judge_end_time.send_keys('12:00 PM')
        passcode = self.driver.find_element_by_name('passcode')
        passcode.send_keys('passcode')

        # submit
        self.driver.find_element_by_id('create_event').click()

        '''
        VERIFY APPLICATION WAS CREATED
        AND ALL INFORMATION IS CORRECT
        '''
        self.assertTrue('test application' in self.driver.page_source)
        self.assertTrue('DASHBOARD' in self.driver.page_source)
        self.driver.find_element_by_link_text('EDIT APPLICATION').click()
        self.assertTrue('Edit Application' in self.driver.page_source)

        # STEP 1: find everything again
        name = self.driver.find_element_by_name('event_name')
        location = self.driver.find_element_by_name('event_location')
        start_date = self.driver.find_element_by_name('event_start_date')
        start_time = self.driver.find_element_by_name('event_start_time')
        end_date = self.driver.find_element_by_name('event_end_date')
        end_time = self.driver.find_element_by_name('event_end_time')

        # now test values are what I made them
        self.assertEquals(name.get_attribute('value'), 'test application')
        self.assertEquals(location.get_attribute('value'), 'Art Gallery Road, Sydney New South Wales, Australia')
        self.assertEquals(start_date.get_attribute('value'), '01/01/2018')
        self.assertEquals(start_time.get_attribute('value'), '12:00 PM')
        self.assertEquals(end_date.get_attribute('value'), '12/31/2018')
        self.assertEquals(end_time.get_attribute('value'), '12:00 PM')

        self.driver.find_element_by_xpath("//input[@value='Edit Questions']").click()

        # STEP 2
        mc_title = self.driver.find_element_by_id('mc0')
        mc_help = self.driver.find_element_by_id('mc0-help')
        mc_choice_1 = self.driver.find_element_by_id('mc0_c0')
        mc_choice_2 = self.driver.find_element_by_id('mc0_c1')
        mc_choice_3 = self.driver.find_element_by_id('mc0_c2')
        dd_title = self.driver.find_element_by_id('dd1')
        dd_help = self.driver.find_element_by_id('dd1-help')
        dd_choice_1 = self.driver.find_element_by_id('dd1_c0')
        dd_choice_2 = self.driver.find_element_by_id('dd1_c1')
        dd_choice_3 = self.driver.find_element_by_id('dd1_c2')
        dd_choice_4 = self.driver.find_element_by_id('dd1_c3')
        pp_title = self.driver.find_element_by_id('pp2')
        pp_help = self.driver.find_element_by_id('pp2-help')
        char_lim = self.driver.find_element_by_id('pp2-cl')
        dt_title = self.driver.find_element_by_id('dt3')
        dt_help = self.driver.find_element_by_id('dt3-help')
        fu_title = self.driver.find_element_by_id('fu4')
        fu_help = self.driver.find_element_by_id('fu4-help')

        self.assertEquals(mc_title.get_attribute('value'), 'multiple choice')
        self.assertEquals(mc_help.get_attribute('value'), 'multiple choice description')
        self.assertEquals(mc_choice_1.get_attribute('value'), 'choice 1')
        self.assertEquals(mc_choice_2.get_attribute('value'), 'choice 2')
        self.assertEquals(mc_choice_3.get_attribute('value'), 'choice 3')
        self.assertEquals(dd_title.get_attribute('value'), 'dropdown')
        self.assertEquals(dd_help.get_attribute('value'), 'dropdown description')
        self.assertEquals(dd_choice_1.get_attribute('value'), 'choice 1')
        self.assertEquals(dd_choice_2.get_attribute('value'), 'choice 2')
        self.assertEquals(dd_choice_3.get_attribute('value'), 'choice 3')
        self.assertEquals(dd_choice_4.get_attribute('value'), 'choice 4')
        self.assertEquals(pp_title.get_attribute('value'), 'text response')
        self.assertEquals(pp_help.get_attribute('value'), 'text response description')
        self.assertEquals(char_lim.get_attribute('value'), '10')
        self.assertEquals(dt_title.get_attribute('value'), 'date choice')
        self.assertEquals(dt_help.get_attribute('value'), 'date choice description')
        self.assertEquals(fu_title.get_attribute('value'), 'file upload')
        self.assertEquals(fu_help.get_attribute('value'), 'file upload description')

        # submit and go through modal
        self.driver.find_element_by_id('next-btn').click()
        wait.until(EC.visibility_of_element_located((By.ID, 'submit-questions')))
        self.driver.find_element_by_class_name('js--submit-form').click()
        wait.until(EC.title_contains(('Rubric')))

        # STEP 3
        r_mc_title = self.driver.find_element_by_id('mc0')
        r_mc_help = self.driver.find_element_by_id('mc0-help')
        r_mc_choice_1 = self.driver.find_element_by_id('mc0_c0')
        r_mc_choice_2 = self.driver.find_element_by_id('mc0_c1')
        r_mc_choice_3 = self.driver.find_element_by_id('mc0_c2')
        r_pp_title = self.driver.find_element_by_id('pp1')
        r_pp_help = self.driver.find_element_by_id('pp1-help')
        r_char_lim = self.driver.find_element_by_id('pp1-cl')
        sc_title = self.driver.find_element_by_id('sc2')
        sc_help = self.driver.find_element_by_id('sc2-help')
        maximum = self.driver.find_element_by_id('sc2-cp')
        fr_title = self.driver.find_element_by_id('fr3')
        fr_help = self.driver.find_element_by_id('fr3-help')
        sl_title = self.driver.find_element_by_id('sl4')
        sl_help = self.driver.find_element_by_id('sl4-help')
        out_of = self.driver.find_element_by_id('sl4-cp')
        rt_title = self.driver.find_element_by_id('rt5')
        rt_help = self.driver.find_element_by_id('rt5-help')

        self.assertEquals(r_mc_title.get_attribute('value'), 'multiple choice')
        self.assertEquals(r_mc_help.get_attribute('value'), 'multiple choice description')
        self.assertEquals(r_mc_choice_1.get_attribute('value'), 'choice 1')
        self.assertEquals(r_mc_choice_2.get_attribute('value'), 'choice 2')
        self.assertEquals(r_mc_choice_3.get_attribute('value'), 'choice 3')
        self.assertEquals(r_pp_title.get_attribute('value'), 'text response')
        self.assertEquals(r_pp_help.get_attribute('value'), 'text response description')
        self.assertEquals(r_char_lim.get_attribute('value'), '10')
        self.assertEquals(sc_title.get_attribute('value'), 'linear scale')
        self.assertEquals(sc_help.get_attribute('value'), 'linear scale description')
        self.assertEquals(maximum.get_attribute('value'), '6')
        self.assertEquals(fr_title.get_attribute('value'), 'forced ranking')
        self.assertEquals(fr_help.get_attribute('value'), 'forced ranking description')
        self.assertEquals(sl_title.get_attribute('value'), 'slider')
        self.assertEquals(sl_help.get_attribute('value'), 'slider description')
        self.assertEquals(out_of.get_attribute('value'), '9')
        self.assertEquals(rt_title.get_attribute('value'), 'star rating')
        self.assertEquals(rt_help.get_attribute('value'), 'star rating description')

        self.driver.find_element_by_id('next-btn').click()
        wait.until(EC.title_contains(('Judges')))

        # STEP 4
        judge_end_date = self.driver.find_element_by_name('judge_end_date')
        judge_end_time = self.driver.find_element_by_name('judge_end_time')
        passcode = self.driver.find_element_by_name('passcode')

        self.assertEquals(judge_end_date.get_attribute('value'), '01/31/19')
        self.assertEquals(judge_end_time.get_attribute('value'), '12:00 PM')
        self.assertEquals(passcode.get_attribute('value'), 'passcode')

        self.driver.find_element_by_id('create_event').click()

        self.assertTrue('test application' in self.driver.page_source)
        self.assertTrue('DASHBOARD' in self.driver.page_source)


    def test_valid_application(self):
        '''Test submitting an application with all valid responses'''
        self.test_create_application()
        self.driver.get(self.domain + reverse('logout'))
        self.cornell_user_login()
        self.driver.get(self.domain)

        # find and view application
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.LINK_TEXT, 'test application')))
        self.driver.find_element_by_link_text('test application').click()
        self.assertIn('test application', self.driver.title)
        self.driver.find_element_by_link_text('Apply').click()
        self.assertIn('Apply', self.driver.title)

        # enter valid answers
        self.driver.find_element_by_class_name('question__mc').click()
        self.driver.find_element_by_class_name('question__dd').click()
        self.driver.find_element_by_xpath("//select/option[3]").click()
        text_answer = self.driver.find_element_by_class_name('question__pp')
        text_answer.send_keys('hi there')
        date_answer = self.driver.find_element_by_class_name('question__dt')
        date_answer.send_keys('03.15.2018')
        # can't test file uploading

        self.driver.find_element_by_xpath("//input[@value='Send']").click()

        # verify that application submitted properly and values are correct
        self.assertFalse('You have errors in your application' in self.driver.page_source)
        wait.until(EC.title_contains(('Event')))
        self.driver.find_element_by_link_text('Update Application').click()

        dropdown_answer = self.driver.find_element_by_class_name('question__dd')
        text_answer = self.driver.find_element_by_class_name('question__pp')
        date_answer = self.driver.find_element_by_class_name('question__dt')

        self.assertEquals(dropdown_answer.get_attribute('value'), '6')
        self.assertEquals(text_answer.get_attribute('value'), 'hi there')
        self.assertEquals(date_answer.get_attribute('value'), '03.15.2018')

        self.driver.find_element_by_xpath("//input[@value='Send']").click()
        wait.until(EC.title_contains(('Event')))

        # check for data in the backend
        member = StartupMember.objects.get_user(user=self.regular.user)
        event = Event.objects.get(name='test application')
        uni = self.cornell
        application = custom_member_dict(member, uni.id, 3, event.event_id)
        for q in application:
            if q['q_text'] == 'multiple choice':
                for c in q['q_choices']:
                    if c['text'] == 'choice 1':
                        self.assertTrue(c['chosen'])
                    else:
                        self.assertFalse(c['chosen'])
            if q['q_text'] == 'dropdown':
                for c in q['q_choices']:
                    if c['text'] == 'choice 3':
                        self.assertTrue(c['chosen'])
                    else:
                        self.assertFalse(c['chosen'])
            if q['q_text'] == 'text response':
                self.assertEqual(q['a_text'], 'hi there')
            if q['q_text'] == 'date choice':
                self.assertEqual(q['a_date'], '03.15.2018')


    def test_invalid_application(self):
        '''Test submitting applications with invalid responses'''
        self.test_create_application()
        self.driver.get(self.domain + reverse('logout'))
        self.cornell_user_login()
        self.driver.get(self.domain)

        # find and view application
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.LINK_TEXT, 'test application')))
        self.driver.find_element_by_link_text('test application').click()
        self.assertIn('test application', self.driver.title)
        self.driver.find_element_by_link_text('Apply').click()
        self.assertIn('Apply', self.driver.title)

        # submit blank application
        self.driver.find_element_by_xpath("//input[@value='Send']").click()
        self.assertIn('Apply', self.driver.title)
        self.assertTrue('You have errors in your application' in self.driver.page_source)

        # check that nothing was submitted in backend
        event = Event.objects.get(name='test application')
        q_ids = event.form.questions.all().values_list('id', flat=True)
        num_answers = Answer.objects.filter(q_id__in=q_ids)
        num_applicants = event.attending_users

        self.assertEqual(num_answers.count(), 0)
        self.assertEqual(num_applicants.count(), 0)

        '''
        submit applications without answering all required questions
        '''
        # missing multiple choice
        text_answer = self.driver.find_element_by_class_name('question__pp')
        text_answer.send_keys('hi there')
        date_answer = self.driver.find_element_by_class_name('question__dt')
        date_answer.send_keys('03.15.2018')
        self.driver.find_element_by_xpath("//input[@value='Send']").click()
        self.assertIn('Apply', self.driver.title)
        self.assertTrue('You have errors in your application' in self.driver.page_source)

        # user's answers should save, but user should not be counted as an applicant
        self.assertEqual(num_answers.count(), 5)
        self.assertEqual(num_applicants.count(), 0)

        # missing text response
        self.driver.find_element_by_class_name('question__mc').click()
        text_answer = self.driver.find_element_by_class_name('question__pp')
        text_answer.clear()
        self.driver.find_element_by_xpath("//input[@value='Send']").click()
        self.assertIn('Apply', self.driver.title)
        self.assertTrue('You have errors in your application' in self.driver.page_source)

        self.assertEqual(num_answers.count(), 5)
        self.assertEqual(num_applicants.count(), 0)

        # missing date choice
        text_answer = self.driver.find_element_by_class_name('question__pp')
        text_answer.send_keys('hi there')
        date_answer = self.driver.find_element_by_class_name('question__dt')
        date_answer.clear()
        self.driver.find_element_by_xpath("//input[@value='Send']").click()
        self.assertIn('Apply', self.driver.title)
        self.assertTrue('You have errors in your application' in self.driver.page_source)

        self.assertEqual(num_answers.count(), 5)
        self.assertEqual(num_applicants.count(), 0)

        # submit text answer exceeding character limit
        # should submit properly since char limits aren't enforced anymore
        text_answer = self.driver.find_element_by_class_name('question__pp')
        text_answer.clear()
        text_answer.send_keys('the quick brown fox jumps over the lazy dog')
        self.assertTrue('too many' in self.driver.page_source)
        date_answer = self.driver.find_element_by_class_name('question__dt')
        date_answer.send_keys('03.15.2018')
        self.driver.find_element_by_xpath("//input[@value='Send']").click()
        self.assertFalse('You have errors in your application' in self.driver.page_source)

        self.assertEqual(num_answers.count(), 5)
        self.assertEqual(num_applicants.count(), 1)

        wait.until(EC.title_contains(('Event')))
        self.driver.find_element_by_link_text('Update Application')
