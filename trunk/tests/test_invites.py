from django.urls import reverse

from branch.models import StartupMember
from emailuser.models import UserEmail
from emailuser.factory import UserEmailFactory
from forms.models import AnonymousApplicant

from trunk.models import Event

from trunk.tests.test_judges import (
    TestBase,
    START_DAY_DELTA,
    END_DAY_DELTA,
    NOON,
    END_OF_DAY,
    DateUtils,
)

from typing import Iterable, TypedDict


NUM_USERS = 5
NUM_GUESTS = 5


class InviteeContact(TypedDict):
    first_name: str
    last_name: str
    email: str


class TestInvites(TestBase):
    def setUp(self):
        super().setUp()

    def make_event(self, event_type: str):
        """
        event_type: survey, application, competition
        """
        tz = self.cornell.style.timezone

        start = DateUtils.dt_from_delta(day_delta=START_DAY_DELTA, time=NOON, timezone=tz)
        end = DateUtils.dt_from_delta(day_delta=END_DAY_DELTA, time=END_OF_DAY, timezone=tz)

        if event_type == 'survey':
            event_type_args = {'is_survey': True}
        elif event_type in ['competition', 'application']:
            event_type_args = {
                'is_competition': True,
                'is_application': event_type == 'application'
            }
        else:
            raise ValueError(f'invalid event_type: {event_type}')

        return Event.objects.create_event(
            name='Test Event',
            description='Test Event Description',
            start=start,
            end=end,
            timezone=tz,
            university=self.cornell,
            is_published=True,
            ** event_type_args
        )

    @staticmethod
    def make_member(i: int) -> StartupMember:
        contact = TestInvites.make_member_contact(i)
        email = contact['email']
        UserEmail.objects.filter(email=email).delete()
        user = UserEmailFactory(email=email)
        StartupMember.objects.filter(user=user).delete()
        return StartupMember.objects.create_sm(
            first_name=contact['first_name'],
            last_name=contact['last_name'],
            user_email=user
        )

    @staticmethod
    def make_member_contact(i: int) -> InviteeContact:
        return {
            'email': TestInvites.make_member_email(i),
            'first_name': f'FMember{i}',
            'last_name': f'LMember{i}',
        }

    @staticmethod
    def make_guest_contact(i: int) -> InviteeContact:
        return {
            'email': TestInvites.make_guest_email(i),
            'first_name': f'FGuest{i}',
            'last_name': f'LGuest{i}',
        }

    @staticmethod
    def make_guest_email(i: int) -> str:
        return f'guest{i}@hello.com'

    @staticmethod
    def make_member_email(i: int) -> str:
        return f'member{i}@hello.com'

    @staticmethod
    def make_invite_form_data(contacts: Iterable[InviteeContact], num_contacts: int) -> dict:
        data = {
            'form-TOTAL_FORMS': str(num_contacts),
            'form-INITIAL_FORMS': '0',
        }

        def make_formset_name(name: str, index: int) -> str:
            return f'form-{index}-{name}'

        for i, contact in enumerate(contacts):
            for k, v in contact.items():
                data[make_formset_name(k, i)] = v

        return data

    def assertUserInvited(self, event: Event, user_contact: InviteeContact):
        email = user_contact['email']
        user = UserEmail.objects.get(email=email)
        self.assertTrue(user in event.invited_users.all())

    @staticmethod
    def get_guest_kwargs(event: Event, guest_contact: InviteeContact):
        return {
            'event': event,
            'first_name': guest_contact['first_name'],
            'last_name': guest_contact['last_name'],
            'email': guest_contact['email'],
            'is_invited': True
        }

    @staticmethod
    def get_guest(event: Event, guest_contact: InviteeContact):
        return AnonymousApplicant.objects.get(**TestInvites.get_guest_kwargs(event, guest_contact))

    def assertGuestInvited(self, event: Event, guest_contact: InviteeContact):
        anon_applicants = AnonymousApplicant.objects.filter(**self.__class__.get_guest_kwargs(event, guest_contact))
        self.assertEqual(anon_applicants.count(), 1)

    def assertOnManageParticipantsPage(self,
                                       event: Event,
                                       user_contacts: Iterable[InviteeContact],
                                       guest_contacts: Iterable[InviteeContact]):
        url_name = 'uni-manage-participants' if not event.is_application else 'uni-manage-applicants'
        path = reverse(url_name, kwargs={'event_id': event.event_id})

        self.login_user(self.client, self.admin.user.email, self.password)
        response = self.client.get(path, {'view': 'progress'})

        for user_contact in user_contacts:
            user_sm = StartupMember.objects.get(user__email=user_contact['email'])
            self.assertUserInContext(user_sm, response)

        for guest_contact in guest_contacts:
            anon_applicant = self.__class__.get_guest(event, guest_contact)
            self.assertGuestInContext(anon_applicant, response)

    def assertUserInContext(self, user: StartupMember, response):
        """Tests that user appears in the context returned by branch.views.competitions.view_applicants() """
        ratings = response.context['ratings']

        def is_user(rating):
            return \
                rating['url_name'] == user.url_name and \
                rating['applicant'] == user and \
                rating['applicant_id'] == user.id

        user_ratings = list(filter(is_user, ratings))

        self.assertEqual(len(user_ratings), 1)

    def assertGuestInContext(self, anon_applicant: AnonymousApplicant, response):
        ratings = response.context['ratings']

        def is_guest(rating):
            return \
                rating['applicant'] == anon_applicant.name and \
                str(rating['applicant_id']) == str(anon_applicant.applicant_id) and \
                rating['url_name'] == str(anon_applicant.applicant_id) and \
                rating['email'] == anon_applicant.email and \
                rating['is_anonymous']

        guest_ratings = list(filter(is_guest, ratings))

        self.assertEqual(len(guest_ratings), 1)

    def general_invite_more_test(self, is_application: bool, num_users: int = NUM_USERS, num_guests: int = NUM_GUESTS):
        for i in range(num_users):
            TestInvites.make_member(i)

        user_contacts = list(map(TestInvites.make_member_contact, range(num_users)))
        guest_contacts = list(map(TestInvites.make_guest_contact, range(num_guests)))

        all_contacts = user_contacts + guest_contacts
        num_contacts = num_users + num_guests

        event_type = 'competition' if not is_application else 'application'
        event = self.make_event(event_type)

        path = reverse('events-invite-users', kwargs={'event_id': event.event_id})

        data = self.make_invite_form_data(all_contacts, num_contacts)

        response = self.post(self.admin.user.email, self.password, path, data)

        self.assertEqual(response.status_code, 200)

        for user_contact in user_contacts:
            self.assertUserInvited(event, user_contact)

        for guest_contact in guest_contacts:
            self.assertGuestInvited(event, guest_contact)

        self.assertOnManageParticipantsPage(event, user_contacts, guest_contacts)

    def test_competition_invite_more(self):
        self.general_invite_more_test(is_application=False)

    def test_application_invite_more(self):
        self.general_invite_more_test(is_application=True)

    def test_survey_invite_more(self):
        pass
