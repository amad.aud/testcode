from auto_test_utility.utills import AbastractUniTestBase
from django.urls.base import reverse


class CreateEventTest(AbastractUniTestBase):
    def test_create_event(self):
        self.cornell_admin_login()
        self.driver.get(self.domain + reverse('uni-create-event'))
        self.assertEqual(self.domain + reverse('uni-create-event'), self.driver.current_url)
        # event name
        name = self.driver.find_element_by_name('event_name')
        name.send_keys('amber')

        # location
        location = self.driver.find_element_by_name('event_location')
        location.send_keys('Art Gallery Road, Sydney, New South Wales, Australia')

        # date + times
        start_date = self.driver.find_element_by_name('event_start_date')
        start_date.send_keys('7/28/2017')
        start_time = self.driver.find_element_by_name('event_start_time')
        start_time.send_keys('04:00 PM')
        end_date = self.driver.find_element_by_name('event_end_date')
        end_date.send_keys('7/29/2017')
        end_time = self.driver.find_element_by_name('event_end_time')
        end_time.send_keys('04:00 PM')

        # website links
        event_website = self.driver.find_element_by_name('event_website')
        event_website.send_keys('google.com')
        ticket_website = self.driver.find_element_by_name('ticket_website')
        ticket_website.send_keys('google.com')

        # industry checkboxes
        advertising = self.driver.find_element_by_id('Advertising').click()
        aerospace = self.driver.find_element_by_id('Aerospace').click()
        apparel = self.driver.find_element_by_id('Apparel').click()

        # event type
        conference = self.driver.find_element_by_id('conference').click()

        # food + swag
        food = self.driver.find_element_by_id('food_checkbox').click()
        swag = self.driver.find_element_by_id('swag_checkbox').click()

        # submit form
        self.driver.save_screenshot('creat-event.png')
        self.driver.find_element_by_id('create_event').click()
        self.driver.implicitly_wait(5)

        # click on edit button
        self.driver.find_element_by_id('AMBER').click()
        self.driver.implicitly_wait(5)
        self.driver.save_screenshot('edit-event.png')

        # find everything again
        name = self.driver.find_element_by_name('event_name')
        location = self.driver.find_element_by_name('event_location')
        start_date = self.driver.find_element_by_name('event_start_date')
        start_time = self.driver.find_element_by_name('event_start_time')
        end_date = self.driver.find_element_by_name('event_end_date')
        end_time = self.driver.find_element_by_name('event_end_time')
        event_website = self.driver.find_element_by_name('event_website')
        ticket_website = self.driver.find_element_by_name('ticket_website')
        advertising = self.driver.find_element_by_id('Advertising')
        aerospace = self.driver.find_element_by_id('Aerospace')
        apparel = self.driver.find_element_by_id('Apparel')
        conference = self.driver.find_element_by_id('conference')
        food = self.driver.find_element_by_id('food_checkbox')
        swag = self.driver.find_element_by_id('swag_checkbox')

        # now test values are what I made them
        self.assertEquals(name.get_attribute('value'), 'amber')
        self.assertEquals(location.get_attribute('value'), 'Art Gallery Road, Sydney, New South Wales, Australia')
        self.assertEquals(start_date.get_attribute('value'), '7/28/2017')
        self.assertEquals(start_time.get_attribute('value'), '04:00 PM')
        self.assertEquals(end_date.get_attribute('value'), '7/29/2017')
        self.assertEquals(end_time.get_attribute('value'), '04:00 PM')
        self.assertEquals(event_website.get_attribute('value'), 'https://google.com')
        self.assertEquals(ticket_website.get_attribute('value'), 'https://google.com')
        self.assertEquals(advertising.get_attribute('checked'), 'checked')
        self.assertEquals(aerospace.get_attribute('checked'), 'checked')
        self.assertEquals(apparel.get_attribute('checked'), 'checked')
        self.assertEquals(conference.get_attribute('checked'), 'checked')
        self.assertEquals(food.get_attribute('checked'), 'checked')
        self.assertEquals(swag.get_attribute('checked'), 'checked')
