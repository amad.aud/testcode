from auto_test_utility.utills import AbastractUniTestBase
from django.test import Client
from django.urls import reverse
from forms.factory import (
    QuestionFactory,
    RubricFactory,
    MemberApplicantFactory,
    AnonymousApplicantFactory
)
from forms.models import MemberApplicant, AnonymousApplicant
from trunk.factory.event import CompetitionFactory, JudgeBoardFactory
from trunk.models import Event
from django.utils import timezone
from datetime import timedelta
import pytz
from StartupTree.utils import get_date_time_object


class CompetitionRoundsTest(AbastractUniTestBase):
    def setUp(self):
        super(CompetitionRoundsTest, self).setUp()
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)

        self.root = CompetitionFactory.create()
        for x in range(5):
            new_question = QuestionFactory.create()
            new_rubric = RubricFactory.create(round=1)
            self.root.form.questions.add(new_question)
            self.root.form.rubrics.add(new_rubric)
            MemberApplicantFactory.create(event=self.root)
            AnonymousApplicantFactory.create(event=self.root)
        root_judge_board = JudgeBoardFactory.create(
            competition=self.root, round=1, deadline=timezone.now() + timedelta(days=1)
        )
        self.root.judge_boards.add(root_judge_board)

    def test_create_new_comp_round(self):
        self.login_user(self.client, self.admin.user.email, self.password)

        invited = []
        invited_member = MemberApplicant.objects.filter(event=self.root).first()
        invited_guest = AnonymousApplicant.objects.filter(event=self.root).first()
        invited.append(invited_member.member.url_name)
        invited.append(invited_guest.applicant_id)

        data = {
            'check[]': invited
        }

        post_url = reverse('competition-add-round', kwargs={'event_id': self.root.event_id})
        self.client.post(post_url, data)
        self.root.refresh_from_db()

        new_round_name = self.root.name + ' Round 2'
        self.assertTrue(Event.objects.filter(name=new_round_name).exists())
        self.assertTrue(self.root.event_board.rounds.filter(number=2).exists())

        new_round = Event.objects.get(name=new_round_name)
        self.assertEqual(1, new_round.invited_users.all().count())
        self.assertEqual(1, AnonymousApplicant.objects.filter(event=new_round).count())

    def test_create_new_judge_round(self):
        self.login_user(self.client, self.admin.user.email, self.password)

        post_url = reverse('competition-add-judge-round', kwargs={'event_id': self.root.event_id})
        self.client.post(post_url)
        self.assertTrue(self.root.judge_boards.filter(round=2).exists())
        self.assertTrue(self.root.form.rubrics.filter(round=2).exists())

        for rubric in self.root.form.rubrics.filter(round=1):
            self.assertTrue(self.root.form.rubrics.filter(round=2, text=rubric.text).exists())

    def test_edit_judge_round(self, num_rounds=2, round_to_edit=1):
        self.login_user(self.client, self.admin.user.email, self.password)
        edit_url = reverse('uni-edit-competition-judges', kwargs={'event_id': self.root.event_id})

        while self.root.judge_boards.count() < num_rounds:
            curr_round_num = self.root.judge_boards.count()
            next_round_num = curr_round_num + 1
            new_judge_board = JudgeBoardFactory.create(
                competition=self.root, round=next_round_num, deadline=timezone.now() + timedelta(days=1)
            )
            self.root.judge_boards.add(new_judge_board)

        get_data = {
            'round': round_to_edit
        }
        self.client.get(edit_url, get_data)

        target_round = self.root.judge_boards.get(round=round_to_edit)
        new_passcode = 'newpass'

        post_data = {
            'round': round_to_edit,
            'judge_end_date': '12/31/2031',
            'judge_end_time': '12:00 PM',
            'passcode': new_passcode,
            'form-TOTAL_FORMS': '1',
            'form-INITIAL_FORMS': '0'
        }
        self.client.post(edit_url, post_data)

        new_deadline = timezone.make_aware(
            get_date_time_object('12/31/2031', '12:00 PM'),
            pytz.timezone(self.root.timezone))
        for round in self.root.judge_boards.all():
            if round == target_round:
                self.assertEqual(new_deadline, round.deadline)
                self.assertEqual(new_passcode, round.passcode)
            else:
                self.assertNotEqual(new_deadline, round.deadline)
                self.assertNotEqual(new_passcode, round.passcode)
