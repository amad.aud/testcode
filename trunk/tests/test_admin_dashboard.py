import datetime
from dateutil.relativedelta import relativedelta
from django.test import TestCase, Client
from django.test.client import RequestFactory
from auto_test_utility.utills import AbastractUniTestBase

from trunk.factory.event import EventFactory, EventRSVPFactory
from forms.factory import MemberApplicantFactory, AnonymousApplicantFactory
from venture.factory import CandidateFactory
from emailuser.factory import UserEmailFactory
from mentorship.factory.mentorship import MentorFactory, MentorshipFactory, MentorshipSessionFactory
from branch.factory import StartupMemberFactory
from activity.factory.activity import ActivityUserFactory

from activity.aggregate.users import (
    get_list_of_users_activity,
    get_list_of_active_users_activity
)
from trunk.aggregate.events import (
    get_list_of_rsvp_users,
    get_list_of_application_users
)
from mentorship.aggregate.mentorship import (
    get_list_of_mentorship_users
)
from venture.aggregate.candidate import (
    get_list_of_candidate
)

from venture.models import (Candidate)


class AdminDashboardTest(AbastractUniTestBase):
    def setUp(self):
        super(AdminDashboardTest, self).setUp()   # Set Up Using Super Class AbastractUniTestBase

        # Set up competition where users will apply to
        self.competition = self.setup_application_competition(self.cornell,create_candidate=False) # Note one member applicant is made here
        self.rsvp_event = EventFactory(university=self.cornell) # Set up rsvp event here

        # Create 23 Member Applicant Users, RSVPs to random event, Mentorship objects with one session each
        # (46 total mentor users) --> 23 mentee's + 23 mentors
        self.generated_users = 23
        for _ in range(self.generated_users):
            MemberApplicantFactory(event=self.competition, is_applied=True)
            EventRSVPFactory(rsvp_event=self.rsvp_event)
            CandidateFactory(university_map = self.cornell)

            ActivityUserFactory(code_map=0) #Joined normally
            ActivityUserFactory(code_map=13) #Joined as mentor
            ActivityUserFactory(code_map=15) #Any other random code

            temp_mentor = MentorFactory(platform=self.cornell)
            temp_student = StartupMemberFactory()
            temp_mentorship = MentorshipFactory(student=temp_student,mentor=temp_mentor)
            MentorshipSessionFactory(mentorship=temp_mentorship)

    def test_member_applicant_query(self):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
        application_users = get_list_of_application_users(self.cornell.id,start_date_filter,end_date_filter)
        self.assertEqual(self.generated_users, len(application_users))    # 1 included from competition factory

    def test_rsvp_counts_query(self):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
        rsvp_users = get_list_of_rsvp_users(self.cornell.id,start_date_filter,end_date_filter)
        self.assertEqual(self.generated_users, len(rsvp_users))

    def test_job_application_query(self):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
        jobs_users = get_list_of_candidate(self.cornell.id,start_date_filter,end_date_filter)
        self.assertEqual(self.generated_users, len(jobs_users))

    def test_mentor_mentee_query(self):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
        mentorship_users = get_list_of_mentorship_users(self.cornell.id,start_date_filter,end_date_filter)
        self.assertEqual(self.generated_users+self.generated_users, len(mentorship_users))

    def test_activity_list_query(self):
        start_date_filter=(datetime.datetime.today()-relativedelta(months=6))
        end_date_filter=datetime.datetime.today()
        general_users = get_list_of_users_activity(self.cornell.id,start_date_filter,end_date_filter)
        active_users = get_list_of_active_users_activity(self.cornell.id,start_date_filter,end_date_filter)
        self.assertEqual(self.generated_users*3, len(general_users))
        self.assertEqual(self.generated_users, len(active_users))
