from auto_test_utility.utills import AbastractUniTestBase
from django.test import Client
from trunk.factory import UniversityEmailDomainFactory
from trunk.factory.event import EventFactory
from trunk.models import UniversityEmailDomain
from forms.factory import FormFactory, QuestionFactory
from forms.models import Form, Question
from branch.modelforms import GuestRSVPForm
from django.utils import timezone
from datetime import timedelta


class EventRSVPTest(AbastractUniTestBase):
    def setUp(self):
        super(EventRSVPTest, self).setUp()
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)

        event_form = FormFactory.create(
            form=Form.EVENT
        )
        question = QuestionFactory.create(
            q_type=Question.PP,
            is_required=True
        )
        event_form.questions.add(question)
        self.event_form = event_form

    def test_member_rsvp_and_un_rsvp(self):
        current_event = EventFactory.create(
            end=timezone.now() + timedelta(hours=1)
        )

        self.login_user(self.client, self.regular.user.email, self.password)

        rsvp_data = {
            'event_id': current_event.event_id,
            'user_decision': 'yes'
        }
        self.client.post("/event/rsvp", rsvp_data)
        self.assertEqual(1, current_event.get_attending_users_count())

        un_rsvp_data = {
            'event_id': current_event.event_id,
            'user_decision': 'no'
        }
        self.client.post("/event/rsvp", un_rsvp_data)
        self.assertEqual(0, current_event.get_attending_users_count())

        self.logout_user(self.client)

    def test_guest_rsvp(self):
        current_event = EventFactory.create(
            end=timezone.now() + timedelta(hours=1),
            allow_guest_rsvp=True
        )

        data = {
            'user_decision': 'yes',
            'fname': 'fname',
            'lname': 'lname',
            'email': 'test@email.com'
        }
        self.client.post("/event/guest_rsvp/{}".format(current_event.event_id), data)
        self.assertEqual(1, current_event.get_attending_users_count())

    def test_member_rsvp_with_custom_questions(self):
        custom_question_event = EventFactory.create(
            end=timezone.now() + timedelta(hours=1),
            form=self.event_form
        )

        self.login_user(self.client, self.regular.user.email, self.password)

        # first try RSVPing without answering the required question. RSVP
        # should not go through.
        no_custom_question_data = {
            'event_id': custom_question_event.event_id,
            'user_decision': 'yes',
            'count': 1,
            'type0': 'Paragraph',
            'id0': custom_question_event.form.questions.first().id,
            'pp0': ''
        }
        self.client.post("/event/rsvp", no_custom_question_data)
        self.assertEqual(0, custom_question_event.get_attending_users_count())

        # now answer the question correctly and try again. RSVP should now
        # go through.
        custom_question_data = {
            'event_id': custom_question_event.event_id,
            'user_decision': 'yes',
            'count': 1,
            'type0': 'Paragraph',
            'id0': custom_question_event.form.questions.first().id,
            'pp0': 'sample text'
        }
        self.client.post("/event/rsvp", custom_question_data)
        self.assertEqual(1, custom_question_event.get_attending_users_count())

        self.logout_user(self.client)

    def test_guest_rsvp_with_custom_questions(self):
        custom_question_event = EventFactory.create(
            end=timezone.now() + timedelta(hours=1),
            form=self.event_form,
            allow_guest_rsvp=True
        )

        data = {
            'user_decision': 'yes',
            'fname': 'fname',
            'lname': 'lname',
            'email': 'test@email.com',
            'count': 1,
            'type0': 'Paragraph',
            'id0': custom_question_event.form.questions.first().id,
            'pp0': 'sample text'
        }
        self.client.post("/event/guest_rsvp/{}".format(custom_question_event.event_id), data)
        self.assertEqual(1, custom_question_event.get_attending_users_count())

    def test_rsvp_to_past_event(self):
        past_event = EventFactory.create(
            start=timezone.now() - timedelta(hours=2),
            end=timezone.now() - timedelta(hours=1)
        )

        self.login_user(self.client, self.regular.user.email, self.password)

        data = {
            'event_id': past_event.event_id,
            'user_decision': 'yes'
        }
        self.client.post("/event/rsvp", data)
        self.assertEqual(0, past_event.get_attending_users_count())

        self.logout_user(self.client)

    def test_guest_rsvp_to_member_only_event(self):
        member_only_event = EventFactory.create(
            end=timezone.now() + timedelta(hours=1),
            allow_guest_rsvp=False
        )

        data = {
            'user_decision': 'yes',
            'fname': 'fname',
            'lname': 'lname',
            'email': 'test@email.com'
        }
        self.client.post("/event/guest_rsvp/{}".format(member_only_event.event_id), data)
        self.assertEqual(0, member_only_event.get_attending_users_count())

    def test_guest_rsvp_with_restricted_domain(self):
        UniversityEmailDomainFactory.create(
            type=UniversityEmailDomain.GUEST
        )
        restricted_domain_event = EventFactory.create(
            end=timezone.now() + timedelta(hours=1),
            allow_guest_rsvp=True,
            restrict_guest_rsvp=True
        )

        wrong_domain_data = {
            'user_decision': 'yes',
            'fname': 'fname',
            'lname': 'lname',
            'email': 'test@email.com'
        }
        self.client.post("/event/guest_rsvp/{}".format(restricted_domain_event.event_id), wrong_domain_data)
        self.assertEqual(0, restricted_domain_event.get_attending_users_count())

        valid_domain_data = {
            'user_decision': 'yes',
            'fname': 'fname',
            'lname': 'lname',
            'email': 'test@example.com'
        }
        self.client.post("/event/guest_rsvp/{}".format(restricted_domain_event.event_id), valid_domain_data)
        self.assertEqual(1, restricted_domain_event.get_attending_users_count())
