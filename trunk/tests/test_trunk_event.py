from django.test import TestCase, Client
from django.test.client import RequestFactory
from trunk.models import (
    University,
    UniversityStyle,
    School,
    Site,
    Major,
    get_uni_logo_url,
    Event,
    Judge,
    JudgeBoard,
    JudgeReview
)
from trunk.factory.event import EventFactory, JudgeBoardFactory, JudgeFactory
from branch.factory import StartupMemberFactory
from branch.tasks import check_upcoming
from forms.factory import MemberApplicantFactory, AnonymousApplicantFactory
from forms.models import MemberApplicant, AnonymousApplicant
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Count
from branch.tasks import AssignJudge
from emailuser.conf import UNIVERSITY
from trunk.modelforms import (
    UniversityForm,
)
from activity.models import Activity
from StartupTree.settings import ROOT_DOMAIN
from django.urls import reverse
from django.utils import timezone
from datetime import timedelta, datetime
from auto_test_utility.utills import AbastractUniTestBase
from django.core import mail


class EventEmailRemindTest(AbastractUniTestBase):
    # python3 manage.py test trunk.tests.test_trunk_event.EventEmailRemindTest
    def test_event_reminder_3_days(self):
        number_of_participants = 3

        name = "Timmy Event"
        description = "Timmy Event description"
        start_date = timezone.now() + timedelta(days=3, hours=1) # we want it to be slightly more than 3 days away.
        end_date = start_date + timedelta(hours=2)
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone,
            send_emails_days_prior=3,
        )

        for i in range(number_of_participants):
            member_i = StartupMemberFactory.create()
            new_event.attending_users.add(member_i.user)

        check_upcoming.apply((new_event.id, ))

        notification_mails = mail.outbox
        # we are using bcc. thus only one email, but to multiple recipient.
        self.assertEquals(len(notification_mails), 1)
        notification_mail = notification_mails[0]
        self.assertEquals(notification_mail.subject, "An event you RSVPed for is coming up")
        self.assertEquals(len(notification_mail.bcc), number_of_participants)

    def test_event_reminder_off(self):
        number_of_participants = 3

        name = "Timmy Event"
        description = "Timmy Event description"
        start_date = timezone.now() + timedelta(days=3, hours=1) # we want it to be slightly more than 3 days away.
        end_date = start_date + timedelta(hours=2)
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone,
            send_emails_days_prior=0,
        )

        for i in range(number_of_participants):
            member_i = StartupMemberFactory.create()
            new_event.attending_users.add(member_i.user)

        check_upcoming.apply((new_event.id, ))

        notification_mails = mail.outbox
        self.assertEquals(len(notification_mails), 0)

    def test_event_reminder_before_reminder_date(self):
        number_of_participants = 3

        name = "Timmy Event"
        description = "Timmy Event description"
        start_date = timezone.now() + timedelta(days=5, hours=1) # we want it to be slightly more than 3 days away.
        end_date = start_date + timedelta(hours=2)
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone,
            send_emails_days_prior=3,
        )

        for i in range(number_of_participants):
            member_i = StartupMemberFactory.create()
            new_event.attending_users.add(member_i.user)

        check_upcoming.apply((new_event.id, ))

        notification_mails = mail.outbox
        self.assertEquals(len(notification_mails), 0)

    def test_event_reminder_after_reminder_date(self):
        number_of_participants = 3

        name = "Timmy Event"
        description = "Timmy Event description"
        start_date = timezone.now() + timedelta(days=2, hours=1) # we want it to be slightly more than 3 days away.
        end_date = start_date + timedelta(hours=2)
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone,
            send_emails_days_prior=3,
        )

        for i in range(number_of_participants):
            member_i = StartupMemberFactory.create()
            new_event.attending_users.add(member_i.user)

        check_upcoming.apply((new_event.id, ))

        notification_mails = mail.outbox
        self.assertEquals(len(notification_mails), 0)

    def test_event_reminder_one_day_noti(self):
        number_of_participants = 3

        name = "Timmy Event"
        description = "Timmy Event description"
        start_date = timezone.now() + timedelta(days=1, hours=1) # we want it to be slightly more than 3 days away.
        end_date = start_date + timedelta(hours=2)
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone,
            send_emails_days_prior=3,
        )

        for i in range(number_of_participants):
            member_i = StartupMemberFactory.create()
            new_event.attending_users.add(member_i.user)

        check_upcoming.apply((new_event.id, ))

        notification_mails = mail.outbox
        self.assertEquals(len(notification_mails), 1)
        notification_mail = notification_mails[0]
        self.assertEquals(notification_mail.subject, "An event you RSVPed for is coming up")
        self.assertEquals(len(notification_mail.bcc), number_of_participants)

    def test_event_reminder_past_event(self):
        number_of_participants = 3

        name = "Timmy Event"
        description = "Timmy Event description"
        start_date = timezone.now() - timedelta(days=2, hours=1) # we want it to be slightly more than 3 days away.
        end_date = start_date + timedelta(hours=2)
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone,
            send_emails_days_prior=3,
        )

        for i in range(number_of_participants):
            member_i = StartupMemberFactory.create()
            new_event.attending_users.add(member_i.user)

        check_upcoming.apply((new_event.id, ))

        notification_mails = mail.outbox
        self.assertEquals(len(notification_mails), 0)


class TrunkEventModelTest(AbastractUniTestBase):

    def test_create_event_success(self):
        '''Normal TEST'''
        name = "This is test event"
        description = "THis is event description!!"
        start_date = timezone.now()
        end_date = start_date + timedelta(hours=2)
        # image = None
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone
        )

        self.assertEqual(new_event.name, name)
        self.assertEqual(new_event.description, description)
        self.assertEqual(new_event.start, start_date)
        self.assertEqual(new_event.end, end_date)
        self.assertEqual(new_event.location, location)
        self.assertEqual(new_event.is_competition, is_competition)
        self.assertEqual(new_event.display_activity_feed, display_activity_feed)

        new_event = EventFactory.create(
            name=name,
            description='',
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone
        )

        self.assertEqual(new_event.name, name)
        self.assertEqual(new_event.description, '')
        self.assertEqual(new_event.start, start_date)
        self.assertEqual(new_event.end, end_date)
        self.assertEqual(new_event.location, location)
        self.assertEqual(new_event.is_competition, is_competition)
        self.assertEqual(new_event.display_activity_feed, display_activity_feed)

    def test_event_model_function(self):
        name = "This is test event"
        description = "THis is event description!!"
        year = 2030
        month = 4
        day = 12
        hour = 17
        minute = second = 0
        start_date = timezone.datetime(year, month, day, hour, minute, second)
        start_date_str = start_date.strftime("%A, %B %d, %Y")
        start_datetime_str = "{0} at {1}:00 PM".format(start_date_str, hour-12)
        start_month_day_str = end_month_day_str = start_date.strftime("%B %d, %Y")
        end_date = start_date + timedelta(hours=2)
        end_date_str = end_date.strftime("%A, %B %d, %Y")
        end_datetime_str = "{0} at {1}:00 PM".format(end_date_str, hour+2-12)
        # image = None
        display_activity_feed = True
        is_competition = False
        location = "Ithaca, NY"
        new_event = EventFactory.create(
            name=name,
            description=description,
            start=start_date,
            end=end_date,
            location=location,
            university=self.cornell,
            display_activity_feed=display_activity_feed,
            is_competition=is_competition,
            latitude=0.0,
            longitude=0.0,
            timezone=self.cornell.style.timezone
        )

        self.assertEqual('/event/{0}'.format(new_event.event_id), new_event.get_url())
        self.assertEqual(True, new_event.not_started())
        self.assertEqual(start_date_str, new_event.formatted_start_date)
        self.assertEqual(start_datetime_str, new_event.get_formatted_start_datetime())
        self.assertEqual(end_date_str, new_event.formatted_end_date)
        self.assertEqual(end_datetime_str, new_event.get_formatted_end_datetime())
        self.assertEqual(start_month_day_str, new_event.get_formatted_start_month_day())
        self.assertEqual(end_month_day_str, new_event.get_formatted_end_month_day())
        self.assertEqual(self.cornell.style.timezone, new_event.get_timezone())
        self.assertEqual(True, new_event.is_one_day_event())
        end_date = start_date + timedelta(days=1)
        new_event.end = end_date
        new_event.save()
        self.assertEqual(False, new_event.is_one_day_event())


class CompetitionBasicTest(AbastractUniTestBase):
    def setUp(self):
        super(CompetitionBasicTest, self).setUp()
        self.competition = self.setup_application_competition(self.cornell)
        self.board = self.competition.judge_boards.all()[0]

        # setup applicants
        for _ in range(40):
            MemberApplicantFactory(event=self.competition, is_applied=True)
        for _ in range(30):
            AnonymousApplicantFactory(event=self.competition, is_applied=True)

        self.all_applicants_cnt = MemberApplicant.objects.filter(event=self.competition, is_applied=True).count() + \
            AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).count()

    def test_judge_assign_all(self):
        assign = AssignJudge(self.competition, self.board)
        assign.auto_assign_all_applicants()
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')):
            self.assertEqual(self.all_applicants_cnt, judge.app_cnt)

    def test_judge_assign_judge_per_app(self):
        # setup judges
        for _ in range(5):
            test_judge = JudgeFactory(event=self.competition, platform=self.cornell)
            self.board.add_judge(test_judge)
        self.all_judge_cnt = Judge.objects.filter(event=self.competition).count()
        per_app = 3
        self.board.status = JudgeBoard.JUDGE_PER_APP
        self.board.allotment = per_app
        assign = AssignJudge(self.competition, self.board)
        assign.auto_assign_all_applicants()
        for anony in AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('review_anon')):
            self.assertEqual(per_app, anony.judge_cnt)
        for mem in MemberApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('member__review_applicant')):
            self.assertEqual(per_app, mem.judge_cnt)
        judge_assigned = int(self.all_applicants_cnt * per_app / self.all_judge_cnt)
        more_judge_assigned = judge_assigned
        remain = (self.all_applicants_cnt * per_app) % self.all_judge_cnt
        if remain > 0:
            more_judge_assigned += remain
        remain_counter = 0
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')).order_by('-app_cnt', 'id'):
            if remain_counter < remain:
                self.assertEqual(judge_assigned + 1, judge.app_cnt)
                remain_counter += 1
            else:
                self.assertEqual(judge_assigned, judge.app_cnt)

    def test_judge_assign_judge_per_app2(self):
        """
        12 Judges
        151 total applicants
        :return:
        """
        for _ in range(11):
            test_judge = JudgeFactory(event=self.competition, platform=self.cornell)
            self.board.add_judge(test_judge)
        self.all_judge_cnt = Judge.objects.filter(event=self.competition).count()

        for _ in range(80):
            MemberApplicantFactory(event=self.competition, is_applied=True)
        self.all_applicants_cnt = MemberApplicant.objects.filter(event=self.competition, is_applied=True).count() + \
                                  AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).count()
        per_app = 3
        self.board.status = JudgeBoard.JUDGE_PER_APP
        self.board.allotment = per_app
        assign = AssignJudge(self.competition, self.board)
        assign.auto_assign_all_applicants()
        for anony in AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('review_anon')):
            self.assertEqual(per_app, anony.judge_cnt)
        for mem in MemberApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('member__review_applicant')):
            self.assertEqual(per_app, mem.judge_cnt)
        judge_assigned = int(self.all_applicants_cnt * per_app / self.all_judge_cnt)
        more_judge_assigned = judge_assigned
        remain = (self.all_applicants_cnt * per_app) % self.all_judge_cnt
        if remain > 0:
            more_judge_assigned += remain
        remain_counter = 0
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')).order_by('-app_cnt', 'id'):
            if remain_counter < remain:
                self.assertEqual(judge_assigned + 1, judge.app_cnt)
                remain_counter += 1
            else:
                self.assertEqual(judge_assigned, judge.app_cnt)
                
    def test_judge_assign_app_per_judge_no_force(self):
        # setup judges
        for _ in range(200):
            test_judge = JudgeFactory(event=self.competition, platform=self.cornell)
            self.board.add_judge(test_judge)
        self.all_judge_cnt = Judge.objects.filter(event=self.competition).count()
        per_judge = 3
        self.board.status = JudgeBoard.APP_PER_JUDGE
        self.board.allotment = per_judge
        assign = AssignJudge(self.competition, self.board)
        assign.auto_assign_all_applicants(force=False)
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')).order_by('id'):
            self.assertEqual(per_judge, judge.app_cnt)
        assigned_app_total = per_judge * self.all_judge_cnt
        judges_per_app = int(assigned_app_total / self.all_applicants_cnt)
        overloaded_app_cnt = assigned_app_total % self.all_applicants_cnt
        app_cnt = 0
        total_judge_review = 0
        for mem in MemberApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('member__review_applicant')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, mem.judge_cnt)
            else:
                self.assertEqual(judges_per_app, mem.judge_cnt)
            app_cnt += 1
            total_judge_review += mem.judge_cnt
        for anony in AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('review_anon')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, anony.judge_cnt)
            else:
                self.assertEqual(judges_per_app, anony.judge_cnt)
            app_cnt += 1
            total_judge_review += anony.judge_cnt
        self.assertEqual(assigned_app_total, total_judge_review)

    def test_judge_assign_app_per_judge_force(self):
        # setup judges
        for _ in range(200):
            test_judge = JudgeFactory(event=self.competition, platform=self.cornell)
            self.board.add_judge(test_judge)
        self.all_judge_cnt = Judge.objects.filter(event=self.competition).count()
        per_judge = 8
        self.board.status = JudgeBoard.APP_PER_JUDGE
        self.board.allotment = per_judge
        assign = AssignJudge(self.competition, self.board)
        assign.auto_assign_all_applicants()
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')).order_by('id'):
            self.assertEqual(per_judge, judge.app_cnt)
        assigned_app_total = per_judge * self.all_judge_cnt
        judges_per_app = int(assigned_app_total / self.all_applicants_cnt)
        overloaded_app_cnt = assigned_app_total % self.all_applicants_cnt
        app_cnt = 0
        total_judge_review = 0
        for mem in MemberApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('member__review_applicant')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, mem.judge_cnt)
            else:
                self.assertEqual(judges_per_app, mem.judge_cnt)
            app_cnt += 1
            total_judge_review += mem.judge_cnt
        for anony in AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('review_anon')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, anony.judge_cnt)
            else:
                self.assertEqual(judges_per_app, anony.judge_cnt)
            app_cnt += 1
            total_judge_review += anony.judge_cnt
        self.assertEqual(assigned_app_total, total_judge_review)

        # try one more to see things reset
        assign.auto_assign_all_applicants()
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')).order_by('id'):
            self.assertEqual(per_judge, judge.app_cnt)
        assigned_app_total = per_judge * self.all_judge_cnt
        judges_per_app = int(assigned_app_total / self.all_applicants_cnt)
        overloaded_app_cnt = assigned_app_total % self.all_applicants_cnt
        app_cnt = 0
        total_judge_review = 0
        for mem in MemberApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('member__review_applicant')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, mem.judge_cnt)
            else:
                self.assertEqual(judges_per_app, mem.judge_cnt)
            app_cnt += 1
            total_judge_review += mem.judge_cnt
        for anony in AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('review_anon')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, anony.judge_cnt)
            else:
                self.assertEqual(judges_per_app, anony.judge_cnt)
            app_cnt += 1
            total_judge_review += anony.judge_cnt
        self.assertEqual(assigned_app_total, total_judge_review)

        # Set some to IN Progress and try reassign
        tmp_reviews = JudgeReview.objects.filter(judge__in=self.board.judges.all()).values_list('id', flat=True)[:total_judge_review % 200]
        JudgeReview.objects.filter(id__in=tmp_reviews).update(status=JudgeReview.IN_PROGRESS)
        assign.auto_assign_all_applicants()
        for judge in self.board.judges.all().annotate(app_cnt=Count('review_judge')).order_by('id'):
            self.assertEqual(per_judge, judge.app_cnt)
        assigned_app_total = per_judge * self.all_judge_cnt
        judges_per_app = int(assigned_app_total / self.all_applicants_cnt)
        overloaded_app_cnt = assigned_app_total % self.all_applicants_cnt
        app_cnt = 0
        total_judge_review = 0
        for mem in MemberApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('member__review_applicant')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, mem.judge_cnt)
            else:
                self.assertEqual(judges_per_app, mem.judge_cnt)
            app_cnt += 1
            total_judge_review += mem.judge_cnt
        for anony in AnonymousApplicant.objects.filter(event=self.competition, is_applied=True).annotate(judge_cnt=Count('review_anon')).order_by('-judge_cnt', 'id'):
            if app_cnt < overloaded_app_cnt:
                self.assertEqual(judges_per_app + 1, anony.judge_cnt)
            else:
                self.assertEqual(judges_per_app, anony.judge_cnt)
            app_cnt += 1
            total_judge_review += anony.judge_cnt
        self.assertEqual(assigned_app_total, total_judge_review)


# class TrunkEventViewTest(TestCase):
#     serialized_rollback = True
#
#     def setUp(self):
#         self.university = University.objects.get(short_name='cornell')
#         self.name = self.university.name
#         self.short_name = self.university.short_name
#         self.uni_staff_email = 'aaaa@sunghopark.com'
#         self.password = '1234'
#         self.login_url = reverse('regular_login')
#         self.client = Client(HTTP_HOST=LOCAL_HOST)
#
#     def test_create_edit_event(self):
#         """
#         Test if the page is only visible to university admin and staffs
#         :return:
#         """
#         self.client.post(self.login_url,
#                          {'email': self.uni_staff_email,
#                           'password': self.password})
#         # successful case
#         name = "This is test event"
#         description = "THis is event description!!"
#         year = 2030
#         month = 4
#         day = 12
#         hour = 17
#         minute = second = 0
#         start_date = timezone.datetime(year, month, day, hour, minute, second)
#         end_date = start_date + timedelta(hours=2)
#         with open('static/img/branch.jpg', 'rb') as img:
#             image = SimpleUploadedFile('logo.jpg', img.read(), 'image/jpeg')
#         display_activity_feed = True
#         display_activity_feed_checkbox = "on"
#         is_competition = False
#         location = "Ithaca, NY"
#         response = self.client.post(
#             reverse('uni-create-event'),
#             {
#                 'event_name': name,
#                 'text': description,
#                 'event_start_date': "{0}/{1}/{2}".format(month, day, year),
#                 'event_start_time': "{0}:{1} pm".format(hour-12, minute),
#                 'event_end_date': "{0}/{1}/{2}".format(month, day, year),
#                 'event_end_time': "{0}:{1} pm".format(hour-10, minute),
#                 'event_image': image,
#                 'event_location': location,
#                 'activity_checkbox': display_activity_feed_checkbox,
#                 'latitude': 1,
#                 'longitude': 2,
#             }
#         )
#         # redirects to uni-manage-events with status of creation in GET.
#         self.assertEquals(response.status_code, 302)
#         expected_url = "http://{0}{1}?s=s".format(LOCAL_HOST, reverse('uni-manage-events'))
#         self.assertRedirects(response, expected_url)
#         new_event = Event.objects.get(name=name)
#         self.assertEqual(new_event.name, name)
#         self.assertEqual(new_event.description, description)
#         self.assertEqual(new_event.get_formatted_start_datetime(), start_date.strftime("%A, %B %d at %-I:%M %p"))
#         self.assertEqual(new_event.get_formatted_end_datetime(), end_date.strftime("%A, %B %d at %-I:%M %p"))
#         self.assertEqual(new_event.location, location)
#         self.assertEqual(new_event.is_competition, is_competition)
#         self.assertEqual(new_event.display_activity_feed, display_activity_feed)
#         # activity was created
#         self.assertTrue(Activity.objects.filter(obj_event=new_event).exists())
#         # error
#         name = "This is test event2"
#         response = self.client.post(
#             reverse('uni-create-event'),
#             {
#                 'event_name': name,
#                 'event_description': description,
#                 # 'event_start_date': "{0}/{1}/{2}".format(month, day, year),
#                 # 'event_start_time': "{0}:{1} pm".format(hour-12, minute),
#                 # 'event_end_date': "{0}/{1}/{2}".format(month, day, year),
#                 # 'event_end_time': "{0}:{1} pm".format(hour-10, minute),
#                 'event_image': image
#             }
#         )
#         self.assertEquals(response.status_code, 302)
#         expected_url = "http://{0}{1}?s=e".format(LOCAL_HOST, reverse('uni-manage-events'))
#         self.assertRedirects(response, expected_url)
#         self.assertFalse(Event.objects.filter(name=name).exists())
#
#         '''test editing the event'''
#         new_event_id = new_event.event_id
#         new_description = "AHhhh I made a mistake?"
#         with open('static/img/branch.jpg', 'rb') as img:
#             image = SimpleUploadedFile('logo.jpg', img.read(), 'image/jpeg')
#         self.client.post(
#             reverse('uni-manage-events-edit', kwargs={'event_id': new_event_id}),
#             {
#                 'event_name': "Modified event!",
#                 'text': new_description,
#                 'event_start_date': "{0}/{1}/{2}".format(month, day+1, year),
#                 'event_start_time': "{0}:{1} pm".format(hour-11, minute),
#                 'event_end_date': "{0}/{1}/{2}".format(month, day+1, year),
#                 'event_end_time': "{0}:{1} pm".format(hour-9, minute),
#                 'event_image': image,
#                 'location': 'Los Angeles, CA',
#                 'latitude': 1,
#                 'longitude': 2,
#             }
#         )
#         modified_event = Event.objects.get(event_id=new_event_id)
#         self.assertNotEqual(modified_event.name, new_event.name)
#         self.assertNotEqual(modified_event.description, new_event.description)
#         self.assertNotEqual(modified_event.get_formatted_start_datetime(), new_event.get_formatted_start_datetime())
#         self.assertNotEqual(modified_event.get_formatted_end_datetime(), new_event.get_formatted_end_datetime())
#         self.assertNotEqual(modified_event.location, location)
#         self.assertEqual(modified_event.is_competition, is_competition)
#         self.assertNotEqual(modified_event.display_activity_feed, display_activity_feed)
#
#         '''test deleting event'''
#         self.client.post(
#             reverse('uni-delete-event'),
#             {
#                 'event_id': new_event_id
#             }
#         )
#         self.assertFalse(Event.objects.filter(event_id=new_event_id).exists())
#         # activity was deleted
#         self.assertFalse(Activity.objects.filter(obj_event=new_event).exists())
