from django.http import HttpResponse
from django.utils import timezone as django_timezone
from django.urls import reverse

from auto_test_utility.utills import AbastractUniTestBase
from branch.models import StartupMember
from emailuser.models import UserEmail
from emailuser.factory import UserEmailFactory
from forms.models import (
    Form,
    MemberApplicant,
    Question,
    Rubric,
    RubricAnswer,
)
from StartupTree.utils import QuerySetType
from StartupTree.utils.pretty import printer
from trunk.models import (
    Event,
    Judge,
    JudgeBoard,
    JudgeReview,
)

from abc import ABC, abstractmethod
import datetime as dt
import pytz
import xlsxwriter
import random
from typing import (
    cast,
    Dict,
    Iterable,
    List,
    Optional,
    Tuple,
    TypedDict,
)


COMPETITION_NAME = "Test Competition"
COMPETITION_DESCRIPTION = "Test Competition DESCRIPTION"

NOON = dt.time(hour=12, minute=0, second=0)
END_OF_DAY = dt.time(hour=17, minute=0, second=0)

# number of days after (or before if negative) today for start, end, and judge deadline
START_DAY_DELTA = -1
END_DAY_DELTA = 5
JUDGE_DEADLINE_DELTA = 10

QUESTION_TEXT = "TEST QUESTION"
# CHOICE_A_TEXT = "Test Choice A"
# CHOICE_B_TEXT = "Test Choice B"

RUBRIC_TEXT = "TEST RUBRIC SCORE"
RUBRIC_CAP = 10000
RUBRIC_ANSWER_SCORE = 3110
JUDGE_COMMENT = "Judge Comment"
JUDGE_NOTES = "Judge Notes"

START_TIME = NOON
END_TIME = END_OF_DAY

JUDGE_PASSCODE = '123456'

JUDGE_IMPORT_FILE_PATH = 'tmp/file.xlsx'

NUM_PARTICIPANTS = 10
NUM_JUDGES = 5

# Event Create Steps
STEP_DETAILS = 0
STEP_QUESTIONS = 1
STEP_RUBRIC = 2
STEP_JUDGES = 3

# User Action Steps
STEP_PARTICIPANTS = 4
STEP_ASSIGN_JUDGES = 5
STEP_JUDGE_REVIEW = 6


# Button Id's
BUTTON_AUTO_ASSIGN = 'auto-assign-judge'
BUTTON_MANUAL = 'manual_assign_btn'
BUTTON_DONE = 'btn_done'


class PeopleUtils:
    class JudgeContact(TypedDict):
        first_name: str
        last_name: str
        email: str

    def __init__(self, competition: Event):
        self.competition = competition

    def get_judge(self, i: int) -> Judge:
        email = PeopleUtils.make_judge_email(i)
        return Judge.objects.get(email=email, event=self.competition)

    @staticmethod
    def get_participant(i: int) -> StartupMember:
        email = PeopleUtils.make_participant_email(i)
        return StartupMember.objects.get(user__email=email)

    def get_member_applicant(self, participant: StartupMember) -> MemberApplicant:
        return MemberApplicant.objects.get(member=participant, event=self.competition)

    @staticmethod
    def make_judge_email(i: int) -> str:
        return f'judge{i}@hello.com'

    @staticmethod
    def make_judge_contact(i: int) -> JudgeContact:
        return {
            'first_name': f'FJudge{i}',
            'last_name': f'LJudge{i}',
            'email': PeopleUtils.make_judge_email(i),
        }

    @staticmethod
    def make_participant_email(i: int) -> str:
        return f'participant{i}@hello.com'

    @staticmethod
    def make_participant(i: int) -> StartupMember:
        email = PeopleUtils.make_participant_email(i)
        UserEmail.objects.filter(email=email).delete()
        user = UserEmailFactory(email=email)
        StartupMember.objects.filter(user=user).delete()

        return StartupMember.objects.create_sm(
            first_name=f'FParticipant{i}',
            last_name=f'LParticipant{i}',
            user_email=user)

    def get_people_from_manual_assignments(
            self,
            manual_judging_assignments: 'ManualJudgingAssignments'
    ) -> Iterable[Tuple[StartupMember, QuerySetType[Judge]]]:
        """Returns an iterable of tuples that map a participant to the intended assigned judges"""
        def get_people(manual_judging_assignment) -> Tuple[StartupMember, QuerySetType[Judge]]:
            participant_num, judge_nums = cast(Iterable[Tuple[int, List[int]]], manual_judging_assignment)
            participant = self.get_participant(participant_num)
            judge_emails = list(map(self.make_judge_email, judge_nums))
            judges = Judge.objects.filter(email__in=judge_emails)
            return participant, judges

        return map(get_people, manual_judging_assignments.items())


class JudgingParams(TypedDict):
    """Adjustable judging parameters to test"""
    judge_deadline: dt.datetime
    judge_assign_type: int
    allotment: Optional[int]
    num_judges: int


class ParticipantsParams(TypedDict):
    num_participants: int
    participant_answers: Dict[int, str]  # maps a participant index to their intended text answer


ManualJudgingAssignments = Dict[int, List[int]]  # mapping form participant number to list of judge numbers assigned to judge


class MakeJudgeImportFile:
    workbook: xlsxwriter.Workbook
    sheet: xlsxwriter.workbook.Worksheet

    def __init__(self, num_judges: int, file_path: str):
        self.judges_list = list(map(PeopleUtils.make_judge_contact, range(num_judges)))
        self.file_path = file_path

    def _write_row(self, judge_contact: PeopleUtils.JudgeContact, row: int):
        self.sheet.write(row, 0, judge_contact['first_name'])
        self.sheet.write(row, 1, judge_contact['last_name'])
        self.sheet.write(row, 2, judge_contact['email'])

    def _write(self):
        self.workbook = xlsxwriter.Workbook(self.file_path)
        self.sheet = self.workbook.add_worksheet()

        for row, judge_contact in enumerate(self.judges_list):
            self._write_row(judge_contact, row)

        self.workbook.close()

    @staticmethod
    def write(num_judges: int) -> str:
        """
        Creates a spreadsheet at JUDGE_IMPORT_FILE_PATH of num_judges judges,
        where each row represents a judge
        and the columns respectively contain first name, last name, and email.

        returns the path of the file written to
        """
        MakeJudgeImportFile(num_judges, JUDGE_IMPORT_FILE_PATH)._write()
        return JUDGE_IMPORT_FILE_PATH


class AdminJudgeForm:
    def __init__(self, judging_params: JudgingParams, is_assign=False):
        self.judging_params = judging_params
        self.is_assign = is_assign

    @staticmethod
    def _make_passcode_data():
        return {'passcode': JUDGE_PASSCODE}

    def _make_judge_deadline_data(self):
        deadline = self.judging_params['judge_deadline']
        return {
            'judge_end_date': deadline.strftime('%m/%d/%Y'),
            'judge_end_time': deadline.strftime('%I:%M %p'),
        }

    @staticmethod
    def _make_empty_formset():
        """
        returns an empty valid formset (otherwise, backend will raise an error).
        """
        return {
            'form-TOTAL_FORMS': '1',
            'form-INITIAL_FORMS': '0',
        }

    def _open_judge_import_file(self):
        file = MakeJudgeImportFile.write(self.judging_params['num_judges'])
        return open(file, 'rb')

    def get_button_pressed(self) -> str:
        assign_type = self.judging_params['judge_assign_type']
        if self.is_assign:
            if assign_type == JudgeBoard.MANUALLY_ASSIGN:
                return BUTTON_MANUAL
            elif assign_type in [JudgeBoard.JUDGE_PER_APP, JudgeBoard.ALL_FOR_ALL, JudgeBoard.APP_PER_JUDGE]:
                return BUTTON_AUTO_ASSIGN
            else:
                raise ValueError(f'Invalid JudgeBoard.status value: {assign_type}')
        else:
            return BUTTON_DONE

    def _make_form_data(self):
        allotment = self.judging_params['allotment']
        return {
            'round': '1',
            'status': str(self.judging_params['judge_assign_type']),
            'allotment': str(allotment) if allotment is not None else '',
            'file': self._open_judge_import_file(),
            ** self.__class__._make_passcode_data(),
            ** self._make_judge_deadline_data(),
            ** self.__class__._make_empty_formset(),
            self.get_button_pressed(): '1',
        }

    @staticmethod
    def make_form_data(judging_params: JudgingParams, is_assign: bool) -> dict:
        """
        Returns the request data (to be supplied to client.POST) containing the POST data and FILES that
        CreateJudgeBoard.post() expects
        """
        return AdminJudgeForm(judging_params, is_assign)._make_form_data()


class AddParticipants:
    def __init__(self, test_base: 'TestBase', competition: Event, participants_params: ParticipantsParams):
        self.test_base = test_base
        self.competition = competition
        self.participants_params = participants_params

    def _make_form_data(self, answer_text: str):
        """
        returns post data simulating a custom question answered.
        """
        question = self.competition.form.questions.first()

        return {
            'count': 1,
            'type0': question.get_question_type_str(),
            'id0': question.id,
            'pp0': answer_text,
        }

    def _add_participants(self):
        for i in range(self.participants_params['num_participants']):
            member = PeopleUtils.make_participant(i)

            test_base = self.test_base
            apply_path = UrlUtils(self.competition).get_participant_apply_path()
            data = self._make_form_data(self.participants_params['participant_answers'][i])

            test_base.post(member.user.email, '1234', apply_path, data)

    @staticmethod
    def add_participants(test_base: 'TestBase', competition: Event, participants_params: ParticipantsParams):
        AddParticipants(test_base, competition, participants_params)._add_participants()


class CreateCompetition:
    competition: Event

    def __init__(self, test_base: 'TestBase'):
        self.uni = test_base.cornell
        self.timezone = test_base.cornell.style.timezone
        self.test_base = test_base

    def _dt_from_delta(self, day_delta: int, time: dt.time) -> dt.datetime:
        return DateUtils.dt_from_delta(day_delta, time, self.timezone)

    def _add_questions(self):
        form = self.competition.form

        question = Question.objects.create(
            q_type=Question.PP,
            number=1,
            text=QUESTION_TEXT,
            helper_text=None,
            char_limit=None,
            is_required=True,
            is_judge_visible=True,
        )

        form.questions.add(question)

        self.competition.form = form
        self.competition.save()

    def _add_rubrics(self):
        form = self.competition.form

        rubric = Rubric.objects.create(
            r_type=Rubric.SC,
            number=1,
            description=None,
            text=RUBRIC_TEXT,
            min=None,
            cap=RUBRIC_CAP,
            weight=None,
            round=1,
        )

        form.rubrics.add(rubric)

    def _add_judging(self, judging_params: JudgingParams):
        form_data = AdminJudgeForm.make_form_data(judging_params, is_assign=False)
        test_base = self.test_base

        path = UrlUtils(self.competition).get_admin_judge_path()

        test_base.post(test_base.admin.user.email, '1234', path, form_data)

    def _assign_judges(self, judging_params: JudgingParams):
        url_utils = UrlUtils(self.competition)
        form_data = AdminJudgeForm.make_form_data(judging_params, is_assign=True)
        test_base = self.test_base
        test_base.post(test_base.admin.user.email, '1234', url_utils.get_admin_judge_path(), form_data)

    def create_competition(
            self,
            start_day_delta: int,
            end_day_delta: int,
            is_application: bool,
            test_step: int,
            judging_params: Optional[JudgingParams],
            participants_params: Optional[ParticipantsParams],
    ) -> Event:
        if test_step == STEP_DETAILS:
            raise ValueError('No need to create an event prior to testing event creation step')

        if test_step > STEP_JUDGES and judging_params is None:
            raise ValueError('judging_params must be specified when testing a step after STEP_JUDGES')

        if test_step > STEP_PARTICIPANTS and participants_params is None:
            raise ValueError('participants_params must be specified when testing a step after STEP_PARTICIPANTS')

        start = self._dt_from_delta(start_day_delta, START_TIME)
        end = self._dt_from_delta(end_day_delta, END_TIME)

        self.competition = Event.objects.create_event(
            name=COMPETITION_NAME,
            description=COMPETITION_DESCRIPTION,
            start=start,
            end=end,
            timezone=self.timezone,
            university=self.uni,
            is_competition=True,
            is_published=True,
            is_application=is_application,
        )

        self.competition.form = Form.objects.create(form=Form.COMPETITION, university=self.uni)
        self.competition.save()

        if STEP_QUESTIONS < test_step:
            self._add_questions()

        if STEP_RUBRIC < test_step:
            self._add_rubrics()

        if STEP_JUDGES < test_step:
            self._add_judging(judging_params)

        if STEP_PARTICIPANTS < test_step:
            AddParticipants.add_participants(self.test_base, self.competition, participants_params)

        if STEP_ASSIGN_JUDGES < test_step:
            self._assign_judges(judging_params)

        return self.competition

    @staticmethod
    def create(
            test_base: 'TestBase',
            start_day_delta: int,
            end_day_delta: int,
            is_application: bool,
            test_step: int,
            judging_params: JudgingParams = None,
            participants_params: ParticipantsParams = None
    ):
        """
        Creates a competition whose start day is start_day_delta after today, and end day is end_day_delta after today.

        Deltas may be negative for past.

        test_step is which step of the application/competition creation process we want to test.
        For example, if we want to test the judging step, then an event will be created as if all prior
        steps (details, questions, rubric) have been done.

        Testing any step after STEP_JUDGES requires judging_params (so we know how to set up judging).
        Testing any step after STEP_PARTICIPANTS requires participants_params
        """
        return CreateCompetition(test_base).create_competition(
            start_day_delta, end_day_delta, is_application, test_step, judging_params, participants_params)


class UrlUtils:
    def __init__(self, competition: Event):
        self.competition = competition

    def get_admin_judge_path(self):
        return self.get_path('uni-edit-competition-judges', 'uni-edit-application-judges')

    def get_participant_apply_path(self):
        return self.get_path('competition-enter', 'application-apply')

    def get_manual_assign_judges_path(self):
        return self.get_path('competition-manually-assign-judges', 'application-manually-assign-judges')

    def get_path(self, competition_url_name, application_url_name):
        url_name = competition_url_name if not self.competition.is_application else application_url_name
        return reverse(url_name, kwargs={'event_id': self.competition.event_id})


class JudgeUrlUtils:
    def __init__(self, judge_id):
        self.judge_id = judge_id

    def get_judge_login_url(self, competition: Event):
        kwargs = {
            'event_id': competition.event_id,
            'judge_id': self.judge_id
        }
        return reverse('login_judge_via_pass', kwargs=kwargs)

    def get_judge_all_apps_view_url(self):
        return reverse('judge_all_apps_view', kwargs={'judge_id': self.judge_id})

    def get_save_review_url(self, applicant_urlname):
        return reverse('save_review', kwargs={'judge_id': self.judge_id, 'app_url_name': applicant_urlname})

    def get_submit_review_url(self):
        return reverse('submit_review', kwargs={'judge_id': self.judge_id})


class DateUtils:
    @staticmethod
    def make_date(day_delta: int) -> dt.datetime:
        return django_timezone.now() + dt.timedelta(days=day_delta)

    @staticmethod
    def dt_from_delta(day_delta: int, time: dt.time, timezone: str) -> dt.datetime:
        date = django_timezone.now().date() + dt.timedelta(days=day_delta)
        return dt.datetime.combine(date, time).astimezone(pytz.timezone(timezone))


class TestBase(AbastractUniTestBase):
    def post(self, email: str, password: str, path: str, data: dict):
        """
        Logs in user, and performs a POST request to path with data.
        Then logs user out (needed so that another user can login)
        """
        self.login_user(self.client, email, password)
        response = self.client.post(path, data)
        self.logout_user(self.client)

        return response


class GeneralJudgeAddTests(TestBase):
    def general_judge_add_test(
            self,
            is_application: bool,
            start_day_delta: int = START_DAY_DELTA,
            end_day_delta: int = END_DAY_DELTA,
            judge_deadline_delta: int = JUDGE_DEADLINE_DELTA,
    ):
        competition = CreateCompetition.create(
            self,
            start_day_delta=start_day_delta,
            end_day_delta=end_day_delta,
            is_application=is_application,
            test_step=STEP_JUDGES)

        utils = UrlUtils(competition)

        path = utils.get_admin_judge_path()

        form_data = AdminJudgeForm.make_form_data({
            'judge_deadline': DateUtils.make_date(judge_deadline_delta),
            'judge_assign_type': JudgeBoard.ALL_FOR_ALL,
            'allotment': None,
            'num_judges': 5,
        }, is_assign=False)

        response = self.post(self.admin.user.email, '1234', path, form_data)

        self.assertLess(response.status_code, 400)


class GeneralAssignJudgesTests(TestBase):
    def general_assign_judges_judgeperapp_test(self, is_application: bool):
        self.general_assign_judges_test(
            is_application=is_application,
            judge_assign_type=JudgeBoard.JUDGE_PER_APP,
            allotment=int(NUM_JUDGES * 3 / 5),
            num_judges=NUM_JUDGES,
            num_participants=NUM_PARTICIPANTS,
        )

    def general_assign_judges_allforall_test(self, is_application: bool):
        self.general_assign_judges_test(is_application=is_application, judge_assign_type=JudgeBoard.ALL_FOR_ALL)

    def general_assign_judges_manual_test(
            self,
            is_application: bool,
            num_judges: int = NUM_JUDGES,
            num_participants: int = NUM_PARTICIPANTS,
    ):
        self.general_assign_judges_test(
            is_application=is_application,
            judge_assign_type=JudgeBoard.MANUALLY_ASSIGN,
            num_judges=num_judges,
            num_participants=num_participants,
            manual_judging_assignments=self.generate_manual_assignments(num_judges, num_participants))

    @staticmethod
    def generate_manual_assignments(num_judges: int, num_participants: int) -> ManualJudgingAssignments:
        participant_num: int
        judges_per = int(num_judges * 3 / 5)
        judge_nums = list(range(num_judges))
        return {
            participant_num: list(random.choices(judge_nums, k=judges_per))
            for participant_num in range(num_participants)
        }

    def general_assign_judges_appperjudge_test(self, is_application: bool):
        self.general_assign_judges_test(
            is_application=is_application,
            judge_assign_type=JudgeBoard.APP_PER_JUDGE,
            allotment=int(NUM_PARTICIPANTS * 3 / 5),
            num_judges=NUM_JUDGES,
            num_participants=NUM_PARTICIPANTS,
        )

    def general_assign_judges_test(
            self,
            is_application: bool,
            judge_assign_type: int,
            allotment: Optional[int] = None,
            start_day_delta: int = START_DAY_DELTA,
            end_day_delta: int = END_DAY_DELTA,
            judge_deadline_delta: int = JUDGE_DEADLINE_DELTA,
            num_judges: int = NUM_JUDGES,
            num_participants: int = NUM_PARTICIPANTS,
            manual_judging_assignments: ManualJudgingAssignments = None  # list of pairing from judge to participant number
    ):

        judging_params: JudgingParams = {
            'judge_deadline': DateUtils.make_date(judge_deadline_delta),
            'judge_assign_type': judge_assign_type,
            'allotment': allotment,
            'num_judges': num_judges,
        }

        i: int
        participants_params: ParticipantsParams = {
            'num_participants': num_participants,
            'participant_answers': {
                i: f'Answer {i}'
                for i in range(num_participants)
            }
        }

        competition = CreateCompetition.create(
            self,
            start_day_delta=start_day_delta,
            end_day_delta=end_day_delta,
            is_application=is_application,
            test_step=STEP_ASSIGN_JUDGES,
            judging_params=judging_params,
            participants_params=participants_params,
        )

        utils = UrlUtils(competition)

        path = utils.get_admin_judge_path()

        form_data = AdminJudgeForm.make_form_data({
            'judge_deadline': DateUtils.make_date(judge_deadline_delta),
            'judge_assign_type': judge_assign_type,
            'allotment': allotment,
            'num_judges': num_judges,
        }, is_assign=True)

        response = self.post(self.admin.user.email, '1234', path, form_data)
        self.assertLess(response.status_code, 400)

        if judge_assign_type == JudgeBoard.MANUALLY_ASSIGN:
            assign_responses = self.assign_manual_judging(competition, manual_judging_assignments)
            for r in assign_responses:
                self.assertLess(r.status_code, 400)

        self.assertJudgesAssigned(competition, judging_params, participants_params, manual_judging_assignments)

    def assign_manual_judging(
            self,
            competition: Event,
            manual_judging_assignments: ManualJudgingAssignments) -> List[HttpResponse]:
        people_utils = PeopleUtils(competition)

        def assign_to_participant(participant: StartupMember, judges: QuerySetType[Judge]) -> HttpResponse:
            data = {
                'applicant': participant.id,
                'judges[]': list(map(str, judges.values_list('judge_id', flat=True))),
            }

            url = UrlUtils(competition).get_manual_assign_judges_path()

            return self.post(self.admin.user.email, '1234', url, data)

        responses = []

        for p, js in people_utils.get_people_from_manual_assignments(manual_judging_assignments):
            r = assign_to_participant(p, js)
            responses.append(r)

        return responses

    def assertJudgesAssigned(
            self,
            competition: Event,
            judging_params: JudgingParams,
            participants_params: ParticipantsParams,
            manual_judging_assignments: Optional[ManualJudgingAssignments]
    ):
        judge_board: JudgeBoard = competition.judge_boards.first()
        judge_assign_type = judge_board.status

        if judge_assign_type == JudgeBoard.JUDGE_PER_APP:
            self.assertJudgesAssigned_JudgePerApp(competition, judging_params, participants_params)

        elif judge_assign_type == JudgeBoard.ALL_FOR_ALL:
            self.assertJudgesAssigned_AllForAll(competition, judging_params, participants_params)

        elif judge_assign_type == JudgeBoard.MANUALLY_ASSIGN:
            self.assertJudgesAssigned_Manual(competition, manual_judging_assignments)

        elif judge_assign_type == JudgeBoard.APP_PER_JUDGE:
            self.assertJudgesAssigned_AppPerJudge(competition, judging_params, participants_params)

        else:
            raise ValueError('invalid judge_assign_type')

    def assertJudgesAssigned_JudgePerApp(self,
                                         competition: Event,
                                         judging_params: JudgingParams,
                                         participants_params: ParticipantsParams):
        allotment = judging_params['allotment']

        def check_participant(participant: StartupMember):
            """Verify that each participant has alloted number of judges"""
            reviews = JudgeReview.objects.filter(event=competition, applicant=participant).order_by('judge').distinct('judge')
            self.assertEqual(reviews.count(), allotment)

        def check_judge(judge: Judge):
            """Verify that judge is assigned to at least one participant """
            reviews = JudgeReview.objects.filter(event=competition, judge=judge)
            self.assertGreaterEqual(reviews.count(), 1)

        people_utils = PeopleUtils(competition)
        num_participants = participants_params['num_participants']

        for i in range(num_participants):
            p = people_utils.get_participant(i)
            check_participant(p)

        num_judges = judging_params['num_judges']

        if num_judges <= allotment * num_participants:
            # each judge should have an opportunity to judge.
            for i in range(judging_params['num_judges']):
                j = people_utils.get_judge(i)
                check_judge(j)

    def assertJudgesAssigned_Manual(self,
                                    competition: Event,
                                    manual_judging_assignments: ManualJudgingAssignments):
        people_utils = PeopleUtils(competition)
        judge_board: JudgeBoard = competition.judge_boards.first()
        all_judges = judge_board.judges.all()
        for participant, judges in people_utils.get_people_from_manual_assignments(manual_judging_assignments):
            for judge in judges:
                self.assertReviewCount(judge, participant, intended_count=1)
            for not_judge in all_judges.difference(judges):
                self.assertReviewCount(not_judge, participant, intended_count=0)

    def assertJudgesAssigned_AllForAll(
            self,
            competition: Event,
            judging_params: JudgingParams,
            participants_params: ParticipantsParams
    ):
        people_utils = PeopleUtils(competition)
        judges = list(map(people_utils.get_judge, range(judging_params['num_judges'])))
        participants = list(map(PeopleUtils.get_participant, range(participants_params['num_participants'])))

        def check_judge(judge: Judge):
            judge_board: JudgeBoard = competition.judge_boards.first()
            self.assertTrue(judge in judge_board.judges.all())

            for participant in participants:
                self.assertReviewCount(judge, participant, intended_count=1)

        for j in judges:
            check_judge(j)

    def assertJudgesAssigned_AppPerJudge(self,
                                         competition: Event,
                                         judging_params: JudgingParams,
                                         participants_params: ParticipantsParams):
        allotment = judging_params['allotment']
        people_utils = PeopleUtils(competition)
        num_judges = judging_params['num_judges']
        num_participants = participants_params['num_participants']

        def check_judge(judge: Judge):
            """Verify that judge is assigned to at allotted number of participants """
            reviews = JudgeReview.objects.filter(event=competition, judge=judge).order_by('applicant').distinct(
                'applicant')
            self.assertEqual(reviews.count(), allotment)

        def check_participant(participant: StartupMember):
            """Verify that each participant has at least one judge"""
            reviews = JudgeReview.objects.filter(event=competition, applicant=participant)
            self.assertGreaterEqual(reviews.count(), 1)

        for i in range(num_judges):
            j = people_utils.get_judge(i)
            check_judge(j)

        for i in range(num_participants):
            p = people_utils.get_participant(i)
            check_participant(p)

    def assertReviewCount(self, judge: Judge, participant: StartupMember, intended_count: int):
        existing_reviews = JudgeReview.objects.filter(judge=judge, applicant=participant)
        self.assertEqual(existing_reviews.count(), intended_count)


class GeneralJudgeReviewTests(TestBase):
    def general_judge_access_test(
            self,
            is_application: bool,
            start_day_delta: int = START_DAY_DELTA,
            end_day_delta: int = END_DAY_DELTA,
            judge_deadline_delta: int = JUDGE_DEADLINE_DELTA,
            num_judges: int = NUM_JUDGES,
            num_participants: int = NUM_PARTICIPANTS,
    ):
        judging_params: JudgingParams = {
            'judge_deadline': DateUtils.make_date(judge_deadline_delta),
            'judge_assign_type': JudgeBoard.ALL_FOR_ALL,
            'allotment': None,
            'num_judges': num_judges,
        }

        i: int
        participants_params: ParticipantsParams = {
            'num_participants': num_participants,
            'participant_answers': {
                i: f'Answer {i}'
                for i in range(num_participants)
            }
        }

        competition = CreateCompetition.create(
            self,
            start_day_delta=start_day_delta,
            end_day_delta=end_day_delta,
            is_application=is_application,
            test_step=STEP_JUDGE_REVIEW,
            judging_params=judging_params,
            participants_params=participants_params,
        )

        people_utils = PeopleUtils(competition)
        judge = people_utils.get_judge(0)
        participant = people_utils.get_participant(0)
        judge_url_utils = JudgeUrlUtils(judge.judge_id)

        data_valid_passcode = {
            'passcode': JUDGE_PASSCODE
        }

        data_invalid_passcode = {
            'passcode': f'{JUDGE_PASSCODE}XXX'
        }

        judge_login_url = judge_url_utils.get_judge_login_url(competition)
        judge_all_apps_view_url = judge_url_utils.get_judge_all_apps_view_url()
        judge_save_review_url = judge_url_utils.get_save_review_url(participant.url_name)
        judge_submit_url = judge_url_utils.get_submit_review_url()

        # test invalid login
        response_invalid_login = self.client.post(judge_login_url, data_invalid_passcode)
        self.assertEqual(response_invalid_login.status_code, 200)  # back to login
        self.assertTemplateUsed(response_invalid_login, 'branch/login_judge.html')

        # test valid login
        response_valid_login = self.client.post(judge_login_url, data_valid_passcode)
        self.assertRedirects(response_valid_login, judge_all_apps_view_url)

        rubric: Rubric = competition.form.rubrics.first()

        data_save_review = {
            'count': 1,
            'type0': rubric.get_rubric_type(),
            'id0': rubric.id,
            'sc0': RUBRIC_ANSWER_SCORE,
            'comment': JUDGE_COMMENT,
            'notes': JUDGE_NOTES,
        }

        data_submit_review = {

        }

        # test save
        response_save = self.client.post(judge_save_review_url, data_save_review)
        self.assertLess(response_save.status_code, 400)
        self.assertReviewSaved(competition, judge, participant)

        # test submit
        response_submit = self.client.post(judge_submit_url, data_submit_review)
        self.assertLess(response_submit.status_code, 400)
        self.assertReviewSubmitted(competition, judge, participant)

    def assertReviewSaved(self, competition: Event, judge: Judge, participant: StartupMember):
        rubric: Rubric = competition.form.rubrics.first()
        answers = RubricAnswer.objects.filter(r=rubric, judge=judge, applicant=participant)

        self.assertEqual(answers.count(), 1)

        if answers.count() == 1:
            answer = answers.first()
            self.assertEqual(answer.score, RUBRIC_ANSWER_SCORE)

    def assertReviewSubmitted(self, competition: Event, judge: Judge, participant: StartupMember):
        review = JudgeReview.objects.get(judge=judge, applicant=participant, event=competition)
        self.assertEqual(review.status, JudgeReview.COMPLETE)


class TestJudgeAdd(GeneralJudgeAddTests):
    def test_competition_judging(self):
        self.general_judge_add_test(is_application=False)

    def test_application_judging(self):
        self.general_judge_add_test(is_application=True)


class TestAssignJudges(GeneralAssignJudgesTests):
    def test_competition_assign_judges_judgeperapp(self):
        self.general_assign_judges_judgeperapp_test(is_application=False)

    def test_application_assign_judges_judgeperapp(self):
        self.general_assign_judges_judgeperapp_test(is_application=True)

    def test_competition_assign_judges_allforall(self):
        self.general_assign_judges_allforall_test(is_application=False)

    def test_application_assign_judges_allforall(self):
        self.general_assign_judges_allforall_test(is_application=True)

    def test_competition_assign_judges_manual(self):
        self.general_assign_judges_manual_test(is_application=False)

    def test_application_assign_judges_manual(self):
        self.general_assign_judges_manual_test(is_application=True)

    def test_competition_assign_judges_appperjudge(self):
        self.general_assign_judges_appperjudge_test(is_application=False)

    def test_application_assign_judges_appperjudge(self):
        self.general_assign_judges_appperjudge_test(is_application=True)


class TestJudgeReview(GeneralJudgeReviewTests):
    def test_competition_judge_access(self):
        self.general_judge_access_test(is_application=False)

    def test_application_judge_access(self):
        self.general_judge_access_test(is_application=True)


class TestAll(TestJudgeAdd, TestAssignJudges, TestJudgeReview):
    pass
