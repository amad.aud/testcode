#rest_framework integration trial begin
from trunk.views.api import UniversityList
from rest_framework.test import APIRequestFactory, APITestCase
from django.urls import reverse
from rest_framework import status
from trunk.models import University
from trunk.serializers.university import UniversitySerializer

class UniversityListTests(APITestCase):
	def test_the_number_of_universities(self):
		#Creating and Saving 2 Universities. 
		uni_1 = University(name="Cornell University", short_name="Cornell")
		uni_2 = University(name="Stanford University", short_name="Stanford")
		uni_1.save()
		uni_2.save()

		#Make the API call
		url = reverse("uni-list")
		response = self.client.get(url, format='json')

		#TEST 1: Check that it went through.
		self.assertEqual(response.status_code, status.HTTP_200_OK)

		#TEST 2: Check that it returned the correct number of universities
		self.assertEqual(len(response.data), 2)

		#TEST 3: Check that it returned the correct names and short_names of the universities
		for university in response.json():
			self.assertIn(university["name"], ['Cornell University', 'Stanford University'])
			self.assertIn(university["short_name"], ['Cornell', 'Stanford'])

#rest_framework integration trial end