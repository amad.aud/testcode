from auto_test_utility.utills import AbastractUniTestBase
from django.core import mail
from django.test import Client
from branch.factory import StartupMemberFactory
from branch.tasks import check_upcoming
from trunk.factory import UniversityStyleFactory
from trunk.factory.event import EventFactory, EventRSVPFactory, GuestRSVPFactory
from django.urls import reverse
from django.utils import timezone
from datetime import timedelta
from pathlib import Path
from os import listdir
import os


class EventReminderTest(AbastractUniTestBase):
    def setUp(self):
        super(EventReminderTest, self).setUp()
        self.host = 'cornell.startuptreetest.co:8000'
        self.client = Client(HTTP_HOST=self.host)

        self.rsvp_member = StartupMemberFactory.create()
        self.rsvp_guest = GuestRSVPFactory.create()

        self.three_days_before_event = EventFactory.create(
            start=timezone.now() + timedelta(days=3.5),
            end=timezone.now() + timedelta(days=4),
            allow_guest_rsvp=True,
            send_emails_days_prior=3
        )
        self.day_before_event = EventFactory.create(
            start=timezone.now() + timedelta(days=1.5),
            end=timezone.now() + timedelta(days=1),
            allow_guest_rsvp=True
        )
        UniversityStyleFactory.create(
            _university=self.day_before_event.university,
            send_rsvp_reminders=True
        )
        self.update_event = EventFactory.create(
            start=timezone.now() + timedelta(days=3),
            end=timezone.now() + timedelta(days=3),
            allow_guest_rsvp=True,
            send_updates=True
        )

    def test_member_reminder_days_prior(self):
        self.three_days_before_event.attending_users.add(self.rsvp_member.user)
        check_upcoming(self.three_days_before_event.id)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].subject, 'An event you registered for is coming up')
        self.assertEqual(outbox[0].to, [self.rsvp_member.user.email])

    def test_guest_reminder_days_prior(self):
        self.three_days_before_event.guest_rsvps.add(self.rsvp_guest)
        check_upcoming(self.three_days_before_event.id)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].subject, 'An event you registered for is coming up')
        self.assertEqual(outbox[0].to, [self.rsvp_guest.email])


    def test_member_reminder_day_before(self):
        self.day_before_event.attending_users.add(self.rsvp_member.user)
        check_upcoming(self.day_before_event.id)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].subject, 'An event you registered for is coming up')
        self.assertEqual(outbox[0].to, [self.rsvp_member.user.email])

    def test_guest_reminder_day_before(self):
        self.day_before_event.guest_rsvps.add(self.rsvp_guest)
        check_upcoming(self.day_before_event.id)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].subject, 'An event you registered for is coming up')
        self.assertEqual(outbox[0].to, [self.rsvp_guest.email])
        

    def test_event_updated_noti(self):
        self.update_event.attending_users.add(self.rsvp_member.user)
        EventRSVPFactory.create(
            member=None, guest=self.rsvp_guest, rsvp_event=self.update_event
        )

        self.login_user(self.client, self.admin.user.email, self.password)

        data = {
            'event_name': self.update_event.name,
            'event_start_date': '01/01/2021',
            'event_start_time': '12:00 PM',
            'event_end_date': '01/01/2031',
            'event_end_time': '12:00 PM',
            'event-type': 'conference',
            'publish_checkbox': 'on',
            'update_checkbox': 'on',
            'send-update': 'true'
        }
        post_url = reverse('uni-manage-events-edit', kwargs={'event_id': self.update_event.event_id})
        self.client.post(post_url, data)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 2)
        self.assertTrue(outbox[0].subject == outbox[1].subject == \
            'Details for {} have changed'.format(self.update_event.name))
        if outbox[0].to == [self.rsvp_member.user.email]:
            self.assertEqual(outbox[1].to, [self.rsvp_guest.email])
        else:
            self.assertEqual(outbox[0].to, [self.rsvp_guest.email])
            self.assertEqual(outbox[1].to, [self.rsvp_member.user.email])

    def test_member_confirmation(self):
        self.login_user(self.client, self.regular.user.email, self.password)

        rsvp_data = {
            'event_id': self.day_before_event.event_id,
            'user_decision': 'yes'
        }
        self.client.post(reverse('event_rsvp'), rsvp_data)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].subject, "You're confirmed for {0} on {1}!".format(
            self.day_before_event.name,
            self.day_before_event.formatted_start_date))
        self.assertEqual(outbox[0].to, [self.regular.user.email])

        self.logout_user(self.client)

    def test_guest_confirmation(self):
        data = {
            'user_decision': 'yes',
            'fname': 'fname',
            'lname': 'lname',
            'email': 'test@email.com'
        }
        post_url = reverse('guest_rsvp', kwargs={'event_id': self.day_before_event.event_id})
        self.client.post(post_url, data)

        outbox = mail.outbox
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].subject, "Confirming your registration to {}".format(
            self.day_before_event.name))
        self.assertEqual(outbox[0].to, ['test@email.com'])
