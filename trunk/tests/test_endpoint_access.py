from auto_test_utility.utills import EndpointAccessTest
from django.urls.base import reverse
from app.user_types import *
from mentorship.factory.mentorship import MentorshipRequestFactory, MentorshipSessionFactory


class TrunkEndpointAccessTest(EndpointAccessTest):
    def test_mentorship_url(self):
        mentorship = self.default_mentorship
        request = MentorshipRequestFactory(requested_mentorship=mentorship)
        session = MentorshipSessionFactory(mentorship=mentorship)
        self.check_endpoint_permission(reverse('uni-dash-cal'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-pending-mentorships-table'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-pending-meetings-table'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-manage-mentorships-table'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-export-mentorship'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP), success_is_redirect=reverse('uni-export'))
        self.check_endpoint_permission(reverse('uni-dashboard-mentors'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-manage-mentors'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-manage-mentors-table'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-new-meeting', kwargs={'mentor_id': mentorship.mentor_id}),
                                       permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))  ## FIXME: the view requires require_post !! also the arguments.
        self.check_endpoint_permission(reverse('uni-new-meeting-select-time'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-new-meeting-review-meeting'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-manage-metrics'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-mentorship-approval'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-mentorship-approval-view', kwargs={'req_id': request.id}),
                                       permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-meeting-approval'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-meeting-approval-view', kwargs={'req_id': session.id}),
                                       permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-mentorship-decide'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP), is_post=True)
        self.check_endpoint_permission(reverse('uni-meeting-decide'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP), is_post=True)
        self.check_endpoint_permission(reverse('uni-mentorship-activities'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-mentorship-list'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))
        self.check_endpoint_permission(reverse('uni-sessions-details'), permissions=(USER_TYPE_ST_STAFF, ALL, MENTORSHIP))


    def test_team_mentorship_url(self):
        pass

    def test_application_url(self):
        # TODO
        pass