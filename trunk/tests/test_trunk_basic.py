from django.test import TestCase
from django.test.client import RequestFactory
from trunk.models import (
    University,
    UniversityStyle,
    School,
    Site,
    Major,
    get_uni_logo_url
)
from emailuser.modelforms import (
    UserEmailForm,
)
from emailuser.models import Group, PERMISSIONS
from emailuser.conf import UNIVERSITY
from trunk.modelforms import (
    UniversityForm,
)
from StartupTree.settings import ROOT_DOMAIN


class TrunkMethodUnitTest(TestCase):
    serialized_rollback = True

    def setUp(self):
        self.university = University.objects.get(short_name='cornell')
        self.name = self.university.name
        self.short_name = self.university.short_name

    def test_university_str(self):
        uni = University.objects.get(name=self.name)
        self.assertEqual(str(uni), self.name)

    def test_site_exists(self):
        site = Site.objects.get(name=self.name)
        self.assertEqual(site.domain, "{0}.{1}".format(self.short_name,
                                                       ROOT_DOMAIN))

    def test_add_school(self):
        uni = University.objects.get(name=self.name)
        name = "College of Hamsters"
        school = School.objects.create_school(name=name, university=uni)
        self.assertEqual(school, School.objects.get_by_name(name, uni))

        name2 = "college of HamsTers"
        school2 = School.objects.create_school(name=name2, university=uni)
        self.assertEqual(school2, School.objects.get_by_name(name, uni))

    def test_add_major(self):
        uni = University.objects.get(name=self.name)
        name = "College of Hamsters"
        school = School.objects.create_school(name=name, university=uni)
        self.assertEqual(school, School.objects.get_by_name(name, uni))
        cat = "Hamster Management"
        major = Major.objects.create_major(cat)
        self.assertEqual(major, Major.objects.get_by_name(cat))

        cat2 = "Hamster managemEnt"
        major2 = Major.objects.create_major(cat2)
        self.assertEqual(major2, Major.objects.get_by_name(cat))

    def test_get_uni_logo_url(self):
        uni = University.objects.get(name=self.name)
        args = {'logo': 'hamster.gif'}
        style = UniversityStyle.objects.update_style(uni, args)
        path = get_uni_logo_url(style, "hamster.gif")
        self.assertEqual(path, "media/uni/%s/logo/hamster.gif" % uni.short_name)

    def test_add_style(self):
        uni = University.objects.get(name=self.name)
        style = UniversityStyle.objects.update_style(uni)
        self.assertEqual(style, uni.style)
        args = {'logo': 'hamster.gif'}
        style = UniversityStyle.objects.update_style(uni, args)
        self.assertEqual(style, uni.style)


class TrunkFormTest(TestCase):
    serialized_rollback = True

    def setUp(self):
        data = {
            'email': "hello22@sunghopark.com",
            'email2': "hello22@sunghopark.com",
            'password1': '1234go',
            'password2': '1234go'
        }
        form = UserEmailForm(data)
        self.assertTrue(form.is_valid())
        self.user = form.save()
        self.assertEqual(repr(self.user), '<UserEmail: hello22@sunghopark.com>')
        self.user.add_group(UNIVERSITY)
        self.result_objects = [self.user]

    def test_universityform_success(self):
        u_data = {
            'name': 'Sungho University',
            'short_name': 'sungho',
        }
        u_form = UniversityForm(u_data)
        self.assertTrue(u_form.is_valid())
        university = u_form.save()
        self.result_objects.append(university)
        self.assertEqual(repr(university), '<University: Sungho University>')


class TrunkIOTest(TestCase):
    serialized_rollback = True

    def setUp(self):
        pass


class TrunkViewTest(TestCase):
    serialized_rollback = True

    def setUp(self):
        self.rq = RequestFactory()
