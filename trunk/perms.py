from django.db.models import Q, ObjectDoesNotExist
from django.http import JsonResponse, HttpResponseForbidden
from branch.models import StartupMember
from trunk.models import University, StartupTreeStaff, UniversityStaff, AccessLevel
from rest_framework.permissions import BasePermission
from StartupTree.strings import *
import json


def is_startuptree_staff(request, university=None):
    """
    Check if currently logged in user is StartupTree staff.
    Optional parameter university to check if the staff has been
    authorized to access the given university.
    :param request:
    :param university:
    :return: staff object if user is a valid staff. None otherwise.
    """
    user = request.user
    if not user.is_authenticated or user.startup_member.is_archived:
        return None
    try:
        staff = StartupTreeStaff.objects.get(staff_id=user.id)
        if university is None:
            return staff
        if university.startuptreestaff_set.filter(id=staff.id).exists():
            return staff
        return None
    except StartupTreeStaff.DoesNotExist:
        pass
    return None


def is_page_admin(request, page):
    if not request.user.is_authenticated or request.is_archived:
        return False
    member = request.current_member

    is_admin = False
    if page.admin.filter(id=member.id).exists():
        is_admin = True
    elif request.is_super or AccessLevel.GROUPS in request.access_levels:
        is_admin = True

    return is_admin



"""checks if user is university or university admin"""


def is_university_or_admin(request):
    current_site = request.current_site
    try:
        university = University.objects.get(site=current_site)
    except University.DoesNotExist:
        return False
    try:
        user = University.objects.get(admin_id=request.user.id)
        return True
    except:
        pass
    if request.user in university.uni_staffs.all():
        return True
    return False


#
# def is_university_user(request, root=False, boolean=False):
#     app_logger.debug("original method")
#     """
#     Check if currently logged in user is university user or startuptree staff
#     :param request:
#     :param root:
#     :param boolean:
#     :return:
#     """
#     is_university = False
#     university = None
#     user = request.user
#     if not user.is_authenticated:
#         return university
#     session_group = request.session.get('ws4redis:memberof')
#     if session_group:
#         session_group = request.session.get('ws4redis:memberof')
#
#     staff = is_startuptree_staff(request)
#     current_site = request.current_site
#     # first, check if user is a staff.
#     if staff is not None:
#         is_university = True
#         try:
#             university = staff.universities.get(site_id=current_site.id)
#         except ObjectDoesNotExist:
#             # current staff is not the member of current university
#
#             # check if it is landing page (site id is 1).
#             if current_site.id != 1:
#                 raise RuntimeError("Error code UXD002")
#     else:
#         if session_group is not None:
#             is_university = \
#                 (UNIVERSITY_NAME in session_group) or (STAFF_NAME in session_group)
#         else:
#             groups = user.groups.all()
#             for group in groups:
#                 is_university = False
#                 if isinstance(group, str):
#                     if group == UNIVERSITY_NAME:
#                         is_university = True
#                 elif group.permission == UNIVERSITY:
#                     is_university = True
#         if is_university:
#             try:
#                 universities = \
#                     University.objects.filter(Q(admin_id=user.id) | Q(staffs=user))
#                 for uni in universities:
#                     if uni.site == current_site or root:
#                         university = uni
#                         break
#                 if university is None:
#                     # User is not a universiy staff of the current site.
#                     is_university = False
#             except University.DoesNotExist:
#                 raise RuntimeError("Error code UXD001")
#     if boolean:
#         return is_university
#     return university


def is_university_user(request, root=False, boolean=False,
                       access_granted=AccessLevel.ALL,
                       is_get_access_level=False):
    """
    Check if currently logged in user is startuptree staff or university staff.
    Returns either the current university (if current user is staff), or None
    (if current user is not staff). These can also be represented as booleans
    (True if current user is staff, False if not).
    Can also optionally return the access level of the current user as an integer.
    :param request:
    :param root: deprecated? not being used
    :param boolean: whether or not a boolean value will be returned
    :param access_granted: access level to compare to current user's access level
    :param is_get_access_level: whether or not the access level will be returned
    :return:
    """
    user = request.user
    result = None
    access_levels = []
    try:
        if user.is_authenticated and not user.startup_member.is_archived:
            # below returns startuptreestaff object if current user is startuptreestaff
            # otherwise it returns none
            staff = is_startuptree_staff(request)
            university = request.university
            current_site = request.current_site
            if not university:
                try:
                    university = University.objects.get(site_id=current_site.id)
                except University.DoesNotExist:
                    pass
            if university:
                if staff is not None:  # current user is startuptreestaff
                    try:
                        # check if current user is authorized to access current university
                        # if yes, they are super user, and is_startuptree_staff for current uni is true
                        # below is commented out as currently all startuptreestaff can access all universities
                        # result = staff.universities.get(id=university.id)
                        result = university
                        access_levels = [AccessLevel.ALL]
                        request.is_startuptree_staff = True
                    except ObjectDoesNotExist:
                        # current staff is not authorized for current university
                        # check if we are on the landing page (site id is 1).
                        if current_site.id != 1:
                            raise RuntimeError("Error code UXD002")
                elif university.uni_staffs.filter(id=user.id).exists():  # current user is admin of current uni
                    try:
                        # get universitystaff object corresponding to current user and current uni
                        admin = UniversityStaff.objects.select_related('university').get(admin_id=user.id, university_id=university.id)
                        if admin.is_super:
                            admin_access_levels = [AccessLevel.ALL]
                        else:
                            admin_access_levels = admin.access_levels.all().values_list('name', flat=True)
                        # if we want to know current staff's access level,
                        # or if they have the access level that we specified,
                        # then retrieve the university they are admin of and their access level
                        if is_get_access_level or access_granted in admin_access_levels:
                            result = admin.university
                            access_levels = admin_access_levels
                    except UniversityStaff.DoesNotExist:
                        pass
    except ObjectDoesNotExist:
        pass

    if boolean:
        # if result is a university object, then user is startuptreestaff or universitystaff,
        # so return true. if result is none, then user is not any kind of staff, so return false.
        if result:
            result = True
        else:
            result = False
    if is_get_access_level:
        return result, access_levels
    return result


class IsUniversityUser(BasePermission):
    def has_permission(self, request, view):
        return is_university_user(request, boolean=True)


def is_admin_authorized(request__is_university, request__is_super,
                        request__access_levels, request__is_startuptree_staff, access_level):
    """access_level is a choicee in AccessLevel.ACCESS_LEVELS

    request__variable_name is request.variable_name

    Returns whether this is an admin that is allowed to access the feature
    corresponding to access_level
    """
    if request__is_startuptree_staff:
        return True
    else:
        if request__is_university:
            return request__is_super or access_level in request__access_levels
        else:
            return False


def is_tag_restricted_admin(request__university, request__is_super, request__is_startuptree_staff):
    """
    A tag-restricted admin is any non-super admin on a platform that
        "Only allow admins to manage profiles with which they share a tag".

    request__variable_name is request.variable_name


    NOTE ABOUT RECENT CHANGES:

        Prior to October 2020, it was decided that super-admins be considered tag-restricted, in order to
        appease an early request from Columbia. Columbia no longer requires this.

        On October 2020, StartupTree internally decided that super-admins should NOT be considered tag-restricted.

        Many parts of our code that assume the old definition (before this helper method was made) will need
        to be refactored to take the new definition into account. This refactor is a low priority because
        so few schools actually use the tag restriction feature. However, any part of the codebase you come across
        that evaluates tag restriction permission with the outdated definition, please replace with a call to
        this method.

    """
    is_gte_super = request__is_super or request__is_startuptree_staff
    return request__university.style.restrict_tagged_admins and not is_gte_super


def is_admin_can_edit_venture(request__university: University, request__is_university, request__is_super,
                              request__access_levels, request__is_startuptree_staff):
    """request__variable_name is request.variable_name

    Returns whether this is an admin that can edit ventures."""
    # return request__university.style.allow_admin_jobs \
    #     and is_admin_authorized(request__is_university, request__is_super, request__access_levels,
    #                             request__is_startuptree_staff, AccessLevel.JOBS)
    return is_admin_authorized(request__is_university, request__is_super, request__access_levels,
                               request__is_startuptree_staff, AccessLevel.JOBS)


def is_admin_authorized_mentorship(request_data, var_name,
                                   request__is_university, request__is_super,
                                   request__access_levels, request__is_startuptree_staff):
    """
    If request is team mentorship, verifies that admin has access to team mentorship.
    If request is single-mentorship, verifies that admin has access to single mentorship.

    return format:
        {
            is_team: <bool or None>
            is_authorized: <bool or None>
            invalid_response: <HttpResponse if invalid/unauthorized else None>
        }

        If invalid_response is None, is_team is guaranteed to be defined.

    input format:

        request_data is request.POST or request.GET

        var_name is the variable name of the 'is_team' variable passed

        request__<variable_name> is request.<variable_name>
    """
    try:
        is_team = json.loads(request_data.get(var_name))
        if not isinstance(is_team, bool):
            raise TypeError
    except TypeError:
        payload = {
            PAYLOAD_STATUS: INVALID_REQUEST_CODE,
            PAYLOAD_MSG: 'Invalid Request Param Format'
        }
        return {
            'is_team': None,
            'is_authorized': None,
            'invalid_response': JsonResponse(payload, status=INVALID_REQUEST_CODE)
        }

    access_level = AccessLevel.MENTORSHIP if not is_team else AccessLevel.TEAM_MENTORSHIP
    is_authorized = is_admin_authorized(request__is_university, request__is_super,
                                        request__access_levels, request__is_startuptree_staff, access_level)

    invalid_response = None if is_authorized else HttpResponseForbidden()

    # Logically equivalent: invalid_response is None => is_team is boolean
    assert (invalid_response is not None) or isinstance(is_team, bool)

    return {
        'is_team': is_team,
        'is_authorized': is_authorized,
        'invalid_response': invalid_response
    }


def is_view_admin_only_startup_custom_questions(request, is_project):
    """
    Whether the current user of the request is an admin/startuptreestaff
    that is allowed to view "admin-only" custom questions of the startup.

    Main Use: The result of this call is meant to be used as 'is_uni_admin' argument to
    forms.serializer.forms.custom_startup_dict()

    For all schools (except Temple), returns true if user is a platform startup-level admin or startuptreestaff.

    SPECIAL CASE FOR TEMPLE UNIVERSITY:
        Venture Custom Questions that are "Admin-Only" should only be accessible
        by admins WHO HAVE THE TECH COMMERCIALIZATION TAG (whose ID is specified by
        StartupTree.strings.TEMPLE_TECH_COMMERCIALIZATION_TAG_ID). (Of course, startuptreestaff should have access
        regardless.)

    is_project is whether we are dealing with projects or ventures (Startup.is_project). (for access-level purposes)
    """
    uni: University = request.university
    access_level = AccessLevel.PROJECTS if is_project else AccessLevel.VENTURES
    is_startup_level_admin = is_admin_authorized(request.is_university, request.is_super,
                                                 request.access_levels, request.is_startuptree_staff, access_level)

    if uni.short_name != TEMPLE_UNI_SHORTNAME:
        return is_startup_level_admin

    else:
        if not is_startup_level_admin:
            return False
        else:
            if request.is_startuptree_staff:
                return True
            else:
                # This is a temple university admin, with venture/project access level.
                admin_member: StartupMember = request.current_member
                return admin_member.labels.filter(id=TEMPLE_TECH_COMMERCIALIZATION_TAG_ID).exists()
