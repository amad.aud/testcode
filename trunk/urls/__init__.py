from django.conf.urls import url, include
from forms.views import manage_forms, reorder_forms
import trunk.views as tv
from trunk.views import eprogram as ep
from trunk.views import export as trunk_export
from venture import views as vv

urlpatterns = [
    url(r'^dashboard$', tv.Home.as_view(), name='uni-dashboard'),
    url(r'^panel$', tv.HomeLegacy.as_view(), name='uni-dashboard-legacy'),  # for backward compatibility
    url(r'^dashboard/filter/(?P<days>[0-9]+)$', tv.HomeFilterDays.as_view(),
        name='uni-dashboard-filter'),
    url(r'^my-pages$', tv.Home.as_view(), name='uni-admin-pages'),

    url(r'^manage/',
        include([
            url(r'^events/', include('trunk.urls.events')),
            url(r'^competitions/', include('trunk.urls.competition')),
            url(r'^applications/', include('trunk.urls.application')),
            url(r'^surveys/', include('trunk.urls.survey')),
            url(r'^mentorship/', include('trunk.urls.mentorship')),
            url(r'^jobs/dashboard', vv.OpportunitiesDashboard.as_view(), name='uni-manage-jobs'),
            url(r'^jobs/dashboard/detail', vv.OpportunitiesDashboard.as_view(), name='uni-manage-jobs-detail'),
            url(r'^templates', ep.ManageTemplates.as_view(), name='uni-manage-templates'),
        ])),

    url(r'^promote/', include('trunk.urls.eprogram')),

    url(r'^metrics/',
        include([
            url(r'^export$', tv.ExportView.as_view(), name='uni-export'),
            url(r'^export/(?P<export_id>[0-9]+)$', tv.ExportView.as_view(), name='uni-export-xlsx'),
            url(r'^export/general$', trunk_export.GeneralExportRequest.as_view(), name='uni-export-general'),
            url(r'^export/users$', trunk_export.UserGeneralExportRequest.as_view(), name='uni-export-users'),
            url(r'^export/users/funding$',
                trunk_export.UserFundingExportRequest.as_view(), name='uni-export-user-funding'),
            url(r'^export/judge-review$',
                trunk_export.JudgeExportRequest.as_view(), name='uni-export-judge-review'),
            url(r'^reports/', include('trunk.urls.reports')),
        ])),

    url(r'^profiles/',
        include([
            url(r'^tags$', ep.ManageTags, name='uni-manage-tags'),
            url(r'^forms/', include([
                url(r'^$', manage_forms, name='uni-manage-forms'),
                url(r'^(?P<form_name>\w+)$', manage_forms, name='uni-manage-forms'),
                url(r'^(?P<form_id>[0-9]+)/reorder_questions$', reorder_forms, name='uni-reorder-forms'),
                url(r'^(?P<form_name>\w+)/(?P<event_id>[^/]+)$', manage_forms, name='uni-manage-forms')
            ])),
            url(r'^ventures$', ep.ManageVentures.as_view(), name='uni-manage-ventures'),
            url(r'^projects$', ep.ManageProjects.as_view(), name='uni-manage-projects'),
            url(r'^decision$', ep.StartupDecision.as_view(), name='startup-decision'),
        ])),

    url(r'^people/',
        include([
            url(r'^users$', ep.ManageUsers.as_view(), name='uni-manage-users'),
            url(r'^invites/', ep.ManageInvites.as_view(), name='uni-manage-invites'),
            url(r'^admins$', ep.ManageAdmins.as_view(), name='uni-manage-admins'),
            url(r'^admins/delete$', tv.DeleteAdmin.as_view()),
            url(r'^admins/resend$', tv.ResendAdmin.as_view()),
        ])),

    url(r'^platform/',
        include([
            url(r'^customize/', include('trunk.urls.customization')),
            url(r'^pages/pages$', ep.ManagePages.as_view(), name='uni-manage-pages'),
            url(r'^pages/tools/', ep.ManageTools.as_view(), name='uni-manage-tools'),
        ])),
]
