from django.conf.urls import url
from trunk.views import reports as tv


urlpatterns = [
    url(r'^dashboard$', tv.ManageReports.as_view(), name='uni-manage-reports'),
    url(r'^create$', tv.CreateReport.as_view(), name='uni-create-report'),
    url(r'^(?P<r_id>[^/]+)/edit$',
        tv.EditReport.as_view(), name='uni-edit-report'),
    url(r'^(?P<r_id>[^/]+)/dashboard$',
        tv.ReportDashboard.as_view(), name='uni-manage-report'),
    url(r'^(?P<r_id>[^/]+)/template$',
        tv.GenerateReportFromTemplate.as_view(), name='report-generate-template'),
    url(r'^(?P<r_id>[^/]+)/reorder_questions$',
        tv.ReportQuestionReorder.as_view(), name='uni-reorder-report-questions'),
    url(r'^(?P<r_id>[^/]+)/data$', tv.EnterData.as_view(), name='uni-enter-data'),
    url(r'^export/(?P<r_id>[^/]+)$',
        tv.ExportData.as_view(), name='uni-export-report-data'),
    url(r'^archive$', tv.ArchiveReport.as_view(), name='uni-archive-report'),
    url(r'^delete$', tv.DeleteReport.as_view(), name='uni-delete-report'),
]
