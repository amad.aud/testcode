from django.conf.urls import url
from trunk.views import surveys as tv
from branch.views import surveys as bv


urlpatterns = [

    url(r'^list$',
        tv.ManageSurveys.as_view(), name='uni-manage-surveys'),
    url(r'^create',
        tv.CreateSurvey.as_view(), name='uni-create-survey'),
    url(r'^(?P<event_id>[^/]+)/edit$',
        tv.EditSurvey.as_view(), name='uni-edit-survey'),
    url(r'^(?P<event_id>[^/]+)/questions$',
        tv.ManageQuestions.as_view(), name='uni-edit-survey-question'),
    url(r'^(?P<event_id>[^/]+)/dashboard$',
        tv.SurveyDashboard.as_view(), name='uni-manage-surveys-view'),
    url(r'^(?P<event_id>[^/]+)/respondents$',
        tv.ViewResps.as_view(), name='uni-manage-respondents'),
    url(r'^(?P<event_id>[^/]+)/respondents/(?P<app_url_name>[^/]+)$',
        tv.ViewSurvey.as_view(), name='uni-view-survey'),
    url(r'^(?P<event_id>[^/]+)/submit_response/(?P<app_url_name>[^/]+)$',
        tv.AdminSubmitRespondent.as_view(), name='uni-submit-respondent'),
    url(r'^(?P<event_id>[^/]+)/export/respondents$',
        tv.ExportSurveyResponses.as_view(), name='uni-export-all-response'),
    url(r'^(?P<event_id>[^/]+)/export/respondents/(?P<app_url_name>[^/]+)$',
        tv.ExportSurveyResponses.as_view(), name='uni-export-response'),
    url(r'^(?P<event_id>[^/]+)/export/attachments$',
        tv.ExportSurveyAttachments.as_view(), name='uni-export-all-survey-attachments'),
    url(r'^(?P<event_id>[^/]+)/invite_users$',
        bv.invite_users, name='surveys-invite-users'),
    url(r'^(?P<event_id>[^/]+)/template$',
        tv.GenerateSurveyFromTemplate.as_view(), name='surveys-generate-template'),
    url(r'^(?P<event_id>[^/]+)/reorder_questions$',
        tv.SurveyQuestionReorder.as_view(), name='uni-reorder-survey-questions'),
    url(r'^export/(?P<event_id>[^/]+)$',
        tv.ExportSurvey.as_view(), name='uni-export-survey'),
    url(r'^delete',
        tv.DeleteSurvey.as_view(), name='uni-delete-survey'),
    url(r'^survey/(?P<event_id>[^/]+)$',
        bv.show_survey, name='survey'),
]
