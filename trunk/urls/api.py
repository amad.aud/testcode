#rest_framework integration trial begin
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from trunk.views.api import UniversityList

#explicitly linking the Class-based View with URL
uni_list = UniversityList.as_view()

urlpatterns = [
	url(r'^list/$', uni_list, name='uni-list'),
]
#rest_framework integration trial end