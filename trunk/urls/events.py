from django.conf.urls import url
from django.urls import path
from trunk.views import events as tv
from venture import views as venture_views


urlpatterns = [
    url(r'^dashboard$', tv.ManageEvents.as_view(), name='uni-manage-events'),
    url(r'^create$', tv.CreateEvent.as_view(), name='uni-create-event'),
    path('<uuid:event_id>/edit',
        tv.EditEvent.as_view(), name='uni-manage-events-edit'),
    path('<uuid:event_id>/dashboard',
        tv.EventDashboard.as_view(), name='uni-manage-events-view'),
    path('<uuid:event_id>/rsvps-and-checkins',
        tv.ManageRSVPCheckins.as_view(), name='uni-manage-events-rsvp-checkin'),
    path('<uuid:event_id>/rsvps-and-checkins/edit',
        tv.EditRSVPCheckins.as_view(), name='uni-edit-rsvp-checkin'),
    path('<uuid:event_id>/customize-emails',
        tv.CustomizeEventEmails.as_view(), name='uni-edit-event-emails'),

    path('<uuid:event_id>/invite_users',
        tv.invite_users, name='events-invite-users'),
    path('<uuid:event_id>/bulk_invite',
        tv.BulkInvite.as_view(), name='uni-manage-events-bulk-invite'),

    path('<uuid:event_id>/export-rsvps',
        tv.ExportRSVPs.as_view(), name='uni-manage-export-rsvp'),

    path('<uuid:event_id>/<str:publish>',
        tv.PublishEvent.as_view(), name='uni-manage-events-publish'),
    url(r'^checkin/(?P<event_id>[^/]+)/(?P<user_id>[0-9]+)$',
        tv.AdminCheckin.as_view(), name='admin-checkin'),
    url(r'^checkin/(?P<event_id>[^/]+)/guest$',
        tv.AdminCheckinGuest.as_view(), name='admin-checkin-guest'),
    url(r'^checkin/(?P<event_id>[^/]+)/guest/(?P<guest_id>[0-9]+)$',
        tv.AdminCheckinGuest.as_view(), name='admin-checkin-guest'),
    url(r'^rsvp/', tv.AdminRSVP.as_view(),
        name='admin-rsvp'),
    url(r'^(?P<action>[\w-]+)/(?P<event_id>[^/]+)/(?P<user_id>[0-9]+)$',
        tv.ArchiveUser.as_view(), name="archive-event-user"),
    url(r'^(?P<action>[\w-]+)/(?P<event_id>[^/]+)/guest/(?P<guest_id>[0-9]+)$',
        tv.ArchiveGuest.as_view(), name="archive-event-guest"),
    url(r'^stat$', tv.EventMetrics.as_view(), name='uni-stat-event'),
    url(r'^delete$', tv.DeleteEvent.as_view(), name='uni-delete-event'),
    url(r'^shared_url/(?P<event_id>[^/]+)$',
        tv.get_shared_url, name='get-shared-url'),
    url(r'^decision$', tv.EventDecision.as_view(), name='event-decision'),

]
