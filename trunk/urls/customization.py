from django.conf.urls import url, include
from trunk.views import customization as cs


urlpatterns = [
    url(r'^program$', cs.customize_general, name='uni-customize-general'),
    url(r'^design$', cs.customize_design, name='uni-customize-design'),
    url(r'^pages$', cs.customize_pages, name='uni-customize-pages'),
    url(r'^roles$', cs.customize_roles, name='uni-customize-roles'),
    url(r'^text$', cs.customize_text, name='uni-customize-text'),
    url(r'^signup$', cs.customize_signup, name='uni-customize-signup'),
    url(r'^integration$', cs.customize_integration, name='uni-customize-integration'),
    url(r'^platform$', cs.customize_platform, name='uni-customize-platform'),
    url(r'^api$', cs.manage_api, name='uni-manage-api'),
    url(r'^subscription$', cs.ModifySubscription.as_view(), name='uni-subscription'),
]
