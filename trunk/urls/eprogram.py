from django.conf.urls import url, include
from trunk.views.eprogram import ManageTicket, Announcement


urlpatterns = [

    # url(r'^tickets/', include([
    #     url(r'^$',
    #         ManageTicket.as_view(),
    #         name='uni-manage-tickets'),
    #     url(r'^(?P<ticket_id>[0-9]+)$',
    #         ManageTicket.as_view(),
    #         name='uni-view-ticket')
    # ])),
    url(r'^announcement/', include([
        url(r'^$',
            Announcement.as_view(),
            name='uni-mass-message'),
        url(r'^(?P<msg_id>[0-9]+)$',
            Announcement.as_view(),
            name='uni-view-messages'),
        url(r'^new$',
            Announcement.as_view(),
            name='uni-new-mass-msg'),
    ]))
]
