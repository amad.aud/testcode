# Django Imports
from django.contrib.sites.models import Site
from django.db import IntegrityError, transaction
from django.utils import timezone

# StartupTree Imports
from activity.models import InvitedMember
from branch.models import (
    StartupMember,
    Label,
    UniversityAffiliation
)
from branch.modelforms import EmailForm
from emailuser.authenticate import get_admin_home_url
from emailuser.conf import (
    PROTOCOL,
    EMAIL_INVITATION_URL
)
from emailuser.engine import render_send_email
from emailuser.models import UserEmail
from forms.models import RubricAnswer, MemberApplicant
from hamster.config import Roborovski
from hamster.tasks import send_invitation
from StartupTree.loggers import rq_logger, app_logger
from trunk.export import (
    GeneralExport,
    MentorshipExport,
    TeamMentorshipExport,
    UserExport,
    JudgeExport,
)
from trunk.models import (
    AdminCalSyncOption,
    Event,
    EventRSVP,
    Judge,
    JudgeReview,
    University,
    UniversityExportHistory,
    UniversityStaff,
)

# Library Imports
import xlsxwriter
import datetime
from io import BytesIO
import uuid


@Roborovski.task
@transaction.atomic
def send_staff_invitation(
        eu_id,
        name,
        current_site_id,
        invitor="StartupTree",
        university="StartupTree",
        program_name=None):
    """Send invitation email to the email user who was just created by
    someone else on the platform.
    :param eu_id:
    :param name:
    :param current_site_id:
    :param invitor:
    :param university:
    :param program_name:
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    eu = UserEmail.objects.select_for_update().get(id=int(eu_id))
    tmp = eu.generate_confirmation_key()
    # is_admin = False
    if invitor == university:
        # is_admin = True
        if program_name:
            invitor = program_name
    if program_name is None:
        program_name = university
    eu.set_password(tmp)
    protocol = PROTOCOL
    url = "{0}://{1}/{2}".format(
        protocol,
        current_site.domain,
        EMAIL_INVITATION_URL.format(eu.email, tmp)
    )

    uni = University.objects.get(site=current_site)
    if uni.program_name:
        program_name = uni.program_name

    admin = UniversityStaff.objects.get(admin_id=eu.id, university_id=uni.id)
    access_levels = admin.access_levels.all()
    is_super = admin.is_super

    subject = "Admin Invitation to join {0}'s entrepreneurship community on StartupTree".format(program_name)
    template = "email_templates/trunk/Staff_Invite_to_join_StartupTree.html"

    ctd = {
        "invite_url": url,
        "year": datetime.datetime.now().year,
        "new_staff_first_name": name,
        "invitor": invitor,
        "uni_name": university,
        "subject": subject,
        "access_levels": access_levels,
        "is_super": is_super,
        "is_existing_user": False
    }

    render_send_email(subject, template, ctd, current_site, eu.email)
    eu.confirmation_sent = timezone.now()
    eu.save()
    rq_logger.debug("Send admin invitation")


@Roborovski.task
def send_staff_notification(
        eu_id,
        name,
        current_site_id,
        invitor="StartupTree",
        university="StartupTree",
        program_name=None):
    """
    Send notification that exisiting user was added as a staff member
    :param eu_id: id of StartupMember object
    :param name:
    :param current_site_id:
    :param invitor:
    :param university:
    :param program_name:
    :return:
    """
    current_site = Site.objects.get(id=int(current_site_id))
    eu = StartupMember.objects.get(id=int(eu_id))
    if invitor == university:
        if program_name:
            invitor = program_name
    if program_name is None:
        program_name = university
    uni = University.objects.get(site=current_site)
    if uni.program_name:
        program_name = uni.program_name

    admin = UniversityStaff.objects.get(admin_id=eu.user_id, university_id=uni.id)
    access_levels = admin.access_levels.all()
    access_levels_list = access_levels.values_list('name', flat=True)
    is_super = admin.is_super

    protocol = PROTOCOL
    urlpath = current_site.domain

    if is_super:
        url = '{0}://{1}/university/panel'.format(
            protocol,
            urlpath
        )
    else:
        url = get_admin_home_url(True, access_levels_list, uni)

    subject = "Admin Invitation to join {0}'s entrepreneurship community on StartupTree".format(program_name)
    template = "email_templates/trunk/Staff_Invite_to_join_StartupTree.html"

    ctd = {
        "invite_url": url,
        "year": datetime.datetime.now().year,
        "new_staff_first_name": name,
        "invitor": invitor,
        "uni_name": university,
        "subject": subject,
        "access_levels": access_levels,
        "is_super": is_super,
        "is_existing_user": True
    }

    render_send_email(subject, template, ctd, current_site, eu.user.email)
    rq_logger.debug("sending admin notification.")


@Roborovski.task
def judge_export_task(history_id):
    history = UniversityExportHistory.objects.get(id=history_id)
    JudgeExport(history).export()


@Roborovski.task
def general_export_task(history_id):
    history = UniversityExportHistory.objects.get(id=history_id)
    GeneralExport(history).export()


@Roborovski.task
def user_funding_export_task(history_id):
    history = UniversityExportHistory.objects.get(id=history_id)
    UserExport(history, is_funding=True).export()


@Roborovski.task
def user_general_export_task(history_id):
    history = UniversityExportHistory.objects.get(id=history_id)
    UserExport(history, is_funding=False).export()


@Roborovski.task
def mentorship_export_task(history_id):
    history = UniversityExportHistory.objects.get(id=history_id)
    MentorshipExport(history).export()


@Roborovski.task
def team_mentorship_export_task(history_id):
    history = UniversityExportHistory.objects.get(id=history_id)
    TeamMentorshipExport(history).export()


@Roborovski.task
def create_user_invite(invitee_email, existing_emails, invalid_emails,
                       invitee_first, invitee_last, univ_id, tag_ids=None, total=1,
                       guest_id=None, send_invite=False, is_delay=False,
                       invitor_name='', email_subject = '', email_body=''):
    """
            Input: invitee_email as string, existing_emails list, invalid_emails list
            Checks that invitee_email is valid and creates new UserEmail/StartupMember/InvitedMember
            objects as well as adding to the appropriate lists for display on frontend
            """
    """
    @TAGINVITE
    for create_invite function, we want it to also have extra argument, "tags", which
    is a list of Label object
    """
    univ = University.objects.get(id=univ_id)
    tags = []
    if tag_ids:
        tags = Label.objects.filter(id__in=tag_ids)
    existing = False
    invite = False
    if invitee_email:
        eform = EmailForm({'email': invitee_email})
        if eform.is_valid():
            invitee_email = invitee_email.lower()
            try:
                eu = UserEmail.objects.get(email__iexact=invitee_email)
                created = False
            except UserEmail.DoesNotExist:
                eu = UserEmail.objects.create(email=invitee_email)
                eu.set_password(str(uuid.uuid4()))
                eu.save()
                created = True
            try:
                member = StartupMember.objects.get_user(user=eu)
            except StartupMember.DoesNotExist:
                member = None
            if created or member is None:
                try:
                    with transaction.atomic():
                        member = \
                            StartupMember.objects.create_sm(
                                invitee_first,
                                invitee_last,
                                user_email=eu)
                        member.universities.add(univ)
                        if guest_id is not None:
                            try:
                                guest = EventRSVP.objects.get(id=guest_id, guest__isnull=False)
                                guest_aff = guest.affiliation
                                new_aff = UniversityAffiliation.objects.create(member=member, university=univ)
                                if guest_aff == EventRSVP.STUDENT:
                                    new_aff.affiliation = UniversityAffiliation.UNIVERSITY_STUDENT
                                elif guest_aff == EventRSVP.ALUMNI:
                                    new_aff.affiliation = UniversityAffiliation.UNIVERSITY_ALUMNI
                                elif guest_aff == EventRSVP.FACULTY:
                                    new_aff.affiliation = UniversityAffiliation.UNIVERSITY_FACULTY
                                elif guest_aff == EventRSVP.STAFF:
                                    new_aff.affiliation = UniversityAffiliation.UNIVERSITY_STAFF
                                elif guest_aff == EventRSVP.NOT_AFFILIATED:
                                    new_aff.affiliation = UniversityAffiliation.NOT_UNIVERSITY_PERSON
                                # for backward compatibility
                                elif guest_aff == EventRSVP.FACULTY_STAFF:
                                    new_aff.affiliation = UniversityAffiliation.UNIVERSITY_FACULTY
                                    UniversityAffiliation.objects.create(member=member, university=univ,
                                                                         affiliation=UniversityAffiliation.UNIVERSITY_STAFF)
                                new_aff.save()
                                rsvp_guest = guest.guest
                                rsvp_guest.is_invited = True
                                rsvp_guest.save()
                            except EventRSVP.DoesNotExist:
                                invalid_emails.append(invitee_email)
                        if not InvitedMember.objects.filter(member_id=member, invited_platform_id=univ.id).exists():
                            invite = True
                            InvitedMember.objects.create(
                                member=member,
                                invited_platform=univ
                            )
                        member.labels.add(*tags)
                except IntegrityError:
                    existing = True
                    pass
                """
                @TAGINVITE
                So basically, after member get created, we want to
                member.labels.add(*tags_list)
                notice *. This allows us to add list to manytomany instead of adding
                objects one by one.
                https://stackoverflow.com/questions/400739/what-does-asterisk-mean-in-python
                """
            else:
                try:
                    guest = EventRSVP.objects.get(id=guest_id)
                    if guest.guest:
                        rsvp_guest = guest.guest
                        rsvp_guest.is_invited = True
                        rsvp_guest.save()
                except:
                    pass
                if member.universities.filter(id=univ.id).exists():
                    if not is_delay:
                        existing_emails.append(eu.email)
                else:
                    member.universities.add(univ)
                    if not InvitedMember.objects.filter(member_id=member, invited_platform_id=univ.id).exists():
                        invite = True
                        InvitedMember.objects.create(
                            member=member,
                            invited_platform=univ
                        )
                    member.labels.add(*tags)
            if send_invite and invite:
                transaction.on_commit(lambda: send_invitation.delay(
                    eu.id,
                    invitee_first,
                    univ.site.id,
                    invitor_name,
                    univ.name,
                    univ.program_name,
                    True,
                    email_subject,
                    email_body,
                    False,
                    guest_id
                ))
        else:
            if not is_delay:
                invalid_emails.append(invitee_email)

    if not is_delay:
        # msg
        num_sent = total - len(invalid_emails) - len(existing_emails)
        if num_sent == 1 and not existing:
            msg = "1 Invitation is now being processed. This may take up to 1 hour depending on the length of the invite list."
        elif num_sent == 0:
            msg = "Oops! Unable to process invitation(s)."
        elif existing:
            msg = msg = "This email is already associated with a StartupTree Account."
        else:
            msg = str(
                num_sent) + " Invitations are now being processed. This may take up to 1 hour depending on the length of the invite list."
        return num_sent, msg


@Roborovski.task
def create_user_invite_bulk(invitee_lists, univ_id, tag_ids=None, is_guest=False, total=1,
                       send_invite=False, is_delay=False, invitor_name='', email_subject='', email_body=''):
    for invitee in invitee_lists:
        invitee_first = invitee[0]
        invitee_last = invitee[1]
        invitee_email = invitee[2]
        if is_guest:
            guest_id = invitee[3]
        else:
            guest_id = None
        create_user_invite(
            invitee_email,
             [],
             [],
             invitee_first,
             invitee_last,
             univ_id,
             tag_ids,
             guest_id=guest_id,
             total=total,
             send_invite=send_invite,
             is_delay=is_delay,
             invitor_name=invitor_name,
             email_subject=email_subject,
             email_body=email_body)


def init_admin_cal_sync(uni_staff: UniversityStaff, sync_type: int):
    transaction.on_commit(lambda: init_admin_cal_sync_task.delay(uni_staff.id, sync_type))


def remove_admin_cal_sync(uni_staff: UniversityStaff, sync_type: int):
    transaction.on_commit(lambda: remove_admin_cal_sync_task.delay(uni_staff.id, sync_type))


@Roborovski.task
def init_admin_cal_sync_task(uni_staff_id, sync_type):
    uni_staff = UniversityStaff.objects.get(id=uni_staff_id)
    sync_util = AdminCalSyncOption.get_sync_util(uni_staff, sync_type)
    sync_util.init_sync()


@Roborovski.task
def remove_admin_cal_sync_task(uni_staff_id, sync_type):
    uni_staff = UniversityStaff.objects.get(id=uni_staff_id)
    sync_util = AdminCalSyncOption.get_sync_util(uni_staff, sync_type)
    sync_util.remove_sync()
