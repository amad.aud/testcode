def pluralize(word):
    if word.endswith('y'):
        plural_word = word[:-1]
        plural_word += 'ies'
    elif word.endswith(('s', 'x', 'z', 'ch', 'sh')):
        plural_word = word + 'es'
    else:
        plural_word = word + 's'
    return plural_word

def get_new_session_mentee_email_body(uni_style):
    mentorship_word = uni_style.mentorship_word
    mentor_word = uni_style.mentor_word
    mentors_word = pluralize(mentor_word).title()

    return "<span>Your {0} session has been scheduled with _mentor_name_ for _meeting_datetime_. Time zone is based on {1} profile settings. Please convert to your local time if needed.</span>\
    <br><br>\
    <span>Here is what you would like to discuss at this meeting: _meeting_topic_</span>\
    <br><br>\
    <span>Please note, this meeting will take place via _meeting_type_. On the day of the meeting, contact your {1} at _meeting_location_.</span>\
    <br><br>\
    <span>Make sure to be respectful of your {1}'s time and arrive early. We recommend adding this meeting to your calendar right away by using one of the links below.</span>\
    <br><br>\
    <span>If you cannot make your meeting due to an unavoidable conflict, please go to My {2} on StartupTree to cancel the upcoming meeting, and then send a message to your {1} through the platform with your reason.</span>".format(
        mentorship_word,
        mentor_word,
        mentors_word
    )

def get_new_session_mentor_email_body(uni_style):
    mentorship_word = uni_style.mentorship_word
    mentee_word = uni_style.mentee_word
    mentees_word = pluralize(mentee_word)
    capital_mentees_word = mentees_word.title()
    mentor_word = uni_style.mentor_word
    mentors_word = pluralize(mentor_word)

    return "<span>A new {0} session with _mentee_name_ has been approved for _meeting_datetime_. We recommend adding this meeting to your calendar using one of the links below.</span>\
    <br><br>\
    <span>Here is what your {1} would like to discuss at this meeting: _meeting_topic_</span>\
    <br><br>\
    <span>This meeting will take place via _meeting_type_, as you indicated on your schedule. On the day of the meeting, your {1} will contact you at _meeting_location_.</span>\
    <br><br>\
    <span>All of your {2} and past interactions can be accessed through My {3} on StartupTree. Any notes shared or goals assigned from the My {3} section will be visible to Admins and helpful to us in better serving both {4} and {2}!</span>\
    <br><br>\
    <span>If you need to reach your {1} for any reason, you can message them on the platform or email them at _mentee_email_.</span>\
    <br><br>\
    <span>Thank you for being a valuable {5} in our growing community!</span>".format(
        mentorship_word,
        mentee_word,
        mentees_word,
        capital_mentees_word,
        mentors_word,
        mentor_word
    )

def get_session_reminder_mentee_email_body(uni_style):
    mentorship_word = uni_style.mentorship_word
    mentor_word = uni_style.mentor_word
    mentors_word = pluralize(mentor_word).title()

    return "<span>Your {0} session with _mentor_name_ is scheduled for _meeting_datetime_. Time zone is based on {1} profile settings. Please convert to your local time if needed.</span>\
    <br><br>\
    <span>Here is what you would like to discuss at this meeting: _meeting_topic_</span>\
    <br><br>\
    <span>This meeting will take place via _meeting_type_. At the time of the meeting, contact your {1} at _meeting_location_.</span>\
    <br><br>\
    <span>Make sure to be respectful of your {1}'s time and arrive early.</span>\
    <br><br>\
    <span>If you cannot make your meeting due to an unavoidable conflict, please go to My {2} on StartupTree to cancel the upcoming meeting, and then send a message to your {1} through the platform with your reason.</span>".format(
        mentorship_word,
        mentor_word,
        mentors_word
    )

def get_session_reminder_mentor_email_body(uni_style):
    mentorship_word = uni_style.mentorship_word
    mentee_word = uni_style.mentee_word
    mentees_word = pluralize(mentee_word)
    capital_mentees_word = mentees_word.title()
    mentor_word = uni_style.mentor_word
    mentors_word = pluralize(mentor_word)

    return "<span>Your {0} session with _mentee_name_ is scheduled for _meeting_datetime_.</span>\
    <br><br>\
    <span>Here is what your {1} would like to discuss at this meeting: _meeting_topic_</span>\
    <br><br>\
    <span>This meeting will take place via _meeting_type_. Your {1} will contact you at _meeting_location_.</span>\
    <br><br>\
    <span>All of your {2} and past interactions can be accessed through My {3} on StartupTree. Any notes shared or goals assigned from the My {3} section will be visible to Admins and helpful to us in better serving both {4} and {2}!</span>\
    <br><br>\
    <span>If you need to reach your {1} for any reason, you can message them on the platform or email them at _mentee_email_.</span>".format(
        mentorship_word,
        mentee_word,
        mentees_word,
        capital_mentees_word,
        mentors_word
    )
