from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models import Case, When, QuerySet
from StartupTree.strings import DEFAULT_PRIMARY_COLOR
from StartupTree.utils import make_thumb
from datetime import datetime
import os
import pytz
from typing import Optional
import uuid
from urllib import parse


def format_datetime(input_dt, tz='US/Eastern', fmt=None):
    """pytz in one string of pytz.common_timezones
    fmt is a valid input for strftime.

    Default ex: "2020-07-21 16:54:36 EDT"
    """
    if fmt is None:
        fmt = '%F %T %Z'
    return input_dt.astimezone(pytz.timezone(tz)).strftime(fmt)


def format_datetime_12hr(input_dt, tz='US/Eastern'):
    """Eg "2020-07-21 05:10:21 PM EDT" """
    return format_datetime(input_dt, tz, "%F %r %Z")


def format_datetime_english(input_dt: datetime, tz='US/Eastern'):
    """
    EG: 'Thu, Dec 3 at 05:33 am'
    """
    return format_datetime(input_dt, tz, fmt='%a, %b %e at %I:%M %P %Z')


def format_datetime_export(uni_style, input_dt):
    """If input_dt is datetime, return more human read-able string.
    Else (input_dt is string), return original string."""
    if isinstance(input_dt, datetime):
        uni_tz = uni_style.timezone
        return format_datetime(input_dt, uni_tz)
    else:
        return input_dt


def validate_nonnegative_input(input, default_value=0):
    ''' Validate input that is supposed to be a non-negative integer, and return
    validated value. Else, return default value. '''
    try:
        input_int = int(input)
        if input_int < 0:
            input_int = default_value
    except ValueError:
        input_int = default_value

    return input_int


def get_event_image_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance.university.short_name, "event_image", filename)


def get_team_landing_image_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance.university.short_name, "team_landing_image", filename)


def get_uni_logo_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance._university.short_name, "logo", filename)


def get_uni_css_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance._university.short_name, "css", filename)


def get_rmblock_image_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance.university.short_name, "block_image", filename)


def get_rmblock_file_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance.university.short_name, "block_file", filename)


def get_mentor_banner_url(instance, filename):
    return os.path.join(
        "media", "uni", "%s" % instance._university.short_name, "mentor_banner", filename)


def create_event_thumbnails(image, event):
    if image:
        uid = uuid.uuid4()
        name_prefix = event.name.replace('/', '')
        large_thumbnail, large_ios = make_thumb(image, size=(720, 405))
        large_thumbnail_name = parse.quote("{0}_l_{1}.png".format(name_prefix, uid))
        large_thumbnail_file = InMemoryUploadedFile(
            large_thumbnail, None, large_thumbnail_name,
            'image/png', large_thumbnail.tell(), None
        )

        medium_thumbnail, med_ios = make_thumb(image, size=(320, 180), pad=False)
        medium_thumbnail_name = parse.quote("{0}_m_{1}.png".format(name_prefix, uid))
        medium_thumbnail_file = InMemoryUploadedFile(
            medium_thumbnail, None, medium_thumbnail_name,
            'image/png', medium_thumbnail.tell(), None
        )

        small_thumbnail, sm_ios = make_thumb(image, size=(40, 40))
        small_thumbnail_name = parse.quote("{0}_s_{1}.png".format(name_prefix, uid))
        small_thumbnail_file = InMemoryUploadedFile(
            small_thumbnail, None, small_thumbnail_name,
            'image/png', small_thumbnail.tell(), None
        )

        image_name = parse.quote("{0}_{1}.png".format(name_prefix, uid))
        event.image.save(image_name, image)
        if large_thumbnail_file:
            event.image_large.save(large_thumbnail_name, large_thumbnail_file)
            large_thumbnail.close()
            for file in large_ios:
                file.close()
        if medium_thumbnail_file:
            event.image_medium.save(medium_thumbnail_name, medium_thumbnail_file)
            medium_thumbnail.close()
            for file in med_ios:
                file.close()
        if small_thumbnail_file:
            event.image_small.save(small_thumbnail_name, small_thumbnail_file)
            small_thumbnail.close()
            for file in sm_ios:
                file.close()


def order_events(events: QuerySet):
    """Takes a queryset of Event objects and returns a them sorted as such:
         If competition or application, then by end date (ie deadline)
         If any other kind of event, then by start date.
     """
    return events.order_by(Case(
        When(is_competition=True, then='end'),
        default='start'
    ))


def get_can_share_event(user, event):
    """
    Given a UserEmail object and an Event object, this function determines and
    returns the following:
        is_shareable: a boolean representing whether or not the specified user
            is able to feature the specified event on their own platform.
        is_shared: a boolean representing whether or not the specified event is
            already featured on the specified user's platform.
        shareable_platforms: currently, a University object indicating which
            platform the specified user has permission to feature the specified
            event on. Should eventually be converted to a QuerySet of University
            objects.

    The function takes into account the admin access level of the specified user
    (i.e. which modules they have admin permissions for), whether the specified
    event is shared globally or only within a certain platform group, and
    whether the user is an admin of a platform where the event can be featured
    based on those constraints.
        -> If the event is shared globally, the user only needs to be an admin
            of a platform other than the one the event belongs to in order to
            feature it. (Events can't be featured on their own platforms,
            because this conflicts with the "pin" feature).
        -> If the event is shared within a platform group, the user needs to be
            an admin of a platform that is part of that same group in order to
            feature it.

    Currently, the function only considers the admin user's first platform, if
    they have more than one, and bases the logic around whether the admin has
    permission to feature the event ON THAT PLATFORM ONLY. Eventually the
    function will need to consider ALL platforms that the user is an admin of,
    and determine whether the admin has permission to feature the event on each
    platform individually based on the constraints described above. The full
    list of platforms on which the admin is able to feature the event should
    then be returned as a QuerySet stored in shareable_platforms.
    """
    # avoid circular import
    from trunk.models import UniversityStaff, AccessLevel, UniversitySubscription

    is_shareable = False
    is_shared = False
    shareable_platforms = None
    event_uni = event.university
    try:
        if UniversityStaff.objects.filter(admin=user).exclude(university=event_uni).exists():
            # TODO: add handling for when there is more than 1 matching UniversityStaff
            # object, i.e. user is admin of multiple "shareable" platforms
            admin_obj = UniversityStaff.objects.filter(admin=user).exclude(
                university=event_uni).first()
            admin_uni = admin_obj.university

            if event.is_application:
                event_access = AccessLevel.objects.get(name=AccessLevel.APPLICATIONS,
                    university=admin_uni)
            elif event.is_competition:
                event_access = AccessLevel.objects.get(name=AccessLevel.COMPETITIONS,
                    university=admin_uni)
            elif event.is_survey:
                event_access = AccessLevel.objects.get(name=AccessLevel.SURVEYS,
                    university=admin_uni)
            else:
                event_access = AccessLevel.objects.get(name=AccessLevel.EVENTS,
                    university=admin_uni)

            is_event_admin = admin_obj.is_super or event_access in admin_obj.access_levels.all()
            is_platform_group_admin = False
            if UniversitySubscription.objects.filter(university=event.university,
                    university_group__isnull=False).exists():
                platform_group = UniversitySubscription.objects.get(
                    university=event.university).university_group
                if is_event_admin:
                    is_platform_group_admin = UniversitySubscription.objects.filter(
                        university=admin_uni, university_group=platform_group).exists()

            if (is_event_admin and event.allow_global_sharing) or \
                    (is_platform_group_admin and event.allow_group_sharing):
                is_shareable = True
                shareable_platforms = admin_uni

            is_shared = admin_uni in event.featured_on_unis.all()
    except:
        return is_shareable, is_shared, shareable_platforms
        
    return is_shareable, is_shared, shareable_platforms


def get_primary_color(style) -> str:
    from trunk.models import UniversityStyle  # avoid circular import
    style: Optional[UniversityStyle]
    if style and style.first_color:
        return style.first_color
    else:
        return DEFAULT_PRIMARY_COLOR
