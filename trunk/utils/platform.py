import typing
from django.db.models import Q
from activity.conf import COMPANY_ONLY
from trunk.models import UniversityStyle, University, UniversityGroup


def check_private_platform_startup():
    pass


FEED_TYPE_STARTUP = 0
FEED_TYPE_MEMBER = 1
FEED_TYPE_ACTIVITY = 2
FEED_TYPE_EVENT = 3
FEED_TYPE_FORUM = 4
FEED_TYPE_JOB = 5

FEED_GLOBAL = UniversityStyle.GLOBAL
FEED_LOCAL = UniversityStyle.UNIVERSITY
FEED_GROUP = UniversityStyle.GROUP_GLOBAL


def feed_query_filter(platform: University, platform_style: UniversityStyle,
                      platform_group: typing.Optional[UniversityGroup],
                      queryset, queryset_type: int, current_feed: int):
    """

    :param platform:
    :param platform_style:
    :param platform_group: None if platform is not a member of a regional platform.
    :param queryset:
    :param queryset_type:
    :param current_feed:
    :return: result_queryset, frontend_global_options
    """
    allowed_querysets = [FEED_TYPE_STARTUP, FEED_TYPE_MEMBER, FEED_TYPE_ACTIVITY,
                         FEED_TYPE_EVENT, FEED_TYPE_FORUM, FEED_TYPE_JOB]
    if queryset_type not in allowed_querysets:
        raise ValueError("Unknown type {0}".format(queryset_type))

    KEY_NAME = 'name'
    KEY_VALUE = 'value'
    KEY_SELECTED = 'selected'
    KEY_TYPE = 'type'
    frontend_global_options = []

    global_enabled = False
    group_global_enabled = False
    exclude_local_in_global = False
    no_local_feed = False

    group_platform_ids = []

    default_view = FEED_LOCAL

    is_private_platform = platform.is_private_platform
    if platform_group:
        is_private_platform = False

    if queryset_type == FEED_TYPE_STARTUP:
        default_view = platform_style.default_startups_view
        if platform_style.default_startups_view == UniversityStyle.GLOBAL_ONLY:
            no_local_feed = True
    if queryset_type == FEED_TYPE_MEMBER:
        default_view = platform_style.default_people_view
        if platform_style.default_people_view == UniversityStyle.GLOBAL_ONLY:
            no_local_feed = True
    if queryset_type == FEED_TYPE_ACTIVITY:
        default_view = platform_style.default_feed_view
        if platform_style.default_feed_view == UniversityStyle.GLOBAL_ONLY:
            no_local_feed = True
    if queryset_type == FEED_TYPE_EVENT:
        default_view = platform_style.default_events_view
        if platform_style.default_events_view == UniversityStyle.GLOBAL_ONLY:
            no_local_feed = True
    if queryset_type == FEED_TYPE_FORUM:
        default_view = platform_style.default_discussion_view
        if platform_style.default_discussion_view == UniversityStyle.GLOBAL_ONLY:
            no_local_feed = True
    if queryset_type == FEED_TYPE_JOB:
        default_view = platform_style.default_jobs_view
        if platform_style.default_jobs_view == UniversityStyle.GLOBAL_ONLY:
            no_local_feed = True

    if queryset_type == FEED_TYPE_STARTUP and platform_style.enable_global_startups == UniversityStyle.GLOBAL_ON:
        global_enabled = True
    if queryset_type == FEED_TYPE_MEMBER and platform_style.enable_global_people == UniversityStyle.GLOBAL_ON:
        global_enabled = True
    if queryset_type == FEED_TYPE_ACTIVITY and platform_style.enable_global == UniversityStyle.GLOBAL_ON:
        global_enabled = True
    if queryset_type == FEED_TYPE_EVENT and platform_style.enable_global_events == UniversityStyle.GLOBAL_ON:
        global_enabled = True
    if queryset_type == FEED_TYPE_FORUM and platform_style.enable_global_discussion == UniversityStyle.GLOBAL_ON:
        global_enabled = True
    if queryset_type == FEED_TYPE_JOB and platform_style.enable_global_jobs == UniversityStyle.GLOBAL_ON:
        global_enabled = True

    if current_feed <= 0:
        if default_view == FEED_GLOBAL and not global_enabled:
            if platform_group:
                default_view = FEED_GROUP
            else:
                default_view = FEED_LOCAL
        current_feed = default_view

    if global_enabled and not is_private_platform:
        global_name = 'GLOBAL'
        frontend_global_options.append(
            {
                KEY_NAME: global_name,
                KEY_VALUE: FEED_GLOBAL,
                KEY_SELECTED: current_feed == FEED_GLOBAL,
                KEY_TYPE: queryset_type
            }
        )

    if platform_group:
        group_platform_ids = platform_group.universitysubscription_set.all().values_list('university_id', flat=True)
        if queryset_type == FEED_TYPE_STARTUP and \
                platform_style.enable_group_global_startups == UniversityStyle.GLOBAL_ON:
            group_global_enabled = True
        if queryset_type == FEED_TYPE_MEMBER and \
                platform_style.enable_group_global_people == UniversityStyle.GLOBAL_ON:
            group_global_enabled = True
        if queryset_type == FEED_TYPE_ACTIVITY and \
                platform_style.enable_group_global_activity == UniversityStyle.GLOBAL_ON:
            group_global_enabled = True
        if queryset_type == FEED_TYPE_EVENT and \
                platform_style.enable_group_global_events == UniversityStyle.GLOBAL_ON:
            group_global_enabled = True
        if queryset_type == FEED_TYPE_FORUM and \
                platform_style.enable_group_global_discussion == UniversityStyle.GLOBAL_ON:
            group_global_enabled = True
        if queryset_type == FEED_TYPE_JOB and \
                platform_style.enable_group_global_jobs == UniversityStyle.GLOBAL_ON:
            group_global_enabled = True

        if group_global_enabled:
            if platform.short_name == 'harvard':
                group_global_name = 'MIT'
            elif platform.short_name == 'mit':
                group_global_name = 'HARVARD'
            else:
                group_global_name = platform_group.name
            frontend_global_options.append(
                {
                    KEY_NAME: group_global_name,
                    KEY_VALUE: FEED_GROUP,
                    KEY_SELECTED: current_feed == FEED_GROUP,
                    KEY_TYPE: queryset_type
                }
            )

    if global_enabled or group_global_enabled or is_private_platform:
        # if global is not enabled, no need to have global feed header.
        if not no_local_feed:
            frontend_global_options.append(
                {
                    KEY_NAME: platform.short_name,
                    KEY_VALUE: FEED_LOCAL,
                    KEY_SELECTED: current_feed == FEED_LOCAL,
                    KEY_TYPE: queryset_type
                }
            )

    if len(frontend_global_options) == 1:
        frontend_global_options = []

    if not platform_style.include_self_global:
        exclude_local_in_global = True

    result_queryset = queryset

    if current_feed == FEED_GLOBAL and global_enabled:
        if queryset_type == FEED_TYPE_STARTUP:
            result_queryset = result_queryset.exclude(
                Q(university__short_name='forest') |
                Q(university__is_private_platform=True))
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(university=platform)
        if queryset_type == FEED_TYPE_MEMBER:
            result_queryset = result_queryset.exclude(
                Q(universities__short_name='forest') |
                Q(universities__is_private_platform=True))
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(universities=platform)
        if queryset_type == FEED_TYPE_ACTIVITY:
            result_queryset = result_queryset.exclude(
                Q(university__short_name='forest') |
                Q(university__is_private_platform=True))
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(university_id=platform.id)
            # SW-3262 hide it in global activity feed too
            exclude_platforms = UniversityStyle.objects \
                .filter(include_mentorship_connections=UniversityStyle.DISPLAY_OFF) \
                .values_list('_university_id', flat=True)
            result_queryset = result_queryset.exclude(Q(university_id__in=exclude_platforms) & Q(activity_type=27))
        if queryset_type == FEED_TYPE_EVENT:
            result_queryset = result_queryset.exclude(
                Q(university__is_private_platform=True) | Q(university__short_name='forest'))
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(university_id=platform.id)
        if queryset_type == FEED_TYPE_FORUM:
            result_queryset = result_queryset.filter(is_global=True)
            result_queryset = result_queryset.filter(
                    Q(aff_uni__universitystyle__display_forum=UniversityStyle.DISPLAY_ON) |
                    Q(aff_uni=platform))\
                .exclude(
                    Q(aff_uni_id__is_private_platform=True) |
                    Q(aff_uni__short_name='forest'))
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(aff_uni=platform)
        if queryset_type == FEED_TYPE_JOB:
            result_queryset = result_queryset.exclude(
                Q(startup__university__is_private_platform=True) |
                Q(startup__university__short_name='forest'))
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(startup__university=platform)
    elif current_feed == FEED_GROUP and group_global_enabled:
        if queryset_type == FEED_TYPE_STARTUP:
            result_queryset = result_queryset.filter(university__id__in=group_platform_ids)
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(university=platform)
        if queryset_type == FEED_TYPE_MEMBER:
            result_queryset = result_queryset.filter(universities__id__in=group_platform_ids)
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(universities=platform)
        if queryset_type == FEED_TYPE_ACTIVITY:
            result_queryset = result_queryset.filter(university_id__in=group_platform_ids)
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(university_id=platform.id)
        if queryset_type == FEED_TYPE_EVENT:

            result_queryset = result_queryset.filter(university_id__in=group_platform_ids)
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(university_id=platform.id)
        if queryset_type == FEED_TYPE_FORUM:
            result_queryset = result_queryset.filter(aff_uni_id__in=group_platform_ids)
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(aff_uni=platform)
        if queryset_type == FEED_TYPE_JOB:
            result_queryset = result_queryset.filter(startup__university__id__in=group_platform_ids)
            if exclude_local_in_global:
                result_queryset = result_queryset.exclude(startup__university=platform)
    else:
        # default is always Local
        if not no_local_feed:
            if queryset_type == FEED_TYPE_STARTUP:
                result_queryset = result_queryset.filter(university=platform)
            if queryset_type == FEED_TYPE_MEMBER:
                result_queryset = result_queryset.filter(universities=platform)
            if queryset_type == FEED_TYPE_ACTIVITY:
                result_queryset = result_queryset.filter(university_id=platform.id)
            if queryset_type == FEED_TYPE_EVENT:
                result_queryset = result_queryset.filter(university_id=platform.id)
            if queryset_type == FEED_TYPE_FORUM:
                result_queryset = result_queryset.filter(aff_uni=platform)
            if queryset_type == FEED_TYPE_JOB:
                result_queryset = result_queryset.filter(startup__university=platform)

    return result_queryset, frontend_global_options
