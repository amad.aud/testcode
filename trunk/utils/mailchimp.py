from mailchimp_marketing import Client

mailchimp = Client()


def set_config_mailchimp(apikey, server):
    mailchimp.set_config({
        "api_key": apikey,
        "server": server
    })


def create_list(apikey, server, list_name):
    pass
    set_config_mailchimp(apikey, server)
    body = {
        "permission_reminder": "You signed up for updates on our website",
        "email_type_option": False,
        "name": list_name,
        "contact": {
            "company": "Mailchimp",
            "address1": "675 Ponce de Leon Ave NE",
            "address2": "Suite 5000",
            "city": "Atlanta",
            "state": "GA",
            "zip": "30308",
            "country": "US"
        }, "campaign_defaults":
            {"from_name": "from_name",
             "from_email": "Opal10@gmail.com",
             "subject": "subject",
             "language": "language"}
    }
    try:
        response = mailchimp.lists.create_list(body)
        return response['id']
    except Exception as e:
        print(e)
        return False


def add_members(apikey, server, list_id, member,first_name, last_name):
    pass
    set_config_mailchimp(apikey, server)
    try:
        response = mailchimp.lists.add_list_member(list_id,
                                                   {"email_address": member, "status": "subscribed", "merge_fields": {
                                                       "FNAME": first_name,
                                                       "LNAME": last_name
                                                   }})
        return response['id']
    except Exception as e:
        print(e)
        return False

# try:
#     # response = mailchimp.lists.add_list_member("a343c9df7f",
#     #                                         {"email_address": "Estrella.Rolfson49@yahoo.com", "status": "pending"})
#
#     response = mailchimp.lists.get_list_members_info("a343c9df7f")
#     print("Response: {}".format(response))
# except Exception as e:
#     print("An exception occurred: {}".format(e.text))