from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from trunk.models import Major, Event
from branch.models import Label
from branch.documents_format import GROUP_FORMAT
from emailuser.documents_format import USEREMAIL_FORMAT
from mentorship.documents_format import MENTOR_FORMAT
from trunk.documents_format import (
    UNIVERSITY_FORMAT, SIMPLE_UNIVERSITY_FORMAT
)
from StartupTree.config import search as sconfig
from StartupTree.utils.search import ngram_analyzer, html_strip


@registry.register_document
class MajorDocument(Document):
    name = fields.KeywordField(attr='get_name')

    doc_type = fields.KeywordField()
    suggest = fields.CompletionField()

    class Index:
        name = sconfig.MAJOR_DOCUMENT

    class Django:
        model = Major

    def prepare_suggest(self, instance):
        return instance.get_name()

    def prepare_doc_type(self, instance):
        return sconfig.MAJOR_DOCUMENT


@registry.register_document
class EventDocument(Document):
    name = fields.KeywordField(attr='name')
    event_id = fields.KeywordField(attr='get_event_id')
    image = fields.KeywordField(attr='get_image')
    image_large = fields.KeywordField(attr='get_image_large')
    image_medium = fields.KeywordField(attr='get_image_medium')
    image_small = fields.KeywordField(attr='get_image_small')
    url = fields.KeywordField(attr='get_url')
    location = fields.KeywordField(attr='location')
    university = fields.NestedField(properties=SIMPLE_UNIVERSITY_FORMAT)
    description = fields.TextField(analyzer=html_strip)
    groups = fields.NestedField(properties=GROUP_FORMAT)
    is_one_day_event = fields.BooleanField(attr='is_one_day_event')
    get_formatted_start_datetime = fields.KeywordField(attr='get_formatted_start_datetime')
    get_formatted_end_datetime = fields.KeywordField(attr='get_formatted_end_datetime')
    get_formatted_start_month_day = fields.KeywordField(attr='get_formatted_start_month_day')
    get_formatted_end_month_day = fields.KeywordField(attr='get_formatted_end_month_day')

    doc_type = fields.KeywordField()
    suggest = fields.TextField(analyzer=ngram_analyzer)

    class Index:
        name = sconfig.EVENT_DOCUMENT

    class Django:
        model = Event
        fields = [
            'id',
            'status',
            'start',
            'end',
            'is_competition',
            'is_published',
            'is_private',
            'is_archived_event',
            'is_application',
            'is_survey',
            'event_type',
            'map_location',
            'free_food',
            'free_swag'
        ]
        # related_models = [Label] performance issue. Maybe saving just "id" and use those ids for search later would be optimal way.

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Label):
            return related_instance.event_set.all()

    def prepare_suggest(self, instance):
        return instance.name

    def prepare_doc_type(self, instance):
        return sconfig.EVENT_DOCUMENT

    def prepare_groups(self, instace):
        return list(instace.get_groups().all().values())