from django_elasticsearch_dsl import fields

UNIVERSITY_FORMAT = {
    'id': fields.IntegerField(),
    'name': fields.KeywordField(),
    'site': fields.NestedField(properties={
        'domain': fields.KeywordField(),
        'name': fields.KeywordField(),
        'id': fields.IntegerField()
    }),
    'is_private_platform': fields.BooleanField(),
    'short_name': fields.KeywordField(),
    'universitysubscription': fields.ObjectField(properties={
        'university_group': fields.ObjectField(properties={
            'id': fields.IntegerField(),
            'name': fields.KeywordField()
        })
    })
}

SIMPLE_UNIVERSITY_FORMAT = {
    'id': fields.IntegerField(),
    'name': fields.KeywordField(),
    'is_private_platform': fields.BooleanField(),
    'short_name': fields.KeywordField(),
}