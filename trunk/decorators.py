from django.http.response import (
    JsonResponse, HttpResponseRedirect, Http404
)
from StartupTree.loggers import app_logger
from django.utils.six import wraps
from trunk.models import StartupTreeStaff, AccessLevel


def only_team_mentorship_admin(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
            raise Http404('Not team mentorship admin')
        return view_func(request, *args, **kwargs)
    return _wrapper


def only_mentorship_admin(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        if kwargs.get('is_team', False):
            if AccessLevel.TEAM_MENTORSHIP not in request.access_levels and not request.is_super:
                raise Http404()
        else:
            if AccessLevel.MENTORSHIP not in request.access_levels and not request.is_super:
                raise Http404('Not mentorship admin')
        return view_func(request, *args, **kwargs)
    return _wrapper


def only_university(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        university = request.is_university
        kwargs["university"] = university
        if not university:
            app_logger.warning(
                "{0} isn't a university user!".format(request.user)
            )
            if json:
                return JsonResponse(
                    {'status': 403,
                     'msg': 'Unauthorized Access.'}
                )
            return HttpResponseRedirect('/')
        return view_func(request, *args, **kwargs)
    return _wrapper


def only_startuptree_staff(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and not request.is_archived and \
                StartupTreeStaff.objects.filter(staff_id=user.id).exists():
            return view_func(request, *args, **kwargs)
        if json:
            return JsonResponse(
                {'status': 404}
            )
        return HttpResponseRedirect('/')
    return _wrapper


def at_least_events(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        university = None
        if AccessLevel.EVENTS in request.access_levels or request.is_super:
            university = request.university
        kwargs["university"] = university
        if not university:
            app_logger.warning(
                "{0} isn't a university user!".format(request.user)
            )
            if json:
                return JsonResponse(
                    {'status': 403,
                     'msg': 'Unauthorized Access.'}
                )
            return HttpResponseRedirect('/')
        return view_func(request, *args, **kwargs)
    return _wrapper


def at_least_competitions(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        university = None
        if AccessLevel.APPLICATIONS in request.access_levels or \
            AccessLevel.COMPETITIONS in request.access_levels or request.is_super:
            university = request.university
        kwargs["university"] = university
        if not university:
            app_logger.warning(
                "{0} isn't a university user!".format(request.user)
            )
            if json:
                return JsonResponse(
                    {'status': 403,
                     'msg': 'Unauthorized Access.'}
                )
            return HttpResponseRedirect('/')
        return view_func(request, *args, **kwargs)
    return _wrapper


def any_event_type(view_func, json=False):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        university = None
        if AccessLevel.EVENTS in request.access_levels or \
                AccessLevel.APPLICATIONS in request.access_levels or \
                AccessLevel.COMPETITIONS in request.access_levels or \
                AccessLevel.SURVEYS in request.access_levels or request.is_super:
            university = request.university
        kwargs["university"] = university
        if not university:
            app_logger.warning(
                "{0} isn't a university user!".format(request.user)
            )
            if json:
                return JsonResponse(
                    {'status': 403,
                     'msg': 'Unauthorized Access.'}
                )
            return HttpResponseRedirect('/')
        return view_func(request, *args, **kwargs)
    return _wrapper


def only_university_json(view_func):
    return only_university(view_func, json=True)


def at_least_competitions_json(view_func):
    return at_least_competitions(view_func, json=True)


def at_least_events_json(view_func):
    return at_least_events(view_func, json=True)


def only_job_admin(view_func):
    @wraps(view_func)
    def _wrapper(request, *args, **kwargs):
        if AccessLevel.JOBS not in request.access_levels and not request.is_super:
            raise Http404('Not Job admin')
        return view_func(request, *args, **kwargs)
    return _wrapper
