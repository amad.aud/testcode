import datetime
import re
import urllib
import uuid

import pytz
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import transaction
from django.db.models import Q
from django.db.models.aggregates import Count
from django.http import HttpRequest
from django.http.response import (
    JsonResponse, HttpResponseRedirect, Http404
)
from django.shortcuts import redirect
# from django.utils.html import strip_tags
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.http import is_safe_url

from StartupTree import settings
# venture imports
# mentorship import
# StartupTree
from StartupTree.loggers import app_logger
from StartupTree.utils import get_date_time_object, month_delta, make_thumb
# activity imports
from activity.clerk import UniversityActivityClerk, EventActivityClerk
from activity.models import Activity
# branch imports
from branch.modelforms import (
    TextEditorForm2,
    EmailEditorForm,
    EmailEditorForm2,
    GuestRSVPForm,
    EmailFormset,
)
from branch.models import (
    StartupMember,
    UniversityAffiliation,
    Label
)
from branch.serializer.member import sm_to_dict
from branch.tasks import notify_event_creator, send_event_updated_noti, send_checkin_confirmation
from branch.utils.tags import get_manageable_tags, get_manageable_tags_titles
from branch.views.competitions import view_applicants
from branch.views.events import (
    retrieve_rsvps,
    export_rsvps
)
# From emailuser
from emailuser.models import UserEmail
from emailuser.tasks import send_event_invite
from forms.models import Form, AnonymousApplicant, MemberApplicant, Question, Choice
from forms.parser import CustomAnswersParser
from forms.serializer.forms import custom_member_dict
# From hamster
from notifications.clerks import NotificationClerk
# trunk imports
from trunk.io import xlrd
from trunk.modelforms import ImportForm
from trunk.models import (
    Event,
    EventsBlacklist,
    EventRSVP,
    EventCheckin,
    GuestRSVP,
    University,
    School,
    UniversityEmailDomain,
    is_comp_app_admin
)
from trunk.views.abstract import EventOnlyDashboardView, EventOnlyDashboardJsonView, AllEventDashboardView
from trunk.utils import create_event_thumbnails, validate_nonnegative_input
from trunk.views.utils import prepare_event
from trunk.modelforms import EventImageForm
from branch.modelforms import TinyMCEWidget
from StartupTree.settings import IS_DEV, IS_STAGING
from StartupTree.strings import AUTO_CONFERENCE_DEFAULT_EMAIL, MIDDLE_MAN_EMAILUSER_ID_DEV
from mentorship.st_cronofy import create_or_update_conference_event, delete_conference_event, establish_cronofy_for_user


def sorted_events_admin_panel(request, sort_by, university, archive_dropdown_events,
                              archive_dropdown_comp, archive_dropdown_survey,
                              is_comp, is_app, is_survey, page=1):
    is_archived_event_index = request.GET.get('archived-event', None)
    is_archived_comp_index = request.GET.get('archived-comp', None)
    is_archived_survey_index = request.GET.get('archived-survey', None)

    if not is_comp and not is_app and not is_survey:
        if is_archived_event_index is not None:
            e = Event.objects.get(event_id=str(is_archived_event_index))
            if e.is_archived_event:
                e.is_archived_event = False
            else:
                e.is_archived_event = True
            e.save()
    elif is_comp or is_app:
        if is_archived_comp_index is not None:
            e = Event.objects.get(event_id=str(is_archived_comp_index))
            if e.is_archived_event:
                e.is_archived_event = False
            else:
                e.is_archived_event = True
            e.save()
    elif is_survey:
        if is_archived_survey_index is not None:
            e = Event.objects.get(event_id=str(is_archived_survey_index))
            if e.is_archived_event:
                e.is_archived_event = False
            else:
                e.is_archived_event = True
            e.save()

    events = Event.objects.filter(university=university,
                                  is_competition=is_comp,
                                  is_application=is_app,
                                  is_template_only=False,
                                  is_survey=is_survey)\
                          .order_by('start')

    if archive_dropdown_events == "archived":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=False, is_archived_event=True, is_template_only=False)
    elif archive_dropdown_events == "published":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=False, is_published=True, is_template_only=False)
    elif archive_dropdown_events == "unpublished":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=False, is_published=False, is_template_only=False)
    elif archive_dropdown_events == "all":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=False, is_template_only=False)

    if archive_dropdown_comp == "archived":
        events = Event.objects.filter(university=university, is_competition=True, is_application=is_app, is_survey=False, is_archived_event=True, is_template_only=False)
    elif archive_dropdown_comp == "unarchived":
        events = Event.objects.filter(university=university, is_competition=True, is_application=is_app, is_survey=False, is_archived_event=False, is_template_only=False)
    elif archive_dropdown_comp == "published":
        events = Event.objects.filter(university=university, is_competition=True, is_application=is_app, is_survey=False, is_published=True, is_template_only=False)
    elif archive_dropdown_comp == "unpublished":
        events = Event.objects.filter(university=university, is_competition=True, is_application=is_app, is_survey=False, is_published=False, is_template_only=False)
    elif archive_dropdown_comp == "all":
        events = Event.objects.filter(university=university, is_competition=True, is_application=is_app, is_survey=False, is_template_only=False)

    if archive_dropdown_survey == "archived":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=True, is_archived_event=True, is_template_only=False)
    elif archive_dropdown_survey == "published":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=True, is_published=True, is_template_only=False)
    elif archive_dropdown_survey == "unpublished":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=True, is_published=False, is_template_only=False)
    elif archive_dropdown_survey == "all":
        events = Event.objects.filter(university=university, is_competition=False, is_survey=True, is_template_only=False)

    if university.style.restrict_tagged_admins and not request.is_startuptree_staff:
        member = request.current_member
        events = events.annotate(num_tags=Count('tags')) \
            .filter(Q(tags__in=member.labels.all()) | Q(num_tags=0)).distinct()

    if sort_by == 'start-desc':
        events = events.order_by('-start')
    elif sort_by == 'start-asc':
        events = events.order_by('start')
    elif sort_by == 'name-asc':
        events = events.order_by('name')
    elif sort_by == 'name-desc':
        events = events.order_by('-name')
    elif sort_by == 'rsvps-asc':
        events = events.annotate(
            r_a_count=Count('attending_users')).order_by('r_a_count')
    elif sort_by == 'rsvps-desc':
        events = events.annotate(
            r_d_count=Count('attending_users')).order_by('-r_d_count')
    elif sort_by == 'views-asc':
        events = events.order_by('view_count')
    elif sort_by == 'views-desc':
        events = events.order_by('-view_count')
    elif sort_by == 'checkins-asc':
        events = events.annotate(
            c_a_count=Count('checked_in')).order_by('c_a_count')
    elif sort_by == 'checkins-desc':
        events = events.annotate(
            c_d_count=Count('checked_in')).order_by('-c_d_count')
    else:
        events = events.order_by('-start')

    # TODO: pagination
    return events


def get_all_rsvps(events):
    """
    Gets all RSVPs for a specified QuerySet of events.

    Deprecated? This helper function is only called once in the whole code base,
    and the data it returns is never used.
    """
    rsvps = {}
    for event in events:
        if event.attending_users.all().count() != 0 or event.interested_users.all().count() != 0 or event.checked_in.all().count() != 0:
            for rsvp in event.get_attending_user_without_archive():
                rsvp_dict = build_rsvp_dict(rsvp, event, 'Yes')
                if event in rsvps:
                    rsvps[event].append(rsvp_dict)
                else:
                    rsvps[event] = [rsvp_dict]
            for interested in event.interested_users.all():
                interested_dict = build_rsvp_dict(interested, event, 'Interested')
                if event in rsvps:
                    rsvps[event].append(interested_dict)
                else:
                    rsvps[event] = [interested_dict]
            for checked_in in event.checked_in.all():
                try:
                    checkin_member = UserEmail.objects.get(id=checked_in.member.user_id)
                except AttributeError:
                    checkin_member = GuestRSVP.objects.get(id=checked_in.guest.id)
                if checkin_member not in event.get_attending_user_without_archive() and checked_in.member not in event.interested_users.all():
                    checkin_dict = build_rsvp_dict(checkin_member, event, 'No')
                    if event in rsvps:
                        rsvps[event].append(checkin_dict)
                    else:
                        rsvps[event] = [checkin_dict]
        else:
            rsvps[event] = {}
    # sort all entries alphabetically within each event
    for event_name, event_dict in rsvps.items():
        new_dict = sorted(event_dict, key=lambda k: k['name'])
        rsvps[event_name] = new_dict
    return rsvps


def build_rsvp_dict(rsvp, event, status):
    """
    Builds a dictionary of a single RSVP or check-in for a specified event.

    Deprecated? This helper function is only called in get_all_rsvps() above,
    which is not used.
    """
    all_checkins = []
    if isinstance(rsvp, UserEmail):
        rsvp_member = StartupMember.objects.get_user(rsvp)
        try:
            # timestamp keeps track of latest checkin only
            checkin_status = event.checked_in.all().filter(member=rsvp_member).order_by('-created')[:1]
            all_checkins = event.checked_in.all().filter(member=rsvp_member).order_by('-created')
            timestamp = checkin_status[0].created
        except:
            checkin_status = False
            timestamp = None
        rsvp_dict = {
            'rsvp': rsvp,
            'name': rsvp.get_full_name(),
            'url': rsvp_member.get_url(),
            'user_id': rsvp_member.id,
            'status': status,
            'checkin_status': checkin_status,
            'timestamp': timestamp,
            'previous_checkins': all_checkins
        }
    elif isinstance(rsvp, GuestRSVP):
        try:
            # timestamp keeps track of latest checkin only
            checkin_status = event.checked_in.all().filter(guest=rsvp).order_by('-created')[:1]
            all_checkins = event.checked_in.all().filter(guest=rsvp).order_by('-created')
            timestamp = checkin_status[0].created
        except:
            checkin_status = False
            timestamp = None
        rsvp_dict = {
            'rsvp': rsvp,
            'name': '{0} {1}'.format(rsvp.first_name, rsvp.last_name),
            'url': None,
            'user_id': rsvp.id,
            'status': status,
            'checkin_status': checkin_status,
            'timestamp': timestamp,
            'previous_checkins': all_checkins
        }
    return rsvp_dict


class ManageEvents(EventOnlyDashboardView):
    def get(self, request, university, event_id=None, is_create=0):
        events = None
        rsvps = None
        event_table = None
        sort_by = request.GET.get('sort', 'start-desc')
        archive_dropdown_filter = request.GET.get('sort2', '')
        tag_filter = request.GET.get('filter', 'all')
        unarchive_event = request.GET.get('unarchive-event-type', 'default')

        page = request.GET.get('page', 1)

        if unarchive_event != 'default':
            event_unarchived = Event.objects.get(event_id=unarchive_event)
            event_unarchived.is_archived_event = False
            event_unarchived.save()
        archive_dropdown_filter2 = ''
        archive_dropdown_filter3 = ''

        uni_labels = get_manageable_tags_titles(request)

        try:
            events = sorted_events_admin_panel(request, sort_by, university, archive_dropdown_filter,
                                               archive_dropdown_filter2, archive_dropdown_filter3,
                                               False, False, False)
            if tag_filter != 'all':
                events = events.filter(tags__title=tag_filter)

            event_table = list(map(lambda x: (
                x, x.get_url(), x.not_started(), x.not_ended(), x.get_formatted_start_end_date_for_display()),
                                   events))
            show_calendar = request.university_style.display_calendar_admin
            submitted_events_exist = events.filter(creator__isnull=False, is_archived_event=False).exists()
            archived_events = events.filter(is_archived_event=True)
        except Event.DoesNotExist:
            pass
        if len(events) == 0:
            events = None
            event_table = None
            rsvps = None
            show_calendar = False
            submitted_events_exist = False
        template = 'trunk/manage/events/list.html'

        page_name = 'manage-events-metrics'

        return TemplateResponse(
            request, template,
            {
                'title': 'Events',
                'page_name': page_name,
                'events': events,
                'sort_by': sort_by,
                'a': archive_dropdown_filter,
                'tag_filter': tag_filter,
                'tags': uni_labels,
                'event_table': event_table,
                'university': university,
                'counter': 0,
                'show_calendar': show_calendar,
                'submitted_events_exist': submitted_events_exist,
                'archived_events': archived_events
            }
        )

    def post(self, request, university, event_id=None, is_create=0, is_archived_event='fg'):
        # event = Event.objects.get(event_id=event_id)
        site = request.current_site
        clerk = UniversityActivityClerk(site, university)

        if not is_archived_event:
            self.archive(clerk)
        elif is_archived_event:
            pass
        else:
            # self.unarchive(clerk)
            # app_logger.debug(self.get_is_archived)
            pass
        return redirect('uni-manage-events')


class EventDashboard(EventOnlyDashboardView):
    def get(self, request, university, event_id=None):
        page_name = 'manage-events-view'
        event = None
        is_edit_event = False
        if event_id:
            event = Event.objects.get(event_id=event_id)
            if event.is_competition:
                raise Http404
            eboard = event.event_board
            round_count = 0

            # Display times

            # Ensure that datetimes are AWARE as opposed to NAIVE for proper localtime
            timeuntil = event.calc_timeuntil()
            open = True
            if timeuntil < 0:
                timeuntil = '0'
                open = False
            else:
                timeuntil = str(timeuntil)

            # whether or not emails related to this event are able to be customized
            editable_reminder = (event.send_emails_days_prior > 0) or university.style.send_rsvp_reminders
            customizable_emails = editable_reminder or event.allow_rsvp

            majordict = {}
            roledict = {}
            graddict = {}
            skilldict = {}
            user_count = event.get_attending_user_without_archive().count()
            if user_count > 0:
                for email in event.get_attending_user_without_archive().iterator():
                    member = UserEmail.objects.get(email=email)
                    startupmember = \
                        StartupMember.objects.get_user(user=member)
                    data = sm_to_dict(startupmember,
                                      university.id,
                                      detailed_degree=True)
                    skills = data['skills']
                    roles = data['roles']
                    degrees = data['degrees']
                    for skill in skills:
                        if skill in skilldict:
                            skilldict[skill] += 1
                        else:
                            skilldict[skill] = 1
                    for role in roles:
                        if role in roledict:
                            roledict[role] += 1
                        else:
                            roledict[role] = 1
                    gradyr = None
                    major = None
                    for degree in degrees:
                        if 'graduated_on' in degree and degree['graduated_on'] is not None:
                            year = degree['graduated_on']
                        if 'major' in degree and degree['major'] is not '':
                            majr = degree['major']
                        elif degree['major'] is '':
                            majr = 'Undeclared'
                        elif degree['major'] is None:
                            pass
                        else:
                            pass
                        if gradyr is None:
                            if year is not '':
                                gradyr = year
                                major = majr
                            else:
                                major = majr
                        elif year is '':
                            major = majr
                        elif year > gradyr:
                            gradyr = year
                            major = majr
                        else:
                            pass
                    if gradyr in graddict:
                        graddict[gradyr] += 1
                    else:
                        graddict[gradyr] = 1
                    if major in majordict:
                        majordict[major] += 1
                    else:
                        majordict[major] = 1

                # sorting majors
                majorkeys = list(majordict.keys())
                majorvals = list(majordict.values())
                mindex = list(range(len(majorvals)))
                mindex.sort(key=majorvals.__getitem__, reverse=True)
                majorlabels = list(map(majorkeys.__getitem__, mindex))
                majordata = list(map(majorvals.__getitem__, mindex))
                if len(majorlabels) > 10:
                    otherd = sum(majordata[9:])
                    majorlabels = majorlabels[:9]
                    majordata = majordata[:9]
                    majorlabels.append('Other')
                    majordata.append(otherd)
                elif len(majorlabels) == 1 and None in majorlabels:
                    majorlabels = []
                    majordata = []
                else:
                    pass
                # sorting roles
                rolekeys = list(roledict.keys())
                rolevals = list(roledict.values())
                rindex = list(range(len(rolevals)))
                rindex.sort(key=rolevals.__getitem__, reverse=True)
                rolelabels = list(map(rolekeys.__getitem__, rindex))
                roledata = list(map(rolevals.__getitem__, rindex))
                if len(rolelabels) > 10:
                    otherd = sum(roledata[9:])
                    rolelabels = rolelabels[:9]
                    roledata = roledata[:9]
                    rolelabels.append('Other')
                    roledata.append(otherd)
                else:
                    pass
                # sorting grad years
                gradkeys = list(graddict.keys())
                gradvals = list(graddict.values())
                gindex = list(range(len(gradvals)))
                gindex.sort(key=gradvals.__getitem__, reverse=True)
                gradlabels = list(map(gradkeys.__getitem__, gindex))
                graddata = list(map(gradvals.__getitem__, gindex))
                if len(gradlabels) > 10:
                    otherd = sum(graddata[9:])
                    gradlabels = gradlabels[:9]
                    graddata = graddata[:9]
                    gradlabels.append('Other')
                    graddata.append(otherd)
                elif len(gradlabels) == 1 and None in gradlabels:
                    graddata = []
                    gradlabels = []
                else:
                    pass
                # sorting skills
                skillkeys = list(skilldict.keys())
                skillvals = list(skilldict.values())
                sindex = list(range(len(skillvals)))
                sindex.sort(key=skillvals.__getitem__, reverse=True)
                skilllabels = list(map(skillkeys.__getitem__, sindex))
                skilldata = list(map(skillvals.__getitem__, sindex))
                if len(skilllabels) > 10:
                    skilllabels = skilllabels[:10]
                    skilldata = skilldata[:10]
                else:
                    pass

                return TemplateResponse(
                    request,
                    'trunk/manage/events/dashboard.html',
                    {
                        'title': 'Event Description',
                        'page_name': page_name,
                        'event': event,
                        'has_data': True,
                        'gradlabels': gradlabels,
                        'graddata': graddata,
                        'majorlabels': majorlabels,
                        'majordata': majordata,
                        'rolelabels': rolelabels,
                        'roledata': roledata,
                        'skilllabels': skilllabels,
                        'skilldata': skilldata,
                        'timeuntil': timeuntil,
                        'usercount': user_count + event.get_guest_rsvps_without_archive_count(),
                        'checkincount': event.checked_in.count(),
                        'eventopen': open,
                        'customizable_emails': customizable_emails,
                        'university': university,
                        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY
                    }
                )
            else:
                return TemplateResponse(
                    request,
                    'trunk/manage/events/dashboard.html',
                    {
                        'title': 'Event Description',
                        'page_name': page_name,
                        'event': event,
                        'has_data': False,
                        'timeuntil': timeuntil,
                        'usercount': user_count,
                        'checkincount': event.checked_in.count(),
                        'eventopen': open,
                        'customizable_emails': customizable_emails,
                        'university': university,
                        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY
                    })
        else:
            messages.add_message(request,
                                 messages.Error,
                                 'Event does not exist')
            return HttpResponseRedirect(reverse('uni-manage-events'))


class ManageRSVPCheckins(EventOnlyDashboardView):
    """
    Renders a list of all users who have RSVPed, checked in, or been invited
    to a particular event.
    """

    def get(self, request, university, event_id):
        try:
            event = Event.objects.get(
                event_id=event_id,
                university=university
            )
        except:
            raise Http404()

        invite_set = EmailFormset()

        # get view filter
        ALL_ATTENDEES = 'all'
        ARCHIVED = 'archived'
        view_option = request.GET.get('view', ALL_ATTENDEES)
        # TODO: add pagination
        # current_page = request.GET.get('page')
        # sort_by = request.GET.get('sort', 'name')  # name or affiliation (aff)
        # sort_direction = request.GET.get('direct', 'asc')  # ascending (asc) or descending (des)

        all_rsvp = retrieve_rsvps(event, view_option)
        guest_form = GuestRSVPForm()
        uni_schools = School.objects.filter(university=university)

        rsvp_custom = custom_member_dict(None, university.id, 9, event_id)

        try:
            # If email invitation had just been sent, then show a modal with invitation info.
            emails_st_invited = request.emails_st_invited
            emails_st_already_invited = request.emails_st_already_invited
            emails_not_st_invited = request.emails_not_st_invited
            invitations_sent = "true"
        except AttributeError:
            emails_st_invited = None
            emails_st_already_invited = None
            emails_not_st_invited = None
            invitations_sent = "false"

        return TemplateResponse(request, 'trunk/manage/events/manage_rsvps_checkins.html',
                                {
                                    'university': university,
                                    'rsvps': all_rsvp,
                                    'event': event,
                                    'form': guest_form,
                                    'uni_schools': uni_schools,
                                    'view_option': view_option,
                                    'invite_set' : invite_set,
                                    'event_id': str(event_id),
                                    'emails_st_invited' : emails_st_invited,
                                    'emails_st_already_invited' : emails_st_already_invited,
                                    'emails_not_st_invited' : emails_not_st_invited,
                                    'invitations_sent' : invitations_sent,
                                    'event_type' : "event",
                                    'rsvp_custom': rsvp_custom,
                                    'page_name': 'manage rsvp checkins'
                                })


class EditRSVPCheckins(EventOnlyDashboardView):
    """
    Handles a POST request from an admin editing the RSVP and check-in data
    rendered by ManageRSVPCheckins above.
    """
    def post(self, request, university, event_id):
        try:
            event = Event.objects.get(
                event_id=event_id,
                university=university
            )
            attending_members = event.attending_users.all().values_list('id', flat=True)
            checkedin_members = event.checked_in.exclude(member=None).values_list('member__user_id', flat=True)
            attending_guests = event.guest_rsvps.all().values_list('id', flat=True)
            checkedin_guests = event.checked_in.filter(guest__isnull=False).values_list('guest__id', flat=True)
        except:
            raise Http404()
        for data in request.POST:
            if data != 'csrfmiddlewaretoken':
                user_id = data.split('-')[2]
                if 'edit-name-' in data:
                    guest = GuestRSVP.objects.get(id=user_id)
                    new_name = request.POST.get(data).split(' ', 1)
                    guest.first_name = new_name[0]
                    guest.last_name = new_name[1]
                    guest.save()
                elif 'edit-email-' in data:
                    guest = GuestRSVP.objects.get(id=user_id)
                    new_email = request.POST.get(data)
                    guest.email = new_email
                    guest.save()
                elif 'edit-aff-' in data:
                    new_aff = int(request.POST.get(data, 0))
                    if new_aff > 4 or new_aff < 1:
                        continue
                    if int(user_id) in attending_members or int(user_id) in checkedin_members:
                        user = StartupMember.objects.get_user(user=UserEmail.objects.get(id=user_id))
                        if EventRSVP.objects.filter(member=user, rsvp_event=event).exists():
                            rsvp = EventRSVP.objects.get(member=user, rsvp_event=event)
                            rsvp.affiliation = new_aff
                            rsvp.save()
                        else:
                            EventRSVP.objects.create(
                                member=user, rsvp_event=event, affiliation=new_aff)
                    if int(user_id) in attending_guests or int(user_id) in checkedin_guests:
                        guest = GuestRSVP.objects.get(id=user_id)
                        if EventRSVP.objects.filter(guest=guest, rsvp_event=event).exists():
                            rsvp = EventRSVP.objects.get(guest=guest, rsvp_event=event)
                            rsvp.affiliation = new_aff
                            rsvp.save()
                        else:
                            EventRSVP.objects.create(
                                guest=guest, rsvp_event=event, affiliation=new_aff)
                elif 'edit-school-' in data:
                    new_school = request.POST.get(data)
                    if int(user_id) in attending_members or int(user_id) in checkedin_members:
                        user = StartupMember.objects.get_user(user=UserEmail.objects.get(id=user_id))
                        if EventRSVP.objects.filter(member=user, rsvp_event=event).exists():
                            rsvp = EventRSVP.objects.get(member=user, rsvp_event=event)
                            rsvp.school = new_school
                            rsvp.save()
                        else:
                            EventRSVP.objects.create(
                                member=user, rsvp_event=event, school=new_school)
                    if int(user_id) in attending_guests or int(user_id) in checkedin_guests:
                        guest = GuestRSVP.objects.get(id=user_id)
                        if EventRSVP.objects.filter(guest=guest, rsvp_event=event).exists():
                            rsvp = EventRSVP.objects.get(guest=guest, rsvp_event=event)
                            rsvp.school = new_school
                            rsvp.save()
                        else:
                            EventRSVP.objects.create(
                                guest=guest, rsvp_event=event, school=new_school)

        return HttpResponseRedirect(reverse('uni-manage-events-rsvp-checkin', kwargs={'event_id': event_id}))


class CustomizeEventEmails(EventOnlyDashboardView):
    """
    Allows admin users to customize the following event email messages:
        RSVP confirmation for members
        RSVP confirmation for guests (if applicable)
        Reminder of upcoming event (both 1 day before and a specified number
            of days before)
    """
    def post(self, request, university, event_id):
        event = None
        if event_id:
            event = Event.objects.get(event_id=event_id)

            m_rsvp_conf = event.member_rsvp_confirmation_email
            g_rsvp_conf = event.guest_rsvp_confirmation_email
            reminder_email = event.reminder_email

            if university.program_name is not None:
                program_name = university.program_name
            else:
                program_name = university.name

            if university.short_name == "cornell":
                rsvped_word = "registered"
                rsvping_word = "registering"
            else:
                rsvped_word = "RSVPed"
                rsvping_word = "RSVPing"

            job_word = request.university_style.job_word
            if job_word.endswith('y'):
                plural_jobs = job_word[:-1]
                plural_jobs += 'ies'
            elif job_word.endswith(('s', 'x', 'z', 'ch', 'sh')):
                plural_jobs = job_word + 'es'
            else:
                plural_jobs = job_word + 's'

            default_m_rsvp_confirmation = "We're excited to see you at {0}.<br><br>\
            Here are the details for your event:<br>\
            <a href=\"{1}\">{0}</a><br>\
            {2}<br>\
            {3} to {4} ({5})<br><br>\
            Need a reminder? Click below to add this event to your calendar!".format(
                event.name,
                event.get_full_url(),
                event.location,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone)

            default_g_rsvp_confirmation = "Thank you for {0} for {1}. \
            We look forward to seeing you there!<br><br>\
            Event date: {2} to {3} ({4})<br>\
            Event location: {5}<br><br>\
            Want to stay informed about more events like this in your community? \
            Join {6}'s StartupTree, the #1 platform for university \
            entrepreneurship. From your account, you can apply for {7}, \
            meet potential team members, and have unlimited access to all the \
            entrepreneurship resources {6} has to offer. \
            And best of all, it's free!".format(
                rsvping_word,
                event.name,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone,
                event.location,
                program_name,
                plural_jobs)

            default_reminder = "We're sending this to remind you that \
            an event you {0} for, {1}, is happening soon.<br><br>\
            When: {2} to {3} ({4})<br>\
            Where: {5}<br><br>\
            We can't wait to see you there!".format(
                rsvped_word,
                event.name,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone,
                event.location)

            member_rsvp_confirmation_email = EmailEditorForm(request.POST)
            if member_rsvp_confirmation_email.is_valid():
                member_rsvp_confirmation_email_body = member_rsvp_confirmation_email.cleaned_data['email_text']
                if member_rsvp_confirmation_email_body == '<br>':
                    member_rsvp_confirmation_email_body = default_m_rsvp_confirmation
            else:
                member_rsvp_confirmation_email_body = default_m_rsvp_confirmation

            guest_rsvp_confirmation_email = EmailEditorForm2(request.POST)
            if guest_rsvp_confirmation_email.is_valid():
                guest_rsvp_confirmation_email_body = guest_rsvp_confirmation_email.cleaned_data['email_text2']
                if guest_rsvp_confirmation_email_body == '<br>':
                    guest_rsvp_confirmation_email_body = default_g_rsvp_confirmation
            else:
                guest_rsvp_confirmation_email_body = default_g_rsvp_confirmation

            reminder_email = TextEditorForm2(request.POST)
            if reminder_email.is_valid():
                reminder_email_body = reminder_email.cleaned_data['text']
                if reminder_email_body == '<br>':
                    reminder_email_body = default_reminder
            else:
                reminder_email_body = default_reminder

            event.member_rsvp_confirmation_email = member_rsvp_confirmation_email_body
            event.guest_rsvp_confirmation_email = guest_rsvp_confirmation_email_body
            event.reminder_email = reminder_email_body
            event.save()

            messages.add_message(request, messages.SUCCESS, 'Email(s) successfully saved!')

        return self.get(request, university, event_id)

    def get(self, request, university, event_id):
        event = None
        if event_id:
            event = Event.objects.get(event_id=event_id)

            editable_reminder = (event.send_emails_days_prior > 0) or university.style.send_rsvp_reminders
            if not editable_reminder and not event.allow_rsvp:
                return HttpResponseRedirect(reverse('uni-manage-events-view', kwargs={'event_id': event_id}))

            m_rsvp_conf = event.member_rsvp_confirmation_email
            g_rsvp_conf = event.guest_rsvp_confirmation_email
            reminder_email = event.reminder_email

            if university.program_name is not None:
                program_name = university.program_name
            else:
                program_name = university.name

            if university.short_name == "cornell":
                rsvped_word = "registered"
                rsvping_word = "registering"
            else:
                rsvped_word = "RSVPed"
                rsvping_word = "RSVPing"

            job_word = request.university_style.job_word
            if job_word.endswith('y'):
                plural_jobs = job_word[:-1]
                plural_jobs += 'ies'
            elif job_word.endswith(('s', 'x', 'z', 'ch', 'sh')):
                plural_jobs = job_word + 'es'
            else:
                plural_jobs = job_word + 's'

            default_m_rsvp_confirmation = "We're excited to see you at {0}.<br><br>\
            Here are the details for your event:<br>\
            <a href=\"{1}\">{0}</a><br>\
            {2}<br>\
            {3} to {4} ({5})<br><br>\
            Need a reminder? Click below to add this event to your calendar!".format(
                event.name,
                event.get_full_url(),
                event.location,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone)

            default_g_rsvp_confirmation = "Thank you for {0} for {1}. \
            We look forward to seeing you there!<br><br>\
            Event date: {2} to {3} ({4})<br>\
            Event location: {5}<br><br>\
            Want to stay informed about more events like this in your community? \
            Join {6}'s StartupTree, the #1 platform for university \
            entrepreneurship. From your account, you can apply for {7}, \
            meet potential team members, and have unlimited access to all the \
            entrepreneurship resources {6} has to offer. \
            And best of all, it's free!".format(
                rsvping_word,
                event.name,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone,
                event.location,
                program_name,
                plural_jobs)

            default_reminder = "We're sending this to remind you that \
            an event you {0} for, {1}, is happening soon.<br><br>\
            When: {2} to {3} ({4})<br>\
            Where: {5}<br><br>\
            We can't wait to see you there!".format(
                rsvped_word,
                event.name,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone,
                event.location)

            if m_rsvp_conf is None or m_rsvp_conf == '':
                member_rsvp_confirmation_email_body = EmailEditorForm({'email_text': default_m_rsvp_confirmation})
            else:
                member_rsvp_confirmation_email_body = EmailEditorForm({'email_text': m_rsvp_conf})

            if g_rsvp_conf is None or g_rsvp_conf == '':
                guest_rsvp_confirmation_email_body = EmailEditorForm2({'email_text2': default_g_rsvp_confirmation})
            else:
                guest_rsvp_confirmation_email_body = EmailEditorForm2({'email_text2': g_rsvp_conf})

            if reminder_email is None or reminder_email == '':
                reminder_email_body = TextEditorForm2({'text': default_reminder})
            else:
                reminder_email_body = TextEditorForm2({'text': reminder_email})

            return TemplateResponse(
                request,
                'trunk/manage/events/customize_emails.html',
                {
                    'title': 'Custom Event Emails',
                    'page_name': 'customize-event-emails-view',
                    'university': university,
                    'event': event,
                    'member_rsvp_confirmation_email_body': member_rsvp_confirmation_email_body,
                    'guest_rsvp_confirmation_email_body': guest_rsvp_confirmation_email_body,
                    'reminder_email_body': reminder_email_body,
                    'editable_reminder': editable_reminder
                }
            )


@login_required
def invite_users(request, event_id):
    '''invite users via modal'''
    invited_users = []
    emailset = EmailFormset(request.POST)
    for form in emailset:
        if form.is_valid():
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            invited_user = {
                "first_name": first_name,
                "last_name": last_name,
                "email": email
            }
            invited_users.append(invited_user)
    return invite_users_general(request, event_id, invited_users)


@login_required
def invite_users_bulk(request, event_id):
    import_form = ImportForm(request.POST, request.FILES)
    invited_users = []
    if import_form.is_valid():
        '''
        Parses an Excel file and add EmailUser objects in the invitation_queue array
        '''
        try:
            excel_file = request.FILES['file']
            wb = xlrd.open_workbook(file_contents=excel_file.read())
            for sheet in wb.sheets():
                for row in range(sheet.nrows):
                    invitee_email = sheet.cell(row, 0).value.strip()
                    invitee_first = sheet.cell(row, 1).value.strip()
                    invitee_last = sheet.cell(row, 2).value.strip()
                    invited_user = {
                        "first_name": invitee_first,
                        "last_name": invitee_last,
                        "email": invitee_email
                    }
                    invited_users.append(invited_user)
            return invite_users_general(request, event_id, invited_users)
        except xlrd.biffh.XLRDError:
            error = "Unsupported format or corrupt file. \
                Please make sure the file is valid \
                Excel spreadsheets (xls, xlsx)."
        except AssertionError:
            error = "Error: Cannot import the file."
    else:
        error = "An error occured. Please make sure you uploaded the correct file type with correct format."
    request.bulk_invite_error = error
    return TemplateResponse(request, 'trunk/manage/events/general_bulk_invite.html', {
        'error' : error
    })


def invite_users_general(request, event_id, invited_users):
    current_site = request.current_site
    invitor = request.user
    try:
        event = Event.objects.get(event_id=event_id)
        university = event.university
    except Event.DoesNotExist:
        raise Http404
    errors = []
    if request.admin_university or is_comp_app_admin(request, event.is_application):
        if request.method == 'POST':
            tzname = event.timezone

            # store email in a function closure (rationale: python is dynamically scoped)
            make_invitation = lambda email, user_exists: lambda: send_event_invite(
                current_site.id, email, event.id, invitor.startup_member.get_full_name(), user_exists, tzname)

            # Store which emails have been sent invitations for popup modal confirmation.
            emails_st_invited = []
            emails_st_already_invited = []
            emails_not_st_invited = []

            for invited in invited_users:
                email = invited["email"]
                try:
                    user = UserEmail.objects.get(email=email)
                    if user not in event.invited_users.all():
                        event.add_private_user(user)
                        emails_st_invited.append(email)
                        transaction.on_commit(make_invitation(email, True))
                    else:
                        emails_st_already_invited.append(email)
                        transaction.on_commit(make_invitation(email, True))
                    try:
                        sm = StartupMember.objects.get_user(user)
                        if not MemberApplicant.objects.filter(member=sm, event=event).exists():
                            MemberApplicant.objects.create(member=sm, event=event, is_applied=False)
                    except StartupMember.DoesNotExist:
                        pass
                except UserEmail.DoesNotExist:
                    if event.is_competition or event.is_application or event.is_survey:
                        AnonymousApplicant.objects.create(event=event,
                                                          first_name=invited["first_name"],
                                                          last_name=invited["last_name"],
                                                          email=email,
                                                          is_invited=True)
                    emails_not_st_invited.append(email)
                    transaction.on_commit(make_invitation(email, False))

            request.emails_st_invited = emails_st_invited
            request.emails_st_already_invited = emails_st_already_invited
            request.emails_not_st_invited = emails_not_st_invited

        if event.is_competition:
            return view_applicants(request, event_id, university, errors)
        if event.is_survey:
            from trunk.views.surveys import view_respondents
            return view_respondents(request, event_id, university)
        else:
            return ManageRSVPCheckins().get(request, university, event_id)
            # return redirect('uni-manage-events-rsvp-checkin', request, university, event_id)
    else:
        raise PermissionDenied


class BulkInvite(AllEventDashboardView):
    def get(self, request, *args, **kwargs):
        univ = request.is_university
        event_id = kwargs['event_id']

        try:
            event = Event.objects.get(
                event_id=event_id,
                university=univ
            )
        except:
            raise Http404()

        if event.is_survey:
            event_type = "survey"
        elif event.is_competition and not event.is_application:
            event_type = "competiton"
        elif event.is_competition and event.is_application:
            event_type = univ.style.application_word
        else:
            event_type = "event"

        try:
            error = request.bulk_invite_error
        except AttributeError:
            error = None

        return TemplateResponse(request, 'trunk/manage/events/general_bulk_invite.html', {
            'error' : error,
            'event_name' : event.name,
            'event_type' : event_type
        })
    def post(self, request, *args, **kwargs):
        event_id = kwargs['event_id']
        return invite_users_bulk(request, event_id)


class ExportRSVPs(EventOnlyDashboardView):
    """
    Exports a CSV file containing all RSVP data for a specified event.
    """

    def get(self, request, university, event_id):
        try:
            event = Event.objects.get(
                event_id=event_id,
                university=university
            )
        except:
            raise Http404()
        # StartupTree Member RSVPs
        response = export_rsvps(request, event)
        return response


class PublishEvent(EventOnlyDashboardView):
    def get(self, request, university, event_id=None, publish=''):
        event = Event.objects.get(event_id=event_id)
        if not event:
            return redirect(reverse('uni-manage-events'))

        site = request.current_site
        clerk = UniversityActivityClerk(site, university)

        if publish == "true":
            event.publish(clerk)
            event.save()
        elif publish == "false":
            event.unpublish(clerk)
            event.save()
        else:
            app_logger.info("Error detected with publishing - URL get parameter does not contain true or false")
        return redirect(reverse('uni-manage-events'))


class AdminCheckin(EventOnlyDashboardJsonView):
    def post(self, request, university, event_id, user_id):
        assert isinstance(request, HttpRequest)
        event = Event.objects.get(event_id=event_id)
        user = StartupMember.objects.get_user(user=UserEmail.objects.get(id=user_id))
        checkedin_guests = event.checked_in.filter(guest__isnull=False).values_list('guest__email', flat=True)
        if request.POST.get('type') == 'check-in':
            if str(user.user) not in checkedin_guests:
                checkinObject = EventCheckin.objects.create(member=user, checkedin_event=event)
                event.checked_in.add(checkinObject)
                if event.show_custom_checkin:
                    try:
                        rsvp = EventRSVP.objects.get(member=user, rsvp_event=event)
                    except EventRSVP.DoesNotExist:
                        rsvp = EventRSVP.objects.create(member=user, rsvp_event=event)
                        custom_answer_parser = CustomAnswersParser(request.POST, event.university, None, None, event_id, request.FILES, event_user=rsvp)
                        with transaction.atomic():
                            custom_answer_parser.parse_answers()
                        errors = custom_answer_parser.errors
                        try:
                            if errors != {}:
                                raise ValueError()
                        except ValueError:
                            errors = custom_answer_parser.get_errors()
                            app_logger.exception("ERROR: {0}".format(errors))
                if EventCheckin.objects.filter(member=user, checkedin_event=event).count() == 1:
                    send_checkin_confirmation.delay(request.current_site.id, user.first_name, event.id, user.user.email, checkinObject.created)
                if Activity.objects.filter(user=user,
                                           university=university,
                                           site=request.current_site,
                                           obj_event=event,
                                           activity_type=30).count() == 0:
                    clerk = EventActivityClerk(site=request.current_site, user=user, event=event)
                    clerk.record_event_checkin()
            else:
                return JsonResponse({'status': 100})
        elif request.POST.get('type') == 'check-out':
            date = request.POST.get('date')
            if EventCheckin.objects.filter(member=user, checkedin_event=event, created=date).exists():
                checkinObject = EventCheckin.objects.get(member=user, checkedin_event=event, created=date)
                event.checked_in.remove(checkinObject)
                checkinObject.delete()
                if EventRSVP.objects.filter(member=user, rsvp_event=event).exists():
                    EventRSVP.objects.get(member=user, rsvp_event=event).delete()
                if Activity.objects.filter(user=user,
                                           university=University.objects.get(site=request.current_site),
                                           site=request.current_site,
                                           obj_event=event,
                                           activity_type=30).exists():
                    clerk = EventActivityClerk(site=request.current_site, user=user, event=event)
                    clerk.delete_event_checkin()
        return JsonResponse({"status": 203})


class AdminCheckinGuest(EventOnlyDashboardJsonView):
    def post(self, request, university, event_id, guest_id=None):
        assert isinstance(request, HttpRequest)
        if guest_id is None:
            event = Event.objects.get(event_id=event_id)
            checkedin_users = event.checked_in.exclude(member=None).values_list('member__user__email', flat=True)
            form = GuestRSVPForm(request.POST)
            if form.is_valid():
                fname = form.cleaned_data.get('fname')
                lname = form.cleaned_data.get('lname')
                email = form.cleaned_data.get('email')
                affiliation = form.data.get('affiliation')
                school = form.data.get('school', None)
                major = form.cleaned_data.get('major', None)

                domain_list = UniversityEmailDomain.objects.filter(university=university,
                    type=UniversityEmailDomain.GUEST).values_list('domain', flat=True)
                if event.restrict_guest_checkins and len(domain_list) > 0:
                    email_address = email.split('@')[1].strip()
                    if email_address not in domain_list:
                        domain_list_str = ""
                        for domain in domain_list:
                            domain_list_str += "@{0} ".format(domain)
                        messages.add_message(request, messages.ERROR, "Unable to check in. Guest's email address must end with: {0}.".format(
                            domain_list_str), "alert--error")
                        return redirect(reverse('uni-manage-events-rsvp-checkin', kwargs={'event_id': event_id}))
                member = None
                guest = None
                if email not in checkedin_users:
                    if UserEmail.objects.filter(email__iexact=email).exists():
                        member = StartupMember.objects.get_user(user=UserEmail.objects.get(email__iexact=email))
                    else:
                        if GuestRSVP.objects.filter(email__iexact=email):
                            guest = GuestRSVP.objects.filter(email__iexact=email)[0]
                            guest_is_changed = False
                            if guest.first_name != fname:
                                guest.first_name = fname
                                guest_is_changed = True
                            if guest.last_name != lname:
                                guest.last_name = lname
                                guest_is_changed = True
                            if guest_is_changed:
                                guest.save()
                        else:
                            guest = GuestRSVP.objects.create(
                                first_name=fname,
                                last_name=lname,
                                email=email
                            )
                    rsvp = EventRSVP.objects.create(member=member, guest=guest, rsvp_event=event)
                    if guest is not None:
                        if affiliation == '1':
                            rsvp.affiliation = EventRSVP.STUDENT
                        elif affiliation == '2':
                            rsvp.affiliation = EventRSVP.ALUMNI
                        elif affiliation == '3':
                            rsvp.affiliation = EventRSVP.FACULTY
                        elif affiliation == '4':
                            rsvp.affiliation = EventRSVP.STAFF
                        elif affiliation == '5':
                            rsvp.affiliation = EventRSVP.NOT_AFFILIATED

                        if school is not None:
                            rsvp.school = school
                        if major is not None and major != '':
                            rsvp.major = major
                        rsvp.save()
                    checkinObject = EventCheckin.objects.create(member=member, guest=guest, checkedin_event=event)
                    event.checked_in.add(checkinObject)
                    if event.show_custom_checkin:
                        custom_answer_parser = CustomAnswersParser(request.POST, event.university, None, None, event_id, request.FILES, event_user=rsvp)
                        with transaction.atomic():
                            custom_answer_parser.parse_answers()
                        errors = custom_answer_parser.errors
                        try:
                            if errors != {}:
                                raise ValueError()
                        except ValueError:
                            errors = custom_answer_parser.get_errors()
                            app_logger.exception("ERROR: {0}".format(errors))
                    if EventCheckin.objects.filter(member=member, guest=guest, checkedin_event=event).count() == 1:
                        send_checkin_confirmation.delay(
                            request.current_site.id, fname, event.id, email, checkinObject.created)
                else:
                    messages.add_message(request, messages.SUCCESS, '{0} has already been checked into this event as a member.'.format(email))
                    return redirect(reverse('uni-manage-events-rsvp-checkin', kwargs={'event_id': event_id}))
            return redirect(reverse('uni-manage-events-rsvp-checkin', kwargs={'event_id': event_id}))
        else:
            event = Event.objects.get(event_id=event_id)
            guest = GuestRSVP.objects.get(id=guest_id)
            checkedin_users = event.checked_in.exclude(member=None).values_list('member__user__email', flat=True)
            if request.POST.get('type') == 'check-in':
                if str(guest.email) not in checkedin_users:
                    checkinObject = EventCheckin.objects.create(guest=guest, checkedin_event=event)
                    event.checked_in.add(checkinObject)
                else:
                    return JsonResponse({'status': 100})
            elif request.POST.get('type') == 'check-out':
                date = request.POST.get('date')
                if EventCheckin.objects.filter(guest=guest, checkedin_event=event, created=date).exists():
                    checkinObject = EventCheckin.objects.get(guest=guest, checkedin_event=event, created=date)
                    event.checked_in.remove(checkinObject)
            return JsonResponse({"status": 203})


class AdminRSVP(EventOnlyDashboardJsonView):
    """
    CURRENTLY THIS FUNCTION DOES NOTHING EXCEPT BRING IN STARTUPMEMBER DATA FOR CHECKINS
    HOWEVER I AM KEEPING THE CODE HERE BC I ANTICIPATE IN THE FUTURE IT MIGHT BE NEEDED
    FOR ADMIN TO RSVP USERS SHOULD THEY WANT TO DO SO. JUST UNCOMMENT FOR THE FUNCTIONALITY.
    """

    def post(self, request, university):
        assert isinstance(request, HttpRequest)
        event_id = request.POST['event_id']
        email = request.POST['email']
        try:
            # event = Event.objects.get(event_id=event_id)
            member = UserEmail.objects.get(email__iexact=email)
            startupmember = StartupMember.objects.get_user(user=member)
            # if member in event.attending_users.all() or member in event.interested_users.all():
            #     return JsonResponse({'status': 100, 'name': startupmember.get_full_name()});
            # else:
            #     event.attending_users.add(member);
        except Exception:
            return JsonResponse({'status': 500})
        return JsonResponse({"status": 203,
                             'name': startupmember.get_full_name(),
                             'user_id': startupmember.user_id})


class ArchiveUser(EventOnlyDashboardJsonView):
    def post(self, request, university, event_id, action, user_id):
        assert isinstance(request, HttpRequest)
        try:
            event = Event.objects.get(event_id=event_id)
            archived_users = event.archived_users.all().values_list('id', flat=True)
            if action == 'archive' and int(user_id) not in archived_users:
                event.archived_users.add(user_id)
            elif action == 'unarchive' and int(user_id) in archived_users:
                event.archived_users.remove(user_id)
        except Exception:
            return JsonResponse({"status": 500})

        return JsonResponse({"status": 203})


class ArchiveGuest(EventOnlyDashboardJsonView):
    def post(self, request, university, event_id, action, guest_id):
        assert isinstance(request, HttpRequest)
        try:
            event = Event.objects.get(event_id=event_id)
            guest = GuestRSVP.objects.get(id=guest_id)
            if action == 'archive' and not guest.is_archived:
                guest.is_archived = True
                guest.save()
            elif action == 'unarchive' and guest.is_archived:
                guest.is_archived = False
                guest.save()
        except Exception:
            return JsonResponse({"status": 500})

        return JsonResponse({"status": 203})


class EventMetrics(EventOnlyDashboardView):
    @staticmethod
    def get_metric(university, internal_call):
        now = timezone.now()
        five_months_back = month_delta(now, -5)
        events = Event.objects.filter(university_id=university.id,
                                      start__range=(five_months_back, now),
                                      is_template_only=False) \
            .order_by('start') \
            .prefetch_related('attending_users')
        events_by_month = {}
        participants_by_month = {}
        participantIDs_by_month = {}
        guests_by_month = {}
        checkins_by_month = {}
        attending_users = []
        months = []
        for i in range(6):
            tmp = (five_months_back.month + i) % 12
            if tmp == 0:
                tmp = 12
            months.append(tmp)
        for event in events:
            month = event.start.month
            users = event.get_attending_user_without_archive().values_list('id', flat=True)
            guests = event.get_guest_rsvps_without_archive().values_list('id', flat=True)
            checkins = event.checked_in.all().values_list('id', flat=True)
            if month in events_by_month:
                events_by_month[month] += 1
                participantIDs_by_month[month].update(users)
                guests_by_month[month].update(guests)
                checkins_by_month[month].update(checkins)
                participants_by_month[month] = len(participantIDs_by_month[month]) + \
                                               len(guests_by_month[month]) + len(checkins_by_month[month])
            else:
                events_by_month[month] = 1
                participantIDs_by_month[month] = set(users)
                guests_by_month[month] = set(guests)
                checkins_by_month[month] = set(checkins)
                participants_by_month[month] = len(participantIDs_by_month[month]) + \
                                               len(guests_by_month[month]) + len(checkins_by_month[month])
            attending_users += users
            attending_users += guests
            attending_users += checkins
        events_and_month = [[], []]
        participants_and_month = [[], []]
        first_month = five_months_back.month

        for month in months:
            if month in events_by_month:
                month_string = datetime.date(1900, month, 1).strftime('%B')
                events_and_month[0].append(month_string)
                participants_and_month[0].append(month_string)
                events_and_month[1].append(events_by_month[month])
                participants_and_month[1].append(participants_by_month[month])
            else:
                month_string = datetime.date(1900, first_month, 1).strftime('%B')
                events_and_month[0].append(month_string)
                participants_and_month[0].append(month_string)
                events_and_month[1].append(0)
                participants_and_month[1].append(0)
            first_month += 1
            if first_month > 12:
                first_month = 1

        # participants = StartupMember.objects.filter(user_id__in=users)
        affiliations = UniversityAffiliation.objects.filter(member__user_id__in=attending_users)
        by_group = []
        by_affiliation = [0, 0, 0, 0, 0]
        for aff in affiliations:
            if aff.affiliation == UniversityAffiliation.UNIVERSITY_STAFF:
                by_affiliation[3] += 1
            elif aff.affiliation == UniversityAffiliation.UNIVERSITY_FACULTY:
                by_affiliation[2] += 1
            elif aff.affiliation == UniversityAffiliation.NOT_UNIVERSITY_PERSON:
                by_affiliation[4] += 1
            elif aff.affiliation == UniversityAffiliation.UNIVERSITY_ALUMNI:
                by_affiliation[1] += 1
            elif aff.affiliation == UniversityAffiliation.UNIVERSITY_STUDENT:
                if aff.is_alumni:
                    by_affiliation[1] += 1
                else:
                    by_affiliation[0] += 1
        if internal_call:
            return sum(by_affiliation)
        return JsonResponse({
            'status': 200,
            'events_by_month': events_and_month,
            'participants_by_month': participants_and_month,
            'by_group': by_group,
            'by_affiliation': by_affiliation
        })

    def get(self, request, university):
        return self.get_metric(university, False)


class DeleteEvent(AllEventDashboardView):
    def post(self, request, university):
        return self.delete(request)

    def delete(self, request):
        try:
            event_id = request.POST.get('event_id')
            event_to_delete = Event.objects.get(event_id=event_id)
            is_competition = event_to_delete.is_competition
            is_application = event_to_delete.is_application
            is_survey = event_to_delete.is_survey
            eboard = event_to_delete.event_board

            uni_style = request.university_style

            # delete the round associated with this event if it exists
            # if eboard:
            #     round_to_delete = eboard.rounds.get(event=event_to_delete)
            #     round_to_delete.delete()

            # delete the activity object that the event corresponds to
            # make sure no error occurs by accouting for possible duplicate activity object
            activities_to_delete = Activity.objects.filter(obj_event=event_to_delete)
            for activity in activities_to_delete:
                activity.delete()
            # delete the Google event, if there is one
            if event_to_delete.auto_conference_link:
                delete_conference_event(event_to_delete)
            # delete the event object
            if event_to_delete.fb_event_id:
                EventsBlacklist.objects.create(fb_event_id=event_to_delete.fb_event_id)
            event_to_delete.delete()
        except Event.DoesNotExist:
            return HttpResponseRedirect(reverse('uni-manage-events'))
        if is_application:
            messages.add_message(request, messages.SUCCESS, '{0} successfully deleted!'.format(uni_style.application_word.capitalize()))
            return HttpResponseRedirect(reverse('uni-manage-applications'))
        elif is_competition:
            messages.add_message(request, messages.SUCCESS, 'Competition successfully deleted!')
            return HttpResponseRedirect(reverse('uni-manage-competitions'))
        elif is_survey:
            messages.add_message(request, messages.SUCCESS, 'Survey successfully deleted!')
            return HttpResponseRedirect(reverse('uni-manage-surveys'))
        else:
            messages.add_message(request, messages.SUCCESS, 'Event successfully deleted!')
        return HttpResponseRedirect(reverse('uni-manage-events'))


# revert-REQ-585
class EventDecision(AllEventDashboardView):
    def post(self, request, university):
        event_id = request.POST.get('event_id')
        decision = request.POST.get('decision')
        event = Event.objects.get(event_id=event_id)
        current_site = request.current_site
        # rejected
        if decision == "2":
            event.status = 2
            event.save()
        # approved
        else:
            event.status = 1
            event.save()
            if event.is_published and not event.is_private and not event.is_archived_event and event.display_activity_feed:
                university = request.university
                event_activity = UniversityActivityClerk(current_site, university)
                event_activity.record_event_updated(event)
                event_notification = NotificationClerk(current_site, university)
                transaction.on_commit(
                    lambda: event_notification.create_new_event_noti.delay(event_notification.serialize(), event.id)
                )
        notify_event_creator(event.id, current_site.id)
        return HttpResponseRedirect(reverse('uni-manage-events'))


def get_shared_url(request, event_id):
    event = Event.objects.get(event_id=event_id)
    domain = request.current_site.domain
    does_match = match_url(event.name, event.shared_url)
    if event.shared_url is None or not does_match:
        shared_url = create_shared_url(event.name)
        event.shared_url = shared_url
        event.save()
    full_url = domain + event.get_shared_url()
    return JsonResponse({"shared_url": full_url})


def match_url(name, shared_url):
    max_length = 20
    alpha_name = re.sub(r'\W+', '', name)[:max_length]
    if shared_url is not None:
        return alpha_name in shared_url
    return False


def create_shared_url(name):
    max_length = 20
    alpha_name = re.sub(r'\W+', '', name)[:max_length]
    count = 1
    new_name = alpha_name
    event_name_exists = False
    try:
        event = Event.objects.get(shared_url=new_name)
        event_name_exists = True
    except:
        pass

    while event_name_exists:
        try:
            event = Event.objects.get(shared_url=new_name)
            new_name = alpha_name + str(count)
            count = count + 1
        except:
            event_name_exists = False
    return new_name


class EditEvent(AllEventDashboardView):
    template = "trunk/manage/events/create_edit.html"

    def post(self, request, university, event_id):
        uni_style = university.style
        event = Event.objects.get(event_id=event_id)
        is_survey = event.is_survey
        event_description = TinyMCEWidget(request.POST)
        is_application = False
        can_restrict_access = UniversityEmailDomain.objects.filter(
            university=university, type=UniversityEmailDomain.GUEST)\
            .count() > 0
        if event_description.is_valid():
            description = event_description.cleaned_data['content']
        else:
            description = ''
        event_name = request.POST.get('event_name')
        template_name = request.POST.get('template_name', event_name)
        start_time = request.POST.get('event_start_time')
        if start_time == '' and is_survey:
            start_time = '12:01 AM'
        end_time = request.POST.get('event_end_time')
        if end_time == '' and is_survey:
            end_time = '11:59 PM'
        tz = request.POST.get('event_timezone', None)
        if tz is None:
            tz = university.style.timezone
        start = timezone.make_aware(
            get_date_time_object(
                request.POST.get('event_start_date'), start_time),
            pytz.timezone(tz))
        end = timezone.make_aware(
            get_date_time_object(
                request.POST.get('event_end_date'), end_time),
            pytz.timezone(tz))
        location = request.POST.get('event_location')
        map_location = request.POST.get('event_map')
        display_view_count = request.POST.get('viewcount_checkbox')
        allow_rsvp = request.POST.get('rsvp_checkbox')
        allow_max_rsvp = request.POST.get('max_rsvp_checkbox')
        max_rsvp = request.POST.get('num_rsvps')
        enable_waitlist = request.POST.get('enable_waitlist')
        allow_checkins = request.POST.get('checkin_checkbox')
        allow_guest_rsvp = request.POST.get('guest_rsvp_checkbox')
        restrict_guest_rsvp = request.POST.get('restrict_guest_rsvp_checkbox')
        show_custom_rsvp = request.POST.get('show_custom_questions_rsvp_checkbox')
        show_custom_checkin = request.POST.get('show_custom_questions_checkin_checkbox')
        allow_guest_checkins = request.POST.get('guest_checkin_checkbox')
        restrict_guest_checkins = request.POST.get('restrict_guest_checkin_checkbox')
        free_food = request.POST.get('food_checkbox')
        free_swag = request.POST.get('swag_checkbox')
        allow_group_sharing = request.POST.get('group_sharing')
        allow_global_sharing = request.POST.get('global_sharing')
        latitude = request.POST.get("latitude")
        longitude = request.POST.get("longitude")
        event_type = request.POST['event-type']
        event_website = request.POST.get('event_website', '')
        ticket_website = request.POST.get('ticket_website', '')

        display_activity_feed = request.POST.get('activity_checkbox')
        pin_to_feed = request.POST.get('pinned_checkbox')
        display_attendees = request.POST.get('attendees_checkbox')
        display_applicants = request.POST.get('applicant_checkbox')
        is_published = request.POST.get('publish_checkbox')
        is_private = request.POST.get('private_checkbox')
        is_archived_event = request.POST.get('archive-forms')

        save_as_template = request.POST.get('save_as_template')
        save_as_event = request.POST.get('save_as_event')
        from_template = request.POST.get('from_template', None)
        template_id = request.POST.get('template_id', None)
        send_emails_days_prior = request.POST.get('email_remind_days_prior', 0)
        send_updates = request.POST.get('update_checkbox')
        send_single_update = request.POST.get('send-update')

        curr_location = request.POST.get('curr_location')
        curr_map_location = request.POST.get('curr_map_location')
        curr_start_date = request.POST.get('curr_start_date')
        curr_start_time = request.POST.get('curr_start_time')
        curr_end_date = request.POST.get('curr_end_date')
        curr_end_time = request.POST.get('curr_end_time')
        curr_timezone = request.POST.get('curr_timezone')

        redirect_to = request.POST.get('redirect_to')

        is_public_competition = request.POST.get('isPublicCompetition_checkbox')

        auto_generate_conference_link = request.POST.get('auto_conference')

        chk_google_meet_link = request.POST.get('chk_google_meet_link')
        chk_own_link = request.POST.get('chk_own_link')
        input_own_link = request.POST.get('input_own_link')

        if chk_google_meet_link is None:
            chk_google_meet_link = False
        elif chk_google_meet_link == "on":
            chk_google_meet_link = True
        else:
            chk_google_meet_link = False

        if chk_own_link is None:
            chk_own_link = False
        elif chk_own_link == "on":
            chk_own_link = True
        else:
            chk_own_link = False

        if auto_generate_conference_link is None or event.is_competition:
            auto_generate_conference_link = False
        elif auto_generate_conference_link == "on":
            auto_generate_conference_link = True
        else:
            auto_generate_conference_link = False

        if is_public_competition is None:
            is_public_competition = False
        elif is_public_competition == "on":
            is_public_competition = True
        else:
            is_public_competition = False
        app_or_comp = request.POST.get('app_or_comp')
        if app_or_comp == 'application':
            is_application = True
        if event_website[:8] != 'https://' and event_website[:7] != 'http://' \
                and event_website is not "":
            event_website = 'https://' + event_website

        if ticket_website[:8] != 'https://' and ticket_website[:7] != 'http://' \
                and ticket_website is not "":
            ticket_website = 'https://' + ticket_website

        if event_type == 'careerfair':
            event_type = 1
        elif event_type == 'conference':
            event_type = 2
        elif event_type == 'networking':
            event_type = 3
        elif event_type == 'product':
            event_type = 4
        elif event_type == 'talk':
            event_type = 5
        elif event_type == 'workshop':
            event_type = 6
        elif event_type == 'fundraising':
            event_type = 7
        elif event_type == 'hackathon':
            event_type = 8
        elif event_type == 'competition':
            event_type = 9
        elif event_type == 'application':
            event_type = 10
        elif event_type == 'demo':
            event_type = 11
        elif event_type == 'other':
            event_type = 12

        if display_activity_feed == 'on':
            display_activity_feed = True
        elif display_activity_feed is None:
            display_activity_feed = False
        else:
            display_activity_feed = False
        if pin_to_feed == 'on':
            pin_to_feed = True
        elif pin_to_feed is None:
            pin_to_feed = False
        else:
            pin_to_feed = False
        if display_attendees is None:
            display_attendees = False
        elif display_attendees == 'on':
            display_attendees = True
        else:
            display_attendees = False
        if display_view_count is None:
            display_view_count = False
        elif display_view_count == 'on':
            display_view_count = True
        else:
            display_view_count = False
        if display_applicants is None:
            display_applicants = False
        elif display_applicants == 'on':
            display_applicants = True
        else:
            display_applicants = False
        if event.is_competition:
            allow_rsvp = True
        elif allow_rsvp is None:
            allow_rsvp = False
        elif allow_rsvp == 'on':
            allow_rsvp = True
        else:
            allow_rsvp = False
        if allow_max_rsvp is None:
            allow_max_rsvp = False
        elif allow_max_rsvp == 'on':
            allow_max_rsvp = True
        else:
            allow_max_rsvp = False
        if enable_waitlist is None:
            enable_waitlist = False
        elif enable_waitlist == 'on':
            enable_waitlist = True
        else:
            enable_waitlist = False
        if allow_checkins is None:
            allow_checkins = False
        elif allow_checkins == 'on':
            allow_checkins = True
        else:
            allow_checkins = False
        if allow_guest_rsvp is None:
            allow_guest_rsvp = False
        elif allow_guest_rsvp == 'on':
            allow_guest_rsvp = True
        else:
            allow_guest_rsvp = False
        if restrict_guest_rsvp is None:
            restrict_guest_rsvp = False
        elif restrict_guest_rsvp == 'on':
            restrict_guest_rsvp = True
        else:
            restrict_guest_rsvp = False
        if show_custom_rsvp is None:
            show_custom_rsvp = False
        elif show_custom_rsvp == 'on':
            show_custom_rsvp = True
        else:
            show_custom_rsvp = False
        if show_custom_checkin is None:
            show_custom_checkin = False
        elif show_custom_checkin == 'on':
            show_custom_checkin = True
        else:
            show_custom_checkin = False
        custom_questions_turned_on = False
        if not (event.show_custom_rsvp or event.show_custom_checkin) and \
                (show_custom_rsvp or show_custom_checkin):
            custom_questions_turned_on = True
        if allow_guest_checkins is None:
            allow_guest_checkins = False
        elif allow_guest_checkins == 'on':
            allow_guest_checkins = True
        else:
            allow_guest_checkins = False
        if restrict_guest_checkins is None:
            restrict_guest_checkins = False
        elif restrict_guest_checkins == 'on':
            restrict_guest_checkins = True
        else:
            restrict_guest_checkins = False
        if free_food is None:
            free_food = False
        elif free_food == 'on':
            free_food = True
        else:
            free_food = False
        if free_swag is None:
            free_swag = False
        elif free_swag == 'on':
            free_swag = True
        else:
            free_swag = False
        if allow_group_sharing is None:
            allow_group_sharing = False
        elif allow_group_sharing == 'on':
            allow_group_sharing = True
        else:
            allow_group_sharing = False
        if allow_global_sharing is None:
            allow_global_sharing = False
        elif allow_global_sharing == 'on':
            allow_global_sharing = True
        else:
            allow_global_sharing = False
        if is_published == "on":
            is_published = True
        else:
            is_published = False
        if event.is_competition:
            is_private = event.is_private
        elif is_private == "on":
            is_private = True
        else:
            is_private = False
        if (event.is_competition or event.is_survey) and from_template is not None:
            template_to_edit = Event.objects.get(event_id=from_template)
        else:
            template_to_edit = None
        if save_as_template == "true":
            save_as_template = True
        else:
            save_as_template = False
        if save_as_event == "true":
            save_as_event = True
        else:
            save_as_event = False
        send_emails_days_prior = validate_nonnegative_input(send_emails_days_prior)
        if send_updates == "on":
            send_updates = True
        else:
            send_updates = False
        if send_single_update == "true":
            send_single_update = True
        else:
            send_single_update = False
        try:
            latitude = float(latitude)
            longitude = float(longitude)
        except (ValueError, TypeError) as error:
            latitude = None
            longitude = None
        if event.fb_event_id:
            EventsBlacklist.objects.create(fb_event_id=event.fb_event_id)
        is_competition = event.is_competition
        # event_type = request.POST.get('event_type')
        if save_as_template and template_to_edit is not None:
            # a new event has already been created, so need to delete it
            event.delete()
            event = template_to_edit

        event.name = event_name
        event.is_application = is_application
        event.start = start
        event.end = end
        event.timezone = tz
        event.location = location
        event.map_location = map_location
        event.university = university
        event.description = description
        event.is_competition = is_competition
        event.display_activity_feed = display_activity_feed
        event.pin_to_feed = pin_to_feed
        event.auto_generate_link = auto_generate_conference_link
        event.display_attendees = display_attendees
        event.display_applicants = display_applicants
        event.display_view_count = display_view_count
        event.allow_rsvp = allow_rsvp
        event.allow_max_rsvp = allow_max_rsvp
        event.max_rsvp = max_rsvp
        event.enable_waitlist = enable_waitlist
        event.allow_checkins = allow_checkins
        event.allow_guest_rsvp = allow_guest_rsvp
        event.restrict_guest_rsvp = restrict_guest_rsvp
        event.show_custom_rsvp = show_custom_rsvp
        event.show_custom_checkin = show_custom_checkin
        event.allow_guest_checkins = allow_guest_checkins
        event.restrict_guest_checkins = restrict_guest_checkins
        event.free_food = free_food
        event.free_swag = free_swag
        event.allow_group_sharing = allow_group_sharing
        event.allow_global_sharing = allow_global_sharing
        event.latitude = latitude
        event.longitude = longitude
        event.send_emails_days_prior = send_emails_days_prior
        event.send_updates = send_updates
        event.event_type = event_type
        event.event_website = event_website
        event.ticket_website = ticket_website
        event.is_public_competition = is_public_competition
        event.updated = timezone.now()

        event.chk_google_meet_link = chk_google_meet_link
        event.chk_own_link = chk_own_link
        if chk_own_link == True:
            event.input_own_link = input_own_link.strip()


        if event.auto_conference_link and not auto_generate_conference_link:
            # delete_conference_event(event) removing because of random bug
            event.auto_conference_link = None
            event.save()

        can_edit_questions = event.get_can_edit_questions()
        can_edit_rubric = event.get_can_edit_rubric()

        if not event.allow_max_rsvp:
            event.max_rsvp = None

        if can_edit_questions is None:
            event.can_edit_questions = False
        else:
            event.can_edit_questions = can_edit_questions
        if can_edit_rubric is None:
            event.can_edit_rubric = False
        else:
            event.can_edit_rubric = can_edit_rubric

        if (event.is_published and not is_published) or (not event.is_private and is_private):
            event_unpublish_clerk = UniversityActivityClerk(request.current_site,
                                                            university)
            try:
                with transaction.atomic():
                    event_unpublish_clerk.record_event_unpublished(event)
            except:
                pass
        if event.is_published and not is_published:
            event.is_published = False
        elif not event.is_published and is_published:
            event.is_published = True
        if event.is_private and not is_private:
            event.is_private = False
        elif not event.is_private and is_private:
            event.is_private = True

        try:
            process_event(request, event, True, generate_link=auto_generate_conference_link)
            event_create_error = 's'
        except Exception as e:
            app_logger.error(str(e))
            event_create_error = 'e'

        new_event = None
        if save_as_event and save_as_template:
            if event.is_template_only:
                is_template_only = False
                name = event_name
            else:
                is_template_only = True
                name = template_name
            kwargs = {
                'name': name,
                'description': description,
                'start': start,
                'end': end,
                'timezone': tz,
                'location': location,
                'map_location': map_location,
                'university': university,
                'is_application': is_application,
                'event_type': event_type,
                'display_activity_feed': display_activity_feed,
                'pin_to_feed': pin_to_feed,
                'allow_checkins': allow_checkins,
                'allow_rsvp': allow_rsvp,
                'allow_max_rsvp': allow_max_rsvp,
                'max_rsvp': max_rsvp,
                'enable_waitlist': enable_waitlist,
                'allow_guest_rsvp': allow_guest_rsvp,
                'restrict_guest_rsvp': restrict_guest_rsvp,
                'show_custom_rsvp': show_custom_rsvp,
                'show_custom_checkin': show_custom_checkin,
                'allow_guest_checkins': allow_guest_checkins,
                'restrict_guest_checkins': restrict_guest_checkins,
                'creator': event.creator,
                'status': event.status,
                'display_attendees': display_attendees,
                'display_view_count': display_view_count,
                'display_applicants': display_applicants,
                'is_published': is_published,
                'is_private': is_private,
                'is_competition': is_competition,
                'is_survey': event.is_survey,
                'free_food': free_food,
                'free_swag': free_swag,
                'allow_group_sharing': allow_group_sharing,
                'allow_global_sharing': allow_global_sharing,
                'is_template_only': is_template_only,
                'send_emails_days_prior': send_emails_days_prior,
                'send_updates': send_updates,
                'can_edit_questions': can_edit_questions,
                'can_edit_rubric': can_edit_rubric,
                'latitude': latitude,
                'longitude': longitude,
                'event_website': event_website,
                'ticket_website': ticket_website,
                'is_public_competition': is_public_competition,
                'auto_generate_link' : auto_generate_conference_link,
                'chk_google_meet_link' : chk_google_meet_link,
                'chk_own_link' : chk_own_link,
                'input_own_link' : input_own_link
            }
            # transaction.atomic prevents TransactionManagementError when creating event
            with transaction.atomic():
                new_event = Event.objects.create_event(**kwargs)
            process_event(request, new_event, False, template_id, image=event.image, creator=event.creator, generate_link=auto_generate_conference_link)

        if event.is_published and event.send_updates and send_single_update:
            email_list = []
            if is_competition:
                for m in StartupMember.objects.filter(user__isnull=False, universities__id=university.id)\
                        .exclude(user__in=event.attending_users.all()):
                    email_list.append(m.user.email)
            else:
                for attending in event.get_attending_user_without_archive():
                    email_list.append(attending.email)
                for interested in event.interested_users.all():
                    email_list.append(interested.email)
                for invited in event.invited_users.all():
                    email_list.append(invited.email)
                for checkedin in EventCheckin.objects.filter(checkedin_event=event):
                    if checkedin.member is not None and checkedin.member.user not in email_list:
                        email_list.append(checkedin.member.user.email)
                    elif checkedin.guest is not None and checkedin.guest.email not in email_list:
                        email_list.append(checkedin.guest.email)
                for guest in EventRSVP.objects.filter(rsvp_event=event, guest__isnull=False)\
                        .exclude(guest__email__in=email_list):
                    email_list.append(guest.guest.email)
            transaction.on_commit(lambda: send_event_updated_noti.delay(event.event_id,
                                                                        request.current_site.id, email_list))

        is_application = False
        if redirect_to == 'templates':
            return HttpResponseRedirect(reverse('uni-manage-templates'))
        elif redirect_to == 'dashboard':
            if event.is_template_only and new_event is not None:
                goto_event = new_event
            else:
                goto_event = event

            if event.is_application:
                application_url = reverse('uni-edit-application-question', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(application_url)
            elif is_competition:
                competition_url = reverse('uni-edit-competition-question', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(competition_url)
            elif is_survey:
                survey_url = reverse('uni-edit-survey-question', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(survey_url)
            elif custom_questions_turned_on:
                questions_url = reverse('uni-manage-forms', kwargs={'form_name': 'event', 'event_id': goto_event.event_id})
                return HttpResponseRedirect(questions_url)
            else:
                event_url = reverse('uni-manage-events-view', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(event_url)
        else:
            if event.is_competition:
                self.template = 'trunk/manage/competitions/create_edit.html'
                is_application = event.is_application
            elif event.is_survey:
                self.template = 'trunk/manage/surveys/create_edit.html'
            elif custom_questions_turned_on:
                questions_url = reverse('uni-manage-forms', kwargs={'form_name': 'event', 'event_id': event.event_id})
                return HttpResponseRedirect(questions_url)
        event_dict, event_description = prepare_event(event_id)
        redirect_link = request.POST.get('after-submit', None)
        if redirect_link and event_create_error == 's':
            if is_safe_url(redirect_link, request.current_site.domain):
                return HttpResponseRedirect(redirect_link)
        elif event_create_error == 's' and redirect_to == 'dashboard':
            return HttpResponseRedirect(reverse('uni-manage-events-view',
                kwargs={'event_id': event_id}))
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None

        # TODO Check on re-save for URL

        return TemplateResponse(
            request, self.template,
            {
                'title': 'Events',
                'university': university,
                'event_description': event_description,
                'is_edit': True,
                'is_application': is_application,
                'event': event_dict,
                'uni_labels': uni_labels,
                'can_restrict_access': can_restrict_access,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                'counter': 0,
                'event_create_error': event_create_error,
                'allow_platform_wide_deadline_reminders': university.style.send_platform_wide_deadline_reminders,
            }
        )

    def get(self, request, university, event_id):
        event, event_description = prepare_event(event_id)
        can_restrict_access = UniversityEmailDomain.objects.filter(
            university=university, type=UniversityEmailDomain.GUEST)\
            .count() > 0
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None
        if event is None:
            raise Http404('Event does not exist')
        return TemplateResponse(
            request, self.template,
            {
                'title': 'Events',
                'university': university,
                'event_description': event_description,
                'is_edit': True,
                'event': event,
                'uni_labels': uni_labels,
                'can_restrict_access': can_restrict_access,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                'counter': 0,
                'allow_platform_wide_deadline_reminders': university.style.send_platform_wide_deadline_reminders,
            }
        )


class CreateEvent(AllEventDashboardView):
    # revert-REQ-585
    # def post(self, request, university, is_competition=False, is_user=False, creator_email=None):
    #     return self.create(request, university, is_competition, is_user)
    def post(self, request, university, is_competition=False, is_survey=False):
        return self.create(request, university, is_competition, is_survey)

    def get(self, request, university):
        template = "trunk/manage/events/create_edit.html"
        event_description = TinyMCEWidget()
        is_template = request.GET.get('template', None)
        is_create_template = request.GET.get('as_template', None)
        is_create_template = True if is_create_template == "true" else False
        event = None
        if is_template:
            event_id = is_template
            event, event_description = prepare_event(event_id)
            is_template = True
        can_restrict_access = UniversityEmailDomain.objects.filter(
            university=university, type=UniversityEmailDomain.GUEST)\
            .count() > 0
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None
        return TemplateResponse(
            request, template,
            {
                'title': 'Events',
                'event': event,
                'is_template': is_template,
                'is_create_template': is_create_template,
                'uni_labels': uni_labels,
                'can_restrict_access': can_restrict_access,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform,
                'university': university,
                'event_description': event_description,
                'is_edit': False,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
            }
        )

    # revert-REQ-585
    # def create(self, request, university, is_competition, is_user=False, creator_email=None):
    def create(self, request, university, is_competition, is_survey):
        """ Create an event """
        # revert-REQ-585
        # if creator_email != None:
        creator = None
        if request.is_university and request.is_super and \
                (request.is_startuptree_staff or not university.style.restrict_tagged_admins):
            creator = None
            status = 1
        elif request.is_university:
            creator = request.current_member
            status = 1

        current_site = request.current_site
        error_on_save = False

        can_restrict_access = UniversityEmailDomain.objects.filter(
            university=university, type=UniversityEmailDomain.GUEST)\
            .count() > 0

        event_name = request.POST.get('event_name')
        template_name = request.POST.get('template_name', event_name)
        description = TinyMCEWidget(request.POST)
        if description.is_valid():
            description = description.cleaned_data['content']
        else:
            description = ''
        start_time = request.POST.get('event_start_time')
        if start_time == '' and is_survey:
            start_time = '12:01 AM'
        end_time = request.POST.get('event_end_time')
        if end_time == '' and is_survey:
            end_time = '11:59 PM'
        tz = request.POST.get('event_timezone', None)
        if tz is None:
            tz = university.style.timezone
        start = timezone.make_aware(
            get_date_time_object(request.POST.get('event_start_date'), start_time),
            pytz.timezone(tz))
        end = timezone.make_aware(
            get_date_time_object(request.POST.get('event_end_date'), end_time),
            pytz.timezone(tz))
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
        image = request.FILES.get('event_image')
        display_activity_feed = request.POST.get('activity_checkbox')
        pin_to_feed = request.POST.get('pinned_checkbox')
        allow_rsvp = request.POST.get('rsvp_checkbox')
        allow_max_rsvp = request.POST.get('max_rsvp_checkbox')
        max_rsvp = request.POST.get('num_rsvps', 0)
        enable_waitlist = request.POST.get('enable_waitlist')
        allow_checkins = request.POST.get('checkin_checkbox')
        allow_guest_rsvp = request.POST.get('guest_rsvp_checkbox')
        restrict_guest_rsvp = request.POST.get('restrict_guest_rsvp_checkbox')
        show_custom_rsvp = request.POST.get('show_custom_questions_rsvp_checkbox')
        show_custom_checkin = request.POST.get('show_custom_questions_checkin_checkbox')
        allow_guest_checkins = request.POST.get('guest_checkin_checkbox')
        restrict_guest_checkins = request.POST.get('restrict_guest_checkin_checkbox')
        display_attendees = request.POST.get('attendees_checkbox')
        display_view_count = request.POST.get('viewcount_checkbox')
        display_applicants = request.POST.get('applicant_checkbox')
        free_food = request.POST.get('food_checkbox')
        free_swag = request.POST.get('swag_checkbox')
        allow_group_sharing = request.POST.get('group_sharing')
        allow_global_sharing = request.POST.get('global_sharing')
        is_published = request.POST.get('publish_checkbox')
        is_private = request.POST.get('private_checkbox')
        save_as_template = request.POST.get('save_as_template')
        save_as_event = request.POST.get('save_as_event')
        template_id = request.POST.get('template_id', None)
        send_emails_days_prior = request.POST.get('email_remind_days_prior', 0)
        send_updates = request.POST.get('update_checkbox')
        event_type = request.POST['event-type']
        event_website = request.POST.get('event_website')
        ticket_website = request.POST.get('ticket_website')
        app_or_comp = request.POST.get('app_or_comp')
        redirect_to = request.POST.get('redirect_to')
        is_public_competition = request.POST.get('isPublicCompetition_checkbox')
        auto_generate_conference_link = request.POST.get('auto_conference')

        chk_google_meet_link = request.POST.get('chk_google_meet_link')
        chk_own_link = request.POST.get('chk_own_link')
        input_own_link = request.POST.get('input_own_link')

        if auto_generate_conference_link is None or is_competition:
            auto_generate_conference_link = False
        elif auto_generate_conference_link == "on":
            auto_generate_conference_link = True
        else:
            auto_generate_conference_link = False

        if chk_google_meet_link is None:
            chk_google_meet_link = False
        elif chk_google_meet_link == "on":
            chk_google_meet_link = True
        else:
            chk_google_meet_link = False


        if chk_own_link is None:
            chk_own_link = False
        elif chk_own_link == "on":
            chk_own_link = True
        else:
            chk_own_link = False


        if is_public_competition is None:
            is_public_competition = False
        elif is_public_competition == "on":
            is_public_competition = True
        else:
            is_public_competition = False

        is_application = False
        if app_or_comp == 'application':
            is_application = True

        if event_website[:8] != 'https://' and event_website[:7] != 'http://' \
                and event_website is not "":
            event_website = 'https://' + event_website

        if ticket_website[:8] != 'https://' and ticket_website[:7] != 'http://' \
                and ticket_website is not "":
            ticket_website = 'https://' + ticket_website

        if event_type == 'careerfair':
            event_type = 1
        elif event_type == 'conference':
            event_type = 2
        elif event_type == 'networking':
            event_type = 3
        elif event_type == 'product':
            event_type = 4
        elif event_type == 'talk':
            event_type = 5
        elif event_type == 'workshop':
            event_type = 6
        elif event_type == 'fundraising':
            event_type = 7
        elif event_type == 'hackathon':
            event_type = 8
        elif event_type == 'competition':
            event_type = 9
        elif event_type == 'application':
            event_type = 10
        elif event_type == 'demo':
            event_type = 11
        elif event_type == 'other':
            event_type = 12

        can_edit_questions = True
        can_edit_rubric = True
        if display_activity_feed == 'on':
            display_activity_feed = True
        elif display_activity_feed is None:
            display_activity_feed = False
        else:
            display_activity_feed = False
        if pin_to_feed == 'on':
            pin_to_feed = True
        elif pin_to_feed is None:
            pin_to_feed = False
        else:
            display_activity_feed = False
        if is_competition:
            allow_rsvp = True
        elif allow_rsvp is None:
            allow_rsvp = False
        elif allow_rsvp == 'on':
            allow_rsvp = True
        else:
            allow_rsvp = False
        if allow_max_rsvp is None:
            allow_max_rsvp = False
        elif allow_max_rsvp == 'on':
            allow_max_rsvp = True
        else:
            allow_max_rsvp = False
        if enable_waitlist is None:
            enable_waitlist = False
        elif enable_waitlist == 'on':
            enable_waitlist = True
        else:
            enable_waitlist = False
        if allow_checkins is None:
            allow_checkins = False
        elif allow_checkins == 'on':
            allow_checkins = True
        else:
            allow_checkins = False
        if allow_guest_rsvp is None:
            allow_guest_rsvp = False
        elif allow_guest_rsvp == 'on':
            allow_guest_rsvp = True
        else:
            allow_guest_rsvp = False
        if restrict_guest_rsvp is None:
            restrict_guest_rsvp = False
        elif restrict_guest_rsvp == 'on':
            restrict_guest_rsvp = True
        else:
            restrict_guest_rsvp = False
        if show_custom_rsvp is None:
            show_custom_rsvp = False
        elif show_custom_rsvp == 'on':
            show_custom_rsvp = True
        else:
            show_custom_rsvp = False
        if show_custom_checkin is None:
            show_custom_checkin = False
        elif show_custom_checkin == 'on':
            show_custom_checkin = True
        else:
            show_custom_checkin = False
        custom_questions_turned_on = show_custom_rsvp or show_custom_checkin
        if allow_guest_checkins is None:
            allow_guest_checkins = False
        elif allow_guest_checkins == 'on':
            allow_guest_checkins = True
        else:
            allow_guest_checkins = False
        if restrict_guest_checkins is None:
            restrict_guest_checkins = False
        elif restrict_guest_checkins == 'on':
            restrict_guest_checkins = True
        else:
            restrict_guest_checkins = False
        if display_attendees is None:
            display_attendees = False
        elif display_attendees == 'on':
            display_attendees = True
        else:
            display_attendees = False
        if display_view_count is None:
            display_view_count = False
        elif display_view_count == 'on':
            display_view_count = True
        else:
            display_view_count = False
        if display_applicants is None:
            display_applicants = False
        elif display_applicants == 'on':
            display_applicants = True
        else:
            display_applicants = False
        if free_food is None:
            free_food = False
        elif free_food == 'on':
            free_food = True
        else:
            free_food = False
        if free_swag is None:
            free_swag = False
        elif free_swag == 'on':
            free_swag = True
        else:
            free_swag = False
        if allow_group_sharing is None:
            allow_group_sharing = False
        elif allow_group_sharing == 'on':
            allow_group_sharing = True
        else:
            allow_group_sharing = False
        if allow_global_sharing is None:
            allow_global_sharing = False
        elif allow_global_sharing == 'on':
            allow_global_sharing = True
        else:
            allow_global_sharing = False
        if is_published == 'on':
            is_published = True
        else:
            is_published = False
        if is_private == 'on':
            is_private = True
        else:
            is_private = False
        if save_as_template == 'true':
            save_as_template = True
            name = template_name
        else:
            save_as_template = False
            name = event_name
        if save_as_event == 'true':
            save_as_event = True
        else:
            save_as_event = False
        send_emails_days_prior = validate_nonnegative_input(send_emails_days_prior)
        if send_updates == "on":
            send_updates = True
        else:
            send_updates = False

        location = request.POST.get('event_location')
        map_location = request.POST.get('event_map')
        try:
            latitude = float(latitude)
            longitude = float(longitude)
        except (ValueError, TypeError) as error:
            latitude = None
            longitude = None
        if not allow_max_rsvp:
            max_rsvp = None
        kwargs = {
            'name': name,
            'description': description,
            'start': start,
            'end': end,
            'timezone': tz,
            'location': location,
            'map_location': map_location,
            'university': university,
            'is_application': is_application,
            'event_type': event_type,
            'display_activity_feed': display_activity_feed,
            'pin_to_feed': pin_to_feed,
            'allow_checkins': allow_checkins,
            'allow_rsvp': allow_rsvp,
            'allow_max_rsvp': allow_max_rsvp,
            'max_rsvp': max_rsvp,
            'enable_waitlist': enable_waitlist,
            'allow_guest_rsvp': allow_guest_rsvp,
            'restrict_guest_rsvp': restrict_guest_rsvp,
            'show_custom_rsvp': show_custom_rsvp,
            'show_custom_checkin': show_custom_checkin,
            'allow_guest_checkins': allow_guest_checkins,
            'restrict_guest_checkins': restrict_guest_checkins,
            'creator': creator,
            'status': status,
            'display_attendees': display_attendees,
            'display_view_count': display_view_count,
            'display_applicants': display_applicants,
            'is_published': is_published,
            'is_private': is_private,
            'is_competition': is_competition,
            'auto_generate_link':auto_generate_conference_link,
            'is_survey': is_survey,
            'free_food': free_food,
            'free_swag': free_swag,
            'allow_group_sharing': allow_group_sharing,
            'allow_global_sharing': allow_global_sharing,
            'is_template_only': save_as_template,
            'send_emails_days_prior': send_emails_days_prior,
            'send_updates': send_updates,
            'can_edit_questions': can_edit_questions,
            'can_edit_rubric': can_edit_rubric,
            'latitude': latitude,
            'longitude': longitude,
            'event_website': event_website,
            'ticket_website': ticket_website,
            'is_public_competition': is_public_competition,
            'auto_conference_link' : auto_generate_conference_link,
            'chk_google_meet_link' : chk_google_meet_link,
            'chk_own_link' : chk_own_link,
            'input_own_link' : input_own_link

        }
        new_event = Event.objects.create_event(**kwargs)
        process_event(request, new_event, False, None, image=image, creator=creator, generate_link=auto_generate_conference_link)

        if save_as_event and save_as_template:
            if new_event.is_template_only:
                kwargs['is_template_only'] = False
                kwargs['name'] = event_name
            else:
                kwargs['is_template_only'] = True
                kwargs['name'] = template_name
                template_id = None
            new_event_1 = Event.objects.create_event(**kwargs)
            process_event(request, new_event_1, False, template_id, image=image, creator=creator, generate_link=auto_generate_conference_link)

        if redirect_to == 'templates':
            return HttpResponseRedirect(reverse('uni-manage-templates'))
        elif redirect_to == 'dashboard':
            if new_event.is_template_only and new_event_1 is not None:
                goto_event = new_event_1
            else:
                goto_event = new_event

            if new_event.is_application:
                application_url = reverse('uni-edit-application-question', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(application_url)
            elif is_competition:
                competition_url = reverse('uni-edit-competition-question', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(competition_url)
            elif is_survey:
                survey_url = reverse('uni-edit-survey-question', kwargs={'event_id': goto_event.event_id})
                return HttpResponseRedirect(survey_url)
            elif custom_questions_turned_on:
                questions_url = reverse('uni-manage-forms', kwargs={'form_name': 'event', 'event_id': goto_event.event_id})
                return HttpResponseRedirect(questions_url)
            else:
                response = reverse('uni-manage-events-view', kwargs={'event_id': goto_event.event_id})
        else:
            messages.add_message(request, messages.SUCCESS, 'Template was created successfully!')
            if new_event.is_application:
                response = reverse('uni-edit-application', kwargs={'event_id': new_event.event_id})
            elif is_competition:
                response = reverse('uni-edit-competition', kwargs={'event_id': new_event.event_id})
            elif is_survey:
                response = reverse('uni-edit-survey', kwargs={'event_id': new_event.event_id})
            elif custom_questions_turned_on:
                response = reverse('uni-manage-forms', kwargs={'form_name': 'event', 'event_id': new_event.event_id})
            else:
                response = reverse('uni-manage-events-edit', kwargs={'event_id': new_event.event_id})
        # if university is not None:
        #     response = redirect('uni-manage-events')
        # else:
        #     response = redirect('discover_events')
        if error_on_save:
            response += "?s=e"
        else:
            response += "?s=s"
        return HttpResponseRedirect(response)


def process_event(request, event, is_edit, template_id=None, image=None, creator=None, generate_link=False):
    current_site = request.current_site
    university = request.university
    uni_style = request.university_style
    if is_edit:
        valiadte = EventImageForm({'event_image': request.FILES.get('event_image')}, request.FILES)
        if valiadte.is_valid():
            image = request.FILES.get('event_image')
            create_event_thumbnails(image, event)
        else:
            app_logger.warning(valiadte.errors)

        if event.is_template_only:
            event.tags.clear()
        else:
            # Remove all tags that person has access to, and then add back the ones they selected.
            manageable_tags = get_manageable_tags(request)
            event.tags.remove(*manageable_tags)
            for tag in Label.objects.filter(university=university, field_type="groups"):
                if str(tag).replace(" ", "") in request.POST:
                    tag = str(tag).lower().strip()
                    try:
                        t = Label.objects.get_label(tag, university, "groups")
                    except Label.DoesNotExist:
                        continue
                    event.tags.add(t)
        event.save()

        if event.is_published and not event.is_private:
            event_activity = UniversityActivityClerk(current_site,
                                                     university)
            event_activity.record_updated_event_info(event)
    else:
        valiadte = EventImageForm({'event_image': request.POST.get('event_image')}, request.FILES)
        if valiadte.is_valid() and image is not None:
            create_event_thumbnails(image, event)
        if image is None and 'existing_image' in request.POST:
            image_url = request.POST.get('existing_image')
            image_url = image_url.split(settings.MEDIA_URL)[1]
            event.image = image_url
            medium_image_url = request.POST.get('existing_image2')
            medium_image_url = medium_image_url.split(settings.MEDIA_URL)[1]
            event.image_medium = medium_image_url
            small_image_url = request.POST.get('existing_image3')
            small_image_url = small_image_url.split(settings.MEDIA_URL)[1]
            event.image_small = small_image_url
            event.save()

        if not event.is_template_only:
            for tag in Label.objects.filter(university=university, field_type="groups"):
                if str(tag).replace(" ", "") in request.POST:
                    tag = str(tag).lower().strip()
                    try:
                        t = Label.objects.get_label(tag, university, "groups")
                    except Label.DoesNotExist:
                        continue
                    event.tags.add(t)

        if event.is_published and event.display_activity_feed and not event.is_private:
            event_activity = UniversityActivityClerk(current_site, university)
            event_activity.record_event_updated(event)
            event_notification = NotificationClerk(current_site, university)
            transaction.on_commit(
                lambda: event_notification.create_new_event_noti.delay(event_notification.serialize(), event.id)
            )

    if generate_link and event.is_published and not event.is_template_only:
        central_events_calendar = None
        if uni_style.central_events_calendar and uni_style.central_events_calendar != "":
            central_events_calendar = uni_style.central_events_calendar
        create_or_update_conference_event(event, invite=central_events_calendar)
        if IS_DEV and not IS_STAGING:
            user = UserEmail.objects.get(id=MIDDLE_MAN_EMAILUSER_ID_DEV)
        else:
            user = UserEmail.objects.get(email=AUTO_CONFERENCE_DEFAULT_EMAIL)
        cronofy = establish_cronofy_for_user(user)
        cronofy_events = cronofy.read_events(from_date=event.start)
        for c_event in cronofy_events:
            if c_event.get('event_id') and int(c_event['event_id']) == event.id:
                try:
                    conference_link = c_event['conferencing']['join_url']
                except:
                    conference_link = "https://www.startuptree.co/"
                event.auto_conference_link = conference_link
                event.save()

    if template_id is not None and template_id != "" and template_id != event.event_id:
        try:
            template = Event.objects.get(event_id=template_id)
            if template.form:
                form = Form.objects.create(
                    form=Form.EVENT,
                    university=university
                )
                event.form = form
                event.save()
                if template.form.questions:
                    questions = []
                    choices = []
                    for q in template.form.questions.all().order_by('number', 'id'):
                        options = q.choices.all()
                        old_q_id = q.id
                        q.pk = None
                        q.id = None
                        q.save()
                        new_q_id = q.id
                        questions.append({
                            "old_q_id": old_q_id,
                            "new_q_id": new_q_id
                        })
                        if q.q_type == 1 or q.q_type == 2:
                            if q.choices:
                                for c in options:
                                    if c.goto_question:
                                        goto_id = c.goto_question.id
                                    else:
                                        goto_id = None
                                    old_c_id = c.id
                                    q.choices.remove(c)
                                    c.pk = None
                                    c.id = None
                                    c.save()
                                    new_c_id = c.id
                                    if goto_id is not None:
                                        choices.append({
                                            "old_q_id": goto_id,
                                            "new_c_id": new_c_id
                                        })
                                    q.add_choice(c)
                        q.save()
                        form.questions.add(q)
                        form.save()
                    for choice in choices:
                        new_choice = Choice.objects.get(id=choice["new_c_id"])
                        for question in questions:
                            if question["old_q_id"] == choice["old_q_id"]:
                                new_question = Question.objects.get(id=question["new_q_id"])
                                new_choice.goto_question = new_question
                                new_choice.save()
                if (template.is_competition or template.is_application) and template.form.rubrics:
                    for r in template.form.rubrics.all().order_by('number'):
                        r_options = r.choices.all()
                        r.pk = None
                        r.id = None
                        r.save()
                        if r.r_type == 1 or r.r_type == 2:
                            if r.choices:
                                for c in r_options:
                                    r.choices.remove(c)
                                    c.pk = None
                                    c.id = None
                                    c.save()
                                    r.add_choice(c)
                        r.save()
                        form.rubrics.add(r)
                        form.save()
        except Event.DoesNotExist:
            pass

