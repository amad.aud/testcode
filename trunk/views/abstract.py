from django.views import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from trunk.decorators import only_university, only_university_json, \
    at_least_events, at_least_events_json, at_least_competitions, \
    at_least_competitions_json, any_event_type


class UniversityDashboardView(View):
    @method_decorator(only_university)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UniversityDashboardView, self).dispatch(*args, **kwargs)


class EventOnlyDashboardView(View):
    ''' Same purpose as UniversityDashboardView, except only restricts access
    to those with the "events" access level.
    '''
    @method_decorator(at_least_events)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EventOnlyDashboardView, self).dispatch(*args, **kwargs)


class CompetitionOnlyDashboardView(View):
    ''' Same purpose as UniversityDashboardView, except only restricts access
    to those with the "applications" access level.
    '''
    @method_decorator(at_least_competitions)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompetitionOnlyDashboardView, self).dispatch(*args, **kwargs)


class AllEventDashboardView(View):
    ''' For classes that are shared between events, competitions, and surveys
    (create, edit, delete). Allows access to any user with access to either
    events, competitions, or surveys.
    '''
    @method_decorator(any_event_type)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AllEventDashboardView, self).dispatch(*args, **kwargs)


class UniversityDashboardJsonView(View):
    @method_decorator(only_university_json)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UniversityDashboardJsonView, self).dispatch(*args, **kwargs)


class EventOnlyDashboardJsonView(View):
    @method_decorator(at_least_events_json)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EventOnlyDashboardJsonView, self).dispatch(*args, **kwargs)


class CompetitionOnlyDashboardJsonView(View):
    @method_decorator(at_least_competitions_json)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompetitionOnlyDashboardJsonView, self).dispatch(*args, **kwargs)
