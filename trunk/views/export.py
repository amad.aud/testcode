# Django Imports
from django.db import transaction
from django.db.models import QuerySet
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseForbidden,
)
from django.shortcuts import redirect
from django.urls import reverse

# StartupTree Imports
from branch.models import Label
from StartupTree.loggers import app_logger
from StartupTree.utils import QuerySetType
from trunk.models import (
    AccessLevel,
    Event,
    University,
    UniversityExportHistory,
)
from trunk.perms import (
    is_admin_authorized,
    is_tag_restricted_admin,
    is_view_admin_only_startup_custom_questions,
)
from trunk.tasks import (
    general_export_task,
    user_funding_export_task,
    user_general_export_task,
    judge_export_task,
    mentorship_export_task,
    team_mentorship_export_task,
)
from trunk.views.abstract import UniversityDashboardView

# Library Imports
from abc import ABC, abstractmethod
import datetime as dt
from typing import Optional


class AbstractExportRequest(UniversityDashboardView, ABC):
    """
    Requires:
        Params:
            export_category:
                int choice of UniversityExportHistory.EXPORT_CATEGORY

            task:
                The Roborovski.get_task that should be queued. This must be a task that takes a history.id
                and instantiates an AbstractUniversityExporter, and runs the export.
                eg trunk.tasks.general_export_task

        Methods:
            security_check:
                Implementation of AbstractExportRequest.security_check() that returns whether
                current request is authorized.
                    Note: Because AbstractExportRequest implements UniversityDashboardView, it can be assumed
                    request is originating from admin.


        HttpRequest with the following parameters:
            'start_date': Date String 'm/d/y', or None if not applicable
            'end_date': Date String 'm/d/y', or None if not applicable
            Any other parameters as required by AbstractExportRequest.call_task()
    """
    def __init__(self, export_category: int, task):
        super().__init__()
        self.export_category = export_category
        self.task = task
        self.request: Optional[HttpRequest] = None
        self.university: Optional[University] = None
        self.selected_tags: Optional[QuerySetType[Label]] = None
        self.history: Optional[UniversityExportHistory] = None

    def post(self, request: HttpRequest, university: University) -> HttpResponse:
        if not self.security_check(request, university):
            return HttpResponseForbidden()

        self.request = request
        self.university = university
        self.selected_tags = AbstractExportRequest.__get_tags(request)
        self.history = self.__make_history()

        transaction.on_commit(lambda: self.task.delay(self.history.id))

        return redirect(reverse('uni-export'))

    @staticmethod
    @abstractmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        """ Returns whether current request is authorized. """
        pass

    def __make_history(self) -> UniversityExportHistory:
        """
        Requires the following attributes/properties defined:
            self.university
            self.request
            self.export_category
            self.selected_tags

        returns:
            history: UniversityHistoryObject

        IMPORTANT: See specs for UniversityExportHistory for detailed specs on how front-end data
        is supposed to be translated to backend.
        """
        kwargs = self.__get_additional_info_kwargs()

        history = UniversityExportHistory.objects.create(university=self.university,
                                                         category=self.export_category,
                                                         **kwargs)
        if self.selected_tags:
            self.__set_history_tags(history)

        return history

    def __get_additional_info_kwargs(self):
        export_category = self.export_category
        request = self.request

        kwargs = {}

        # Exports on profiles (with tagging) over a time range
        if export_category in [
            UniversityExportHistory.GENERAL,
            UniversityExportHistory.USER_FUNDING,
            UniversityExportHistory.MENTORSHIP,
            UniversityExportHistory.TEAM_MENTORSHIP,
            UniversityExportHistory.USER_GENERAL
        ]:
            start_date, end_date = AbstractExportRequest.__get_dates(request)

            include_non_tagged = self.selected_tags.count() == 0  # Logic explained in UniversityExportHistory specs

            kwargs.update({
                'start_date': start_date,
                'end_date': end_date,
                'include_non_tagged': include_non_tagged,
            })

        # Exports related to Users
        if export_category in [
            UniversityExportHistory.GENERAL,
            UniversityExportHistory.USER_FUNDING,
            UniversityExportHistory.USER_GENERAL,
        ]:
            kwargs['include_placeholder_members'] \
                = AbstractExportRequest.__get_include_placeholder_members(request)

        # Exports related to Venture/Project
        if export_category in [UniversityExportHistory.GENERAL]:
            kwargs['exclude_admin_only_startup_custom_questions'] \
                = not is_view_admin_only_startup_custom_questions(request, is_project=False)

        # Judge Export
        if export_category in [UniversityExportHistory.JUDGE_RUBRICS]:
            event_id = request.POST.get('competition_id')
            kwargs['competition'] = Event.objects.get(event_id=event_id)

        return kwargs

    def __set_history_tags(self, history: UniversityExportHistory):
        selected_tags = self.selected_tags
        request = self.request

        # Logic, below, explained in UniversityExportHistory specs
        if selected_tags.count() > 0:
            history.tags.set(selected_tags)
        elif is_tag_restricted_admin(request.university, request.is_super, request.is_startuptree_staff):
            history.tags.set(request.current_member.labels.all())
        # in else case, we leave history.tags empty.

    @staticmethod
    def __get_dates(request):
        start_date_str = request.POST.get('start_date')
        end_date_str = request.POST.get('end_date')

        if bool(start_date_str) != bool(end_date_str):
            raise ValueError('Only one date bound was provided.')

        start_date = AbstractExportRequest.__parse_date_str(start_date_str) if start_date_str else None
        end_date = AbstractExportRequest.__parse_date_str(end_date_str) if end_date_str else None

        return start_date, end_date

    @staticmethod
    def __parse_date_str(date_str: str):
        """
        Given a date string of format "m/d/y", return the corresponding date object.
        """
        month, day, year = date_str.split('/')
        return dt.date(year=int(year), month=int(month), day=int(day))

    @staticmethod
    def __get_tags(request) -> QuerySet:
        """ Returns Queryset of labels that the user selected from export modal """
        data = request.POST
        ids = (data[key] for key in data if 'tag-id' in key)
        return Label.objects.filter(id__in=ids)

    @staticmethod
    def __get_include_placeholder_members(request):
        data = request.POST
        return 'include_placeholder_members' in data


class GeneralExportRequest(AbstractExportRequest):
    def __init__(self):
        super().__init__(UniversityExportHistory.GENERAL, general_export_task)

    @staticmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        return is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                   request.is_startuptree_staff, AccessLevel.EXPORT)


class UserFundingExportRequest(AbstractExportRequest):
    def __init__(self):
        super().__init__(UniversityExportHistory.USER_FUNDING, user_funding_export_task)

    @staticmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        return is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                   request.is_startuptree_staff, AccessLevel.EXPORT)


class UserGeneralExportRequest(AbstractExportRequest):
    def __init__(self):
        super().__init__(UniversityExportHistory.USER_GENERAL, user_general_export_task)

    @staticmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        return is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                   request.is_startuptree_staff, AccessLevel.EXPORT)


class MentorshipExportRequest(AbstractExportRequest):
    def __init__(self):
        super().__init__(UniversityExportHistory.MENTORSHIP, mentorship_export_task)

    @staticmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        return is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                   request.is_startuptree_staff, AccessLevel.EXPORT)


class TeamMentorshipExportRequest(AbstractExportRequest):
    def __init__(self):
        super().__init__(UniversityExportHistory.TEAM_MENTORSHIP, team_mentorship_export_task)

    @staticmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        return is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                   request.is_startuptree_staff, AccessLevel.EXPORT)


class JudgeExportRequest(AbstractExportRequest):
    def __init__(self):
        super().__init__(UniversityExportHistory.JUDGE_RUBRICS, judge_export_task)

    @staticmethod
    def security_check(request: HttpRequest, university: University) -> bool:
        return is_admin_authorized(request.is_university, request.is_super, request.access_levels,
                                   request.is_startuptree_staff, AccessLevel.EXPORT)
