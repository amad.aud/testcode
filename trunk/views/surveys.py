import datetime
from io import BytesIO

import pytz
import xlsxwriter
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import PermissionDenied
from django.http.response import (
    JsonResponse, HttpResponse, HttpResponseRedirect, Http404, HttpResponseBadRequest
)
from django.shortcuts import redirect
from django.template.loader import get_template
from django.template.response import TemplateResponse
from django.urls import reverse
from xhtml2pdf import pisa

from StartupTree import settings
from StartupTree.loggers import app_logger
from StartupTree.utils import filtered_filename
from branch.modelforms import TextEditorForm2, EmailFormset
from branch.modelforms import TinyMCEWidget
from branch.models import (
    StartupMember,
    Startup
)
from branch.serializer.member import sm_to_dict
from branch.tasks import send_survey_email_noti
from branch.templatetags import static, media
from branch.utils.tags import get_manageable_tags_titles
from branch.views.competitions import admin_reorder_questions
from emailuser.models import UserEmail
from forms.models import (
    Form,
    MemberApplicant,
    AnonymousApplicant,
    Answer,
    Question,
    Choice
)
from forms.serializer.forms import custom_member_dict
from forms.views import manage_comp_forms
from trunk.models import (
    Event,
    University,
    AccessLevel
)
from trunk.views.abstract import UniversityDashboardView, View
from trunk.views.competitions import link_callback
from trunk.views.events import CreateEvent, EditEvent, DeleteEvent, \
    sorted_events_admin_panel, get_all_rsvps
from trunk.views.utils import prepare_event


class ManageSurveys(UniversityDashboardView):
    def get(self, request, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        rsvps = None
        event_table = None
        events = None
        sort_by = request.GET.get('sort', 'start-desc')
        tag_filter = request.GET.get('filter', 'all')
        archive_dropdown_filter = None
        archive_dropdown_filter2 = None
        archive_dropdown_filter3 = request.GET.get('sort3', '')

        uni_labels = get_manageable_tags_titles(request)

        try:
            events = sorted_events_admin_panel(request, sort_by, university, archive_dropdown_filter,
                                               archive_dropdown_filter2, archive_dropdown_filter3,
                                               False, False, True)

            if tag_filter != 'all':
                events = events.filter(tags__title=tag_filter)

            event_table = list(map(
                lambda x: (
                    x, x.get_url(), x.not_started(), x.not_ended(), x.get_formatted_start_end_date_for_display()),
                events))
            rsvps = get_all_rsvps(events)
            if len(events) == 0:
                events = None
                event_table = None
                rsvps = None
        except Event.DoesNotExist:
            pass
        return TemplateResponse(
            request,
            'trunk/manage/surveys/list.html',
            {
                'events': events,
                'rsvps': rsvps,
                'sort_by': sort_by,
                'tag_filter': tag_filter,
                'tags': uni_labels,
                'event_table': event_table,
                'university': university
            }
        )


class SurveyDashboard(UniversityDashboardView):
    def post(self, request, university, event_id):
        event = Event.objects.get(event_id=event_id)
        if not event.is_survey:
            raise Http404

        default_email = "Your program admins have posted a \
                    new survey: {0}. We hope to see you \
                    participate!<br><br>\
                    Survey opens: {1} ({3})<br>\
                    Survey closes: {2} ({3})".format(
            event.name,
            event.get_formatted_start_datetime(),
            event.get_formatted_end_datetime(),
            event.timezone)

        survey_email = TextEditorForm2(request.POST)
        if survey_email.is_valid():
            survey_email_body = survey_email.cleaned_data['text']
            if survey_email_body == '<br>':
                survey_email_body = default_email
        else:
            survey_email_body = default_email
        event.reminder_email = survey_email_body
        event.save()
        if event.is_published and not event.is_private:
            send_survey_email_noti.delay(request.current_site.id, event_id)

        return self.get(request, university, event_id)

    def get(self, request, university, event_id=None):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        page_name = 'manage-surveys-view'
        event = None
        is_edit_event = False
        can_edit_questions = False
        if event_id:
            event = Event.objects.get(event_id=event_id)
            if not event.is_survey:
                raise Http404

            # Ensure that datetimes are AWARE as opposed to NAIVE for proper localtime
            timeuntil = event.calc_timeuntil()
            open = True
            if timeuntil < 0:
                timeuntil = '0'
                open = False
            else:
                timeuntil = str(timeuntil)
            can_edit_questions = event.get_can_edit_questions()
            if can_edit_questions is None:
                can_edit_questions = False
            else:
                can_edit_questions = can_edit_questions

            default_email = "Your program admins have posted a \
                                new survey: {0}. We hope to see you \
                                participate!<br><br>\
                                Survey opens: {1} ({3})<br>\
                                Survey closes: {2} ({3})".format(
                event.name,
                event.get_formatted_start_datetime(),
                event.get_formatted_end_datetime(),
                event.timezone)

            if event.reminder_email is None or event.reminder_email == '':
                survey_email_body = TextEditorForm2({'text': default_email})
            else:
                survey_email_body = TextEditorForm2({'text': event.reminder_email})

            majordict = {}
            roledict = {}
            graddict = {}
            skilldict = {}

            user_count = event.get_applicant_count()
            if user_count > 0:
                for member_applicant in event.get_member_applicants().iterator():
                    startupmember = member_applicant.member
                    data = sm_to_dict(startupmember,
                                      university.id,
                                      detailed_degree=True)
                    skills = data['skills']
                    roles = data['roles']
                    degrees = data['degrees']
                    for skill in skills:
                        if skill in skilldict:
                            skilldict[skill] += 1
                        else:
                            skilldict[skill] = 1
                    for role in roles:
                        if role in roledict:
                            roledict[role] += 1
                        else:
                            roledict[role] = 1
                    gradyr = None
                    major = None
                    for degree in degrees:
                        try:
                            if 'graduated_on' in degree and degree['graduated_on'] is not None:
                                year = degree['graduated_on']
                            if 'major' in degree and degree['major'] != '':
                                majr = degree['major']
                            elif degree['major'] == '':
                                majr = 'Undeclared'
                            elif degree['major'] is None:
                                pass
                            else:
                                pass
                            if gradyr is None:
                                if year != '':
                                    gradyr = year
                                    major = majr
                                else:
                                    major = majr
                            elif year == '':
                                major = majr
                            elif int(year) > gradyr:
                                gradyr = year
                                major = majr
                            else:
                                pass
                        except:
                            app_logger.exception("TypeError in one of [gradyr, major]")
                    if gradyr in graddict:
                        graddict[gradyr] += 1
                    else:
                        graddict[gradyr] = 1
                    if major in majordict:
                        majordict[major] += 1
                    else:
                        majordict[major] = 1

                # sorting majors
                majorkeys = list(majordict.keys())
                majorvals = list(majordict.values())
                mindex = list(range(len(majorvals)))
                mindex.sort(key=majorvals.__getitem__, reverse=True)
                majorlabels = list(map(majorkeys.__getitem__, mindex))
                majordata = list(map(majorvals.__getitem__, mindex))
                if len(majorlabels) > 10:
                    otherd = sum(majordata[9:])
                    majorlabels = majorlabels[:9]
                    majordata = majordata[:9]
                    majorlabels.append('Other')
                    majordata.append(otherd)
                elif len(majorlabels) == 1 and None in majorlabels:
                    majorlabels = []
                    majordata = []
                else:
                    pass
                # sorting roles
                rolekeys = list(roledict.keys())
                rolevals = list(roledict.values())
                rindex = list(range(len(rolevals)))
                rindex.sort(key=rolevals.__getitem__, reverse=True)
                rolelabels = list(map(rolekeys.__getitem__, rindex))
                roledata = list(map(rolevals.__getitem__, rindex))
                if len(rolelabels) > 10:
                    otherd = sum(roledata[9:])
                    rolelabels = rolelabels[:9]
                    roledata = roledata[:9]
                    rolelabels.append('Other')
                    roledata.append(otherd)
                else:
                    pass
                # sorting grad years
                gradkeys = list(graddict.keys())
                gradvals = list(graddict.values())
                gindex = list(range(len(gradvals)))
                gindex.sort(key=gradvals.__getitem__, reverse=True)
                gradlabels = list(map(gradkeys.__getitem__, gindex))
                graddata = list(map(gradvals.__getitem__, gindex))
                if len(gradlabels) > 10:
                    otherd = sum(graddata[9:])
                    gradlabels = gradlabels[:9]
                    graddata = graddata[:9]
                    gradlabels.append('Other')
                    graddata.append(otherd)
                elif len(gradlabels) == 1 and None in gradlabels:
                    graddata = []
                    gradlabels = []
                else:
                    pass
                # sorting skills
                skillkeys = list(skilldict.keys())
                skillvals = list(skilldict.values())
                sindex = list(range(len(skillvals)))
                sindex.sort(key=skillvals.__getitem__, reverse=True)
                skilllabels = list(map(skillkeys.__getitem__, sindex))
                skilldata = list(map(skillvals.__getitem__, sindex))
                if len(skilllabels) > 10:
                    skilllabels = skilllabels[:10]
                    skilldata = skilldata[:10]
                else:
                    pass
                return TemplateResponse(
                    request,
                    'trunk/manage/surveys/dashboard.html',
                    {
                        'title': 'Survey Dashboard',
                        'page_name': page_name,
                        'event': event,
                        'has_data': True,
                        'gradlabels': gradlabels,
                        'graddata': graddata,
                        'majorlabels': majorlabels,
                        'majordata': majordata,
                        'rolelabels': rolelabels,
                        'roledata': roledata,
                        'skilllabels': skilllabels,
                        'skilldata': skilldata,
                        'timeuntil': timeuntil,
                        'usercount': user_count,
                        'eventopen': open,
                        'survey_email_body': survey_email_body,
                        'university': university,
                        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY
                    }
                )
            else:
                return TemplateResponse(
                    request,
                    'trunk/manage/surveys/dashboard.html',
                    {
                        'title': 'Event Description',
                        'page_name': page_name,
                        'event': event,
                        'has_data': False,
                        'timeuntil': timeuntil,
                        'usercount': user_count,
                        'eventopen': open,
                        'survey_email_body': survey_email_body,
                        'university': university,
                        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY
                    })
        else:
            messages.add_message(request, messages.ERROR, 'Survey does not exist')
            return HttpResponseRedirect(reverse('uni-manage-surveys'))


class CreateSurvey(CreateEvent):
    def get(self, request, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        is_edit = False
        is_create_template = request.GET.get('as_template', None)
        is_create_template = True if is_create_template == "true" else False
        event_description = TinyMCEWidget()
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None
        return TemplateResponse(
            request,
            'trunk/manage/surveys/create_edit.html',
            {
                'event_description': event_description,
                'university': university,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                'uni_labels': uni_labels,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform,
                'is_edit': is_edit,
                'is_create_template': is_create_template
            }
        )

    def post(self, request, university, is_competition=False, is_survey=True):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        return self.create(request, university, is_competition, is_survey)


class EditSurvey(EditEvent):
    def get(self, request, university, event_id):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        is_edit = True
        from_template = request.GET.get("from_template", None)
        try:
            event, event_description = prepare_event(event_id)
        except Event.DoesNotExist:
            raise Http404('Event does not exist')
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None
        return TemplateResponse(
            request,
            'trunk/manage/surveys/create_edit.html',
            {
                'event_description': event_description,
                'university': university,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                'event': event,
                'is_edit': is_edit,
                'from_template': from_template,
                'uni_labels': uni_labels,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform
            }
        )


class ViewResps(UniversityDashboardView):
    def post(self, request, event_id, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        survey = Event.objects.get(event_id=event_id)
        try:
            action = int(request.POST.get('action', 0))
            if action == 1:
                # Archive user
                url_name = request.POST.get('uid', None)
                if url_name:
                    try:
                        member = StartupMember.objects.get(url_name=url_name)
                        uid = member.user_id
                        if (survey.invited_users.filter(id=uid).exists() or
                                survey.attending_users.filter(id=uid).exists()):
                            survey.archived_users.add(uid)
                            return JsonResponse({}, status=200)
                    except StartupMember.DoesNotExist:
                        try:
                            anonymous = AnonymousApplicant.objects.get(applicant_id=url_name, event=survey)
                            if not anonymous.is_archived:
                                anonymous.is_archived = True
                                anonymous.save()
                                return JsonResponse({}, status=200)
                        except AnonymousApplicant.DoesNotExist:
                            app_logger.warning("Member does not exist")
                        pass
            elif action == 2:
                # Unarchive user
                url_name = request.POST.get('uid', None)
                if url_name:
                    try:
                        member = StartupMember.objects.get(url_name=url_name)
                        uid = member.user_id
                        if survey.archived_users.filter(id=uid).exists():
                            survey.archived_users.remove(uid)
                            return JsonResponse({}, status=200)
                    except StartupMember.DoesNotExist:
                        try:
                            anonymous = AnonymousApplicant.objects.get(applicant_id=url_name, event=survey)
                            if anonymous.is_archived:
                                anonymous.is_archived = False
                                anonymous.save()
                                return JsonResponse({}, status=200)
                        except AnonymousApplicant.DoesNotExist:
                            app_logger.warning("Member does not exist")
                        pass
        except TypeError:
            return JsonResponse({}, status=500)
        return JsonResponse({}, status=404)

    def get(self, request, event_id, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        return view_respondents(request, event_id, university)


class ViewSurvey(UniversityDashboardView):
    def get(self, request, event_id, university, app_url_name):
        return get_survey(request, event_id, app_url_name)


class AdminSubmitRespondent(UniversityDashboardView):
    def get(self, request, event_id, app_url_name, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        survey = Event.objects.get(event_id=event_id, university=university)
        try:
            sm = StartupMember.objects.get(url_name=app_url_name)
            is_created = False
            if not MemberApplicant.objects.filter(member=sm, event=survey).exists():
                is_created = True
                respondent = MemberApplicant.objects.create(member=sm, event=survey)
            else:
                respondent = MemberApplicant.objects.get(member=sm, event=survey)
            if not is_created:  # default when is_applied wasn't given is True
                respondent.is_applied = True
                respondent.save()
            survey.attending_users.add(sm.user)
        except StartupMember.DoesNotExist:
            try:
                anonymous = AnonymousApplicant.objects.get(applicant_id=app_url_name, event=survey)
                if not anonymous.is_applied:
                    anonymous.is_applied = True
                    anonymous.save()
            except AnonymousApplicant.DoesNotExist:
                pass
        return HttpResponseRedirect(reverse('uni-manage-respondents', kwargs={'event_id': event_id}))


class ManageQuestions(UniversityDashboardView):
    def get(self, request, event_id, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        try:
            event = Event.objects.get(event_id=event_id)
        except:
            raise Http404
        if event.get_can_edit_questions() or request.is_startuptree_staff:
            return manage_comp_forms(request, university, event_id, False, True)
        else:
            return redirect(reverse('uni-manage-surveys-view', kwargs={'event_id': event.event_id}))

    def post(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id)
        except:
            raise Http404
        if event.get_can_edit_questions() or request.is_startuptree_staff:
            return manage_comp_forms(request, university, event_id, False, True)
        else:
            raise PermissionDenied


class ExportSurvey(UniversityDashboardView):
    def respondents_data(self, survey, survey_id, uni_id):
        respondents_ue = survey.get_member_applicants()
        respondents_list = []
        for member_respondent in respondents_ue:
            respondent_member = member_respondent.member
            respondent_timestamp = member_respondent.date_applied
            respondent_name = respondent_member.first_name + " " + respondent_member.last_name
            respondent_email = UserEmail.objects.get(id=respondent_member.user_id)
            respondent_email = "n/a" if respondent_email is None else respondent_email
            if member_respondent and member_respondent.venture:
                respondent_venture = member_respondent.venture
            else:
                respondent_venture = None

            custom = custom_member_dict(respondent_member, uni_id, Form.COMPETITION, survey_id)
            respondents_list.append({'name': respondent_name,
                                     'email': respondent_email,
                                     'timestamp': respondent_timestamp,
                                     'venture': respondent_venture,
                                     'custom': custom})

        for anonymous_respondent in AnonymousApplicant.objects.filter(event_id=survey.id, is_applied=True, is_archived=False):
            if anonymous_respondent.venture:
                anonymous_respondent_venture = anonymous_respondent.venture
            else:
                anonymous_respondent_venture = None
            temp = {
                'name': anonymous_respondent.name,
                'email': anonymous_respondent.email,
                'timestamp': anonymous_respondent.date_created,
                'venture': anonymous_respondent_venture
            }
            answers = custom_member_dict(None, uni_id, Form.COMPETITION, survey_id,
                                         anonymous_applicant_id=anonymous_respondent.id)
            temp['custom'] = answers
            respondents_list.append(temp)
        return respondents_list

    def get(self, request, university, event_id=None):
        if university is None:
            app_logger.warning("Security Alert! - export_competition:\
                {0} isn't a university user!".format(request.user))
            return HttpResponseRedirect('/')

        try:
            survey = Event.objects.get(event_id=event_id)
        except Event.DoesNotExist:
            raise Exception("Survey does not exist.")

        rsp_lst = self.respondents_data(survey, event_id, university)
        venture_word = university.style.venture_word.capitalize()

        if len(rsp_lst) == 0:
            messages.add_message(request, messages.ERROR, 'You cannot export data for a survey with no respondents')
            return HttpResponseRedirect(reverse('uni-manage-surveys'))
        else:
            filtered_name = filtered_filename(survey.name)
            filename = "{0}_export_{1}.xlsx".format(filtered_name, datetime.date.today().strftime("%y%m%d"))
            attachment = BytesIO()
            workbook = xlsxwriter.Workbook(attachment)
            survey_sheet = workbook.add_worksheet("Exported")
            wrap_format = workbook.add_format()
            wrap_format.set_text_wrap()

            # Get column heads
            if survey.form and survey.form.requires_venture:
                col_head = ["Name", "Email", "Date Survey Taken", "{0}".format(venture_word)]
            else:
                col_head = ["Name", "Email", "Date Survey Taken"]
            start = rsp_lst[0]
            for q in start['custom']:
                col_head.append("{0}".format(q['q_text']))
            for offset, entry in enumerate(col_head):
                survey_sheet.write(0, offset, entry)

            # Now fill spreadsheet
            row_cnt = 1
            for rsp in rsp_lst:
                if survey.form and survey.form.requires_venture:
                    data = ["{0}".format(rsp['name']), "{0}".format(rsp['email']),
                            "{0}".format(rsp['timestamp']), "{0}".format(rsp['venture'])]
                else:
                    data = ["{0}".format(rsp['name']), "{0}".format(rsp['email']),
                            "{0}".format(rsp['timestamp'])]
                for q in rsp['custom']:
                    if not q['a_id']:
                        data.append("None")
                    elif q['hidden_until_choice']:
                        data.append("N/A")
                    elif q['q_type'] == 'Paragraph':
                        data.append("{0}".format(q['a_text']))
                    elif q['q_type'] == 'Dropdown' or q['q_type'] == 'Multiple Choice':
                        no_response = True
                        selected_choices = ""
                        for choice in q['q_choices']:
                            if choice['chosen']:
                                no_response = False
                                if len(selected_choices) != 0:
                                    selected_choices += "\n"
                                selected_choices += choice['text']
                        if no_response:
                            selected_choices = "None"
                        data.append(selected_choices)
                    elif q['q_type'] == 'Date Time':
                        data.append("{0}".format(q['a_date']))
                    elif q['q_type'] == 'File Upload':
                        if q['a_url'] is None:
                            data.append("No")
                        else:
                            data.append("Yes")
                for offset, entry in enumerate(data):
                    survey_sheet.write(row_cnt, offset, entry, wrap_format)
                row_cnt += 1
            workbook.close()
            attachment.seek(0)
            response = HttpResponse(
                attachment.read(),
                content_type="application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
            attachment.close()
            return response


class ExportSurveyAttachments(View):
    def get(self, request, event_id):
        university = University.objects.get(site_id=request.current_site.id)
        try:
            survey = Event.objects.get(event_id=event_id, university_id=university.id)
        except Event.DoesNotExist:
            return HttpResponseBadRequest()

        all_files = Answer.objects.filter(q__q_type=5, q__form=survey.form,
                                          uploaded_file__isnull=False) \
                                  .order_by('q__number')
        respondent_list = []
        respondents_ue = survey.get_member_applicants()
        anonymous_respondents = AnonymousApplicant.objects.filter(event_id=survey.id, is_applied=True)
        for respondent in respondents_ue:
            respondent_member = respondent.member
            respondent_list.append(respondent_member.get_full_name())
        for anonymous_respondent in anonymous_respondents:
            respondent_list.append(str(anonymous_respondent))

        context = {
            'survey': survey,
            'all_files': all_files,
            'respondents': respondent_list,
            'full_domain': "{0}".format(request.university.get_url()),
            "ST_GULP_VERSION": settings.ST_GULP_VERSION
        }
        template = get_template('trunk/manage/surveys/exported_attachments.html')

        context.update(static(request))
        context.update(media(request))
        content = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        pisa_status = pisa.CreatePDF(
            content, dest=response, link_callback=link_callback)
        # if error then show some funy view
        if pisa_status.err:
            app_logger.error("Survey export error: {0}".format(pisa_status.err))
            return HttpResponse('We had some errors generating the file export. Please contact our staff.')
        return response


class ExportSurveyResponses(View):
    def get(self, request, event_id, app_url_name=None):
        university = University.objects.get(site_id=request.current_site.id)
        try:
            survey = Event.objects.get(event_id=event_id, university_id=university.id)
        except Event.DoesNotExist:
            return HttpResponseBadRequest()
        # export for all respondents of the survey
        if not app_url_name:
            respondent_list = []
            answer_list = []
            respondents_ue = survey.get_member_applicants()
            anonymous_respondents = AnonymousApplicant.objects.filter(event_id=survey.id, is_applied=True, is_archived=False)
            for respondent in respondents_ue:
                respondent_member = respondent.member
                respondent_list.append(respondent_member.get_full_name())
                answers = custom_member_dict(respondent_member, university.id, Form.COMPETITION, survey.event_id)
                answer_list.append(answers)
            for anonymous_respondent in anonymous_respondents:
                answers = custom_member_dict(None, university.id, Form.COMPETITION, survey.event_id,
                                             anonymous_applicant_id=anonymous_respondent.id)
                respondent_list.append(str(anonymous_respondent))
                answer_list.append(answers)
        else:
            try:
                respondent = StartupMember.objects.get(url_name=app_url_name)
                answers = custom_member_dict(respondent, university.id, Form.COMPETITION, survey.event_id)
            except StartupMember.DoesNotExist:
                try:
                    respondent = AnonymousApplicant.objects.get(applicant_id=app_url_name, is_applied=True, event=survey)
                    answers = custom_member_dict(None, university.id, Form.COMPETITION, survey.event_id,
                                                 anonymous_applicant_id=respondent.id)
                except AnonymousApplicant.DoesNotExist:
                    return HttpResponseBadRequest()
            answer_list = [answers]
            respondent_list = [str(respondent)]
        context = {
            'survey': survey,
            'custom_questions_list': answer_list,
            'resps': respondent_list,
            'full_domain': "{0}".format(request.university.get_url()),
            "ST_GULP_VERSION": settings.ST_GULP_VERSION
        }
        template = get_template('trunk/manage/surveys/exported_survey.html')

        context.update(static(request))
        context.update(media(request))
        content = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        pisa_status = pisa.CreatePDF(
            content, dest=response, link_callback=link_callback)
        # if error then show some funy view
        if pisa_status.err:
            app_logger.error("Survey export error: {0}".format(pisa_status.err))
            return HttpResponse('We had some errors generating export for the survey. Please contact our staff.')
        return response


class GenerateSurveyFromTemplate(UniversityDashboardView):
    def get(self, request, event_id, university):
        try:
            survey = Event.objects.select_related('form').get(
                event_id=event_id,
                university=university
            )
        except:
            try:
                survey = Event.objects.select_related('form').get(
                    event_id=event_id,
                    university__short_name='readymadetemplates'
                )
            except:
                raise Http404('No such survey exists.')

        display_activity_feed = survey.display_activity_feed
        is_public_survey = survey.is_public_competition
        display_view_count = survey.display_view_count
        display_respondents = survey.display_attendees
        is_private = survey.is_private
        start = datetime.datetime.now() + datetime.timedelta(days=7)
        end = start + datetime.timedelta(days=7)
        new_survey = Event.objects.create_event(
            name=survey.name,
            description=survey.description,
            start=start,
            end=end,
            timezone=survey.timezone,
            university=survey.university,
            display_activity_feed=display_activity_feed,
            pin_to_feed=survey.pin_to_feed,
            is_public_competition=is_public_survey,
            display_view_count=display_view_count,
            display_attendees=display_respondents,
            is_private=is_private,
            is_survey=True,
            is_published=survey.is_published,
            event_type=7)
        form = Form.objects.create(
            form=3,
            university=university
        )
        new_survey.form = form
        new_survey.save()
        if survey.image:
            new_survey.image = survey.image
            new_survey.save()
        if survey.form and survey.form.questions:
            questions = []
            choices = []
            for q in survey.form.questions.all().order_by('number', 'id'):
                options = q.choices.all()
                old_q_id = q.id
                q.pk = None
                q.id = None
                q.save()
                new_q_id = q.id
                questions.append({
                    "old_q_id": old_q_id,
                    "new_q_id": new_q_id
                })
                if q.q_type == 1 or q.q_type == 2:
                    if q.choices:
                        for c in options:
                            if c.goto_question:
                                goto_id = c.goto_question.id
                            else:
                                goto_id = None
                            old_c_id = c.id
                            q.choices.remove(c)
                            c.pk = None
                            c.id = None
                            c.save()
                            new_c_id = c.id
                            if goto_id is not None:
                                choices.append({
                                    "old_q_id": goto_id,
                                    "new_c_id": new_c_id
                                })
                            q.add_choice(c)
                q.save()
                form.questions.add(q)
                form.save()
            for choice in choices:
                new_choice = Choice.objects.get(id=choice["new_c_id"])
                for question in questions:
                    if question["old_q_id"] == choice["old_q_id"]:
                        new_question = Question.objects.get(id=question["new_q_id"])
                        new_choice.goto_question = new_question
                        new_choice.save()
        new_survey.form.save()
        new_survey.save()

        return redirect(reverse('uni-edit-survey',
            kwargs={"event_id": new_survey.event_id}) + "?from_template=" + event_id)


class SurveyQuestionReorder(UniversityDashboardView):
    def get(self, request, event_id, university):
        try:
            survey = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        return admin_reorder_questions(request, event_id)

    def post(self, request, event_id, university):
        try:
            survey = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        return admin_reorder_questions(request, event_id)


class DeleteSurvey(DeleteEvent):
    def post(self, request, university):
        if AccessLevel.SURVEYS not in request.access_levels and not request.is_super:
            raise Http404()
        self.delete(request)
        return HttpResponseRedirect(reverse('uni-manage-surveys'))


@login_required
def view_respondents(request, survey_id, university, errors=None, page=1):
    respondents = []
    invited_users = []
    try:
        survey = Event.objects.select_related('form').get(event_id=survey_id)
    except:
        raise Http404
    if not survey.is_survey:
        raise Http404

    if survey.form is None:
        survey.form = Form.objects.create(
            form=Form.COMPETITION, university=university)
        survey.save()
    requires_venture = survey.form.requires_venture

    # get view filter
    ARCHIVED = 'archived'
    SUBMITTED = 'submitted'
    PROGRESS = 'progress'
    view_option = request.GET.get('view', SUBMITTED)

    archived_users = survey.archived_users.all().values_list('id', flat=True)

    loggedin_users = StartupMember.objects.none()
    if view_option == PROGRESS:
        loggedin_users = MemberApplicant.objects.select_related('member', 'member__user')\
            .filter(event_id=survey.id, is_applied=False)\
            .exclude(member__user_id__in=archived_users).exclude(member__user=None)\
            .distinct('member__user')
    elif view_option == ARCHIVED:
        loggedin_users = MemberApplicant.objects.select_related('member', 'member__user')\
            .filter(event_id=survey.id, member__user_id__in=archived_users)\
            .exclude(member__user=None).distinct('member__user')
    else:
        loggedin_users = MemberApplicant.objects.select_related('member', 'member__user')\
            .filter(event_id=survey.id, is_applied=True)\
            .exclude(member__user_id__in=archived_users).exclude(member__user=None)\
            .distinct('member__user')

    loggedin_users_list = loggedin_users.values_list('member__user__id', flat=True)

    anonymous_users = AnonymousApplicant.objects.none()
    if view_option == PROGRESS:
        anonymous_users = AnonymousApplicant.objects.filter(
            event_id=survey.id, is_applied=False, is_archived=False)\
            .exclude(email=None).distinct('email')
    elif view_option == ARCHIVED:
        anonymous_users = AnonymousApplicant.objects.filter(
            event_id=survey.id, is_archived=True).exclude(email=None)\
            .distinct('email')
    else:
        anonymous_users = AnonymousApplicant.objects.filter(
            event_id=survey.id, is_applied=True, is_archived=False)\
            .exclude(email=None).distinct('email')

    if survey.invited_users:
        invited_users = survey.invited_users.exclude(
            id__in=archived_users).exclude(id__in=loggedin_users_list)\
            .exclude(email=None).distinct('email')\
            .values_list('id', flat=True)

    # TODO: paginate users

    for respondent in loggedin_users:
        temp = {}
        sm = respondent.member
        temp['respondent'] = sm
        temp['respondent_id'] = sm.id
        temp['url_name'] = sm.url_name
        if Startup.objects.filter(name=respondent.venture, university=university).exists():
            temp['venture'] = Startup.objects.filter(
                name=respondent.venture, university=university)[0]
            temp['venture_url'] = temp['venture'].get_url()
        else:
            temp['venture'] = respondent.venture
            temp['venture_url'] = None
        temp['timestamp'] = respondent.date_applied
        temp['answered'] = respondent.is_applied
        respondents.append(temp)

    for anonymous_respondent in anonymous_users:
        temp = {}
        temp['respondent'] = anonymous_respondent.name
        temp['respondent_id'] = anonymous_respondent.applicant_id
        temp['url_name'] = str(anonymous_respondent.applicant_id)
        if Startup.objects.filter(name=anonymous_respondent.venture, university=university).exists():
            temp['venture'] = Startup.objects.filter(
                name=anonymous_respondent.venture, university=university)[0]
            temp['venture_url'] = temp['venture'].get_url()
        else:
            temp['venture'] = anonymous_respondent.venture
            temp['venture_url'] = None
        temp['timestamp'] = anonymous_respondent.date_created
        if view_option == SUBMITTED:
            temp['answered'] = True
        else:
            temp['answered'] = False
        temp['is_anonymous'] = True
        respondents.append(temp)

    # Some weird logic regarding Invited (Comented out by SW-4193)
    # Original Commit: b843f5d06ec8bdb1de074cb1cd8252b69617b05b
    if view_option == PROGRESS:
        for user in UserEmail.objects.filter(id__in=invited_users):
            temp = {}
            sm = StartupMember.objects.get_user(user=user)
            temp['respondent'] = sm
            temp['respondent_id'] = sm.id
            temp['url_name'] = sm.url_name
            temp['venture'] = None
            temp['venture_url'] = None
            temp['timestamp'] = 'Not Yet Answered'
            respondents.append(temp)

    # # sort table
    if view_option == PROGRESS and len(invited_users) > 0:
        # list timestamped respondents first, sorted chronologically, then list invited users
        invited_respondents = []
        for respondent in respondents[:]:
            if respondent['timestamp'] == 'Not Yet Answered':
                respondents.remove(respondent)
                invited_respondents.append(respondent)
        respondents = sorted(
            respondents, key=lambda k: k['timestamp'], reverse=True)
        respondents.extend(invited_respondents)
    else:
        # all respondents should have a timestamp, so can sort chronologically
        respondents = sorted(respondents, key=lambda k: k['timestamp'], reverse=True)

    invite_set = EmailFormset()

    try:
        # If email invitation had just been sent, then show a modal with invitation info.
        emails_st_invited = request.emails_st_invited
        emails_st_already_invited = request.emails_st_already_invited
        emails_not_st_invited = request.emails_not_st_invited
        invitations_sent = "true"
    except AttributeError:
        emails_st_invited = None
        emails_st_already_invited = None
        emails_not_st_invited = None
        invitations_sent = "false"

    return TemplateResponse(request, 'trunk/manage/surveys/manage_respondents.html', {
        'university': university,
        'respondents': respondents,
        'survey_name': survey.name,
        'survey_id': str(survey.event_id),
        'invite_set': invite_set,
        'errors': errors,
        'requires_venture': requires_venture,
        'view_option': view_option,
        'event_id' : str(survey.event_id),
        'emails_st_invited' : emails_st_invited,
        'emails_st_already_invited' : emails_st_already_invited,
        'emails_not_st_invited' : emails_not_st_invited,
        'invitations_sent' : invitations_sent,
        'event_type' : "survey"
    })


@login_required
def get_survey(request, survey_id, resp_url_name):
    try:
        uni = University.objects.get(
            site=get_current_site(request)
        )
        survey = Event.objects.get(
            event_id=survey_id,
            university=uni
        )
        archived_users = survey.archived_users.all().values_list('id', flat=True)
        try:
            resp = StartupMember.objects.get(
                url_name=resp_url_name
            )
            custom_questions = custom_member_dict(
                resp, uni.id, Form.COMPETITION, survey.event_id)
            is_anonymous = False
            is_submitted = MemberApplicant.objects.select_related('member', 'member__user')\
                .filter(event_id=survey.id, member=resp, is_applied=True)\
                .exclude(member__user_id__in=archived_users).exists()
        except StartupMember.DoesNotExist:
            try:
                resp = AnonymousApplicant.objects.get(
                    applicant_id=resp_url_name, event_id=survey.id)
                custom_questions = custom_member_dict(None, uni.id, Form.COMPETITION, survey.event_id,
                                                      anonymous_applicant_id=resp.id)
                is_anonymous = True
                is_submitted = True
            except AnonymousApplicant.DoesNotExist:
                is_anonymous = None
                is_submitted = None
    except Exception as e:
        raise Http404(e)
    return TemplateResponse(request, 'trunk/manage/surveys/review_respondent.html',
                            {
                                'university': uni,
                                'custom_questions': custom_questions,
                                'resp': resp,
                                'survey': survey,
                                'is_anonymous': is_anonymous,
                                'is_submitted': is_submitted
                            })
