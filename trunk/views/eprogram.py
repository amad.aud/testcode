# Django imports
from django.template.response import TemplateResponse, get_template
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpRequest
from django.http.response import (
    JsonResponse, HttpResponseRedirect
)
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import redirect
from django.views.generic import TemplateView
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.db import transaction
from django.db.models import Q, F, Value, Count
from django.db.models.aggregates import Sum, Max
from django.db.models.functions import Coalesce
from django.urls import reverse
# app
from app.customize import get_industries
# trunk imports
from trunk.models import (
    University,
    UniversityStaff,
    UniversityStyle,
    AccessLevel,
    PointOfContact,
    Event,
    EventRSVP,
    DataReport, UniversityMailChimpAPI, MailChimpAudience
)
from trunk.modelforms import ImportForm
from trunk.decorators import only_university
# from trunk.utils.mailchimp import create_list, add_members
from trunk.utils.mailchimp import add_members, create_list
from trunk.views.abstract import UniversityDashboardView
# From inbox
from inbox.models import (
    InboxConversationRequest,
    RequestMessage,
    Inbox,
    InboxConversation,
    InboxConversationGroup
)
# emailuser
from emailuser.conf import REGULAR, STAFF
# excel reading
from trunk.io import xlrd
from inbox.views import handle_message
# StartupTree
from StartupTree.settings import DEBUG
from StartupTree.loggers import app_logger
from StartupTree.caches import (
    # get_total_funding,
    # get_total_exit,
    set_new_startups,
    # get_user_stat,
)
# branch models
from branch.models import (
    Startup,
    StartupMember,
    UserEmail,
    Label,
    Organization,
    Tool,
    ToolCategory,
    ToolCategoryBlock
)
from branch.modelforms import TextEditorForm2, SignupAdditionalForm, ToolFileForm, TinyMCEWidget
from branch.serializer.deal import DealSerializer, ToolCategoryBlockSerializer
from branch.tasks import notify_startup_creator
from branch.aggregate.project import (
    recently_added_projects,
    count_project_by_ages,
    get_added_project_num
)
from branch.aggregate.venture import (
    recently_added_startups,
    get_added_startup_num,
    count_startup_by_ages
)
from branch.templatetags import static
from branch.utils.tags import get_manageable_tags
# hamster tasks for sending emails
from hamster.tasks import send_invitations, schedule_update_company_stat
# activity to keep track of invited member
from activity.clerk import StartupActivityClerk
from activity.models import InvitedMember, Activity
from trunk.tasks import (
    create_user_invite,
    create_user_invite_bulk,
    send_staff_invitation,
    send_staff_notification
)
from StartupTree.utils import diggy_paginator
from functools import reduce
from itertools import chain
# Other imports
import json


class ManageVentures(UniversityDashboardView):
    def post(self, request, university):
        assert isinstance(request, HttpRequest)
        if AccessLevel.VENTURES not in request.access_levels and not request.is_super:
            raise Http404()

        current_site = request.current_site
        bulk = request.POST.get("bulk-options")
        if bulk != "" and bulk is not None:
            bulks = bulk.split(",")
            operation = bulks.pop(0)
            if operation == "delete":
                bulks = map(int, bulks)
                startups = Startup.objects.filter(
                    id__in=bulks, is_project=False)
                delete_list = []
                for startup in startups:
                    unis = \
                        startup.university.all().values_list('id', flat=True)
                    num_uni = len(unis)
                    delete_list.append(startup.id)
                    startup.university.remove(university)
                    startup.site.remove(university.site)
                    if num_uni - 1 == 0:
                        startup.delete()
                Activity.objects.filter(
                    startup_id__in=delete_list, site=current_site).delete()
                schedule_update_company_stat.delay(
                    university.site_id, university.id)
                startups = None
            elif operation == "archive":
                bulks = map(int, bulks)
                startups = Startup.objects.filter(
                    id__in=bulks, is_project=False)
                for startup in startups:
                    if not startup.is_archived:
                        startup.is_archived = True
                        startup.save()
            elif operation == "unarchive":
                bulks = map(int, bulks)
                startups = Startup.objects.filter(
                    id__in=bulks, is_project=False)
                for startup in startups:
                    if startup.is_archived:
                        startup.is_archived = False
                        if startup.status == 2:
                            startup.status = 0
                        startup.save()
            elif operation == "add to group":
                l_id = bulks.pop(0)
                label = Label.objects.get(label_id=l_id, university=university)
                projects = Startup.objects.filter(
                    id__in=bulks, is_project=False)
                for project in projects:
                    project.labels.add(label)
        get_param = request.GET.copy()
        return HttpResponseRedirect("{0}?{1}".format(reverse('uni-manage-ventures'), get_param.urlencode()))

    def get(self, request, university):
        assert isinstance(request, HttpRequest)
        if AccessLevel.VENTURES not in request.access_levels and not request.is_super:
            raise Http404()

        current_site = request.current_site
        industry_filter = request.GET.get("filter-options")
        labels = get_manageable_tags(request)

        view = request.GET.get("page-options")
        if view == None or view == "":
            view = 'all'
        if industry_filter != "" and industry_filter is not None:
            industry_filter = industry_filter.lower()
            startups = Startup.objects.select_related('industry') \
                .filter(university=university,
                        industry__name=industry_filter,
                        is_project=False)
            if view == 'archived':
                startups = startups.filter(is_archived=True)
            else:
                startups = startups.filter(is_archived=False)
        else:
            startups = Startup.objects.select_related('industry') \
                .filter(university=university, is_project=False)
            if view == 'archived':
                startups = startups.filter(is_archived=True)
            else:
                startups = startups.filter(is_archived=False)

        if startups is None:
            startups = Startup.objects.select_related('industry') \
                .filter(university=university, is_project=False)
            if view == 'archived':
                startups = startups.filter(is_archived=True)
            else:
                startups = startups.filter(is_archived=False)

        query = request.GET.get('q', '')
        if query:
            startups = startups.filter(name__icontains=query)

        startups = startups.distinct().annotate(
            total_funding=Coalesce(Sum('investment__amount'), Value(0)),
            exit_value=Coalesce(Max('acquiree__amount'), Value(0))) \
            .values('id',
                    'name',
                    'url_name',
                    'industry__name',
                    'pitch',
                    'total_funding',
                    'exit_value',
                    'is_archived',
                    'status')

        recent_startups, startup_count = recently_added_startups(university)
        pending_startups = Startup.objects.select_related('industry') \
            .filter(university=university,
                    is_project=False,
                    status=Startup.PENDING).distinct() \
            .values('id',
                    'name',
                    'url_name',
                    'industry__name',
                    'created_on') \
            .order_by('-created_on')

        can_view_recent = True
        if university.style.restrict_tagged_admins and not request.is_startuptree_staff:
            admin_user = request.current_member
            can_view_recent = False
            if admin_user.labels.all():
                startups = startups.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=admin_user.labels.all()) | Q(num_tags=0)).distinct()
                pending_startups = pending_startups.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=admin_user.labels.all()) | Q(num_tags=0)).distinct()
            else:
                startups = startups.annotate(num_tags=Count('labels')) \
                    .filter(num_tags=0)
                pending_startups = pending_startups.annotate(num_tags=Count('labels')) \
                    .filter(num_tags=0)

        requires_approval = university.style.require_startup_approval
        allow_admin_jobs = university.style.allow_admin_jobs

        name_order = request.GET.get('name_order', 'ascend')
        industry_order = request.GET.get('industry_order', 'none')
        funding_order = request.GET.get('funding_order', 'none')
        exit_order = request.GET.get('exit_order', 'none')
        order_by = []
        if industry_order != 'none':
            is_reverse = True if industry_order == 'descend' else False
            if is_reverse:
                order_by.append('-industry__name')
            else:
                order_by.append('industry__name')
        if funding_order != 'none':
            is_reverse = True if funding_order == 'descend' else False
            if is_reverse:
                order_by.append('-total_funding')
            else:
                order_by.append('total_funding')
        if exit_order != 'none':
            is_reverse = True if exit_order == 'descend' else False
            if is_reverse:
                order_by.append('-exit_value')
            else:
                order_by.append('exit_value')

        if name_order == 'descend':
            order_by.append('-name')
        else:
            order_by.append('name')
        startups = startups.order_by(*order_by)

        page = request.GET.get('page')
        startup_paginator = Paginator(startups, 15)
        try:
            startup_page = startup_paginator.page(page)
        except PageNotAnInteger:
            startup_page = startup_paginator.page(1)
        except EmptyPage:
            startup_page = startup_paginator.page(startup_paginator.num_pages)
        prev_page, curr_pages, next_page = diggy_paginator(startup_paginator, startup_page.number)
        is_push_state = self.request.META.get('HTTP_X_PJAX', False)
        if is_push_state:
            template = get_template('trunk/profiles/search_venture.html')
            context = {
                'startups': startup_page,
                'searching': query,
                'prev_page': prev_page,
                'next_page': next_page,
                'curr_pages': curr_pages,
                'curr_page': startup_page.number,
                'name_order': name_order,
                'industry_order': industry_order,
                'funding_order': funding_order,
                'exit_order': exit_order,
                'allow_admin_jobs': allow_admin_jobs,
                'requires_approval': requires_approval
            }
            context.update(static(request))
            content = template.render(context)
            data = {'results': content, 'title': 'Ventures | {0}'.format(request.university),
                    'url': "{0}?{1}".format(reverse('uni-manage-ventures'), request.GET.urlencode())}
            return JsonResponse(data)

        industries = get_industries(university.short_name)

        new_startups = get_added_startup_num(university.id)
        if DEBUG:
            new_startups = None
        if new_startups is None:
            new_startups = get_added_startup_num(university.id)
            set_new_startups(current_site.name, new_startups)
        younger_startups = count_startup_by_ages(university, 1)
        mature_startups = count_startup_by_ages(university, 3, False)

        return TemplateResponse(
            request,
            'trunk/profiles/manage_ventures.html',
            {
                'page_name': 'manage-ventures',
                'title': 'Startups',
                'university': university,
                'view': view,
                'startups': startup_page,
                'industries': industries,
                'active_industry': industry_filter,
                'total_startups': startup_count,
                'new_startups': new_startups,
                'recent_startups': recent_startups,
                'pending_startups': pending_startups,
                'requires_approval': requires_approval,
                'allow_admin_jobs': allow_admin_jobs,
                'can_view_recent': can_view_recent,
                'young': younger_startups,
                'mature': mature_startups,
                'curr_pages': curr_pages,
                'prev_page': prev_page,
                'next_page': next_page,
                'curr_page': startup_page.number,
                'name_order': name_order,
                'industry_order': industry_order,
                'funding_order': funding_order,
                'exit_order': exit_order,
                'labels': labels
            }
        )


class ManageProjects(UniversityDashboardView):
    def post(self, request, university):
        assert isinstance(request, HttpRequest)
        if AccessLevel.PROJECTS not in request.access_levels and not request.is_super:
            raise Http404()
        current_site = request.current_site
        bulk = request.POST.get("bulk-options")
        if bulk != "" and bulk is not None:
            bulks = bulk.split(",")
            operation = bulks.pop(0)
            if operation == "delete":
                bulks = map(int, bulks)
                projects = Startup.objects.filter(
                    id__in=bulks, is_project=True)
                delete_list = []
                for project in projects:
                    unis = \
                        project.university.all().values_list('id', flat=True)
                    num_uni = len(unis)
                    delete_list.append(project.id)
                    project.university.remove(university)
                    project.site.remove(university.site)
                    if num_uni - 1 == 0:
                        project.delete()
                Activity.objects.filter(
                    startup_id__in=delete_list, site=current_site).delete()
                schedule_update_company_stat.delay(
                    university.site_id, university.id)
                projects = None
            elif operation == "archive":
                bulks = map(int, bulks)
                projects = Startup.objects.filter(
                    id__in=bulks, is_project=True)
                for project in projects:
                    if not project.is_archived:
                        project.is_archived = True
                        project.save()
            elif operation == "unarchive":
                bulks = map(int, bulks)
                projects = Startup.objects.filter(
                    id__in=bulks, is_project=True)
                for project in projects:
                    if project.is_archived:
                        project.is_archived = False
                        if project.status == 2:
                            project.status = 0
                        project.save()
            elif operation == "add to group":
                l_id = bulks.pop(0)
                label = Label.objects.get(label_id=l_id, university=university)
                url_names = bulks
                pr = Startup.objects.filter(id__in=bulks, is_project=True)
                for project in pr:
                    project.labels.add(label)
        get_param = request.GET.copy()
        return HttpResponseRedirect("{0}?{1}".format(reverse('uni-manage-projects'), get_param.urlencode()))

    def get(self, request, university):
        assert isinstance(request, HttpRequest)
        if AccessLevel.PROJECTS not in request.access_levels and not request.is_super:
            raise Http404()
        industry_filter = request.GET.get("filter-options")
        labels = get_manageable_tags(request)

        view = request.GET.get("page-options")
        if view == None or view == "":
            view = 'all'
        if industry_filter != "" and industry_filter is not None:
            industry_filter = industry_filter.lower()
            projects = Startup.objects.select_related('industry') \
                .filter(university=university,
                        industry__name=industry_filter,
                        is_project=True)
            if view == 'archived':
                projects = projects.filter(is_archived=True)
            else:
                projects = projects.filter(is_archived=False)
        else:
            projects = Startup.objects.select_related('industry') \
                .filter(university=university, is_project=True)
            if view == 'archived':
                projects = projects.filter(is_archived=True)
            else:
                projects = projects.filter(is_archived=False)

        if projects is None:
            projects = Startup.objects.select_related('industry') \
                .filter(university=university, is_project=True)
            if view == 'archived':
                projects = projects.filter(is_archived=True)
            else:
                projects = projects.filter(is_archived=False)

        query = request.GET.get('q', '')
        if query:
            projects = projects.filter(name__icontains=query)

        projects = projects.distinct().annotate(
            total_funding=Coalesce(Sum('investment__amount'), Value(0))) \
            .values('id',
                    'name',
                    'url_name',
                    'industry__name',
                    'pitch',
                    'total_funding',
                    'is_archived',
                    'status')

        industries = get_industries(university.short_name)

        recent_projects, project_count = recently_added_projects(university)
        pending_projects = Startup.objects.select_related('industry') \
            .filter(university=university,
                    is_project=True,
                    status=Startup.PENDING).distinct() \
            .values('id',
                    'name',
                    'url_name',
                    'industry__name',
                    'created_on') \
            .order_by('-created_on')

        can_view_recent = True
        if university.style.restrict_tagged_admins and not request.is_startuptree_staff:
            admin_user = request.current_member
            can_view_recent = False
            if admin_user.labels.all():
                projects = projects.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=admin_user.labels.all()) | Q(num_tags=0)).distinct()
                pending_projects = pending_projects.annotate(num_tags=Count('labels')) \
                    .filter(Q(labels__in=admin_user.labels.all()) | Q(num_tags=0)).distinct()
            else:
                projects = projects.annotate(num_tags=Count('labels')) \
                    .filter(num_tags=0)
                pending_projects = pending_projects.annotate(num_tags=Count('labels')) \
                    .filter(num_tags=0)

        requires_approval = university.style.require_startup_approval
        allow_admin_jobs = university.style.allow_admin_jobs

        order_by = []
        name_order = request.GET.get('name_order', 'ascend')
        industry_order = request.GET.get('industry_order', 'none')
        if industry_order != 'none':
            is_reverse = True if industry_order == 'descend' else False
            if is_reverse:
                order_by.append('-industry__name')
            else:
                order_by.append('industry__name')
        if name_order == 'descend':
            order_by.append('-name')
        else:
            order_by.append('name')
        projects = projects.order_by(*order_by)

        page = request.GET.get('page')
        project_paginator = Paginator(projects, 15)
        try:
            project_page = project_paginator.page(page)
        except PageNotAnInteger:
            project_page = project_paginator.page(1)
        except EmptyPage:
            project_page = project_paginator.page(project_paginator.num_pages)
        prev_page, curr_pages, next_page = diggy_paginator(project_paginator, project_page.number)
        is_push_state = self.request.META.get('HTTP_X_PJAX', False)
        if is_push_state:
            template = get_template('trunk/profiles/search_project.html')
            context = {
                'projects': project_page,
                'searching': query,
                'prev_page': prev_page,
                'next_page': next_page,
                'curr_pages': curr_pages,
                'curr_page': project_page.number,
                'name_order': name_order,
                'industry_order': industry_order,
                'allow_admin_jobs': allow_admin_jobs,
                'requires_approval': requires_approval
            }
            context.update(static(request))
            content = template.render(context)
            data = {'results': content, 'title': 'Projects | {0}'.format(request.university),
                    'url': "{0}?{1}".format(reverse('uni-manage-projects'), request.GET.urlencode())}
            return JsonResponse(data)

        new_projects = get_added_project_num(university)

        younger_projects = count_project_by_ages(university, 1)
        mature_projects = count_project_by_ages(university, 3, False)

        return TemplateResponse(
            request,
            'trunk/profiles/manage_projects.html',
            {
                'page_name': 'manage-projects',
                'title': 'projects',
                'university': university,
                'view': view,
                'projects': project_page,
                'industries': industries,
                'active_industry': industry_filter,
                'total_projects': project_count,
                'new_projects': new_projects,
                'recent_projects': recent_projects,
                'pending_projects': pending_projects,
                'requires_approval': requires_approval,
                'allow_admin_jobs': allow_admin_jobs,
                'can_view_recent': can_view_recent,
                'young': younger_projects,
                'mature': mature_projects,
                'curr_pages': curr_pages,
                'prev_page': prev_page,
                'next_page': next_page,
                'curr_page': project_page.number,
                'name_order': name_order,
                'industry_order': industry_order,
                'labels': labels,
            }
        )


class StartupDecision(UniversityDashboardView):
    def post(self, request, university):
        current_site = request.current_site
        university = request.university
        startup_id = request.POST.get('startup_id')
        decision = request.POST.get('decision')
        startup = Startup.objects.get(id=startup_id, university=university)
        # rejected
        if decision == "2":
            startup.status = 2
            startup.is_archived = True
            startup.save()
        # approved
        elif decision == "1":
            startup.status = 1
            startup.save()
            startup_activity = StartupActivityClerk(current_site, startup)
            startup_activity.record_startup_created()
        notify_startup_creator.delay(startup.id, current_site.id)

        if startup.is_project:
            return HttpResponseRedirect(reverse('uni-manage-projects'))
        else:
            return HttpResponseRedirect(reverse('uni-manage-ventures'))


class ManageUsers(UniversityDashboardView):
    num_per_page = 15

    def prepare_users(self, request, university, view):
        query = request.GET.get('q', '')
        if view != 'guests':
            is_archived = view == 'archived'
            users = StartupMember.objects.select_related('user').filter(
                user__isnull=False, is_archived=is_archived, universities=university)
            affiliation_filter = request.GET.get("filter-options", '')
            audience_filter = request.GET.get("audience-filter", '')
            tag_filter = request.GET.get("tag-filter", '')
            if affiliation_filter.lower() == 'staff':
                users = users.filter(universityaffiliation__affiliation=1)
            elif affiliation_filter.lower() == 'faculty':
                users = users.filter(universityaffiliation__affiliation=2)
            elif affiliation_filter.lower() == 'community member':
                users = users.filter(universityaffiliation__affiliation=3)
            elif affiliation_filter.lower() == 'student':
                users = users.filter(universityaffiliation__affiliation=4)
                for user in users:
                    StartupMember.objects.get_is_alumni(user)
            elif affiliation_filter.lower() == 'alumni':
                users = users.filter(universityaffiliation__affiliation=5)
            # for BEN
            elif affiliation_filter.lower() == 'advisor':
                users = users.filter(universityaffiliation__affiliation=6)
            elif affiliation_filter.lower() == 'ceo':
                users = users.filter(universityaffiliation__affiliation=7)
            elif affiliation_filter.lower() == 'partner':
                users = users.filter(universityaffiliation__affiliation=8)
            elif affiliation_filter.lower() == 'none':
                users = users.filter(universityaffiliation__isnull=True)
            elif tag_filter  and tag_filter!='all':
                label = Label.objects.get(label_id=tag_filter, university=university)
                users = users.filter(labels__in=[label])
            if audience_filter and audience_filter!='all':
                mailchimp = UniversityMailChimpAPI.objects.get(university=university)
                audience = MailChimpAudience.objects.get(mail_chimp=mailchimp,audience_id =audience_filter )
                users = audience.members.all()
            if university.style.restrict_tagged_admins and not request.is_startuptree_staff:
                admin_user = request.current_member
                if admin_user.labels.all():
                    users = users.annotate(num_tags=Count('labels')) \
                        .filter(Q(labels__in=admin_user.labels.all()) | Q(num_tags=0)).distinct()
                else:
                    users = users.annotate(num_tags=Count('labels')) \
                        .filter(num_tags=0)
            queries = query.split(' ')
            if queries:
                for q in queries:
                    users = users.filter(Q(first_name__icontains=q) | Q(last_name__icontains=q))
                users = users.distinct()
            users = users.order_by('first_name', 'last_name')
        else:
            users = EventRSVP.objects.select_related('guest').filter(
                rsvp_event__university__id=university.id,
                guest__isnull=False, waitlisted=False)
            affiliation_filter = request.GET.get("filter-options", '')
            audience_filter = request.GET.get("audience-filter", '')
            tag_filter = request.GET.get("tag-filter", '')
            if affiliation_filter.lower() == 'student':
                users = users.filter(affiliation=1)
            elif affiliation_filter.lower() == 'alumni':
                users = users.filter(affiliation=2)
            elif affiliation_filter.lower() == 'faculty':
                users = users.filter(Q(affiliation=3) |
                                     Q(affiliation=6))
            elif affiliation_filter.lower() == 'staff':
                users = users.filter(Q(affiliation=4) |
                                     Q(affiliation=6))
            elif affiliation_filter.lower() == 'none':
                users = users.filter(affiliation=5)
            elif tag_filter  and tag_filter!='all':
                label = Label.objects.get(label_id=tag_filter, university=university)
                users = users.filter(labels__in=[label])
            queries = query.split(' ')
            if queries:
                for q in queries:
                    users = users.filter(Q(guest__first_name__icontains=q) | Q(guest__last_name__icontains=q))
            users = users.distinct('guest__email')
            users = users.order_by('guest__email', 'guest__first_name', 'guest__last_name')
        name_order = request.GET.get('name_order', 'ascend')
        if name_order == 'descend':
            users = users.reverse()
        # we have pagniation ! do not load all of them right now!
        # users = sorted([usr for usr in users],
        #                key=lambda usr: usr.get_full_name().lower())
        user_paginator = Paginator(users, self.num_per_page)
        page = request.GET.get('page')
        try:
            user_page = user_paginator.page(page)
        except PageNotAnInteger:
            user_page = user_paginator.page(1)
        except EmptyPage:
            user_page = user_paginator.page(user_paginator.num_pages)
        prev_page, cur_pages, next_page = diggy_paginator(user_paginator, user_page.number, adjacent_pages=5)
        if len(users) <= self.num_per_page:
            cur_pages = None
        return user_page, prev_page, next_page, cur_pages, affiliation_filter,tag_filter, audience_filter, name_order, query

    def post(self, request, university):
        if AccessLevel.USERS not in request.access_levels and not request.is_super:
            raise Http404()
        current_user_email = request.user.email
        bulk = request.POST.get("bulk-options", None)
        if bulk != "" and bulk is not None:
            bulks = bulk.split(",")
            operation = bulks.pop(0)
            if operation == "delete":
                url_names = bulks
                members = StartupMember.objects.filter(url_name__in=url_names)
                for member in members:
                    if not current_user_email == member.user.email:
                        member.user.delete()
            elif operation == "archive":
                url_names = bulks
                members = StartupMember.objects.filter(url_name__in=url_names)
                for member in members:
                    if not member.is_archived:
                        member.is_archived = True
                        member.save()
            elif operation == "unarchive":
                url_names = bulks
                members = StartupMember.objects.filter(url_name__in=url_names)
                for member in members:
                    if member.is_archived:
                        member.is_archived = False
                        member.save()
            elif operation == "add to group":
                l_id = bulks.pop(0)
                label = Label.objects.get(label_id=l_id, university=university)
                url_names = bulks
                members = StartupMember.objects.filter(url_name__in=url_names)
                for member in members:
                    member.labels.add(label)
            elif operation == "add to audience":
                mail_chimp_api = UniversityMailChimpAPI.objects.get(university=university)
                l_id = bulks.pop(0)
                url_names = bulks
                members = StartupMember.objects.filter(url_name__in=url_names)
                # adding new list
                if l_id == "new_audience":
                    # audience or list Name
                    list_name = request.POST.get("list_name", None)
                    list_id = create_list(mail_chimp_api.api_key,
                                          mail_chimp_api.server_code,
                                          list_name)
                    if list_id:
                        MailChimpAudience.objects.create_audience(mail_chimp_api, list_name, list_id)

                        for member in members:
                            response = add_members(mail_chimp_api.api_key,
                                        mail_chimp_api.server_code,
                                        list_id, member.user.email, member.first_name, member.last_name)
                            if response:
                                MailChimpAudience.objects.add_member(mail_chimp_api, list_id, member)


                else:
                    for member in members:
                        response = add_members(mail_chimp_api.api_key,
                                               mail_chimp_api.server_code,
                                               l_id, member.user.email,  member.first_name, member.last_name)
                        if response:
                            MailChimpAudience.objects.add_member(mail_chimp_api, l_id, member)
            elif operation == "invite":
                guest_ids = bulks
                guest_list = []
                guest_count = 0
                for guest_id in guest_ids:
                    try:
                        rsvp_guest = EventRSVP.objects.get(id=guest_id, guest__isnull=False)
                        guest = rsvp_guest.guest
                    except EventRSVP.DoesNotExist:
                        continue
                    guest_list.append([guest.first_name, guest.last_name, guest.email, guest_id])
                    guest_count += 1

                uni_style: UniversityStyle = university.style
                if university.program_name:
                    invitor_name = university.program_name
                else:
                    invitor_name = university.name
                if uni_style.email_invite_user_body:
                    email_subject = uni_style.email_invite_user_subject
                    email_body = uni_style.email_invite_user_body
                else:
                    email_subject = uni_style.generate_default_email_invite_user_subject()
                    email_body = uni_style.generate_default_email_invite_user_body()
                create_user_invite_bulk.delay(
                    guest_list,
                    university.id,
                    total=guest_count,
                    is_guest=True,
                    send_invite=True,
                    is_delay=True,
                    invitor_name=invitor_name,
                    email_subject=email_subject,
                    email_body=email_body)
            elif operation == 'invite-all':
                guest_list = list(EventRSVP.objects.select_related('guest', 'rsvp_event').filter(
                    rsvp_event__university_id=university.id,
                    guest__isnull=False, waitlisted=False,
                    guest__is_invited=False).distinct('guest__email')
                                  .order_by('guest__email', 'guest__first_name', 'guest__last_name')
                                  .values_list('guest__first_name', 'guest__last_name', 'guest__email', 'id'))
                guest_count = len(guest_list)
                uni_style: UniversityStyle = university.style
                if university.program_name:
                    invitor_name = university.program_name
                else:
                    invitor_name = university.name
                if uni_style.email_invite_user_body:
                    email_subject = uni_style.email_invite_user_subject
                    email_body = uni_style.email_invite_user_body
                else:
                    email_subject = uni_style.generate_default_email_invite_user_subject()
                    email_body = uni_style.generate_default_email_invite_user_body()
                create_user_invite_bulk.delay(
                    guest_list,
                    university.id,
                    total=guest_count,
                    is_guest=True,
                    send_invite=True,
                    is_delay=True,
                    invitor_name=invitor_name,
                    email_subject=email_subject,
                    email_body=email_body)
        get_param = request.GET.copy()
        return HttpResponseRedirect("{0}?{1}".format(reverse('uni-manage-users'), get_param.urlencode()))

    def get(self, request, university):
        if AccessLevel.USERS not in request.access_levels and not request.is_super:
            raise Http404()
        page_options = request.GET.get('page-options', 'all')

        try:
            mail_chimp_api = UniversityMailChimpAPI.objects.get(university=university)
            labels_audience = MailChimpAudience.objects.filter(mail_chimp = mail_chimp_api)
        except:
            mail_chimp_api = None
            labels_audience = None



        users, prev_page, next_page, cur_pages, affiliation_filter, tag_filter, audience_filter, name_order, query = \
            self.prepare_users(request, university, page_options)
        is_push_state = self.request.META.get('HTTP_X_PJAX', False)
        if is_push_state:
            template = get_template('trunk/people/search_user.html')
            context = {
                'view': page_options,
                'users': users,
                'searching': query,
                'prev_page': prev_page,
                'next_page': next_page,
                'cur_pages': cur_pages,
                'cur_page': users.number,
                'name_order': name_order,
                # 'num_refs_order': num_refs_order
            }
            context.update(static(request))
            content = template.render(context)
            data = {'results': content, 'title': 'User | {0}'.format(request.university),
                    'url': "{0}?{1}".format(reverse('uni-manage-users'), request.GET.urlencode())}
            return JsonResponse(data)
        labels = get_manageable_tags(request)



        export_tags = labels  # TODO (allow startuptreestaff only in export)
        bulk_add_tags = labels

        can_export = AccessLevel.EXPORT in request.access_levels or request.is_super
        uni_created_on = "{0}/{1}/{2}".format(university.created_on.month, university.created_on.day,
                                              university.created_on.year)
        now = timezone.now()
        today = "{0}/{1}/{2}".format(now.month, now.day,
                                     now.year)
        show_guests = university.style.show_guest_data

        return TemplateResponse(
            request,
            'trunk/people/manage_users.html',
            {
                'page_name': 'manage-users',
                'title': 'Users',
                'university': university,
                'view': page_options,
                'users': users,
                'prev_page': prev_page,
                'next_page': next_page,
                'cur_pages': cur_pages,
                'cur_page': users.number,
                'name_order': name_order,
                # 'num_refs_order': '',
                'affiliation_filter': affiliation_filter,
                'tag_filter': tag_filter,
                'audience_filter': audience_filter,
                'labels': bulk_add_tags,  # for bulk adding tags
                'labels_audience': labels_audience,  # for bulk adding tags
                'uni_labels_objs': export_tags,  # for export
                'can_export': can_export,
                'show_guests': show_guests,
                'uni_created_on': uni_created_on,
                'today': today
            }
        )


class ManagePages(UniversityDashboardView):
    def get(self, request, university):
        assert isinstance(request, HttpRequest)
        if AccessLevel.GROUPS not in request.access_levels and not request.is_super:
            raise Http404()
        page = request.GET.get('page')
        organizations = Organization.objects.filter(
            platform=university, is_template=False) \
            .order_by('name')

        pagniator = Paginator(organizations, 30)
        try:
            organizations = pagniator.page(page)
            page = int(page)
        except PageNotAnInteger:
            organizations = pagniator.page(1)
            page = 1
        except EmptyPage:
            organizations = pagniator.page(pagniator.num_pages)
            page = pagniator.num_pages

        prev_page, cur_pages, next_page = diggy_paginator(pagniator, page)
        return TemplateResponse(
            request,
            'trunk/platform/pages/manage_groups.html',
            {
                'page_name': 'manage-organizations',
                'title': 'Organizations',
                'university': university,
                'organizations': organizations,
                'prev_page': prev_page,
                'next_page': next_page,
                'cur_pages': cur_pages,
                'cur_page': page,
                'current_site_domain': request.current_site.domain
            }
        )


class ManageAdmins(UniversityDashboardView):
    def prepare_staffs(self, user, university):
        forum = AccessLevel.PRIVATE_DISCUSSION
        events = AccessLevel.EVENTS
        competitions = AccessLevel.COMPETITIONS
        applications = AccessLevel.APPLICATIONS
        staffs_tmp = university.uni_staffs.all()
        staffs = []
        for staff in staffs_tmp:
            s_unistaff = UniversityStaff.objects.get(
                admin_id=staff.id, university_id=university.id)
            is_me = False
            staff_access_levels = s_unistaff.access_levels.all().values_list('name', flat=True)
            num_access_levels = len(staff_access_levels)
            is_forum_only = False
            is_event_only = False
            is_app_and_comp = False
            is_event_and_app_and_comp = False
            if forum in staff_access_levels and num_access_levels == 1:
                is_forum_only = True
            if events in staff_access_levels and num_access_levels == 1:
                is_event_only = True
            if competitions in staff_access_levels and applications in staff_access_levels and \
                    num_access_levels == 2:
                is_app_and_comp = True
            if events in staff_access_levels and competitions in staff_access_levels and \
                    applications in staff_access_levels and num_access_levels == 3:
                is_event_and_app_and_comp = True
            try:
                s = StartupMember.objects.get_user(staff)
                if s.user_id == user.id:
                    is_me = True
                staffs.append({"id": staff.id,
                               "name": s.get_full_name(),
                               "email": staff.email,
                               "admin_team": s_unistaff.admin_team,
                               "registered": s.user.is_active,
                               "is_me": is_me,
                               "is_super": s_unistaff.is_super,
                               "is_forum_only": is_forum_only,
                               "is_event_only": is_event_only,
                               "is_app_and_comp": is_app_and_comp,
                               "is_event_and_app_and_comp": is_event_and_app_and_comp,
                               "access_levels": staff_access_levels,
                               "point_of_contact": s_unistaff.point_of_contact.all()
                               })
            except StartupMember.DoesNotExist:
                staffs.append({"id": staff.id,
                               "name": "Anonymous Admin",
                               "email": staff.email,
                               "admin_team": s_unistaff.admin_team,
                               "registered": False,
                               "is_me": is_me,
                               "is_super": s_unistaff.is_super,
                               "is_forum_only": is_forum_only,
                               "is_event_only": is_event_only,
                               "is_app_and_comp": is_app_and_comp,
                               "is_event_and_app_and_comp": is_event_and_app_and_comp,
                               "access_levels": staff_access_levels,
                               "point_of_contact": s_unistaff.point_of_contact.all()
                               })
        return staffs

    def post(self, request, university):
        if AccessLevel.ADMINS not in request.access_levels and not request.is_super:
            raise Http404()
        errors = []
        email = None
        submitted = False
        is_not_add_staff = False
        max_access_levels = len(AccessLevel.ACCESS_LEVELS)
        form = SignupAdditionalForm()
        if len(errors) == 0:
            errors = []
        if request.method == "POST":
            submitted = True
            try:
                # with transaction.atomic():
                form = SignupAdditionalForm(request.POST)
                if form.is_valid():  # When inviting new admin via "Add Admin" fields
                    email = request.POST.get('email')
                    if len(email.strip()) == 0:
                        errors.append("Please provide a valid email.")
                        raise ValueError()
                    email = email.lower()
                    try:
                        new_user = UserEmail.objects.get(email__iexact=email)
                        created = False
                    except UserEmail.DoesNotExist:
                        created = True
                        new_user = UserEmail.objects.create(email=email)
                    if not created:
                        for staff in university.uni_staffs.all():
                            if staff == new_user:
                                errors.append("{0} is already a staff "
                                              "member.".format(email))
                                raise ValueError()

                    access_level = request.POST.get('access_level')
                    admin_team_name = request.POST.get('admin_team_name')
                    if len(admin_team_name.strip()) == 0:
                        admin_team_name = None

                    if access_level == "super":
                        super_admin = UniversityStaff(
                            university=university,
                            admin=new_user,
                            is_super=True,
                            admin_team=admin_team_name
                        )
                        super_admin.save()
                        super_admin.uni_staff_access_levels__m2m_changed()
                    elif access_level == "forum_only":
                        forum_admin = UniversityStaff(
                            university=university,
                            admin=new_user,
                            admin_team=admin_team_name
                        )
                        forum_admin.save()
                        forum = AccessLevel.objects.get(
                            name=AccessLevel.PRIVATE_DISCUSSION, university=university)
                        forum_admin.access_levels.add(forum)
                        forum_admin.uni_staff_access_levels__m2m_changed()
                    elif access_level == "event_only":
                        event_admin = UniversityStaff(
                            university=university,
                            admin=new_user,
                            admin_team=admin_team_name
                        )
                        event_admin.save()
                        events = AccessLevel.objects.get(
                            name=AccessLevel.EVENTS, university=university)
                        event_admin.access_levels.add(events)
                        event_admin.uni_staff_access_levels__m2m_changed()
                    elif access_level == "app_and_comp":
                        app_comp_admin = UniversityStaff(
                            university=university,
                            admin=new_user,
                            admin_team=admin_team_name
                        )
                        app_comp_admin.save()
                        competitions = AccessLevel.objects.get(
                            name=AccessLevel.COMPETITIONS, university=university)
                        applications = AccessLevel.objects.get(
                            name=AccessLevel.APPLICATIONS, university=university)
                        app_comp_admin.access_levels.add(competitions)
                        app_comp_admin.access_levels.add(applications)
                        app_comp_admin.uni_staff_access_levels__m2m_changed()
                    elif access_level == "event_and_app_and_comp":
                        both_admin = UniversityStaff(
                            university=university,
                            admin=new_user,
                            admin_team=admin_team_name
                        )
                        both_admin.save()
                        events = AccessLevel.objects.get(
                            name=AccessLevel.EVENTS, university=university)
                        competitions = AccessLevel.objects.get(
                            name=AccessLevel.COMPETITIONS, university=university)
                        applications = AccessLevel.objects.get(
                            name=AccessLevel.APPLICATIONS, university=university)
                        both_admin.access_levels.add(events)
                        both_admin.access_levels.add(competitions)
                        both_admin.access_levels.add(applications)
                        both_admin.uni_staff_access_levels__m2m_changed()
                    elif access_level == "custom":
                        levels_to_add = request.POST.getlist('new-admin-levels')
                        custom_admin = UniversityStaff(
                            university=university,
                            admin=new_user,
                            admin_team=admin_team_name
                        )
                        custom_admin.save()
                        if len(levels_to_add) == max_access_levels:
                            custom_admin.is_super = True
                            custom_admin.save()
                        else:
                            for level in levels_to_add:
                                if AccessLevel.objects.filter(name=level).exists():
                                    access_level, _ = AccessLevel.objects.get_or_create(name=level,
                                                                                        university=university)
                                    custom_admin.access_levels.add(access_level)
                        custom_admin.uni_staff_access_levels__m2m_changed()
                    else:
                        errors.append("Please select appropriate access level.")
                        raise ValueError('No such access level exists!')
                    new_user.add_group(STAFF)
                    new_user.add_group(REGULAR)
                    try:
                        new = StartupMember.objects.get_user(new_user)
                        add_uni = True
                        name = new.get_first_name()
                        for tmp in new.universities.all():
                            if tmp.id == university.id:
                                add_uni = False
                                break
                        if add_uni:
                            new.universities.add(university)
                        app_logger.info("Add existing user as a staff.")
                        admin_invite = None

                        transaction.on_commit(lambda: send_staff_notification.delay(
                            new.id,
                            name,
                            university.site_id,
                            university.name,
                            university.name,
                            university.program_name
                        ))
                    except StartupMember.DoesNotExist:
                        new = form.save(new_user, university)
                        name = new.get_first_name()
                        transaction.on_commit(lambda:
                                              send_staff_invitation.delay(
                                                  new_user.id,
                                                  name,
                                                  university.site_id,
                                                  university.name,
                                                  university.name,
                                                  university.program_name
                                              )
                                              )
                    form = SignupAdditionalForm()
                    app_logger.info("Registered Staff")
                elif 'staff_id' in request.POST: # When editing specific user's access via "Customize Access" modal OR editing user's team name via "Update Admin Team Name" Modal
                    staff_id = request.POST.get('staff_id')
                    try:
                        admin = UniversityStaff.objects.get(
                            university=university,
                            admin_id=staff_id
                        )
                        is_not_add_staff = True

                        if 'update_team_name' in request.POST: # If editing user's team name via "Update Admin Team Name" Modal
                            updated_admin_team_name = request.POST.get('update-admin-team-name-{0}'.format(staff_id))
                            if len(updated_admin_team_name.strip()) == 0: # If the user left the input empty OR only whitespace
                                updated_admin_team_name = None
                            admin.admin_team = updated_admin_team_name
                            admin.save()
                        else:
                            levels_to_add = request.POST.getlist('edit-admin-levels-{0}'.format(staff_id))
                            pointofcontacts_to_add = request.POST.getlist('edit-admin-pointofcontact-{0}'.format(staff_id))

                            for old_level in admin.access_levels.all():
                                admin.access_levels.remove(old_level)
                            if len(levels_to_add) == max_access_levels:
                                admin.is_super = True
                                admin.save()
                            else:
                                if admin.is_super:
                                    admin.is_super = False
                                    admin.save()
                                for new_level in levels_to_add:
                                    if AccessLevel.objects.filter(name=new_level).exists():
                                        access_level, _ = AccessLevel.objects.get_or_create(name=new_level, university=university)
                                        admin.access_levels.add(access_level)
                            admin.uni_staff_access_levels__m2m_changed()

                            for old_pointofcontact in admin.point_of_contact.all():
                                admin.point_of_contact.remove(old_pointofcontact)
                            for new_pointofcontact in pointofcontacts_to_add:
                                if PointOfContact.objects.filter(name=new_pointofcontact).exists():
                                    pointofcontact = PointOfContact.objects.get(name=new_pointofcontact)
                                    admin.point_of_contact.add(pointofcontact) # Else editing specific user's access via "Customize Access" modal

                    except UniversityStaff.DoesNotExist:
                        raise ValueError('Admin does not exist')
                elif 'restrict_tags' in request.POST:
                    style = university.style
                    restrict_tags = request.POST.get('restrict_tags')
                    restrict_tags = True if restrict_tags == 'true' else False
                    style.restrict_tagged_admins = restrict_tags
                    style.save()
                else:
                    errors.append("Please provide a valid name and email.")
            except ValueError:
                errors.append("An error occured. Please try again, or contact support if the problem persists.")
        staffs = self.prepare_staffs(request.user, university)

        pointofcontact_all = PointOfContact.objects.all()

        return TemplateResponse(
            request,
            'trunk/people/manage_admins.html',
            {
                'page_name': 'manage-account',
                'title': 'Account',
                'university': university,
                'errors': errors,
                'staffs': staffs,
                'pointofcontact_all': pointofcontact_all,
                'form': form,
                'email': email,
                'is_not_add_staff': is_not_add_staff,
                'submitted': submitted,
            }
        )

    def get(self, request, university):
        if AccessLevel.ADMINS not in request.access_levels and not request.is_super:
            raise Http404()
        errors = []
        email = None
        submitted = False
        is_not_add_staff = False
        form = SignupAdditionalForm()
        if len(errors) == 0:
            errors = None

        staffs = self.prepare_staffs(request.user, university)

        pointofcontact_all = PointOfContact.objects.all()

        return TemplateResponse(
            request,
            'trunk/people/manage_admins.html',
            {
                'page_name': 'manage-account',
                'title': 'Account',
                'university': university,
                'errors': errors,
                'staffs': staffs,
                'pointofcontact_all': pointofcontact_all,
                'form': form,
                'email': email,
                'is_not_add_staff': is_not_add_staff,
                'submitted': submitted,
            }
        )


class ManageTemplates(UniversityDashboardView):
    def get(self, request, university):
        access_levels = request.access_levels
        is_super = request.is_super
        if not is_super and AccessLevel.EVENTS not in access_levels and \
                AccessLevel.COMPETITIONS not in access_levels and \
                AccessLevel.APPLICATIONS not in access_levels and \
                AccessLevel.SURVEYS not in access_levels and \
                AccessLevel.REPORTING not in access_levels and \
                AccessLevel.GROUPS not in access_levels:
            raise Http404()

        is_event_admin = is_super or AccessLevel.EVENTS in access_levels
        is_comp_admin = is_super or AccessLevel.COMPETITIONS in access_levels
        is_app_admin = is_super or AccessLevel.APPLICATIONS in access_levels
        is_survey_admin = is_super or AccessLevel.SURVEYS in access_levels
        is_report_admin = is_super or AccessLevel.REPORTING in access_levels
        is_page_admin = is_super or AccessLevel.GROUPS in access_levels

        comp_templates = []
        app_templates = []
        survey_templates = []
        report_templates = []
        page_templates = []
        all_templates = Event.objects.filter(university=university).filter(Q(is_saved=True) | Q(is_template_only=True))
        event_templates = all_templates.filter(is_competition=False, is_survey=False)
        ready_made_templates = Event.objects.filter(university__short_name="readymadetemplates").filter(
            Q(is_saved=True) | Q(is_template_only=True))

        events_exist = event_templates.exists() or \
                       ready_made_templates.filter(is_competition=False, is_survey=False).exists()
        comps_exist = all_templates.filter(is_competition=True, is_application=False).exists() or \
                      event_templates.filter(event_type=Event.COMPETITION).exists() or \
                      ready_made_templates.filter(is_competition=True, is_application=False).exists()
        apps_exist = all_templates.filter(is_competition=True, is_application=True).exists() or \
                     event_templates.filter(event_type=Event.APPLICATION).exists() or \
                     ready_made_templates.filter(is_competition=True, is_application=True).exists()
        surveys_exist = all_templates.filter(is_competition=False, is_survey=True).exists() or \
                        ready_made_templates.filter(is_competition=False, is_survey=True).exists()
        reports_exist = DataReport.objects.filter(university=university, is_template=True).exists()
        pages_exist = Organization.objects.filter(platform=university, is_template=True).exists()

        type_filter = request.GET.get('type', 'all')

        type_filter_options = [{'url_name': 'all', 'label': 'All'}]
        exclude_types = [Event.COMPETITION, Event.APPLICATION, Event.OTHER]
        if is_event_admin and events_exist:
            type_filter_options.append({'url_name': 'event', 'label': 'Event'})
            for event_type in Event.EVENT_TYPES:
                if event_type[0] not in exclude_types and \
                        event_templates.filter(event_type=event_type[0]).exists():
                    type_dict = {
                        'url_name': event_type[1].lower().replace(' ', '_').replace('/', '_'),
                        'label': event_type[1]
                    }
                    type_filter_options.append(type_dict)
        if is_comp_admin and comps_exist:
            type_filter_options.append({'url_name': 'competition', 'label': 'Competition'})
        if is_app_admin and apps_exist:
            app_word = request.university_style.application_word.capitalize()
            type_filter_options.append({'url_name': 'application', 'label': app_word})
        if is_survey_admin and surveys_exist:
            type_filter_options.append({'url_name': 'survey', 'label': 'Survey'})
        if is_report_admin and reports_exist:
            type_filter_options.append({'url_name': 'report', 'label': 'Report'})
        if is_page_admin and pages_exist:
            type_filter_options.append({'url_name': 'page', 'label': 'Landing Page'})
        if university.short_name != "readymadetemplates" and ready_made_templates.exists():
            type_filter_options.append({'url_name': 'ready_made', 'label': 'Ready Made'})
        if is_event_admin and event_templates.filter(event_type=Event.OTHER).exists():
            type_filter_options.append({'url_name': 'other', 'label': 'Other'})

        if type_filter == "all":
            ready_made_events = []
            ready_made_comps = []
            ready_made_apps = []
            ready_made_surveys = []
            ready_made_pages = []
            if is_event_admin:
                ready_made_events = ready_made_templates.filter(is_competition=False, is_application=False)
            else:
                event_templates = []
            if is_comp_admin:
                comp_templates = all_templates.filter(is_competition=True, is_application=False)
                ready_made_comps = ready_made_templates.filter(is_competition=True, is_application=False)
            if is_app_admin:
                app_templates = all_templates.filter(is_competition=True, is_application=True)
                ready_made_apps = ready_made_templates.filter(is_competition=True, is_application=True)
            if is_survey_admin:
                survey_templates = all_templates.filter(is_competition=False, is_survey=True)
                ready_made_surveys = ready_made_templates.filter(is_competition=False, is_survey=True)
            if is_report_admin:
                report_templates = DataReport.objects.filter(university=university, is_template=True)
            if is_page_admin:
                page_templates = Organization.objects.filter(
                    platform=university, is_template=True, built_with_new_page_builder=True)
                ready_made_pages = Organization.objects.filter(
                    platform__short_name="readymadetemplates", is_template=True, built_with_new_page_builder=True)
            ready_made_templates = list(chain(ready_made_events, ready_made_comps,
                                              ready_made_apps, ready_made_surveys, ready_made_pages))
        elif type_filter == "event" and is_event_admin:
            ready_made_templates = ready_made_templates.filter(is_competition=False, is_survey=False)
        elif type_filter == "career_fair" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.CAREER_FAIR)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.CAREER_FAIR)
        elif type_filter == "conference" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.CONFERENCE)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.CONFERENCE)
        elif type_filter == "networking" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.NETWORKING)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.NETWORKING)
        elif type_filter == "product_launch" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.PRODUCT_LAUNCH)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.PRODUCT_LAUNCH)
        elif type_filter == "talk_discussion" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.TALK)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.TALK)
        elif type_filter == "workshop" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.WORKSHOP)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.WORKSHOP)
        elif type_filter == "fundraising" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.FUNRAISING)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.FUNRAISING)
        elif type_filter == "hackathon" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.HACKATHON)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.HACKATHON)
        elif type_filter == "demo_day" and is_event_admin:
            event_templates = event_templates.filter(event_type=Event.DEMO_DAY)
            ready_made_templates = ready_made_templates.filter(
                is_competition=False, is_survey=False, event_type=Event.DEMO_DAY)
        elif type_filter == "competition":
            if is_event_admin:
                event_templates = event_templates.filter(event_type=Event.COMPETITION)
            else:
                event_templates = []
            if is_comp_admin:
                comp_templates = all_templates.filter(is_competition=True, is_application=False)
            ready_made_templates = ready_made_templates.filter(
                (Q(is_competition=False) & Q(is_survey=False) & Q(event_type=Event.COMPETITION)) |
                (Q(is_competition=True) & Q(is_application=False)))
        elif type_filter == "application":
            if is_event_admin:
                event_templates = event_templates.filter(event_type=Event.APPLICATION)
            else:
                event_templates = []
            if is_app_admin:
                app_templates = all_templates.filter(is_competition=True, is_application=True)
            ready_made_templates = ready_made_templates.filter(
                (Q(is_competition=False) & Q(is_survey=False) & Q(event_type=Event.APPLICATION)) |
                (Q(is_competition=True) & Q(is_application=True)))
        elif type_filter == "survey" and is_survey_admin:
            event_templates = []
            survey_templates = all_templates.filter(is_competition=False, is_survey=True)
            ready_made_templates = ready_made_templates.filter(is_competition=False, is_survey=True)
        elif type_filter == "report" and is_report_admin:
            event_templates = []
            ready_made_templates = []
            report_templates = DataReport.objects.filter(university=university, is_template=True)
        elif type_filter == "page" and is_page_admin:
            event_templates = []
            page_templates = Organization.objects.filter(platform=university, is_template=True)
            ready_made_templates = Organization.objects.filter(platform__short_name="readymadetemplates",
                                                               is_template=True)
        elif type_filter == "ready_made":
            event_templates = []
            ready_made_events = []
            ready_made_comps = []
            ready_made_apps = []
            ready_made_surveys = []
            ready_made_pages = []
            if is_event_admin:
                ready_made_events = ready_made_templates.filter(is_competition=False, is_application=False)
            if is_comp_admin:
                ready_made_comps = ready_made_templates.filter(is_competition=True, is_application=False)
            if is_app_admin:
                ready_made_apps = ready_made_templates.filter(is_competition=True, is_application=True)
            if is_survey_admin:
                ready_made_surveys = ready_made_templates.filter(is_competition=False, is_survey=True)
            if is_page_admin:
                ready_made_pages = Organization.objects.filter(platform__short_name="readymadetemplates",
                                                               is_template=True)
            ready_made_templates = list(chain(ready_made_events, ready_made_comps,
                                              ready_made_apps, ready_made_surveys, ready_made_pages))
        elif is_event_admin:
            event_templates = event_templates.filter(event_type=Event.OTHER)
            ready_made_templates = ready_made_templates.filter(is_competition=False, is_survey=False,
                                                               event_type=Event.OTHER)

        if university.short_name != "readymadetemplates":
            if type_filter == "ready_made":
                template_count = len(ready_made_templates)
            else:
                template_count = len(event_templates) + len(comp_templates) + len(app_templates) + \
                                 len(survey_templates) + len(report_templates) + len(page_templates) + len(
                    ready_made_templates)
        else:
            template_count = len(event_templates) + len(comp_templates) + len(app_templates) + \
                             len(survey_templates) + len(page_templates)

        return TemplateResponse(
            request,
            'trunk/manage/templates.html',
            {
                'page_name': 'manage-templates',
                'title': 'Templates',
                'university': university,
                'is_event_admin': is_event_admin,
                'is_comp_admin': is_comp_admin,
                'is_app_admin': is_app_admin,
                'is_survey_admin': is_survey_admin,
                'is_report_admin': is_report_admin,
                'is_page_admin': is_page_admin,
                'type_filter': type_filter,
                'type_filter_options': type_filter_options,
                'template_count': template_count,
                'event_templates': event_templates,
                'comp_templates': comp_templates,
                'app_templates': app_templates,
                'survey_templates': survey_templates,
                'report_templates': report_templates,
                'page_templates': page_templates,
                'ready_made_templates': ready_made_templates
            }
        )


class ManageTools(UniversityDashboardView):
    """
    Allows the admin to edit, create, or delete Tools
    """

    def post(self, request, university):
        if AccessLevel.TOOLS not in request.access_levels and not request.is_super:
            raise Http404()
        is_create = request.POST.get('is_create')
        is_delete = request.POST.get('is_delete')
        create_tool = (is_create == "true")
        delete_tool = (is_delete == "true")
        try:
            if ToolCategoryBlock.objects.filter(university=university).exists():
                blocks = ToolCategoryBlock.objects.filter(
                    university=university)
            else:
                blocks = None
            uc = ToolCategory.objects.get(
                name="Curated", is_curated=True, university=university)
        except ToolCategory.DoesNotExist:
            uc = ToolCategory.objects.create_category(
                "Curated", None, True, university)

        tools = Tool.objects.filter(
            university=university, category__is_curated=True)

        if not create_tool and not delete_tool:
            # Edit tool
            tool_id = request.POST.get('tool_id')
            try:
                tool = Tool.objects.get(university=university, id=tool_id)
            except Tool.DoesNotExist:
                return HttpResponseRedirect('/')
            name = request.POST.get('tool_name')
            price = request.POST.get('tool_price')
            description = request.POST.get('tool_description')
            website = request.POST.get('tool_website')
            uploaded_file = request.FILES.get('tool_file')

            if university.style.tools == 0:
                tool_category_id = request.POST.get('tool_category')
                try:
                    tool_category = ToolCategory.objects.get(id=tool_category_id, university=university)
                except ToolCategory.DoesNotExist:
                    tool_category = uc
            else:
                tool_category = uc

            tool.update_name(name)
            tool.update_price(price)
            tool.update_description(description)
            tool.update_website(website)
            tool.update_category(tool_category)

            if uploaded_file:
                form = ToolFileForm(request.POST, request.FILES)
                if form.is_valid():
                    try:
                        file_name = uploaded_file
                        tool.uploaded_file = uploaded_file
                        tool.save()
                    except KeyError:
                        pass
                    except ValidationError as e:
                        app_logger.warning(e)

            page_name = 'manage-tools'
            return TemplateResponse(
                request,
                'trunk/platform/pages/manage_tools.html',
                {
                    'title': 'Tools',
                    'page_name': page_name,
                    'blocks': blocks,
                    'tools': tools,
                    'university': university
                }
            )
        elif not create_tool and delete_tool:
            # Deleting Tool
            return self.delete(request, university)
        else:
            # Creating tool
            return self.create(request, university)

    def get(self, request, university):
        if AccessLevel.TOOLS not in request.access_levels and not request.is_super:
            raise Http404()
        try:
            if ToolCategoryBlock.objects.filter(university=university).exists():
                blocks = ToolCategoryBlock.objects.filter(
                    university=university).order_by('header')
            else:
                blocks = None

            uc = ToolCategory.objects.get(
                name="Curated", university=university)
            if not uc.is_curated:
                uc.is_curated = True
                uc.save()

            tools = Tool.objects.filter(
                category__is_curated=True, university=university)
            if len(tools) == 0:
                tools = None
        except ToolCategory.DoesNotExist:
            tools = None

        page_name = 'manage-tools'

        # tools_serialized = json.dumps([DealSerializer(deal).data for deal in tools])

        if blocks:
            blocks_serialized = json.dumps([ToolCategoryBlockSerializer(block).data for block in blocks])
        else:
            blocks_serialized = []

        return TemplateResponse(
            request,
            'trunk/platform/pages/manage_tools.html',
            {
                'title': 'Tools',
                'page_name': page_name,
                'blocks': blocks,
                'tools': tools,
                'blocks_serialized': blocks_serialized,
                'university': university
            }
        )

    def create(self, request, university):
        """ Create Tool """
        assert isinstance(request, HttpRequest)
        current_site = request.current_site
        if university is None:
            app_logger.warning("Securit Alert! - create tool:\
                                {0} isn't a admin!".format(request.user))
            return HttpResponseRedirect('/')

        new_tool = None
        name = request.POST.get('tool_name')
        price = request.POST.get('tool_price')
        description = request.POST.get('tool_description')
        website = request.POST.get('tool_website')
        uploaded_file = request.FILES.get('tool_file')

        try:
            category = ToolCategory.objects.get(
                name="Curated", is_curated=True, university=university)
        except ToolCategory.DoesNotExist:
            category = ToolCategory.objects.create_category(
                "Curated", None, True, university)

        if university.style.tools == 0:
            tool_category_id = request.POST.get('tool_category')
            try:
                tool_category = ToolCategory.objects.get(id=tool_category_id, university=university)
            except ToolCategory.DoesNotExist:
                tool_category = category
        else:
            tool_category = category

        new_tool = Tool.objects.create_tool(name=name,
                                            university=university,
                                            description=description,
                                            price=price,
                                            website=website,
                                            category=tool_category
                                            )

        if uploaded_file:
            form = ToolFileForm(request.POST, request.FILES)
            if form.is_valid():
                try:
                    file_name = uploaded_file
                    new_tool.uploaded_file = uploaded_file
                    new_tool.save()
                except KeyError:
                    pass
                except ValidationError as e:
                    app_logger.warning(e)

        response = redirect('uni-manage-tools')
        return response

    def delete(self, request, university):
        """ Delete Tool """
        assert isinstance(request, HttpRequest)
        if university is None:
            app_logger.warning("Securit Alert! - create tool:\
                                {0} isn't a admin!".format(request.user))
            return HttpResponseRedirect('/')
        tool_id = request.POST.get('tool_id')
        try:
            tool = Tool.objects.get(id=tool_id, university=university)
            tool.delete()
            response = redirect('uni-manage-tools')
            return response
        except Tool.DoesNotExist:
            response = redirect('uni-manage-tools')
            return response


@only_university
@login_required
def ManageTags(request, university):
    assert isinstance(request, HttpRequest)
    if AccessLevel.TAGS not in request.access_levels and not request.is_super:
        raise Http404()

    style = university.style
    alphabetize_tags = style.alphabetize_tags
    submitted = False
    if request.method == "POST":
        submitted = True
        alphabetize_tags = request.POST.get('alphabetize_tags')
        if alphabetize_tags == 'on':
            alphabetize_tags = True
        else:
            alphabetize_tags = False
        style.alphabetize_tags = alphabetize_tags
        style.save()
        # delete labels
        deleted_labels = request.POST.getlist('label_delete[]')
        for label in deleted_labels:
            try:
                Label.objects.get_label(label, university).delete()
            except (Label.DoesNotExist, ValueError):
                pass
        # edit labels
        existing_labels = request.POST.getlist('label_id[]')
        labels_names = request.POST.getlist('edit_label')
        labels_taggable = request.POST.getlist('taggable_by_users')
        labels_visible = request.POST.getlist('visible_to_users')

        for idx, edit_label in enumerate(existing_labels):
            try:
                label = Label.objects.get_label(edit_label, university)
                if label is None:
                    continue
            except ValueError:
                continue
            new_label = labels_names[idx]
            modified = False
            if new_label.lower().strip() != label.label_id:
                label.label_id = new_label.lower().strip()
                label.title = new_label
                modified = True
            if str(label.id) in labels_visible:
                if not label.visible_to_users:
                    label.visible_to_users = True
                    modified = True
                if str(label.id) in labels_taggable:
                    if not label.users_can_tag:
                        label.users_can_tag = True
                        modified = True
                elif label.users_can_tag:
                    label.users_can_tag = False
                    modified = True
            else:
                if label.users_can_tag:
                    label.users_can_tag = False
                    modified = True
                if label.visible_to_users:
                    label.visible_to_users = False
                    modified = True
            if modified:
                label.save()

        # create labels
        labels = request.POST.getlist('label[]')
        taggable = request.POST.getlist('new_taggable_by_users')
        visible = request.POST.getlist('new_visible_to_users')
        for label in labels:
            if not label:
                continue
            try:
                users_can_tag = False
                visible_to_users = False
                if label in visible:
                    visible_to_users = True
                    if label in taggable:
                        users_can_tag = True
                new_label = Label.objects.create_label(label, university,
                                                       users_can_tag=users_can_tag, visible_to_users=visible_to_users)
            except ValueError as e:
                app_logger.error(f'Error while making label: {e}')

    labels = get_manageable_tags(request)

    user_is_startuptreestaff = request.is_startuptree_staff

    return TemplateResponse(
        request,
        'trunk/profiles/customize_tags.html',
        {
            'title': 'Groups',
            'page_name': 'customize-groups',
            'university': university,
            'submitted': submitted,
            'labels': labels,
            'alphabetize_tags': alphabetize_tags,
            'user_is_startuptreestaff': user_is_startuptreestaff,
        }
    )


class ManageTicket(TemplateView):
    page_name = 'manage-tickets'
    title = 'Tickets'
    template = 'trunk/promote/manage_tickets.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ManageTicket, self).dispatch(*args, **kwargs)

    def get(self, request, ticket_id=None):
        if AccessLevel.SUPPORT not in request.access_levels and not request.is_super:
            raise Http404()
        university = request.is_university
        u_id = request.user.id
        if university is None:
            app_logger.warning(
                "{0} isn't a university user!".format(request.user))
            return HttpResponseRedirect('/')

        tickets = InboxConversationRequest.objects.select_related(
            'conversation__latest_inbox',
            'conversation__latest_inbox__member_obj',
        ).filter(
            # request_progress=filter_status,
            university_id=university.id).exclude(
            Q(
                conversation__rejected__id=u_id) | Q(
                conversation__participants=None)).order_by('-conversation__datetime')

        filter_status = request.GET.get("status")
        if filter_status != 'all' and filter_status is not None:
            tickets = tickets.filter(request_progress=filter_status)

        open_tickets = 0
        for ticket in tickets:
            if ticket.is_open():
                open_tickets += 1
        '''
        If a particular ticket is chosen:
        '''
        if ticket_id:
            ticket = InboxConversationRequest.objects.select_related(
                'conversation').get(conversation_id=ticket_id)
            r_types = RequestMessage.REQUESTS
            participant = ticket.conversation.get_participants_profiles()[0]
            conversations = Inbox.objects.filter(conversation_id=ticket_id) \
                .select_related('message') \
                .order_by('datetime')
            return TemplateResponse(
                request,
                self.template,
                {
                    'page_name': self.page_name,
                    'university': university,
                    'title': self.title,
                    'tickets': tickets,
                    'current_ticket': ticket,
                    'r_types': r_types,
                    'participant': participant,
                    'ticket_id': ticket_id,
                    'conversations': conversations,
                    'filter_status': filter_status
                }
            )
        return TemplateResponse(
            request,
            self.template,
            {
                'page_name': self.page_name,
                'university': university,
                'title': self.title,
                'tickets': tickets,
                'open_tickets': open_tickets,
                'filter_status': filter_status
            }
        )

    def post(self, request):
        if AccessLevel.SUPPORT not in request.access_levels and not request.is_super:
            raise Http404()
        ticket_id = request.POST.get('t')
        progress = request.POST.get('p')
        try:
            ticket = InboxConversationRequest.objects.get(id=ticket_id)
        except InboxConversationRequest.DoesNotExist:
            return JsonResponse({'status': 403})
        ticket.request_progress = progress
        ticket.save()
        percent = 'badge--warning'
        if progress == 'a':
            percent = 'badge--success'
        elif progress == 'c':
            percent = 'badge--disabled'
        elif progress == 'r':
            percent = 'badge--danger'
        prog = None
        for p in InboxConversationRequest.PROGRESS:
            if p[0] == progress:
                prog = p[1]
                break
        return JsonResponse({'status': 200,
                             'progress': prog,
                             'percent': percent})


class Announcement(TemplateView):
    page_name = 'mass-message'
    title = 'Announcement'
    template = 'trunk/promote/announcement.html'

    @method_decorator(login_required, only_university)
    def dispatch(self, *args, **kwargs):
        return super(Announcement, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        if AccessLevel.ANNOUNCE not in request.access_levels and not request.is_super:
            raise Http404()
        university = request.is_university
        u_id = request.user.id
        msg_type = InboxConversation.ANNOUNCEMENT
        subject = request.POST.get('subject')
        if subject is not None and len(subject.strip()) > 0:
            group_id = request.POST.get('group_id')
            if group_id is not None and len(group_id.strip()) > 0:
                try:
                    group = Label.objects.get(id=group_id)
                    msg_type = InboxConversation.GROUP
                except BaseException:
                    return HttpResponseRedirect(
                        reverse('uni-new-mass-msg') + '?g=e')
                invited_list = list(
                    StartupMember.objects.filter(user_id__in=university.uni_staffs.all().values_list('id', flat=True))
                )
                invited_list += list(StartupMember.objects
                                     .select_related('user')
                                     .filter(labels=group, user__isnull=False)
                                     .values_list('id', flat=True))
                new_conv_group = InboxConversationGroup.objects.create_group_conv(
                    subject, group, request.current_member, invited_list, university, type_index=msg_type)
                new_conv_id = new_conv_group.conversation_id
            else:
                new_conv_group = InboxConversationGroup.objects.create_group_conv(
                    subject,
                    None,
                    request.current_member,
                    [],
                    university,
                    type_index=msg_type
                )
                new_conv_id = new_conv_group.conversation_id
        else:
            # no subject, group msg requires subject.
            new_conv_id = InboxConversationGroup.objects.get_general_announcement(
                university, request.current_member).conversation_id

        '''
        handle message calls publish message to actually send a message to all university users
        '''
        handle_message(request, new_conv_id)
        # redirect to the message page
        return HttpResponseRedirect(
            reverse(
                'uni-view-messages',
                kwargs={
                    'msg_id': new_conv_id}))

    def get(self, request, *args, **kwargs):
        if AccessLevel.ANNOUNCE not in request.access_levels and not request.is_super:
            raise Http404()
        university = request.is_university
        msg_id = kwargs.get('msg_id')
        is_new = False
        announcement_description = TinyMCEWidget()
        groups = Label.objects.filter(
            university=university, field_type="groups")
        if request.university.style.alphabetize_tags:
            groups = groups.order_by('title')
        if 'new' in request.path:
            is_new = True
        u_id = request.user.id
        if university is None:
            app_logger.warning(
                "{0} isn't a university user!".format(request.user))
            return HttpResponseRedirect('/')
        univ = University.objects.get(site=request.current_site)
        """
        Get general public message.
        """
        g_convs = InboxConversationGroup.objects \
            .select_related(
            'conversation',
            'conversation__latest_inbox',
            'conversation__platform',
            'conversation__latest_inbox__member_obj',
            'conversation__latest_inbox__uni_obj',
            'group'
        ).filter(
            conversation__platform=university
        ).order_by('-id')

        filter_status = request.GET.get('group-filter')
        if filter_status is not None and filter_status != 'all':
            if filter_status == 'general':
                g_convs = g_convs.filter(group=None)
            else:
                g_convs = g_convs.filter(group=filter_status)

        if msg_id:
            threads = Inbox.objects \
                .select_related() \
                .filter(conversation_id=msg_id) \
                .order_by('datetime')
            selected_group_conv = InboxConversationGroup.objects \
                .select_related(
                'conversation',
                'group'
            ).get(conversation_id=msg_id)
            return TemplateResponse(
                request,
                self.template,
                {
                    'announcement_description': announcement_description,
                    'page_name': self.page_name,
                    'university': university,
                    'title': self.title,
                    'conv_id': msg_id,
                    'threads': threads,
                    'g_convs': g_convs,
                    'selected_group_conv': selected_group_conv,
                    'groups': groups
                }
            )

        return TemplateResponse(
            request,
            self.template,
            {
                'announcement_description': announcement_description,
                'page_name': self.page_name,
                'university': university,
                'title': self.title,
                'g_convs': g_convs,
                'is_new': is_new,
                'groups': groups
            }
        )


class ManageInvites(TemplateView):
    page_name = 'manage-invites'
    title = 'Invites'
    template = 'trunk/people/manage_invites.html'
    err_msg_session = 'uni-manage-invite-error'

    @method_decorator(login_required, only_university)
    def dispatch(self, *args, **kwargs):
        return super(ManageInvites, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if AccessLevel.INVITE not in request.access_levels and not request.is_super:
            raise Http404()
        univ = request.is_university
        uni_style: UniversityStyle = univ.style
        invitation_histories = InvitedMember.objects.select_related('member', 'member__user').filter(
            invited_platform_id=univ.id).order_by('-date_last_invitation_sent')
        num_invited = invitation_histories.count()
        num_accepted = invitation_histories.filter(date_signin__isnull=False).count()
        num_pending = invitation_histories.filter(date_signin__isnull=True).count()
        all_tags = get_manageable_tags(request)

        email_subject = uni_style.email_invite_user_subject \
            if uni_style.email_invite_user_subject \
            else uni_style.generate_default_email_invite_user_subject()
        email_description_text = uni_style.email_invite_user_body \
            if uni_style.email_invite_user_body \
            else uni_style.generate_default_email_invite_user_body()
        email_description = TextEditorForm2({'text': email_description_text})

        error_msg = None
        if request.session.get(self.err_msg_session):
            error_msg = request.session.get(self.err_msg_session)
            del request.session[self.err_msg_session]
        invitation_histories = invitation_histories.order_by(F('date_last_invitation_sent').desc(nulls_first=True),
                                                             'member__first_name')
        page = request.GET.get('page')
        pagniator = Paginator(invitation_histories, 30)
        try:
            invitation_histories = pagniator.page(page)
            page = int(page)
        except PageNotAnInteger:
            invitation_histories = pagniator.page(1)
            page = 1
        except EmptyPage:
            invitation_histories = pagniator.page(pagniator.num_pages)
            page = pagniator.num_pages
        prev_page, cur_pages, next_page = diggy_paginator(pagniator, page)
        return TemplateResponse(
            request,
            self.template,
            {
                'page_name': self.page_name,
                'title': self.title,
                'university': univ,
                'invitation_histories': invitation_histories,
                'num_invited': num_invited,
                'num_accepted': num_accepted,
                'num_pending': num_pending,
                'all_tags': all_tags,
                'email_subject': email_subject,
                'email_description': email_description,
                'msg': error_msg,
                'prev_page': prev_page,
                'next_page': next_page,
                'cur_pages': cur_pages,
                'cur_page': page
            }
        )

    def post(self, request, *args, **kwargs):
        if AccessLevel.INVITE not in request.access_levels and not request.is_super:
            raise Http404()

        msg = ""
        invitation_queue = []
        invalid_emails = []
        existing_emails = []

        univ = request.is_university
        uni_style: UniversityStyle = univ.style
        current_site = univ.site
        """
        @TAGINVITE
        here, we want to get request.POST.getlist('tags') to get the selected tags.
        Based on the selected tags, either we pass 'id' as value or text name as the values of the form.
        THen, using that information, do appropriate querying to "Label" object so that we can get them.
        Note that "Label" object is the legacy name somewhat. There is already "tag" object, which serves
        different purpose. DONOT get confused.
        (this is why naming something at the beginning is really important and hard)
        """
        tags_list_ids = request.POST.getlist('tag-invite[]')
        tags = list(Label.objects.filter(label_id__in=tags_list_ids).values_list('id', flat=True))

        # FIXME code in general is really messy. Need refactor

        if 'save_email_message' in request.POST:
            email_description = TextEditorForm2(request.POST)
            if email_description.is_valid():
                uni_style.email_invite_user_body = email_description.cleaned_data['text']
            email_subject = request.POST.get('custom-email-subject')
            if email_subject:
                uni_style.email_invite_user_subject = email_subject
            uni_style.save()
            return HttpResponseRedirect(reverse('uni-manage-invites'))

        email_subject = uni_style.email_invite_user_subject \
            if uni_style.email_invite_user_subject \
            else uni_style.generate_default_email_invite_user_subject()
        email_body = uni_style.email_invite_user_body \
            if uni_style.email_invite_user_body \
            else uni_style.generate_default_email_invite_user_body()

        if 'send_invitation' in request.POST:
            '''
            Sends an invitation email to a list of students in the excel file uploaded
            Precondition: columns are in the order of FIRST NAME, LAST NAME, EMAIL
            '''
            import_form = ImportForm(request.POST, request.FILES)
            if import_form.is_valid():
                '''
                Parses an Excel file and add EmailUser objects in the invitation_queue array
                '''
                try:
                    # list of InvitedMember object fork bulk create
                    invited_members = []
                    # For now, only allow .xls or .xlsx files.
                    excel_file = request.FILES['file']
                    # extension = excel_file.name.split(".")[-1]
                    content = excel_file.read()
                    wb = xlrd.open_workbook(file_contents=content)
                    invitee_lists = []
                    total = 0
                    for sheet in wb.sheets():
                        for row in range(sheet.nrows):
                            total = sheet.nrows
                            invitee_first = sheet.cell(
                                row, 0).value.strip()
                            invitee_last = sheet.cell(row, 1).value.strip()
                            invitee_email = sheet.cell(
                                row, 2).value.strip()
                            invitee_lists.append([invitee_first, invitee_last, invitee_email])
                    create_user_invite_bulk.delay(
                        invitee_lists,
                        univ.id,
                        tags,
                        total=total,
                        send_invite=True,
                        is_delay=True,
                        invitor_name=request.session.get('user_name'),
                        email_subject=email_subject,
                        email_body=email_body)
                    msg = "Invitations are being sent out... " \
                          "Please be patient as it may take sometime to process all invitations."
                except xlrd.biffh.XLRDError:
                    msg = "Unsupported format or corrupt file. \
                        Please make sure the file is valid \
                        Excel spreadsheets (xls, xlsx)."
                except AssertionError:
                    msg = "Error: Cannot import the file."
                    app_logger.exception(msg)
            else:
                msg = "An error occured. Please make sure you uploaded the correct file type with correct format."
        elif 'resend_invites' in request.POST:
            history_id = request.POST.get('rid')
            if not history_id:
                return JsonResponse({'status': 403, 'msg': 'Invalid history'})
            try:
                history = InvitedMember.objects.select_related(
                    'member__user').get(id=history_id)
            except BaseException:
                return JsonResponse({'status': 403, 'msg': 'Invalid history'})
            user_email = history.member.user.email
            invitation_queue.append(history.member.user_id)
            '''
            Re-Sends a university invite email to invited users
            '''
            admin_invite = True
            transaction.on_commit(lambda: send_invitations(
                request.session.get('user_name'),
                invitation_queue,
                current_site.id,
                univ.id,
                admin_invite,
                email_subject,
                email_body))
            return JsonResponse(
                {'status': 200, 'msg': 'Resent invitation to {0}'.format(user_email)})
        elif 'resend_all_invites' in request.POST:
            '''
            Resends invite email to all pending users
            '''
            pending_queue = InvitedMember.objects.select_related('member', 'member__user').filter(
                invited_platform_id=univ.id,
                is_staff=False,
                date_signin__isnull=True
            ).values_list('member__user_id', flat=True)
            admin_invite = True
            transaction.on_commit(lambda: send_invitations(
                request.session.get('user_name'),
                pending_queue,
                current_site.id,
                univ.id,
                admin_invite,
                email_subject,
                email_body))
            return JsonResponse(
                {'status': 200, 'msg': 'Successfully resent invitation to all pending users.'})
        elif 'send_single_invite' in request.POST:
            '''
            Sends single invite instead of mass invite
            '''
            invited_members = []
            invitee_first = request.POST.get("first")
            invitee_last = request.POST.get("last")
            invitee_email = request.POST.get("email")
            # Sends a university invite email to non-StartupTree users
            num_sent, msg = create_user_invite(invitee_email,
                                               existing_emails,
                                               invalid_emails,
                                               invitee_first,
                                               invitee_last,
                                               univ.id, tags,
                                               send_invite=True,
                                               invitor_name=request.session.get('user_name'),
                                               email_subject=email_subject,
                                               email_body=email_body)
        elif 'delete_pending' in request.POST:
            '''
            Removes pending invite
            '''
            history_id = request.POST.get('rid')
            if not history_id:
                return JsonResponse({'status': 403, 'msg': 'Invalid history'})
            try:
                history = InvitedMember.objects.select_related(
                    'member').get(id=history_id)
            except BaseException:
                return JsonResponse({'status': 403, 'msg': 'Invalid history'})
            if not history.date_signin:
                history.member.delete()
                history.delete()
        if msg:
            request.session[self.err_msg_session] = msg
        if 'reset_default' in request.POST:
            # 'None' denotes default for these fields.
            uni_style.email_invite_user_subject = None
            uni_style.email_invite_user_body = None
            uni_style.save()

        return HttpResponseRedirect(reverse('uni-manage-invites'))
