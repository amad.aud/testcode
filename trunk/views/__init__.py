# Django imports
# etc
import json
import datetime
import time
import uuid
from dateutil.relativedelta import relativedelta
from django.db import transaction
from django.db.models import Q
from django.http import HttpRequest, Http404
from django.http.response import (
    JsonResponse, HttpResponse
)
# from django.utils.html import strip_tags
from django.template.response import TemplateResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.core.files.uploadedfile import InMemoryUploadedFile

from StartupTree import settings
# venture imports
# mentorship import
from StartupTree.caches import get_uni_export
from StartupTree.caches import (
    set_total_funding,
    set_total_exit,
    set_total_revenue,
    # get_total_funding,
    # get_total_exit,
    set_company_size,
    set_members_by_school,
    get_members_by_school,
    set_members_by_year,
    get_members_by_year,
    # get_user_stat,
    set_user_stat,
)
# StartupTree
from StartupTree.loggers import app_logger
from StartupTree.settings import DEBUG
from StartupTree.utils import (
    number_to_dollar_string,
    size_down_pic
)
from trunk.perms import is_university_user
# activity imports
from branch.aggregate.funding import (
    get_sum_of_startup_funding,
    get_sum_of_startup_exits,
    get_total_platform_revenue
)
from branch.aggregate.member import (
    get_users_by_days
)
# branch imports
from branch.aggregate.venture import (
    get_employ_numbers,
    get_new_user_num,
    get_startups_by_days
)
from branch.models import (
    StartupMember,
    Startup,
    Label
)
from branch.utils.tags import get_manageable_tags_titles_and_objs
# data collection
from data_collection import aggregate as data_aggre
from emailuser.conf import STAFF, UNIVERSITY
# From emailuser
from emailuser.models import UserEmail
# forms
# From hamster
from trunk.aggregate import (
    group_members_by_school,
    group_members_by_grad_year,
    group_members_by_group,
    calculate_user_breakdowns,
    format_percent_with_total
)
from trunk.io import (
    xlrd,
    EXIT_FIELD,
    FUNDING_FIELD,
    PERSON_FILED,
    StartupDictionary,
    ImportStartup,
)
from trunk.modelforms import ImportForm
# trunk imports
from trunk.models import (
    University,
    UniversityStaff,
    UniversityStyle,
    School,
    UniversityExportHistory,
    RoadMapBlock,
    RoadMapColumn,
    RoadMap,
    AccessLevel,
    EventRSVP,
    Event
)
from trunk.perms import is_tag_restricted_admin, is_view_admin_only_startup_custom_questions
from trunk.tasks import send_staff_invitation
from trunk.views.abstract import UniversityDashboardView, UniversityDashboardJsonView
from django.views import View
from trunk.views.events import EventMetrics
from venture.models import (
    Opportunity)
# Newly Added Imports
from activity.aggregate.users import (
    get_list_of_users_activity,
    get_list_of_active_users_activity
)
from trunk.aggregate.events import (
    get_list_of_rsvp_users,
    get_list_of_application_users
)
from mentorship.aggregate.mentorship import (
    get_list_of_mentorship_users
)
from venture.aggregate.candidate import (
    get_list_of_candidate
)


class Home(UniversityDashboardView):
    def get(self, request, university):
        assert isinstance(request, HttpRequest)

        current_site = request.current_site
        if AccessLevel.DASHBOARD not in request.access_levels and not request.is_super:
            if request.is_university:
                return TemplateResponse(
                    request,
                    'trunk/admin_panel.html',
                    {
                        'title': 'My Pages',
                        'page_name': 'my_pages',
                        'university': university
                    }
                )
            else:
                raise Http404()

        # JEANS CODE
        start_date_filter = datetime.datetime.today()-relativedelta(months=6)
        end_date_filter = datetime.datetime.today()

        module_check = request.university_style

        has_mentorship = module_check.display_mentors
        has_competiton = module_check.display_competitions #These are the same just wording things
        has_application = module_check.display_applications #These are the same just wording things
        has_events = module_check.display_events
        has_jobs = module_check.display_jobs
        #display_forums
        #display_ventures

        included_user_groups = []


        all_users_from_activity = get_list_of_users_activity(university.id,start_date_filter,end_date_filter)

        activity_active_users = get_list_of_active_users_activity(university.id,start_date_filter,end_date_filter)

        if(has_events == 1):
            rsvp_users = get_list_of_rsvp_users(university.id,start_date_filter,end_date_filter)
            included_user_groups.append("RSVP Users")
        else:
            rsvp_users = []

        if(has_application == 1):
            application_users = get_list_of_application_users(university.id,start_date_filter,end_date_filter)
            included_user_groups.append("Competition Users")
        else:
            application_users = []

        if(has_mentorship == 1):
            mentorship_users = get_list_of_mentorship_users(university.id,start_date_filter,end_date_filter)
            included_user_groups.append("Mentorship Users")
        else:
            mentorship_users = []

        if(has_jobs == 1):
            job_users = get_list_of_candidate(university.id,start_date_filter,end_date_filter)
            included_user_groups.append("Job Application Users")
        else:
            job_users = []

        # TO-DO
        # List of users that joined/were a part of a startup in a certain site and date range
        # Joined a startup or
        #startup_members = StartupMember.objects.select_related('startupmember').prefetch_related

        user_data_dict = \
        { \
        "Mentorship_Users" : \
        {"raw_data":(mentorship_users)}, \
        "Job_Application_Users" : \
        {"raw_data":(job_users)}, \
        "Competition_Users" : \
        {"raw_data":(application_users)}, \
        "RSVP_Users" : \
        {"raw_data":(rsvp_users)}, \
        "Active_Users" : \
        {"raw_data":(activity_active_users)+(job_users)+(mentorship_users)+(application_users)+(rsvp_users)}, \
        "Total_Activity_Users" : \
        {"raw_data":(all_users_from_activity)} \
        }

        user_breakdown_result = calculate_user_breakdowns(user_data_dict)

        included_user_groups_data_dict = {}
        for i in included_user_groups:
            included_user_groups_data_dict[i] = user_breakdown_result[i.replace(" ","_")]

        # total_funding = get_total_funding(current_site.name)
        tmp_funding = get_sum_of_startup_funding(current_site)
        total_funding = number_to_dollar_string(tmp_funding)
        set_total_funding(current_site.name, total_funding)

        # total_exit = get_total_exit(current_site.name)
        tmp_exit = get_sum_of_startup_exits(current_site)
        total_exit = number_to_dollar_string(tmp_exit)
        set_total_exit(current_site.name, total_exit)

        tmp_revenue = get_total_platform_revenue(current_site)
        total_revenue = number_to_dollar_string(tmp_revenue)
        set_total_revenue(current_site.name, total_revenue)

        company_sizes = None  # get_company_size(current_site.name)
        if company_sizes is None:
            company_size, avg_company_size = \
                get_employ_numbers(university, percent=True)
            set_company_size(current_site.name, (company_size, avg_company_size))
        else:
            company_size = company_sizes[0]
            avg_company_size = company_sizes[1]

        startup_qset = Startup.objects.filter(university__site_id=current_site.id,
                                              is_project=False, is_archived=False,
                                              status=Startup.APPROVED)
        members = StartupMember.objects.filter(universities__id=university.id,
                                               is_archived=False) \
            .prefetch_related('degrees')
        total_startups = startup_qset.count()
        total_jobs = total_startups * avg_company_size

        has_school = True
        school_count = School.objects.filter(university_id=university.id).count()
        if school_count <= 1:
            has_school = False

        if has_school:
            members_by_school = get_members_by_school(current_site.name)
            if DEBUG:
                members_by_school = None
            if members_by_school is None:
                members_by_school = group_members_by_school(university)
                set_members_by_school(current_site.name, members_by_school)
        else:
            members_by_school = None

        group_count = Label.objects.filter(university_id=university.id,
            field_type="groups").count()
        members_by_groups = group_members_by_group(university)

        # apostrophes cause parsing problems when rendering charts, so we have to remove them here
        if members_by_school:
            for school in members_by_school:
                school[0] = school[0].replace("'", "")

        for group in members_by_groups:
            group[0] = group[0].replace("'", "")

        members_by_year = get_members_by_year(current_site.name)
        if members_by_year is None or len(members_by_year) <= 4:
            members_by_year, total_alumni, total_students, total_others, total_staff, total_faculty, \
            total_community_members, total_guests = \
                group_members_by_grad_year(university.id, False, True)
            set_members_by_year(
                current_site.name,
                (members_by_year, total_alumni, total_students, total_others, total_staff, total_faculty,
                 total_community_members, total_guests)
            )
        else:
            total_community_members = members_by_year[6]
            total_staff = members_by_year[4]
            total_faculty = members_by_year[5]
            total_others = members_by_year[3]
            total_alumni = members_by_year[1]
            total_students = members_by_year[2]
            total_guests = members_by_year[7]
            members_by_year = members_by_year[0]

        # BEN stuff
        total_advisors = 0
        total_ceo = 0
        total_partners = 0
        dict = []
        if university.short_name == 'bencolorado':
            advisors = members.filter(universityaffiliation__affiliation=6)
            total_advisors = advisors.count()
            ceo = members.filter(universityaffiliation__affiliation=7)
            total_ceo = ceo.count()
            partners = members.filter(universityaffiliation__affiliation=8)
            total_partners = partners.count()
            dict.append(['Advisors', total_advisors])
            dict.append(['CEOs', total_ceo])
            dict.append(['Community Partners', total_partners])
            members_by_year = dict
        messages_sent_qset = data_aggre.get_messages_sent(current_site)
        client_request_qset = data_aggre.get_client_request(current_site)
        data_points_set = data_aggre.get_data_points_by_days(university.id)

        total_page_view = data_aggre.get_total_page_views(client_request_qset)
        total_message_sent = data_aggre.get_total_messages_sent(messages_sent_qset)
        total_visitors = data_aggre.get_unique_visitors(client_request_qset)

        event_metrics = EventMetrics.get_metric(university, True)
        page_views, visitors = data_aggre.get_page_views_and_visitors_by_days(current_site)
        ticks_range = range(len(visitors[0]) // 5)
        # tmp_ustat = get_user_stat(current_site.name)

        new_users, total_user_signedup = get_new_user_num(university.id)
        # set_user_stat(current_site.name, (new_users, total_user_signedup))
        total_not = members.filter(universityaffiliation__isnull=True).count()
        total_user = members.count()
        return TemplateResponse(
            request,
            'trunk/admin_panel.html',
            {
                'title': 'Dashboard',
                'page_name': 'dashboard',
                'university': university,
                'total_funding': total_funding,
                'total_exit': total_exit,
                'total_revenue': total_revenue,
                'total_user': total_user,
                'total_alumni': total_alumni,
                'total_students': total_students,
                'total_others': total_others,
                'total_staff': total_staff,
                'total_faculty': total_faculty,
                'total_community_members': total_community_members,
                'total_not': total_not,
                'total_guests': total_guests,
                'total_advisors': total_advisors,
                'total_ceo': total_ceo,
                'total_partners': total_partners,
                'total_startups': total_startups,
                'total_jobs': total_jobs,
                'total_data_points': data_points_set,
                'new_users': new_users,
                'company_size': company_size,
                'average_company_size': avg_company_size,
                'has_school': has_school,
                'school_count': school_count,
                'members_by_school': members_by_school,
                'members_by_year': members_by_year,
                'total_page_view': total_page_view,
                'total_message_sent': total_message_sent,
                'total_visitors': total_visitors,
                'page_views': page_views,
                'visitors': visitors,
                'group_count': group_count,
                'members_by_groups': members_by_groups,
                'event_metrics': event_metrics,
                'ticks_range': ticks_range,
                'Mentorship_Users' : format_percent_with_total(user_breakdown_result['Mentorship_Users'],user_breakdown_result['Total_Users']),
                'Job_Application_Users' : format_percent_with_total(user_breakdown_result['Job_Application_Users'],user_breakdown_result['Total_Users']),
                'Competition_Users' : format_percent_with_total(user_breakdown_result['Competition_Users'],user_breakdown_result['Total_Users']),
                'RSVP_Users' : format_percent_with_total(user_breakdown_result['RSVP_Users'],user_breakdown_result['Total_Users']),
                'Active_Users' : user_breakdown_result['Active_Users'],
                'Active_Users_nice' : format_percent_with_total(user_breakdown_result['Active_Users'],user_breakdown_result['Total_Users']),
                'Total_Users' : (user_breakdown_result['Total_Users']),
                'has_mentorship' :  has_mentorship,
                'has_competiton' :  has_competiton,
                'has_application' :  has_application,
                'has_events' :  has_events,
                'has_jobs' :  has_jobs,
                'included_user_groups_data_dict' : included_user_groups_data_dict
            }
        )


class HomeLegacy(UniversityDashboardView):
    def get(self, request, university):
        return redirect(reverse('uni-dashboard'))


class HomeFilterDays(UniversityDashboardView):
    def get(self, request, university, days):
        days = int(days)
        current_site = request.current_site
        messages_sent_qset = data_aggre.get_messages_sent(current_site)
        client_request_qset = data_aggre.get_client_request(current_site)
        startup_qset = Startup.objects.filter(university__site_id=current_site.id,
                                              is_project=False)
        data = {}

        data['visitors'] = "+{}".format(data_aggre.get_unique_visitors_by_days(
            client_request_qset, days=days))
        data['views'] = "+{}".format(data_aggre.get_page_views_by_days(
            client_request_qset, days=days))
        data['msg'] = "+{}".format(data_aggre.get_messages_sent_by_days(
            messages_sent_qset, days=days))
        data['data_points'] = "+{}".format(data_aggre.get_data_points_by_days(
            university.id, days=days))
        data['users'] = "+{}".format(get_users_by_days(university, days=days))
        data['startups'] = "+{}".format(get_startups_by_days(startup_qset, days=days))
        return JsonResponse(data)


class ExportView(UniversityDashboardView):
    template = 'trunk/metrics/export/export_home.html'

    @method_decorator(transaction.atomic)
    def dispatch(self, *args, **kwargs):
        return super(ExportView, self).dispatch(*args, **kwargs)

    def get(self, request, export_id=None, university=None):
        if AccessLevel.EXPORT not in request.access_levels and not request.is_super:
            raise Http404()
        if export_id:
            exported = UniversityExportHistory.objects.get(id=int(export_id))
            response = HttpResponse(content_type="application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = f'attachment; filename="{exported.generate_filename()}"'
            response.write(bytes(exported.data))
            return response
        import_form = ImportForm()
        export_histories = UniversityExportHistory.objects.filter(university_id=university.id)
        if is_tag_restricted_admin(request.university, request.is_super, request.is_startuptree_staff):
            export_histories = export_histories.filter(tags__in=request.current_member.labels.all()).distinct()
        if not is_view_admin_only_startup_custom_questions(request, is_project=False):
            # user shouldn't be seeing custom questions
            # exclude_admin_only_startup_custom_questions = False means the export contains sensitive information
            export_histories = export_histories.exclude(category=UniversityExportHistory.GENERAL, exclude_admin_only_startup_custom_questions=False)
        export_histories = export_histories.order_by('-id')
        uni_created_on = "{0}/{1}/{2}".format(university.created_on.month, university.created_on.day,
                                              university.created_on.year)
        now = timezone.now()
        today = "{0}/{1}/{2}".format(now.month, now.day,
                                     now.year)

        uni_labels, uni_labels_objs = get_manageable_tags_titles_and_objs(request)

        return TemplateResponse(
            request,
            self.template,
            {
                'page_name': 'import-export',
                'title': 'Import &amp; Export',
                'import_form': import_form,
                'university': university,
                'errors': None,
                'submitted': False,
                'export_histories': export_histories,
                'uni_created_on': uni_created_on,
                'today': today,
                'uni_labels': uni_labels,
                'uni_labels_objs': uni_labels_objs,
            }
        )

    # import feature not provided anymore
    # def post(self, request, university):
    #     '''Import Excel File
    #     '''
    #     # error = "File was not uploaded correctly."
    #     error = "Import feature may not work properly."
    #
    #     import_form = ImportForm(request.POST, request.FILES)
    #     if import_form.is_valid():
    #         app_logger.info(
    #             "startup import: {0}".format(request.FILES['file']))
    #         try:
    #             # For now, only allow .xls or .xlsx files.
    #             excel_file = request.FILES['file']
    #             # extension = excel_file.name.split(".")[-1]
    #
    #             to_import = ImportStartup(university)
    #             field_start_index = {
    #                 'funding': 0,
    #                 'exit': 0,
    #                 'founder': 0,
    #                 'alumni': 0,
    #                 'advisor': 0,
    #                 'end': 0,
    #             }
    #             for idx, chunk in enumerate(excel_file.chunks()):
    #                 wb = xlrd.open_workbook(file_contents=chunk)
    #                 for sheet in wb.sheets():
    #                     for row in range(sheet.nrows):
    #                         if idx == 0 and row == 0:
    #                             pass
    #                         elif idx == 0 and row == 1:
    #                             col = 12
    #                             field = sheet.cell(row, col).value
    #                             if not field.lower() == 'linkedin':
    #                                 error = \
    #                                     'Please review excel sheet format.'
    #                                 raise AssertionError()
    #                             col = 13
    #                             field_start_index['funding'] = col
    #                             while 'funding' in \
    #                                     sheet.cell(row, col).value.lower():
    #                                 col += len(FUNDING_FIELD)
    #                             field_start_index['exit'] = col
    #                             col += len(EXIT_FIELD)
    #                             field_start_index['founder'] = col
    #                             f_col = 3
    #                             while 'founder' in \
    #                                     sheet.cell(0, f_col).value.lower():
    #                                 f_col += 1
    #                             col += (f_col - 2) * 8
    #                             field_start_index['alumni'] = col
    #                             col += len(PERSON_FILED)
    #                             while True:
    #                                 if sheet.cell(row, col).value.lower() == \
    #                                         'first name 1':
    #                                     break
    #                                 col += len(PERSON_FILED)
    #                             field_start_index['advisor'] = col
    #                             while True:
    #                                 if sheet.cell(row, col).value.lower() == \
    #                                         'first name 1':
    #                                     break
    #                                 col += len(PERSON_FILED)
    #                             field_start_index['end'] = col
    #                         else:
    #                             sd = StartupDictionary(university)
    #                             sd.set_name(sheet.cell(row, 0).value)
    #                             sd.set_founded_on(sheet.cell(row, 1).value)
    #                             sd.set_industry(sheet.cell(row, 2).value)
    #                             sd.set_geography(sheet.cell(row, 3).value,
    #                                              sheet.cell(row, 4).value)
    #                             sd.set_employees(sheet.cell(row, 5).value)
    #                             sd.set_pitch(sheet.cell(row, 6).value)
    #                             sd.set_general(sheet.cell(row, 7).value)
    #                             sd.set_email(sheet.cell(row, 8).value)
    #                             sd.set_website(sheet.cell(row, 9).value)
    #                             sd.set_facebook(sheet.cell(row, 10).value)
    #                             sd.set_twitter(sheet.cell(row, 11).value)
    #                             sd.set_linkedin(sheet.cell(row, 12).value)
    #                             f_col = field_start_index['funding']
    #                             e_col = field_start_index['exit']
    #                             while f_col < e_col:
    #                                 amount = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 date = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 inv = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 is_ind = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 if amount == '' and date == '' and \
    #                                                 inv == '':
    #                                     continue
    #                                 sd.set_funding(amount, date, inv, is_ind)
    #                             e_value = sheet.cell(row, e_col).value
    #                             e_col += 1
    #                             e_date = sheet.cell(row, e_col).value
    #                             e_col += 1
    #                             e_type = sheet.cell(row, e_col).value
    #                             sd.set_exit(e_value, e_date, e_type)
    #                             f_col = field_start_index['founder']
    #                             a_col = field_start_index['alumni']
    #                             while f_col < a_col:
    #                                 fn = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 ln = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 t = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 e = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 p = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 w = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 g = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 s = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 if fn == '' or ln == '':
    #                                     continue
    #                                 sd.set_founder(fn, ln, t, e, p, w, g, s)
    #                             f_col = a_col
    #                             a_col = field_start_index['advisor']
    #                             while f_col < a_col:
    #                                 fn = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 ln = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 if fn == '' or ln == '':
    #                                     continue
    #                                 sd.set_alumni(fn, ln)
    #                             f_col = a_col
    #                             a_col = field_start_index['end']
    #                             while f_col < a_col:
    #                                 fn = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 ln = sheet.cell(row, f_col).value
    #                                 f_col += 1
    #                                 if fn == '' or ln == '':
    #                                     continue
    #                                 sd.set_advisors(fn, ln)
    #                             to_import.add_startup_dict(sd)
    #             to_import.create_startup()
    #             error = None
    #             import_form = ImportForm()
    #         except xlrd.biffh.XLRDError:
    #             error = "Unsupported format or corrupt file. \
    #                 Please make sure the file is valid \
    #                 Excel spreadsheets (xls, xlsx)."
    #         except AssertionError:
    #             app_logger.exception("Error: Cannot import the file.")
    #     return TemplateResponse(
    #         request,
    #         self.template,
    #         {
    #             'page_name': 'import-export',
    #             'title': 'Import &amp; Export',
    #             'import_form': import_form,
    #             'university': university,
    #             'errors': error,
    #             'submitted': True,
    #         }
    #     )


class DeleteAdmin(UniversityDashboardJsonView):
    def post(self, request, university):
        staff_id = request.POST.get('sid')
        try:
            staff = UserEmail.objects.get(id=staff_id)
        except UserEmail.DoesNotExist:
            return JsonResponse({'status': 500, 'msg': 'Incorrect Request.'})
        UniversityStaff.objects.get(university_id=university.id, admin_id=staff.id).delete()
        tmp = University.objects.filter(uni_staffs__id=staff.id)
        if len(tmp) == 0:
            staff.remove_group(STAFF)
            staff.remove_group(UNIVERSITY)
        return JsonResponse({'status': 200})


class ResendAdmin(UniversityDashboardJsonView):
    def post(self, request, university):
        '''
        Precondition: staff <UserEmail object> exists
        '''
        app_logger.info("Resend staff email method")
        assert isinstance(request, HttpRequest)
        staff_id = request.POST.get('sid')
        try:
            staff = UserEmail.objects.get(id=staff_id)
        except UserEmail.DoesNotExist:
            return JsonResponse({'status': 500, 'msg': 'Incorrect Request.'})
        '''
        Send an invite admin email
        '''
        try:
            staff_sm = StartupMember.objects.get_user(staff)
            add_uni = True
            first_name = staff_sm.get_first_name()
            for tmp in staff_sm.universities.all():
                if tmp.id == university.id:
                    add_uni = False
                    break
            if add_uni:
                staff_sm.universities.add(university)
                app_logger.info("Add existing user as a staff.")
        except StartupMember.DoesNotExist:
            first_name = "Anonymous Admin"
        app_logger.info("enqueue invitation")
        transaction.on_commit(lambda: send_staff_invitation.delay(
            staff.id,
            first_name,
            university.site_id,
            university.name,
            university.name,
            university.program_name
        ))
        app_logger.info("Registered Staff")
        return JsonResponse({'status': 200, 'msg': 'Resent an admin invitation'})


class ViewRoadMap(View):

    def get(self, request):  # no impact on database / might be branch
        """
        backend method for admin development of roadmap page
        """
        current_site = request.current_site
        university = request.university
        current_user = request.user
        is_admin = AccessLevel.ROADMAP in request.access_levels or request.is_super
        try:
            roadmap = RoadMap.objects.get(university=university)
        except RoadMap.DoesNotExist:
            roadmap = RoadMap.objects.create(university=university)
        is_private = university.style.display_roadmap == UniversityStyle.DISPLAY_PRIVATE

        rm_columns = RoadMapColumn.objects.filter(road_map=roadmap).order_by('number')
        rm_blocks = RoadMapBlock.objects.filter(road_map=roadmap)

        column_lst = list(rm_columns)

        # blocks in two or more consecutive columns span all columns they belong to
        # TODO: handling for cases when a block is in columns 1, 2, 4 or 1, 3, 4

        multi_column_blocks = []
        multi_column_block_ids = []
        for block in rm_blocks.order_by('id'):
            column_numbers = []
            for column in block.rm_columns.all():
                column_numbers.append(column.number)
            if len(column_numbers) > 1 and \
                    ((0 in column_numbers and 1 in column_numbers) or \
                    (1 in column_numbers and 2 in column_numbers) or \
                    (2 in column_numbers and 3 in column_numbers) or \
                    (3 in column_numbers and 4 in column_numbers) or \
                    (4 in column_numbers and 5 in column_numbers) or \
                    (5 in column_numbers and 6 in column_numbers)):
                multi_column_blocks.append(block)
                multi_column_block_ids.append(block.id)

        # prepping for templateresponse
        block_lsts = []
        for i in range(len(column_lst)):
            tmp_blocks = list(rm_blocks.filter(rm_columns__number=i).exclude(
                id__in=multi_column_block_ids).order_by('id'))
            block_lsts.append(tmp_blocks)

        numbers = []
        for i in range(len(column_lst)):
            numbers.append(i)

        # header is "untitled" if column.name is None or == ''
        headers = [column.name if column.name else 'untitled' for column in column_lst]

        display_columns = []
        for i in range(len(column_lst)):
            display_columns.append(is_admin or (headers[i] != 'untitled' or len(block_lsts[i]) > 0))

        return TemplateResponse(
            request,
            'app/roadmap_new.html',
            {
                'page_name': 'roadmap',
                'user_full_name': request.session.get('user_full_name'),
                'university': current_site.name,
                'title': 'RoadMap',
                'current_user': current_user,
                'is_private': is_private,
                'roadmap': roadmap,
                'is_admin': is_admin,
                'multi_column_blocks': multi_column_blocks,
                # blocks
                'block_lsts': block_lsts,
                # columns
                'column_lst': column_lst,
                # column headers
                'headers': headers,
                'headers_obj': rm_columns,  # to access RoadMapColumn id's
                # display columns
                'display_columns': display_columns,
                'numbers': numbers,
            }
        )


class ManageRoadMap(ViewRoadMap):
    def addColumn(self, request, roadmap, n):
        # cannot add more than 7 columns
        if n > 6:
            pass
        else:
            try:
                return RoadMapColumn.objects.get(road_map=roadmap, number=n)
            except RoadMapColumn.DoesNotExist:
                RoadMapColumn.objects.create(road_map=roadmap, number=n)

    def deleteColumn(self, request, roadmap, n):
        rm_col = RoadMapColumn.objects.filter(road_map=roadmap)
        rm_col.get(number=n).delete()
        for i in range(n+1, 11):
            try:
                col = rm_col.get(number=i)
                col.decr_num()
            except:
                pass

    def editColumnTitles(self, request):
        col_ids = request.POST.getlist('RoadMapColumnIDs')
        col_vals = request.POST.getlist('titles')

        for c_id, val in zip(col_ids, col_vals):
            column = RoadMapColumn.objects.get(id=c_id)

            # Assertion safeguard against modifying column of another university.
            if request.university != column.road_map.university:
                app_logger.error(f"Error: Saving the column (id = {column.id}) "
                                 f"of a university ({column.road_map.university}) that is different "
                                 f"than current university ({request.university}).")
                break

            if column.name != val:
                column.name = val
                column.save()

    def add(self, request, roadmap, block_id, block_title, block_text,
            block_image_url, block_file_url, block_columns, website, rm_block_color):
        rm_blocks = RoadMapBlock.objects.filter(road_map=roadmap)

        if block_image_url:
            thumbnail, image = size_down_pic(block_image_url)
            thumbnail_name = \
                "{0}_thumb_{1}.png".format(
                    block_title, uuid.uuid4())
            thumbnail_file = InMemoryUploadedFile(
                thumbnail, None, thumbnail_name,
                'image/png', thumbnail.tell(), None
            )

        if website and 'http://' not in website and 'https://' not in website:
            website = 'http://' + website

        r = RoadMapBlock.objects.create(
            title=block_title,
            text=block_text,
            char_limit=500,
            time_created=datetime.datetime.now(),
            university=roadmap.university,
            road_map=roadmap,
            website=website,
            uploaded_file=block_file_url,
            rm_block_color = rm_block_color
        )
        r.save()

        rm_column = RoadMapColumn.objects.get(number=block_columns, road_map=roadmap)
        r.rm_columns.add(rm_column)

        if block_image_url:
            if thumbnail_name is not None and thumbnail_file is not None:
                r.image.save(thumbnail_name, thumbnail_file)
            thumbnail.close()
            image.close()

    def edit(self, request, roadmap, block_id, block_title, block_text,
             block_image_url, block_file_url, block_columns, block_website, rm_block_color):
        if block_website and 'http://' not in block_website and 'https://' not in block_website:
            block_website = 'http://' + block_website
        if block_id is not None and block_id != 'default':
            block_id_int = int(block_id)
            block = RoadMapBlock.objects.get(id=block_id_int)
            block.title = block_title
            block.text = block_text
            block.char_limit = 500
            block.image = block_image_url
            block.road_map = roadmap
            block.website = block_website
            block.rm_block_color = rm_block_color
            if block_file_url:
                block.uploaded_file = block_file_url
            block.save()

            rm_column = RoadMapColumn.objects.get(number=block_columns, road_map=roadmap)
            block.rm_columns.remove(*block.rm_columns.all())
            if rm_column not in block.rm_columns.all():
                block.rm_columns.add(rm_column)

    def delete(self, request, roadmap):
        block_num = request.POST.get("block-id", "None")
        blocks = RoadMapBlock.objects.filter(road_map=roadmap)
        block = blocks.get(id=int(block_num))
        block.delete()

    def post(self, request):
        """
        upon submission of the edit-modal after admin is done editing
        """
        block_id = request.POST.get('block-id')
        block_title = request.POST.get('block-title')
        block_text = request.POST.get('block-text')
        block_image_url = request.FILES.get('block-image-url')
        block_file_url = request.FILES.get('block-file-url')
        rm_block_color = request.POST.get('rm_block_color')
        # How to handle this as well?
        block_additions = request.POST.get('addto_col')
        block_website = request.POST.get('block_website')
        current_site = request.current_site
        university = University.objects.get(site=current_site)
        roadmap = RoadMap.objects.get(university=university)

        if rm_block_color == '#ffffff':
            rm_block_color = roadmap.university.style.first_color

        if block_text == '':
            # TODO
            app_logger.error('No description was provided.')

        is_admin = AccessLevel.ROADMAP in request.access_levels or request.is_super

        # How to iterate on this??
        block_columns=block_additions

        if is_admin:
            action = request.POST.get("action", "default")
            if block_website and 'http://' not in block_website and 'https://' not in block_website:
                block_website = 'http://' + block_website
            if action == "add":
                self.add(request, roadmap, block_id, block_title, block_text,
                    block_image_url, block_file_url, block_columns, block_website, rm_block_color)
            elif action == "edit":
                self.edit(request, roadmap, block_id, block_title, block_text,
                    block_image_url, block_file_url, block_columns, block_website, rm_block_color)
            elif action == "delete":
                self.delete(request, roadmap)
            elif action == "add_title":
                self.addColumn(request, roadmap, RoadMapColumn.objects.filter(road_map=roadmap).count())
            elif action == 'edit_titles':
                self.editColumnTitles(request)
            elif action.find("del_title") != -1:
                col_id = int(action[len(action) - 1])
                self.deleteColumn(request, roadmap, col_id)
            else:
                return redirect('discover_roadmap')

        return redirect('discover_roadmap')
