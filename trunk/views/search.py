from django.http import JsonResponse
from django.views.decorators.http import require_GET
from trunk.documents import MajorDocument
from StartupTree.utils.search import escape_query


@require_GET
def autocomplete_major(request):
    query = request.GET.get('q', '')
    query = escape_query(query)
    suggestions = []
    if not query:
        return JsonResponse(suggestions, safe=False)
    sq = MajorDocument.search().suggest('auto_complete', query, completion={'field': 'suggest', 'size': 20})
    response = sq.execute()
    response = response.suggest.auto_complete[0].options
    for result in response:
        suggestions.append(result.text)
    return JsonResponse(suggestions, safe=False)
