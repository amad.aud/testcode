from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
# from mailchimp_marketing import Client
from StartupTree.utils import make_thumb
from api.models import APIToken, BrowserAPIToken, BrowserAPIReferrer
from branch.modelforms import (
    UniStyleForm,
    URLForm,
    TextEditorForm2
)
from hamster.tasks import schedule_update_css
from trunk.decorators import only_university
from trunk.modelforms import (
    UniversityStyleForm,
    UniversityFeedForm,
    UniversityGlobal1Form,
    UniversityGlobal2Form,
    UniversityGroupGlobalForm,
    UniversityPageForm,
    UniversityEmailForm,
    UniversityPermissionForm,
    UniversityOtherForm,
    UniversitySubscriptionForm,
    UniversityGroupForm
)
from trunk.models import (
    UniversityStyle,
    UniversitySubscription,
    StartupTreeStaff, UniversityMailChimpAPI
)

try:
    import StringIO
except ImportError:
    import io as StringIO
import mailchimp_marketing as MailchimpMarketing
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import transaction
from django.http import HttpRequest
from django.http.response import (
    Http404,
    HttpResponseBadRequest
)
# from django.utils.html import strip_tags
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils import timezone
# StartupTree
from StartupTree.loggers import app_logger
# branch imports
from branch.models import (
    SkillGroup,
    Label,
    UniversitySkillGroup, StartupMember
)
# trunk imports
from trunk.models import School, UniversityEmailDomain, AccessLevel
from trunk.views.abstract import UniversityDashboardView
from trunk.context_processor import platform_related


@only_university
@login_required
def customize_general(request, university):
    assert isinstance(request, HttpRequest)
    if AccessLevel.PROGRAM not in request.access_levels and not request.is_super:
        raise Http404()

    MEMBER_DOMAIN = UniversityEmailDomain.MEMBER
    GUEST_DOMAIN = UniversityEmailDomain.GUEST

    submitted = False
    errors = {}
    style = request.university_style
    program_name = university.program_name
    if program_name is None:
        program_name = university.name

    if style.job_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        job_article = 'an'
    else:
        job_article = 'a'
    if style.mentor_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
        mentor_article = 'an'
    else:
        mentor_article = 'a'

    default_welcome = "Thanks for joining {0}'s entrepreneurship network! \
    Here are some things you can do as you get started: \
    <ul><li>Add your {1} or {2}</li> \
    <li>Create {3} {4} posting to recruit new members</li>\
    <li>Send {5} {6} request</li></ul> \
    But first, let's verify your email address.".format(program_name,
                                                        style.venture_word, style.project_word, job_article,
                                                        style.job_word,
                                                        mentor_article, style.mentor_word)

    if style.welcome_email is None or style.welcome_email == '':
        welcome_email_body = TextEditorForm2({'text': default_welcome})
    else:
        welcome_email_body = TextEditorForm2({'text': style.welcome_email})
    # about = ''
    # if style is not None:
    #     about = style.markdown_about

    if request.method == "POST":
        submitted = True
        style = university.style
        university_name = request.POST.get('uni-name', "")
        university_name_sanity = university_name.strip().lower()
        if university_name_sanity != university.name.lower() and \
                len(university_name_sanity) > 0:
            university.name = university_name
            university.site.name = university_name
            university.site.save()
            request.session['user_full_name'] = university_name
            university.save()
        if university.style is None:
            style = UniversityStyle.objects \
                .create(_university=university)
            university.style = style
            university.save()
        program_name = request.POST.get('program-name', "")
        program_name_sanity = program_name.lower().strip()
        if len(program_name_sanity) == 0:
            program_name = None
        if program_name:
            university.program_name = program_name
            university.save()
            university = university
        elif university.program_name is not None:
            university.program_name = None
            university.save()
        default_uni_name = request.POST.get('default-uni-name', "")
        default_uni_name_sanity = default_uni_name.lower().strip()
        if len(default_uni_name_sanity) == 0:
            default_uni_name = None
        if default_uni_name:
            style.default_uni_name = default_uni_name
            style.save()
        elif style.default_uni_name is not None:
            style.default_uni_name = None
            style.save()
        enable_alma_matters = request.POST.get('enable_alma_matters', 'off')
        if enable_alma_matters == 'on' and not university.enable_alma_matters:
            university.enable_alma_matters = True
            university.save()
            Label.objects.create_label("Alma Matters member", university)
        elif enable_alma_matters == 'off' and university.enable_alma_matters:
            university.enable_alma_matters = False
            university.save()
            if Label.objects.get_label("Alma Matters member", university) is not None:
                Label.objects.get_label("Alma Matters member", university).delete()
        enable_central_events_calendar = request.POST.get('enable-central-events-calendar')
        if enable_central_events_calendar == '1' and not style.enable_central_events_calendar:
            style.enable_central_events_calendar = UniversityStyle.ENABLED
            style.save()
        elif enable_central_events_calendar == '0' and style.enable_central_events_calendar:
            style.enable_central_events_calendar = UniversityStyle.DISABLED
            style.save()
        central_events_calendar = request.POST.get('central-events-calendar', "")
        central_events_calendar_sanity = central_events_calendar.lower().strip()
        if len(central_events_calendar_sanity) == 0 or \
                style.enable_central_events_calendar == UniversityStyle.DISABLED:
            central_events_calendar = None
        if central_events_calendar:
            style.central_events_calendar = central_events_calendar
            style.save()
        elif style.central_events_calendar is not None:
            style.central_events_calendar = None
            style.save()
        # delete schools
        deleted_schools = request.POST.getlist('school_delete[]')
        for school_id in deleted_schools:
            School.objects.get(id=school_id).delete()
        # check edited schools
        existing_schools = request.POST.getlist('school_id[]')
        school_names = request.POST.getlist('edit_school')
        for idx, school_id in enumerate(existing_schools):
            school = School.objects.get(id=school_id)
            name = school_names[idx]
            if school.name != name.lower().strip():
                school.name = name.lower().strip()
                school.displayed = name
                school.save()
        # add new schools
        schools = request.POST.getlist('new_school')
        for school in schools:
            if school.strip() != '':
                School.objects.create_school(school, university)
        # delete labels
        deleted_labels = request.POST.getlist('label_delete[]')
        for label in deleted_labels:
            try:
                label = Label.objects.get_label(label, university).delete()
            except Label.DoesNotExist:
                pass
        # edit labels
        existing_labels = request.POST.getlist('label_id[]')
        labels_names = request.POST.getlist('edit_label')
        for idx, edit_label in enumerate(existing_labels):
            try:
                label = Label.objects.get_label(edit_label, university)
                if label is None:
                    continue
            except ValueError:
                continue
            new_label = labels_names[idx]
            if new_label.lower().strip() != label.label_id:
                label.label_id = new_label.lower().strip()
                label.title = new_label
                label.save()
        # create labels
        labels = request.POST.getlist('label[]')
        for label in labels:
            try:
                Label.objects.create_label(label, university)
            except ValueError:
                pass

        email_domains = request.POST.getlist('email-domains[]')
        guest_email_domains = request.POST.getlist('guest-email-domains[]')
        submitted_list = []
        for email_domain in email_domains:
            email_domain = email_domain.strip()
            if len(email_domain) == 0:
                continue
            valid_url = URLForm({'url': email_domain})
            if valid_url.is_valid():
                email_domain_obj = UniversityEmailDomain.objects.add_domain(
                    university, email_domain, MEMBER_DOMAIN)
                submitted_list.append(email_domain_obj.id)
            else:
                errors['email-domains'] = "{0} is not a valid email address domain.".format(
                    email_domain)
        for guest_email_domain in guest_email_domains:
            guest_email_domain = guest_email_domain.strip()
            if len(guest_email_domain) == 0:
                continue
            valid_url = URLForm({'url': guest_email_domain})
            if valid_url.is_valid():
                email_domain_obj = UniversityEmailDomain.objects.add_domain(
                    university, guest_email_domain, GUEST_DOMAIN)
                submitted_list.append(email_domain_obj.id)
            else:
                errors['guest-email-domains'] = "{0} is not a valid email address domain.".format(
                    guest_email_domain)
        # Delete any domains not submitted
        UniversityEmailDomain.objects.filter(
            university_id=university.id).exclude(id__in=submitted_list).delete()

        welcome_email = TextEditorForm2(request.POST)
        if welcome_email.is_valid():
            email_body = welcome_email.cleaned_data['text']
            if email_body == '<br>':
                email_body = default_welcome
        else:
            email_body = default_welcome
        style.welcome_email = email_body

        homepage_blurb = request.POST.get('homepage-blurb', '')
        if len(homepage_blurb) > 1000:
            errors['homepage-blurb'] = "The homepage blurb must be less than 1000 characters long."
        else:
            style.homepage_blurb = homepage_blurb

        style.save()
        welcome_email_body = TextEditorForm2({'text': style.welcome_email})

    #     texts = TextEditorForm(request.POST)
    #     if texts.is_valid():
    #         about = texts.cleaned_data['text']
    #     if style is None:
    #         style = \
    #             UniversityStyle.objects.create_with_about(university,
    #                                                       markdown_about=about)
    #         university.style = style
    #         university.save()
    #     else:
    #         style.set_about(about)
    #         style.save()
    # else:
    #     texts = TextEditorForm(initial={'text': about})

    if errors == {}:
        errors = None

    email_domain_limits = UniversityEmailDomain.objects.filter(
        university_id=university.id, type=MEMBER_DOMAIN).values_list('domain', flat=True)
    guest_email_domain_limits = UniversityEmailDomain.objects.filter(
        university_id=university.id, type=GUEST_DOMAIN).values_list('domain', flat=True)

    schools = School.objects.filter(
        university_id=university.id).order_by('name')
    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_program.html',
        {
            'title': 'General',
            'page_name': 'customize-general',
            'current_schools': schools,
            'university': university,
            'uni_style': style,
            'errors': errors,
            'submitted': submitted,
            # 'texts': texts,
            'welcome_email_body': welcome_email_body,
            'homepage_blurb': style.homepage_blurb,
            'email_domain_limits': email_domain_limits,
            'guest_email_domain_limits': guest_email_domain_limits,
        }
    )


@only_university
@login_required
def customize_design(request, university):
    assert isinstance(request, HttpRequest)
    if AccessLevel.DESIGN not in request.access_levels and not request.is_super:
        raise Http404()

    submitted = False
    color_change_msg = None
    if university.style is None:
        style = UniversityStyle.objects.create(_university=university)
        university.style = style
        university.save()
    else:
        style = UniversityStyle.objects.get(_university=university)
    c1 = '#0ea284'
    c2 = '#0A8D72'
    errors = {}
    if request.method == "POST":
        submitted = True
        style_form = UniStyleForm(request.POST, request.FILES)

        if style_form.is_valid():
            # set uni logo
            logo = style_form.cleaned_data['logo']
            if logo is not None:
                style.ori_logo = logo
                logo_thumbnail, file_ios = make_thumb(logo)
                new_logo_name = "thumb_{0}.png".format(university.short_name)
                logo_thumbnail_file = InMemoryUploadedFile(
                    logo_thumbnail, None, new_logo_name, 'image/png',
                    logo_thumbnail.tell(), None)
                style.logo.save(new_logo_name, logo_thumbnail_file)
                for file in file_ios:
                    file.close()
            # set mentorship banner
            banner = style_form.cleaned_data['banner']
            if banner is not None:
                style.mentor_banner = banner
                banner_thumbnail, file_ios = make_thumb(banner)
                new_banner_name = "thumb_{0}_{1}.png".format(
                    university.short_name, university.style.mentorship_word)
                banner_thumbnail_file = InMemoryUploadedFile(
                    banner_thumbnail, None, new_banner_name, 'image/png',
                    banner_thumbnail.tell(), None)
                style.mentor_banner.save(new_banner_name, banner)
                for file in file_ios:
                    file.close()
        else:
            app_logger.error(style_form.errors)

        nc1 = request.POST.get('c1')
        nc2 = request.POST.get('c2')

        new_colors = [c1, c2]
        color_changed = False
        if nc1 != c1:
            style.first_color = nc1
            new_colors[0] = nc1
            color_changed = True
        if nc2 != c2:
            style.second_color = nc2
            new_colors[1] = nc2
            color_changed = True

        enable_user_dashboard = request.POST.get('user_dashboard', 1)
        style.enable_user_dashboard = int(enable_user_dashboard)

        media_option = UniversityStyleForm(request.POST)
        if media_option.is_valid():
            eship = media_option.cleaned_data['eship_website']
            web = media_option.cleaned_data['uni_website']
            blog = media_option.cleaned_data['uni_blog']
            style.eship_website = eship
            style.uni_website = web
            style.uni_blog = blog
            facebook = request.POST.get('facebook')
            twitter = request.POST.get('twitter')
            instagram = request.POST.get('instagram')
            slack = request.POST.get('slack')
            if facebook is not None and 'facebook.com' in facebook:
                style.facebook = facebook
            elif facebook is None or facebook == '':
                style.facebook = None
            if twitter is not None and 'twitter.com' in twitter:
                style.twitter = twitter
            elif twitter is None or twitter == '':
                style.twitter = None
            if instagram is not None and 'instagram.com' in instagram:
                style.instagram = instagram
            elif instagram is None or instagram == '':
                style.instagram = None
            if slack is not None and 'slack.com' in slack:
                style.slack = slack
            elif slack is None or slack == '':
                style.slack = None
            style.timezone = media_option.cleaned_data['timezone']
        else:
            app_logger.error(str(media_option.errors))

        style.save()
        if color_changed:
            # schedule generating css. schedule_update_css involves saving style object, and thus should be scheduled
            # at the very end.
            transaction.on_commit(lambda: schedule_update_css.delay(university.id, new_colors))
            color_change_msg = \
                "New color scheme will be applied in a few minutes."
        request.university_style = style
    media_option = UniversityStyleForm(instance=style)
    logo = None
    enable_user_dashboard = True
    mentor_banner = None
    if request.university_style:
        logo = request.university_style.logo
        enable_user_dashboard = request.university_style.enable_user_dashboard
        mentor_banner = request.university_style.mentor_banner

    if style is not None:
        if style.first_color is not None:
            c1 = style.first_color
        if style.second_color is not None:
            c2 = style.second_color
    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_design.html',
        {
            'page_name': 'customize-design',
            'title': 'Design',
            'university': university,
            'style': style,
            'logo': logo,
            'c1': c1,
            'c2': c2,
            'enable_user_dashboard': enable_user_dashboard,
            'mentor_banner': mentor_banner,
            'media_option': media_option,
            'color_change_msg': color_change_msg,
            'submitted': submitted,
            'errors': errors,
        }
    )


@only_university
@login_required
def customize_platform(request, university):
    assert isinstance(request, HttpRequest)
    if AccessLevel.PLATFORM not in request.access_levels and not request.is_super:
        raise Http404()

    submitted = False
    if university.style is None:
        style = UniversityStyle.objects.create(_university=university)
        university.style = style
        university.save()
    else:
        style = UniversityStyle.objects.get(_university=university)

    errors = {}
    if request.method == "POST":
        submitted = True

        # set toggle options
        email_option = UniversityEmailForm(request.POST)
        permission_option = UniversityPermissionForm(request.POST)
        other_option = UniversityOtherForm(request.POST)
        if email_option.is_valid():
            style.enable_event_reminders = \
                email_option.cleaned_data['enable_event_reminders']
            style.send_platform_wide_deadline_reminders = \
                email_option.cleaned_data['send_platform_wide_deadline_reminders']
            style.send_rsvp_reminders = \
                email_option.cleaned_data['send_rsvp_reminders']
            style.send_new_user_notis = \
                email_option.cleaned_data['send_new_user_notis']
            style.mentor_request_noti = \
                email_option.cleaned_data['mentor_request_noti']
            style.meeting_request_noti = \
                email_option.cleaned_data['meeting_request_noti']
            style.custom_questions_noti = \
                email_option.cleaned_data['custom_questions_noti']
            style.community_forum_noti = \
                email_option.cleaned_data['community_forum_noti']
            style.save()
        if permission_option.is_valid():
            style.require_startup_approval = \
                permission_option.cleaned_data['require_startup_approval']
            style.require_meeting_approval = \
                permission_option.cleaned_data['require_meeting_approval']
            style.require_mentor_review_meetings = \
                permission_option.cleaned_data['require_mentor_review_meetings']
            style.allow_admin_jobs = \
                permission_option.cleaned_data['allow_admin_jobs']
            style.require_job_deadlines = \
                permission_option.cleaned_data['require_job_deadlines']
            style.enable_private_discussion = \
                permission_option.cleaned_data['enable_private_discussion']
            style.user_profile_public = \
                permission_option.cleaned_data['user_profile_public']
            style.venture_profile_public = \
                permission_option.cleaned_data['venture_profile_public']
            style.save()
        if other_option.is_valid():
            style.tools = \
                other_option.cleaned_data['tools']
            style.show_guest_data = \
                other_option.cleaned_data['show_guest_data']
            style.display_pages_fields = \
                other_option.cleaned_data['display_pages_fields']
            style.save()

        style.mentor_schedule_start = int(request.POST.get('mentor_schedule_start'))
        # style.display_mentor_btn = int(request.POST.get('display_mentor_btn'))

        mentor_url = request.POST.get('mentor_url')
        valid_url = URLForm({'url': mentor_url})
        if mentor_url != '' and valid_url.is_valid():
            style.mentor_url = mentor_url
        else:
            style.mentor_url = None

        if university.universitysubscription.is_team_mentorship:
            team_mentee_opt_out = request.POST.get('team_mentee_opt_out')
            admin_notify_of_team_meeting_scheduled = request.POST.get('admin_notify_of_team_meeting_scheduled')
            admin_notify_of_team_meeting_cancelled = request.POST.get('admin_notify_of_team_meeting_cancelled')
            if team_mentee_opt_out is not None:
                style.team_mentee_opt_out = team_mentee_opt_out
            if admin_notify_of_team_meeting_scheduled is not None:
                style.admin_notify_of_team_meeting_scheduled = admin_notify_of_team_meeting_scheduled
            if admin_notify_of_team_meeting_cancelled is not None:
                style.admin_notify_of_team_meeting_cancelled = admin_notify_of_team_meeting_cancelled

        style.save()
        style.refresh_from_db()

    email_option = UniversityEmailForm(instance=style)
    permission_option = UniversityPermissionForm(instance=style)
    other_option = UniversityOtherForm(instance=style)

    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_platform.html',
        {
            'page_name': 'customize-platform',
            'title': 'Platform',
            'university': university,
            'style': style,
            'email_option': email_option,
            'permission_option': permission_option,
            'other_option': other_option,
            'submitted': submitted,
            'errors': errors,
        }
    )


@only_university
@login_required
def customize_pages(request, university):
    if AccessLevel.PAGES not in request.access_levels and not request.is_super:
        raise Http404()

    submitted = False
    if university.style is None:
        style = UniversityStyle.objects.create(_university=university)
    else:
        style = UniversityStyle.objects.get(_university=university)
    is_private_platform = university.is_private_platform

    errors = {}
    if request.method == "POST":
        submitted = True

        # set toggle options
        feed_option = UniversityFeedForm(request.POST, instance=style)
        if feed_option.is_valid():
            feed_option.save()

        global_option = UniversityGlobal1Form(request.POST, instance=style)
        if global_option.is_valid() and (not is_private_platform or request.platform_group):
            global_option.save()
        is_group = False
        if request.platform_group:
            is_group = True
            group_global_option = UniversityGroupGlobalForm(request.POST, instance=style)
            if group_global_option.is_valid():
                group_global_option.save()
        global_display_option = UniversityGlobal2Form(request.POST, instance=style, is_group=is_group)
        if global_display_option.is_valid() and (not is_private_platform or request.platform_group):
            global_display_option.save()

        page_option = UniversityPageForm(request.POST, instance=style)
        if page_option.is_valid():
            page_option.save()

    feed_option = UniversityFeedForm(instance=style)
    global_option = UniversityGlobal1Form(instance=style)
    if request.platform_group:
        global_display_option = UniversityGlobal2Form(instance=style, is_group=True)
        group_global_option = UniversityGroupGlobalForm(instance=style)
    else:
        global_display_option = UniversityGlobal2Form(instance=style)
        group_global_option = None
    page_option = UniversityPageForm(instance=style)

    page_option_excludes = []
    if not request.is_engage:
        page_option_excludes.append('display_mentors')
    if not request.is_team_mentorship:
        page_option_excludes.append('display_team_mentors')
        page_option_excludes.append('default_team_mentors_view')
    if not request.is_resource:
        page_option_excludes.append('display_deals')

    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_pages.html',
        {
            'page_name': 'customize-pages',
            'title': 'Pages',
            'university': university,
            'style': style,
            'feed_option': feed_option,
            'global_option': global_option,
            'global_display_option': global_display_option,
            'group_global_option': group_global_option,
            'page_option': page_option,
            'page_option_excludes': page_option_excludes,
            'submitted': submitted,
            'errors': errors,
        }
    )


@only_university
@login_required
def customize_roles(request, university):
    if AccessLevel.CUSTOM_ROLES not in request.access_levels and not request.is_super:
        raise Http404()

    current_site = request.current_site
    if university.program_name:
        uni_name = university.program_name
    else:
        uni_name = university.name

    submitted = False
    errors = []
    if request.method == "POST":
        submitted = True
        # select/deselect roles
        selected_roles = request.POST.getlist('existing-roles')
        UniversitySkillGroup.objects.filter(
            university_id=university.id).delete()
        bulk_list = []
        for selected_role in selected_roles:
            bulk_list.append(UniversitySkillGroup(
                university_id=university.id, skillgroup_id=selected_role))
        UniversitySkillGroup.objects.bulk_create(bulk_list)
        # add new roles
        new_roles = request.POST.getlist('new-role')
        for new_role in new_roles:
            new_role = new_role.strip()
            if SkillGroup.objects.filter(name__iexact=new_role).exists():
                new_role_obj = SkillGroup.objects.filter(
                    name__iexact=new_role)[0]
            else:
                new_role_obj = SkillGroup.objects.create(
                    name=new_role, is_default=False)
            if UniversitySkillGroup.objects.filter(university_id=university.id, skillgroup=new_role_obj).exists():
                errors.append(
                    "The role '{0}' already exists.".format(new_role))
            else:
                UniversitySkillGroup.objects.create(
                    university_id=university.id, skillgroup=new_role_obj)

    default_roles = SkillGroup.objects.filter(is_default=True).order_by('name')
    tmp = UniversitySkillGroup.objects.select_related('skillgroup').filter(
        university_id=university.id).values_list('skillgroup__name')
    custom_roles = SkillGroup.objects.filter(
        name__in=tmp, is_default=False).distinct().order_by('name')
    selected_roles = SkillGroup.objects.filter(
        name__in=tmp).values_list('name', flat=True)

    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_roles.html',
        {
            'page_name': 'customize-roles',
            'title': 'Custom Roles',
            'university': university,
            'uni_name': uni_name,
            'default_roles': default_roles,
            'custom_roles': custom_roles,
            'selected_roles': selected_roles,
            'submitted': submitted,
            'errors': errors
        }
    )


@only_university
@login_required
def customize_text(request, university):
    if AccessLevel.CUSTOM_TEXT not in request.access_levels and not request.is_super:
        raise Http404()

    submitted = False
    if university.style is None:
        style = UniversityStyle.objects.create(_university=university)
        university.style = style
        university.save()
    else:
        style = UniversityStyle.objects.get(_university=university)

    uni_enable_dashboard = style.enable_user_dashboard

    errors = {}
    context = {}
    if request.method == "POST":
        submitted = True

        mentor_word = request.POST.get('mentor_word', '')
        mentee_word = request.POST.get('mentee_word', '')
        mentorship_word = request.POST.get('mentorship_word', '')
        venture_word = request.POST.get('venture_word', '')
        project_word = request.POST.get('project_word', '')
        job_word = request.POST.get('job_word', '')
        application_word = request.POST.get('application_word', '')
        page_word = request.POST.get('page_word', '')
        deal_word = request.POST.get('deal_word', '')
        team_word = request.POST.get('team_word', '')
        if mentor_word == '':
            style.mentor_word = 'mentor'
        else:
            style.mentor_word = mentor_word
        if mentee_word == '':
            style.mentee_word = 'mentee'
        else:
            style.mentee_word = mentee_word
        if mentorship_word == '':
            style.mentorship_word = 'mentorship'
        else:
            style.mentorship_word = mentorship_word
        if venture_word == '':
            style.venture_word = 'venture'
        else:
            style.venture_word = venture_word
        if project_word == '':
            style.project_word = 'project'
        else:
            style.project_word = project_word
        if job_word == '':
            style.job_word = 'job'
        else:
            style.job_word = job_word
        if application_word == '':
            style.application_word = 'application'
        else:
            style.application_word = application_word
        if page_word == '':
            style.page_word = 'page'
        else:
            style.page_word = page_word
        if deal_word == '':
            style.deal_word = 'deal'
        else:
            style.deal_word = deal_word
        if team_word == '':
            style.team_word = 'build team'
        else:
            style.team_word = team_word
        style.save()
        request.university_style = style
        context.update(platform_related(request))
    context.update({
        'page_name': 'customize-text',
        'title': 'Custom Text',
        'university': university,
        'style': style,
        'uni_enable_dashboard': uni_enable_dashboard,
        'submitted': submitted,
        'errors': errors,
    })
    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_text.html',
    )


@only_university
@login_required
def customize_integration(request, university):
    if AccessLevel.CUSTOM_TEXT not in request.access_levels and not request.is_super:
        raise Http404()

    submitted = False
    member = StartupMember.objects.get_user(request.user)
    if UniversityMailChimpAPI.objects.filter(university=university).exists():
        mail_chimp_api = UniversityMailChimpAPI.objects.get(university=university)
    else:
        mail_chimp_api = UniversityMailChimpAPI.objects.create_apikey(
            university,
            '',
            '',
            member,
            False
        )

    errors = {}
    context = {}
    if request.method == "POST":
        submitted = True

        mail_chimp = request.POST.get('mail_chimp', '')
        mail_chimp_server = request.POST.get('mail_chimp_server', '')
        if mail_chimp == '':
            mail_chimp_api.api_key = ''
        else:
            mail_chimp_api.api_key = mail_chimp
            key_worked = False
            try:
                mailchimp = MailchimpMarketing.Client()
                mailchimp.set_config({
                    "api_key": mail_chimp,
                    "server":mail_chimp_server
                })
                response = mailchimp.ping.get()
                key_worked = True
            except:
                key_worked = False

            UniversityMailChimpAPI.objects.update_apikey(
                university,
                mail_chimp,
                mail_chimp_server,
                member,
                key_worked
            )
        context.update(platform_related(request))

    mail_chimp_api = UniversityMailChimpAPI.objects.get(university=university)
    context.update({
        'page_name': 'customize-intergration',
        'title': 'Custom Intergration',
        'university': university,
        'mail_chimp':mail_chimp_api.api_key,
        'mail_chimp_server':mail_chimp_api.server_code,
        'keys_status': mail_chimp_api.api_worked,
        'submitted': submitted,
        'errors': errors,
    })
    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_integration.html',
        context,
    )


@only_university
@login_required
def customize_signup(request, university):
    if AccessLevel.USER_SIGNUP not in request.access_levels and not request.is_super:
        raise Http404()

    submitted = False
    if university.style is None:
        style = UniversityStyle.objects.create(_university=university)
        university.style = style
        university.save()
    else:
        style = UniversityStyle.objects.get(_university=university)

    errors = {}
    if request.method == "POST":
        submitted = True

        style.signup_step1 = True if 'step1_checkbox' in request.POST else False
        style.signup_step2 = True if 'step2_checkbox' in request.POST else False
        style.signup_tags = True if 'tags_checkbox' in request.POST else False
        style.signup_step3 = True if 'step3_checkbox' in request.POST else False
        style.signup_skills = True if 'step4_checkbox' in request.POST else False
        style.signup_biography = True if 'step5_checkbox' in request.POST else False
        
        # style.signup_step4 = True if 'step4_checkbox' in request.POST else False
        # style.signup_step5 = True if 'step5_checkbox' in request.POST else False

        style.require_step1 = True if 'step1_req_checkbox' in request.POST else False
        style.require_step2 = True if 'step2_req_checkbox' in request.POST else False
        style.require_tags = True if 'tags_req_checkbox' in request.POST else False
        style.require_step3 = True if 'step3_req_checkbox' in request.POST else False
        style.require_skills = True if 'step4_req_checkbox' in request.POST else False
        style.require_biography = True if 'step5_req_checkbox' in request.POST else False
        
        # style.require_step4 = True if 'step4_req_checkbox' in request.POST else False
        # style.require_step5 = True if 'step5_req_checkbox' in request.POST else False

        style.enable_default_skills = True if 'default_skills_checkbox' in request.POST else False

        style.save()

    return TemplateResponse(
        request,
        'trunk/platform/customize/customize_signup.html',
        {
            'page_name': 'customize-signup',
            'title': 'Signup',
            'university': university,
            'style': style,
            'submitted': submitted,
            'errors': errors,
        }
    )


@only_university
@login_required
def manage_api(request, university):
    TOKEN_CREATE = 'create'
    TOKEN_REFRESH = 'refresh'
    TOKEN_DELETE = 'delete'

    BROWSER_TOKEN_CREATE = 'browser-create'
    BROWSER_TOKEN_REFRESH = 'browser-refresh'
    BROWSER_TOKEN_DELETE = 'browser-delete'
    BROWSER_TOKEN_REFERRER = 'browser-referrer'
    BROWSER_TOKEN_REFERRER_KEYS = 'browser-referrer-keys'
    POST_NAME = 'operation'
    token = None
    browser_token = None
    errors = []
    referrers = BrowserAPIReferrer.objects.none()
    if APIToken.objects.filter(university_id=university.id, is_invalid__isnull=True).exists():
        try:
            token = APIToken.objects.get(university_id=university.id, is_invalid__isnull=True,
                                         browserapitoken__isnull=True)
        except APIToken.DoesNotExist:
            token = None
        try:
            browser_token = BrowserAPIToken.objects.get(university_id=university.id, is_invalid__isnull=True)
            referrers = BrowserAPIReferrer.objects.filter(university_id=university.id)
        except BrowserAPIToken.DoesNotExist:
            browser_token = None
    if request.method == "POST":
        operation = request.POST.get(POST_NAME)
        if operation == TOKEN_CREATE:
            token = APIToken.objects.issue_new(university, request.user)
        elif operation == TOKEN_REFRESH:
            token.is_invalid = timezone.now()
            token.save()
            token = APIToken.objects.issue_new(university, request.user)
        elif operation == TOKEN_DELETE:
            token.is_invalid = timezone.now()
            token.save()
            token = None
        elif operation == BROWSER_TOKEN_CREATE:
            browser_token = BrowserAPIToken.objects.issue_new(university, request.user)
        elif operation == BROWSER_TOKEN_REFRESH:
            browser_token.is_invalid = timezone.now()
            browser_token.save()
            browser_token = BrowserAPIToken.objects.issue_new(university, request.user)
        elif operation == BROWSER_TOKEN_DELETE:
            browser_token.is_invalid = timezone.now()
            browser_token.save()
            browser_token = None
        elif operation == BROWSER_TOKEN_REFERRER:
            inputs = request.POST.getlist(BROWSER_TOKEN_REFERRER_KEYS)
            new_referrers = []
            for input in inputs:
                if input:
                    form = URLForm({'url': input})
                    if form.is_valid():
                        new_referrers.append(BrowserAPIReferrer(university=university, referrer=input))
                    else:
                        errors.append(
                            {'msg': 'Please provide valid full url. ex) https://www.example.com/', 'value': input})
            referrers.delete()
            BrowserAPIReferrer.objects.bulk_create(new_referrers)
            referrers = BrowserAPIReferrer.objects.filter(university_id=university.id)
        else:
            return HttpResponseBadRequest()
        if not errors:
            return redirect(reverse('uni-manage-api'))

    return TemplateResponse(
        request,
        'trunk/platform/manage_api.html',
        {
            'page_name': 'manage-api',
            'university': university,
            'token': token,
            'TOKEN_CREATE': TOKEN_CREATE,
            'TOKEN_REFRESH': TOKEN_REFRESH,
            'TOKEN_DELETE': TOKEN_DELETE,
            'BROWSER_TOKEN_CREATE': BROWSER_TOKEN_CREATE,
            'BROWSER_TOKEN_REFRESH': BROWSER_TOKEN_REFRESH,
            'BROWSER_TOKEN_DELETE': BROWSER_TOKEN_DELETE,
            'BROWSER_TOKEN_REFERRER': BROWSER_TOKEN_REFERRER,
            'BROWSER_TOKEN_REFERRER_KEYS': BROWSER_TOKEN_REFERRER_KEYS,
            'POST_NAME': POST_NAME,
            'browser_token': browser_token,
            'referrers': referrers,
            'errors': errors
        }
    )


class ModifySubscription(UniversityDashboardView):
    """
    Modify subscription of a platform. Only StartupTree staff can access and edit the page.
    """

    def get(self, request, university):
        # only StartupTree staff is allowed.
        if not StartupTreeStaff.objects.filter(staff_id=request.user.id).exists():
            raise Http404()
        form = UniversitySubscriptionForm(
            instance=UniversitySubscription.objects.get(university_id=university.id))
        group_form = UniversityGroupForm()

        tags = Label.objects.filter(university_id=university.id, field_type='groups').order_by('id')

        context = {
            'subscription_form': form,
            'group_form': group_form,
            'tags': tags,
            'university': university
        }

        return TemplateResponse(request, "trunk/platform/manage_subscription.html", context)

    def post(self, request, university):
        # only StartupTree staff is allowed.
        if not StartupTreeStaff.objects.filter(staff_id=request.user.id).exists():
            raise Http404()

        form_type = request.POST.get('form_type')

        if form_type == 'subscription':
            form = UniversitySubscriptionForm(
                request.POST, instance=UniversitySubscription.objects.get(university_id=university.id))
            if form.is_valid():
                form.save()

        elif form_type == 'group':
            group_form = UniversityGroupForm(request.POST)
            if group_form:
                group_form.save()

        elif form_type == 'startuptreestaff-assignable-only-tags':
            new_staff_tags = request.POST.getlist('startuptreestaff-tags')

            current_staff_tags = Label.objects.filter(
                university_id=university.id, startuptreestaff_assignable_only=True)

            new_set = set(map(int, new_staff_tags))
            current_set = set(tag.id for tag in current_staff_tags)

            enable_set = new_set - current_set
            disable_set = current_set - new_set

            Label.objects.filter(id__in=enable_set).update(startuptreestaff_assignable_only=True, users_can_tag=False)
            Label.objects.filter(id__in=disable_set).update(startuptreestaff_assignable_only=False)

        return redirect(reverse('uni-subscription'))