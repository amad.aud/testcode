import datetime
import json
import re
import urllib
import uuid
from io import BytesIO
import xlsxwriter
from xhtml2pdf import pisa
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.db import transaction
from django.http import HttpRequest
from django.http.response import (
    JsonResponse, HttpResponse, HttpResponseRedirect, Http404
)
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.utils import timezone
from StartupTree import settings
from StartupTree.loggers import app_logger
from branch.models import (
    StartupMember,
    Label
)
from branch.utils.tags import get_manageable_tags_titles
from emailuser.models import UserEmail
from forms.models import Form, Question
from forms.parser import QuestionsParser, CustomAnswersParser
from forms.serializer.forms import custom_report_dict
from forms.serializer.questions import QuestionSerializer
from trunk.models import (
    DataReport,
    University,
    UniversityStaff,
    AccessLevel,
    StartupTreeStaff
)
from trunk.views.abstract import UniversityDashboardView


class ManageReports(UniversityDashboardView):
    def get(self, request, university, r_id=None, is_create=0):
        try:
            admin = UniversityStaff.objects.get(university=university, admin=request.user)
        except UniversityStaff.DoesNotExist:
            try:
                admin = StartupTreeStaff.objects.get(staff=request.user)
            except StartupTreeStaff.DoesNotExist:
                raise PermissionDenied
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()

        archive_filter = request.GET.get('archived', 'false')
        tag_filter = request.GET.get('tag', 'all')
        semester_filter = request.GET.get('sem', '0')
        year_filter = request.GET.get('year', 'all')

        uni_labels = get_manageable_tags_titles(request)

        reports = DataReport.objects.filter(university=university, is_template=False) \
            .order_by('-created')
        has_templates = DataReport.objects.filter(university=university, is_template=True).exists()

        years = []
        for report in reports:
            if report.year not in years:
                years.append(report.year)

        if archive_filter == "false":
            reports = reports.filter(is_archived=False)
        elif archive_filter == "true":
            reports = reports.filter(is_archived=True)
        if tag_filter != "all":
            reports = reports.filter(tags__title=tag_filter)
        if semester_filter != "0":
            reports = reports.filter(semester=semester_filter)
        if year_filter != "all":
            year_filter = int(year_filter)
            reports = reports.filter(year=year_filter)

        template = 'trunk/metrics/reports/list.html'

        page_name = 'manage-reports'

        return TemplateResponse(
            request, template,
            {
                'title': 'Reports',
                'page_name': page_name,
                'university': university,
                'reports': reports,
                'admin': admin,
                'has_templates': has_templates,
                'archived_filter': archive_filter,
                'tag_filter': tag_filter,
                'sem': semester_filter,
                'year_filter': year_filter,
                'tags': uni_labels,
                'years': years
            }
        )


class ReportDashboard(UniversityDashboardView):
    def get(self, request, university, r_id=None):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()
        page_name = 'report-dashboard'
        report = None
        if r_id:
            report = DataReport.objects.get(r_id=r_id)
            current_admin = None
            all_admins = []
            for admin in UniversityStaff.objects.filter(university=university):
                if admin.is_super or AccessLevel.REPORTING in admin.access_levels.all().values_list('name', flat=True):
                    all_admins.append(admin)
                    if admin.admin_id == request.user.id:
                        admin = admin
            total_admins = len(all_admins)
            admins_entered = report.admins_entered.count()
            days_left = 0
            if report.deadline:
                days_left = (report.deadline - datetime.datetime.now().date()).days
            if report.deadline is None or days_left < 0:
                days_left = '--'

            return TemplateResponse(
                request,
                'trunk/metrics/reports/dashboard.html',
                {
                    'title': 'Report Dashboard',
                    'page_name': page_name,
                    'university': university,
                    'report': report,
                    'admin': current_admin,
                    'total_admins': total_admins,
                    'admins_entered': admins_entered,
                    'days_left': days_left
                })
        else:
            messages.add_message(request,
                                 messages.Error,
                                 'Report does not exist')
        return HttpResponseRedirect(reverse('uni-manage-reports'))


class CreateReport(UniversityDashboardView):
    def post(self, request, university):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()

        return self.create(request, university)

    def get(self, request, university):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()

        template = "trunk/metrics/reports/create_edit.html"
        report = None
        is_create_template = request.GET.get('as_template', None)
        is_create_template = True if is_create_template == "true" else False
        uni_labels = get_manageable_tags_titles(request)
        return TemplateResponse(
            request, template,
            {
                'title': 'Reports',
                'university': university,
                'report': report,
                'r_id': None,
                'uni_labels': uni_labels,
                'is_report': True,
                'is_edit': False,
                'is_create_template': is_create_template,
                'can_edit_questions': True,
                'can_reorder_questions': False
            }
        )

    def create(self, request, university):
        report_name = request.POST.get('report-title')
        template_name = request.POST.get('template-title', report_name)
        semester = request.POST.get('report-semester')
        year = request.POST.get('report-year')
        deadline = request.POST.get('report-deadline', None)
        is_template = request.POST.get('is-template')
        is_report = request.POST.get('is-report')
        redirect_to = request.POST.get('redirect-to')

        if deadline is not None and deadline != '':
            month, day, year = deadline.split('/')
            deadline_str = '{0}-{1}-{2}'.format(year, month, day)
            formatted_deadline = datetime.datetime.strptime(deadline_str, '%Y-%m-%d')
        else:
            formatted_deadline = None

        if is_template == 'true':
            is_template = True
            name = template_name
        else:
            is_template = False
            name = report_name
        if is_report == 'true':
            is_report = True
        else:
            is_report = False

        kwargs = {
            'name': name,
            'university': university,
            'semester': int(semester),
            'year': year,
            'deadline': formatted_deadline,
            'is_template': is_template
        }

        new_report = DataReport.objects.create_report(**kwargs)
        process_report(request, new_report, False)

        if is_template and is_report:
            if new_report.is_template:
                kwargs['is_template'] = False
                kwargs['name'] = report_name
            else:
                kwargs['is_template'] = True
                kwargs['name'] = template_name
            new_report_1 = DataReport.objects.create_report(**kwargs)
            process_report(request, new_report_1, False)

        if redirect_to == 'templates':
            return HttpResponseRedirect(reverse('uni-manage-templates'))
        elif redirect_to == 'dashboard':
            return HttpResponseRedirect(reverse('uni-manage-report',
                                                kwargs={'r_id': new_report.r_id}))
        else:
            return HttpResponseRedirect(reverse('uni-edit-report',
                                                kwargs={'r_id': new_report.r_id}))


class EditReport(UniversityDashboardView):
    template = "trunk/metrics/reports/create_edit.html"

    def post(self, request, university, r_id):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()

        report = DataReport.objects.get(r_id=r_id)

        report_name = request.POST.get('report-title')
        template_name = request.POST.get('template-title', report_name)
        semester = request.POST.get('report-semester')
        year = request.POST.get('report-year')
        deadline = request.POST.get('report-deadline', None)
        is_template = request.POST.get('is-template')
        is_report = request.POST.get('is-report')
        from_template = request.POST.get('from-template', None)
        redirect_to = request.POST.get('redirect-to')

        if deadline is not None and deadline != '':
            month, day, year = deadline.split('/')
            deadline_str = '{0}-{1}-{2}'.format(year, month, day)
            formatted_deadline = datetime.datetime.strptime(deadline_str, '%Y-%m-%d')
        else:
            formatted_deadline = None

        if is_template == 'true':
            is_template = True
        else:
            is_template = False
        if is_report == 'true':
            is_report = True
        else:
            is_report = False
        if from_template is not None:
            template_to_edit = DataReport.objects.get(r_id=from_template)
        else:
            template_to_edit = None

        if is_template and template_to_edit is not None:
            # a new report has already been created, so need to delete it
            report.delete()
            report = template_to_edit

        report.name = report_name
        report.university = university
        report.semester = int(semester)
        report.year = year
        report.deadline = formatted_deadline
        report.updated = timezone.now()
        report.save()
        process_report(request, report, True)

        if is_template and is_report:
            if report.is_template:
                is_template = False
                name = report_name
            else:
                is_template = True
                name = template_name
            kwargs = {
                'name': name,
                'university': university,
                'semester': int(semester),
                'year': year,
                'deadline': formatted_deadline,
                'is_template': is_template
            }
            # transaction.atomic prevents TransactionManagementError when creating report
            with transaction.atomic():
                new_report = DataReport.objects.create(**kwargs)
            process_report(request, new_report, False)

        custom_questions = report.form.questions.all()
        can_reorder_questions = custom_questions.count() > 1

        report_tags = report.tags.all().values_list('title', flat=True)
        uni_labels = get_manageable_tags_titles(request)

        if redirect_to == 'templates':
            return HttpResponseRedirect(reverse('uni-manage-templates'))
        elif redirect_to == 'dashboard':
            return HttpResponseRedirect(reverse('uni-manage-report',
                                                kwargs={'r_id': report.r_id}))

        return TemplateResponse(
            request, self.template,
            {
                'title': 'Reports',
                'university': university,
                'is_report': True,
                'is_edit': True,
                'report': report,
                'r_id': r_id,
                'can_edit_questions': True,
                'uni_labels': uni_labels,
                'report_tags': report_tags,
                'custom_questions': QuestionSerializer(custom_questions, many=True),
                'can_reorder_questions': can_reorder_questions
            }
        )

    def get(self, request, university, r_id):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()

        report = DataReport.objects.get(r_id=r_id)
        if report is None:
            raise Http404('Report does not exist')

        from_template = request.GET.get("from_template", None)

        if report.deadline:
            deadline = report.deadline.strftime('%m/%d/%Y')
        else:
            deadline = None
        uni_labels = get_manageable_tags_titles(request)
        report_tags = report.tags.all().values_list('title', flat=True)

        if report.form and report.form.questions:
            custom_questions = report.form.questions.all()
            custom_questions = QuestionSerializer(custom_questions, many=True)
            form_length = report.form.questions.count()
        else:
            custom_questions = None
            form_length = 0

        can_reorder_questions = form_length > 1

        return TemplateResponse(
            request, self.template,
            {
                'title': 'Reports',
                'university': university,
                'is_report': True,
                'is_edit': True,
                'from_template': from_template,
                'report': report,
                'r_id': r_id,
                'can_edit_questions': True,
                'deadline': deadline,
                'uni_labels': uni_labels,
                'report_tags': report_tags,
                'custom_questions': custom_questions,
                'can_reorder_questions': can_reorder_questions
            }
        )


def process_report(request, report, is_edit):
    university = request.university
    tags = request.POST.getlist('tags[]')
    uni_labels = get_manageable_tags_titles(request)
    if is_edit:
        if uni_labels and not report.is_template:
            report.tags.clear()
            for tag in Label.objects.filter(university=university, field_type="groups"):
                if tag.title in tags:
                    tag = str(tag).lower().strip()
                    try:
                        t = Label.objects.get_label(tag, university, "groups")
                    except Label.DoesNotExist:
                        continue
                    report.tags.add(t)

        if report.form is None:
            form = Form.objects.create(form=Form.REPORT, university=uni)
            report.form = form
            report.save()
        else:
            form = report.form
        parser = QuestionsParser(request.POST, form, university)
        parser.parse()
        report.refresh_from_db()
        form.refresh_from_db()
    else:
        if not report.is_template:
            if uni_labels:
                for tag in Label.objects.filter(university=university, field_type="groups"):
                    if tag.title in tags:
                        tag = str(tag).lower().strip()
                        try:
                            t = Label.objects.get_label(tag, university, "groups")
                        except Label.DoesNotExist:
                            continue
                        report.tags.add(t)
            else:
                for tag in request.current_member.labels.filter(university=university, field_type="groups"):
                    report.tags.add(t)

        # questions
        form = Form.objects.create(form=Form.REPORT, university=university)
        report.form = form
        report.save()
        parser = QuestionsParser(request.POST, form, university)
        parser.parse()
        report.refresh_from_db()
        form.refresh_from_db()


@login_required
def reorder_report_questions(request, r_id):
    """Endpoint to retrieve and post order of questions"""
    try:
        report = DataReport.objects.get(r_id=r_id)
    except:
        raise Http404
    questions = report.form.questions.order_by('number', 'id')
    custom = []
    if request.method == "POST":
        try:
            count = int(request.POST.get('count'))
            ranks = json.loads(request.POST.get('order'))
            for rank, q_id in ranks.items():
                try:
                    rank = int(rank)
                    q_id = int(q_id)
                    q = questions.get(id=q_id)
                    q.number = rank
                    q.save()
                except Exception as e:
                    app_logger.exception(e)
            return JsonResponse({'statusCode': '404'})
        except Exception as e:
            app_logger.exception(e)
            return JsonResponse({'statusCode': '500'})
    else:
        count = questions.count()
        tickets = list(range(1, count+1))
        for q in questions:
            entry = {'text': q.text, 'q_id': q.id}
            if q.number in tickets:
                entry['number'] = q.number
                tickets.remove(q.number)
            else:
                q.number = tickets.pop(0)
                entry['number'] = q.number
            q.save()
            custom.append(entry)
        return JsonResponse(dict(order=custom))


class ReportQuestionReorder(UniversityDashboardView):
    def get(self, request, r_id, university):
        try:
            report = DataReport.objects.get(r_id=r_id, university=university)
        except:
            raise Http404
        return reorder_report_questions(request, r_id)

    def post(self, request, r_id, university):
        try:
            report = DataReport.objects.get(r_id=r_id, university=university)
        except:
            raise Http404
        return reorder_report_questions(request, r_id)


class GenerateReportFromTemplate(UniversityDashboardView):
    def get(self, request, r_id, university):
        try:
            report = DataReport.objects.select_related('form').get(
                r_id=r_id,
                university=university
            )
        except:
            raise Http404('No such report exists.')

        this_year = datetime.datetime.now().year
        new_report = DataReport.objects.create_report(
            name=report.name,
            university=report.university,
            semester=DataReport.ALL,
            year=this_year)

        for tag in report.tags.all():
            new_report.tags.add(tag)

        form = Form.objects.create(
            form=Form.REPORT,
            university=university
        )
        new_report.form = form
        new_report.save()
        if report.form and report.form.questions:
            for q in report.form.questions.all().order_by('number', 'id'):
                options = q.choices.all()
                q.pk = None
                q.id = None
                q.save()
                if q.q_type == 1 or q.q_type == 2:
                    if q.choices:
                        for c in options:
                            q.choices.remove(c)
                            c.pk = None
                            c.id = None
                            c.save()
                            q.add_choice(c)
                q.save()
                form.questions.add(q)
                form.save()
        new_report.form.save()
        new_report.save()

        return redirect(reverse('uni-edit-report',
            kwargs={"r_id": new_report.r_id}) + "?from_template=" + r_id)


class EnterData(UniversityDashboardView):
    def get(self, request, r_id, university):
        try:
            if r_id is None or str(r_id) == 'None':
                raise DataReport.DoesNotExist('Invalid report id was provided')
            try:
                admin = UniversityStaff.objects.get(university=university, admin=request.user)
            except UniversityStaff.DoesNotExist:
                try:
                    admin = StartupTreeStaff.objects.get(staff=request.user)
                except StartupTreeStaff.DoesNotExist:
                    raise PermissionDenied
            if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
                raise Http404()

            report = DataReport.objects.get(r_id=r_id)
            if report.form is None:
                report.form = Form.objects.create(
                    form=Form.REPORT, university=university)
                report.save()

            custom_form = custom_report_dict(admin, university.id, Form.REPORT, report.r_id)

            form_length = len(custom_form)
            return TemplateResponse(request, 'trunk/metrics/reports/enter_data.html',
                                    {
                                        'report': report,
                                        'page_name': 'report',
                                        'university': university,
                                        'title': 'Enter Data',
                                        'custom': custom_form,
                                        'form_length': form_length,
                                        'admin': admin
                                    })

        except DataReport.DoesNotExist:
            raise Http404('Page not found')

    def post(self, request, r_id, university):
        report = DataReport.objects.get(r_id=r_id)

        try:
            admin = UniversityStaff.objects.get(university=university, admin=request.user)
        except UniversityStaff.DoesNotExist:
            try:
                admin = StartupTreeStaff.objects.get(staff=request.user)
            except StartupTreeStaff.DoesNotExist:
                raise PermissionDenied
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()

        admin_user = StartupMember.objects.get_user(admin.admin)

        custom_answer_parser = CustomAnswersParser(request.POST, university, admin_user,
                                                   None, r_id)
        try:
            with transaction.atomic():
                custom_answer_parser.parse_answers()
            errors = custom_answer_parser.errors
            if errors != {}:
                raise ValueError()
            if not custom_answer_parser.is_draft:
                if admin not in report.admins_entered.all():
                    report.admins_entered.add(admin)
            else:
                if admin not in report.admins_in_progress.all():
                    report.admins_in_progress.add(admin)
        except ValueError:
            errors = custom_answer_parser.get_errors()
            error_list = []
            # make validation errors human readable
            for error in errors:
                error_list.append("Question #{0}: {1}".format(
                    errors[error]['value'] if errors[error]['value'] else errors[error]['q_text'], errors[error]['msg']))

            return TemplateResponse(request, 'trunk/metrics/reports/enter_data.html',
                                    {
                                        'report': report,
                                        'page_name': 'report',
                                        'title': 'Enter Data',
                                        'university': university,
                                        'custom': custom_form,
                                        'form_length': form_length,
                                        'admin': admin,
                                        'errors': errors,
                                        'error_list': error_list
                                    })

        if custom_answer_parser.is_draft:
            messages.add_message(request, messages.SUCCESS, "Success! Your draft has been saved.",
                                 "text--large text--bold")
        else:
            messages.add_message(request, messages.SUCCESS, "Success! Your data has been submitted.",
                                 "text--large text--bold")
        return HttpResponseRedirect(reverse('uni-manage-report', kwargs={'r_id': r_id}))


class ExportData(UniversityDashboardView):
    def admin_data(self, report, r_id, uni_id):
        admins = report.admins_entered.all()
        admin_list = []
        for admin in admins:
            admin_name = admin.admin.startup_member.get_full_name()
            custom = custom_report_dict(admin, uni_id, Form.REPORT, r_id)
            admin_list.append({'name': admin_name,
                               'custom': custom})
        return admin_list

    def get(self, request, university, r_id):
        try:
            report = DataReport.objects.get(r_id=r_id)
        except DataReport.DoesNotExist:
            raise Http404("Report does not exist.")

        admin_list = self.admin_data(report, r_id, university)

        if len(admin_list) == 0:
            messages.add_message(request, messages.ERROR, 'You cannot export a report with no data.')
            return HttpResponseRedirect(reverse('uni-manage-reports'))
        else:
            filename = "data_export_" + str(report.name) + datetime.date.today().strftime("%m.%d.%y") + ".xlsx"
            attachment = BytesIO()
            workbook = xlsxwriter.Workbook(attachment)
            data_sheet = workbook.add_worksheet("Exported")
            wrap_format = workbook.add_format()
            wrap_format.set_text_wrap()

            header_cell_format = workbook.add_format({
                'bold': True,
                'bg_color': '#A6EDC3',
                'text_wrap': True
            })

            # Get column heads
            row_head = []
            start = admin_list[0]
            for q in start['custom']:
                row_head.append("{0}".format(q['q_text']))
            for offset, entry in enumerate(row_head):
                data_sheet.write(offset, 0, entry, header_cell_format)

            # Now fill spreadsheet
            col_cnt = 1
            for admin in admin_list:
                data = []
                for q in admin['custom']:
                    if not q['a_id']:
                        data.append("None")
                    elif q['q_type'] == 'Paragraph':
                        data.append("{0}".format(q['a_text']))
                    elif q['q_type'] == 'Dropdown' or q['q_type'] == 'Multiple Choice':
                        selected_choices = []
                        for choice in q['q_choices']:
                            if choice['chosen']:
                                selected_choices.append(choice['text'])
                        choices = ", ".join(selected_choices)
                        if len(selected_choices) == 0:
                            choices = "None"
                        data.append(choices)
                    elif q['q_type'] == 'Date Time':
                        data.append("{0}".format(q['a_date']))
                for offset, entry in enumerate(data):
                    data_sheet.write(offset, col_cnt, entry, wrap_format)
                col_cnt += 1
            workbook.close()
            attachment.seek(0)
            response = HttpResponse(
                attachment.read(),
                content_type="application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
            attachment.close()
            return response


class ArchiveReport(UniversityDashboardView):
    def post(self, request, university):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()
        try:
            r_id = request.POST.get('rid')
            report = DataReport.objects.get(r_id=r_id, university=university)
        except DataReport.DoesNotExist:
            raise Http404('Report does not exist')

        if request.POST.get('action') == 'archive' and not report.is_archived:
            report.is_archived = True
            report.save()
        elif request.POST.get('action') == 'unarchive' and report.is_archived:
            report.is_archived = False
            report.save()

        return HttpResponseRedirect(reverse('uni-manage-reports'))


class DeleteReport(UniversityDashboardView):
    def post(self, request, university):
        if AccessLevel.REPORTING not in request.access_levels and not request.is_super:
            raise Http404()
        return self.delete(request)

    def delete(self, request):
        try:
            r_id = request.POST.get('rid')
            DataReport.objects.get(r_id=r_id).delete()
        except DatReport.DoesNotExist:
            return HttpResponseRedirect(reverse('uni-manage-reports'))
        messages.add_message(request, messages.SUCCESS, 'Report successfully deleted!')
        return HttpResponseRedirect(reverse('uni-manage-reports'))
