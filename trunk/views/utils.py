from trunk.models import Event
from branch.modelforms import TinyMCEWidget


def prepare_event(event_id):
    """
    IMPORTANT: Include
        {{ event_description.media }}
    In the {% block styles %} section in any django template
    where you want the TinyMCE editor to render.
    """
    try:
        event = Event.objects.get(event_id=event_id)
    except Event.DoesNotExist:
        return None, None
    is_ready_made = event.university.short_name == 'readymadetemplates'
    event_description = TinyMCEWidget({'content': event.description})
    event_start_date = event.get_edit_start_date()
    event_end_date = event.get_edit_end_date()
    event_start_time = event.get_edit_start_time()
    event_end_time = event.get_edit_end_time()
    event_timezone = event.get_timezone()
    allow_checkins = event.get_allow_checkins()
    allow_rsvp = event.get_allow_rsvp()
    add_my_link = event.add_my_link
    allow_max_rsvp = event.get_allow_max_rsvp()
    allow_guest_rsvp = event.get_allow_guest_rsvp()
    restrict_guest_rsvp = event.get_restrict_guest_rsvp()
    show_custom_rsvp = event.get_show_custom_rsvp()
    show_custom_checkin = event.get_show_custom_checkin()
    allow_guest_checkins = event.get_allow_guest_checkins()
    restrict_guest_checkins = event.get_restrict_guest_checkins()
    display_attendees = event.get_display_attendees()
    display_view_count = event.get_display_view_count()
    display_applicants = event.get_display_applicants()
    # try to use get method if possible
    # free_food = event.get_free_food()
    # free_swag = event.get_free_swag()
    free_food = event.free_food
    free_swag = event.free_swag
    allow_group_sharing = event.allow_group_sharing
    auto_generate_link = event.auto_generate_link
    allow_global_sharing = event.allow_global_sharing
    is_published = event.get_is_published()
    is_private = event.get_is_private()
    is_saved = event.get_is_saved()
    can_edit_questions = event.get_can_edit_questions()
    can_edit_rubric = event.get_can_edit_rubric()
    event_type = event.get_event_type()
    event_tags = event.tags.all().values_list('title', flat=True)
    event_website = event.get_event_website()
    ticket_website = event.get_ticket_website()
    chk_google_meet_link = event.chk_google_meet_link
    chk_own_link = event.chk_own_link
    input_own_link = event.input_own_link

    event_dict = {
        'name': event.name,
        'event_id': event.event_id,
        'url': event.get_url(),
        'is_ready_made': is_ready_made,
        'start_date': event_start_date,
        'start_time': event_start_time,
        'end_time': event_end_time,
        'end_date': event_end_date,
        'timezone': event_timezone,
        'location': event.location,
        'add_my_link':add_my_link,
        'map_location': event.map_location,
        'image': event.image,
        'auto_conference_link': event.auto_conference_link,
        'is_competition': event.is_competition,
        'is_application': event.is_application,
        'is_public_competition': event.is_public_competition,
        'display_activity_feed': event.display_activity_feed,
        'pin_to_feed': event.pin_to_feed,
        'display_attendees': display_attendees,
        'auto_generate_link':auto_generate_link,
        'display_view_count': display_view_count,
        'display_applicants': display_applicants,
        'allow_checkins': allow_checkins,
        'allow_rsvp': allow_rsvp,
        'allow_max_rsvp': allow_max_rsvp,
        'max_rsvp': event.max_rsvp,
        'enable_waitlist': event.enable_waitlist,
        'allow_guest_rsvp': allow_guest_rsvp,
        'restrict_guest_rsvp': restrict_guest_rsvp,
        'show_custom_rsvp': show_custom_rsvp,
        'show_custom_checkin': show_custom_checkin,
        'allow_guest_checkins': allow_guest_checkins,
        'restrict_guest_checkins': restrict_guest_checkins,
        'can_edit_questions': can_edit_questions,
        'can_edit_rubric': can_edit_rubric,
        'free_food': free_food,
        'free_swag': free_swag,
        'allow_group_sharing': allow_group_sharing,
        'allow_global_sharing': allow_global_sharing,
        'is_published': is_published,
        'is_private': is_private,
        'is_saved': is_saved,
        'is_template_only': event.is_template_only,
        'send_emails_days_prior': event.send_emails_days_prior,
        'send_updates': event.send_updates,
        'latitude': event.latitude,
        'longitude': event.longitude,
        'event_type': event_type,
        'event_tags': event_tags,
        'event_website': event_website,
        'ticket_website': ticket_website,
        'chk_google_meet_link' : chk_google_meet_link,
        'chk_own_link' : chk_own_link,
        'input_own_link' : input_own_link,
        }
    return event_dict, event_description
