import datetime
import pytz
from io import BytesIO
import xlsxwriter
from xhtml2pdf import pisa
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.db import transaction
from django.http.response import (
    JsonResponse, HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden, HttpResponseBadRequest
)
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.utils import timezone
from django.template.loader import get_template
from branch.templatetags import static, media
import os
from django.conf import settings
from StartupTree.loggers import app_logger
from StartupTree.utils import get_date_time_object, filtered_filename
from activity.models import Activity
from branch.modelforms import (
    TextEditorForm2,
    EmailEditorForm,
    EmailEditorForm2,
    PasscodeForm,
    InviteJudgeFormset
)
from branch.models import (
    StartupMember,
    Label
)
from branch.serializer.member import sm_to_dict
from branch.tasks import AssignJudge, send_passcode_changes, send_judges_link_and_pass
from branch.utils.tags import get_manageable_tags_titles
from branch.views.competitions import (
    view_applicants,
    view_judges,
    admin_judge_review,
    get_application,
    admin_reorder_questions,
    admin_reorder_rubric
)
from emailuser.models import UserEmail
from forms.models import (
    Form,
    MemberApplicant,
    AnonymousApplicant,
    Answer,
    Question,
    Choice
)
from forms.serializer.forms import custom_member_dict
from forms.views import manage_comp_forms
from trunk.models import (
    Event,
    JudgeBoard,
    JudgeReview,
    Judge,
    University,
    UniversityExportHistory,
    is_comp_app_admin
)
from trunk.modelforms import ImportForm
import xlrd
from trunk.views.abstract import UniversityDashboardView, View, \
    CompetitionOnlyDashboardView
from trunk.views.events import CreateEvent, EditEvent, DeleteEvent, \
    sorted_events_admin_panel, get_all_rsvps
from trunk.views.utils import prepare_event
from branch.modelforms import TinyMCEWidget
import requests
import zipfile
import uuid
from shutil import copyfile


class ManageCompetitionsView(CompetitionOnlyDashboardView):

    def get(self, request, university, event_id=None):
        page_name = 'manage-competition-view'
        event = None
        is_edit_event = False
        can_edit_questions = False
        can_edit_rubric = False
        if event_id:
            event = Event.objects.get(event_id=event_id)
            if not event.is_competition:
                raise Http404
            if not is_comp_app_admin(request, event.is_application):
                return HttpResponseForbidden()
            eboard = event.event_board
            round_count = 0

            # Manage event board
            prev_round = None
            next_round = None
            if eboard:
                if eboard.root == event:
                    prev_round = None
                    try:
                        next_round = eboard.rounds.get(number=2).event
                    except:
                        next_round = None
                else:
                    try:
                        curr_round = eboard.rounds.get(event=event).number
                        try:
                            prev_round = \
                                eboard.rounds.get(number=curr_round - 1).event
                        except:
                            pass
                        try:
                            next_round = \
                                eboard.rounds.get(number=curr_round + 1).event
                        except:
                            pass
                    except Exception as e:
                        app_logger.exception(e)

            # Manage Judge board
            jboard = event.judge_boards.all().last()
            if jboard and jboard.deadline:
                judge_deadline = '{0} {1} (Round {2})'.format(
                    jboard.get_end_date(), jboard.get_end_time(), jboard.round)
                if jboard.timezone:
                    judging_timezone = jboard.timezone
                else:
                    judging_timezone = event.timezone
            else:
                judge_deadline = None
                judging_timezone = None

            # Judge Ratings
            all_ratings = JudgeReview.objects.filter(event=event, judge__is_archived=False).count()
            completed_ratings = JudgeReview.objects.filter(event=event,
                                                           status=3,
                                                           judge__is_archived=False).count()

            # Ensure that datetimes are AWARE as opposed to NAIVE for proper localtime
            timeuntil = event.calc_timeuntil()
            open = True
            if timeuntil < 0:
                timeuntil = '0'
                open = False
            else:
                timeuntil = str(timeuntil)
            can_edit_questions = event.get_can_edit_questions()
            can_edit_rubric = event.get_can_edit_rubric()
            if can_edit_questions is None:
                can_edit_questions = False
            else:
                can_edit_questions = can_edit_questions
            if can_edit_rubric is None:
                can_edit_rubric = False
            else:
                can_edit_rubric = can_edit_rubric
            majordict = {}
            roledict = {}
            graddict = {}
            skilldict = {}

            user_count = event.get_applicant_count()
            if user_count > 0:
                for member_applicnat in event.get_member_applicants().iterator():
                    startupmember = member_applicnat.member
                    data = sm_to_dict(startupmember,
                                      university.id,
                                      detailed_degree=True)
                    skills = data['skills']
                    roles = data['roles']
                    degrees = data['degrees']
                    for skill in skills:
                        if skill in skilldict:
                            skilldict[skill] += 1
                        else:
                            skilldict[skill] = 1
                    for role in roles:
                        if role in roledict:
                            roledict[role] += 1
                        else:
                            roledict[role] = 1
                    gradyr = None
                    major = None
                    for degree in degrees:
                        try:
                            if 'graduated_on' in degree and degree['graduated_on'] is not None:
                                year = degree['graduated_on']
                            if 'major' in degree and degree['major'] != '':
                                majr = degree['major']
                            elif degree['major'] == '':
                                majr = 'Undeclared'
                            elif degree['major'] is None:
                                pass
                            else:
                                pass
                            if gradyr is None:
                                if year != '':
                                    gradyr = year
                                    major = majr
                                else:
                                    major = majr
                            elif year == '':
                                major = majr
                            elif int(year) > gradyr:
                                gradyr = year
                                major = majr
                            else:
                                pass
                        except:
                            app_logger.exception("TypeError in one of [gradyr, major]")
                    if gradyr in graddict:
                        graddict[gradyr] += 1
                    else:
                        graddict[gradyr] = 1
                    if major in majordict:
                        majordict[major] += 1
                    else:
                        majordict[major] = 1

                # sorting majors
                majorkeys = list(majordict.keys())
                majorvals = list(majordict.values())
                mindex = list(range(len(majorvals)))
                mindex.sort(key=majorvals.__getitem__, reverse=True)
                majorlabels = list(map(majorkeys.__getitem__, mindex))
                majordata = list(map(majorvals.__getitem__, mindex))
                if len(majorlabels) > 10:
                    otherd = sum(majordata[9:])
                    majorlabels = majorlabels[:9]
                    majordata = majordata[:9]
                    majorlabels.append('Other')
                    majordata.append(otherd)
                elif len(majorlabels) == 1 and None in majorlabels:
                    majorlabels = []
                    majordata = []
                else:
                    pass
                # sorting roles
                rolekeys = list(roledict.keys())
                rolevals = list(roledict.values())
                rindex = list(range(len(rolevals)))
                rindex.sort(key=rolevals.__getitem__, reverse=True)
                rolelabels = list(map(rolekeys.__getitem__, rindex))
                roledata = list(map(rolevals.__getitem__, rindex))
                if len(rolelabels) > 10:
                    otherd = sum(roledata[9:])
                    rolelabels = rolelabels[:9]
                    roledata = roledata[:9]
                    rolelabels.append('Other')
                    roledata.append(otherd)
                else:
                    pass
                # sorting grad years
                gradkeys = list(graddict.keys())
                gradvals = list(graddict.values())
                gindex = list(range(len(gradvals)))
                gindex.sort(key=gradvals.__getitem__, reverse=True)
                gradlabels = list(map(gradkeys.__getitem__, gindex))
                graddata = list(map(gradvals.__getitem__, gindex))
                if len(gradlabels) > 10:
                    otherd = sum(graddata[9:])
                    gradlabels = gradlabels[:9]
                    graddata = graddata[:9]
                    gradlabels.append('Other')
                    graddata.append(otherd)
                elif len(gradlabels) == 1 and None in gradlabels:
                    graddata = []
                    gradlabels = []
                else:
                    pass
                # sorting skills
                skillkeys = list(skilldict.keys())
                skillvals = list(skilldict.values())
                sindex = list(range(len(skillvals)))
                sindex.sort(key=skillvals.__getitem__, reverse=True)
                skilllabels = list(map(skillkeys.__getitem__, sindex))
                skilldata = list(map(skillvals.__getitem__, sindex))
                if len(skilllabels) > 10:
                    skilllabels = skilllabels[:10]
                    skilldata = skilldata[:10]
                else:
                    pass
                return TemplateResponse(
                    request,
                    'trunk/manage/competitions/dashboard.html',
                    {
                        'title': 'Event Description',
                        'page_name': page_name,
                        'event': event,
                        'has_data': True,
                        'gradlabels': gradlabels,
                        'graddata': graddata,
                        'majorlabels': majorlabels,
                        'majordata': majordata,
                        'rolelabels': rolelabels,
                        'roledata': roledata,
                        'skilllabels': skilllabels,
                        'skilldata': skilldata,
                        'timeuntil': timeuntil,
                        'usercount': user_count,
                        'eventopen': open,
                        'university': university,
                        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                        'judge_deadline': judge_deadline,
                        'judging_timezone': judging_timezone,
                        'all_ratings': all_ratings,
                        'completed_ratings': completed_ratings,
                        'prev_round': prev_round,
                        'next_round': next_round
                    }
                )
            else:
                return TemplateResponse(
                    request,
                    'trunk/manage/competitions/dashboard.html',
                    {
                        'title': 'Event Description',
                        'page_name': page_name,
                        'event': event,
                        'has_data': False,
                        'timeuntil': timeuntil,
                        'usercount': user_count,
                        'eventopen': open,
                        'university': university,
                        'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                        'judge_deadline': judge_deadline,
                        'judging_timezone': judging_timezone,
                        'all_ratings': all_ratings,
                        'completed_ratings': completed_ratings,
                        'prev_round': prev_round,
                        'next_round': next_round
                    })
        else:
            messages.add_message(request,
                                 messages.Error,
                                 'Competition does not exist')
            return HttpResponseRedirect(reverse('uni-manage-competitions'))


class CreateJudgeBoard(CompetitionOnlyDashboardView):

    def reset_passcode(self, judge_board, newpass):
        '''Reset the passcode for all judges in judge_board
        to newpass but NOT the passcode of the judgeboard'''
        for judge in judge_board.judges.all():
            judge.passcode = newpass
            judge.save()

    def judge_board_view(self, request, event_id):
        '''Separate helper function for viewing the judgeboard
        pre: user already has proper credentials to view and edit
                judgeboard
        why this is in branch app: looking to possibly allow
        users to be able to create, and hence judge, applications.'''

        current_site = request.current_site
        platform = University.objects.get(site=current_site)
        event = Event.objects.get(event_id=event_id)
        errors = []
        messages = []
        try:
            round_numbers = []
            last_round = event.judge_boards.all().order_by('id').last().round
            for num in range(1, (last_round + 1)):
                round_numbers.append(num)
            # get which round we are viewing, latest round by default
            curr_round = request.GET.get('round', last_round)
            board = JudgeBoard.objects.get(competition=event, round=curr_round)
        except:
            round_numbers = [1]
            curr_round = 1
            board = JudgeBoard(competition=event, round=curr_round)
            board.save()
            event.judge_boards.add(board)
        if not event.is_competition:
            raise Http404()

        if event.is_application:
            submissions_word = "applications"
        else:
            submissions_word = "entries"
        default_invite = "You've been invited to judge the event <b>{0}</b>. \
        As a judge for this event, you will have the ability to review {1} \
        and evaluate them. Click the <b>Get Started</b> button to get to your \
        personal login page where you can choose to login with your StartupTree account \
        or the passcode below.<br><br>\
        Keep your unique link and passcode confidential to ensure the safety \
        of your evaluations. We suggest bookmarking the link for easy access!" \
            .format(event.name, submissions_word)

        if board.invite_email is None or board.invite_email == '':
            judge_invite_email_body = TextEditorForm2({'text': default_invite})
        else:
            judge_invite_email_body = TextEditorForm2({'text': board.invite_email})

        all_judges = []
        passcode = board.passcode
        if request.method == 'POST':
            # have to do this again or it only changes the most recent round...
            curr_round = request.POST.get('round')
            board = JudgeBoard.objects.get(competition=event, round=curr_round)
            passform = PasscodeForm(request.POST)
            import_form = ImportForm(request.POST, request.FILES)
            formset = InviteJudgeFormset(request.POST)
            invite_email = TextEditorForm2(request.POST)
            if invite_email.is_valid():
                invite_email_body = invite_email.cleaned_data['text']
                if invite_email_body == '<br>':
                    invite_email_body = default_invite
            else:
                invite_email_body = default_invite
            board.invite_email = invite_email_body
            board.save()
            judge_invite_email_body = TextEditorForm2({'text': board.invite_email})

            # Judging setting related
            passcode_change = False
            if passform.is_valid():
                newpass = passform.cleaned_data['passcode']
                if len(newpass) < 6 or len(newpass) > 20:
                    errors.append('Passcode does not meet the requirements.')
                else:
                    newpass = newpass.strip()
                    if newpass != passcode:
                        self.reset_passcode(board, newpass)
                        passcode_change = True
                    board.passcode = newpass
                    passcode = newpass
            judge_end_date = request.POST.get('judge_end_date')
            judge_end_time = request.POST.get('judge_end_time')
            tz = request.POST.get('board_timezone', None)
            if tz is None:
                tz = event.timezone
            try:
                deadline = timezone.make_aware(
                    get_date_time_object(judge_end_date, judge_end_time),
                    pytz.timezone(tz))
                board.deadline = deadline
                board.timezone = tz
            except:
                errors.append('Please enter a valid judging deadline.')
            board.status = int(request.POST.get('status', 1))
            fast = request.POST.get('allotment', None)
            if (board.status == 1 or board.status == 4) and fast:
                board.allotment = int(fast)
            elif (board.status == 1 or board.status == 4) and not fast:
                if event.is_application:
                    errors.append('No value was entered for minimum Judges per'
                                  ' Applicant. Some changes were saved.')
                else:
                    errors.append('No value was entered for minimum Judges per'
                                  ' Participant. Some changes were saved.')

            blind_judging = request.POST.get('blind_checkbox')
            if blind_judging is None:
                board.is_blind = False
            elif blind_judging == 'on':
                board.is_blind = True
            else:
                board.is_blind = False
            anonymous_judging = request.POST.get('anon_checkbox')
            if anonymous_judging is None:
                board.is_anonymous = False
            elif anonymous_judging == 'on':
                board.is_anonymous = True
            else:
                board.is_anonymous = False
            display_invitation = request.POST.get('display_checkbox')
            if display_invitation is None:
                board.display_invitation = False
            elif display_invitation == 'on':
                board.display_invitation = True
            else:
                board.display_invitation = False
            auto_email = request.POST.get('email_checkbox')
            if auto_email is None:
                board.auto_send_email = False
            elif auto_email == 'on':
                board.auto_send_email = True
            else:
                board.auto_send_email = False
            board.save()

            # judge invite related
            shoot_email = []
            if import_form.is_valid():
                try:
                    excel_file = request.FILES['file']
                    content = excel_file.read()
                    wb = xlrd.open_workbook(file_contents=content)
                    judge_list = []
                    for sheet in wb.sheets():
                        for row in range(sheet.nrows):
                            judge_first = str(sheet.cell(row, 0).value).strip()
                            judge_last = str(sheet.cell(row, 1).value).strip()
                            judge_email = str(sheet.cell(row, 2).value).strip()
                            judge_list.append([judge_first, judge_last, judge_email])
                    for judge in judge_list:
                        try:
                            mem = StartupMember.objects.get(email=judge[2])
                        except:
                            mem = None
                        if not Judge.objects.filter(event=event, round=curr_round, email=judge[2]).exists():
                            new_judge = Judge.objects.create(
                                first_name=judge[0],
                                last_name=judge[1],
                                event=event,
                                round=curr_round,
                                email=judge[2],
                                passcode=passcode,
                                platform=platform,
                                profile=mem)
                            board.add_judge(new_judge)
                            shoot_email.append(new_judge.judge_id)
                        elif Judge.objects.filter(event=event, round=curr_round, email=judge[2], platform=platform,
                                                  is_archived=True).exists():
                            judge = Judge.objects.get(event=event, round=curr_round, email=judge[2], platform=platform,
                                                      is_archived=True)
                            judge.is_archived = False
                            judge.save()
                            shoot_email.append(judge.judge_id)
                    messages.append(
                        "File imported successfully! Judge invitations are being sent out. Please be patient as it may take some time to process all invitations.")
                except xlrd.biffh.XLRDError:
                    errors.append(
                        "Unsupported format or corrupt file. Please make sure the file is a valid Excel spreadsheet (xls, xlsx).")
                except AssertionError:
                    errors.append("Error: Cannot import file.")
            elif len(request.FILES) > 0:
                errors.append(
                    "An error occurred with the file upload. Please make sure you attached the correct file type with the correct format.")
            if formset.is_valid():
                for form in formset.forms:
                    email = form.cleaned_data.get('email')
                    if email is None:
                        continue
                    first_name = form.cleaned_data.get('first_name')
                    last_name = form.cleaned_data.get('last_name')
                    try:
                        mem = StartupMember.objects.get(email=email)
                    except:
                        mem = None
                    if not Judge.objects.filter(event=event, round=curr_round, email=email, platform=platform).exists():
                        new_judge = Judge.objects.create(
                            first_name=first_name,
                            last_name=last_name,
                            event=event,
                            round=curr_round,
                            email=email,
                            passcode=passcode,
                            platform=platform,
                            profile=mem)
                        board.add_judge(new_judge)
                        # Send email to the person!!!
                        shoot_email.append(new_judge.judge_id)
                    elif Judge.objects.filter(event=event, round=curr_round, email=email, platform=platform,
                                              is_archived=True).exists():
                        judge = Judge.objects.get(event=event, round=curr_round, email=email, platform=platform,
                                                  is_archived=True)
                        judge.is_archived = False
                        judge.save()
                        shoot_email.append(judge.judge_id)
                formset = InviteJudgeFormset()
            else:
                errors.append('Info for one or more judges was not completed properly.')

            if len(shoot_email) > 0:
                if board.auto_send_email:
                    transaction.on_commit(lambda: send_judges_link_and_pass(shoot_email,
                                                                            board.competition.event_id,
                                                                            board.id,
                                                                            request.current_site.id))

            # send passcode change notification to existing judges
            if board.auto_send_email and passcode_change:
                transaction.on_commit(lambda: send_passcode_changes(
                    board.judges.filter(round=curr_round, is_archived=False) \
                        .exclude(judge_id__in=shoot_email).values_list('judge_id', flat=True),
                    board.competition.event_id,
                    request.current_site.id
                ))

            is_manage = False
            if 'btn_done' in request.POST:
                is_manage = True
            if 'auto-assign-judge' in request.POST:
                is_manage = True
                if board.status == board.MANUALLY_ASSIGN:
                    errors.append("Cannot auto assign under manual assign preference.")
                else:
                    AssignJudge(event, board).auto_assign_all_applicants(force=True)

            if len(errors) == 0:
                if is_manage and len(errors) == 0:
                    if event.is_application:
                        return redirect(reverse('uni-manage-apps-view', kwargs={'event_id': event_id}))
                    else:
                        return redirect(reverse('uni-manage-comps-view', kwargs={'event_id': event_id}))
                if 'manual_assign_btn' in request.POST:
                    if event.is_application:
                        return redirect(reverse('uni-manage-applicants', kwargs={'event_id': event_id}))
                    else:
                        return redirect(reverse('uni-manage-participants', kwargs={'event_id': event_id}))
                if len(messages) == 0:
                    messages.append('All changes were successfully saved.')
            board.refresh_from_db()
            passform = PasscodeForm(initial={'passcode': passcode})
        else:
            # "GET" request
            passform = PasscodeForm(initial={'passcode': passcode})

            formset = InviteJudgeFormset()

        all_judges = board.judges.filter(round=curr_round, is_archived=False).order_by('first_name')
        status = board.status
        allotment = board.allotment
        if board.deadline:
            end_date = board.get_end_date
            end_time = board.get_end_time
        else:
            end_date = None
            end_time = None
        is_blind = board.is_blind
        is_anonymous = board.is_anonymous
        display_invitation = board.display_invitation
        auto_email = board.auto_send_email

        judges_assigned = JudgeReview.objects.filter(judge__in=all_judges).exists()

        timezones = pytz.common_timezones
        if board.timezone is None:
            board.timezone = event.timezone
            board.save()

        return TemplateResponse(request, 'trunk/manage/competitions/judges.html', {
            'site': current_site,
            'university': platform,
            'board_id': board.id,
            'passform': passform,
            'formset': formset,
            'passcode': passcode,
            'judge_invite_email_body': judge_invite_email_body,
            'display_invitation': display_invitation,
            'status': status,
            'allotment': allotment,
            'all_judges': all_judges,
            'round_numbers': round_numbers,
            'curr_round': int(curr_round),
            'end_date': end_date,
            'end_time': end_time,
            'is_blind': is_blind,
            'is_anonymous': is_anonymous,
            'auto_email': auto_email,
            'judges_assigned': judges_assigned,
            'timezones': timezones,
            'board_timezone': board.timezone,
            'event_id': event.event_id,
            'is_application': event.is_application,
            'can_edit_questions': event.can_edit_questions,
            'can_edit_rubric': event.get_can_edit_rubric(),
            'errors': errors,
            'messages': messages
        })

    def get(self, request, university, event_id):
        return self.judge_board_view(request, event_id)

    def post(self, request, university, event_id):
        return self.judge_board_view(request, event_id)


class ManageCompetitions(CompetitionOnlyDashboardView):
    def get(self,
            request,
            university):
        applications = 'applications' in request.META.get('PATH_INFO')
        if not is_comp_app_admin(request, applications):
            return HttpResponseForbidden()
        rsvps = None
        events = None
        sort_by = request.GET.get('sort', 'start-desc')
        tag_filter = request.GET.get('filter', 'all')
        archive_dropdown_filter = None
        archive_dropdown_filter2 = request.GET.get('sort3', 'unarchived')
        archive_dropdown_filter3 = None

        uni_labels = get_manageable_tags_titles(request)

        # if is_create == "0" or is_create is True:
        #     create_comp = False
        #     page_name = 'manage-competition-review'
        try:
            if applications:
                events_list = sorted_events_admin_panel(request, sort_by, university, archive_dropdown_filter,
                                                        archive_dropdown_filter2, archive_dropdown_filter3,
                                                        True, True, False)
            else:
                events_list = sorted_events_admin_panel(request, sort_by, university, archive_dropdown_filter,
                                                        archive_dropdown_filter2, archive_dropdown_filter3,
                                                        True, False, False)
            # if user and request.is_university and not request.is_super:
            #     events = events.filter(creator=user)
            if tag_filter != 'all':
                events_list = events_list.filter(tags__title=tag_filter)

            paginator = Paginator(events_list, 10)
            page = request.GET.get('page')
            try:
                events = paginator.page(page)
            except PageNotAnInteger:
                events = paginator.page(1)
            except EmptyPage:
                events = paginator.page(paginator.num_pages)
        except Event.DoesNotExist:
            pass
        return TemplateResponse(
            request,
            'trunk/manage/competitions/list.html',
            {
                'events': events,
                'applications': applications,
                'sort_by': sort_by,
                'tag_filter': tag_filter,
                'tags': uni_labels,
                'university': university,
                'archive_dropdown_filter': archive_dropdown_filter2
            }
        )


class CreateCompetition(CreateEvent):
    def get(self, request, university):
        is_edit = False
        is_application = 'applications' in request.META.get('PATH_INFO')
        is_create_template = request.GET.get('as_template', None)
        is_create_template = True if is_create_template == "true" else False
        if not is_comp_app_admin(request, is_application):
            return HttpResponseForbidden()
        event_description = TinyMCEWidget()
        user = request.current_member
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None
        return TemplateResponse(
            request,
            'trunk/manage/competitions/create_edit.html',
            {
                'event_description': event_description,
                'university': university,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                'uni_labels': uni_labels,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform,
                'is_edit': is_edit,
                'is_application': is_application,
                'is_create_template': is_create_template,
                'allow_platform_wide_deadline_reminders': university.style.send_platform_wide_deadline_reminders,
            }
        )

    def post(self, request, university, is_competition=True, is_survey=False):
        return self.create(request, university, is_competition, is_survey)


class EditCompetition(EditEvent):
    def get(self, request, university, event_id):
        is_edit = True
        from_template = request.GET.get("from_template", None)
        try:
            event, event_description = prepare_event(event_id)
        except Event.DoesNotExist:
            raise Http404('Event does not exist')
        if not is_comp_app_admin(request, event['is_application']):
            return HttpResponseForbidden()
        uni_labels = get_manageable_tags_titles(request)
        timezones = pytz.common_timezones
        platform_timezone = university.style.timezone
        if request.platform_group_obj:
            platform_group_name = request.platform_group_obj.name
        else:
            platform_group_name = None
        return TemplateResponse(
            request,
            'trunk/manage/competitions/create_edit.html',
            {
                'event_description': event_description,
                'university': university,
                'GOOGLE_API_KEY': settings.GOOGLE_API_KEY,
                'event': event,
                'is_edit': is_edit,
                'is_application': event['is_application'],
                'from_template': from_template,
                'uni_labels': uni_labels,
                'timezones': timezones,
                'platform_timezone': platform_timezone,
                'platform_group_name': platform_group_name,
                'is_private_platform': request.is_private_platform,
                'allow_platform_wide_deadline_reminders': university.style.send_platform_wide_deadline_reminders,
            }
        )


class CustomizeCompetitionEmails(CompetitionOnlyDashboardView):
    def post(self, request, university, event_id):
        event = None
        if event_id:
            event = Event.objects.get(event_id=event_id)

            uni_style = request.university_style
            if university.program_name is not None:
                program_name = university.program_name
            else:
                program_name = university.name

            if event.is_application:
                submission_type = "application"
                submit_verb = "apply"
                submitted_verb = "applied"
            else:
                submission_type = "entry"
                submit_verb = "enter"
                submitted_verb = "entered"

            job_word = uni_style.job_word
            if job_word.endswith('y'):
                plural_jobs = job_word[:-1]
                plural_jobs += 'ies'
            elif job_word.endswith(('s', 'x', 'z', 'ch', 'sh')):
                plural_jobs = job_word + 'es'
            else:
                plural_jobs = job_word + 's'

            default_member_confirmation = "Congrats! Your {0} to {1} has been \
            successfully submitted.".format(
                submission_type,
                event.name)

            default_guest_confirmation = "Congrats! Your {0} to {1} has been \
            successfully submitted.<br><br>\
            Want to stay informed about more events like this in your community? \
            Join {2}'s StartupTree, the #1 platform for university \
            entrepreneurship. From your account, you can apply for {3}, \
            meet potential team members, and have unlimited access to all the \
            entrepreneurship resources {2} has to offer. \
            And best of all, it's free!".format(
                submission_type,
                event.name,
                program_name,
                plural_jobs)

            default_reminder = "We would like to remind you that \
            there is an upcoming deadline: {0}. If you haven't \
            already {1}, the deadline to {2} is {3} ({4}). \
            We'd love to see you participate!".format(
                event.name,
                submitted_verb,
                submit_verb,
                event.get_formatted_end_datetime(),
                event.timezone)

            member_confirmation_email = EmailEditorForm(request.POST)
            if member_confirmation_email.is_valid():
                member_confirmation_email_body = member_confirmation_email.cleaned_data['email_text']
                if member_confirmation_email_body == '<br>':
                    member_confirmation_email_body = default_member_confirmation
            else:
                member_confirmation_email_body = default_member_confirmation

            guest_confirmation_email = EmailEditorForm2(request.POST)
            if guest_confirmation_email.is_valid():
                guest_confirmation_email_body = guest_confirmation_email.cleaned_data['email_text2']
                if guest_confirmation_email_body == '<br>':
                    guest_confirmation_email_body = default_guest_confirmation
            else:
                guest_confirmation_email_body = default_guest_confirmation

            reminder_email = TextEditorForm2(request.POST)
            if reminder_email.is_valid():
                reminder_email_body = reminder_email.cleaned_data['text']
                if reminder_email_body == '<br>':
                    reminder_email_body = default_reminder
            else:
                reminder_email_body = default_reminder

            event.member_rsvp_confirmation_email = member_confirmation_email_body
            event.guest_rsvp_confirmation_email = guest_confirmation_email_body
            event.reminder_email = reminder_email_body
            event.save()

            messages.add_message(request, messages.SUCCESS, 'Email(s) successfully saved!')

        return self.get(request, university, event_id)

    def get(self, request, university, event_id):
        event = None
        if event_id:
            event = Event.objects.get(event_id=event_id)

            uni_style = request.university_style
            if university.program_name is not None:
                program_name = university.program_name
            else:
                program_name = university.name

            editable_reminder = (event.send_emails_days_prior > 0) or \
                uni_style.enable_event_reminders

            member_conf = event.member_rsvp_confirmation_email
            guest_conf = event.guest_rsvp_confirmation_email
            reminder_email = event.reminder_email

            if event.is_application:
                page_title = 'Custom Application Emails'
                page_name = 'customize-app-emails-view'

                submission_type = "application"
                submit_verb = "apply"
                submitted_verb = "applied"
            else:
                page_title = 'Custom Competition Emails'
                page_name = 'customize-comp-emails-view'

                submission_type = "entry"
                submit_verb = "enter"
                submitted_verb = "entered"

            job_word = uni_style.job_word
            if job_word.endswith('y'):
                plural_jobs = job_word[:-1]
                plural_jobs += 'ies'
            elif job_word.endswith(('s', 'x', 'z', 'ch', 'sh')):
                plural_jobs = job_word + 'es'
            else:
                plural_jobs = job_word + 's'

            default_member_confirmation = "Congrats! Your {0} to {1} has been \
            successfully submitted.".format(
                submission_type,
                event.name)

            default_guest_confirmation = "Congrats! Your {0} to {1} has been \
            successfully submitted.<br><br>\
            Want to stay informed about more events like this in your community? \
            Join {2}'s StartupTree, the #1 platform for university \
            entrepreneurship. From your account, you can apply for {3}, \
            meet potential team members, and have unlimited access to all the \
            entrepreneurship resources {2} has to offer. \
            And best of all, it's free!".format(
                submission_type,
                event.name,
                program_name,
                plural_jobs)

            default_reminder = "We would like to remind you that \
            there is an upcoming deadline: {0}. If you haven't \
            already {1}, the deadline to {2} is {3} ({4}). \
            We'd love to see you participate!".format(
                event.name,
                submitted_verb,
                submit_verb,
                event.get_formatted_end_datetime(),
                event.timezone)

            if member_conf is None or member_conf == '':
                member_confirmation_email_body = EmailEditorForm({'email_text': default_member_confirmation})
            else:
                member_confirmation_email_body = EmailEditorForm({'email_text': member_conf})

            if guest_conf is None or guest_conf == '':
                guest_confirmation_email_body = EmailEditorForm2({'email_text2': default_guest_confirmation})
            else:
                guest_confirmation_email_body = EmailEditorForm2({'email_text2': guest_conf})

            if reminder_email is None or reminder_email == '':
                reminder_email_body = TextEditorForm2({'text': default_reminder})
            else:
                reminder_email_body = TextEditorForm2({'text': reminder_email})

            return TemplateResponse(
                request,
                'trunk/manage/competitions/customize_emails.html',
                {
                    'title': page_title,
                    'page_name': page_name,
                    'university': university,
                    'event': event,
                    'member_confirmation_email_body': member_confirmation_email_body,
                    'guest_confirmation_email_body': guest_confirmation_email_body,
                    'reminder_email_body': reminder_email_body,
                    'editable_reminder': editable_reminder
                }
            )


class ExportCompetition(CompetitionOnlyDashboardView):
    def applicants_data(self, competition, competition_id, uni_id):
        applicants_ue = competition.get_member_applicants()
        applicants_list = []
        for member_applicant in applicants_ue:
            applicant_member = member_applicant.member
            applicant_timestamp = member_applicant.date_applied
            # FIXME: need to include rating score later on
            all_ratings = JudgeReview.objects.filter(applicant_id=applicant_member.id, judge__is_archived=False)
            average_rating = 0
            ratings_num = all_ratings.count()
            # if ratings_num > 0:
            #     average_rating = float(average_rating) / ratings_num
            #     average_rating = int(average_rating + 0.5)
            applicant_name = applicant_member.first_name + " " + applicant_member.last_name
            applicant_email = UserEmail.objects.get(id=applicant_member.user_id)
            applicant_email = "n/a" if applicant_email is None else applicant_email
            if member_applicant and member_applicant.venture:
                applicant_venture = member_applicant.venture
            else:
                applicant_venture = None

            custom = custom_member_dict(applicant_member, uni_id, Form.COMPETITION, competition_id)
            applicants_list.append({'name': applicant_name,
                                    'email': applicant_email,
                                    'timestamp': applicant_timestamp,
                                    'venture': applicant_venture,
                                    'custom': custom,
                                    'ratings': {'all_ratings': all_ratings,
                                                'average_rating': average_rating
                                                }})

        for anonymous_applicant in AnonymousApplicant.objects.filter(event_id=competition.id, is_applied=True, is_archived=False):
            if anonymous_applicant.venture:
                anonymous_applicant_venture = anonymous_applicant.venture
            else:
                anonymous_applicant_venture = None
            temp = {
                'name': anonymous_applicant.name,
                'email': anonymous_applicant.email,
                'timestamp': anonymous_applicant.date_created,
                'venture': anonymous_applicant_venture
            }
            # FIXME: need to include rating score later on
            all_ratings = JudgeReview.objects.filter(anonymous_applicant_id=anonymous_applicant.id, judge__is_archived=False)
            average_rating = 0
            ratings_num = all_ratings.count()
            if ratings_num > 0:
                average_rating = float(average_rating) / ratings_num
                average_rating = int(average_rating + 0.5)
            temp['ratings'] = {
                'all_ratings': all_ratings,
                'average_rating': average_rating
            }
            answers = custom_member_dict(None, uni_id, Form.COMPETITION, competition_id,
                                         anonymous_applicant_id=anonymous_applicant.id)
            temp['custom'] = answers
            applicants_list.append(temp)
        return applicants_list

    def get(self, request, university, event_id=None):

        if university is None:
            app_logger.warning("Security Alert! - export_competition:\
                {0} isn't a university user!".format(request.user))
            return HttpResponseRedirect('/')

        try:
            event = Event.objects.get(event_id=event_id)
        except Event.DoesNotExist:
            raise Exception("Competition does not exist.")
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()

        app_lst = self.applicants_data(event, event_id, university)
        venture_word = university.style.venture_word.capitalize()

        # First check if there are any applicants
        if len(app_lst) == 0:
            if event.is_application:
                app_word = university.style.application_word
                if app_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
                    app_article = 'an'
                else:
                    app_article = 'a'
                messages.add_message(request, messages.ERROR, 'You cannot export data for {0} {1} with no applicants'.format(
                    app_article, app_word))
                return HttpResponseRedirect(reverse('uni-manage-applications'))
            else:
                messages.add_message(request, messages.ERROR, 'You cannot export data for a competition with no participants')
                return HttpResponseRedirect(reverse('uni-manage-competitions'))
        else:
            filtered_name = filtered_filename(event.name)
            filename =  "{0}_export_{1}.xlsx".format(filtered_name, datetime.date.today().strftime("%y%m%d"))
            attachment = BytesIO()
            workbook = xlsxwriter.Workbook(attachment)
            application_sheet = workbook.add_worksheet("Exported")
            wrap_format = workbook.add_format()
            wrap_format.set_text_wrap()

            # Get column heads
            if event.form and event.form.requires_venture:
                if event.is_application:
                    col_head = ["Name", "Email", "Date Applied", "{0}".format(venture_word)]
                else:
                    col_head = ["Name", "Email", "Date Entered", "{0}".format(venture_word)]
            else:
                if event.is_application:
                    col_head = ["Name", "Email", "Date Applied"]
                else:
                    col_head = ["Name", "Email", "Date Entered"]
            start = app_lst[0]
            for q in start['custom']:
                col_head.append("{0}".format(q['q_text']))
            for offset, entry in enumerate(col_head):
                application_sheet.write(0, offset, entry)

            # Now fill spreadsheet
            row_cnt = 1
            for app in app_lst:
                if event.form and event.form.requires_venture:
                    data = ["{0}".format(app['name']), "{0}".format(app['email']),
                            "{0}".format(app['timestamp']), "{0}".format(app['venture'])]
                else:
                    data = ["{0}".format(app['name']), "{0}".format(app['email']),
                            "{0}".format(app['timestamp'])]
                for q in app['custom']:
                    if not q['a_id']:
                        data.append("None")
                    elif q['hidden_until_choice']:
                        data.append("N/A")
                    elif q['q_type'] == 'Paragraph':
                        data.append("{0}".format(q['a_text']))
                    elif q['q_type'] == 'Dropdown' or q['q_type'] == 'Multiple Choice':
                        no_response = True
                        selected_choices = ""
                        for choice in q['q_choices']:
                            if choice['chosen']:
                                no_response = False
                                if len(selected_choices) != 0:
                                    selected_choices += "\n"
                                selected_choices += choice['text']
                        if no_response:
                            selected_choices = "None"
                        data.append(selected_choices)
                    elif q['q_type'] == 'Date Time':
                        data.append("{0}".format(q['a_date']))
                    elif q['q_type'] == 'File Upload':
                        if q['a_url'] is None:
                            data.append("No")
                        else:
                            data.append("Yes")
                for offset, entry in enumerate(data):
                    application_sheet.write(row_cnt, offset, entry, wrap_format)
                row_cnt += 1
            workbook.close()
            attachment.seek(0)
            response = HttpResponse(
                attachment.read(),
                content_type="application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
            attachment.close()
            return response


class DeleteCompetition(DeleteEvent):
    def post(self, request, university, is_competition=True):
        return self.delete(request)


class AdminApplicantsView(UniversityDashboardView):
    def get(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404('The requested event was not found')
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        board = event.judge_board
        if not board:
            judge = None
        raise Http404


class AdminSubmitApplicant(CompetitionOnlyDashboardView):
    def get(self, request, event_id, app_url_name, university):
        comp = Event.objects.get(event_id=event_id, university=university)
        if not is_comp_app_admin(request, comp.is_application):
            return HttpResponseForbidden()
        try:
            sm = StartupMember.objects.get(url_name=app_url_name)
            is_created = False
            if not MemberApplicant.objects.filter(member=sm, event=comp).exists():
                is_created = True
                applicant = MemberApplicant.objects.create(member=sm, event=comp)
            else:
                applicant = MemberApplicant.objects.get(member=sm, event=comp)
            if not is_created:  # default when is_applied wasn't given is True
                applicant.is_applied = True
                applicant.is_viewed_only = False
                applicant.save()
            comp.attending_users.add(sm.user)
        except StartupMember.DoesNotExist:
            try:
                anonymous = AnonymousApplicant.objects.get(applicant_id=app_url_name, event=comp)
                if not anonymous.is_applied:
                    anonymous.is_applied = True
                    anonymous.save()
            except AnonymousApplicant.DoesNotExist:
                pass

        if comp.is_application:
            return HttpResponseRedirect(reverse('uni-manage-applicants', kwargs={'event_id': event_id}))
        else:
            return HttpResponseRedirect(reverse('uni-manage-participants', kwargs={'event_id': event_id}))


class ViewJudges(CompetitionOnlyDashboardView):
    def get(self, request, event_id, university):
        if event_id:
            event = Event.objects.get(event_id=event_id, university=university)
        else:
            raise Http404
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        return view_judges(request, event_id)

    def post(self, request, event_id, university):
        if event_id:
            event = Event.objects.get(event_id=event_id)
            if not is_comp_app_admin(request, event.is_application):
                return HttpResponseForbidden()
        return False


class ViewApps(CompetitionOnlyDashboardView):
    def post(self, request, event_id, university):
        try:
            comp = Event.objects.get(event_id=event_id)
            if not is_comp_app_admin(request, comp.is_application):
                return HttpResponseForbidden()
            action = int(request.POST.get('action', 0))
            round_to_remove = int(request.POST.get('round_to_remove', 0))
            if action == 1:
                # Archive user
                url_name = request.POST.get('uid', None)
                if url_name:
                    try:
                        member = StartupMember.objects.get(url_name=url_name)
                        uid = member.user_id
                        if (comp.invited_users.filter(id=uid).exists() or
                                comp.attending_users.filter(id=uid).exists()):
                            comp.archived_users.add(uid)
                            return JsonResponse({}, status=200)
                        elif Activity.objects.select_related('user').filter(activity_type=25, obj_event_id=comp.id, user_id=member.id).exists():
                            # just in case backward compatibility
                            comp.attending_users.add(uid)
                            comp.archived_users.add(uid)
                            return JsonResponse({}, status=200)
                    except StartupMember.DoesNotExist:
                        try:
                            anonymous = AnonymousApplicant.objects.get(applicant_id=url_name, event=comp)
                            anonymous.is_archived = True
                            anonymous.date_before_archive = anonymous.date_created
                            anonymous.save()
                            return JsonResponse({}, status=200)
                        except AnonymousApplicant.DoesNotExist:
                            app_logger.warning("Member does not exist")
                        pass
            elif action == 2:
                # Unarchive user
                url_name = request.POST.get('uid', None)
                if url_name:
                    try:
                        member = StartupMember.objects.get(url_name=url_name)
                        uid = member.user_id
                        if comp.archived_users.filter(id=uid).exists():
                            comp.archived_users.remove(uid)
                            return JsonResponse({}, status=200)
                    except StartupMember.DoesNotExist:
                        try:
                            anonymous = AnonymousApplicant.objects.get(applicant_id=url_name, event=comp)
                            anonymous.is_archived = False
                            anonymous.save()
                            return JsonResponse({}, status=200)
                        except AnonymousApplicant.DoesNotExist:
                            app_logger.warning("Member does not exist")
                        pass
            if round_to_remove != 0:
                try:
                    judge_board = JudgeBoard.objects.get(competition=comp, round=round_to_remove)
                    form = comp.form
                    for judge in judge_board.judges.all():
                        for rating in judge.ratings.all():
                            rating.delete()
                        judge.delete()
                    judge_board.delete()
                    for rubric in form.rubrics.filter(round=round_to_remove):
                        rubric.delete()
                except:
                    pass
                return view_applicants(request, event_id, university)
        except Event.DoesNotExist:
            pass
        except TypeError:
            return JsonResponse({}, status=500)
        return JsonResponse({}, status=404)

    def get(self, request, event_id, university):
        return view_applicants(request, event_id, university)


class ViewApplication(CompetitionOnlyDashboardView):
    def get(self, request, event_id, university, app_url_name):
        return get_application(request, event_id, app_url_name)


class ManageQuestions(CompetitionOnlyDashboardView):
    '''Compatibility with legacy code. Have to redirect the questions
    through here as in order to have control over admin restrictions
    which cannot be safely implemented in manage_forms'''

    def get(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id)
        except:
            raise Http404
        if request.is_university and is_comp_app_admin(request, event.is_application):
            return manage_comp_forms(request, university, event_id, True, False)
        else:
            raise PermissionDenied

    def post(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id)
        except:
            raise Http404
        if request.is_university and is_comp_app_admin(request, event.is_application):
            return manage_comp_forms(request, university, event_id, True, False)
        else:
            raise PermissionDenied


class GenerateCompFromTemplate(CompetitionOnlyDashboardView):
    ''' Copies over information from the event_id to
    create a new event. Also clones the questions and
    rubrics objects in the form, creating separates
    instances in the databases

    TODO: Take over to branch/view/competitions if we
    want to allow users to create templates for comps'''

    def get(self, request, event_id, university):
        try:
            event = Event.objects.select_related('form').get(
                event_id=event_id,
                university=university
            )
        except:
            try:
                event = Event.objects.select_related('form').get(
                    event_id=event_id,
                    university__short_name='readymadetemplates'
                )
            except:
                raise Http404('No such event exists.')
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()

        is_application = event.is_application
        display_applicants = event.display_applicants
        display_view_count = event.display_view_count
        display_activity_feed = event.display_activity_feed
        start = datetime.datetime.now() + datetime.timedelta(days=7)
        end = start + datetime.timedelta(days=7)
        new_event = Event.objects.create_event(
            name=event.name,
            description=event.description,
            start=start,
            end=end,
            timezone=event.timezone,
            location=event.location,
            university=event.university,
            display_applicants=display_applicants,
            display_view_count=display_view_count,
            display_activity_feed=display_activity_feed,
            pin_to_feed=event.pin_to_feed,
            is_competition=True,
            is_application=is_application,
            is_published=event.is_published,
            send_updates=event.send_updates,
            send_emails_days_prior=event.send_emails_days_prior,
            is_public_competition=event.is_public_competition,
            event_type=7)
        form = Form.objects.create(
            form=3,
            university=university
        )
        new_event.form = form
        new_event.save()
        if event.image:
            new_event.image = event.image
            new_event.save()
        if event.form:
            if event.form.questions:
                questions = []
                choices = []
                for q in event.form.questions.all().order_by('number', 'id'):
                    options = q.choices.all()
                    old_q_id = q.id
                    q.pk = None
                    q.id = None
                    q.save()
                    new_q_id = q.id
                    questions.append({
                        "old_q_id": old_q_id,
                        "new_q_id": new_q_id
                    })
                    if q.q_type == 1 or q.q_type == 2:
                        if q.choices:
                            for c in options:
                                if c.goto_question:
                                    goto_id = c.goto_question.id
                                else:
                                    goto_id = None
                                old_c_id = c.id
                                q.choices.remove(c)
                                c.pk = None
                                c.id = None
                                c.save()
                                new_c_id = c.id
                                if goto_id is not None:
                                    choices.append({
                                        "old_q_id": goto_id,
                                        "new_c_id": new_c_id
                                    })
                                q.add_choice(c)
                    q.save()
                    form.questions.add(q)
                    form.save()
                for choice in choices:
                    new_choice = Choice.objects.get(id=choice["new_c_id"])
                    for question in questions:
                        if question["old_q_id"] == choice["old_q_id"]:
                            new_question = Question.objects.get(id=question["new_q_id"])
                            new_choice.goto_question = new_question
                            new_choice.save()
            if event.form.rubrics:
                for r in event.form.rubrics.all().order_by('number'):
                    r_options = r.choices.all()
                    r.pk = None
                    r.id = None
                    r.save()
                    if r.r_type == 1 or r.r_type == 2:
                        if r.choices:
                            for c in r_options:
                                r.choices.remove(c)
                                c.pk = None
                                c.id = None
                                c.save()
                                r.add_choice(c)
                    r.save()
                    form.rubrics.add(r)
                    form.save()
        new_event.form.save()
        new_event.save()

        if new_event.is_application:
            return redirect(reverse('uni-edit-application',
                kwargs={"event_id": new_event.event_id}) + "?from_template=" + event_id)
        else:
            return redirect(reverse('uni-edit-competition',
                kwargs={"event_id": new_event.event_id}) + "?from_template=" + event_id)


class ViewJudgeReview(UniversityDashboardView):

    def get(self, request, event_id, judge_id, university):
        try:
            event = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        return admin_judge_review(request, event_id, judge_id)


class ApplicationQuestionReorder(CompetitionOnlyDashboardView):

    def get(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        return admin_reorder_questions(request, event_id)

    def post(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        return admin_reorder_questions(request, event_id)


class ApplicationRubricReorder(CompetitionOnlyDashboardView):

    def get(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        return admin_reorder_rubric(request, event_id)

    def post(self, request, event_id, university):
        try:
            event = Event.objects.get(event_id=event_id, university=university)
        except:
            raise Http404
        if not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        return admin_reorder_rubric(request, event_id)


def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    # use short variable names
    static_url = settings.STATIC_URL
    static_root = settings.STATIC_ROOT
    media_url = settings.MEDIA_URL
    media_root = settings.MEDIA_ROOT
    # convert URIs to absolute system paths
    if uri.startswith('/media'):
        if not settings.IS_STAGING and not settings.IS_PRODUCTION:
            path = os.path.join(media_root, uri)
        else:
            path = uri
    elif uri.startswith('/static'):
        if not settings.IS_STAGING and not settings.IS_PRODUCTION:
            path = os.path.join(settings.PROJECT_ROOT, uri)
        else:
            path = uri
    else:
        return uri  # handle absolute uri (ie: http://some.tld/foo.png)
    return path


class ExportApplication(View):
    def get(self, request, event_id, app_url_name=None, judge_id=None):
        university = University.objects.get(site_id=request.current_site.id)
        try:
            event = Event.objects.get(event_id=event_id, university_id=university.id)
        except Event.DoesNotExist:
            return HttpResponseBadRequest()
        is_judge = False
        # allow judges from exporting it.
        judge_id = request.session.get('judge_id')
        if Judge.objects.filter(judge_id=judge_id, event_id=event.id, is_archived=False).exists():
            is_judge = True
        elif not is_comp_app_admin(request, event.is_application):
            return HttpResponseForbidden()
        if not app_url_name:
            # export for all applicants of the competition
            applicant_list = []
            answer_list = []
            if judge_id:
                reviews = JudgeReview.objects.select_related(
                    'event', 'applicant', 'anonymous_applicant', 'form', 'judge').filter(judge__judge_id=judge_id)
                for review in reviews:
                    if review.applicant:
                        applicant_member = review.applicant
                        applicant_list.append(applicant_member.get_full_name())
                        answers = custom_member_dict(applicant_member, university.id, Form.COMPETITION,
                                                     event.event_id, only_is_judge_visible=is_judge)
                        answer_list.append(answers)
                    elif review.anonymous_applicant:
                        anonymous_applicant = review.anonymous_applicant
                        answers = custom_member_dict(None, university.id, Form.COMPETITION,
                                                     event.event_id, only_is_judge_visible=is_judge,
                                                     anonymous_applicant_id=anonymous_applicant.id)
                        applicant_list.append(str(anonymous_applicant))
                        answer_list.append(answers)
            else:
                applicants_ue = event.get_member_applicants()
                anonymous_applicants = AnonymousApplicant.objects.filter(event_id=event.id, is_applied=True,
                                                                         is_archived=False)
                for applicant in applicants_ue:
                    applicant_member = applicant.member
                    applicant_list.append(applicant_member.get_full_name())
                    answers = custom_member_dict(applicant_member, university.id, Form.COMPETITION, event.event_id,
                                                 only_is_judge_visible=is_judge)
                    answer_list.append(answers)
                for anonymous_applicant in anonymous_applicants:
                    answers = custom_member_dict(None, university.id, Form.COMPETITION,
                                                 event.event_id, only_is_judge_visible=is_judge,
                                                 anonymous_applicant_id=anonymous_applicant.id)
                    applicant_list.append(str(anonymous_applicant))
                    answer_list.append(answers)
        else:
            try:
                applicant = StartupMember.objects.get(url_name=app_url_name)
                answers = custom_member_dict(applicant, university.id, Form.COMPETITION, event.event_id, only_is_judge_visible=is_judge)
            except StartupMember.DoesNotExist:
                try:
                    applicant = AnonymousApplicant.objects.get(applicant_id=app_url_name, is_applied=True, event=event)
                    answers = custom_member_dict(None, university.id, Form.COMPETITION,
                                                 event.event_id, only_is_judge_visible=is_judge,
                                                 anonymous_applicant_id=applicant.id)
                except AnonymousApplicant.DoesNotExist:
                    return HttpResponseBadRequest()
            answer_list = [answers]
            applicant_list = [str(applicant)]
        context = {
            'event': event,
            'custom_questions_list': answer_list,
            'apps': applicant_list,
            'full_domain': "{0}".format(request.university.get_url()),
            "ST_GULP_VERSION": settings.ST_GULP_VERSION
        }
        app_logger.info(context)
        template = get_template('trunk/manage/competitions/exported_application.html')

        context.update(static(request))
        context.update(media(request))
        content = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        pisa_status = pisa.CreatePDF(
            content, dest=response, link_callback=link_callback)
        # if error then show some funy view
        if pisa_status.err:
            app_logger.error("Application export error: {0}".format(pisa_status.err))
            if event.is_application:
                return HttpResponse('We had some errors generating export for the {0}. Please contact our staff.'.format(university.style.application_word))
            else:
                return HttpResponse('We had some errors generating export for the competition. Please contact our staff.')
        return response


class ExportCompAttachments(View):
    def get(self, request, event_id, app_url_name=None):
        university = University.objects.get(site_id=request.current_site.id)
        try:
            event = Event.objects.get(event_id=event_id, university_id=university.id)
        except Event.DoesNotExist:
            return HttpResponseBadRequest()
        if not is_comp_app_admin(request, event.is_application):
            # allow judges from exporting it.
            judge_id = request.session.get('judge_id')
            if not Judge.objects.filter(judge_id=judge_id, event_id=event.id, is_archived=False).exists():
                return HttpResponseForbidden()
        # export for all applicants of the competition
        all_files = Answer.objects.filter(q__q_type=5, q__form=event.form,
                                          uploaded_file__isnull=False) \
                                   .order_by('q__number')
        applicant_list = []
        if app_url_name:
            try:
                applicant = StartupMember.objects.get(url_name=app_url_name)
                answers = custom_member_dict(applicant, university.id, Form.COMPETITION, event.event_id, answer_queryset_only=True)
            except StartupMember.DoesNotExist:
                try:
                    applicant = AnonymousApplicant.objects.get(applicant_id=app_url_name, is_applied=True, event=event)
                    answers = custom_member_dict(None, university.id, Form.COMPETITION, event.event_id,
                                                 anonymous_applicant_id=applicant.id, answer_queryset_only=True)
                except AnonymousApplicant.DoesNotExist:
                    return HttpResponseBadRequest()
            file_upload_counts = answers.filter(q__q_type=Question.FU).count()
            if file_upload_counts == 1:
                file_answer = answers.filter(q__q_type=Question.FU)[0]
                return redirect(reverse('answer-attached', kwargs={'pk': file_answer.id}))
            elif file_upload_counts > 1:
                # TODO: need zip logic
                byte_stream = BytesIO()
                zf = zipfile.ZipFile(byte_stream, 'w')
                try:
                    zip_file_dir = '/tmp/{0}'.format(uuid.uuid4())
                    os.mkdir(zip_file_dir)
                    zip_filename = '{0}_files.zip'.format(app_url_name)
                    for file_answer in answers.filter(q__q_type=Question.FU):
                        tmp_file = "{0}/Q{1}_{2}".format(zip_file_dir, file_answer.q.number,
                                                         os.path.basename(file_answer.uploaded_file.name))
                        if settings.DEBUG and not settings.IS_STAGING:
                            copyfile("{0}{1}".format(settings.PROJECT_ROOT, file_answer.get_uploaded_file_path()),
                                     tmp_file)
                        else:
                            try:
                                verify = True
                                if settings.DEBUG:
                                    verify = False
                                file_response = requests.get(file_answer.get_uploaded_file_path(), verify=verify)
                                if file_response.status_code == 200:
                                    with open(tmp_file, 'wb') as tmpf:
                                        tmpf.write(file_response.content)
                            except RuntimeError:
                                continue
                        fdir, fname = os.path.split(tmp_file)
                        zip_path = os.path.join(zip_file_dir, fname)
                        zf.write(tmp_file, fname, zipfile.ZIP_DEFLATED)
                        if os.path.isfile(tmp_file):
                            os.remove(tmp_file)
                    zf.close()
                    os.rmdir(zip_file_dir)
                    response = HttpResponse(byte_stream.getvalue(), content_type="application/x-zip-compressed")
                    response['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
                    return response
                except Exception as e:
                    zf.close()
                    raise e
            else:
                return HttpResponseBadRequest()
        else:
            applicants_ue = event.get_member_applicants()
            anonymous_applicants = AnonymousApplicant.objects.filter(event_id=event.id, is_applied=True)
            for applicant in applicants_ue:
                applicant_member = applicant.member
                applicant_list.append(applicant_member.get_full_name())
            for anonymous_applicant in anonymous_applicants:
                applicant_list.append(str(anonymous_applicant))

        context = {
            'event': event,
            'all_files': all_files,
            'applicants': applicant_list,
            'full_domain': "{0}".format(request.university.get_url()),
            "ST_GULP_VERSION": settings.ST_GULP_VERSION
        }
        template = get_template('trunk/manage/competitions/exported_attachments.html')

        context.update(static(request))
        context.update(media(request))
        content = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        pisa_status = pisa.CreatePDF(
            content, dest=response, link_callback=link_callback)
        # if error then show some funy view
        if pisa_status.err:
            app_logger.error("Application export error: {0}".format(pisa_status.err))
            return HttpResponse('We had some errors generating the file export. Please contact our staff.')
        return response
