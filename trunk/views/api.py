from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from api.views import BasicAuthView
from trunk.models import Major
from rest_framework.views import APIView
from rest_framework import status
from trunk.serializers.university import UniversitySerializer
from django.template.response import TemplateResponse
from trunk.models import University
from trunk.views.abstract import UniversityDashboardView


class AutocompleteMajor(BasicAuthView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(BasicAuthView, self).dispatch(*args, **kwargs)

    def get(self, request):
        query = request.GET.get('q', '')
        sq = SearchQuerySet().models(Major).autocomplete(content_auto=query)
        suggestions = []
        for result in sq:
            suggestions.append(result.name)
        return Response(
            {
            'status':200,
            'body': suggestions
            }
        )


class UniversityList(APIView):
    """
    List all Universities, or create a new University.
    """
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        unis = University.objects.filter(site__isnull=False)
        serializer = UniversitySerializer(unis, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UniversitySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIDocument(UniversityDashboardView):
    def get(self, request, university):

        template = 'layouts/api_doc.html'

        page_name = 'uni-api-doc'

        return TemplateResponse(
            request, template,
            {
                'schema_url':'openapi-schema',
            }
        )
