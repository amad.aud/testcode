# Generated by Django 2.2.11 on 2020-07-24 00:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0141_roadmapblock_uploaded_file'),
    ]

    operations = [
        migrations.AlterField(
            model_name='universitystyle',
            name='require_meeting_approval',
            field=models.PositiveSmallIntegerField(choices=[(0, 'No Approval Required'), (1, 'Require Approval for Office Hours'), (2, 'Require Approval for All Meetings')], default=0),
        ),
    ]
