# Generated by Django 2.2.11 on 2020-04-18 15:42

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0132_univeristyexporthistory_tags'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='enable_waitlist',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='eventrsvp',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eventrsvp',
            name='waitlisted',
            field=models.BooleanField(default=False),
        ),
    ]
