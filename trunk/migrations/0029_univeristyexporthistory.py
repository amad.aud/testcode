# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-06-08 13:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0028_judgereview_is_anonymous'),
    ]

    operations = [
        migrations.CreateModel(
            name='UniveristyExportHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.BinaryField()),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('in_progress', models.NullBooleanField(default=True)),
                ('university', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trunk.University')),
            ],
        ),
    ]
