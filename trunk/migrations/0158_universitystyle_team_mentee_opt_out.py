# Generated by Django 2.2.17 on 2020-11-25 08:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0157_auto_20201119_0211'),
    ]

    operations = [
        migrations.AddField(
            model_name='universitystyle',
            name='team_mentee_opt_out',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Allow'), (2, "Don't Allow")], default=1),
        ),
    ]
