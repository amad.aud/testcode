# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-02-15 19:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0056_merge_20190214_1755'),
    ]

    operations = [
        migrations.AddField(
            model_name='judgereview',
            name='is_sent',
            field=models.BooleanField(default=False),
        ),
    ]
