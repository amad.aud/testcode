# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-10-15 18:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0103_auto_20191009_1524'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessLevel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(choices=[('dashboard', 'Dashboard'), ('users', 'Users'), ('ventures', 'Ventures'), ('projects', 'Projects'), ('groups', 'Groups'), ('tools', 'Tools'), ('tags', 'Tags'), ('export', 'Export'), ('custom_data', 'Custom Data'), ('reporting', 'Reporting'), ('events', 'Events'), ('jobs', 'Jobs'), ('applications', 'Applications/Competitions'), ('surveys', 'Surveys'), ('mentorship', 'Mentorship'), ('team_mentorship', 'Team Mentorship'), ('invite', 'Invite'), ('announce', 'Announce'), ('support', 'Support'), ('admins', 'Admins'), ('program', 'Program'), ('design', 'Design'), ('platform', 'Platform'), ('custom_roles', 'Custom Roles'), ('private_discussion', 'Private Discussion')], max_length=25)),
                ('university', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trunk.University')),
            ],
        ),
        migrations.AddField(
            model_name='universitystaff',
            name='is_super',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='universitystaff',
            name='access_levels',
            field=models.ManyToManyField(to='trunk.AccessLevel'),
        ),
    ]
