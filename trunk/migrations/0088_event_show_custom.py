# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-08-06 13:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0087_merge_20190806_1102'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='show_custom',
            field=models.BooleanField(default=True),
        ),
    ]
