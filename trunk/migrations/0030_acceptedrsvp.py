# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-06-18 13:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0029_univeristyexporthistory'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcceptedRSVP',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(blank=True, default=None, max_length=254, null=True)),
                ('accept_time', models.DateTimeField(auto_now_add=True)),
                ('event', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='trunk.Event')),
            ],
        ),
    ]
