# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2017-12-19 11:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0016_auto_20171214_0430'),
    ]

    operations = [
        migrations.AddField(
            model_name='university',
            name='is_none_university',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='display_applications',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Show'), (0, 'Hide')], default=1),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='display_competitions',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Show'), (0, 'Hide')], default=1),
        ),
    ]
