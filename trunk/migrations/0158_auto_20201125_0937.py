# Generated by Django 2.2.17 on 2020-11-25 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0157_auto_20201119_0211'),
    ]

    operations = [
        migrations.AddField(
            model_name='universitystyle',
            name='enable_group_global_activity',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Off'), (1, 'On')], default=1),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='enable_group_global_discussion',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Off'), (1, 'On')], default=1),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='enable_group_global_events',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Off'), (1, 'On')], default=1),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='enable_group_global_jobs',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Off'), (1, 'On')], default=1),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='enable_group_global_people',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Off'), (1, 'On')], default=1),
        ),
        migrations.AddField(
            model_name='universitystyle',
            name='enable_group_global_startups',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Off'), (1, 'On')], default=1),
        ),
        migrations.AlterField(
            model_name='universitystyle',
            name='default_discussion_view',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Global'), (2, 'Network'), (3, 'Display Only Global View'), (4, 'Display Global within your group')], default=1),
        ),
        migrations.AlterField(
            model_name='universitystyle',
            name='default_events_view',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Global'), (2, 'Network'), (3, 'Display Only Global View'), (4, 'Display Global within your group')], default=1),
        ),
        migrations.AlterField(
            model_name='universitystyle',
            name='default_feed_view',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Global'), (2, 'Network'), (3, 'Display Only Global View'), (4, 'Display Global within your group')], default=1),
        ),
        migrations.AlterField(
            model_name='universitystyle',
            name='default_jobs_view',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Global'), (2, 'Network'), (3, 'Display Only Global View'), (4, 'Display Global within your group')], default=1),
        ),
        migrations.AlterField(
            model_name='universitystyle',
            name='default_people_view',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Global'), (2, 'Network'), (3, 'Display Only Global View'), (4, 'Display Global within your group')], default=1),
        ),
        migrations.AlterField(
            model_name='universitystyle',
            name='default_startups_view',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Global'), (2, 'Network'), (3, 'Display Only Global View'), (4, 'Display Global within your group')], default=1),
        ),
    ]
