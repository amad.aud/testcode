# Generated by Django 2.2.6 on 2019-11-04 20:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0109_auto_20191102_0020'),
    ]

    operations = [
        migrations.RenameField(
            model_name='eventrsvp',
            old_name='affiliated_with',
            new_name='major',
        ),
        migrations.AddField(
            model_name='eventrsvp',
            name='school',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
