# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-03-03 16:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0057_auto_20190219_1652'),
        ('trunk', '0061_universitystyle_project_word'),
        ('trunk', '0061_auto_20190301_1402'),
        ('trunk', '0061_guestrsvp_is_archived'),
    ]

    operations = [
    ]
