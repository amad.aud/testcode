# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-10-17 11:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0099_universitystyle_alphabetize_tags'),
        ('trunk', '0105_auto_20191015_1142'),
    ]

    operations = [
    ]
