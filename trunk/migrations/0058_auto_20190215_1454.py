# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-02-15 14:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0057_auto_20190215_1443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='allow_guest_checkins',
            field=models.BooleanField(default=False),
        ),
    ]
