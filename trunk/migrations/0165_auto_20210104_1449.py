# Generated by Django 2.2.17 on 2021-01-04 19:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trunk', '0164_auto_20201214_0953'),
    ]

    operations = [
        migrations.AddField(
            model_name='universitystyle',
            name='homepage_blurb',
            field=models.CharField(default='', max_length=1000),
        ),
    ]
