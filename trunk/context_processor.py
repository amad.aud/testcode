from django.core.exceptions import ObjectDoesNotExist
from trunk.models import (
    StartupTreeStaff,
    University,
    UniversityStyle,
    UniversitySubscription,
    AccessLevel
)
from StartupTree.caches import (
    get_uni_logo,
    get_uni_eship_web,
    get_uni_website,
    get_uni_blog,
    get_uni_css,
    get_uni_eship_web_name,
)
from branch.templatetags.filters import make_plural, capitalize
import json


def platform_related(request):
    current_site = request.current_site
    try:
        uni = request.university
    except ObjectDoesNotExist:
        uni = None
    is_university = request.is_university
    site_name = current_site.name
    style: UniversityStyle = request.university_style
    subscription = request.subscription
    if uni is not None and style:
        uni_short_name = uni.short_name
        default_feed_view = style.default_feed_view
        display_ventures = style.display_ventures
        display_projects = style.display_projects
        display_mentors = style.display_mentors
        display_team_mentors = subscription.is_team_mentorship
        display_investors = style.display_investors
        display_roadmap = style.display_roadmap
        display_forum = style.display_forum
        display_deals = style.display_deals
        display_jobs = style.display_jobs
        display_events = style.display_events
        display_applications = style.display_applications
        display_competitions = style.display_competitions
        is_api_enabled = subscription.is_api_enabled
        all_tools = style.tools
        mentor_word = style.mentor_word
        mentee_word = style.mentee_word
        mentorship_word = style.mentorship_word
        venture_word = style.venture_word
        team_word = style.team_word
        project_word = style.project_word
        deal_word = style.deal_word
        page_word = style.page_word
        application_word = style.application_word
        job_word = style.job_word
        if job_word.lower().startswith(('a', 'e', 'i', 'o', 'u')):
            job_article = 'an'
        else:
            job_article = 'a'
    else:
        uni_short_name = None
        default_feed_view = None
        display_ventures = None
        display_projects = None
        display_mentors = None
        display_team_mentors = None
        display_investors = None
        display_roadmap = None
        display_forum = None
        display_deals = None
        display_jobs = None
        display_events = None
        display_applications = None
        display_competitions = None
        is_api_enabled = False
        all_tools = None
        mentor_word = 'mentor'
        mentee_word = 'mentee'
        mentorship_word = 'mentorship'
        venture_word = 'venture'
        team_word = 'build team'
        project_word = 'project'
        deal_word = 'deal'
        page_word = 'page'
        application_word = 'application'
        job_word = 'job'
        job_article = 'a'


    primary_color = '#00ce88'
    if style is None:
        main_css = None
        panel_css = None
        uni_logo = None
        eship_web = None
        eship_name = None
        uni_website = None
        uni_blog = None
    else:
        main_css = style.main_css
        panel_css = style.panel_css
        if style.first_color:
            primary_color = style.first_color

        uni_logo = style.logo
        eship_web = style.eship_website
        eship_name = uni.program_name
        uni_website = style.uni_website
        uni_blog = style.uni_blog

    is_resource = None
    is_track = None
    is_grow = None
    is_engage = None

    is_membership_fee = False

    if current_site.id > 1:
        is_resource = request.is_resource
        is_track = request.is_track
        is_grow = request.is_grow
        is_engage = request.is_engage
        is_membership_fee = request.is_membership_fee
    is_staff = False
    can_view_templates = False
    access_levels = []
    if request.user.is_authenticated and not request.is_archived:
        is_staff = StartupTreeStaff.objects.filter(staff_id=request.user.id)
        access_levels = request.access_levels
        can_view_templates = AccessLevel.EVENTS in access_levels or \
            AccessLevel.COMPETITIONS in access_levels or \
            AccessLevel.APPLICATIONS in access_levels or \
            AccessLevel.SURVEYS in access_levels or \
            AccessLevel.REPORTING in access_levels

    custom_words = {
        "ventureWord": capitalize(venture_word),
        "venturesWord": capitalize(make_plural(venture_word)),
        "projectWord": capitalize(project_word),
        "projectsWord": capitalize(make_plural(project_word)),
        "jobWord": capitalize(job_word),
        "jobsWord": capitalize(make_plural(job_word)),
        "aJobWord": job_article + " " + job_word,
        "mentorshipWord": capitalize(mentorship_word),
        "mentorWord": capitalize(mentor_word),
        "mentorsWord": capitalize(make_plural(mentor_word)),
        "pageWord": capitalize(page_word),
        "pagesWord": capitalize(make_plural(page_word)),
        "dealWord": capitalize(deal_word),
        "dealsWord": capitalize(make_plural(deal_word)),
        "applicationWord": capitalize(application_word),
        "applicationsWord": capitalize(make_plural(application_word)),
        "menteeWord": capitalize(mentee_word),
        "menteesWord": capitalize(make_plural(mentee_word)),
    }

    display_modules = {
        "displayVentures": display_ventures,
        "displayProjects": display_projects,
        "displayMentors": display_mentors,
        "displayTeamMentors": display_team_mentors,
        "displayInvestors": display_investors,
        "displayRoadmap": display_roadmap,
        "displayForum": display_forum,
        "displayDeals": display_deals,
        "displayJobs": display_jobs,
        "displayEvents": display_events,
        "displayApplications": display_applications,
        "displayCompetitions": display_competitions,
        'subscription': request.is_membership_fee
    }

    result = {
        "platform_color": primary_color,
        "is_startuptree_staff": is_staff,
        'access_levels': access_levels,
        'eship_name': eship_name,
        'eship_web': eship_web,
        'uni_web': uni_website,
        'uni_blog': uni_blog,
        'UNI_CSS': main_css,
        'UNI_PANEL_CSS': panel_css,
        'is_university': is_university,
        'uni_logo': uni_logo,
        'uni_short_name': uni_short_name,
        'is_valid_platform': request.is_valid_platform,
        'is_archived': request.is_archived,
        'default_feed_view': default_feed_view,
        # Enable mentor and discussions are for limiting platform usage
        # based on subscription later on
        'enable_mentor': True,
        'is_engage': is_engage,
        'is_track': is_track,
        'is_grow': is_grow,
        'is_resource': is_resource,
        'is_api_enabled': is_api_enabled,
        'is_membership_fee': is_membership_fee,
        'display_ventures': display_ventures,
        'display_projects': display_projects,
        'display_mentors': display_mentors,
        'display_team_mentors': display_team_mentors,
        'display_investors': display_investors,
        'display_roadmap': display_roadmap,
        'display_forum': display_forum,
        'display_deals': display_deals,
        'display_jobs': display_jobs,
        'display_events': display_events,
        'display_applications': display_applications,
        'display_competitions': display_competitions,
        'all_tools': all_tools,
        'mentor_word': mentor_word,
        'mentee_word': mentee_word,
        'mentorship_word': mentorship_word,
        'venture_word': venture_word,
        'team_word': team_word,
        'project_word': project_word,
        'deal_word': deal_word,
        'page_word': page_word,
        'application_word': application_word,
        'job_word': job_word,
        'can_view_templates': can_view_templates,
        'custom_words': custom_words,
        'display_modules': display_modules,
        'display_modules_json': json.dumps(display_modules, default=str),
    }

    return result
