from branch.models import (
    Startup,
    Experience,
    Industry,
    Exit,
    Investment,
    StartupMember,
    Degree,
    EmployeeTitle
)
from trunk.models import School, University
from StartupTree.utils import dollar_string_to_number, number_to_dollar_string
import csv
import copy
import xlrd  # for reading excel files
from StartupTree.settings import PROJECT_ROOT, path


FUNDING_FIELD = [
    'Funding Round {0} ($)',
    'Funding Round {0} Date',
    'Funding Round {0} Investor',
    'Angel or VC'
]

EXIT_FIELD = [
    'Exit Value ($)',
    'Exit Date',
    'Acquired or IPO',
]

FOUNDER_FIELD = [
    'FIRST NAME',
    'LAST NAME',
    'TITLE',
    'EMAIL',
    'PHONE',
    'WEBSITE',
    'GRAD YEAR',
    'SCHOOL'
]

# For Alumni, Advisors, etc.
PERSON_FILED = [
    'FIRST NAME {0}',
    'LAST NAME {0}',
]

COMPANY_RELATED_FIELD = [
    'NAME',
    'YEAR FOUNDED',
    'INDUSTRY',
    'LOCATION',
    'EMPLOYEES (#)',
    'PITCH (200 CHARACTERS LIMIT)',
    'GENERAL/PRODUCT',
    'EMAIL',
    'WEBSITE',
    'FACEBOOK',
    'TWITTER',
    'LINKEDIN',
]

Q_RESOURCE_FIELD = [
    'YEAR',
    'QUARTER',
    'REVENUE ($)',
    'FULL TIME EMPLOYEE (#)',
    'TOTAL ANNUAL PAYROLL ($)',
    'AVERAGE ANNUAL SALARY ($)'
]

FIELDS_TO_STARTUP_DIC = {
    COMPANY_RELATED_FIELD[0]: 'name',
    COMPANY_RELATED_FIELD[1]: 'year founded',
    COMPANY_RELATED_FIELD[2]: 'industry',
    COMPANY_RELATED_FIELD[3]: 'location',
    COMPANY_RELATED_FIELD[4]: 'employees',
    COMPANY_RELATED_FIELD[5]: 'pitch',
    COMPANY_RELATED_FIELD[6]: 'general',
    COMPANY_RELATED_FIELD[7]: 'email',
    COMPANY_RELATED_FIELD[8]: 'website',
    COMPANY_RELATED_FIELD[9]: 'facebook',
    COMPANY_RELATED_FIELD[10]: 'twitter',
    COMPANY_RELATED_FIELD[11]: 'linkedin',
}

UNKNOWN = 'n/a'

DEFAULT_LESS_FILE = ''

DEFAULT_PANEL_LESS_FILE = ''

# css_path = path.join(PROJECT_ROOT, 'static', 'css')

# with open(path.join(css_path, 'colors.less')) as f:
#     DEFAULT_LESS_FILE = f.read()
# with open(path.join(css_path, 'fonts.less')) as f:
#     DEFAULT_LESS_FILE += f.read()
# with open(path.join(css_path, 'base.less')) as f:
#     DEFAULT_LESS_FILE += f.read()
# DEFAULT_PANEL_LESS_FILE = copy.deepcopy(DEFAULT_LESS_FILE)
# with open(path.join(css_path, 'main.less')) as f:
#     DEFAULT_LESS_FILE += f.read()
# with open(path.join(css_path, 'uni-panel.less')) as f:
#     DEFAULT_PANEL_LESS_FILE += f.read()


def month_year_to_date(string):
    exited = string.split('/')
    if len(exited) == 2:
        date = "{1}-{0}-01".format(exited[0], exited[1])
    elif len(exited) == 1:
        date = "{0}-01-01".format(exited)
    else:
        raise ValueError("Date {0} is not valid".format(string))
    return date


class ImportStartup:
    def __init__(self, university):
        self.startups = []
        self.created = []
        self.university = university

    def _create_funding(self, funding_list, startup):
        inv_bulk = []
        for funding_dict in funding_list:
            amount = funding_dict['amount']
            funded_on = funding_dict['funded_on']
            investor = funding_dict['investor']
            is_individual = funding_dict['is_individual']
            is_private = funding_dict['is_private']
            if investor is None:
                inv_bulk.append(Investment(startup=startup,
                                           amount=amount,
                                           funded_on=funded_on,
                                           is_private=is_private,
                                           ))
            elif is_individual:
                names = investor.split(' ')
                first_name = names[0]
                last_name = " ".join(names[1:])
                investor = StartupMember.objects.goc_member(
                    first_name,
                    last_name,
                    goc=True)
                inv_bulk.append(Investment(startup=startup,
                                           member=investor,
                                           amount=amount,
                                           funded_on=funded_on,
                                           is_private=is_private,
                                           ))
            else:
                investor = Startup.objects.goc_startup(investor)
                inv_bulk.append(Investment(startup=startup,
                                           organization=investor,
                                           amount=amount,
                                           funded_on=funded_on,
                                           is_private=is_private,
                                           ))
        Investment.objects.bulk_create(inv_bulk)

    def _create_founder(self, founder_list, startup):
        exp_bulk = []
        for founder_dict in founder_list:
            first_name = founder_dict['first_name']
            last_name = founder_dict['last_name']
            website = founder_dict['website']
            phone = founder_dict['phone']
            email = founder_dict['email']
            school = founder_dict['school']
            grad_year = founder_dict['grad_year']
            position = founder_dict['position']
            try:
                member = StartupMember.objects.filter(
                    first_name=first_name,
                    last_name=last_name,
                    universities=self.university
                )
                if len(member) < 1:
                    raise StartupMember.DoesNotExist()
                else:
                    member = member[0]
                member.website = website
                member.phone = phone
                member.email = email
                member.save()
            except StartupMember.DoesNotExist:
                member = StartupMember.objects.create_sm(
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    phone=phone,
                    website=website,
                )
                member.universities.add(self.university)
            if school is not None:
                if not member.degrees.filter(school_id=school.id).exists():
                    degree = Degree.objects.create(school=school,
                                                   graduated_on=grad_year)
                    member.degrees.add(degree)
            if not Experience.objects.filter(member=member,
                                             startup=startup,
                                             role=position,
                                             is_founder=True).exists():
                exp_bulk.append(Experience(member=member,
                                           startup=startup,
                                           role=position,
                                           is_founder=True))
        Experience.objects.bulk_create(exp_bulk)
        return True

    def _create_exit(self, exit_dict, startup):
        amount = exit_dict['amount']
        date = exit_dict['exited_on']
        exit_type = exit_dict['exit_type']  # IPO | Merger | Acq
        is_undisclosed = exit_dict['is_undisclosed']
        if exit_type.lower() == 'merger':
            exit_type = 'Meger'
        elif exit_type.lower() == 'closed':
            exit_type = 'Close'
        elif exit_type.lower() == 'acquired':
            exit_type = 'Acq'
        Exit.objects.create(startup=startup,
                            exit_type=exit_type,
                            exited_on=date,
                            amount=amount,
                            is_undisclosed=is_undisclosed,
                            )
        return True

    def _create_alum(self, alumn_list, startup):
        exp_bulk = []
        for alumn_dict in alumn_list:
            first_name = alumn_dict['first_name']
            last_name = alumn_dict['last_name']
            member = StartupMember.objects.goc_member(
                        first_name,
                        last_name,
                        universities=[self.university],
                        goc=True
                    )
            if not Experience.objects.filter(startup=startup,
                                             member=member).exists():
                exp_bulk.append(Experience(startup=startup, member=member))
        Experience.objects.bulk_create(exp_bulk)
        return True

    def _create_advisor(self, advisor_list, startup):
        exp_bulk = []
        for advisor_dict in advisor_list:
            position = EmployeeTitle.objects.create_employee_title('advisor')
            first_name = advisor_dict['first_name']
            last_name = advisor_dict['last_name']
            member = StartupMember.objects.goc_member(
                        first_name,
                        last_name,
                        goc=True
                    )
            if not Experience.objects.filter(startup=startup,
                                             member=member,
                                             role=position).exists():
                exp_bulk.append(Experience(member=member, role=position))
        Experience.objects.bulk_create(exp_bulk)
        return True

    def create_startup(self):
        for startup in self.startups:
            name = startup['name']
            university = startup['university']
            industry = startup['industry']
            pitch = startup['pitch']
            general = startup['general']
            num_emp = startup['employees']
            location = startup['location']
            founded_on = startup['year founded']
            website = startup['website']
            twitter = startup['twitter']
            facebook = startup['facebook']
            linkedin = startup['linkedin']
            email = startup['email']
            new_startup = Startup.objects.create_startup(
                name,  # name
                university,  # university
                industry,  # industry
                pitch,  # pitch
                general,  # general
                num_employees=num_emp,
                location=location,
                founded_on=founded_on,
                website=website,
                twitter=twitter,
                facebook=facebook,
                linkedin=linkedin,
                email=email,
            )
            # fundings
            self._create_funding(startup['funding'], new_startup)
            # exit
            self._create_exit(startup['exit'], new_startup)
            # founders
            self._create_founder(startup['founders'], new_startup)
            # alumni
            self._create_alum(startup['alumni'], new_startup)
            # advisors
            self._create_advisor(startup['advisors'], new_startup)
            new_startup.save()
            self.created.append(new_startup)
        self.startups = []
        return self.created

    def add_startup_dict(self, startup_dict):
        '''Add startup dictionary to startups to create it later.
        '''
        if not isinstance(startup_dict, StartupDictionary):
            raise ValueError('Please provide StartupDictionary Object.')
        self.startups.append(startup_dict.startup_dict)


class StartupDictionary:
    def __init__(self, university):
        self.startup_dict = {
            'funding': [],
            'founders': [],
            'alumni': [],
            'advisors': [],
        }
        self.university = university
        self.startup_dict['university'] = self.university

    def set_geography(self, city, state, state_abbrev=True):
        """TODO
        """
        return None

    def set_name(self, name):
        self.startup_dict['name'] = name

    def set_industry(self, industry_name):
        industry = None
        if industry_name is not None and industry_name != '':
            industry, _ = \
                Industry.objects.get_or_create(name=industry_name.lower())
        self.startup_dict['industry'] = industry
        return industry

    def set_pitch(self, pitch):
        self.startup_dict['pitch'] = pitch

    def set_general(self, general):
        self.startup_dict['general'] = general

    def set_founded_on(self, founded_on):
        if isinstance(founded_on, str) and len(founded_on) == 0:
            founded_on = None
        else:
            founded_on = int(founded_on)
        self.startup_dict['year founded'] = founded_on

    def set_employees(self, num_employees):
        if isinstance(num_employees, str) and len(num_employees) == 0:
            num_employees = None
        else:
            num_employees = int(num_employees)
        self.startup_dict['employees'] = num_employees

    def set_website(self, website):
        if len(website) == 0:
            website = None
        self.startup_dict['website'] = website

    def set_twitter(self, twitter):
        if len(twitter) == 0:
            twitter = None
        self.startup_dict['twitter'] = twitter

    def set_facebook(self, facebook):
        if len(facebook) == 0:
            facebook = None
        self.startup_dict['facebook'] = facebook

    def set_linkedin(self, linkedin):
        if len(linkedin) == 0:
            linkedin = None
        self.startup_dict['linkedin'] = linkedin

    def set_github(self, github):
        if len(github) == 0:
            github = None
        self.startup_dict['github'] = github

    # def set_google_plus(self, gplus):
    #     if len(gplus) == 0:
    #         gplus = None
    #     self.startup_dict['gplus'] = gplus

    def set_instagram(self, instagram):
        if len(instagram) == 0:
            instagram = None
        self.startup_dict['instagram'] = instagram

    def set_other_link(self, other_link):
        if len(other_link) == 0:
            other_link = None
        self.startup_dict['other_link'] = other_link

    def set_email(self, email):
        if len(email) == 0:
            email = None
        self.startup_dict['email'] = email

    def set_exit(self, exit_value, exit_date, exit_type):
        '''Create Exit dictionary
        ('IPO', 'IPO'),
        ("Meger", "Merger"),
        ("Acq", "Acquisition"),
        ("Close", "Closed"),
        '''
        is_undisclosed = False
        exit_type = exit_type.lower()
        if exit_value == '':
            exit_value = None
        elif str(exit_value).lower() == 'undisclosed':
            exit_value = None
            is_undisclosed = True
        elif exit_value != '' and exit_value is not None:
            exit_value = int(exit_value)
        else:
            exit_value = None
        if exit_date == '':
            exit_date = None
        exited = {
            'exit_type': exit_type,
            'exited_on': exit_date,
            'amount': exit_value,
            'is_undisclosed': is_undisclosed,
        }
        self.startup_dict['exit'] = exited

    # fundings
    def set_funding(self, amount, funded_on, investor, is_individual):
        is_private = False
        if amount == '':
            amount = None
        elif str(amount).lower() == 'private':
            is_private = True,
            amount = None
        if funded_on == '':
            funded_on = None
        if investor == '':
            investor = None
        if is_individual.lower() == 'angel':
            is_individual = True
        else:
            is_individual = False
        funding = {
            'amount': amount,
            'funded_on': funded_on,
            'investor': investor,
            'is_individual': is_individual,
            'is_private': is_private
        }
        self.startup_dict['funding'].append(funding)

    def set_founder(self, first_name, last_name, title, email,
                    phone, website, grad_year, school):
        if grad_year == '':
            grad_year = None
        if school == '':
            school = None
        else:
            school = School.objects.create_school(school, self.university)
        position = None
        if title != '':
            position = EmployeeTitle.objects.create_employee_title(title)
        tmp = {
            'position': position,
            'is_founder': True,
            'first_name': str(first_name),
            'last_name': str(last_name),
            'school': school,
            'grad_year': grad_year,
            'email': email,
            'phone': phone,
            'website': website
        }
        self.startup_dict['founders'].append(tmp)

    def set_alumni(self, first_name, last_name):
        tmp = {'first_name': str(first_name), 'last_name': str(last_name)}
        self.startup_dict['alumni'].append(tmp)

    def set_advisors(self, first_name, last_name):
        tmp = {'first_name': str(first_name), 'last_name': str(last_name)}
        self.startup_dict['advisors'].append(tmp)
