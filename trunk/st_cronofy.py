from branch.models import StartupMember
from mentorship.st_cronofy import (
    cronofy_delete_from_calendar,
    get_cronofy_calendar_id,
    is_cronofy_linked,
)
from mentorship.models import MentorshipSession, TeamMentorshipSession
from mentorship.tasks import admin_add_session_to_cronofy
from trunk.models import (
    AdminCalSyncOption,
    UniversityStaff
)
from StartupTree.loggers import app_logger
from StartupTree.utils import QuerySetType

from abc import ABC, abstractmethod
from django.utils import timezone
from typing import Generic, TypeVar

T = TypeVar('T')


class AbstractAdminCalSyncUtil(ABC, Generic[T]):
    sync_type: int
    uni_staff: UniversityStaff

    def __init__(self, sync_type: int, uni_staff: UniversityStaff):
        self.sync_type = sync_type
        self.uni_staff = uni_staff

    def init_sync(self):
        """
        When an admin (uni_staff) enables calendar sync for a particular event type (sync_type):

        First perform checks:
            Security Check:
                Checks that the admin is authorized with the certain access levels.
            Precondition Check:
                Checks that the admin is cronofy-linked.

        Then adds all applicable events to admin's calendar.

        """
        if not self.__precondition_and_security_check():
            return

        events = self.get_events()

        for event in events:
            self.add_event_to_admin_cal(event)

    def remove_sync(self):
        """
        When an admin (uni_staff) disables calendar sync for a particular event type (sync_type):

        Then removes all applicable events from admin's calendar.

        Requires that admin is cronofy-linked.
        """
        if not self.__cronofy_check():
            return

        events = self.get_events()

        for event in events:
            self.delete_event_from_admin_cal(event)

    def __precondition_and_security_check(self):
        """
        Security Check:
            Checks that the admin is authorized with the certain access levels.
        Precondition Check:
            Checks that the admin is cronofy-linked.

        Prints an app_logger.error if any check fails.

        Returns True iff all checks pass.
        """
        uni_staff = self.uni_staff

        if not AdminCalSyncOption.is_admin_authorized(self.uni_staff, self.sync_type):
            app_logger.error(f'Admin {uni_staff} is not authorized. Nothing done.')
            return False

        if not self.__cronofy_check():
            return False

        return True

    def __cronofy_check(self):
        """
        Returns whether the admin is cronofy-linked.
        Prints an app_logger.error if not.
        """
        uni_staff = self.uni_staff
        if not is_cronofy_linked(uni_staff.admin):
            app_logger.error(f'Admin ({uni_staff}) is not cronofy-linked. Nothing was done. ')
            return False
        return True

    @abstractmethod
    def get_events(self) -> QuerySetType[T]:
        """ returns a queryset of events that should be added to the admin's calendar.
        eg, for an admin subscribing to mentorship meeting, then it's the queryset of
        all upcoming mentorship meetings on the admin's platform.

        IMPORTANT: make sure to exclude events that should already be on the admin's calendar anyway. (eg mentorship
        meetings that the mentor is already participating in; if Admin disables admin mentorship calendar sync,
        then all events from this query will be removed from their personal calendar; we want mentorship meetings
        that the mentor is already participating in to persist.)

        """
        pass

    @abstractmethod
    def add_event_to_admin_cal(self, event: T):
        """ Adds the event to the admin's cronofy-synced calendar.  """
        pass

    @abstractmethod
    def delete_event_from_admin_cal(self, event: T):
        """ Deletes the event from the admin's cronofy-synced calendar. """
        pass


class AdminCalSyncUtilMentorshipMeetings(AbstractAdminCalSyncUtil):
    """ One-To-One Mentorship Only """
    def __init__(self, uni_staff: UniversityStaff):
        super().__init__(sync_type=AdminCalSyncOption.MENTORSHIP_MEETINGS, uni_staff=uni_staff)

    def get_events(self) -> QuerySetType[MentorshipSession]:
        uni_staff = self.uni_staff
        uni = uni_staff.university

        member = StartupMember.objects.get_user(uni_staff.admin)

        # In the future, if admin mentees are syncing their own mentorship meetings (which they are attending as mentee)
        # to their personal calendar, then this query will need to exclude those.
        return MentorshipSession.objects.filter(
            mentorship__mentor__platform_id=uni.id,
            datetime__gte=timezone.now(),
            is_cancelled=False,
            is_admin_accepted=True,
            is_mentor_accepted=True,
        ).exclude(mentorship__mentor__profile=member)

    def add_event_to_admin_cal(self, event: MentorshipSession):
        admin_add_session_to_cronofy(self.uni_staff, event)

    def delete_event_from_admin_cal(self, event: MentorshipSession):
        admin_user = self.uni_staff.admin
        calendar_id = get_cronofy_calendar_id(admin_user)
        cronofy_delete_from_calendar(admin_user, calendar_id, event.id, refresh_cronofy_cache=False)


class AdminCalSyncUtilTeamMentorshipMeetings(AbstractAdminCalSyncUtil):
    def __init__(self, uni_staff: UniversityStaff):
        super().__init__(sync_type=AdminCalSyncOption.TEAM_MENTORSHIP_MEETINGS, uni_staff=uni_staff)

    def get_events(self) -> QuerySetType[TeamMentorshipSession]:
        uni_staff = self.uni_staff
        uni = uni_staff.university

        member = StartupMember.objects.get_user(uni_staff.admin)

        # In the future, if admin mentees are syncing their own mentorship meetings (which they are attending as mentee)
        # to their personal calendar, then this query will need to exclude those.
        return TeamMentorshipSession.objects.filter(
            mentorships__mentor__platform_id=uni.id,
            datetime__gte=timezone.now(),
            is_cancelled=False,
        ).exclude(mentorships__mentor__profile=member).distinct()

    def add_event_to_admin_cal(self, event: TeamMentorshipSession):
        admin_add_session_to_cronofy(self.uni_staff, event)

    def delete_event_from_admin_cal(self, event: TeamMentorshipSession):
        admin_user = self.uni_staff.admin
        calendar_id = get_cronofy_calendar_id(admin_user)
        cronofy_delete_from_calendar(admin_user, calendar_id, event.id, refresh_cronofy_cache=False)
