from rest_framework import serializers
from notifications.models import Notification


class NotificationSerializer(serializers.ModelSerializer):
    # type is not included here because we had to use Notification instead of EventNotification for this to work
    # type is 'event' but putting it in Notification doesn't make sense
    url = serializers.CharField()
    img = serializers.CharField(source='img_url')
    time = serializers.CharField()
    desc = serializers.CharField()

    class Meta:
        model = Notification
        fields = ('url', 'img', 'time', 'desc')
