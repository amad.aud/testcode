from rest_framework import serializers
from trunk.models import Event
import pytz


class EventSerializer(serializers.ModelSerializer):
    rsvp_url = serializers.CharField()
    image = serializers.CharField(source='image_url')
    description = serializers.CharField(source='stripped_description')
    startdate = serializers.CharField(source='formatted_start_date')
    enddate = serializers.CharField(source='formatted_end_date')
    endtime = serializers.CharField(source='formatted_end_time')
    starttime = serializers.CharField(source='formatted_start_time')
    university = serializers.CharField(source='university.get_program_name')
    free_food = serializers.BooleanField()
    free_swag = serializers.BooleanField()
    show_rsvp = serializers.SerializerMethodField()
    type_event = serializers.SerializerMethodField()
    action_word = serializers.CharField()
    industry_names = serializers.SerializerMethodField()
    url = serializers.URLField(source='get_full_url')

    def get_industry_names(self, obj):
        return obj.industries.all().values_list('name', flat=True)

    def get_type_event(self, obj):
        return obj.get_event_type_display()

    def get_show_rsvp(self, obj):
        num_participants = obj.get_attending_user_without_archive().count()
        return (obj.allow_rsvp and not obj.allow_max_rsvp) or (obj.allow_max_rsvp and obj.max_rsvp > num_participants)

    class Meta:
        model = Event
        fields = (
        'event_id', 'rsvp_url', 'image', 'name', 'description', 'startdate', 'enddate', 'endtime', 'starttime',
        'location', 'university', 'free_food', 'free_swag', 'attending_users', 'show_rsvp', 'type_event',
        'industry_names', 'url', 'is_application', 'is_competition', 'action_word', 'not_started')


class EventAPISerializer(serializers.ModelSerializer):
    start = serializers.DateTimeField(default_timezone=pytz.timezone('UTC'))
    end = serializers.DateTimeField(default_timezone=pytz.timezone('UTC'))
    description = serializers.CharField(source='get_description')
    url = serializers.URLField(source='get_full_url')

    class Meta:
        model = Event
        fields = ('event_id', 'name', 'start', 'end', 'location', 'description', 'url')
