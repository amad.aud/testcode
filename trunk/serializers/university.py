from rest_framework import serializers
from trunk.models import UniversityStyle, University, School, Major
from emailuser.serializers import UserEmailSerializer, user_to_dict


class UniversityStyleSerializer(serializers.ModelSerializer):
    class Meta:
        model = UniversityStyle
        exclude = ('_university', )


class UniversitySerializer(serializers.ModelSerializer):
    style = UniversityStyleSerializer(read_only=True)
    # staffs = UserEmailSerializer(many=True, read_only=True)
    # staffs field in University is now deprecated. now use uni_staff 

    class Meta:
        model = University
        fields = '__all__'


class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = School
        fields = '__all__'


class MajorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Major
        fields = '__all__'

# def university_to_dict(uni):
#     university = {}
#     university['name'] = uni.name
#     university['short_name'] = uni.short_name
#     university['program_name'] = uni.program_name
#     university['staff'] = []
#     for i in uni.staffs: # CAREFUL staffs is depricated
#         university['staff'].append(user_to_dict(i))
#     return uni
